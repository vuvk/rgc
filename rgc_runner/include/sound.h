#ifndef SOUND_H
#define SOUND_H

#include <vector>
#include <map>
#include "audio_system.h"
//#include "audio_buffer.h"
//#include "audio_source.h"
#include "res_in_mem.h"

/*
class SoundInMem : public BaseResourceInMem
{
public:
    AudioBuffer* buffer = nullptr;

    void clear()
    {
        delete buffer;
    }
};
*/

class SoundBuffer : public AudioBuffer
{
public:
    int id = -1;    // id звука или положение в списке по порядку
                    // назначается при загрузке звуков из паков
    SoundBuffer(const std::string& fileName, bool isStreamed = false) : AudioBuffer(fileName, isStreamed) {};
};


class SoundNode : public AudioSource
{
public :
    SoundNode(AudioBuffer* buffer = nullptr);
    ~SoundNode();

    void play(bool isLoop = false);
    void playOnce();

    /*
    void update();
    */

    inline bool isPlayOnce();
    inline unsigned int getId();

private:
    unsigned int id = 0;
    bool m_playOnce = false;
};

extern std::map<std::string, SoundBuffer*> g_SoundBuffers;
//extern std::vector<SoundInMem> g_Sounds;
extern std::vector<SoundNode*> g_SoundNodes;

void InitSounds();
bool LoadSounds();
void SoundNodesUpdate();

void RegisterSoundJSFunctions();

#endif // SOUND_H
