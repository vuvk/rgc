#pragma once
#ifndef UTIL_H
#define UTIL_H

#include <string>
#include "WorldSim3D.h"

#define SQR(N) ((N)*(N))
#define sqr(N) SQR(N)

uint32_t ARGB1555toARGB8888(uint16_t c);
uint16_t ARGB8888toARGB1555(uint32_t c);

wTexture* LoadTextureFromMemory(const std::string name, wVector2i size, int sizeForRead, void* buffer);
wTexture* LoadTexture(const std::string fileName, const std::string textureName);

/** вернуть полный путь из строки адреса */
std::string ExtractPath(const std::string path);
/** Вернуть имя файла из пути */
std::string ExtractFileName(const std::string path);

#endif // UTIL_H
