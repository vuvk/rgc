#ifndef MAPS_H
#define MAPS_H

#include <string>
#include <cstdint>
#include <array>
#include <vector>

#include "constants.h"

using namespace std;

#define TMapElement uint16_t

/** карта */
struct TMap
{
    string  name = "";          // имя карты
    int     floorColor = 0;     // цвет пола
    int     ceilColor = 0;      // цвет потолка
    int     fogColor = 0;       // цвет тумана
    float   fogIntensity = 0;   // интенсивность тумана
    bool    showFloor = false;  // показывать текстурный пол?
    bool    showCeil = false;   // показывать текстурный потолок?
    bool    showFog = false;    // показывать туман?
    bool    showSky = false;    // показывать небо?
    short   skyNumber = -1;     // номер скайбокса
    //TMapElement ceil [MAP_WIDTH][MAP_HEIGHT];    // массив с индексами текстур потолка
    //TMapElement level[MAP_WIDTH][MAP_HEIGHT];    // массив с содержимым уровня (стены, спрайты, двери и т.д.)
    //TMapElement floor[MAP_WIDTH][MAP_HEIGHT];    // массив с индексами текстур пола
    array<array<TMapElement, MAP_WIDTH>, MAP_HEIGHT> ceil;
    array<array<TMapElement, MAP_WIDTH>, MAP_HEIGHT> level;
    array<array<TMapElement, MAP_WIDTH>, MAP_HEIGHT> floor;
};

/** пак карт */
struct TMapPack
{
    vector<TMap> maps;
};

extern TMapPack mapPack;
extern int g_NumOfCurMap;

/* работа с паком карт */
/** инициализация пака карт*/
void InitMapPack();
/** загрузка пака карт*/
bool LoadMapPack();
/** очистить текущийц уровень*/
void ClearLevel();
/** сгенерировать уровень №__ */
void GenerateLevel(int numOfMap);

void RegisterMapJSFunctions();

#endif // MAPS_H
