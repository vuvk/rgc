#ifndef WEAPON_ITEM_H
#define WEAPON_ITEM_H

#include <vector>

#include "WorldSim3D.h"
#include "res_in_mem.h"
#include "object3d_billboard.h"


/** оружие подбирабельное - эталон, загруженное из пака */
class TWeaponItemInMem : public ScrResourceInMem
{
public:
    int weaponNumber = 0;       // позиция в руках
    std::string pickUpSound;    // имя звука подбора
    int      ammoTypeId = -1;
    bool     infiniteAmmo = false;
    uint32_t curAmmo = 0,
             maxAmmo = 0;

    TWeaponItemInMem() : ScrResourceInMem()
    {
        type = rtWeaponItem;
    }
};

/** нода подбирабельного оружия, создаваемого на карте */
class TWeaponItemNode : public TBillboard
{
public:
    static int count;

    // создание ноды
    TWeaponItemNode (TWeaponItemInMem* index);

    virtual void init(string className = "", string objectName = "", bool numerate = true)
    {
        TObject::init("WeaponItem", "weaponItem");
    }
};

extern vector<TWeaponItemInMem> g_WeaponItems;

void InitWeaponItems();
//void LoadWeaponItemFrames(TWeaponItemInMem& weaponItem);
bool LoadWeaponItemsPack();

#endif // WEAPON_ITEM_H
