#ifndef DOORS_H
#define DOORS_H

#include <vector>

#include "WorldSim3D.h"
#include "object3d.h"
#include "res_in_mem.h"


/** дверь-эталон, загруженная из пака */
class TDoorInMem : public ScrResourceInMem
{
public:
    double  openSpeed = 1.0;     // скорость открывания
    bool    stayOpened = false;  // открывается один раз?
    bool    needKey = false;     // нужен ключ для открывания?
    string  needKeyMsg;          // сообщение о необходимости ключа
    string  keyName;             // имя ключа для открывания
    std::string startSound;      // имя звука начала движения двери
    std::string moveSound;       // имя звука движения двери
    std::string stopSound;       // имя звука останова двери
    double  width = 1.0;         // ширина двери
    wTexture* rib = nullptr;     // текстура ребра

    TDoorInMem() : ScrResourceInMem()
    {
        type = rtDoor;
    }

    void clear()
    {
        ScrResourceInMem::clear();

        if (rib != nullptr)
            wTextureDestroy(rib);
        rib = nullptr;
    }
};

/** нода двери, создаваемая на карте */
class TDoorNode : public TObject3D
{
public:
    static int count;              // кол-во представителей класса

    // создание ноды
    TDoorNode(TDoorInMem* index = nullptr, bool isHorizontal = false, bool isVertical = false);

    virtual void init(string className = "", string objectName = "", bool numerate = true)
    {
        TObject::init("Door", "door", true);
    }
};

extern vector<TDoorInMem> g_Doors;

void InitDoors();
bool LoadDoorsPack();

/* СОЗДАНИЕ МЕША ДВЕРИ */
wMesh* CreateDoorMesh(const string name, wTexture* texture, wTexture* ribTexture, double width, bool isHorizontal, bool isVertical);

#endif // DOORS_H
