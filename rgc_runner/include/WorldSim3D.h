#ifndef __WORLDSIM3D_H_INCLUDED
#define __WORLDSIM3D_H_INCLUDED

#include "WorldSim3D_types.h"

#define __WORLDSIM3D_RUNTIME_LOAD
#ifndef __WORLDSIM3D_RUNTIME_LOAD
	#include "WorldSim3D_link.h"
#else 
	#include "WorldSim3D_runtime.h"
	
	void wLibraryLoad();
	void wLibraryUnload();
#endif  // __WORLDSIM3D_RUNTIME_LOAD

#endif // __WORLDSIM3D_H_INCLUDED
