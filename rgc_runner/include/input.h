#ifndef INPUT_H
#define INPUT_H

/** KEYBOARD */
void RegisterKeyboardJSFunctions();

/** MOUSE */
void RegisterMouseJSFunctions();

#endif // INPUT_H
