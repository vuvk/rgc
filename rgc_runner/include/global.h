#pragma once
#ifndef GLOBAL_H
#define GLOBAL_H

#include <vector>
#include <array>

#include "WorldSim3D.h"
#include "SampleFunctions.h"

#include "file_system.h"
#include "constants.h"
//#include "map.h"

using namespace std;

extern int g_TexturesCount;
extern int g_SpritesCount;
extern int g_DoorsCount;
extern int g_KeysCount;
extern int g_WeaponsCount;
extern int g_BulletsCount;
extern int g_AmmoCount;
extern int g_MapsCount;
extern int64_t g_ScriptsCount;

extern bool g_PreviewMode;

extern wVector2u g_Resolution;
extern wDriverTypes g_Renderer;    /** рендерер, с которым будет запущен раннер */
extern bool   g_FullScreenMode;    /** режим полного экрана */
extern bool   g_LimitFps;          /** установить лимит кадров в MAX_FPS */
extern bool   g_VSync;             /** вертикальная синхронизация */
extern bool   g_ShowFog;           /** отображать ли туман */
extern double g_DeltaTime;         /** время между кадрами реальное */
extern double g_DeltaTimeScript;   /** время между кадрами для скриптов */
extern double g_TimeForRunScript;  /** задержка перед запуском скриптов */
extern double g_TimeForGC;         /** задержка перед запуском сборки мусора */

extern wTexture* g_InvisibleTexture;
extern wMesh*    g_InvisibleMesh;
//extern wNode*    invisibleNode;

extern wVector3f g_IntersectNormal; /** нормаль с плоскостью пересечения лучом */
extern wVector3f g_IntersectPoint;  /** точка пересечения с лучом */

extern wSelector* g_WorldCollider;  /** коллайдер статичного мира - ВСЕ твёрдые объекты (включая стены) */
extern wNode*     g_SolidObjects;   /** пустая нода, к которой припарентены все твердые внутриигровые объекты (только те, которыми можно управлять) */

/* флаги над теми номерами текстур, которые нужно грузить */
extern vector<bool> g_TexturesNeedForLoad;
extern vector<bool> g_SpritesNeedForLoad;
extern vector<bool> g_DoorsNeedForLoad;
extern vector<bool> g_KeysNeedForLoad;
extern vector<bool> g_WeaponItemsNeedForLoad;
extern vector<bool> g_AmmoNeedForLoad;

/** список типов патронов */
extern vector<std::string> g_AmmoTypes;

/* мьютекс для обновления идентификатора объекта, обрабатываемого в данный момент */
//extern Mutex g_MutexNodes;

/* участок для компиляции JS-кода */
extern uint32_t* g_ByteCode;

/** путь запуска */
extern std::string g_RunPath;

// инициализация запускатора - выставление глобальных настроек
void InitRunner();
void StopRunner();
// настройки материала для олдскульности
void MaterialSetOldSchool(wMaterial* material);

// пробегается по карте и заполняет g_TexturesNeedForLoad
void PrepareTexturesNeedForLoad(int numOfMap);
void PrepareSpritesNeedForLoad(int numOfMap);
void PrepareDoorsNeedForLoad(int numOfMap);
void PrepareKeysNeedForLoad(int numOfMap);
void PrepareWeaponsNeedForLoad(int numOfMap);
void PrepareAmmoNeedForLoad(int numOfMap);

wMesh* CreateInvisibleBlock(string name = "", float width = 1.0, float height = 1.0, float offsetX = 0.0, float offsetY = 0.0);

#endif // GLOBAL_H
