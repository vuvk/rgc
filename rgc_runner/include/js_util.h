#pragma once
#ifndef JS_UTIL_H
#define JS_UTIL_H

#include <string>
#include <vector>

#include "jerryscript-core.h"
#include "jerryscript-port.h"
#include "jerryscript-snapshot.h"
#include "handler.h"

#include "WorldSim3D.h"
#include "SampleFunctions.h"

#include "global.h"
//#include "input"
#include "constants.h"

#include "file_system.h"
#include "util.h"

/*
#include "map"
#include "player"
#include "base_object"
#include "weapons_ammo"
*/

#define JS_FUNCTION(FUNCTION_NAME)                                                      \
    static jerry_value_t FUNCTION_NAME                                                  \
                     (const jerry_value_t  func_obj_val, /**< function object */        \
                      const jerry_value_t  this_p,       /**< this arg */               \
                      const jerry_value_t* args_p,       /**< function arguments */     \
                      const jerry_length_t args_cnt)     /**< number of function arguments */


struct TScriptJS
{
    int64_t   id = -1;
    string    fileName = "";
    string    text = "";
    uint32_t* bytecode = nullptr;
    size_t    snapshot_size = 0;

    ~TScriptJS();
    void setId();
    void compile();
    void loadFromFile(const std::string fileName);
    void reload();
    void clear();
    void run();
    void printErrorLine(int lineNum);
};


void JS_InitVM();
void JS_StopVM();
void JS_GC();

/** зарегистрировать нативную функцию */
void JS_RegisterCFunction(jerry_external_handler_t native_function, const char* new_name);
/** зарегистрировать метод объекта */
void JS_RegisterCFunctionForObject(jerry_value_t object, string objectName, jerry_external_handler_t native_function, string new_name);
/** вывести jerry_value_t в зависимости от типа */
void JS_PrintValue(jerry_value_t value);
/** напечатать ошибку. Возвращает строку, содержащую ошибку */
int JS_PrintError(jerry_value_t value);
/** получить строку из интерпретатора */
string JS_GetStringValue(jerry_value_t value);
/** протолкнуть массив чисел в интерпретатор */
jerry_value_t JS_PushDoubleArray(vector<double> values);

#endif // JS_UTIL_H
