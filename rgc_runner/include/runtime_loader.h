#ifndef __RUNTIME_LOADER_H_INCLUDED
#define __RUNTIME_LOADER_H_INCLUDED

#if defined(_WIN32) || defined(WIN32) || defined(_WIN64) || defined(WIN64)
	#include <windows.h>
#else
	#include <dlfcn.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

static void* load_library(const char* lib_name)
{
#if defined(_WIN32) || defined(WIN32) || defined(_WIN64) || defined(WIN64)
	return LoadLibrary(lib_name);
#else
	return dlopen(lib_name, RTLD_LAZY);
#endif
}

static void* load_function(void* lib, const char* func_name)
{
#if defined(_WIN32) || defined(WIN32) || defined(_WIN64) || defined(WIN64)
	return (void*)GetProcAddress((HMODULE)lib, func_name);
#else
	return dlsym(lib, func_name);
#endif
}

static int unload_library(void* lib)
{
#if defined(_WIN32) || defined(WIN32) || defined(_WIN64) || defined(WIN64)
	return FreeLibrary((HMODULE)lib);
#else
	return dlclose(lib);
#endif	
}


#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __RUNTIME_LOADER_H_INCLUDED
