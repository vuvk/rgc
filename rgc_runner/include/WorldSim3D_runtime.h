#ifndef __WORLDSIM3D_RUNTIME_H_INCLUDED
#define __WORLDSIM3D_RUNTIME_H_INCLUDED

#include "WorldSim3D_types.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
///wConsole///
extern void (*wConsoleSetFontColor)(wConsoleFontColor c);

extern void (*wConsoleSetBackColor)(wConsoleBackColor c);

extern Int32 (*wConsoleSaveDefaultColors)();

extern void (*wConsoleResetColors)(Int32 defValues);


///wTexture//
extern wTexture* (*wTextureLoad)(const char* cptrFile );

extern wTexture* (*wTextureCreateRenderTarget)(wVector2i size);

extern wTexture* (*wTextureCreate)(const char* name,
								   wVector2i size,
								   wColorFormat format );

extern void (*wTextureDestroy)(wTexture* texture );

extern UInt32* (*wTextureLock)(wTexture* texture );

extern void (*wTextureUnlock)(wTexture* texture );

extern void (*wTextureSave)(wTexture* texture,
							const char* file);

extern wImage* (*wTextureConvertToImage)(wTexture* texture);

extern void (*wTextureGetInformation)(wTexture* texture,
									  wVector2u* size ,
									  UInt32* pitch,
									  wColorFormat* format );

extern void (*wTextureMakeNormalMap)(wTexture* texture,
									 Float32 amplitude );

extern Int32 (*wTexturesSetBlendMode)(wTexture* texturedest,
									  wTexture* texturesrc,
									  wVector2i offset,
									  wBlendOperation operation );

extern void (*wTextureSetColorKey)(wTexture*texture,
								   wColor4s key);

extern void (*wTextureSetGray)(wTexture** texture);

extern void (*wTextureSetAlpha)(wTexture** texture,
								UInt32 value);

extern void (*wTextureSetInverse)(wTexture** texture);

extern void (*wTextureSetBrightness)(wTexture** texture,
									 UInt32 value);

extern wTexture* (*wTextureCopy)(wTexture* texture,
								 const char* name);

extern void (*wTextureSetContrast)(wTexture** texture,
								   Float32 value);

extern wTexture* (*wTextureFlip)(wTexture** texture,
								 Int32 mode);

extern void (*wTextureSetBlur)(wTexture** texture,
							   Float32 radius);

extern const char* (*wTextureGetFullName)(wTexture* texture );

extern const char* (*wTextureGetInternalName)(wTexture* texture );

extern void (*wTextureDraw)(wTexture* texture,
							wVector2i pos,
							bool useAlphaChannel,
							wColor4s color);

extern void (*wTextureDrawEx)(wTexture* texture,
							  wVector2i pos,
							  wVector2f scale,
							  bool useAlphaChannel);

extern void (*wTextureDrawMouseCursor)(wTexture* texture);

extern void (*wTextureDrawElement)(wTexture* texture,
								   wVector2i pos,
								   wVector2i fromPos,
								   wVector2i toPos,
								   bool useAlphaChannel,
								   wColor4s color);

extern void (*wTextureDrawElementStretch)(wTexture* texture,
										  wVector2i destFromPos,
										  wVector2i destToPos,
										  wVector2i sourceFromPos,
										  wVector2i sourceToPos,
										  bool useAlphaChannel);

extern void (*wTextureDrawAdvanced)(wTexture* texture,
									wVector2i pos,
									wVector2i rotPoint,
									Float32 rotation,
									wVector2f scale,
									bool useAlphaChannel,
									wColor4s color,
									wAntiAliasingMode aliasMode,
									bool bFilter,
									bool tFilter,
									bool aFilter);

extern void (*wTextureDrawElementAdvanced)(wTexture* texture,
										   wVector2i pos,
										   wVector2i fromPos,
										   wVector2i toPos,
										   wVector2i rotPoint,
										   Float32 rotAngleDeg,
										   wVector2f scale,
										   bool useAlphaChannel,
										   wColor4s color,
										   wAntiAliasingMode aliasMode,
										   bool bilinearFilter,
										   bool trilinearFilter,
										   bool anisotropFilter);

///w2d///
extern void  (*w2dDrawRect)(wVector2i minPos,
							wVector2i maxPos,
							wColor4s color);

extern void (*w2dDrawRectWithGradient)(wVector2i minPos,
									   wVector2i maxPos,
									   wColor4s colorLeftUp,
									   wColor4s colorRightUp,
									   wColor4s colorLeftDown,
									   wColor4s colorRightDown);

extern void (*w2dDrawRectOutline)(wVector2i minPos,
								  wVector2i maxPos,
								  wColor4s color);

extern void (*w2dDrawLine)(wVector2i fromPos,
						   wVector2i toPos,
						   wColor4s color);

extern void (*w2dDrawPixel)(wVector2i pos,
							wColor4s color);

extern void  (*w2dDrawPolygon)(wVector2i pos,
							   Float32 Radius,
							   wColor4s color,
							   Int32 vertexCount);

///w3d///
extern void (*w3dDrawLine)(wVector3f start,
						   wVector3f end,
						   wColor4s color);

extern void (*w3dDrawBox)(wVector3f minPoint,
						  wVector3f maxPoint,
						  wColor4s color);

extern void (*w3dDrawTriangle)(wTriangle triangle,
							   wColor4s color);

///wFont///
extern wFont* (*wFontLoad)(const char* fontPath );

extern wFont* (*wFontAddToFont)(const char* fontPath,
								wFont* destFont );

extern wFont* (*wFontGetDefault)();

extern void (*wFontDraw)(wFont* font,
						 const wchar_t* wcptrText,
						 wVector2i fromPos,
						 wVector2i toPos,
						 wColor4s color);

extern void (*wFontDestroy)(wFont* font);

extern wVector2u (*wFontGetTextSize)(wFont* font,
									 const wchar_t* text);

extern void (*wFontSetKerningSize)(wFont* font,
								   wVector2u kerning);

extern wVector2u (*wFontGetKerningSize)(wFont* font);

extern Int32 (*wFontGetCharacterFromPos)(wFont* font,
										 const wchar_t* text,
										 Int32 xPixel);

extern void (*wFontSetInvisibleCharacters)(wFont* font,
										   const wchar_t *s);
extern wFont* (*wFontLoadFromTTF)(char * fontPath,
								  UInt32 size,
								  bool antialias,
								  bool transparency);

extern void (*wFontDrawAsTTF)(wFont* font,
							  const wchar_t* wcptrText,
							  wVector2i fromPos,
							  wVector2i toPos,
							  wColor4s color,
							  bool hcenter,
							  bool vcenter);

///wImage//////
extern wImage* (*wImageLoad)(const char* cptrFile );

extern bool (*wImageSave)(wImage* img,
						  const char* file);

extern wImage* (*wImageCreate)(wVector2i size,
							   wColorFormat format );

extern void (*wImageDestroy)(wImage* image );

extern UInt32* (*wImageLock)(wImage* image );

extern void (*wImageUnlock)(wImage* image );

extern wTexture* (*wImageConvertToTexture)(wImage* img,
										   const char* name);

extern wColor4s (*wImageGetPixelColor)(wImage* img,
									   wVector2u pos);

extern void (*wImageSetPixelColor)(wImage* img,
								   wVector2u pos,
								   wColor4s color,
								   bool blend);

extern void (*wImageGetInformation)(wImage* image,
									wVector2u* size,
									UInt32* pitch,
									wColorFormat* format);

///wTimer///
extern Float32 (*wTimerGetDelta)();

extern UInt32 (*wTimerGetTime)();

extern wRealTimeDate (*wTimerGetRealTimeAndDate)();

//Returns current real time in milliseconds of the system
extern UInt32 (*wTimerGetRealTime)();

//set the current time in milliseconds//
extern void (*wTimerSetTime)(UInt32 newTime );

//Returns if the virtual timer is currently stopped
extern bool (*wTimerIsStopped)();

//Sets the speed of the timer
extern void (*wTimerSetSpeed)(Float32 speed);

//Starts the virtual time
extern void (*wTimerStart)();

//Stops the virtual timer
extern void (*wTimerStop)();

//Advances the virtual time
extern void (*wTimerTick)();

///wLog///
extern void (*wLogSetLevel)(wLoggingLevel level);

extern void (*wLogSetFile)(const char* path);

extern void (*wLogClear)(const char* path);

extern void (*wLogWrite)(const wchar_t* hint,
						 const wchar_t* text,
						 const char* path,
						 UInt32 mode);

///wSystem////
extern void (*wSystemSetClipboardText)(const char* text);

extern const char* (*wSystemGetClipboardText)();

extern UInt32 (*wSystemGetProcessorSpeed)();

extern UInt32  (*wSystemGetTotalMemory)();

extern UInt32  (*wSystemGetAvailableMemory)();

extern wVector2i (*wSystemGetMaxTextureSize)();

extern bool (*wSystemIsTextureFormatSupported)(wColorFormat format);

extern void (*wSystemSetTextureCreationFlag)(wTextureCreationFlag flag,
											 bool value );

extern bool (*wSystemIsTextureCreationFlag)(wTextureCreationFlag flag);

extern wTexture* (*wSystemCreateScreenShot)(wVector2u minPos,
											wVector2u maxPos);

extern bool (*wSystemSaveScreenShot)(const char* file );

///Get the current operation system version as string.
extern const char*	(*wSystemGetVersion)();

///Check if a driver type is supported by the engine.
///Even if true is returned the driver may not be available for
///a configuration requested when creating the device.
extern bool (*wSystemIsDriverSupported)(wDriverTypes testDriver);


///wDisplay///
///Get the graphics card vendor name.
extern const char* (*wDisplayGetVendor)();

extern Int32 (*wDisplayModesGetCount)();

extern Int32 (*wDisplayModeGetDepth)(Int32 modeNumber);

extern wVector2u (*wDisplayModeGetResolution)(Int32 ModeNumber);

extern wVector2u (*wDisplayGetCurrentResolution)();

extern Int32 (*wDisplayGetCurrentDepth)();

///Set the current Gamma Value for the Display.
extern void (*wDisplaySetGammaRamp)(wColor3f gamma,float brightness,float contrast);

extern void (*wDisplayGetGammaRamp)(wColor3f* gamma,float* brightness,float* contrast);

extern bool (*wDisplaySetDepth)(UInt32 depth);

///Возвращает нормализованный вектор
extern wVector3f (*wMathVector3fNormalize)(wVector3f source);

///Возвращает длину вектора
extern Float32 (*wMathVector3fGetLength)(wVector3f vector);

///Get the rotations that would make a (0,0,1) direction vector
///point in the same direction as this direction vector.
extern wVector3f (*wMathVector3fGetHorizontalAngle)(wVector3f vector);

///Возвращает инвертированный вектор (все координаты меняют знак)
extern wVector3f (*wMathVector3fInvert)(wVector3f vector);

///Суммирует два вектора
extern wVector3f (*wMathVector3fAdd)(wVector3f vector1,
									 wVector3f vector2);

///Вычитает из вектора 1 вектор 2
extern wVector3f (*wMathVector3fSubstract)(wVector3f vector1,
										   wVector3f vector2);

///Векторное произведение векторов
extern wVector3f (*wMathVector3fCrossProduct)(wVector3f vector1,
											  wVector3f vector2);
///Скалярное произведение векторов
extern Float32 (*wMathVector3fDotProduct)(wVector3f vector1,
										  wVector3f vector2);

///Определяет кратчайшее расстояние между векторами
extern Float32 (*wMathVector3fGetDistanceFrom)(wVector3f vector1,
											   wVector3f vector2);

///Возвращает интерполированный вектор
extern wVector3f (*wMathVector3fInterpolate)(wVector3f vector1,
											 wVector3f vector2,
											 Float64 d);

///Возвращает случайное число из интервала (first, last)
extern Float64 (*wMathRandomRange)(Float64 first,
								   Float64 last);

///Из градусов- в радианы///
extern Float32 (*wMathDegToRad)(Float32 degrees);

///Из радиан- в градусы
extern Float32 (*wMathRadToDeg)(Float32 radians);

///Математически правильное округление///
extern Float32 (*wMathRound)(Float32 value);

///Округление в большую сторону///
extern Int32 (*wMathCeil)(Float32 value);

///Округление в меньшую сторону///
extern Int32 (*wMathFloor)(Float32 value);


///returns if a equals b, taking possible rounding errors into account
extern bool (*wMathFloatEquals)(Float32 value1,
								Float32 value2,
								Float32 tolerance);

///returns if a equals b, taking an explicit rounding tolerance into account
extern bool (*wMathIntEquals)(Int32 value1,
							  Int32 value2,
							  Int32 tolerance);

///returns if a equals b, taking an explicit rounding tolerance into account
extern bool (*wMathUIntEquals)(UInt32 value1,
							   UInt32 value2,
							   UInt32 tolerance);

///returns if a equals zero, taking rounding errors into account
extern bool (*wMathFloatIsZero)(Float32 value,
								Float32 tolerance);

///returns if a equals zero, taking rounding errors into account
extern bool (*wMathIntIsZero)(Int32 value,
							  Int32 tolerance);

///returns if a equals zero, taking rounding errors into account
extern bool (*wMathUIntIsZero)(UInt32 value,
							   UInt32 tolerance);

///Возвращает больший Float32 из двух///
extern Float32 (*wMathFloatMax2)(Float32 value1,
								 Float32 value2);

///Возвращает больший Float32 из трех///
extern Float32 (*wMathFloatMax3)(Float32 value1,
								 Float32 value2,
								 Float32 value3);

///Возвращает больший Int32 из двух///
extern Float32 (*wMathIntMax2)(Int32 value1,
							   Int32 value2);

///Возвращает больший Int32 из трех///
extern Float32 (*wMathIntMax3)(Int32 value1,
							   Int32 value2,
							   Int32 value3);

///Возвращает меньший Float32 из двух///
extern Float32 (*wMathFloatMin2)(Float32 value1,
								 Float32 value2);

///Возвращает меньший Float32 из трех///
extern Float32 (*wMathFloatMin3)(Float32 value1,
								 Float32 value2,
								 Float32 value3);

///Возвращает меньший Int32 из двух///
extern Float32 (*wMathIntMin2)(Int32 value1,
							   Int32 value2);

///Возвращает меньший Int32 из трех///
extern Float32 (*wMathIntMin3)(Int32 value1,
							   Int32 value2,
							   Int32 value3);

///wUtil///
extern const char* (*wUtilVector3fToStr)(wVector3f vector,
										 const char* s,
										 bool addNullChar);

extern const char* (*wUtilVector2fToStr)(wVector2f vector,
										 const char* s,
										 bool addNullChar);

extern const char* (*wUtilColor4sToStr)(wColor4s color,
										const char* s,
										bool addNullChar);

extern const char* (*wUtilColor4fToStr)(wColor4f color,
										const char* s,
										bool addNullChar);

extern UInt32 (*wUtilColor4sToUInt)(wColor4s color);

extern UInt32 (*wUtilColor4fToUInt)(wColor4f color);

extern wColor4s (*wUtilUIntToColor4s)(UInt32 color);

extern wColor4f (*wUtilUIntToColor4f)(UInt32 color);

///Convert a simple string of base 10 digits into a signed 32 bit integer.
extern Int32 (*wUtilStrToInt)(const char* str);

extern const char* (*wUtilIntToStr)(Int32 value,
									bool addNullChar);

///Converts a sequence of digits into a whole positive floating point value.
///Only digits 0 to 9 are parsed.
///Parsing stops at any other character, including sign characters or a decimal point.
extern Float32 (*wUtilStrToFloat)(const char* str);

extern const char* (*wUtilFloatToStr)(Float32 value,
									  bool addNullChar);

///Convert a simple string of base 10 digits into an unsigned 32 bit integer.
extern UInt32 (*wUtilStrToUInt)(const char* str);

extern const char* (*wUtilUIntToStr)(UInt32 value,
									 bool addNullChar);

///swaps the content of the passed parameters
extern void (*wUtilSwapInt)(int* value1,
							int* value2);

///swaps the content of the passed parameters
extern void (*wUtilSwapUInt)(UInt32* value1,
							 UInt32* value2);

///swaps the content of the passed parameters
extern void (*wUtilSwapFloat)(float* value1,
							  float* value2);

///Конвертирует расширенную строку в С-строку
extern const char* (*wUtilWideStrToStr)(const wchar_t* str);

///Конвертирует С-строку в расширенную строку
extern const wchar_t* (*wUtilStrToWideStr)(const char* str);

///Добавляет символ конца строки
extern void (*wUtilStrAddNullChar)(const char** str);

///Добавляет символ конца строки к расширенной строке
extern void (*wUtilWideStrAddNullChar)(const wchar_t** str);

///wEngine///
extern void (*wEngineSetClipboardText)(const wchar_t* text);

extern const wchar_t* (*wEngineGetClipboardText)();

extern bool (*wEngineLoadCreationParameters)(wEngineCreationParameters* outParams,
											 const char* xmlFilePath);

extern bool (*wEngineSaveCreationParameters)(wEngineCreationParameters* inParams,
											 const char* xmlFilePath);

extern bool (*wEngineStartWithGui)(const wchar_t* captionText,
								   const char* fontPath,
								   const char* logo,
								   wLanguage lang,
								   wEngineCreationParameters* outParams,
								   const char* inXmlConfig);

extern bool (*wEngineStart)(wDriverTypes iDevice,
							wVector2u size,
							UInt32 iBPP,
							bool boFullscreen,
							bool boShadows,
							bool boCaptureEvents,
							bool vsync);

extern void (*wEngineCloseByEsc)();

extern bool (*wEngineStartAdvanced)(wEngineCreationParameters params);

extern void (*wEngineSetTransparentZWrite) (bool value);

extern bool (*wEngineRunning)();

extern void (*wEngineSleep)(UInt32 Ms,
							bool pauseTimer);

///Cause the device to temporarily pause execution and let other processes run.
extern void (*wEngineYield)();

extern void (*wEngineSetViewPort)(wVector2i fromPos,
								  wVector2i toPos);

extern bool (*wEngineIsQueryFeature)(wVideoFeatureQuery  feature);

extern void (*wEngineDisableFeature)(wVideoFeatureQuery feature,
									 bool flag);

extern bool (*wEngineStop)(bool closeDevice);

extern void (*wEngineSetFPS)(UInt32 limit);

extern wMaterial* (*wEngineGetGlobalMaterial)();

/*
Get the 2d override material for altering its values.
The 2d override materual allows to alter certain render states of the 2d methods.
Not all members of SMaterial are honored, especially not MaterialType and Textures.
Moreover, the zbuffer is always ignored, and lighting is always off.
All other flags can be changed, though some might have to effect in most cases.
Please note that you have to enable/disable this effect with enableInitMaterial2D().
This effect is costly, as it increases the number of state changes considerably.
Always reset the values when done.
*/
extern wMaterial* (*wEngineGet2dMaterial)();

extern void (*wEngineSet2dMaterial)(bool value);

extern Int32 (*wEngineGetFPS)();

extern void (*wEngineShowLogo)(bool value);

///wScene/////
extern bool (*wSceneBegin) (wColor4s color);

extern bool (*wSceneBeginAdvanced)(wColor4s backColor,
								   bool clearBackBuffer,
								   bool clearZBuffer);

extern void (*wSceneLoad)(const char* filename );

extern void (*wSceneSave)(const char* filename );

extern void (*wSceneDrawAll)();

extern bool (*wSceneEnd)();

extern void (*wSceneDrawToTexture) (wTexture* renderTarget );

extern void (*wSceneSetRenderTarget)(wTexture*renderTarget,
									 wColor4s backColor,
									 bool clearBackBuffer,
									 bool clearZBuffer);

extern void (*wSceneSetAmbientLight)(wColor4f color);

extern wColor4f (*wSceneGetAmbientLight)();

extern void (*wSceneSetShadowColor)(wColor4s color);

extern wColor4s (*wSceneGetShadowColor)();

extern void (*wSceneSetFog)(wColor4s color,
							wFogType fogtype,
							Float32 start,
							Float32 end,
							Float32 density,
							bool pixelFog,
							bool rangeFog);

extern void (*wSceneGetFog)(wColor4s* color,
							wFogType* fogtype,
							float* start,
							float* end,
							float* density,
							bool*  pixelFog,
							bool*  rangeFog);

extern wNode* (*wSceneGetActiveCamera)();

///Поиск текстуры по АБСОЛЮТНОМУ пути
///Если требуется искать по относительному пути,
///используйте сначала wFileGetAbsolutePath
extern wTexture* (*wSceneGetTextureByName)(const char* name);

// When animating a mesh by "Morphing" or "Skeletal Animation" such as "*.md3", "*.x" and "*.b3d" using "Shaders" for rendering we can improve the final render if we "Cyclically Update" the "Tangents" and "Binormals"..
// We presume that our meshes are, among others, textured with a "NORMAL MAP" used by the "Shader" (cg, hlsl, or glsl etc) in calculating diffuse and specular.
// We also have one or more lights used by the shader.

// Update TANGENTS & BINORMALS at every frame for a skinned animation..

// We dont want to do this for extern meshes like levels etc..
// We also dont want to do it for Rotating, Scaled and translated meshes..(we can however, as a bonus, scale, rotate and translate these)
// Only for animated skinned and morph based meshes..
// This is loose code that works. If anyone can improve it for the engine itself that would be great..
// You'll probably ID possible improvements immediately!

// At every N'th Frame we loop through all the vertices..
// 1. In the loop we Access the VERTEX of POINT A of the "INDEXED TRIANGLE"..
// 2. We interrogate the "OTHER TWO" VERTICES (which thankfully do change at each frame) for their Positions, Normals, and UV Coords to
//    Genertate a "BRAND NEW" (animated) TANGENT and BINORMAL. (We may want to calculate the the "Binormal" in the SHADER to save time)
// 3. We REWRITE the Tangent and Binormal for our SELECTED TRIANGLE POINT.
// 4. We DO THE SAME for POINTS B and C..
//

//  GENERATE "LIVING" TANGENTS & BINBORMALS
//  REMEMBER!
//  WE NEED "LOOP THROUGH ALL ITS BUFFERS"
//  WE NEED "LOOP THROUGH ALL THOSE BUFFER VERTICES"
// Possible types of (animated) meshes.
// Enumerator:
// 1  EAMT_MD2            Quake 2 MD2 model file..
// 2  EAMT_MD3            Quake 3 MD3 model file..
// 10 EAMT_MDL_HALFLIFE   Halflife MDL model file..
// Below is what an item type must be for it to qualify for Tangent Updates..
// 11 EAMT_SKINNED        generic skinned mesh "*.x" "*.b3d" etc.. (see Morphed too!)
//
// We want to change tangents for skinned meshes only so we must determine which ones are "Skinned"..
// This may change if we add and remove meshes during runtime..

extern void (*wMeshUpdateTangentsAndBinormals)(wMesh* mesh);

extern void (*wSceneDestroyAllTextures)();

extern void (*wSceneDestroyAllNodes)();

///Можно для поиска меша использовать относительный путь
extern wMesh* (*wSceneGetMeshByName)(const char* name);

extern wMesh* (*wSceneGetMeshByIndex)(unsigned int index);

extern UInt32 (*wSceneGetMeshesCount)();

extern void (*wSceneDestroyAllMeshes)();

extern bool (*wSceneIsMeshLoaded)(const char* filePath);

extern void (*wSceneDestroyAllUnusedMeshes)();

extern UInt32 (*wSceneGetPrimitivesDrawn)();

extern UInt32 (*wSceneGetNodesCount)();

extern wNode* (*wSceneGetNodeById)(Int32 id );

extern wNode* (*wSceneGetNodeByName)(const char* name );

extern wNode* (*wSceneGetRootNode)();

///wWindow///
extern void (*wWindowSetCaption)(const wchar_t* wcptrText);

extern void (*wWindowGetSize)(wVector2u* size);

extern bool (*wWindowIsFullscreen)();

extern bool (*wWindowIsResizable)();

extern bool (*wWindowIsActive)();

extern bool (*wWindowIsFocused)();

extern bool (*wWindowIsMinimized)();

extern void (*wWindowMaximize)();

extern void (*wWindowMinimize)();

extern void (*wWindowRestore)();

extern void (*wWindowSetResizable)(bool resizable);

extern void (*wWindowMove)(wVector2u pos);

extern void (*wWindowPlaceToCenter)();

extern void (*wWindowResize)(wVector2u newSize);

extern void (*wWindowSetFullscreen)(bool value);

extern bool (*wWindowSetDepth)(UInt32 depth);
///wPostEffect///
extern wPostEffect* (*wPostEffectCreate)(wPostEffectId effectnum,
										 wPostEffectQuality quality,
										 Float32 value1,
										 Float32 value2,
										 Float32 value3,
										 Float32 value4,
										 Float32 value5,
										 Float32 value6,
										 Float32 value7,
										 Float32 value8);

extern void (*wPostEffectDestroy)(wPostEffect* ppEffect);

extern void (*wPostEffectSetParameters)(wPostEffect* ppEffect,
										Float32 para1,
										Float32 para2,
										Float32 para3,
										Float32 para4,
										Float32 para5,
										Float32 para6,
										Float32 para7,
										Float32 para8);

extern void (*wPostEffectsDestroyAll)();

///wXEffects///
extern void (*wXEffectsStart)(bool vsm,
							  bool softShadows,
							  bool bitDepth32,
							  wColor4s color);


extern void (*wXEffectsEnableDepthPass)(bool enable);

extern void (*wXEffectsAddPostProcessingFromFile)(const char* name,
												  Int32 effectType);

extern void (*wXEffectsSetPostProcessingUserTexture)(wTexture* texture );

extern void (*wXEffectsAddShadowToNode)(wNode* node,
										wFilterType filterType,
										wShadowMode shadowType);

extern void (*wXEffectsRemoveShadowFromNode)(wNode* node );

extern void (*wXEffectsExcludeNodeFromLightingCalculations)(wNode* node );

extern void (*wXEffectsAddNodeToDepthPass)(wNode* node );

extern void (*wXEffectsSetAmbientColor)(wColor4s color);

extern void (*wXEffectsSetClearColor)(wColor4s color);

extern void (*wXEffectsAddShadowLight)(UInt32 shadowDimen,
									   wVector3f position,
									   wVector3f target,
									   wColor4f color,
									   Float32 lightNearDist ,
									   Float32 lightFarDist,
									   Float32 angleDeg);

extern UInt32 (*wXEffectsGetShadowLightsCount)();

extern wTexture* (*wXEffectsGetShadowMapTexture)(UInt32 resolution,
												 bool secondary);

extern wTexture* (*wXEffectsGetDepthMapTexture)();

extern void (*wXEffectsSetScreenRenderTargetResolution)(wVector2u size);

extern void (*wXEffectsSetShadowLightPosition)(UInt32 index,
											   wVector3f position);

extern wVector3f (*wXEffectsGetShadowLightPosition)(UInt32 index);

extern void (*wXEffectsSetShadowLightTarget)(UInt32 index,
											 wVector3f target);

extern wVector3f (*wXEffectsGetShadowLightTarget)(UInt32 index);

extern void (*wXEffectsSetShadowLightColor)(UInt32 index,
											wColor4f color);

extern wColor4f (*wXEffectsGetShadowLightColor)(UInt32 index);

extern void (*wXEffectsSetShadowLightMapResolution)(UInt32 index,
													UInt32 resolution);

extern UInt32 (*wXEffectsGetShadowLightMapResolution)(UInt32 index);

extern Float32 (*wXEffectsGetShadowLightFarValue)(UInt32 index);

///wAnimator///
extern wAnimator* (*wAnimatorFollowCameraCreate)(wNode* node,
												 wVector3f position);

extern wAnimator* (*wAnimatorCollisionResponseCreate)(wSelector* selector,
													  wNode* node,
													  Float32 slidingValue);

extern void (*wAnimatorCollisionResponseSetParameters)(wAnimator* anim,
													   wAnimatorCollisionResponse params);

extern void (*wAnimatorCollisionResponseGetParameters)(wAnimator* anim,
													   wAnimatorCollisionResponse* params);

extern wAnimator* (*wAnimatorDeletingCreate)(wNode* node,
											 Int32 delete_after );

extern wAnimator* (*wAnimatorFlyingCircleCreate)(wNode* node,
												 wVector3f pos,
												 Float32 radius,
												 Float32 speed,
												 wVector3f direction,
												 Float32 startPos,
												 Float32 radiusEllipsoid);

extern wAnimator *(*wAnimatorFlyingStraightCreate)(wNode* node,
												   wVector3f startPoint,
												   wVector3f endPoint,
												   UInt32 time,
												   bool loop );

extern wAnimator* (*wAnimatorRotationCreate)(wNode* node,
											 wVector3f pos);

extern wAnimator* (*wAnimatorSplineCreate)(wNode* node,
										   Int32 iPoints,
										   wVector3f *points,
										   Int32 time,
										   Float32 speed,
										   Float32 tightness);

extern wAnimator* (*wAnimatorFadingCreate)(wNode* node,
										   Int32 delete_after,
										   Float32 scale );

extern void (*wAnimatorDestroy)(wNode* node,
								wAnimator* anim );

///wTpsCamera///
extern wNode* (*wTpsCameraCreate)(const char* name);

extern void (*wTpsCameraDestroy)(wNode* ctrl);

extern void (*wTpsCameraUpdate)(wNode* ctrl);

extern void (*wTpsCameraSetTarget)(wNode* ctrl,
								   wNode* node);

extern void (*wTpsCameraRotateHorizontal)(wNode* ctrl,
										  Float32 rotVal);

extern void (*wTpsCameraRotateVertical)(wNode* ctrl,
										Float32 rotVal);

extern void (*wTpsCameraSetHorizontalRotation)(wNode* ctrl,
											   Float32 rotVal);

extern void (*wTpsCameraSetVerticalRotation)(wNode* ctrl,
											 Float32 rotVal);

extern void (*wTpsCameraZoomIn)(wNode* ctrl);

extern void (*wTpsCameraZoomOut)(wNode* ctrl);

extern wNode* (*wTpsCameraGetCamera)(wNode* ctrl);

extern void (*wTpsCameraSetCurrentDistance)(wNode* ctrl,
											Float32 dist);

extern void (*wTpsCameraSetRelativeTarget)(wNode* ctrl,
										   wVector3f target);

extern void (*wTpsCameraSetDefaultDistanceDirection)(wNode* ctrl,
													 wVector3f dir);

extern void (*wTpsCameraSetMaximalDistance)(wNode* ctrl,
											Float32 value);

extern void (*wTpsCameraSetMinimalDistance)(wNode* ctrl,
											Float32 value);

extern void (*wTpsCameraSetZoomStepSize)(wNode* ctrl,
										 Float32 value);

extern void (*wTpsCameraSetHorizontalSpeed)(wNode* ctrl,
											Float32 value);

extern void (*wTpsCameraSetVerticalSpeed)(wNode* ctrl,
										  Float32 value);
///wFpsCamera///
extern wNode*  (*wFpsCameraCreate)(Float32 rotateSpeed,
								   Float32 moveSpeed,
								   wKeyMap* keyMapArray,
								   Int32 keyMapSize,
								   bool noVerticalMovement,
								   Float32 jumpSpeed);

extern Float32 (*wFpsCameraGetSpeed)(wNode* camera);

extern void (*wFpsCameraSetSpeed)(wNode* camera,
								  Float32 newSpeed);

extern Float32 (*wFpsCameraGetRotationSpeed)(wNode* camera);

extern void (*wFpsCameraSetRotationSpeed)(wNode* camera,
										  Float32 rotSpeed);

extern void (*wFpsCameraSetKeyMap)(wNode* camera,
								   wKeyMap* map,
								   UInt32 count);

extern void (*wFpsCameraSetVerticalMovement)(wNode* camera,
											 bool value);

extern void (*wFpsCameraSetInvertMouse)(wNode* camera,
										bool value);

extern void (*wFpsCameraSetMaxVerticalAngle)(wNode* camera,
											 float newValue);

extern Float32 (*wFpsCameraGetMaxVerticalAngle)(wNode* camera);

///wCamera///
extern wNode*  (*wCameraCreate)(wVector3f pos,
								wVector3f target);

extern wNode* (*wMayaCameraCreate)(Float32 rotateSpeed,
								   Float32 zoomSpeed,
								   Float32 moveSpeed);

extern void (*wCameraSetTarget)(wNode* camera,
								wVector3f target);

extern wVector3f (*wCameraGetTarget)(wNode* camera);

extern wVector3f (*wCameraGetUpDirection)(wNode* camera);

extern void (*wCameraSetUpDirection)(wNode* camera,
									 wVector3f upDir);

extern void (*wCameraGetOrientation)(wNode* camera,
									 wVector3f* upDir,
									 wVector3f* forwardDir,
									 wVector3f* rightDir);

extern void (*wCameraSetClipDistance)(wNode* camera,
									  Float32 farDistance,
									  Float32 nearDistance);

extern void (*wCameraSetActive)(wNode* camera);

extern void (*wCameraSetFov)(wNode* camera,
							 Float32 fov );

extern Float32 (*wCameraGetFov)(wNode* camera);

extern void (*wCameraSetOrthogonal)(wNode* camera,
									wVector3f vec);

extern void (*wCameraRevolve)(wNode* camera,
							  wVector3f angleDeg,
							  wVector3f offset);

extern void (*wCameraSetUpAtRightAngle)(wNode* camera );

extern void (*wCameraSetAspectRatio)(wNode* camera,
									 Float32 aspectRatio );

extern void (*wCameraSetInputEnabled)(wNode* camera,
									  bool value);

extern bool (*wCameraIsInputEnabled)(wNode* camera);

extern void (*wCameraSetCollisionWithScene)(wNode* camera,
											wVector3f radius,
											wVector3f gravity,
											wVector3f offset,
											Float32 slidingValue);

///wRtsCamera///
extern wNode* (*wRtsCameraCreate)(wVector3f pos,
								  wVector2f offsetX,
								  wVector2f offsetZ,
								  wVector2f offsetDistance,
								  wVector2f offsetAngle,
								  Float32 driftSpeed,
								  Float32 scrollSpeed,
								  Float32 mouseSpeed,
								  Float32 orbit,
								  wMouseButtons mouseButtonActive);


///wCollision///
extern wSelector* (*wCollisionGroupCreate)();

extern void (*wCollisionGroupAddCollision)(wSelector* group,
										   wSelector* selector );

extern void (*wCollisionGroupRemoveAll)(wSelector* geoup );

extern void (*wCollisionGroupRemoveCollision)(wSelector* group,
											  wSelector* selector );

extern wSelector* (*wCollisionCreateFromMesh)(wMesh* mesh,
											  wNode* node,
											  Int32 iframe );

extern wSelector* (*wCollisionCreateFromexternMesh)(wMesh* externMesh,
													wNode* node);

extern wSelector* (*wCollisionCreateFromBatchingMesh)(wMesh* mesh,
													  wNode* node);

extern wSelector* (*wCollisionCreateFromMeshBuffer)(wMeshBuffer* meshbuffer,
													wNode* node);

extern wSelector* (*wCollisionCreateFromOctreeMesh)(wMesh* mesh,
													wNode* node,
													Int32 iframe);

extern wSelector* (*wCollisionCreateFromBox)(wNode*  node );

extern wSelector* (*wCollisionCreateFromTerrain)(wNode*  node,
												 Int32 level_of_detail);

extern wNode* (*wCollisionGetNodeFromCamera)(wNode* camera );

extern wNode* (*wCollisionGetNodeFromRay)(wVector3f* vectorStart,
										  wVector3f* vectorEnd );

extern wNode* (*wCollisionGetNodeChildFromRay)(wNode* node,
											   Int32 id,
											   bool recurse,
											   wVector3f* vectorStart,
											   wVector3f* vectorEnd);

extern wNode* (*wCollisionGetNodeAndPointFromRay)(wVector3f* vectorStart,
												  wVector3f* vectorEnd,
												  wVector3f* colPoint,
												  wVector3f* normal,
												  Int32 id,
												  wNode* rootNode );

extern wNode* (*wCollisionGetNodeFromScreen)(wVector2i screenPos,
											 Int32 idBitMask,
											 bool bNoDebugObjects,
											 wNode* root);

extern wVector2i (*wCollisionGetScreenCoordFrom3dPosition)(wVector3f pos);

extern void (*wCollisionGetRayFromScreenCoord)(wNode* camera,
											   wVector2i screenCoord,
											   wVector3f* vectorStart,
											   wVector3f* vectorEnd );

extern wVector3f (*wCollisionGet3dPositionFromScreen)(wNode* camera,
													  wVector2i screenPos,
													  wVector3f normal,
													  Float32 distanceFromOrigin);

extern wVector2f (*wCollisionGet2dPositionFromScreen)(wNode* camera,
													  wVector2i screenPos);

extern bool (*wCollisionGetPointFromRay)(wSelector* ts,
										 wVector3f* vectorStart,
										 wVector3f* vectorEnd,
										 wVector3f* collisionPoint,
										 wVector3f* vectorNormal,
										 wTriangle* collisionTriangle,
										 wNode** collNode);

extern wNode* (*wCollisionGetNodeChildFromPoint)(wNode* node,
												 Int32 id,
												 bool recurse,
												 wVector3f* vectorPoint );

extern void (*wCollisionGetResultPosition)(wSelector* selector,
										   wVector3f* ellipsoidPosition,
										   wVector3f* ellipsoidRadius,
										   wVector3f* velocity,
										   wVector3f* gravity,
										   Float32 slidingSpeed,
										   wVector3f* outPosition,
										   wVector3f* outHitPosition,
										   int* outFalling);

///wFile///
extern void (*wFileAddZipArchive)(const char* cptrFile,
								  bool boIgnoreCase,
								  bool boIgnorePaths);

extern void (*wFileAddArchive)(const char *cptrFile,
							   bool boIgnoreCase,
							   bool boIgnorePaths,
							   wFileArchiveType aType,
							   const char* password
);

extern void (*wFileSetWorkingDirectory)(const char* cptrPath );

extern const char* (*wFileGetWorkingDirectory)();

extern void (*wFileAddPakArchive)(const char* cptrFile,
								  bool boIgnoreCase,
								  bool boIgnorePaths );

extern void (*wFileAddDirectory)(const char* cptrFile,
								 bool boIgnoreCase,
								 bool boIgnorePaths );

extern bool (*wFileIsExist)(const char* cptrFile );

extern const char* (*wFileGetAbsolutePath)(const char* cptrPath);

extern const char* (*wFileGetRelativePath)(const char* cptrPath,
										   const char* directory);

///Get the base part of a filename, i.e. the name without the directory part.
///If no directory is prefixed, the full name is returned.
extern const char* (*wFileGetBaseName)(const char* cptrPath,
									   bool keepExtension);

extern const char* (*wFileGetDirectory)(const char*cptrPath);

///for read///
extern wFile* (*wFileOpenForRead)(const char* cptrFile );

extern Int32 (*wFileRead)(wFile* file,
						  void* buffer,
						  UInt32 sizeToRead);

extern Int64 (*wFileGetSize)(wFile* file);

///for write///
extern wFile* (*wFileCreateForWrite)(const char* cptrFile,
									 bool append );
extern Int32 (*wFileWrite)(wFile* file,
						   const void* buffer,
						   UInt32 sizeToWrite);

///for read/write///
extern const char* (*wFileGetName)(wFile* file);

extern Int64 (*wFileGetPos)(wFile* file);

extern bool (*wFileSeek)(wFile* file,
						 Int64 finalPos,
						 bool relativeMovement);

extern void (*wFileClose)(wFile* file);

///XMLReader///
extern wXmlReader* (*wXmlReaderCreate)(const char* cptrFile );

extern wXmlReader* (*wXMLReaderCreateUTF8)(const char* cptrFile );

//Returns attribute count of the current XML node
extern UInt32 (*wXmlGetAttributesCount)(wXmlReader* xml);

//Returns the value of an attribute
extern const wchar_t* (*wXmlGetAttributeNameByIdx)(wXmlReader* xml,
												   Int32 idx);

//Returns the value of an attribute
extern const wchar_t* (*wXmlGetAttributeValueByIdx)(wXmlReader* xml,
													Int32 idx);

//Returns the value of an attribute
extern const wchar_t* (*wXmlGetAttributeValueByName)(wXmlReader* xml,
													 const wchar_t* name);

//Returns the value of an attribute as float
extern Float32 (*wXmlGetAttributeValueFloatByIdx)(wXmlReader* xml,
												  Int32 idx);

//Returns the value of an attribute as float
extern Float32 (*wXmlGetAttributeValueFloatByName)(wXmlReader* xml,
												   const wchar_t* name);

//Returns the value of an attribute as integer
extern Int32 (*wXmlGetAttributeValueIntByIdx)(wXmlReader* xml,
											  Int32 idx);

//Returns the value of an attribute as integer
extern Int32 (*wXmlGetAttributeValueIntByName)(wXmlReader* xml,
											   const wchar_t* name);

//Returns the value of an attribute in a safe way
extern const wchar_t* (*wXmlGetAttributeValueSafeByName)(wXmlReader* xml,
														 const wchar_t* name);

//Returns the name of the current node
extern const wchar_t* (*wXmlGetNodeName)(wXmlReader* xml);

//Returns data of the current node
extern const wchar_t* (*wXmlGetNodeData)(wXmlReader* xml);

//Returns format of the source xml file
extern wTextFormat (*wXmlGetSourceFormat)(wXmlReader* xml);

//Returns format of the strings returned by the parser
extern wTextFormat (*wXmlGetParserFormat)(wXmlReader* xml);

//Returns the type of the current XML node
extern wXmlNodeType (*wXmlGetNodeType)(wXmlReader* xml);

//Returns if an element is an empty element, like <foo />
extern bool (*wXmlIsEmptyElement)(wXmlReader* xml);

//Reads forward to the next xml node
extern bool (*wXmlRead)(wXmlReader* xml);

extern void (*wXmlReaderDestroy)(wXmlReader* xml);

///XmlWriter///
extern wXmlWriter* (*wXmlWriterCreate)(const char* cptrFile );
//Writes the closing tag for an element. Like "</foo>"
extern void (*wXmlWriteClosingTag)(wXmlWriter* xml,
								   const wchar_t* name);

//Writes a comment into the xml file
extern void (*wXmlWriteComment)(wXmlWriter* xml,
								const wchar_t* comment);

//Writes a line break
extern void  (*wXmlWriteLineBreak)(wXmlWriter* xml);

//Writes a text into the file
extern void (*wXmlWriteText)(wXmlWriter* xml,
							 const wchar_t* file);

//Writes an xml 1.0 heade
extern void  (*wXmlWriteHeader)(wXmlWriter* xml);

extern void (*wXmlWriteElement)(wXmlWriter* xml,
								const wchar_t* name,
								bool empty,
								const wchar_t* attr1Name,const wchar_t* attr1Value,
								const wchar_t* attr2Name,const wchar_t* attr2Value,
								const wchar_t* attr3Name,const wchar_t* attr3Value,
								const wchar_t* attr4Name,const wchar_t* attr4Value,
								const wchar_t* attr5Name,const wchar_t* attr5Value);

extern void (*wXmlWriterDestroy)(wXmlWriter* xml);

///wInput///
///keyboard///
///Get character without waiting for Return to be pressed.
extern bool (*wInputWaitKey)();

extern bool (*wInputIsKeyEventAvailable)();

extern wKeyEvent* (*wInputReadKeyEvent)();

extern bool (*wInputIsKeyUp)(wKeyCode num);

extern bool (*wInputIsKeyHit)(wKeyCode num);

extern bool (*wInputIsKeyPressed)(wKeyCode num);

///mouse///
extern bool (*wInputIsMouseEventAvailable)();

extern wMouseEvent* (*wInputReadMouseEvent)();

extern void (*wInputSetCursorVisible)(bool boShow );

extern bool (*wInputIsCursorVisible)();

extern void (*wInputSetMousePosition)(wVector2i* position);

extern void (*wInputGetMousePosition)(wVector2i* position);

extern void (*wInputSetMouseLogicalPosition)(wVector2f* position);

extern void (*wInputGetMouseLogicalPosition)(wVector2f* position);

extern Float32 (*wInputGetMouseWheel)();

extern void (*wInputGetMouseDelta)(wVector2i* deltaPos);

extern bool (*wInputIsMouseUp)(wMouseButtons num);

extern bool (*wInputIsMouseHit)(wMouseButtons num);

extern bool (*wInputIsMousePressed)(wMouseButtons num);

extern Int32 (*wInputGetMouseX)();

extern Int32 (*wInputGetMouseY)();

extern Int32 (*wInputGetMouseDeltaX)();

extern Int32 (*wInputGetMouseDeltaY)();

///joystick///
extern bool (*wInputActivateJoystick)();

extern UInt32 (*wInputGetJoysitcksCount)();

extern void (*wInputGetJoystickInfo)(UInt32 joyIndex,
									 wJoystickInfo* joyInfo);

extern bool (*wInputIsJoystickEventAvailable)();

extern wJoystickEvent* (*wInputReadJoystickEvent)();


///wLight///
extern wNode* (*wLightCreate)(wVector3f position,
							  wColor4f color,
							  Float32 radius);

extern void (*wLightSetAmbientColor)(wNode* light,
									 wColor4f color);

extern wColor4f (*wLightGetAmbientColor)(wNode* light);

extern void (*wLightSetSpecularColor)(wNode* light,
									  wColor4f color);

extern wColor4f (*wLightGetSpecularColor)(wNode* light);

extern void (*wLightSetAttenuation)(wNode* light,
									wVector3f  attenuation); //.x-constant, .y- linear, .z- quadratic

extern wVector3f (*wLightGetAttenuation)(wNode* light);

extern void (*wLightSetCastShadows)(wNode* light,
									bool castShadows);

extern bool (*wLightIsCastShadows)(wNode* light);

extern void (*wLightSetDiffuseColor)(wNode* light,
									 wColor4f color);

extern wColor4f (*wLightGetDiffuseColor)(wNode* light);

extern void (*wLightSetFallOff)(wNode* light,
								Float32 FallOff);

extern Float32 (*wLightGetFallOff)(wNode* light);

extern void (*wLightSetInnerCone)(wNode* light,
								  Float32 InnerCone);

extern Float32 (*wLightGetInnerCone)(wNode* light);

extern void (*wLightSetOuterCone)(wNode* light,
								  Float32 OuterCone);

extern Float32 (*wLightGetOuterCone)(wNode* light);

extern void (*wLightSetRadius)(wNode* light,
							   Float32 Radius );

extern Float32 (*wLightGetRadius)(wNode* light);

extern void (*wLightSetType)(wNode* light,
							 wLightType Type );

extern wLightType (*wLightGetType)(wNode* light);

//Read-ONLY! Direction of the light.
//If Type is WLT_POINT, it is ignored.
//Changed via light scene node's rotation.
extern wVector3f (*wLightGetDirection)(wNode* light);


///wBillboardGroup///
extern wNode* (*wBillboardGroupCreate)(wVector3f position,
									   wVector3f rotation,
									   wVector3f scale);


extern void (*wBillboardGroupSetShadows)(wNode* node,
										 wVector3f direction,
										 Float32 intensity,
										 Float32 ambient );

extern void (*wBillboardGroupResetShadows)(wNode* node);

extern UInt32 (*wBillboardGroupGetSize)(wNode* node );

extern wMeshBuffer* (*wBillboardGroupGetMeshBuffer)(wNode *node);

extern wBillboard* (*wBillboardGroupGetFirstElement)(wNode* node);

extern void (*wBillboardGroupUpdateForce)(wNode* node );

extern wBillboard* (*wBillboardGroupAddElement)(wNode* node,
												wVector3f position,
												wVector2f size,
												Float32 roll,
												wColor4s color);

extern wBillboard* (*wBillboardGroupAddElementByAxis)(wNode* node,
													  wVector3f position,
													  wVector2f size,
													  Float32 roll,
													  wColor4s color,
													  wVector3f axis);

extern void (*wBillboardGroupRemoveElement)(wNode* node,
											wBillboard* billboard);

///wBillboard///
extern wNode* (*wBillboardCreate)(wVector3f position,
								  wVector2f size);

extern void (*wBillboardSetEnabledAxis)(wNode* billboard,
										wBillboardAxisParam param);

extern wBillboardAxisParam (*wBillboardGetEnabledAxis)(wNode* billboard);

extern void (*wBillboardSetColor)(wNode* node,
								  wColor4s topColor,
								  wColor4s bottomColor);

extern void (*wBillboardSetSize)(wNode* node,
								 wVector2f size);

extern wNode* (*wBillboardCreateText)(wVector3f position,
									  wVector2f size,
									  wFont* font,
									  const wchar_t* text,
									  wColor4s topColor,
									  wColor4s bottomColor);

///wSkyBox///
extern wNode* (*wSkyBoxCreate)(wTexture* texture_up,
							   wTexture* texture_down,
							   wTexture* texture_left,
							   wTexture* texture_right,
							   wTexture* texture_front,
							   wTexture* texture_back );

///wSkyDome///
extern wNode* (*wSkyDomeCreate)(wTexture* texture_file,
								UInt32  horiRes,
								UInt32  vertRes,
								Float64  texturePercentage,
								Float64  spherePercentage,
								Float64 domeRadius);


extern void (*wSkyDomeSetColor) (wNode* dome,
								 wColor4s horizonColor,
								 wColor4s zenithColor);

extern void (*wSkyDomeSetColorBand)(wNode* dome,
									wColor4s horizonColor,
									Int32 position,
									Float32 fade,
									bool additive );

extern void (*wSkyDomeSetColorPoint)(wNode* dome,
									 wColor4s horizonColor,
									 wVector3f position,
									 Float32 radius,
									 Float32 fade,
									 bool additive );

///wLodManager///
extern wNode* (*wLodManagerCreate)(UInt32 fadeScale,
								   bool useAlpha,
								   void (*callback)(UInt32, wNode*));

extern void (*wLodManagerAddMesh)(wNode* node,
								  wMesh* mesh,
								  Float32 distance);

extern void (*wLodManagerSetMaterialMap)(wNode* node,
										 wMaterialTypes source,
										 wMaterialTypes target );

///wZoneManager///
extern wNode* (*wZoneManagerCreate)(Float32 initialNearDistance,
									Float32 initialFarDistance );

extern void (*wZoneManagerSetProperties)(wNode* node,
										 Float32 newNearDistance,
										 Float32 newFarDistance,
										 bool accumulateChildBoxes);

extern void (*wZoneManagerSetBoundingBox)(wNode* node,
										  wVector3f position,
										  wVector3f size);

extern void (*wZoneManagerAddTerrain)(wNode* node,
									  wNode* terrainSource,
									  const char* structureMap,
									  const char* colorMap,
									  const char* detailMap,
									  wVector2i pos,
									  Int32 sliceSize );

///wNode///
///primitives///
extern wNode* (*wNodeCreateEmpty)();

extern wNode* (*wNodeCreateCube)(Float32 size,
								 wColor4s color);

extern wNode* (*wNodeCreateSphere)(Float32 radius,
								   Int32 polyCount,
								   wColor4s color);

extern wNode* (*wNodeCreateCylinder)(UInt32 tesselation,
									 Float32 radius,
									 Float32 length,
									 wColor4s color);

extern wNode* (*wNodeCreateCone)(UInt32 tesselation,
								 Float32 radius,
								 Float32 length,
								 wColor4s clorTop,
								 wColor4s clorBottom);

extern wNode* (*wNodeCreatePlane)(Float32 size,
								  UInt32 tileCount,
								  wColor4s color);

extern wNode* (*wNodeCreateFromMesh)(wMesh* mesh);

extern wNode* (*wNodeCreateFromexternMesh)(wMesh* mesh);

extern wNode* (*wNodeCreateFromMeshAsOctree)(wMesh* vptrMesh,
											 Int32 minimalPolysPerNode,
											 bool alsoAddIfMeshPointerZero);

extern wNode* (*wNodeCreateFromBatchingMesh)(wMesh* batchMesh);

extern wNode* (*wNodeCreateFromBatchingMeshAsOctree)(wMesh* batchMesh,
													 Int32 minimalPolysPerNode,
													 bool alsoAddIfMeshPointerZero);

extern void (*wNodeRemoveCollision)(wNode* node,
									wSelector* selector);

extern void (*wNodeAddCollision)(wNode* node,
								 wSelector* selector);

///wWater///
extern wNode* (*wWaterSurfaceCreate)(wMesh* mesh,
									 Float32 waveHeight,
									 Float32 waveSpeed,
									 Float32 waveLength,
									 wVector3f position,
									 wVector3f rotation,
									 wVector3f scale);


///wRealWater///
extern wNode* (*wRealWaterSurfaceCreate)( wTexture* bumpTexture,
										  wVector2f size,
										  wVector2u renderSize);

extern void (*wRealWaterSetWindForce)(wNode* water,
									  Float32 force);

extern void (*wRealWaterSetWindDirection)(wNode* water,
										  wVector2f direction);

extern void (*wRealWaterSetWaveHeight)(wNode* water,
									   Float32 height);

extern void (*wRealWaterSetColor)(wNode* water,
								  wColor4f color);

extern void (*wRealWaterSetColorBlendFactor)(wNode* water,
											 Float32 factor);

///wClouds///
extern wNode* (*wCloudsCreate)( wTexture* texture,
								UInt32 lod,
								UInt32 depth,
								UInt32 density );

///wRealClouds///
extern wNode* (*wRealCloudsCreate)(wTexture* txture,
								   wVector3f height,
								   wVector2f speed,
								   Float32 textureScale);

extern void (*wRealCloudsSetTextureTranslation)(wNode* cloud,
												wVector2f speed);

extern wVector2f (*wRealCloudsGetTextureTranslation)(wNode* cloud);

extern void (*wRealCloudsSetTextureScale)(wNode* cloud,
										  Float32 scale);

extern Float32 (*wRealCloudsGetTextureScale)(wNode* cloud);

extern void (*wRealCloudsSetCloudHeight)(wNode* cloud,
										 wVector3f height);

extern wVector3f (*wRealCloudsGetCloudHeight)(wNode* cloud);

extern void (*wRealCloudsSetCloudRadius)(wNode* cloud,
										 wVector2f radius);

extern wVector2f (*wRealCloudsGetCloudRadius)(wNode* cloud);

extern void (*wRealCloudsSetColors)(wNode* cloud,
									wColor4s centerColor,
									wColor4s innerColor,
									wColor4s outerColor);

extern void (*wRealCloudsGetColors)(wNode* cloud,
									wColor4s* centerColor,
									wColor4s* innerColor,
									wColor4s* outerColor);

///wLensFlare///
extern wNode* (*wLensFlareCreate)(wTexture* texture);

extern void (*wLensFlareSetStrength)(wNode* flare,
									 Float32 strength);

extern Float32 (*wLensFlareGetStrength)(wNode* flare);

///wGrass///
extern wNode* (*wGrassCreate)(wNode* terrain,
							  wVector2i position,
							  UInt32 patchSize,
							  Float32 fadeDistance,
							  bool crossed,
							  Float32 grassScale,
							  UInt32 maxDensity,
							  wVector2u dataPosition,
							  wImage* heightMap,
							  wImage* textureMap,
							  wImage* grassMap,
							  wTexture* grassTexture);

extern void (*wGrassSetDensity)(wNode* grass,
								UInt32 density,
								Float32 distance );

extern void (*wGrassSetWind)(wNode* grass,
							 Float32 strength,
							 Float32 res );

extern UInt32 (*wGrassGetDrawingCount)(wNode* grass );

///wTreeGenerator/////
extern wNode* (*wTreeGeneratorCreate)(const char* xmlFilePath);

extern void (*wTreeGeneratorDestroy)(wNode* generator);

///wTree///
extern wNode* (*wTreeCreate)(wNode* generator,Int32 seed, wTexture* billboardTexture);

extern void (*wTreeSetDistances)(wNode* tree,Float32 midRange,Float32 farRange);

extern wNode* (*wTreeGetLeafNode)(wNode* tree);

extern void (*wTreeSetLeafEnabled)(wNode* tree, bool value);

extern bool (*wTreeIsLeafEnabled)(wNode* tree);

extern wMeshBuffer* (*wTreeGetMeshBuffer)(wNode* tree,
										  UInt32 idx);//0-HIGH meshbuffer,  1- MID meshbuffer

extern void (*wTreeSetBillboardVertexColor)(wNode* tree,wColor4s color);

extern wColor4s (*wTreeGetBillboardVertexColor)(wNode* tree);

///wWindGenerator
extern wNode* (*wWindGeneratorCreate)();

extern void (*wWindGeneratorDestroy)(wNode* windGenerator);

extern void (*wWindGeneratorSetStrength)(wNode* windGenerator,Float32 strength);

extern Float32 (*wWindGeneratorGetStrength)(wNode* windGenerator);

extern void (*wWindGeneratorSetRegularity)(wNode* windGenerator,Float32 regularity);

extern Float32 (*wWindGeneratorGetRegularity)(wNode* windGenerator);

extern wVector2f (*wWindGeneratorGetWind)(wNode* windGenerator,wVector3f position,UInt32 timeMs);

///wBolt///
extern wNode* (*wBoltCreate)();

extern void (*wBoltSetProperties)(wNode* bolt,
								  wVector3f start,
								  wVector3f end,
								  UInt32 updateTime,
								  UInt32 height,
								  Float32 thickness,
								  UInt32 parts,
								  UInt32 bolts,
								  bool steddyend,
								  wColor4s color);
///wBeam///
extern wNode* (*wBeamCreate)();

extern void (*wBeamSetSize)(wNode* beam,
							Float32 size );

extern void (*wBeamSetPosition)(wNode* beam,
								wVector3f start,
								wVector3f end);

///wParticleSystem///
extern wNode* (*wParticleSystemCreate)(bool defaultemitter,
									   wVector3f position,
									   wVector3f rotation,
									   wVector3f scale);

extern wEmitter* (*wParticleSystemGetEmitter)(wNode* ps);

extern void (*wParticleSystemSetEmitter)(wNode* ps,
										 wEmitter* em);

extern void (*wParticleSystemRemoveAllAffectors)(wNode* ps);

extern void (*wParticleSystemSetGlobal)(wNode* ps,
										bool value);

extern void (*wParticleSystemSetParticleSize)(wNode* ps,
											  wVector2f size);

extern void (*wParticleSystemClear)(wNode* ps);

///wParticleBoxEmitter///
extern wEmitter* (*wParticleBoxEmitterCreate)(wNode* ps);

extern void (*wParticleBoxEmitterSetBox)(wEmitter* em,
										 wVector3f boxMin,
										 wVector3f boxMax);

extern void (*wParticleBoxEmitterGetBox)(wEmitter* em,
										 wVector3f* boxMin,
										 wVector3f* boxMax);

///wParticleCylinderEmitter///
extern wEmitter* (*wParticleCylinderEmitterCreate)(wNode* ps,
												   wVector3f center,
												   Float32 radius,
												   wVector3f normal,
												   Float32 lenght);

extern void (*wParticleCylinderEmitterSetParameters)(wEmitter* em,
													 wParticleCylinderEmitter params);

extern void (*wParticleCylinderEmitterGetParameters)(wEmitter* em,
													 wParticleCylinderEmitter* params);

///wParticleMeshEmitter///
extern wEmitter* (*wParticleMeshEmitterCreate)(wNode* ps,
											   wNode* node);

extern void (*wParticleMeshEmitterSetParameters)(wEmitter* em,
												 wParticleMeshEmitter params);

extern void (*wParticleMeshEmitterGetParameters)(wEmitter* em,
												 wParticleMeshEmitter* params);

///wParticlePointEmitter///
extern wEmitter* (*wParticlePointEmitterCreate)(wNode* ps);

///wParticleRingEmitter///
extern wEmitter* (*wParticleRingEmitterCreate)(wNode* ps,
											   wVector3f center,
											   Float32 radius,
											   Float32 ringThickness);

extern void (*wParticleRingEmitterSetParameters)(wEmitter* em,
												 wParticleRingEmitter params);

extern void (*wParticleRingEmitterGetParameters)(wEmitter* em,
												 wParticleRingEmitter* params);

///wParticleSphereEmitter///
extern wEmitter* (*wParticleSphereEmitterCreate)(wNode* ps,
												 wVector3f center,
												 Float32 radius);

extern void (*wParticleSphereEmitterSetParameters)(wEmitter* em,
												   wParticleSphereEmitter params);

extern void (*wParticleSphereEmitterGetParameters)(wEmitter* em,
												   wParticleSphereEmitter* params);

///wParticleEmitter- FOR ALL///
extern void (*wParticleEmitterSetParameters)(wEmitter* em,
											 wParticleEmitter params);

extern void (*wParticleEmitterGetParameters)(wEmitter* em,
											 wParticleEmitter* params);

///wParticleAffector -FOR ALL///
extern void (*wParticleAffectorSetEnable)(wAffector* foa,
										  bool enable );


extern bool (*wParticleAffectorIsEnable)(wAffector* foa);

///wParticleFadeOutAffector///
extern wAffector* (*wParticleFadeOutAffectorCreate)(wNode* ps);

extern void (*wParticleFadeOutAffectorSetTime)(wAffector* paf,
											   UInt32 fadeOutTime);

extern UInt32 (*wParticleFadeOutAffectorGetTime)(wAffector* paf);

extern void (*wParticleFadeOutAffectorSetColor)(wAffector* paf,
												wColor4s targetColor);

extern wColor4s (*wParticleFadeOutAffectorGetColor)(wAffector* paf);

///wParticleGravityAffector///
extern wAffector* (*wParticleGravityAffectorCreate)(wNode* ps);

extern void (*wParticleGravityAffectorSetGravity)(wAffector* paf,
												  wVector3f gravity);

extern wVector3f (*wParticleGravityAffectorGetGravity)(wAffector* paf);

extern void (*wParticleGravityAffectorSetTimeLost)(wAffector* paf,
												   UInt32 timeForceLost);

extern UInt32 (*wParticleGravityAffectorGetTimeLost)(wAffector* paf);

///wParticleAttractionAffector///
extern wAffector* (*wParticleAttractionAffectorCreate)(wNode* ps,
													   wVector3f point,
													   Float32 speed);

extern void (*wParticleAttractionAffectorSetParameters)(wAffector* paf,
														wParticleAttractionAffector params);

extern void (*wParticleAttractionAffectorGetParameters)(wAffector* paf,
														wParticleAttractionAffector* params);

///wParticleRotationAffector///
extern wAffector*  (*wParticleRotationAffectorCreate)(wNode* ps);

extern void (*wParticleRotationAffectorSetSpeed)(wAffector* paf,
												 wVector3f speed);

extern wVector3f (*wParticleRotationAffectorGetSpeed)(wAffector* paf);

extern void (*wParticleRotationAffectorSetPivot)(wAffector* paf,
												 wVector3f pivotPoint);

extern wVector3f (*wParticleRotationAffectorGetPivot)(wAffector* paf);

///wParticleStopAffector///
extern wAffector*  (*wParticleStopAffectorCreate)(wNode* ps,
												  wEmitter* em,
												  UInt32 time);

extern void (*wParticleStopAffectorSetTime)(wAffector* paf,
											UInt32 time);

extern UInt32 (*wParticleStopAffectorGetTime)(wAffector* paf);

///wParticleColorMorphAffector///
extern wAffector*  (*wParticleColorMorphAffectorCreate)(wNode* ps);

extern void (*wParticleColorAffectorSetParameters)(wAffector* paf,
												   wParticleColorMorphAffector params);

extern void (*wParticleColorAffectorGetParameters)(wAffector* paf,
												   wParticleColorMorphAffector* params);

///wParticlePushAffector///
extern wAffector*  (*wParticlePushAffectorCreate)(wNode* ps);

extern void (*wParticlePushAffectorSetParameters)(wAffector* paf,
												  wParticlePushAffector params);

extern void (*wParticlePushAffectorGetParameters)(wAffector* paf,
												  wParticlePushAffector* params);

///wParticleSplineAffector///
extern wAffector*  (*wParticleSplineAffectorCreate)(wNode* ps);

extern void (*wParticleSplineAffectorSetParameters)(wAffector* paf,
													wParticleSplineAffector params);

extern void (*wParticleSplineAffectorGetParameters)(wAffector* paf,
													wParticleSplineAffector* params);

///wParticleScaleAffector///
extern wAffector* (*wParticleScaleAffectorCreate)(wNode* ps,
												  wVector2f scaleTo);

///wNode///
extern void (*wNodeSetDecalsEnabled)(wNode* node);

extern void (*wNodeSetParent)(wNode* node,
							  wNode *parent );

extern wNode* (*wNodeGetParent)(wNode* node );

extern void (*wNodeSetReadOnlyMaterials)(wNode* node,
										 bool readonly);

extern bool (*wNodeIsReadOnlyMaterials)(wNode* node);

extern wNode* (*wNodeGetFirstChild)(wNode* node,
									UInt32* iterator);
extern UInt32 (*wNodeGetChildsCount)(wNode* node,UInt32* iterator);

extern wNode* (*wNodeGetNextChild)(wNode* node,
								   UInt32* iterator);

extern bool (*wNodeIsLastChild)(wNode* node,
								UInt32* iterator);

extern void (*wNodeSetId)(wNode* node,
						  Int32 id);

extern Int32 (*wNodeGetId)(wNode* node);

extern void (*wNodeSetName)(wNode* node,
							const char* name );

extern const char* (*wNodeGetName)(wNode* node);

extern void (*wNodeSetUserData)(wNode* node,
								void* const newData);

extern void* (*wNodeGetUserData)(wNode* node);

extern void (*wNodeSetDebugMode)(wNode* node,
								 wDebugMode visible);

extern void (*wNodeSetDebugDataVisible)(wNode* node,
										bool value);

extern UInt32 (*wNodeGetMaterialsCount)(wNode* node );

extern wMaterial* (*wNodeGetMaterial)(wNode* node,
									  UInt32 matIndex );

extern void (*wNodeSetPosition)(wNode* node,
								wVector3f position);

extern wVector3f (*wNodeGetPosition)(wNode* node);

extern wVector3f (*wNodeGetAbsolutePosition)(wNode* node);

extern void (*wNodeSetRotation) (wNode* node,
								 wVector3f rotation);

extern void (*wNodeSetAbsoluteRotation)(wNode* node,
										wVector3f rotation);

extern wVector3f (*wNodeGetRotation)(wNode* node);

extern wVector3f (*wNodeGetAbsoluteRotation)(wNode* node);

extern void (*wNodeTurn)(wNode* Entity,
						 wVector3f turn);

extern void (*wNodeMove)(wNode* Entity,
						 wVector3f direction);

extern void (*wNodeRotateToNode)(wNode* Entity1,
								 wNode* Entity2);

extern Float32 (*wNodesGetBetweenDistance)(wNode* nodeA,
										   wNode* nodeB );

extern bool (*wNodesAreIntersecting)(wNode* nodeA,
									 wNode* nodeB );

extern bool (*wNodeIsPointInside)(wNode* node,
								  wVector3f pos);

extern void (*wNodeDrawBoundingBox)(wNode* node,
									wColor4s color);

extern void (*wNodeGetBoundingBox)(wNode* Node,
								   wVector3f* min,
								   wVector3f* max);

extern void (*wNodeGetTransformedBoundingBox)(wNode* Node,
											  wVector3f* min,
											  wVector3f* max);

extern void (*wNodeSetScale)(wNode* node,
							 wVector3f scale );

extern wVector3f (*wNodeGetScale)(wNode* node);

extern wNode* (*wNodeDuplicate)(wNode* entity);

extern wNode* (*wNodeGetJointByName)(wNode* node,
									 const char *node_name );

extern wNode* (*wNodeGetJointById)( wNode* node,UInt32 Id);

extern Int32 (*wNodeGetJointsCount)( wNode* node);

extern void (*wNodeSetJointSkinningSpace)(wNode* bone,
										  wBoneSkinningSpace space );

extern wBoneSkinningSpace (*wNodeGetJointSkinningSpace)(wNode* bone);

extern void (*wNodeSetRenderFromIdentity)(wNode* node, bool value);

extern void (*wNodeAddShadowVolume)(wNode* node,
									wMesh* mesh,
									bool zfailMethod,
									Float32 infinity,
									bool oldStyle);

extern wNode* (*wNodeAddShadowVolumeFromMeshBuffer)(wNode* nodeParent,
													wMeshBuffer* meshbuffer,
													bool zfailMethod,
													Float32 infinity,
													bool oldStyle);

extern void (*wNodeUpdateShadow)(wNode* shadow);

extern void (*wNodeSetVisibility)(wNode* node,
								  bool visible );

extern bool (*wNodeIsVisible)(wNode* node);

extern bool (*wNodeIsInView)(wNode* node);

extern void (*wNodeDestroy)(wNode* node);

extern void (*wNodeSetMesh)(wNode* node,
							wMesh* mesh);

extern wMesh* (*wNodeGetMesh)(wNode* node);

extern void (*wNodeSetRotationPositionChange)(wNode* node,
											  wVector3f angles,
											  wVector3f offset,
											  wVector3f* forwardStore,
											  wVector3f* upStore,
											  UInt32 numOffsets,
											  wVector3f* offsetStore );

extern void (*wNodeSetCullingState)(wNode* node,
									wCullingState state);

extern wSceneNodeType (*wNodeGetType)(wNode* node);

extern void (*wNodeSetAnimationRange)(wNode* node,
									  wVector2i range);

extern void (*wNodePlayMD2Animation)(wNode* node,
									 wMd2AnimationType iAnimation);

extern void (*wNodeSetAnimationSpeed)(wNode* node,
									  Float32 fSpeed);

extern void (*wNodeSetAnimationFrame)(wNode* node,
									  Float32 fFrame);

extern Float32 (*wNodeGetAnimationFrame)(wNode* node);

extern void (*wNodeSetTransitionTime)(wNode* node,
									  Float32 fTime);

extern void (*wNodeAnimateJoints)(wNode* node);

extern void (*wNodeSetJointMode)(wNode* node,
								 wJointMode mode);

extern void (*wNodeSetAnimationLoopMode)(wNode* node,
										 bool value);

extern void (*wNodeDestroyAllAnimators)(wNode* node);

extern UInt32 (*wNodeGetAnimatorsCount)(wNode* node);

extern wAnimator* (*wNodeGetFirstAnimator)(wNode* node);

extern wAnimator* (*wNodeGetLastAnimator)(wNode* node);

extern wAnimator* (*wNodeGetAnimatorByIndex)(wNode* node,
											 UInt32 index);

extern void (*wNodeOnAnimate)(wNode* node,UInt32 timeMs);

extern void (*wNodeDraw)(wNode* node);

extern void (*wNodeUpdateAbsolutePosition) (wNode* node);

///wMaterial///
extern void (*wMaterialSetTexture)(wMaterial* material,
								   UInt32 texIdx,
								   wTexture* texture);

extern wTexture* (*wMaterialGetTexture)(wMaterial* material,
										UInt32 texIdx);

extern void (*wMaterialScaleTexture)(wMaterial* material,
									 UInt32 texIdx,
									 wVector2f scale);

extern void (*wMaterialScaleTextureFromCenter)(wMaterial* material,
											   UInt32 texIdx,
											   wVector2f scale);

extern void (*wMaterialTranslateTexture)(wMaterial* material,
										 UInt32 texIdx,
										 wVector2f translate);

extern void (*wMaterialTranslateTextureTransposed)(wMaterial* material,
												   UInt32 texIdx,
												   wVector2f translate);

extern void (*wMaterialRotateTexture)(wMaterial* material,
									  UInt32 texIdx,
									  Float32 angle);

extern void (*wMaterialSetTextureWrapUMode)(wMaterial* material,
											UInt32 texIdx,
											wTextureClamp value);

extern wTextureClamp (*wMaterialGetTextureWrapUMode)(wMaterial* material,
													 UInt32 texIdx);

extern void (*wMaterialSetTextureWrapVMode)(wMaterial* material,
											UInt32 texIdx,
											wTextureClamp value);

extern wTextureClamp (*wMaterialGetTextureWrapVMode)(wMaterial* material,
													 UInt32 texIdx);

extern void (*wMaterialSetTextureLodBias)(wMaterial* material,
										  UInt32 texIdx,
										  UInt32 lodBias);

extern UInt32 (*wMaterialGetTextureLodBias)(wMaterial* material,
											UInt32 texIdx);

extern void (*wMaterialSetFlag)(wMaterial* material,
								wMaterialFlags Flag,
								bool boValue);

extern bool (*wMaterialGetFlag)(wMaterial* material,
								wMaterialFlags matFlag);

extern void (*wMaterialSetType)(wMaterial* material,
								wMaterialTypes type );

extern void (*wMaterialSetShininess)(wMaterial* material,
									 Float32 shininess);

extern Float32 (*wMaterialGetShininess)(wMaterial* material);

extern void (*wMaterialSetVertexColoringMode)(wMaterial* material,
											  wColorMaterial colorMaterial );

extern wColorMaterial (*wMaterialGetVertexColoringMode)(wMaterial* material);

extern void (*wMaterialSetSpecularColor)(wMaterial* material,
										 wColor4s color);

extern wColor4s (*wMaterialGetSpecularColor)(wMaterial* material);

extern void (*wMaterialSetDiffuseColor)(wMaterial* material,
										wColor4s color);

extern wColor4s (*wMaterialGetDiffuseColor)(wMaterial* material);

extern void (*wMaterialSetAmbientColor)(wMaterial* material,
										wColor4s color);

extern wColor4s (*wMaterialGetAmbientColor)(wMaterial* material);

extern void (*wMaterialSetEmissiveColor)(wMaterial* material,
										 wColor4s color);

extern wColor4s (*wMaterialGetEmissiveColor)(wMaterial* material);

extern void (*wMaterialSetTypeParameter)(wMaterial* material,
										 Float32 param1);

extern Float32 (*wMaterialGetTypeParameter)(wMaterial* material);

extern void (*wMaterialSetTypeParameter2)(wMaterial* material,
										  Float32 param2);

extern Float32 (*wMaterialGetTypeParameter2)(wMaterial* material);

extern void (*wMaterialSetBlendingMode)(wMaterial* material,
										const wBlendFactor blendSrc,
										const wBlendFactor blendDest);

//wMaterialGetBlendingMode = wMaterialGetTypeParameter

extern void (*wMaterialSetLineThickness)(wMaterial* material,
										 Float32 lineThickness );

extern Float32 (*wMaterialGetLineThickness)(wMaterial* material);

extern void (*wMaterialSetColorMask)(wMaterial* material,
									 wColorPlane value);

extern wColorPlane (*wMaterialGetColorMask)(wMaterial* material);

extern void (*wMaterialSetAntiAliasingMode)(wMaterial* material,
											wAntiAliasingMode mode);

extern wAntiAliasingMode (*wMaterialGetAntiAliasingMode)(wMaterial* material);


///wShader///
extern bool (*wShaderCreateNamedVertexConstant)(wShader* shader,
												const char* name,
												Int32 preset,
												const float* floats,
												Int32 count);

extern bool (*wShaderCreateNamedPixelConstant)(wShader* shader,
											   const char*	name,
											   int	preset,
											   const float* floats,
											   int	count);

extern bool (*wShaderCreateAddressedVertexConstant)(wShader* shader,
													Int32 address,
													int	preset,
													const float* floats,
													int	count);

extern bool (*wShaderCreateAddressedPixelConstant)(wShader* shader,
												   int	address,
												   int	preset,
												   const float* floats,
												   int	count);

extern wShader* (*wShaderAddHighLevelMaterial)(const char* vertexShaderProgram,
											   const char*  vertexShaderEntryPointName,
											   wVertexShaderVersion wVersion,
											   const char* pixelShaderProgram,
											   const char*  pixelShaderEntryPointName,
											   wPixelShaderVersion pVersion,
											   wMaterialTypes materialType,
											   Int32 userData);

extern wShader* (*wShaderAddHighLevelMaterialFromFiles)(const char* vertexShaderProgramFileName,
														const char*  vertexShaderEntryPointName,
														wVertexShaderVersion wVersion,
														const char * pixelShaderProgramFileName,
														const char*  pixelShaderEntryPointName,
														wPixelShaderVersion pVersion,
														wMaterialTypes materialType,
														Int32 userData);

extern wShader* (*wShaderAddMaterial)(const char*  vertexShaderProgram,
									  const char*  pixelShaderProgram,
									  wMaterialTypes materialType,
									  Int32 userData);

extern wShader* (*wShaderAddMaterialFromFiles)(const char*  vertexShaderProgramFileName,
											   const char*  pixelShaderProgramFileName,
											   wMaterialTypes materialType,
											   Int32 userData);


///with geometry shader
extern wShader* (*wShaderAddHighLevelMaterialEx)(const char* vertexShaderProgram,
												 const char*  vertexShaderEntryPointName,
												 wVertexShaderVersion wVersion,
												 const char* pixelShaderProgram,
												 const char*  pixelShaderEntryPointName,
												 wPixelShaderVersion pVersion,
												 const char* geometryShaderProgram,
												 const char*  geometryShaderEntryPointName,
												 wGeometryShaderVersion gVersion,
												 wPrimitiveType inType,
												 wPrimitiveType outType,
												 UInt32 verticesOut,
												 wMaterialTypes materialType,
												 Int32 userData);

///with geometry shader
extern wShader* (*wShaderAddHighLevelMaterialFromFilesEx)(const char* vertexShaderProgramFileName,
														  const char*  vertexShaderEntryPointName,
														  wVertexShaderVersion wVersion,
														  const char* pixelShaderProgramFileName,
														  const char*  pixelShaderEntryPointName,
														  wPixelShaderVersion pVersion,
														  const char* geometryShaderProgram,
														  const char*  geometryShaderEntryPointName,
														  wGeometryShaderVersion gVersion,
														  wPrimitiveType inType,
														  wPrimitiveType outType,
														  UInt32 verticesOut,
														  wMaterialTypes materialType,
														  Int32 userData);

///wMesh///
extern wMesh* (*wMeshLoad)(const char* cptrFile, bool ToTangents);

extern wMesh* (*wMeshCreate)(const char *cptrMeshName);

extern void (*wMeshAddMeshBuffer)(wMesh* mesh,
								  wMeshBuffer* meshbuffer);

extern wMesh* (*wMeshCreateSphere)(const char* name,
								   Float32 radius,
								   Int32 polyCount );

extern wMesh* (*wMeshCreateCube)();

extern Int32 (*wMeshSave)(wMesh* mesh,
						  wMeshFileFormat type,
						  const char* filename);//return 0/1/2/3  3- успешно

extern void (*wMeshDestroy)(wMesh* mesh);

extern bool (*wMeshSetName)(wMesh* mesh,
							const char* name);

extern const char* (*wMeshGetName)(wMesh* mesh);

extern wAnimatedMeshType (*wMeshGetType)(wMesh* mesh);

extern void (*wMeshFlipSurface)(wMesh* mesh);

extern void (*wMeshMakePlanarTextureMapping)(wMesh* mesh,
											 Float32 resolution);

extern void (*wMeshMakePlanarTextureMappingAdvanced)(wMesh* mesh,
													 Float32 resolutionH,
													 Float32 resolutionV,
													 UInt8 axis,
													 wVector3f offset);

extern wMesh* (*wMeshCreateexternWithTangents)(wMesh* aMesh);

extern void (*wMeshRecalculateNormals)(wMesh* mesh,
									   bool smooth,
									   bool angleWeighted);

extern void (*wMeshRecalculateTangents)(wMesh* mesh,
										bool recalculateNormals,
										bool smooth,
										bool angleWeighted);

extern wMesh* (*wMeshCreateHillPlane)(const char* meshname,
									  wVector2f tilesSize,
									  wVector2i tilesCount,
									  wMaterial* material,
									  Float32 hillHeight,
									  wVector2f countHills,
									  wVector2f texRepeatCount);

extern wMesh* (*wMeshCreateArrow)(const char* name,
								  wColor4s cylinderColor,
								  wColor4s coneColor,
								  UInt32 tesselationCylinder,
								  UInt32 tesselationCone,
								  Float32 height,
								  Float32 heightCylinder,
								  Float32 widthCylinder,
								  Float32 widthCone);

extern wMesh* (*wMeshCreateBatching)();

extern void (*wMeshAddToBatching)(wMesh* meshBatch,
								  wMesh* mesh,
								  wVector3f position,
								  wVector3f rotation,
								  wVector3f scale);

extern void (*wMeshUpdateBatching)(wMesh* meshBatch);

extern void (*wMeshFinalizeBatching)(wMesh* meshBatch);

extern void (*wMeshClearBatching)(wMesh* meshBatch);

extern void (*wMeshDestroyBatching)(wMesh* meshBatch);

extern void (*wMeshEnableHardwareAcceleration)(wMesh* mesh,
											   UInt32 iFrame);

extern UInt32 (*wMeshGetFramesCount)(wMesh* mesh );

extern UInt32 (*wMeshGetIndicesCount)(wMesh* mesh,
									  UInt32 iFrame,
									  UInt32 iMeshBuffer);

extern UInt16* (*wMeshGetIndices)(wMesh* mesh,
								  UInt32 iFrame,
								  UInt32 iMeshBuffer);

extern void (*wMeshSetIndices)(wMesh* mesh,
							   UInt32 iFrame,
							   UInt16* indicies,
							   UInt32 iMeshBuffer);

extern UInt32 (*wMeshGetVerticesCount)(wMesh* mesh,
									   UInt32 iFrame,
									   UInt32 iMeshBuffer);

extern void (*wMeshGetVertices)(wMesh* mesh,
								UInt32 iFrame,
								wVert* verts,
								UInt32 iMeshBuffer);

extern UInt32* (*wMeshGetVerticesMemory)(wMesh* mesh,
										 UInt32 iFrame,
										 UInt32 iMeshBuffer);

extern void (*wMeshSetVertices)(wMesh* mesh,
								UInt32 iFrame,
								wVert* verts,
								UInt32 iMeshBuffer);

extern void (*wMeshSetScale)( wMesh* mesh,
							  Float32 scale,
							  UInt32 iFrame,
							  UInt32 iMeshBuffer,
							  wMesh* sourceMesh);

extern void (*wMeshSetRotation)(wMesh* mesh,
								wVector3f rot);

extern void (*wMeshSetVerticesColors)(wMesh* mesh,
									  UInt32 iFrame,
									  wColor4s* verticesColor,
									  UInt32 groupCount,
									  UInt32* startPos,
									  UInt32* endPos,
									  UInt32 iMeshBuffer);


extern void (*wMeshSetVerticesAlpha)(wMesh* mesh,
									 UInt32 iFrame,
									 UInt8 value);

extern void (*wMeshSetVerticesCoords)(wMesh* mesh,
									  UInt32 iFrame,
									  wVector3f* vertexCoord,
									  UInt32 groupCount,
									  UInt32* startPos,
									  UInt32* endPos,
									  UInt32 iMeshBuffer);

extern void (*wMeshSetVerticesSingleColor)(wMesh* mesh,
										   UInt32 iFrame,
										   wColor4s verticesColor,
										   UInt32 groupCount,
										   UInt32* startPos,
										   UInt32* endPos,
										   UInt32 iMeshBuffer);

extern void (*wMeshGetBoundingBox) (wMesh* mesh,
									wVector3f* min,
									wVector3f* max);

extern wMesh* (*wMeshDuplicate)(wMesh* src);

extern void (*wMeshFit)(wMesh* src,
						wVector3f pivot,
						wVector3f* delta);

extern bool (*wMeshIsEmpty)(wMesh* mesh);

extern UInt32 (*wMeshGetBuffersCount)(wMesh* mesh,
									  UInt32 iFrame);

extern wMeshBuffer* (*wMeshGetBuffer)(wMesh* mesh,
									  UInt32 iFrame,
									  UInt32 index);

///wMeshBuffer///
extern wMeshBuffer* (*wMeshBufferCreate)(
		UInt32 iVertexCount,
		wVert* vVertices,
		UInt32 iIndicesCount,
		UInt16* usIndices);

/*wMeshBuffer* wMeshBufferCreateFromMeshJoint(wMesh* mesh,
                                            Int32 jointIndex);*/

extern void (*wMeshBufferDestroy)(wMeshBuffer* buf);

extern void (*wMeshBufferAddToBatching)(wMesh* meshBatch,
										wMeshBuffer* buffer,
										wVector3f position,
										wVector3f rotation,
										wVector3f scale);

extern wMaterial* (*wMeshBufferGetMaterial)(wMeshBuffer* buf);

///wBsp///
///Get BSP Entity List///
extern UInt32* (*wBspGetEntityList)(wMesh* const mesh);

///Get BSP Entity List size///
extern Int32 (*wBspGetEntityListSize)(UInt32* entityList);

///Get First (vec.x) and Last (vec.y) BSP Entity Index///
extern wVector2i (*wBspGetEntityIndexByName)(void* entityList,
											 const char* EntityName);
///Name BSP Entity From Index
extern const char* (*wBspGetEntityNameByIndex)(UInt32* entityList,
											   UInt32 number);
///Mesh from BSP Brush///
extern wMesh* (*wBspGetEntityMeshFromBrush)(wMesh* bspMesh,
											UInt32* entityList,
											Int32 index);
///BSP VarGroup///
extern UInt32* (*wBspGetVarGroupByIndex)(UInt32* entityList,
										 Int32 index);

extern UInt32 (*wBspGetVarGroupSize)(UInt32* entityList,
									 Int32 index);

extern wVector3f (*wBspGetVarGroupValueAsVec)(UInt32* group,
											  const char* strName,
											  UInt32 parsePos);

extern Float32 (*wBspGetVarGroupValueAsFloat)(UInt32* group,
											  const  char* strName,
											  UInt32 parsePos);

extern const char* (*wBspGetVarGroupValueAsString)(UInt32* group,
												   const char* strName);
/*
UInt32 wBspGetVarGroupVariableSize(UInt32* group);


UInt32* wBspGetVariableFromVarGroup(UInt32* group,
                                          Int32 index);

const char* wBspGetVariableName(UInt32* variable);

const char* wBspGetVariableContent(UInt32* variable);

wVector3f wBspGetVariableValueAsVec(UInt32* variable,
                                    UInt32 parsePos);

Float32 wBspGetVariableValueAsFloat(UInt32* variable,
                                  UInt32 parsePos);
*/

extern wNode* (*wBspCreateFromMesh) (wMesh* const mesh,
									 bool isTangent,
									 bool isOctree,
									 const char* fileEntity,
									 bool isLoadShaders,
									 UInt32 PolysPerNode);

///Occlusion Query
extern void (*wOcclusionQueryAddNode)(wNode* node);

extern void (*wOcclusionQueryAddMesh)(wNode* node,wMesh* mesh);

extern void (*wOcclusionQueryUpdate)(wNode* node,bool block);

extern void (*wOcclusionQueryRun)(wNode* node,bool visible);

extern void (*wOcclusionQueryUpdateAll)(bool block);

extern void (*wOcclusionQueryRunAll)(bool visible);

extern void (*wOcclusionQueryRemoveNode)(wNode* node);

extern void (*wOcclusionQueryRemoveAll)();

extern UInt32 (*wOcclusionQueryGetResult)(wNode* node);

///wSphericalTerrain///
extern wNode* (*wSphericalTerrainCreate)( const char *cptrFile0,
										  const char *cptrFile1,
										  const char *cptrFile2,
										  const char *cptrFile3,
										  const char *cptrFile4,
										  const char *cptrFile5,
										  wVector3f position,
										  wVector3f rotation,
										  wVector3f scale,
										  wColor4s color,
										  Int32 smootFactor,
										  bool spherical,
										  Int32 maxLOD,
										  wTerrainPatchSize patchSize);

extern void (*wSphericalTerrainSetTextures)(wNode* terrain,
											wTexture* textureTop,
											wTexture* textureFront,
											wTexture* textureBack,
											wTexture* textureLeft,
											wTexture* textureRight,
											wTexture* textureBottom,
											UInt32 materialIndex);

extern void (*wSphericalTerrainLoadVertexColor)(wNode* terrain,
												wImage* imageTop,
												wImage* imageFront,
												wImage* imageBack,
												wImage* imageLeft,
												wImage* imageRight,
												wImage* imageBottom );

extern wVector3f (*wSphericalTerrainGetSurfacePosition)(wNode* terrain,
														Int32 face,
														wVector2f logicalPos);

extern wVector3f (*wSphericalTerrainGetSurfaceAngle)(wNode* terrain,
													 Int32 face,
													 wVector2f logicalPos);

extern wVector2f (*wSphericalTerrainGetSurfaceLogicalPosition)(wNode* terrain,
															   wVector3f position,
															   int* face);

///wTerrain///
extern wNode* (*wTerrainCreate)(const char* cptrFile,
								wVector3f position,
								wVector3f rotation,
								wVector3f scale,
								wColor4s color,
								Int32 smoothing,
								Int32 maxLOD,
								wTerrainPatchSize patchSize);


extern void (*wTerrainScaleDetailTexture)(wNode* terrain,
										  wVector2f scale);

extern Float32 (*wTerrainGetHeight)(wNode* terrain,
									wVector2f positionXZ);

///wTiledTerrain///
extern wNode* (*wTiledTerrainCreate)(wImage* image,
									 Int32 tileSize,
									 wVector2i dataSize,
									 wVector3f position,
									 wVector3f rotation,
									 wVector3f scale,
									 wColor4s color,
									 Int32 smoothing,
									 Int32 maxLOD,
									 wTerrainPatchSize patchSize );

extern void (*wTiledTerrainAddTile)(wNode* terrain,
									wNode* neighbour,
									wTiledTerrainEdge edge);

extern void (*wTiledTerrainSetTileStructure)(wNode* terrain,
											 wImage* image,
											 wVector2i data);

extern void (*wTiledTerrainSetTileColor)(wNode* terrain,
										 wImage* image,
										 wVector2i data);

///wSoundBuffer
extern wSoundBuffer* (*wSoundBufferLoad)(const char* filePath);

extern wSoundBuffer* (*wSoundBufferLoadFromMemory)(const char* data,
												   Int32 length,
												   const char* extension);

extern void (*wSoundBufferDestroy)(wSoundBuffer* buf);

///wSound///
extern wSound* (*wSoundLoad)(const char* filePath,
							 bool stream);

extern wSound* (*wSoundLoadFromMemory)(const char* name,
									   const char* data,
									   Int32 length,
									   const char* extension);

extern wSound* (*wSoundLoadFromRaw)(const char* name, const char* data,
									Int32 length,
									UInt32 frequency,
									wAudioFormats format);

extern wSound* (*wSoundCreateFromBuffer)(wSoundBuffer* buf);

extern bool (*wSoundIsPlaying)(wSound* sound);

extern bool (*wSoundIsPaused)(wSound* sound);

extern bool (*wSoundIsStopped)(wSound* sound);

extern void (*wSoundSetVelocity)(wSound* sound,
								 wVector3f velocity);

extern wVector3f (*wSoundGetVelocity)(wSound* sound);

extern void (*wSoundSetDirection)(wSound* sound,
								  wVector3f direction);

extern wVector3f (*wSoundGetDirection)(wSound* sound);

extern void (*wSoundSetVolume)(wSound* sound,
							   Float32 value);

extern Float32 (*wSoundGetVolume)(wSound* sound);

extern void (*wSoundSetMaxVolume)(wSound* sound,
								  Float32 value);

extern Float32 (*wSoundGetMaxVolume)(wSound* sound);

extern void (*wSoundSetMinVolume)(wSound* sound,
								  Float32 value);

extern Float32 (*wSoundGetMinVolume)(wSound* sound);

extern void (*wSoundSetPitch)(wSound* sound,
							  Float32 value);

extern Float32 (*wSoundGetPitch)(wSound* sound);

extern void (*wSoundSetRollOffFactor)(wSound* sound,
									  Float32 value);

extern Float32 (*wSoundGetRollOffFactor)(wSound* sound);

extern void (*wSoundSetStrength)(wSound* sound,
								 Float32 value);

extern Float32 (*wSoundGetStrength)(wSound* sound);

extern void (*wSoundSetMinDistance)(wSound* sound,
									Float32 value);

extern Float32 (*wSoundGetMinDistance)(wSound* sound);

extern void (*wSoundSetMaxDistance)(wSound* sound,
									Float32 Value);

extern Float32 (*wSoundGetMaxDistance)(wSound* sound);

extern void (*wSoundSetInnerConeAngle)(wSound* sound,
									   Float32 Value);

extern Float32 (*wSoundGetInnerConeAngle)(wSound* sound);

extern void (*wSoundSetOuterConeAngle)(wSound* sound,
									   Float32 Value);

extern Float32 (*wSoundGetOuterConeAngle)(wSound* sound);

extern void (*wSoundSetOuterConeVolume)(wSound* sound,
										Float32 Value);

extern Float32 (*wSoundGetOuterConeVolume)(wSound* sound);

extern void (*wSoundSetDopplerStrength)(wSound* sound,
										Float32 Value);

extern Float32 (*wSoundGetDopplerStrength)(wSound* sound);

extern void (*wSoundSetDopplerVelocity)(wSound* sound,
										wVector3f velocity);

extern wVector3f (*wSoundGetDopplerVelocity)(wSound* sound);

extern Float32 (*wSoundCalculateGain)(wSound* sound);

extern void (*wSoundSetRelative)(wSound* sound,
								 bool value);

extern bool (*wSoundIsRelative)(wSound* sound);

extern bool (*wSoundPlay)(wSound* sound,
						  bool loop);

extern void (*wSoundStop)(wSound* sound);

extern void (*wSoundPause)(wSound* sound);

extern void (*wSoundSetLoopMode)(wSound* sound,
								 bool value);

extern bool (*wSoundIsLooping)(wSound* sound);

extern bool (*wSoundIsValid)(wSound* sound);

extern bool (*wSoundSeek)(wSound* sound,
						  Float32 seconds,
						  bool relative);

extern void (*wSoundUpdate)(wSound* sound);

extern Float32 (*wSoundGetTotalAudioTime)(wSound* sound);

extern Int32 (*wSoundGetTotalAudioSize)(wSound* sound);

extern Int32 (*wSoundGetCompressedAudioSize)(wSound* sound);

extern Float32 (*wSoundGetCurrentAudioTime)(wSound* sound);

extern Int32 (*wSoundGetCurrentAudioPosition)(wSound* sound);

extern Int32 (*wSoundGetCurrentCompressedAudioPosition)(wSound* sound);

extern UInt32 (*wSoundGetNumEffectSlotsAvailable)(wSound* sound);

///sound effects///
extern bool (*wSoundAddEffect)(wSound* sound,
							   UInt32 slot,
							   wSoundEffect* effect);

extern void (*wSoundRemoveEffect)(wSound* sound,
								  UInt32 slot);

extern wSoundEffect* (*wSoundCreateEffect)();

extern bool (*wSoundIsEffectValid)(wSoundEffect* effect);

extern bool (*wSoundIsEffectSupported)(wSoundEffectType type);

extern UInt32 (*wSoundGetMaxEffectsSupported)();

extern void (*wSoundSetEffectType)(wSoundEffect* effect,
								   wSoundEffectType type);

extern wSoundEffectType (*wSoundGetEffectType)(wSoundEffect* effect);

extern void (*wSoundSetEffectAutowahParameters)(wSoundEffect* effect,
												wAutowahParameters param);

extern void (*wSoundSetEffectChorusParameters)(wSoundEffect* effect,
											   wChorusParameters param);

extern void (*wSoundSetEffectCompressorParameters)(wSoundEffect* effect,
												   wCompressorParameters param);

extern  void (*wSoundSetEffectDistortionParameters)(wSoundEffect* effect,
													wDistortionParameters param);

extern void (*wSoundSetEffectEaxReverbParameters)(wSoundEffect* effect,
												  wEaxReverbParameters param);

extern void (*wSoundSetEffectEchoParameters)(wSoundEffect* effect,
											 wEchoParameters param);

extern void (*wSoundSetEffectEqualizerParameters)(wSoundEffect* effect,
												  wEqualizerParameters param);

extern void (*wSoundSetEffectFlangerParameters)(wSoundEffect* effect,
												wFlangerParameters param);

extern void (*wSoundSetEffectFrequencyShiftParameters)(wSoundEffect* effect,
													   wFrequencyShiftParameters param);

extern void (*wSoundSetEffectPitchShifterParameters)(wSoundEffect* effect,
													 wPitchShifterParameters param);

extern void (*wSoundSetEffectReverbParameters)(wSoundEffect* effect,
											   wReverbParameters param);

extern void (*wSoundSetEffectRingModulatorParameters)(wSoundEffect* effect,
													  wRingModulatorParameters param);

extern void (*wSoundSetEffectVocalMorpherParameters)(wSoundEffect* effect,
													 wVocalMorpherParameters param);

///Sound filters///
extern wSoundFilter* (*wSoundCreateFilter)();

extern bool (*wSoundIsFilterValid)(wSoundFilter* filter);

extern bool (*wSoundAddFilter)(wSound* sound,
							   wSoundFilter* filter);

extern void (*wSoundRemoveFilter)(wSound* sound);

extern bool (*wSoundIsFilterSupported)(wSoundFilterType type);

extern void (*wSoundSetFilterType)(wSoundFilter* filter,
								   wSoundFilterType type);

extern wSoundFilterType (*wSoundGetFilterType)(wSoundFilter* filter);

extern void (*wSoundSetFilterVolume)(wSoundFilter* filter,
									 Float32 volume);

extern Float32 (*wSoundGetFilterVolume)(wSoundFilter* filter);

extern void (*wSoundSetFilterHighFrequencyVolume)(wSoundFilter* filter,
												  Float32 volumeHF);

extern Float32 (*wSoundGetFilterHighFrequencyVolume)(wSoundFilter* filter);

extern void (*wSoundSetFilterLowFrequencyVolume)(wSoundFilter* filter,
												 Float32 volumeLF);

extern Float32 (*wSoundGetFilterLowFrequencyVolume)(wSoundFilter* filter);

///wVideo///
extern wVideo* (*wVideoLoad)(const char* fileName);

extern void (*wVideoPlay)(wVideo* player);

extern bool (*wVideoIsPlaying)(wVideo* player);

extern void (*wVideoRewind)(wVideo* player);

extern void (*wVideoSetLoopMode)(wVideo* player,
								 bool looping);

extern bool (*wVideoIsLooping)(wVideo* player);

extern wGuiObject* (*wVideoCreateTargetImage)(wVideo* player,
											  wVector2i position);

extern wTexture* (*wVideoGetTargetTexture)(wVideo* player);

extern wSound* (*wVideoGetSoundNode)(wVideo* player);

extern void (*wVideoUpdate)(wVideo* player,
							UInt32 timeMs);

extern void (*wVideoPause)(wVideo* player);

extern bool (*wVideoIsPaused)(wVideo* player);

extern bool (*wVideoIsAtEnd)(wVideo* player);

extern bool (*wVideoIsEmpty)(wVideo* player);

extern Int64 (*wVideoGetFramePosition)(wVideo* player);

extern UInt32 (*wVideoGetTimePosition)(wVideo* player);

extern wVector2i (*wVideoGetFrameSize)(wVideo* player);

extern Int32 (*wVideoGetQuality)(wVideo* player);

extern void (*wVideoDestroy)(wVideo* player);

///wDecal///
extern wNode* (*wDecalCreateFromRay)(wTexture* texture,
									 wVector3f startRay,
									 wVector3f endRay,
									 Float32 dimension,
									 Float32 textureRotation,
									 Float32 lifeTime,
									 Float32 visibleDistance);

extern wNode* (*wDecalCreateFromPoint)(wTexture* texture,
									   wVector3f position,
									   wVector3f normal,
									   Float32 dimension,
									   Float32 textureRotation,
									   Float32 lifeTime,
									   Float32 visibleDistance);

extern Float32 (*wDecalGetLifeTime)(wNode* node);

extern void  (*wDecalSetLifeTime)(wNode* node,
								  Float32 lifeTime);

extern Float32 (*wDecalGetMaxVisibleDistance)(wNode* node);

extern void  (*wDecalSetMaxVisibleDistance)(wNode* node,
											Float32 distance);

extern void  (*wDecalSetFadeOutParams)(wNode* node,
									   const bool isfadeOut,
									   Float32 time);

extern wMaterial* (*wDecalGetMaterial)(wNode* decal);

extern void (*wDecalsClear)();///destroy all + disable new

extern void (*wDecalsDestroyAll)();

extern void (*wDecalsCombineAll)();

extern Int32 (*wDecalsGetCount)();

///wNetPacket///
extern wPacket* (*wNetPacketCreate)(UInt64 id,
									bool inOrder,
									bool reliable,
									UInt64 priority);

extern void (*wNetPacketWriteUInt)(wPacket* msg,
								   UInt32 value);

extern void (*wNetPacketWriteInt)(wPacket* msg,
								  Int32 value);

extern void (*wNetPacketWriteFloat)(wPacket* msg,
									Float32 value);

extern void (*wNetPacketWriteString)(wPacket* msg,
									 const char* newString);

extern UInt32 (*wNetPacketReadUint)(Int32 numPacket);

extern Int32 (*wNetPacketReadInt)(Int32 numPacket);

extern Float32 (*wNetPacketReadFloat)(Int32 numPacket);

extern const char* (*wNetPacketReadString)(Int32 numPacket);

extern const char* (*wNetPacketReadMessage)(Int32 numPacket);

extern UInt64 (*wNetPacketGetId)(Int32 numPacket);

extern const char* (*wNetPacketGetClientIp)(Int32 numPacket);

extern void* (*wNetPacketGetClientPtr)(Int32 numPacket);

extern UInt16 (*wNetPacketGetClientPort)(Int32 numPacket);

///wNetManager///
extern  void (*wNetManagerSetVerbose)(bool value);

extern void (*wNetManagerSetMessageId)(UInt64 newId);

extern UInt64 (*wNetManagerGetMessageId)();

extern void (*wNetManagerDestroyAllPackets)();

extern Int32 (*wNetManagerGetPacketsCount)();

///wNetServer///
extern bool (*wNetServerCreate)(UInt16 port,
								Int32 mode,
								Int32 maxClientsCount);

extern void (*wNetServerUpdate)(Int32 sleepMs,
								Int32 countIteration,
								Int32 maxMSecsToWait);

extern void (*wNetServerClear)();

extern void (*wNetServerSendPacket)(void* destPtr,
									wPacket* msg);

extern void (*wNetServerBroadcastMessage)(const char* text);

extern void (*wNetServerAcceptNewConnections)(bool value);

extern void (*wNetServerStop)(Int32 msTime);

extern Int32 (*wNetServerGetClientsCount)();

extern void (*wNetServerKickClient)(void* clientPtr);

extern void (*wNetServerUnKickClient)(void* clientPtr);

extern void (*wNetServerClearBannedList)();

///wNetClient///
extern bool (*wNetClientCreate)(const char* address,
								UInt16 port,
								Int32 mode,
								Int32 maxMSecsToWait);

extern void (*wNetClientUpdate)(Int32 maxMessagesToProcess,
								Int32 countIteration,
								Int32 maxMSecsToWait);

extern void (*wNetClientDisconnect)(Int32 maxMSecsToWait);

extern void (*wNetClientStop)(Int32 maxMSecsToWait);

extern bool (*wNetClientIsConnected)();

extern void (*wNetClientSendMessage)(const char* text);

extern void (*wNetClientSendPacket)(wPacket* msg);

///wPhys///
extern bool (*wPhysStart)();

extern void (*wPhysUpdate)(Float32 timeStep);

extern void (*wPhysStop)();

extern void (*wPhysSetGravity)(wVector3f gravity);

extern void (*wPhysSetWorldSize)(wVector3f size);

extern void (*wPhysSetSolverModel)(wPhysSolverModel model);

extern void (*wPhysSetFrictionModel)(wPhysFrictionModel model);

extern void (*wPhysDestroyAllBodies)();

extern void (*wPhysDestroyAllJoints)();

extern Int32 (*wPhysGetBodiesCount)();

extern Int32 (*wPhysGetJointsCount)();

extern wNode* (*wPhysGetBodyPicked)(wVector2i position,
									bool mouseLeftKey);

extern wNode* (*wPhysGetBodyFromRay)(wVector3f start,
									 wVector3f end);

extern wNode* (*wPhysGetBodyFromScreenCoords)(wVector2i position);

extern wNode* (*wPhysGetBodyByName)(const char* name);

extern wNode* (*wPhysGetBodyById)(Int32 Id);

extern wNode* (*wPhysGetBodyByIndex)(Int32 idx);

extern wNode* (*wPhysGetJointByName)(const char* name);

extern wNode* (*wPhysGetJointById)(Int32 Id);

extern wNode* (*wPhysGetJointByIndex)(Int32 idx);

///wPhysBody///
extern wNode* (*wPhysBodyCreateNull)();

extern wNode* (*wPhysBodyCreateCube)(wVector3f size,
									 Float32 Mass);

extern wNode* (*wPhysBodyCreateSphere)(wVector3f radius,
									   Float32 Mass);

extern wNode* (*wPhysBodyCreateCone)(Float32 radius,
									 Float32 height,
									 Float32 mass,
									 bool Offset);

extern wNode* (*wPhysBodyCreateCylinder)(Float32 radius,
										 Float32 height,
										 Float32 mass,
										 bool Offset);

extern wNode* (*wPhysBodyCreateCapsule)(Float32 radius,
										Float32 height,
										Float32 mass,
										bool Offset);

extern wNode* (*wPhysBodyCreateHull)(wNode* mesh,
									 Float32 mass);

extern wNode* (*wPhysBodyCreateTree)(wNode* mesh);

extern wNode* (*wPhysBodyCreateTreeBsp)(wMesh* mesh,
										wNode* node);

extern wNode* (*wPhysBodyCreateTerrain)(wNode* mesh,
										Int32 LOD);

extern wNode* (*wPhysBodyCreateHeightField)(wNode* mesh);

extern wNode* (*wPhysBodyCreateWaterSurface)(wVector3f size,
											 Float32 FluidDensity,
											 Float32 LinearViscosity,
											 Float32 AngulaViscosity);

extern wNode* (*wPhysBodyCreateCompound)(wNode** nodes,
										 Int32 CountNodes,
										 Float32 mass);

///=> такой функции НЕ ТРЕБУЕТСЯ, так как
///так как здесь работает wNodeDestroy()
///void wPhysBodyDestroy(void* body);

extern void (*wPhysBodySetName)(wNode* body,
								const char* name);

extern const char* (*wPhysBodyGetName)(wNode* body);

extern void (*wPhysBodySetFreeze)(wNode* body,
								  bool freeze);

extern bool (*wPhysBodyIsFreeze)(wNode* body);

extern void (*wPhysBodySetMaterial)(wNode* body,
									Int32 MatId);

extern Int32 (*wPhysBodyGetMaterial)(wNode* body);

extern void (*wPhysBodySetGravity)(wNode* body,
								   wVector3f gravity);

extern wVector3f (*wPhysBodyGetGravity)(wNode* body);

extern void (*wPhysBodySetMass)(wNode* body,
								Float32 NewMass);

extern Float32 (*wPhysBodyGetMass)(wNode* body);

extern void (*wPhysBodySetCenterOfMass)(wNode* body,
										wVector3f center);

extern wVector3f (*wPhysBodyGetCenterOfMass)(wNode* body);

extern void (*wPhysBodySetMomentOfInertia)(wNode* body,
										   wVector3f value);

extern wVector3f (*wPhysBodyGetMomentOfInertia)(wNode* body);

extern void (*wPhysBodySetAutoSleep)(wNode* body,
									 bool value);

extern bool (*wPhysBodyIsAutoSleep)(wNode* body);

extern void (*wPhysBodySetLinearVelocity)(wNode* body,
										  wVector3f velocity);

extern wVector3f (*wPhysBodyGetLinearVelocity)(wNode* body);

extern void (*wPhysBodySetAngularVelocity)(wNode* body,
										   wVector3f velocity);

extern wVector3f (*wPhysBodyGetAngularVelocity)(wNode* body);

extern void (*wPhysBodySetLinearDamping)(wNode* body,
										 Float32 linearDamp);

extern Float32 (*wPhysBodyGetLinearDamping)(wNode* body);

extern void (*wPhysBodySetAngularDamping)(wNode* body,
										  wVector3f damping);

extern wVector3f (*wPhysBodyGetAngularDamping)(wNode* body);

extern void (*wPhysBodyAddImpulse)(wNode* body,
								   wVector3f velosity,
								   wVector3f position);

extern void (*wPhysBodyAddForce)(wNode* body,
								 wVector3f force);

extern void (*wPhysBodyAddTorque)(wNode* body,
								  wVector3f torque);

extern bool (*wPhysBodiesIsCollide)(wNode* body1,
									wNode* body2);

extern wVector3f (*wPhysBodiesGetCollisionPoint)(wNode* body1,
												 wNode* body2);

extern wVector3f (*wPhysBodiesGetCollisionNormal)(wNode* body1,
												  wNode* body2);

extern void (*wPhysBodyDraw)(wNode* body);

///wPhysJoint///
extern wNode* (*wPhysJointCreateBall)(wVector3f position,
									  wVector3f pinDir,
									  wNode* body1,
									  wNode* body2);

extern wNode* (*wPhysJointCreateHinge)(wVector3f position,
									   wVector3f pinDir,
									   wNode* body1,
									   wNode* body2);

extern wNode* (*wPhysJointCreateSlider)(wVector3f position,
										wVector3f pinDir,
										wNode* body1,
										wNode* body2);

extern wNode* (*wPhysJointCreateCorkScrew)(wVector3f position,
										   wVector3f pinDir,
										   wNode* body1,
										   wNode* body2);

extern wNode* (*wPhysJointCreateUpVector)(wVector3f position,
										  wNode* body);

extern void (*wPhysJointSetName)(wNode* joint,
								 const char* name);

extern const char* (*wPhysJointGetName)(wNode* joint);

extern void (*wPhysJointSetCollisionState)(wNode* joint,
										   bool isCollision);

extern bool (*wPhysJointIsCollision)(wNode* Joint);

extern void (*wPhysJointSetBallLimits)(wNode* joint,
									   Float32 MaxConeAngle,
									   wVector2f twistAngles);

extern void (*wPhysJointSetHingeLimits)(wNode* joint,
										wVector2f anglesLimits);

extern void (*wPhysJointSetSliderLimits)(wNode* joint,
										 wVector2f anglesLimits);

extern void (*wPhysJointSetCorkScrewLinearLimits)(wNode* joint,
												  wVector2f distLimits);

extern void (*wPhysJointSetCorkScrewAngularLimits)(wNode* joint,
												   wVector2f distLimits);

///wPhysPlayerController///
extern wNode* (*wPhysPlayerControllerCreate)(wVector3f position,
											 wNode* body,
											 Float32 maxStairStepFactor,
											 Float32 cushion);

extern void (*wPhysPlayerControllerSetVelocity)(wNode* joint,
												Float32 forwardSpeed,
												Float32 sideSpeed,
												Float32 heading);

///wPhysVehicle
extern wNode* (*wPhysVehicleCreate)(Int32 tiresCount,
									wPhysVehicleType rayCastType,
									wNode* CarBody);

extern Int32 (*wPhysVehicleAddTire)(wNode* Car,
									wNode* UserData,
									wPhysVehicleTireType tireType,
									wVector3f position,
									Float32 Mass,
									Float32 Radius,
									Float32 Width,
									Float32 SLenght,
									Float32 SConst,
									Float32 SDamper);

extern Float32 (*wPhysVehicleGetSpeed)(wNode* Car);

extern Int32 (*wPhysVehicleGetTiresCount)(wNode* Car);

extern void (*wPhysVehicleSetBrake)(wNode* Car,
									bool value);

extern bool (*wPhysVehicleIsBrake)(wNode* Car);

extern bool (*wPhysVehicleIsAllTiresCollided)(wNode* Car);

extern void (*wPhysVehicleSetSteering)(wNode* Car,
									   Float32 angle);///-1.......+1

extern Float32 (*wPhysVehicleGetSteering)(wNode* Car);/// return value: -1......+1

extern void (*wPhysVehicleSetTireMaxSteerAngle)(wNode* Car,
												Int32 tireIndex,
												Float32 angleDeg);

extern Float32 (*wPhysVehicleGetTireMaxSteerAngle)(wNode* Car,
												   Int32 tireIndex);

extern wNode* (*wPhysVehicleGetBody)(wNode* Car);

extern void (*wPhysVehicleSetMotorValue)(wNode* Car,
										 Float32 value);

extern Float32 (*wPhysVehicleGetMotorValue)(wNode* Car);

///Vehicle Tires
extern wVector3f (*wPhysVehicleGetTireLocalPosition)(wNode* Car,
													 Int32 tireIndex);

extern Float32 (*wPhysVehicleGetTireUpDownPosition)(wNode* Car,
													Int32 tireIndex);

/*bool wPhysVehicleIsTireOnAir(wNode* Car,
                             Int32 tireIndex);*/

extern Float32 (*wPhysVehicleGetTireAngularVelocity)(wNode* Car,
													 Int32 tireIndex);

extern Float32 (*wPhysVehicleGetTireSpeed)(wNode* Car,
										   Int32 tireIndex);

extern wVector3f (*wPhysVehicleGetTireContactPoint)(wNode* Car,
													Int32 tireIndex);

extern wVector3f (*wPhysVehicleGetTireContactNormal)(wNode* Car,
													 Int32 tireIndex);

extern bool (*wPhysVehicleIsTireBrake)(wNode* Car,
									   Int32 tireIndex);

extern wPhysVehicleTireType (*wPhysVehicleGetTireType)(wNode* Car,
													   Int32 tireIndex);

extern void (*wPhysVehicleSetTireType)(wNode* Car,
									   Int32 tireIndex,
									   wPhysVehicleTireType tireType);

extern void (*wPhysVehicleSetTireBrakeForce)(wNode* Car,
											 Int32 tireIndex,
											 Float32 value);

extern Float32 (*wPhysVehicleGetTireBrakeForce)(wNode* Car,
												Int32 tireIndex);

extern void (*wPhysVehicleSetTireBrakeLateralFriction)(wNode* Car,
													   Int32 tireIndex,
													   Float32 value);

extern Float32 (*wPhysVehicleGetTireBrakeLateralFriction)(wNode* Car,
														  Int32 tireIndex);

extern void (*wPhysVehicleSetTireBrakeLongitudinalFriction)(wNode* Car,
															Int32 tireIndex,
															Float32 value);

extern Float32 (*wPhysVehicleGetTireBrakeLongitudinalFriction)(wNode* Car,
															   Int32 tireIndex);

extern void (*wPhysVehicleSetTireLateralFriction)(wNode* Car,
												  Int32 tireIndex,
												  Float32 value);

extern Float32 (*wPhysVehicleGetTireLateralFriction)(wNode* Car,
													 Int32 tireIndex);

extern void (*wPhysVehicleSetTireLongitudinalFriction)(wNode* Car,
													   Int32 tireIndex,
													   Float32 value);

extern Float32 (*wPhysVehicleGetTireLongitudinalFriction)(wNode* Car,
														  Int32 tireIndex);

extern void (*wPhysVehicleSetTireMass)(wNode* Car,
									   Int32 tireIndex,
									   Float32 mass);

extern Float32 (*wPhysVehicleGetTireMass)(wNode* Car,
										  Int32 tireIndex);

extern void (*wPhysVehicleSetTireRadius)(wNode* Car,
										 Int32 tireIndex,
										 Float32 radius);

extern Float32 (*wPhysVehicleGetTireRadius)(wNode* Car,
											Int32 tireIndex);

extern void (*wPhysVehicleSetTireWidth)(wNode* Car,
										Int32 tireIndex,
										Float32 width);

extern Float32 (*wPhysVehicleGetTireWidth)(wNode* Car,
										   Int32 tireIndex);

extern void  (*wPhysVehicleSetTireSpringConst)(wNode* Car,
											   Int32 tireIndex,
											   Float32 value);

extern Float32 (*wPhysVehicleGetTireSpringConst)(wNode* Car,
												 Int32 tireIndex);

extern void (*wPhysVehicleSetTireSpringDamper)(wNode* Car,
											   Int32 tireIndex,
											   Float32 value);

extern Float32 (*wPhysVehicleGetTireSpringDamper)(wNode* Car,
												  Int32 tireIndex);

extern void (*wPhysVehicleSetTireSuspensionLenght)(wNode* Car,
												   Int32 tireIndex,
												   Float32 value);

extern Float32 (*wPhysVehicleGetTireSuspensionLenght)(wNode* Car,
													  Int32 tireIndex);

extern void (*wPhysVehicleSetTireUserData)(wNode* Car,
										   Int32 tireIndex,
										   wNode* userData);

extern wNode* (*wPhysVehicleGetTireUserData)(wNode* Car,
											 Int32 tireIndex);

extern void (*wPhysVehicleSetTireMotorForce)(wNode* Car,
											 Int32 tireIndex,
											 Float32 value);

extern Float32 (*wPhysVehicleGetTireMotorForce)(wNode* Car,
												Int32 tireIndex);

extern void (*wPhysVehicleSetTireTurnForceHelper)(wNode* Car,
												  Int32 tireIndex,
												  Float32 value);

extern Float32 (*wPhysVehicleGetTireTurnForceHelper)(wNode* Car,
													 Int32 tireIndex);
extern void (*wPhysVehicleSetTireSpinTorqueFactor)(wNode* Car,
												   Int32 tireIndex,
												   Float32 value);

extern Float32 (*wPhysVehicleGetTireSpinTorqueFactor)(wNode* Car,
													  Int32 tireIndex);

extern void (*wPhysVehicleSetTireTorquePosition)(wNode* Car,
												 Int32 tireIndex,
												 wVector3f position);

extern wVector3f  (*wPhysVehicleGetTireTorquePosition)(wNode* Car,
													   Int32 tireIndex);

extern Float32 (*wPhysVehicleGetTireLoad)(wNode* Car,
										  Int32 tireIndex);

extern void (*wPhysVehicleSetTireSpinForce)(wNode* Car,
											Int32 tireIndex,
											Float32 value);

extern Float32 (*wPhysVehicleGetTireSpinForce)(wNode* Car,
											   Int32 tireIndex);

/*///wPhysRagDoll
UInt32* wPhysRagDollCreate(wNode* node,
                           Int32 bonesCount,
                           wPhysRagDollBoneParameters* params);

Int32 wPhysRagDollGetBonesCount(wNode* ragdoll);

void wPhysRagDollSetBoneCollisionState(wNode* ragdoll,
                                       Int32 boneIndex,
                                       Int32 state);

void wPhysRagDollSetBoneConeLimits(wNode* ragdoll,
                                   Int32 boneIndex,
                                   Float32 angle);

void wPhysRagDollSetBoneTwistLimits(wNode* ragdoll,
                                    Int32 boneIndex,
                                    Float32 minAngle,
                                    Float32 maxAngle);

void wPhysRagDollDraw(wNode* ragdoll);*/

///wPhysMaterial///
extern int	 (*wPhysMaterialCreate)();

extern void (*wPhysMaterialSetElasticity)(Int32 matId1,
										  Int32 matId2,
										  Float32 Elasticity);

extern void (*wPhysMaterialSetFriction)(Int32 matId1,
										Int32 matId2,
										Float32 externFriction,
										Float32 KineticFriction);

extern void (*wPhysMaterialSetContactSound)(Int32 matId1,
											Int32 matId2,
											wSound* soundNode);

extern void  (*wPhysMaterialSetSoftness)(Int32 matId1,
										 Int32 matId2,
										 Float32 Softness);

extern void (*wPhysMaterialSetCollidable)(Int32 matId1,
										  Int32 matId2,
										  bool isCollidable);

///wGui///
extern void (*wGuiDrawAll)();

extern void (*wGuiDestroyAll)();

extern bool (*wGuiIsEventAvailable)();

extern wGuiEvent* (*wGuiReadEvent)();

extern bool (*wGuiLoad)(const char* fileName,
						wGuiObject* start);

extern bool (*wGuiSave)(const char* fileName,
						wGuiObject* start);

extern wGuiObject* (*wGuiGetSkin)();

extern void (*wGuiSetSkin)(wGuiObject* skin);

extern const wchar_t* (*wGuiGetLastSelectedFile)();

///Returns the element which holds the focus.
extern wGuiObject* (*wGuiGetObjectFocused)();

///Returns the element which was last under the mouse cursor.
extern wGuiObject* (*wGuiGetObjectHovered)();

extern wGuiObject* (*wGuiGetRootNode)();

extern wGuiObject* (*wGuiGetObjectById)(Int32 id,
										bool searchchildren);

extern wGuiObject* (*wGuiGetObjectByName)(const char* name,
										  bool searchchildren);

///wGuiObject///
extern void (*wGuiObjectDestroy)(wGuiObject* element );

extern void (*wGuiObjectSetParent)(wGuiObject* element,
								   wGuiObject* parent);

extern wGuiObject* (*wGuiObjectGetParent)(wGuiObject* element);

extern void (*wGuiObjectSetRelativePosition)(wGuiObject* element,
											 wVector2i position);

extern void (*wGuiObjectSetRelativeSize)(wGuiObject* element,
										 wVector2i size);

extern wVector2i (*wGuiObjectGetRelativePosition)(wGuiObject* element);

extern wVector2i (*wGuiObjectGetRelativeSize)(wGuiObject* element);

extern wVector2i (*wGuiObjectGetAbsolutePosition)(wGuiObject* element);

extern wVector2i (*wGuiObjectGetAbsoluteClippedPosition)(wGuiObject* element);

extern wVector2i (*wGuiObjectGetAbsoluteClippedSize)(wGuiObject* element);

///Sets whether the element will ignore its parent's clipping rectangle.
extern void (*wGuiObjectSetClippingMode)(wGuiObject* element,
										 bool value);

extern bool (*wGuiObjectIsClipped)(wGuiObject* element);

extern void (*wGuiObjectSetMaxSize)(wGuiObject* element,
									wVector2i size);

extern void (*wGuiObjectSetMinSize)(wGuiObject* element,
									wVector2i size);

extern void (*wGuiObjectSetAlignment)(wGuiObject* element,
									  wGuiAlignment left,
									  wGuiAlignment right,
									  wGuiAlignment top,
									  wGuiAlignment bottom);

extern void (*wGuiObjectUpdateAbsolutePosition)(wGuiObject* element);

///Возвращает гуи-объект -потомок element-а, который находится на пересечении
///с точкой экрана position
///Если нужен любой объект, то в качестве element-а нужно
///поставить root=wGuiGetRootNode()
///Примечание: Элемент root имеет размер ВСЕГО экрана
extern wGuiObject* (*wGuiObjectGetFromScreenPos)(wGuiObject* element,
												 wVector2i position);

///Персекается ли объект с точкой экрана position
extern bool (*wGuiObjectIsPointInside)(wGuiObject* element,
									   wVector2i position);

extern void (*wGuiObjectDestroyChild)(wGuiObject* element,
									  wGuiObject* child);

///Можно вызывать вместо wGuiDrawAll() для конкретного элемента
extern void (*wGuiObjectDraw)(wGuiObject* element);

extern void (*wGuiObjectMoveTo)(wGuiObject* element,
								wVector2i position);

extern void (*wGuiObjectSetVisible)(wGuiObject* element,
									bool value);

extern bool (*wGuiObjectIsVisible)(wGuiObject* element);

/// Устанавливает, был ли этот элемент управления создан
/// как часть родительского элемента.
/// Например, если полоса прокрутки является частью списка.
/// Подразделы не сохраняются на диск при вызове wGuiSave()
extern void (*wGuiObjectSetSubObject)(wGuiObject* element,
									  bool value);

/// Вовзращает, был ли этот элемент управления создан
/// как часть родительского элемента.
extern bool (*wGuiObjectIsSubObject)(wGuiObject* element);

extern void (*wGuiObjectSetTabStop)(wGuiObject* element,
									bool value);

///Returns true if this element can be focused by navigating with the tab key.
extern bool (*wGuiObjectIsTabStop)(wGuiObject* element);

///Sets the priority of focus when using the tab key to navigate between a group of elements.
extern void (*wGuiObjectSetTabOrder)(wGuiObject* element,
									 Int32 index);

extern Int32 (*wGuiObjectGetTabOrder)(wGuiObject* element);

///If set to true, the focus will visit this element when using the tab key to cycle through elements.
extern void (*wGuiObjectSetTabGroup)(wGuiObject* element,
									 bool value);

extern bool (*wGuiObjectIsTabGroup)(wGuiObject* element);

extern void (*wGuiObjectSetEnable)(wGuiObject* element,
								   bool value);

extern bool (*wGuiObjectIsEnabled)(wGuiObject* element);

extern void (*wGuiObjectSetText)(wGuiObject* element,
								 const wchar_t* text);

extern const wchar_t* (*wGuiObjectGetText)(wGuiObject* element);

///Sets the new caption of this element.
extern void (*wGuiObjectSetToolTipText)(wGuiObject* element,
										const wchar_t* text);

extern const wchar_t* (*wGuiObjectGetToolTipText)(wGuiObject* element);

extern void (*wGuiObjectSetId)(wGuiObject* element,
							   Int32 id);

extern Int32 (*wGuiObjectGetId)(wGuiObject* element);

extern void (*wGuiObjectSetName)(wGuiObject* element,
								 const char* name);

extern bool (*wGuiObjectIsHovered)(wGuiObject* el);

extern const char* (*wGuiObjectGetName)(wGuiObject* element);

///Ищет  среди "детей" объекта искомого по его Id
///Если требуется найти ЛЮБОЙ ГУИ-объект сцены,
///нужно в качестве элемента указать root=wGuiGetRootNode()
extern wGuiObject* (*wGuiObjectGetChildById)(wGuiObject* element,
											 Int32 id,
											 bool searchchildren);

extern wGuiObject* (*wGuiObjectGetChildByName)(wGuiObject* element,
											   const char* name,
											   bool searchchildren);

extern bool (*wGuiObjectIsChildOf)(wGuiObject* element,
								   wGuiObject* child);

extern bool (*wGuiObjectBringToFront)(wGuiObject* element,
									  wGuiObject* subElement);

extern wGuiElementType (*wGuiObjectGetType)(wGuiObject* element);

extern const char* (*wGuiObjectGetTypeName)(wGuiObject* element);

extern bool (*wGuiObjectHasType)(wGuiObject* element,
								 wGuiElementType type);

extern bool (*wGuiObjectSetFocus)(wGuiObject* element);

extern bool (*wGuiObjectRemoveFocus)(wGuiObject* element);

extern bool (*wGuiObjectIsFocused)(wGuiObject* element);

extern void (*wGuiObjectReadFromXml)(wGuiObject* node,
									 wXmlReader* reader);

extern void (*wGuiObjectWriteToXml)(wGuiObject* node,
									wXmlWriter* writer);

///wGuiSkin///
extern wGuiObject* (*wGuiSkinCreate)(wGuiSkinSpace type);

extern wColor4s (*wGuiSkinGetColor)(wGuiObject* skin,
									wGuiDefaultColor elementType);

extern void (*wGuiSkinSetColor)(wGuiObject* skin,
								wGuiDefaultColor elementType,
								wColor4s color);

extern void (*wGuiSkinSetSize)(wGuiObject* skin,
							   wGuiDefaultSize sizeType,
							   Int32 newSize);

extern Int32 (*wGuiSkinGetSize)(wGuiObject* skin,
								wGuiDefaultSize sizeType);

extern const wchar_t* (*wGuiSkinGetDefaultText)(wGuiObject* skin,
												wGuiDefaultText txt);

extern void (*wGuiSkinSetDefaultText)(wGuiObject* skin,
									  wGuiDefaultText txt,
									  const wchar_t* newText);

extern void (*wGuiSkinSetFont)(wGuiObject* skin,
							   wFont* font,
							   wGuiDefaultFont fntType);

extern wFont* (*wGuiSkinGetFont)(wGuiObject* skin,
								 wGuiDefaultFont fntType);

extern void (*wGuiSkinSetSpriteBank)(wGuiObject* skin,
									 wGuiObject* bank);

extern wGuiObject* (*wGuiSkinGetSpriteBank)(wGuiObject* skin);

extern void (*wGuiSkinSetIcon)(wGuiObject* skin,
							   wGuiDefaultIcon icn,
							   UInt32 index);

extern UInt32 (*wGuiSkinGetIcon)(wGuiObject* skin,
								 wGuiDefaultIcon icn);

extern wGuiSkinSpace (*wGuiSkinGetType)(wGuiObject* skin);

///wGuiWindow///
extern wGuiObject* (*wGuiWindowCreate)(const wchar_t* wcptrTitle,
									   wVector2i minPos,
									   wVector2i maxPos,
									   bool modal);

extern wGuiObject* (*wGuiWindowGetButtonClose)(wGuiObject* win);

extern wGuiObject* (*wGuiWindowGetButtonMinimize)(wGuiObject* win);

extern wGuiObject* (*wGuiWindowGetButtonMaximize)(wGuiObject* win);

extern void (*wGuiWindowSetDraggable)(wGuiObject* win,
									  bool value);

extern bool (*wGuiWindowIsDraggable)(wGuiObject* win);

extern void (*wGuiWindowSetDrawBackground)(wGuiObject* win,
										   bool value);

extern bool (*wGuiWindowIsDrawBackground)(wGuiObject* win);

extern void (*wGuiWindowSetDrawTitleBar)(wGuiObject* win,
										 bool value);

extern bool (*wGuiWindowIsDrawTitleBar)(wGuiObject* win);

///wGuiLabel
extern wGuiObject* (*wGuiLabelCreate)(const wchar_t * wcptrText,
									  wVector2i minPos,
									  wVector2i maxPos,
									  bool boBorder,
									  bool boWordWrap);

extern wVector2i (*wGuiLabelGetTextSize)(wGuiObject* txt);

extern void (*wGuiLabelSetOverrideFont)(wGuiObject* obj,
										wFont* font);

extern wFont* (*wGuiLabelGetOverrideFont)(wGuiObject* obj);

extern wFont* (*wGuiLabelGetActiveFont)(wGuiObject* obj);

extern void (*wGuiLabelEnableOverrideColor)(wGuiObject* obj,
											bool value);

extern bool (*wGuiLabelIsOverrideColor)(wGuiObject* obj);

extern void (*wGuiLabelSetOverrideColor)(wGuiObject* obj,
										 wColor4s color);

extern wColor4s (*wGuiLabelGetOverrideColor)(wGuiObject* obj);

extern void (*wGuiLabelSetDrawBackground)(wGuiObject* obj,
										  bool value);

extern bool (*wGuiLabelIsDrawBackGround)(wGuiObject* obj);

extern void (*wGuiLabelSetDrawBorder)(wGuiObject* obj,
									  bool value);

extern bool (*wGuiLabelIsDrawBorder)(wGuiObject* obj) ;

extern void (*wGuiLabelSetTextAlignment)(wGuiObject* obj,
										 wGuiAlignment Horizontalvalue,
										 wGuiAlignment Verticalvalue);

extern void (*wGuiLabelSetWordWrap)(wGuiObject* obj,
									bool value);

extern bool (*wGuiLabelIsWordWrap)(wGuiObject* obj);

extern void (*wGuiLabelSetBackgroundColor)(wGuiObject* obj,
										   wColor4s color);

extern wColor4s (*wGuiLabelGetBackgroundColor)(wGuiObject* obj);

///wGuiButton
extern wGuiObject* (*wGuiButtonCreate)(wVector2i minPos,
									   wVector2i maxPos,
									   const wchar_t* wcptrLabel,
									   const wchar_t* wcptrTip);

extern void (*wGuiButtonSetImage)(wGuiObject* btn,
								  wTexture* img);

extern void (*wGuiButtonSetImageFromRect)(wGuiObject* btn,
										  wTexture* img,
										  wVector2i* minRect,
										  wVector2i* maxRect);

extern void (*wGuiButtonSetPressedImage)(wGuiObject* btn,
										 wTexture* img);


extern void (*wGuiButtonSetPressedImageFromRect)(wGuiObject* btn,
												 wTexture* img,
												 wVector2i* minRect,
												 wVector2i* maxRect);

extern void (*wGuiButtonSetSpriteBank)(wGuiObject* btn,
									   wGuiObject* bank);

extern void (*wGuiButtonSetSprite)(wGuiObject* btn,
								   wGuiButtonState state,
								   Int32 index,
								   wColor4s color,
								   bool loop);

extern void (*wGuiButtonSetPush)(wGuiObject* btn,
								 bool value);

extern bool (*wGuiButtonIsPushed)(wGuiObject* btn);

extern void (*wGuiButtonSetPressed)(wGuiObject* btn,
									bool value);

extern bool (*wGuiButtonIsPressed)(wGuiObject* btn);

extern void (*wGuiButtonUseAlphaChannel)(wGuiObject* btn,
										 bool value);

extern bool (*wGuiButtonIsUsedAlphaChannel)(wGuiObject* btn);

extern void (*wGuiButtonEnableScaleImage)(wGuiObject* btn,
										  bool value);

extern bool (*wGuiButtonIsScaledImage)(wGuiObject* btn);

extern void (*wGuiButtonSetOverrideFont)(wGuiObject* obj,
										 wFont* font);

extern wFont* (*wGuiButtonGetOverrideFont)(wGuiObject* obj);

extern wFont* (*wGuiButtonGetActiveFont)(wGuiObject* obj);

extern void (*wGuiButtonSetDrawBorder)(wGuiObject* obj,
									   bool value);

extern bool (*wGuiButtonIsDrawBorder)(wGuiObject* obj);

///wGuiButtonGroup///
extern wGuiObject* (*wGuiButtonGroupCreate)(wVector2i minPos,
											wVector2i maxPos);

extern Int32 (*wGuiButtonGroupAddButton)(wGuiObject* group,
										 wGuiObject* button);

extern Int32 (*wGuiButtonGroupInsertButton)(wGuiObject* group,
											wGuiObject* button,
											UInt32 index);

extern wGuiObject* (*wGuiButtonGroupGetButton)(wGuiObject* group,
											   UInt32 index);

extern bool (*wGuiButtonGroupRemoveButton)(wGuiObject* group,
										   UInt32 index);

extern void (*wGuiButtonGroupRemoveAll)(wGuiObject* group);

extern UInt32 (*wGuiButtonGroupGetSize)(wGuiObject* group);

extern Int32 (*wGuiButtonGroupGetSelectedIndex)(wGuiObject* group);

extern void (*wGuiButtonGroupSetSelectedIndex)(wGuiObject* group,
											   Int32 index);

extern void (*wGuiButtonGroupClearSelection)(wGuiObject* group);

extern void (*wGuiButtonGroupSetBackgroundColor)(wGuiObject* group,
												 wColor4s color);

extern void (*wGuiButtonGroupDrawBackground)(wGuiObject* group,
											 bool value);
///wGuiListBox///
extern wGuiObject* (*wGuiListBoxCreate)(wVector2i minPos,
										wVector2i maxPos,
										bool background);

extern UInt32 (*wGuiListBoxGetItemsCount)(wGuiObject* lbox);

extern const wchar_t* (*wGuiListBoxGetItemByIndex)(wGuiObject* lbox,
												   UInt32 id);

extern UInt32 (*wGuiListBoxAddItem)(wGuiObject* lbox,
									const wchar_t* text);

extern UInt32 (*wGuiListBoxAddItemWithIcon)(wGuiObject* lbox,
											const wchar_t* text,
											Int32 icon);

extern void (*wGuiListBoxRemoveItem)(wGuiObject* lbox,
									 UInt32 index);

extern void (*wGuiListBoxRemoveAll)(wGuiObject* lbox);

extern void (*wGuiListBoxSetItem)(wGuiObject* lbox,
								  UInt32 index,
								  const wchar_t* text,
								  Int32 icon);

extern void (*wGuiListBoxInsertItem)(wGuiObject* lbox,
									 UInt32 index,
									 const wchar_t* text,
									 Int32 icon);

extern Int32 (*wGuiListBoxGetItemIcon)(wGuiObject* lbox,
									   UInt32 index);

extern UInt32 (*wGuiListBoxGetSelectedIndex)(wGuiObject* lbox);

extern void (*wGuiListBoxSelectItemByIndex)(wGuiObject* lbox,
											UInt32 index);

extern void (*wGuiListBoxSelectItemByText)(wGuiObject* lbox,
										   const wchar_t* item);

extern void (*wGuiListBoxSwapItems)(wGuiObject* lbox,
									UInt32 index1,
									UInt32 index2);

extern void (*wGuiListBoxSetItemsHeight)(wGuiObject* lbox,
										 Int32 height);

extern void (*wGuiListBoxSetAutoScrolling)(wGuiObject* lbox,
										   bool scroll);

extern bool (*wGuiListBoxIsAutoScrolling)(wGuiObject* lbox);

extern void (*wGuiListBoxSetItemColor)(wGuiObject* lbox,
									   UInt32 index,
									   wColor4s color);

extern void (*wGuiListBoxSetElementColor)(wGuiObject* lbox,
										  UInt32 index,
										  wGuiListboxColor colorType,
										  wColor4s color);

extern void (*wGuiListBoxClearItemColor)(wGuiObject* lbox,
										 UInt32 index);

extern void (*wListBoxClearElementColor)(wGuiObject* lbox,
										 UInt32 index,
										 wGuiListboxColor colorType);

extern wColor4s (*wGuiListBoxGetElementColor)(wGuiObject* lbox,
											  UInt32 index,
											  wGuiListboxColor colorType);

extern bool (*wGuiListBoxHasElementColor)(wGuiObject* lbox,
										  UInt32 index,
										  wGuiListboxColor colorType);

extern wColor4s (*wGuiListBoxGetDefaultColor)(wGuiObject* lbox,
											  wGuiListboxColor colorType);

extern void (*wGuiListBoxSetDrawBackground)(wGuiObject* obj,
											bool value);

///wGuiScrollBar
extern wGuiObject* (*wGuiScrollBarCreate)(bool Horizontal,
										  wVector2i minPos,
										  wVector2i maxPos);

extern void (*wGuiScrollBarSetMaxValue)(wGuiObject* scroll,
										Int32 max);

extern Int32 (*wGuiScrollBarGetMaxValue)(wGuiObject* scroll);

extern void (*wGuiScrollBarSetMinValue)(wGuiObject* scroll,
										Int32 min);

extern Int32 (*wGuiScrollBarGetMinValue)(wGuiObject* scroll);

extern void (*wGuiScrollBarSetValue)(wGuiObject* scroll,
									 Int32 value);

extern Int32 (*wGuiScrollBarGetValue)(wGuiObject* scroll);

extern void (*wGuiScrollBarSetSmallStep)(wGuiObject* scroll,
										 Int32 step);

extern Int32 (*wGuiScrollBarGetSmallStep)(wGuiObject* scroll);

extern void (*wGuiScrollBarSetLargeStep)(wGuiObject* scroll,
										 Int32 step);

extern Int32 (*wGuiScrollBarGetLargeStep)(wGuiObject* scroll);

///wGuiEditBox
extern wGuiObject* (*wGuiEditBoxCreate)(const wchar_t* wcptrText,
										wVector2i minPos,
										wVector2i maxPos);

extern void (*wGuiEditBoxSetMultiLine)(wGuiObject* box,
									   bool value);

extern bool (*wGuiEditBoxIsMultiLine)(wGuiObject* box);

extern void (*wGuiEditBoxSetAutoScrolling)(wGuiObject* box,
										   bool value);

extern bool (*wGuiEditBoxIsAutoScrolling)(wGuiObject* box);

extern void (*wGuiEditBoxSetPasswordMode)(wGuiObject* box,
										  bool value);

extern bool (*wGuiEditBoxIsPasswordMode)(wGuiObject* box);

extern wVector2i (*wGuiEditBoxGetTextSize)(wGuiObject* box);

///Sets the maximum amount of characters which may be entered in the box.
extern void (*wGuiEditBoxSetCharactersLimit)(wGuiObject* box,
											 UInt32 max);

extern UInt32 (*wGuiEditGetCharactersLimit)(wGuiObject* box);

extern void (*wGuiEditBoxSetOverrideFont)(wGuiObject* obj,
										  wFont* font);

extern wFont* (*wGuiEditBoxGetOverrideFont)(wGuiObject* obj);

extern wFont* (*wGuiEditBoxGetActiveFont)(wGuiObject* obj);

extern void (*wGuiEditBoxEnableOverrideColor)(wGuiObject* obj,
											  bool value);

extern bool (*wGuiEditBoxIsOverrideColor)(wGuiObject* obj);

extern void (*wGuiEditBoxSetOverrideColor)(wGuiObject* obj,
										   wColor4s color);

extern wColor4s (*wGuiEditBoxGetOverrideColor)(wGuiObject* obj);

extern void (*wGuiEditBoxSetDrawBackground)(wGuiObject* obj,
											bool value);

extern void (*wGuiEditBoxSetDrawBorder)(wGuiObject* obj,
										bool value);

extern bool (*wGuiEditBoxIsDrawBorder)(wGuiObject* obj);

extern void (*wGuiEditBoxSetTextAlignment)(wGuiObject* obj,
										   wGuiAlignment Horizontalvalue,
										   wGuiAlignment Verticalvalue);

extern void (*wGuiEditBoxSetWordWrap)(wGuiObject* obj,
									  bool value);

extern bool (*wGuiEditBoxIsWordWrap)(wGuiObject* obj);

///wGuiImage///
extern wGuiObject* (*wGuiImageCreate)(wTexture* texture,
									  wVector2i size,
									  bool useAlpha);

extern void (*wGuiImageSet)(wGuiObject* img,
							wTexture* tex);

extern wTexture* (*wGuiImageGet)(wGuiObject* img);

extern void (*wGuiImageSetColor)(wGuiObject* img,
								 wColor4s color);

extern wColor4s (*wGuiImageGetColor)(wGuiObject* img);

extern void (*wGuiImageSetScaling)(wGuiObject* img,
								   bool scale);

extern bool (*wGuiImageIsScaled)(wGuiObject* img);

extern void (*wGuiImageUseAlphaChannel)(wGuiObject* img,
										bool use);

extern bool (*wGuiImageIsUsedAlphaChannel)(wGuiObject* img);

///wGuiFader///
extern wGuiObject* (*wGuiFaderCreate)(wVector2i minPos,
									  wVector2i maxPos);

extern void (*wGuiFaderSetColor)(wGuiObject* fader,
								 wColor4s color);

extern wColor4s (*wGuiFaderGetColor)(wGuiObject* fader);

extern void (*wGuiFaderSetColorExt)(wGuiObject* fader,
									wColor4s colorSrc,
									wColor4s colorDest);

extern void (*wGuiFaderFadeIn)(wGuiObject* fader,
							   UInt32 timeMs);

extern void (*wGuiFaderFadeOut)(wGuiObject* fader,
								UInt32 timeMs);

extern bool (*wGuiFaderIsReady)(wGuiObject* fader);

///wGuiCheckBox///
extern wGuiObject* (*wGuiCheckBoxCreate)(const wchar_t* wcptrText,
										 wVector2i minPos,
										 wVector2i maxPos,
										 bool checked);

extern void (*wGuiCheckBoxCheck)(wGuiObject* box,
								 bool checked);

extern bool (*wGuiCheckBoxIsChecked)(wGuiObject* box);

/// Sets whether to draw the background
extern void (*wGuiCheckBoxSetDrawBackground)(wGuiObject* box,
											 bool value);

/// Checks if background drawing is enabled
///return true if background drawing is enabled, false otherwise
extern bool (*wGuiCheckBoxIsDrawBackground)(wGuiObject* box);

/// Sets whether to draw the border
extern void (*wGuiCheckBoxSetDrawBorder)(wGuiObject* box,
										 bool value);

/// Checks if border drawing is enabled
///return true if border drawing is enabled, false otherwise
extern bool (*wGuiCheckBoxIsDrawBorder)(wGuiObject* box);

extern void (*wGuiCheckBoxSetFilled)(wGuiObject* box,
									 bool value);

extern bool (*wGuiCheckBoxIsFilled)(wGuiObject* box);

///wGuiFileOpenDialog
/*Warning:
    When the user selects a folder this does change the current working directory

This element can create the following events of type wGuiEventType:

        wGET_DIRECTORY_SELECTED
        wGET_FILE_SELECTED
        wGET_FILE_CHOOSE_DIALOG_CANCELLED
*/
extern wGuiObject* (*wGuiFileOpenDialogCreate)(const wchar_t* wcptrLabel,
											   bool modal);

///Returns the filename of the selected file. Returns NULL, if no file was selected.
extern const wchar_t* (*wGuiFileOpenDialogGetFile)(wGuiObject* dialog);

///Returns the directory of the selected file. Returns NULL, if no directory was selected.
extern const char* (*wGuiFileOpenDialogGetDirectory)(wGuiObject* dialog);

///wGuiComboBox///
extern wGuiObject* (*wGuiComboBoxCreate)(wVector2i minPos,
										 wVector2i maxPos);

extern UInt32 (*wGuiComboBoxGetItemsCount)(wGuiObject* combo);

extern const wchar_t* (*wGuiComboBoxGetItemByIndex)(wGuiObject* combo,
													UInt32 idx);

extern UInt32 (*wGuiComboBoxGetItemDataByIndex)(wGuiObject* combo,
												UInt32 idx);

extern Int32 (*wGuiComboBoxGetIndexByItemData)(wGuiObject* combo,
											   UInt32 data);

extern UInt32 (*wGuiComboBoxAddItem)(wGuiObject* combo,
									 const wchar_t* text,
									 UInt32 data);

extern void (*wGuiComboBoxRemoveItem)(wGuiObject* combo,
									  UInt32 idx);

extern void (*wGuiComboBoxRemoveAll)(wGuiObject* combo);

///Returns id of selected item. returns -1 if no item is selected.
extern Int32 (*wGuiComboBoxGetSelected)(wGuiObject* combo);

extern void (*wGuiComboBoxSetSelected)(wGuiObject* combo,
									   UInt32 idx);

extern void (*wGuiComboBoxSetMaxSelectionRows)(wGuiObject* combo,
											   UInt32 max);

extern UInt32 (*wGuiComboBoxGetMaxSelectionRows)(wGuiObject* combo);

extern void (*wGuiComboBoxSetTextAlignment)(wGuiObject* obj,
											wGuiAlignment Horizontalvalue,
											wGuiAlignment Verticalvalue);

///wGuiContextMenu///
extern wGuiObject* (*wGuiContextMenuCreate)(wVector2i minPos,
											wVector2i maxPos);

extern void (*wGuiContextMenuSetCloseHandling)(wGuiObject* cmenu,
											   wContextMenuClose onClose);

extern wContextMenuClose (*wGuiContextMenuGetCloseHandling)(wGuiObject* cmenu);

extern UInt32 (*wGuiContextMenuGetItemsCount)(wGuiObject* cmenu);

extern UInt32 (*wGuiContextMenuAddItem)(wGuiObject* cmenu,
										const wchar_t* text,
										Int32 commandId,
										bool enabled,
										bool hasSubMenu,
										bool checked,
										bool autoChecking);

extern UInt32 (*wGuiContextMenuInsertItem)(wGuiObject* cmenu,
										   UInt32 idx,
										   const wchar_t* text,
										   Int32 commandId,
										   bool enabled,
										   bool hasSubMenu,
										   bool checked,
										   bool autoChecking);

extern void (*wGuiContextMenuAddSeparator)(wGuiObject* cmenu);

extern const wchar_t* (*wGuiContextMenuGetItemText)(wGuiObject* cmenu,
													UInt32 idx);

extern void (*wGuiContextMenuSetItemText)(wGuiObject* cmenu,
										  UInt32 idx,
										  const wchar_t* text);

extern void (*wGuiContextMenuSetItemEnabled)(wGuiObject* cmenu,
											 UInt32 idx,
											 bool value);

extern bool (*wGuiContextMenuIsItemEnabled)(wGuiObject* cmenu,
											UInt32 idx);

extern void (*wGuiContextMenuSetItemChecked)(wGuiObject* cmenu,
											 UInt32 idx,
											 bool value);

extern bool (*wGuiContextMenuIsItemChecked)(wGuiObject* cmenu,
											UInt32 idx);

extern void (*wGuiContextMenuRemoveItem)(wGuiObject* cmenu,
										 UInt32 idx);

extern void (*wGuiContextMenuRemoveAll)(wGuiObject* cmenu);

extern Int32 (*wGuiContextMenuGetSelectedItem)(wGuiObject* cmenu);

extern Int32 (*wGuiContextMenuGetItemCommandId)(wGuiObject* cmenu,
												UInt32 idx);

extern Int32 (*wGuiContextMenuFindItem)(wGuiObject* cmenu,
										Int32 id,
										UInt32 idx);

extern void (*wGuiContextMenuSetItemCommandId)(wGuiObject* cmenu,
											   UInt32 idx,
											   Int32 id);

extern wGuiObject* (*wGuiContextMenuGetSubMenu)(wGuiObject* cmenu,
												UInt32 idx);

extern void (*wGuiContextMenuSetAutoChecking)(wGuiObject* cmenu,
											  UInt32 idx,
											  bool autoChecking);

extern bool (*wGuiContextMenuIsAutoChecked)(wGuiObject* cmenu,
											UInt32 idx);

///When an eventparent is set it receives events instead of the usual parent element.
extern void (*wGuiContextMenuSetEventParent)(wGuiObject* cmenu,
											 wGuiObject* parent);

///wGuiMenu///
///Adds a menu to the environment.This is like the menu you can find on top of most windows in modern graphical user interfaces.
///Для работы с меню подходят все команды
///из раздела wGuiContextMenu///
extern wGuiObject* (*wGuiMenuCreate)();

///wGuiModalScreen///
///Adds a modal screen.
///This control stops its parent's members from being able to receive input until its last child is removed,
/// it then deletes itself.
extern wGuiObject* (*wGuiModalScreenCreate)();

///wGuiSpinBox///
extern wGuiObject* (*wGuiSpinBoxCreate)(const wchar_t * wcptrText,
										wVector2i minPos,
										wVector2i maxPos,
										bool border);

extern wGuiObject* (*wGuiSpinBoxGetEditBox)(wGuiObject* box);

extern void (*wGuiSpinBoxSetValue)(wGuiObject* spin,
								   Float32 value);

extern Float32 (*wGuiSpinBoxGetValue)(wGuiObject* spin);

extern void (*wGuiSpinBoxSetRange)(wGuiObject* spin,
								   wVector2f range);

extern Float32 (*wGuiSpinBoxGetMin)(wGuiObject* spin);

extern Float32 (*wGuiSpinBoxGetMax)(wGuiObject* spin);

extern void (*wGuiSpinBoxSetStepSize)(wGuiObject* spin,
									  Float32 step);

extern Float32 (*wGuiSpinBoxGetStepSize)(wGuiObject* spin);

extern void (*wGuiSpinBoxSetDecimalPlaces)(wGuiObject* spin,
										   Int32 places);

///wGuiTab///
extern wGuiObject* (*wGuiTabCreate)(wVector2i minPos,
									wVector2i maxPos );

extern Int32 (*wGuiTabGetNumber)(wGuiObject* tab);

extern void (*wGuiTabSetTextColor)(wGuiObject* tab,
								   wColor4s color);

extern wColor4s (*wGuiTabGetTextColor)(wGuiObject* tab);

extern void (*wGuiTabSetDrawBackground)(wGuiObject* tab,
										bool value);

extern bool (*wGuiTabIsDrawBackground)(wGuiObject* obj);

extern void (*wGuiTabSetBackgroundColor)(wGuiObject* tab,
										 wColor4s color);

extern wColor4s (*wGuiTabGetBackgroundColor)(wGuiObject* tab);

///wGuiTabControl///
extern wGuiObject* (*wGuiTabControlCreate)(wVector2i minPos,
										   wVector2i maxPos,
										   bool background,
										   bool border);

extern Int32 (*wGuiTabControlGetTabsCount)(wGuiObject* control);

extern wGuiObject* (*wGuiTabControlAddTab)(wGuiObject* control,
										   const wchar_t* caption,
										   Int32 id);

extern wGuiObject* (*wGuiTabControlInsertTab)(wGuiObject* control,
											  UInt32 idx,
											  const wchar_t* caption,
											  Int32 id);

extern wGuiObject* (*wGuiTabControlGetTab)(wGuiObject* control,
										   Int32 idx);

extern bool (*wGuiTabControlSetActiveTabByIndex)(wGuiObject* control,
												 Int32 idx);

extern bool (*wGuiTabControlSetActiveTab)(wGuiObject* control,
										  wGuiObject* tab);

extern Int32 (*wGuiTabControlGetActiveTab)(wGuiObject* control);

extern Int32 (*wGuiTabControlGetTabFromPos)(wGuiObject* control,
											wVector2i position);

extern void (*wGuiTabControlRemoveTab)(wGuiObject* control,
									   Int32 idx);

extern void (*wGuiTabControlRemoveAll)(wGuiObject* control);

extern void (*wGuiTabControlSetTabHeight)(wGuiObject* control,
										  Int32 height);

extern Int32 (*wGuiTabControlGetTabHeight)(wGuiObject* control);

extern void (*wGuiTabControlSetTabMaxWidth)(wGuiObject* control,
											Int32 width);

extern Int32 (*wGuiTabControlGetTabMaxWidth)(wGuiObject* control);

extern void (*wGuiTabControlSetVerticalAlignment)(wGuiObject* control,
												  wGuiAlignment al);

extern wGuiAlignment (*wGuiTabControlGetVerticalAlignment)(wGuiObject* control);

extern void (*wGuiTabControlSetTabExtraWidth)(wGuiObject* control,
											  Int32 extraWidth);

extern Int32 (*wGuiTabControlGetTabExtraWidth)(wGuiObject* control);

///wGuiTable///
extern wGuiObject* (*wGuiTableCreate)(wVector2i minPos,
									  wVector2i maxPos,
									  bool background);

extern void (*wGuiTableAddColumn)(wGuiObject* table,
								  wchar_t* caption,
								  Int32 columnIndex);

extern void (*wGuiTableRemoveColumn)(wGuiObject* table,
									 UInt32 columnIndex);

extern Int32 (*wGuiTableGetColumnsCount)(wGuiObject* table);

extern bool (*wGuiTableSetActiveColumn)(wGuiObject* table,
										Int32 idx,
										bool doOrder);

extern Int32 (*wGuiTableGetActiveColumn)(wGuiObject* table);

extern wGuiOrderingMode (*wGuiTableGetActiveColumnOrdering)(wGuiObject* table);

extern void (*wGuiTableSetColumnWidth)(wGuiObject* table,
									   UInt32 columnIndex,
									   UInt32 width);

extern void (*wGuiTableSetColumnsResizable)(wGuiObject* table,
											bool resizible);

extern bool (*wGuiTableIsColumnsResizable)(wGuiObject* table);

extern Int32 (*wGuiTableGetSelected)(wGuiObject* table);

extern void (*wGuiTableSetSelectedByIndex)(wGuiObject* table,
										   Int32 index);

extern Int32 (*wGuiTableGetRowsCount)(wGuiObject* table);

extern UInt32 (*wGuiTableAddRow)(wGuiObject* table,
								 UInt32 rowIndex);

extern void (*wGuiTableRemoveRow)(wGuiObject* table,
								  UInt32 rowIndex);

extern void (*wGuiTableClearRows)(wGuiObject* table);

extern void (*wGuiTableSwapRows)(wGuiObject* table,
								 UInt32 rowIndexA,
								 UInt32 rowIndexB);

extern void (*wGuiTableSetOrderRows)(wGuiObject* table,
									 Int32 columnIndex,
									 wGuiOrderingMode mode);

extern void (*wGuiTableSetCellText)(wGuiObject* table,
									UInt32 rowIndex,
									UInt32 columnIndex,
									const wchar_t* text,
									wColor4s color);

extern void (*wGuiTableSetCellData)(wGuiObject* table,
									UInt32 rowIndex,
									UInt32 columnIndex,
									UInt32* data);

extern void (*wGuiTableSetCellColor)(wGuiObject* table,
									 UInt32 rowIndex,
									 UInt32 columnIndex,
									 wColor4s color);

extern const wchar_t* (*wGuiTableGetCellText)(wGuiObject* table,
											  UInt32 rowIndex,
											  UInt32 columnIndex );

extern UInt32* (*wGuiTableGetCellData)(wGuiObject* table,
									   UInt32 rowIndex,
									   UInt32 columnIndex );

extern void (*wGuiTableSetDrawFlags)(wGuiObject* table,
									 wGuiTableDrawFlags flags);

extern wGuiTableDrawFlags (*wGuiTableGetDrawFlags)(wGuiObject* table);

extern void (*wGuiTableSetOverrideFont)(wGuiObject* obj,
										wFont* font);

extern wFont* (*wGuiTableGetOverrideFont)(wGuiObject* obj);

extern wFont* (*wGuiTableGetActiveFont)(wGuiObject* obj);

extern Int32 (*wGuiTableGetItemHeight)(wGuiObject* obj);

extern wGuiObject* (*wGuiTableGetVerticalScrollBar)(wGuiObject* obj);

extern wGuiObject* (*wGuiTableGetHorizontalScrollBar)(wGuiObject* obj);

extern void (*wGuiTableSetDrawBackground)(wGuiObject* obj, bool value);

extern bool (*wGuiTableIsDrawBackground)(wGuiObject* obj);

///wGuiToolBar///
extern wGuiObject* (*wGuiToolBarCreate)();

extern wGuiObject* (*wGuiToolBarAddButton)(wGuiObject* bar,
										   const wchar_t* text,
										   const wchar_t* tooltiptext,
										   wTexture* img,
										   wTexture* pressedImg,
										   bool isPushButton,
										   bool useAlphaChannel);


///wGuiMessageBox
extern wGuiObject* (*wGuiMessageBoxCreate)(const wchar_t * wcptrTitle,
										   const wchar_t* wcptrTCaption,
										   bool modal,
										   wGuiMessageBoxFlags flags,
										   wTexture* image);

///wGuiTree///
///Create a tree view element.
extern wGuiObject* (*wGuiTreeCreate)(wVector2i minPos,
									 wVector2i maxPos,
									 bool background,
									 bool barvertical,
									 bool barhorizontal);

///returns the root node (not visible) from the tree.
extern wGuiObject* (*wGuiTreeGetRoot)(wGuiObject* tree);

///returns the selected node of the tree or 0 if none is selected
extern wGuiObject* (*wGuiTreeGetSelected)(wGuiObject* tree);

///sets if the tree lines are visible
extern void (*wGuiTreeSetLinesVisible)(wGuiObject* tree,
									   bool visible);

///returns true if the tree lines are visible
extern bool (*wGuiTreeIsLinesVisible)(wGuiObject* tree);

///Sets the font which should be used as icon font.
extern void (*wGuiTreeSetIconFont)(wGuiObject* tree,
								   wFont* font);

///Sets the image list which should be used for the image and selected image of every node.
extern void (*wGuiTreeSetImageList)(wGuiObject* tree,
									wGuiObject* list);

///Returns the image list which is used for the nodes.
extern wGuiObject* (*wGuiTreeGetImageList)(wGuiObject* tree);

///Sets if the image is left of the icon. Default is true.
extern void (*wGuiTreeSetImageLeftOfIcon)(wGuiObject* tree,
										  bool bLeftOf);

///Returns if the Image is left of the icon. Default is true.
extern bool (*wGuiTreeIsImageLeftOfIcon)(wGuiObject* tree);

///Returns the node which is associated to the last event.
extern wGuiObject* (*wGuiTreeGetLastEventNode)(wGuiObject* tree);

///wGuiTreeNode///
///returns the owner (Gui tree) of this node
extern wGuiObject* (*wGuiTreeNodeGetOwner)(wGuiObject* node);

extern wGuiObject* (*wGuiTreeNodeGetParent)(wGuiObject* node);

///returns the text of the node
extern const wchar_t* (*wGuiTreeNodeGetText)(wGuiObject* node);

///sets the text of the node
extern void (*wGuiTreeNodeSetText)(wGuiObject* node,
								   const wchar_t* text);

///sets the icon text of the node
extern void (*wGuiTreeNodeSetIcon)(wGuiObject* node,
								   const wchar_t* icon);

///returns the icon text of the node
extern const wchar_t* (*wGuiTreeNodeGetIcon)(wGuiObject* node);

///sets the image index of the node
extern void (*wGuiTreeNodeSetImageIndex)(wGuiObject* node,
										 UInt32 imageIndex);

///returns the image index of the node
extern UInt32 (*wGuiTreeNodeGetImageIndex)(wGuiObject* node);

///sets the image index of the node
extern void (*wGuiTreeNodeSetSelectedImageIndex)(wGuiObject* node,
												 UInt32 imageIndex);

///returns the image index of the node
extern UInt32 (*wGuiTreeNodeGetSelectedImageIndex)(wGuiObject* node);

///sets the user data (UInt32*) of this node
extern void  (*wGuiTreeNodeSetData)(wGuiObject* node,
									UInt32* data);

///returns the user data (UInt32*) of this node
extern UInt32* (*wGuiTreeNodeGetData)(wGuiObject* node);

///sets the user data2 of this node
extern void (*wGuiTreeNodeSetData2)(wGuiObject* node,
									UInt32* data2);

///returns the user data2 of this node
extern UInt32* (*wGuiTreeNodeGetData2)(wGuiObject* node);

///returns the child item count
extern UInt32 (*wGuiTreeNodeGetChildsCount)(wGuiObject* node);

///Remove a child node.
extern void (*wGuiTreeNodeRemoveChild)(wGuiObject* node,
									   wGuiObject* child);

///removes all children (recursive) from this node
extern void (*wGuiTreeNodeRemoveChildren)(wGuiObject* node);

///returns true if this node has child nodes
extern bool (*wGuiTreeNodeHasChildren)(wGuiObject* node);

///Adds a new node behind the last child node.
extern wGuiObject* (*wGuiTreeNodeAddChildBack)(wGuiObject* node,
											   const wchar_t* text,
											   const wchar_t* icon,
											   Int32 imageIndex,
											   Int32 selectedImageIndex,
											   void* data,
											   UInt32* data2);


///Adds a new node before the first child node.
extern wGuiObject* (*wGuiTreeNodeAddChildFront)(wGuiObject* node,
												const wchar_t* text,
												const wchar_t* icon,
												Int32 imageIndex,
												Int32 selectedImageIndex,
												void* data,
												UInt32* data2);

///Adds a new node behind the other node.
extern wGuiObject* (*wGuiTreeNodeInsertChildAfter)(wGuiObject* node,
												   wGuiObject* other,
												   const wchar_t* text,
												   const wchar_t* icon,
												   Int32 imageIndex,
												   Int32 selectedImageIndex,
												   void* data,
												   UInt32* data2);

///Adds a new node before the other node.
extern wGuiObject* (*wGuiTreeNodeInsertChildBefore)(wGuiObject* node,
													wGuiObject* other,
													const wchar_t* text,
													const wchar_t* icon,
													Int32 imageIndex,
													Int32 selectedImageIndex,
													void* data,
													UInt32* data2);

///Return the first child node from this node.
extern wGuiObject* (*wGuiTreeNodeGetFirstChild)(wGuiObject* node);

///Return the last child node from this node.
extern wGuiObject* (*wGuiTreeNodeGetLastChild)(wGuiObject* node);

///Returns the previous sibling node from this node.
extern wGuiObject* (*wGuiTreeNodeGetPrevSibling)(wGuiObject* node);

///Returns the next sibling node from this node.
extern wGuiObject* (*wGuiTreeNodeGetNextSibling)(wGuiObject* node);

///Returns the next visible (expanded, may be out of scrolling) node from this node.
extern wGuiObject* (*wGuiTreeNodeGetNextVisible)(wGuiObject* node);

///Moves a child node one position up.
extern bool (*wGuiTreeNodeMoveChildUp)(wGuiObject* node,
									   wGuiObject* child);

///Moves a child node one position down.
extern bool (*wGuiTreeNodeMoveChildDown)(wGuiObject* node,
										 wGuiObject* child);

///Sets if the node is expanded.
extern void (*wGuiTreeNodeSetExpanded)(wGuiObject* node,
									   bool expanded);

///Returns true if the node is expanded (children are visible).
extern bool (*wGuiTreeNodeIsExpanded)(wGuiObject* node);

///Sets this node as selected
extern void (*wGuiTreeNodeSetSelected)(wGuiObject* node,
									   bool selected);

///Returns true if the node is currently selected.
extern bool  (*wGuiTreeNodeIsSelected)(wGuiObject* node);

///Returns true if this node is the root node.
extern bool (*wGuiTreeNodeIsRoot)(wGuiObject* node);

///Returns the level of this node.
///The root node has level 0.
///Direct children of the root has level 1 ...
extern Int32 (*wGuiTreeNodeGetLevel)(wGuiObject* node);

///Returns true if this node is visible (all parents are expanded)
extern bool (*wGuiTreeNodeIsVisible)(wGuiObject* node);

///wGuiImageList///
extern wGuiObject* (*wGuiImageListCreate)(wTexture* texture,
										  wVector2i size,
										  bool useAlphaChannel);

extern void (*wGuiImageListDraw)(wGuiObject* list,
								 Int32 index,
								 wVector2i pos,
								 wVector2i clipPos,
								 wVector2i clipSize);

extern Int32 (*wGuiImageListGetCount)(wGuiObject* list);

extern wVector2i (*wGuiImageListGetSize)(wGuiObject* list);

///wGuiColorSelectDialog///
extern wGuiObject* (*wGuiColorSelectDialogCreate)(const wchar_t* title,
												  bool modal);

///wGuiMeshViewer///
extern wGuiObject* (*wGuiMeshViewerCreate)(wVector2i minPos,
										   wVector2i maxPos,
										   const wchar_t* text);

extern void (*wGuiMeshViewerSetMesh)(wGuiObject* viewer,
									 wMesh* mesh);

extern wMesh* (*wGuiMeshViewerGetMesh)(wGuiObject* viewer);

extern void (*wGuiMeshViewerSetMaterial)(wGuiObject* viewer,
										 wMaterial* material);

extern wMaterial* (*wGuiMeshViewerGetMaterial)(wGuiObject* viewer);

///wGuiSpriteBank///
///Returns pointer to the sprite bank with the specified file name.
///Loads the bank if it was not loaded before.
extern wGuiObject* (*wGuiSpriteBankLoad)(const char* file);

extern wGuiObject* (*wGuiSpriteBankCreate)(const char* name);

///Adds a texture to the sprite bank.
extern void (*wGuiSpriteBankAddTexture)(wGuiObject* bank,
										wTexture* texture);

///Changes one of the textures in the sprite bank
extern void (*wGuiSpriteBankSetTexture)(wGuiObject* bank,
										UInt32 index,
										wTexture* texture);

///Add the texture and use it for a single non-animated sprite.
///The texture and the corresponding rectangle and sprite will all be added
/// to the end of each array. returns the index of the sprite or -1 on failure
extern Int32 (*wGuiSpriteBankAddSprite)(wGuiObject* bank,
										wTexture* texture);

extern wTexture* (*wGuiSpriteBankGetTexture)(wGuiObject* bank,
											 UInt32 index);

extern UInt32 (*wGuiSpriteBankGetTexturesCount)(wGuiObject* bank);

extern void (*wGuiSpriteBankRemoveAll)(wGuiObject* bank);

extern void (*wGuiSpriteBankDrawSprite)(wGuiObject* bank,
										UInt32 index,
										wVector2i position,
										wVector2i* clipPosition,
										wVector2i* clipSize,
										wColor4s color,
										UInt32 starttime,
										UInt32 currenttime,
										bool loop,
										bool center);

extern void (*wGuiSpriteBankDrawSpriteBatch)(wGuiObject* bank,
											 UInt32* indexArray,
											 UInt32 idxArrayCount,
											 wVector2i* positionArray,
											 UInt32 posArrayCount,
											 wVector2i* clipPosition,
											 wVector2i* clipSize,
											 wColor4s color,
											 UInt32 startTime,
											 UInt32 currentTime,
											 bool loop,
											 bool center);

///wGuiCheckGroup
extern wGuiObject* (*wGuiCheckBoxGroupCreate)(wVector2i minPos,
											  wVector2i maxPos);

extern Int32 (*wGuiCheckBoxGroupAddCheckBox)(wGuiObject* group,
											 wGuiObject* check);

extern Int32 (*wGuiCheckBoxGroupInsertCheckBox)(wGuiObject* group,
												wGuiObject* check,
												UInt32 index);

extern wGuiObject* (*wGuiCheckBoxGroupGetCheckBox)(wGuiObject* group,
												   UInt32 index);

extern Int32 (*wGuiCheckBoxGroupGetIndex)(wGuiObject* group,
										  wGuiObject* check);

extern Int32 (*wGuiCheckBoxGroupGetSelectedIndex)(wGuiObject* group);

extern bool (*wGuiCheckBoxGroupRemoveCheckBox)(wGuiObject* group,
											   UInt32 index);

extern void (*wGuiCheckBoxGroupRemoveAll)(wGuiObject* group);

extern UInt32 (*wGuiCheckBoxGroupGetSize)(wGuiObject* group);

extern void (*wGuiCheckBoxGroupSelectCheckBox)(wGuiObject* group,
											   Int32 index);

extern void (*wGuiCheckBoxGroupClearSelection)(wGuiObject* group);

extern void (*wGuiCheckBoxGroupSetBackgroundColor)(wGuiObject* obj,
												   wColor4s color);

extern void (*wGuiCheckBoxGroupDrawBackground)(wGuiObject* obj,
											   bool value);

///wGuiProgressBar///
extern wGuiObject* (*wGuiProgressBarCreate)(wVector2i minPos,
											wVector2i maxPos,
											bool isHorizontal);


extern void (*wGuiProgressBarSetPercentage)(wGuiObject* bar,
											UInt32 percent);

extern UInt32 (*wGuiProgressBarGetPercentage)(wGuiObject* bar);

extern void (*wGuiProgressBarSetDirection)(wGuiObject* bar,
										   bool isHorizontal);

extern bool (*wGuiProgressBarIsHorizontal)(wGuiObject* bar);

extern void (*wGuiProgressBarSetBorderSize)(wGuiObject* bar,
											UInt32 size);

extern void (*wGuiProgressBarSetSize)(wGuiObject* bar,
									  wVector2u size);

extern void (*wGuiProgressBarSetFillColor)(wGuiObject* bar,
										   wColor4s color);

extern wColor4s (*wGuiProgressBarGetFillColor)(wGuiObject* bar);

extern void (*wGuiProgressBarSetTextColor)(wGuiObject* bar,
										   wColor4s color);

extern void (*wGuiProgressBarShowText)(wGuiObject* bar,
									   bool value);

extern bool (*wGuiProgressBarIsShowText)(wGuiObject* bar);

extern void (*wGuiProgressBarSetFillTexture)(wGuiObject* bar,
											 wTexture* tex);

extern void (*wGuiProgressBarSetBackTexture)(wGuiObject* bar,
											 wTexture* tex);

extern void (*wGuiProgressBarSetFont)(wGuiObject* bar,
									  wFont* font);

extern void (*wGuiProgressBarSetBackgroundColor)(wGuiObject* bar,
												 wColor4s color);

extern void (*wGuiProgressBarSetBorderColor)(wGuiObject* obj,
											 wColor4s color);

///wGuiTextArea///
extern wGuiObject* (*wGuiTextAreaCreate)(wVector2i minPos,
										 wVector2i maxPos,
										 Int32 maxLines);

extern void (*wGuiTextAreaSetBorderSize)(wGuiObject* tarea,
										 UInt32 size);

extern void (*wGuiTextAreaSetAutoScroll)(wGuiObject* tarea,
										 bool value);

extern void (*wGuiTextAreaSetPadding)(wGuiObject* tarea,
									  UInt32 padding);

extern void (*wGuiTextAreaSetBackTexture)(wGuiObject* tarea,
										  wTexture* tex);

extern void (*wGuiTextAreaSetWrapping)(wGuiObject* tarea,
									   bool value);

extern void (*wGuiTextAreaSetFont)(wGuiObject* tarea,
								   wFont* font);

extern void (*wGuiTextAreaAddLine)(wGuiObject* tarea,
								   const wchar_t* text,
								   UInt32 lifeTime,
								   wColor4s color,
								   wTexture* icon,
								   Int32 iconMode);

extern void (*wGuiTextAreaRemoveAll)(wGuiObject* tarea);

extern void (*wGuiTextAreaSetBackgroundColor)(wGuiObject* tarea,
											  wColor4s color);

extern void (*wGuiTextAreaSetBorderColor)(wGuiObject* tarea,
										  wColor4s color);

///wGuiCEditor///
extern wGuiObject* (*wGuiCEditorCreate)(const wchar_t* wcptrText,
										wVector2i minPos,
										wVector2i maxPos,
										bool border);

extern void (*wGuiCEditorSetHScrollVisible)(wGuiObject* box,
											bool value);

extern void (*wGuiCEditorSetText)(wGuiObject* box,
								  const wchar_t* text);

extern void (*wGuiCEditorSetColors)(wGuiObject* box,
									wColor4s backColor,
									wColor4s lineColor,
									wColor4s textColor);

extern void (*wGuiCEditorSetLinesCountVisible)(wGuiObject* box,
											   bool value);

extern bool (*wGuiCEditorIsLinesCountVisible)(wGuiObject* box);

extern void (*wGuiCEditorSetElementText)(wGuiObject* box,
										 UInt32 index,
										 const wchar_t* text);

extern void (*wGuiCEditorSetSelectionColors)(wGuiObject* box,
											 wColor4s backColor,
											 wColor4s textColor,
											 wColor4s back2Color);

extern void (*wGuiCEditorRemoveText)(wGuiObject* box);

extern void (*wGuiCEditorAddKeyword)(wGuiObject* box,
									 const char* word,
									 wColor4s color,
									 bool matchCase);

extern void (*wGuiCEditorAddLineKeyword)(wGuiObject* box,
										 const char* word,
										 wColor4s color,
										 bool matchCase);

extern void (*wGuiCEditorAddGroupKeyword)(wGuiObject* box,
										  const char* word,
										  const char* endKeyword,
										  wColor4s color,
										  bool matchCase);

extern void (*wGuiCEditorBoxAddKeywordInfo)(wGuiObject* box,
											Int32 size,
											Int32 type);

extern void (*wGuiCEditorBoxRemoveAllKeywords)(wGuiObject* box);

extern void (*wGuiCEditorBoxAddCppKeywords)(wGuiObject* box,
											wColor4s key,
											wColor4s string,
											wColor4s comment);

extern void (*wGuiCEditorAddLuaKeywords)(wGuiObject* box,
										 wColor4s key,
										 wColor4s string,
										 wColor4s comment);

extern void (*wGuiCEditorAddFbKeywords)(wGuiObject* box,
										wColor4s key,
										wColor4s string,
										wColor4s comment);

extern void (*wGuiCEditorReplaceText)(wGuiObject* box,
									  Int32 start,
									  Int32 end,
									  const wchar_t* text);

extern void (*wGuiCEditorPressReturn)(wGuiObject* box);

extern void (*wGuiCEditorAddText)(wGuiObject* box,
								  const wchar_t* addText);

extern const wchar_t* (*wGuiCEditorGetText)(wGuiObject* box);

extern void (*wGuiCEditorSetLineToggleVisible)(wGuiObject* box,
											   bool value);

extern void (*wGuiCEditorSetContextMenuText)(wGuiObject* box,
											 const wchar_t* cut_text,
											 const wchar_t* copy_text,
											 const wchar_t* paste_text,
											 const wchar_t* del_text,
											 const wchar_t* redo_text,
											 const wchar_t* undo_text,
											 const wchar_t* btn_text);

extern void (*wGuiCEditorBoxCopy)(wGuiObject* box);

extern void (*wGuiCEditorCut)(wGuiObject* box);

extern void (*wGuiCEditorPaste)(wGuiObject* box);

extern void (*wGuiCEditorUndo)(wGuiObject* box);

extern void (*wGuiCEditorRedo)(wGuiObject* box);

extern wFont* (*wGuiCEditorGetOverrideFont)(wGuiObject* obj);

extern wFont* (*wGuiCEditorGetActiveFont)(wGuiObject* obj);

extern void (*wGuiCEditorEnableOverrideColor)(wGuiObject* obj,
											  bool value);

extern bool (*wGuiCEditorIsOverrideColor)(wGuiObject* obj);

extern void (*wGuiCEditorSetOverrideColor)(wGuiObject* obj,
										   wColor4s color);

extern wColor4s (*wGuiCEditorGetOverrideColor)(wGuiObject* obj);

extern void (*wGuiCEditorSetDrawBackground)(wGuiObject* obj,
											bool value);

extern bool (*wGuiCEditorIsDrawBackGround)(wGuiObject* obj);

extern void (*wGuiCEditorSetDrawBorder)(wGuiObject* obj,
										bool value);

extern bool (*wGuiCEditorIsDrawBorder)(wGuiObject* obj);

extern void (*wGuiCEditorSetTextAlignment)(wGuiObject* obj,
										   wGuiAlignment Horizontalvalue,
										   wGuiAlignment Verticalvalue);

extern  void (*wGuiCEditorSetWordWrap)(wGuiObject* obj,
									   bool value);

extern bool (*wGuiCEditorIsWordWrap)(wGuiObject* obj);

extern void (*wGuiCEditorSetBackgroundColor)(wGuiObject* obj,
											 wColor4s color);

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __WORLDSIM3D_RUNTIME_H_INCLUDED
