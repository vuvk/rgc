#ifndef OBJECT3D_BILLBOARD_H
#define OBJECT3D_BILLBOARD_H

#include "WorldSim3D.h"
#include "object3d.h"
#include "res_in_mem.h"

class TBillboard : public TObject3D
{
public:
    TBillboard(ScrResourceInMem* index, bool centered = false);

    virtual void init(string className = "", string objectName = "", bool numerate = true)
    {
        TObject::init(className, objectName, numerate);
    }

    virtual void setFrame(int frame);
    virtual void update();

    virtual void setScale(wVector3f& scale);
};

#endif // OBJECT3D_BILLBOARD_H
