#ifndef RES_IN_MEM_H
#define RES_IN_MEM_H

#include <vector>
#include "WorldSim3D.h"
#include "js_util.h"
#include "file_system.h"
#include "res_manager.h"

/** тип ресурса */
enum ResourceType
{
    rtObject,
    rtTexture,
    rtSprite,
    rtDoor,
    rtKey,
    rtWeaponItem,
    rtWeaponInHand,
    rtBullet,
    rtAmmo,
    rtSound
};

class BaseResourceInMem
{
public:
    string  name = "";
    int     posInRep = -1;      // позиция в хранилище
    ResourceType type = rtObject;  // тип объектов, для которых этот ресурс хранится

    BaseResourceInMem()  { clear(); }
    virtual ~BaseResourceInMem() { clear(); }

    virtual void clear()
    {
        name = "";
    }
};

/** ресурс-эталон, загруженный из пака */
class ResourceInMem : public BaseResourceInMem
{
public:
    uint8_t animSpeed;          // кол-во кадров в секунду
    vector<wTexture*> frames;
    float solidX,               // размер ограничивающей коробки
          solidY;
    float scaleX,               // масштаб
          scaleY;

    ResourceInMem();
    virtual ~ResourceInMem();

    virtual bool isLoaded();
    virtual void clear();
    virtual void loadFrames();
    virtual void clearFrames();
};

/** ресурс со скриптом */
class ScrResourceInMem : public ResourceInMem
{
public:
    static vector<ScrResourceInMem*> scrResourcesInMem;

    TScriptJS script;
    string className = "";  // имя класса
    bool isClassCreated = false;

    ScrResourceInMem();
    virtual ~ScrResourceInMem();

    /* создать скрипт класса */
    virtual void init(string className = "", bool numerate = true);
    virtual void reloadScript();
    virtual void clearScript();
    virtual void clear();
};

#endif // RES_IN_MEM_H
