#pragma once
#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <array>
#include <string>
#include <cmath>
#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

#define MAX_FPS 60   /** Максимальный FPS для установки ограничения */

#define MAP_WIDTH  64  /** Максимальная ширина карты */
#define MAP_HEIGHT 64  /** Максимальная высота карты */

#define TEXTURE_MAX_WIDTH  256  /** Максимальная ширина текстуры в пикселях */
#define TEXTURE_MAX_HEIGHT 256  /** Максимальная высота текстуры в пикселях */
#define TEXTURE_MAX_SIZE   TEXTURE_MAX_WIDTH * TEXTURE_MAX_HEIGHT * sizeof(int32_t)   /** Максимальный размер текстуры в байтах */

#define OBJECTS_MAX_COUNT  10240
#define TEXTURES_MAX_COUNT 1024
#define SPRITES_MAX_COUNT  1024
#define DOORS_MAX_COUNT    1024
#define KEYS_MAX_COUNT     1024
#define WEAPONS_MAX_COUNT  10
#define BULLETS_MAX_COUNT  1024
#define AMMO_MAX_COUNT     1024
#define IMAGES_MAX_COUNT   1024

#define BYTECODE_MAX_LENGTH 131072  /** максимальная длина массива под байткод = 0.5 Mb */

#define SCRIPTS_DELAY 0.01 /** частота, с которой будут вызываться скрипты - 100 раз в секунду */

/** размер объектов в игре? */
#define BLOCK_SIZE 1

#define DEG_TO_RAD_COEFF M_PI / 180.0
#define RAD_TO_DEG_COEFF 180.0 / M_PI

/** типы патронов по умолчанию */
static std::array<std::string, 9> g_DefAmmoTypes = {
        "Projectile",
        "Pistol ammo",
        "Rifle ammo",
        "Shotgun ammo",
        "Machine-gun ammo",
        "Grenade",
        "Rocket",
        "Fire",
        "Plasma"
};

#endif // CONSTANTS_H
