#ifndef OBJECT_2D_H
#define OBJECT_2D_H

#include <vector>

#include "WorldSim3D.h"
#include "base_object.h"
#include "res_in_mem.h"

using namespace std;

class TImage2D : public ScrResourceInMem {};

class TObject2D : public TObject
{
public:
    static vector<TObject2D*> objects;

    wVector2f position = wVECTOR2f_ZERO;
    wVector2f scale    = {1, 1};
    float     rotation = 0.0;

    //TImage2D* index      = nullptr;   /* индекс набора кадров */
    wTexture* curTexture = nullptr;   // текущая для отображения текстура

    TObject2D (ScrResourceInMem* index);
    virtual ~TObject2D();

    /*
    virtual void init(string className = "", string objectName = "")
    {
        TObject::init(className, objectName);
    }
    */
    virtual void update()
    {
        if (!isVisible || !index)
            return;

        TObject::update();

        draw();
    }

    virtual void setFrame(int frame);
    virtual inline int getFrame();
    virtual int getFramesCount();

    void draw();

    /* manipulations with object */
    /* position */
    virtual void      setPosition(wVector3f& pos) { position = {pos.x, pos.y}; }
    virtual void      setPosition(wVector2f& pos) { position = pos; }
    virtual void      setPositionX(float& x)      { position.x = x; }
    virtual void      setPositionY(float& y)      { position.y = y; }
    virtual void      setPositionZ(float& z)      {}
    //wVector3f getPosition()               { return {position.x, position.y, 0}; }
    virtual void      getPosition(vector<double>& pos) { pos = {position.x, position.y}; }
    virtual float     getPositionX()              { return position.x; }
    virtual float     getPositionY()              { return position.y; }
    virtual float     getPositionZ()              { return std::numeric_limits<double>::max(); }
    virtual void      move(wVector3f& moveVector) { position.x += moveVector.x;
                                            position.y += moveVector.y; }
    /* rotation */
    virtual void      setRotation(wVector3f& rot) { rotation = rot.x; }
    virtual void      setRotation(float& rot)     { rotation = rot; }
    virtual void      setRotationX(float& x)      {}
    virtual void      setRotationY(float& y)      {}
    virtual void      setRotationZ(float& z)      {}
    //wVector3f getRotation()               { return { rotation, 0, 0}; }
    virtual void      getRotation(vector<double>& rot) { rot = { rotation }; }
    virtual float     getRotationX()              { return std::numeric_limits<double>::max(); }
    virtual float     getRotationY()              { return std::numeric_limits<double>::max(); }
    virtual float     getRotationZ()              { return std::numeric_limits<double>::max(); }
    virtual void      rotate(wVector3f& rot)      { rotation += rot.x; }
    /* size */
    //wVector3f getSize();
    virtual void      getSize(vector<double>& vec);
    virtual float     getSizeX()                  { vector<double> size; getSize(size); return size[0]; }
    virtual float     getSizeY()                  { vector<double> size; getSize(size); return size[1]; }
    virtual float     getSizeZ()                  { return std::numeric_limits<double>::max(); }
    /* scale */
    virtual void      setScale(wVector3f& scale)  { this->scale = {scale.x, scale.y}; }
    virtual void      setScale(wVector2f& scale)  { this->scale = scale; }
    virtual void      setScaleX(float& x)         { scale.x = x; }
    virtual void      setScaleY(float& y)         { scale.y = y; }
    virtual void      setScaleZ(float& z)         {}
    //wVector3f getScale()                  { return {scale.x, scale.y, 0}; }
    virtual void      getScale(vector<double>& vec) { vec = {scale.x, scale.y}; }
    virtual float     getScaleX()                 { return scale.x; }
    virtual float     getScaleY()                 { return scale.y; }
    virtual float     getScaleZ()                 { return std::numeric_limits<double>::max(); }
};

extern vector<TImage2D> g_Images2D;

void InitImages2D();

#endif // OBJECT_2D_H
