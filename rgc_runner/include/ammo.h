#ifndef _AMMO_H
#define _AMMO_H

#include <vector>

#include "WorldSim3D.h"
#include "object3d_billboard.h"
#include "res_in_mem.h"

/** патрон-эталон, загруженный из пака */
class TAmmoInMem : public ScrResourceInMem
{
public:
    uint16_t volume;           // объем патронника
    std::string pickUpSound;   // имя звука подбора
    int ammoTypeId;              // id типа патронов

    TAmmoInMem() : ScrResourceInMem()
    {
        type = rtAmmo;
    }
};

/** нода патронов, создаваемая на карте */
class TAmmoNode : public TBillboard
{
public:
    static int count;

    // создание ноды
    TAmmoNode (TAmmoInMem* index);

    virtual void init(string className = "", string objectName = "", bool numerate = true)
    {
        TObject::init("Ammo", "ammo", true);
    }
};

extern vector<TAmmoInMem> g_Ammo;

void InitAmmo();
void LoadAmmoTypes();
bool LoadAmmoPack();

#endif //_AMMO_H
