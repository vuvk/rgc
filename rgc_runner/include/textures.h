#ifndef TEXTURES_H
#define TEXTURES_H

#include <vector>
#include "WorldSim3D.h"
#include "res_in_mem.h"

using namespace std;

/** текстура-эталон, загруженная из пака */
class TTextureInMem : public ResourceInMem
{
public:
    TTextureInMem() : ResourceInMem() { type = rtTexture; }

    void loadFrames(bool isShadowed = false)
    {
        ResourceInMem::loadFrames();

        /* затенить */
        if (isShadowed)
        {
            wVector2u    size;
            uint32_t     pitch;
            wColorFormat colorFormat;
            int          textureSize;
            uint32_t*    loadTextureBits = nullptr;
            int32_t      color;

            for (int j = 0; j < frames.size(); ++j)
            {
                wTextureGetInformation(frames[j], &size, &pitch, &colorFormat);
                textureSize = size.x * size.y * sizeof(int32_t);

                loadTextureBits = wTextureLock(frames[j]);
                for (int b = 0; b < textureSize / sizeof(int32_t); ++b)
                {
                    color = ((int32_t*)loadTextureBits)[b];
                    color = (color >> 1) & 8355711;
                    ((int32_t*)loadTextureBits)[b] = color;
                }
                wTextureUnlock(frames[j]);
            }
        }
    }
};

/** ноды по именам текстур, создаваемые на карте со ссылкой на меши, у которых будет меняться кадр */
struct TTextureNode
{
    TTextureInMem* index = nullptr;     // индекс текстуры (позиция в массиве textures или texturesShadowed)
    int     curFrame = 0;               // текущий кадр
    float   animSpeed = 0.0;            // скорость анимации
    float   _animSpeed = 0.0;           // значение прироста анимации до смены кадра
    wNode*  node = nullptr;

    TTextureNode(TTextureInMem* index, wNode* object);
    ~TTextureNode();

    void update();
};

extern vector<TTextureInMem> g_Textures;
extern vector<TTextureInMem> g_TexturesShadowed;
extern vector<TTextureNode*> g_TextureNodes;

void InitTextures();
bool LoadTexturesPack();
void TextureNodesUpdate();

#endif // TEXTURES_H
