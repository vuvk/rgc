#ifndef SPRITES_H
#define SPRITES_H

#include <string>
#include <vector>

#include "WorldSim3D.h"

#include "res_in_mem.h"
#include "object3d_billboard.h"

/** спрайт-эталон, загруженный из пака */
class TSpriteInMem : public ScrResourceInMem
{
public:
    uint8_t axisParams  = 0;        // поворот по осям: 0 = смотрит на игрока, 1 = фиксация по горизонтали, 2 = фиксация по вертикали
    bool    collision   = false;    // можно столкнуться
    bool    destroyable = false;    // разрушаемый?
    std::string deathSound = "";    // имя звука разрушения
    double  endurance   = 0.0;      // живучесть
    bool    playOnce    = false;       // играть оин раз?
    bool    deleteOnLastFrame = false; // удалить на последнем кадре объект?

    TSpriteInMem() : ScrResourceInMem()
    {
        type = rtSprite;
    }

    virtual void clear()
    {
        ScrResourceInMem::clear();
        axisParams = 0;
        collision = false;
        destroyable = false;
        deathSound = "";
        endurance = 0.0;
        playOnce = false;
        deleteOnLastFrame = false;
    }
};

/** нода спрайта, создаваемая на карте */
class TSpriteNode : public TBillboard
{
public:
    static int count;                 // количество представителей класса

    // создание ноды
    TSpriteNode(TSpriteInMem* index);

    virtual void init(string className = "", string objectName = "", bool numerate = true)
    {
        TObject::init("Sprite", "sprite", true);
    }
};

extern vector<TSpriteInMem> g_Sprites;

void InitSprites();
bool LoadSpritesPack();

#endif // SPRITES_H
