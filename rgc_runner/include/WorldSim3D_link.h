#ifndef __WORLDSIM3D_LINK_H_INCLUDED
#define __WORLDSIM3D_LINK_H_INCLUDED

#include "WorldSim3D_main.h"

#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus

///wConsole///
void wConsoleSetFontColor(wConsoleFontColor c);

void wConsoleSetBackColor(wConsoleBackColor c);

Int32 wConsoleSaveDefaultColors();

void wConsoleResetColors(Int32 defValues);


///wTexture//
wTexture* wTextureLoad(const char* cptrFile );

wTexture* wTextureCreateRenderTarget(wVector2i size);

wTexture* wTextureCreate(const char* name,
                         wVector2i size,
                         wColorFormat format );

void wTextureDestroy(wTexture* texture );

UInt32* wTextureLock(wTexture* texture );

void wTextureUnlock(wTexture* texture );

void wTextureSave(wTexture* texture,
                  const char* file);

wImage* wTextureConvertToImage(wTexture* texture);

void wTextureGetInformation(wTexture* texture,
                            wVector2u* size ,
                            UInt32* pitch,
                            wColorFormat* format );

void wTextureMakeNormalMap(wTexture* texture,
                           Float32 amplitude );

Int32 wTexturesSetBlendMode(wTexture* texturedest,
                          wTexture* texturesrc,
                          wVector2i offset,
                          wBlendOperation operation );

void wTextureSetColorKey(wTexture*texture,
                         wColor4s key);

void wTextureSetGray(wTexture** texture);

void wTextureSetAlpha(wTexture** texture,
                      UInt32 value);

void wTextureSetInverse(wTexture** texture);

void wTextureSetBrightness(wTexture** texture,
                           UInt32 value);

wTexture* wTextureCopy(wTexture* texture,
                       const char* name);

void wTextureSetContrast(wTexture** texture,
                         Float32 value);

#ifdef __cplusplus
wTexture* wTextureFlip(wTexture** texture,
                       Int32 mode=1);
#else
wTexture* wTextureFlip(wTexture** texture,
                       Int32 mode);
#endif // __cplusplus

void wTextureSetBlur(wTexture** texture,
                      Float32 radius);

const char* wTextureGetFullName(wTexture* texture );

const char* wTextureGetInternalName(wTexture* texture );

#ifdef __cplusplus
void wTextureDraw(wTexture* texture,
                wVector2i pos,
                bool useAlphaChannel=true,
                wColor4s color=wCOLOR4s_WHITE);
#else
void wTextureDraw(wTexture* texture,
                wVector2i pos,
                bool useAlphaChannel,
                wColor4s color);
#endif // __cplusplus

#ifdef __cplusplus
void wTextureDrawEx(wTexture* texture,
                  wVector2i pos,
                  wVector2f scale,
                  bool useAlphaChannel=true);
#else
void wTextureDrawEx(wTexture* texture,
                  wVector2i pos,
                  wVector2f scale,
                  bool useAlphaChannel);
#endif // __cplusplus

void wTextureDrawMouseCursor(wTexture* texture);

#ifdef __cplusplus
void wTextureDrawElement(wTexture* texture,
                       wVector2i pos,
                       wVector2i fromPos,
                       wVector2i toPos,
                       bool useAlphaChannel,
                       wColor4s color=wCOLOR4s_WHITE);
#else
void wTextureDrawElement(wTexture* texture,
                       wVector2i pos,
                       wVector2i fromPos,
                       wVector2i toPos,
                       bool useAlphaChannel,
                       wColor4s color);
#endif // __cplusplus

#ifdef __cplusplus
void wTextureDrawElementStretch(wTexture* texture,
                              wVector2i destFromPos,
                              wVector2i destToPos,
                              wVector2i sourceFromPos,
                              wVector2i sourceToPos,
                              bool useAlphaChannel=true);
#else
void wTextureDrawElementStretch(wTexture* texture,
                              wVector2i destFromPos,
                              wVector2i destToPos,
                              wVector2i sourceFromPos,
                              wVector2i sourceToPos,
                              bool useAlphaChannel);
#endif // __cplusplus

#ifdef __cplusplus
void wTextureDrawAdvanced(wTexture* texture,
                        wVector2i pos,
                        wVector2i rotPoint,
                        Float32 rotation,
                        wVector2f scale,
                        bool useAlphaChannel=true,
                        wColor4s color=wCOLOR4s_WHITE,
                        wAntiAliasingMode aliasMode=wAAM_SIMPLE,
                        bool bilinearFilter=true,
                        bool trilinearFilter=true,
                        bool anisotropFilter=true);
#else
void wTextureDrawAdvanced(wTexture* texture,
                                   wVector2i pos,
                                   wVector2i rotPoint,
                                   Float32 rotation,
                                   wVector2f scale,
                                   bool useAlphaChannel,
                                   wColor4s color,
                                   wAntiAliasingMode aliasMode,
                                   bool bFilter,
                                   bool tFilter,
                                   bool aFilter);
#endif // __cplusplus

#ifdef __cplusplus
void wTextureDrawElementAdvanced(wTexture* texture,
                               wVector2i pos,
                               wVector2i fromPos,
                               wVector2i toPos,
                               wVector2i rotPoint,
                               Float32 rotAngleDeg,
                               wVector2f scale,
                               bool useAlphaChannel,
                               wColor4s color=wCOLOR4s_WHITE,
                               wAntiAliasingMode aliasMode=wAAM_SIMPLE,
                               bool bilinearFilter=true,
                               bool trilinearFilter=true,
                               bool anisotropFilter=true);
#else
void wTextureDrawElementAdvanced(wTexture* texture,
                               wVector2i pos,
                               wVector2i fromPos,
                               wVector2i toPos,
                               wVector2i rotPoint,
                               Float32 rotAngleDeg,
                               wVector2f scale,
                               bool useAlphaChannel,
                               wColor4s color,
                               wAntiAliasingMode aliasMode,
                               bool bilinearFilter,
                               bool trilinearFilter,
                               bool anisotropFilter);
#endif // __cplusplus

///w2d///
#ifdef __cplusplus
void  w2dDrawRect(wVector2i minPos,
                  wVector2i maxPos,
                  wColor4s color=wCOLOR4s_WHITE);
#else
void  w2dDrawRect(wVector2i minPos,
                  wVector2i maxPos,
                  wColor4s color);
#endif // __cplusplus

void w2dDrawRectWithGradient(wVector2i minPos,
                                         wVector2i maxPos,
                                         wColor4s colorLeftUp,
                                         wColor4s colorRightUp,
                                         wColor4s colorLeftDown,
                                         wColor4s colorRightDown);

void w2dDrawRectOutline(wVector2i minPos,
                                    wVector2i maxPos,
                                    wColor4s color);

#ifdef __cplusplus
void w2dDrawLine(wVector2i fromPos,
                 wVector2i toPos,
                 wColor4s color=wCOLOR4s_WHITE);
#else
void w2dDrawLine(wVector2i fromPos,
                 wVector2i toPos,
                 wColor4s color);
#endif // __cplusplus

#ifdef __cplusplus
void w2dDrawPixel(wVector2i pos,
                  wColor4s color=wCOLOR4s_WHITE);
#else
void w2dDrawPixel(wVector2i pos,
                  wColor4s color);
#endif // __cplusplus

#ifdef __cplusplus
void  w2dDrawPolygon(wVector2i pos,
                     Float32 Radius,
                     wColor4s color=wCOLOR4s_WHITE,
                     Int32 vertexCount=12);
#else
void  w2dDrawPolygon(wVector2i pos,
                     Float32 Radius,
                     wColor4s color,
                     Int32 vertexCount);
#endif // __cplusplus

///w3d///
#ifdef __cplusplus
void w3dDrawLine(wVector3f start,
				 wVector3f end,
                 wColor4s color=wCOLOR4s_WHITE);
#else
void w3dDrawLine(wVector3f start,
				 wVector3f end,
                 wColor4s color);
#endif // __cplusplus

#ifdef __cplusplus
void w3dDrawBox(wVector3f minPoint,
                wVector3f maxPoint,
                wColor4s color=wCOLOR4s_WHITE);
#else
void w3dDrawBox(wVector3f minPoint,
                wVector3f maxPoint,
                wColor4s color);
#endif // __cplusplus

#ifdef __cplusplus
void w3dDrawTriangle(wTriangle triangle,
                     wColor4s color=wCOLOR4s_WHITE);
#else
void w3dDrawTriangle(wTriangle triangle,
                     wColor4s color);
#endif // __cplusplus

///wFont///
wFont* wFontLoad(const char* fontPath );

wFont* wFontAddToFont(const char* fontPath,
                      wFont* destFont );

wFont* wFontGetDefault();

#ifdef __cplusplus
void wFontDraw(wFont* font,
               const wchar_t* wcptrText,
               wVector2i fromPos,
               wVector2i toPos,
               wColor4s color=wCOLOR4s_WHITE);
#else
void wFontDraw(wFont* font,
               const wchar_t* wcptrText,
               wVector2i fromPos,
               wVector2i toPos,
               wColor4s color);
#endif // __cplusplus

void wFontDestroy(wFont* font);

wVector2u wFontGetTextSize(wFont* font,
                           const wchar_t* text);

void wFontSetKerningSize(wFont* font,
                         wVector2u kerning);

wVector2u wFontGetKerningSize(wFont* font);

Int32 wFontGetCharacterFromPos(wFont* font,
                             const wchar_t* text,
                             Int32 xPixel);

void wFontSetInvisibleCharacters(wFont* font,
                                 const wchar_t *s);
#ifdef __cplusplus
wFont* wFontLoadFromTTF(const char * fontPath,
                        UInt32 size,
                        bool antialias=false,
                        bool transparency=false);
#else
wFont* wFontLoadFromTTF(char * fontPath,
                        UInt32 size,
                        bool antialias,
                        bool transparency);
#endif // __cplusplus

#ifdef __cplusplus
void wFontDrawAsTTF(wFont* font,
                    const wchar_t* wcptrText,
                    wVector2i fromPos,
                    wVector2i toPos,
                    wColor4s color=wCOLOR4s_WHITE,
                    bool hcenter=false,
                    bool vcenter=false);
#else
void wFontDrawAsTTF(wFont* font,
                    const wchar_t* wcptrText,
                    wVector2i fromPos,
                    wVector2i toPos,
                    wColor4s color,
                    bool hcenter,
                    bool vcenter);
#endif // __cplusplus

///wImage//////
wImage* wImageLoad(const char* cptrFile );

bool wImageSave(wImage* img,
                const char* file);

wImage* wImageCreate(wVector2i size,
                     wColorFormat format );

void wImageDestroy(wImage* image );

UInt32* wImageLock(wImage* image );

void wImageUnlock(wImage* image );

wTexture* wImageConvertToTexture(wImage* img,
                                 const char* name);

wColor4s wImageGetPixelColor(wImage* img,
                             wVector2u pos);

#ifdef __cplusplus
void wImageSetPixelColor(wImage* img,
                         wVector2u pos,
                         wColor4s color,
                         bool blend=false);
#else
void wImageSetPixelColor(wImage* img,
                         wVector2u pos,
                         wColor4s color,
                         bool blend);
#endif // __cplusplus

void wImageGetInformation(wImage* image,
                          wVector2u* size,
                          UInt32* pitch,
                          wColorFormat* format);

///wTimer///
Float32 wTimerGetDelta();

UInt32 wTimerGetTime();

wRealTimeDate wTimerGetRealTimeAndDate();

//Returns current real time in milliseconds of the system
UInt32 wTimerGetRealTime();

//set the current time in milliseconds//
void wTimerSetTime(UInt32 newTime );

//Returns if the virtual timer is currently stopped
bool wTimerIsStopped();

//Sets the speed of the timer
void wTimerSetSpeed(Float32 speed);

//Starts the virtual time
void wTimerStart();

//Stops the virtual timer
void wTimerStop();

//Advances the virtual time
void wTimerTick();

///wLog///
#ifdef __cplusplus
void wLogSetLevel(wLoggingLevel level=wLL_INFORMATION);
#else
void wLogSetLevel(wLoggingLevel level);
#endif // __cplusplus

void wLogSetFile(const char* path);

void wLogClear(const char* path);

#ifdef __cplusplus
void wLogWrite(const wchar_t* hint,
               const wchar_t* text,
               const char* path=0,
               UInt32 mode=1); //mode=0/1
#else
void wLogWrite(const wchar_t* hint,
               const wchar_t* text,
               const char* path,
               UInt32 mode);
#endif // __cplusplus

///wSystem////
void wSystemSetClipboardText(const char* text);

const char* wSystemGetClipboardText();

UInt32 wSystemGetProcessorSpeed();

UInt32  wSystemGetTotalMemory();

UInt32  wSystemGetAvailableMemory();

wVector2i wSystemGetMaxTextureSize();

bool wSystemIsTextureFormatSupported(wColorFormat format);

void wSystemSetTextureCreationFlag(wTextureCreationFlag flag,
                                              bool value );

bool wSystemIsTextureCreationFlag(wTextureCreationFlag flag);

wTexture* wSystemCreateScreenShot(wVector2u minPos,
                                  wVector2u maxPos);

bool wSystemSaveScreenShot(const char* file );

///Get the current operation system version as string.
const char*	wSystemGetVersion();

///Check if a driver type is supported by the engine.
///Even if true is returned the driver may not be available for
///a configuration requested when creating the device.
bool wSystemIsDriverSupported(wDriverTypes testDriver);


///wDisplay///
///Get the graphics card vendor name.
const char* wDisplayGetVendor();

Int32 wDisplayModesGetCount();

Int32 wDisplayModeGetDepth(Int32 modeNumber);

wVector2u wDisplayModeGetResolution(Int32 ModeNumber);

wVector2u wDisplayGetCurrentResolution();

Int32 wDisplayGetCurrentDepth();

///Set the current Gamma Value for the Display.
void wDisplaySetGammaRamp(wColor3f gamma,float brightness,float contrast);

void wDisplayGetGammaRamp(wColor3f* gamma,float* brightness,float* contrast);

bool wDisplaySetDepth(UInt32 depth);

///wMath///
static const Float32 wMathPI = 3.14159265359f;

static const Float64 wMathPI64=3.1415926535897932384626433832795028841971693993751;

///Возвращает нормализованный вектор
wVector3f wMathVector3fNormalize(wVector3f source);

///Возвращает длину вектора
Float32 wMathVector3fGetLength(wVector3f vector);

///Get the rotations that would make a (0,0,1) direction vector
///point in the same direction as this direction vector.
wVector3f wMathVector3fGetHorizontalAngle(wVector3f vector);

///Возвращает инвертированный вектор (все координаты меняют знак)
wVector3f wMathVector3fInvert(wVector3f vector);

///Суммирует два вектора
wVector3f wMathVector3fAdd(wVector3f vector1,
                           wVector3f vector2);

///Вычитает из вектора 1 вектор 2
wVector3f wMathVector3fSubstract(wVector3f vector1,
                                 wVector3f vector2);

///Векторное произведение векторов
wVector3f wMathVector3fCrossProduct(wVector3f vector1,
                                    wVector3f vector2);
///Скалярное произведение векторов
Float32 wMathVector3fDotProduct(wVector3f vector1,
                              wVector3f vector2);

///Определяет кратчайшее расстояние между векторами
Float32 wMathVector3fGetDistanceFrom(wVector3f vector1,
                                   wVector3f vector2);

///Возвращает интерполированный вектор
wVector3f wMathVector3fInterpolate(wVector3f vector1,
                                  wVector3f vector2,
                                              Float64 d);

///Возвращает случайное число из интервала (first, last)
Float64 wMathRandomRange(Float64 first,
                        Float64 last);

///Из градусов- в радианы///
Float32 wMathDegToRad(Float32 degrees);

///Из радиан- в градусы
Float32 wMathRadToDeg(Float32 radians);

///Математически правильное округление///
Float32 wMathRound(Float32 value);

///Округление в большую сторону///
Int32 wMathCeil(Float32 value);

///Округление в меньшую сторону///
Int32 wMathFloor(Float32 value);


///returns if a equals b, taking possible rounding errors into account
#ifdef __cplusplus
bool wMathFloatEquals(Float32 value1,
                      Float32 value2,
                      Float32 tolerance=0.000001f);
#else
bool wMathFloatEquals(Float32 value1,
                      Float32 value2,
                      Float32 tolerance);
#endif // __cplusplus

///returns if a equals b, taking an explicit rounding tolerance into account
#ifdef __cplusplus
bool wMathIntEquals(Int32 value1,
                    Int32 value2,
                    Int32 tolerance=0);
#else
bool wMathIntEquals(Int32 value1,
                    Int32 value2,
                    Int32 tolerance);
#endif // __cplusplus

///returns if a equals b, taking an explicit rounding tolerance into account
#ifdef __cplusplus
bool wMathUIntEquals(UInt32 value1,
                     UInt32 value2,
                     UInt32 tolerance=0);
#else
bool wMathUIntEquals(UInt32 value1,
                     UInt32 value2,
                     UInt32 tolerance);
#endif // __cplusplus

///returns if a equals zero, taking rounding errors into account
#ifdef __cplusplus
bool wMathFloatIsZero(Float32 value,
                      Float32 tolerance=0.000001f);
#else
bool wMathFloatIsZero(Float32 value,
                      Float32 tolerance);
#endif // __cplusplus

///returns if a equals zero, taking rounding errors into account
#ifdef __cplusplus
bool wMathIntIsZero(Int32 value,
                    Int32 tolerance=0);
#else
bool wMathIntIsZero(Int32 value,
                    Int32 tolerance);
#endif // __cplusplus

///returns if a equals zero, taking rounding errors into account
#ifdef __cplusplus
bool wMathUIntIsZero(UInt32 value,
                     UInt32 tolerance=0);
#else
bool wMathUIntIsZero(UInt32 value,
                     UInt32 tolerance);
#endif // __cplusplus

///Возвращает больший Float32 из двух///
Float32 wMathFloatMax2(Float32 value1,
                     Float32 value2);

///Возвращает больший Float32 из трех///
Float32 wMathFloatMax3(Float32 value1,
                     Float32 value2,
                     Float32 value3);

///Возвращает больший Int32 из двух///
Float32 wMathIntMax2(Int32 value1,
                     Int32 value2);

///Возвращает больший Int32 из трех///
Float32 wMathIntMax3(Int32 value1,
                   Int32 value2,
                   Int32 value3);

///Возвращает меньший Float32 из двух///
Float32 wMathFloatMin2(Float32 value1,
                     Float32 value2);

///Возвращает меньший Float32 из трех///
Float32 wMathFloatMin3(Float32 value1,
                   Float32 value2,
                   Float32 value3);

///Возвращает меньший Int32 из двух///
Float32 wMathIntMin2(Int32 value1,
                     Int32 value2);

///Возвращает меньший Int32 из трех///
Float32 wMathIntMin3(Int32 value1,
                   Int32 value2,
                   Int32 value3);

///wUtil///
#ifdef __cplusplus
/// Конвертирует трехмерный вектор с float-компонентами в строку с разделителем s
const char* wUtilVector3fToStr(wVector3f vector,
                               const char* s=";",
                               bool addNullChar=false);
#else
const char* wUtilVector3fToStr(wVector3f vector,
                               const char* s,
                               bool addNullChar);
#endif // __cplusplus

#ifdef __cplusplus
/// Конвертирует двумерный вектор с float-компонентами в строку с разделителем s
const char* wUtilVector2fToStr(wVector2f vector,
                               const char* s=";",
                               bool addNullChar=false);
#else
const char* wUtilVector2fToStr(wVector2f vector,
                               const char* s,
                               bool addNullChar);
#endif // __cplusplus

#ifdef __cplusplus
///Конвертирует цвет с UInt8-компонентами в строку с разделителем s
const char* wUtilColor4sToStr(wColor4s color,
                              const char* s=";",
                              bool addNullChar=false);
#else
const char* wUtilColor4sToStr(wColor4s color,
                              const char* s,
                              bool addNullChar);
#endif // __cplusplus

#ifdef __cplusplus
///Конвертирует цвет с float-компонентами в строку с разделителем s
const char* wUtilColor4fToStr(wColor4f color,
                              const char* s=";",
                              bool addNullChar=false);
#else
const char* wUtilColor4fToStr(wColor4f color,
                              const char* s,
                              bool addNullChar);
#endif // __cplusplus

UInt32 wUtilColor4sToUInt(wColor4s color);

UInt32 wUtilColor4fToUInt(wColor4f color);

wColor4s wUtilUIntToColor4s(UInt32 color);

wColor4f wUtilUIntToColor4f(UInt32 color);

///Convert a simple string of base 10 digits into a signed 32 bit integer.
Int32 wUtilStrToInt(const char* str);

#ifdef __cplusplus
const char* wUtilIntToStr(Int32 value,
                         bool addNullChar=false);
#else
const char* wUtilIntToStr(Int32 value,
                          bool addNullChar);
#endif // __cplusplus

///Converts a sequence of digits into a whole positive floating point value.
///Only digits 0 to 9 are parsed.
///Parsing stops at any other character, including sign characters or a decimal point.
Float32 wUtilStrToFloat(const char* str);

#ifdef __cplusplus
///Конвертирует Float32 в строку
const char* wUtilFloatToStr(Float32 value,
                            bool addNullChar=false);
#else
const char* wUtilFloatToStr(Float32 value,
                            bool addNullChar);
#endif // __cplusplus

///Convert a simple string of base 10 digits into an unsigned 32 bit integer.
UInt32 wUtilStrToUInt(const char* str);

#ifdef __cplusplus
///Конвертирует UInt32 в строку
const char* wUtilUIntToStr(UInt32 value,
                           bool addNullChar=false);
#else
const char* wUtilUIntToStr(UInt32 value,
                           bool addNullChar);
#endif // __cplusplus

///swaps the content of the passed parameters
void wUtilSwapInt(int* value1,
                  int* value2);

///swaps the content of the passed parameters
void wUtilSwapUInt(UInt32* value1,
                   UInt32* value2);

///swaps the content of the passed parameters
void wUtilSwapFloat(float* value1,
                    float* value2);

///Конвертирует расширенную строку в С-строку
const char* wUtilWideStrToStr(const wchar_t* str);

///Конвертирует С-строку в расширенную строку
const wchar_t* wUtilStrToWideStr(const char* str);

///Добавляет символ конца строки
void wUtilStrAddNullChar(const char** str);

///Добавляет символ конца строки к расширенной строке
void wUtilWideStrAddNullChar(const wchar_t** str);

///wEngine///
void wEngineSetClipboardText(const wchar_t* text);

const wchar_t* wEngineGetClipboardText();

bool wEngineLoadCreationParameters(wEngineCreationParameters* outParams,
                                   const char* xmlFilePath);

bool wEngineSaveCreationParameters(wEngineCreationParameters* inParams,
                                   const char* xmlFilePath);

#ifdef __cplusplus
bool wEngineStartWithGui(const wchar_t* captionText,
                         const char* fontPath=0,
                         const char* logo=0,
                         wLanguage lang=wL_EN,
                         wEngineCreationParameters* outParams=0,
                         const char* inXmlConfig=0);
#else
bool wEngineStartWithGui(const wchar_t* captionText,
                         const char* fontPath,
                         const char* logo,
                         wLanguage lang,
                         wEngineCreationParameters* outParams,
                         const char* inXmlConfig);
#endif // __cplusplus

#ifdef __cplusplus
bool wEngineStart(wDriverTypes iDevice=wDRT_OPENGL,
                  wVector2u size=wDEFAULT_SCREENSIZE,
                  UInt32 iBPP=32,
                  bool boFullscreen=false,
                  bool boShadows=true,
                  bool boCaptureEvents=true,
                  bool vsync=true);
#else
bool wEngineStart(wDriverTypes iDevice,
                  wVector2u size,
                  UInt32 iBPP,
                  bool boFullscreen,
                  bool boShadows,
                  bool boCaptureEvents,
                  bool vsync);
#endif // __cplusplus

void wEngineCloseByEsc();

bool wEngineStartAdvanced(wEngineCreationParameters params);

void wEngineSetTransparentZWrite (bool value);

bool wEngineRunning();

#ifdef __cplusplus
///Pause execution and let other processes to run for a specified amount of time.
void wEngineSleep(UInt32 Ms,
                  bool pauseTimer=false);
#else
void wEngineSleep(UInt32 Ms,
                  bool pauseTimer);
#endif

///Cause the device to temporarily pause execution and let other processes run.
void wEngineYield();

void wEngineSetViewPort(wVector2i fromPos,
                        wVector2i toPos);

bool wEngineIsQueryFeature(wVideoFeatureQuery  feature);

void wEngineDisableFeature(wVideoFeatureQuery feature,
                           bool flag);

#ifdef __cplusplus
bool wEngineStop(bool closeDevice=true);
#else
bool wEngineStop(bool closeDevice);
#endif // __cplusplus

void wEngineSetFPS(UInt32 limit);

wMaterial* wEngineGetGlobalMaterial();

/*
Get the 2d override material for altering its values.
The 2d override materual allows to alter certain render states of the 2d methods.
Not all members of SMaterial are honored, especially not MaterialType and Textures.
Moreover, the zbuffer is always ignored, and lighting is always off.
All other flags can be changed, though some might have to effect in most cases.
Please note that you have to enable/disable this effect with enableInitMaterial2D().
This effect is costly, as it increases the number of state changes considerably.
Always reset the values when done.
*/
wMaterial* wEngineGet2dMaterial();

void wEngineSet2dMaterial(bool value);

Int32 wEngineGetFPS();

void wEngineShowLogo(bool value);

///wScene/////
#ifdef __cplusplus
bool wSceneBegin (wColor4s color=wCOLOR4s_BLACK);
#else
bool wSceneBegin (wColor4s color);
#endif // __cplusplus

#ifdef __cplusplus
bool wSceneBeginAdvanced(wColor4s backColor=wCOLOR4s_BLACK,
						bool clearBackBuffer=true,
						bool clearZBuffer=true);
#else
bool wSceneBeginAdvanced(wColor4s backColor,
						bool clearBackBuffer,
						bool clearZBuffer);
#endif // __cplusplus

void wSceneLoad(const char* filename );

void wSceneSave(const char* filename );

void wSceneDrawAll();

bool wSceneEnd();

void wSceneDrawToTexture (wTexture* renderTarget );

#ifdef __cplusplus
void wSceneSetRenderTarget(wTexture*renderTarget,
                           wColor4s backColor=wCOLOR4s_BLACK,
                           bool clearBackBuffer=true,
                           bool clearZBuffer=true);
#else
void wSceneSetRenderTarget(wTexture*renderTarget,
                           wColor4s backColor,
                           bool clearBackBuffer,
                           bool clearZBuffer);
#endif // __cplusplus

void wSceneSetAmbientLight(wColor4f color);

wColor4f wSceneGetAmbientLight();

void wSceneSetShadowColor(wColor4s color);

wColor4s wSceneGetShadowColor();

#ifdef __cplusplus
void wSceneSetFog(wColor4s color,
				  wFogType fogtype,
                  Float32 start,
                  Float32 end,
                  Float32 density,
                  bool pixelFog=false,
                  bool rangeFog=false);
#else
void wSceneSetFog(wColor4s color,
                  wFogType fogtype,
                  Float32 start,
                  Float32 end,
                  Float32 density,
                  bool pixelFog,
                  bool rangeFog);
#endif // __cplusplus

void wSceneGetFog(wColor4s* color,
                  wFogType* fogtype,
                  float* start,
                  float* end,
                  float* density,
                  bool*  pixelFog,
                  bool*  rangeFog);

wNode* wSceneGetActiveCamera();

///Поиск текстуры по АБСОЛЮТНОМУ пути
///Если требуется искать по относительному пути,
///используйте сначала wFileGetAbsolutePath
wTexture* wSceneGetTextureByName(const char* name);

// When animating a mesh by "Morphing" or "Skeletal Animation" such as "*.md3", "*.x" and "*.b3d" using "Shaders" for rendering we can improve the final render if we "Cyclically Update" the "Tangents" and "Binormals"..
// We presume that our meshes are, among others, textured with a "NORMAL MAP" used by the "Shader" (cg, hlsl, or glsl etc) in calculating diffuse and specular.
// We also have one or more lights used by the shader.

// Update TANGENTS & BINORMALS at every frame for a skinned animation..

 // We dont want to do this for static meshes like levels etc..
 // We also dont want to do it for Rotating, Scaled and translated meshes..(we can however, as a bonus, scale, rotate and translate these)
 // Only for animated skinned and morph based meshes..
 // This is loose code that works. If anyone can improve it for the engine itself that would be great..
 // You'll probably ID possible improvements immediately!

 // At every N'th Frame we loop through all the vertices..
 // 1. In the loop we Access the VERTEX of POINT A of the "INDEXED TRIANGLE"..
 // 2. We interrogate the "OTHER TWO" VERTICES (which thankfully do change at each frame) for their Positions, Normals, and UV Coords to
 //    Genertate a "BRAND NEW" (animated) TANGENT and BINORMAL. (We may want to calculate the the "Binormal" in the SHADER to save time)
 // 3. We REWRITE the Tangent and Binormal for our SELECTED TRIANGLE POINT.
 // 4. We DO THE SAME for POINTS B and C..
 //

 //  GENERATE "LIVING" TANGENTS & BINBORMALS
 //  REMEMBER!
 //  WE NEED "LOOP THROUGH ALL ITS BUFFERS"
 //  WE NEED "LOOP THROUGH ALL THOSE BUFFER VERTICES"
 // Possible types of (animated) meshes.
 // Enumerator:
 // 1  EAMT_MD2            Quake 2 MD2 model file..
 // 2  EAMT_MD3            Quake 3 MD3 model file..
 // 10 EAMT_MDL_HALFLIFE   Halflife MDL model file..
 // Below is what an item type must be for it to qualify for Tangent Updates..
 // 11 EAMT_SKINNED        generic skinned mesh "*.x" "*.b3d" etc.. (see Morphed too!)
 //
 // We want to change tangents for skinned meshes only so we must determine which ones are "Skinned"..
 // This may change if we add and remove meshes during runtime..

void wMeshUpdateTangentsAndBinormals(wMesh* mesh);

void wSceneDestroyAllTextures();

void wSceneDestroyAllNodes();

///Можно для поиска меша использовать относительный путь
wMesh* wSceneGetMeshByName(const char* name);

wMesh* wSceneGetMeshByIndex(unsigned int index);

UInt32 wSceneGetMeshesCount();

void wSceneDestroyAllMeshes();

bool wSceneIsMeshLoaded(const char* filePath);

void wSceneDestroyAllUnusedMeshes();

UInt32 wSceneGetPrimitivesDrawn();

UInt32 wSceneGetNodesCount();

wNode* wSceneGetNodeById(Int32 id );

wNode* wSceneGetNodeByName(const char* name );

wNode* wSceneGetRootNode();

///wWindow///
void wWindowSetCaption(const wchar_t* wcptrText);

void wWindowGetSize(wVector2u* size);

bool wWindowIsFullscreen();

bool wWindowIsResizable();

bool wWindowIsActive();

bool wWindowIsFocused();

bool wWindowIsMinimized();

void wWindowMaximize();

void wWindowMinimize();

void wWindowRestore();

void wWindowSetResizable(bool resizable);

void wWindowMove(wVector2u pos);

void wWindowPlaceToCenter();

void wWindowResize(wVector2u newSize);

void wWindowSetFullscreen(bool value);

bool wWindowSetDepth(UInt32 depth);
///wPostEffect///
#ifdef __cplusplus
wPostEffect* wPostEffectCreate(wPostEffectId effectnum,
                               wPostEffectQuality quality,
                               Float32 value1=FLOAT_DEFAULTVALUE,
                               Float32 value2=FLOAT_DEFAULTVALUE,
                               Float32 value3=FLOAT_DEFAULTVALUE,
                               Float32 value4=FLOAT_DEFAULTVALUE,
                               Float32 value5=FLOAT_DEFAULTVALUE,
                               Float32 value6=FLOAT_DEFAULTVALUE,
                               Float32 value7=FLOAT_DEFAULTVALUE,
                               Float32 value8=FLOAT_DEFAULTVALUE);
#else
wPostEffect* wPostEffectCreate(wPostEffectId effectnum,
                               wPostEffectQuality quality,
                               Float32 value1,
                               Float32 value2,
                               Float32 value3,
                               Float32 value4,
                               Float32 value5,
                               Float32 value6,
                               Float32 value7,
                               Float32 value8);
#endif // __cplusplus

void wPostEffectDestroy(wPostEffect* ppEffect);

#ifdef __cplusplus
void wPostEffectSetParameters(wPostEffect* ppEffect,
                              Float32 para1,
                              Float32 para2=FLOAT_DEFAULTVALUE,
                              Float32 para3=FLOAT_DEFAULTVALUE,
                              Float32 para4=FLOAT_DEFAULTVALUE,
                              Float32 para5=FLOAT_DEFAULTVALUE,
                              Float32 para6=FLOAT_DEFAULTVALUE,
                              Float32 para7=FLOAT_DEFAULTVALUE,
                              Float32 para8=FLOAT_DEFAULTVALUE);
#else
void wPostEffectSetParameters(wPostEffect* ppEffect,
                              Float32 para1,
                              Float32 para2,
                              Float32 para3,
                              Float32 para4,
                              Float32 para5,
                              Float32 para6,
                              Float32 para7,
                              Float32 para8);
#endif // __cplusplus

void wPostEffectsDestroyAll();

///wXEffects///
#ifdef __cplusplus
void wXEffectsStart(bool vsm,
                    bool softShadows,
                    bool bitDepth32,
                    wColor4s color=wCOLOR4s_WHITE);
#else
void wXEffectsStart(bool vsm,
                    bool softShadows,
                    bool bitDepth32,
                    wColor4s color);
#endif // __cplusplus


void wXEffectsEnableDepthPass(bool enable);

#ifdef __cplusplus
void wXEffectsAddPostProcessingFromFile(const char* name,
                                        Int32 effectType=1);
#else
void wXEffectsAddPostProcessingFromFile(const char* name,
                                        Int32 effectType);
#endif // __cplusplus

void wXEffectsSetPostProcessingUserTexture(wTexture* texture );

void wXEffectsAddShadowToNode(wNode* node,
                              wFilterType filterType,
                              wShadowMode shadowType);

void wXEffectsRemoveShadowFromNode(wNode* node );

void wXEffectsExcludeNodeFromLightingCalculations(wNode* node );

void wXEffectsAddNodeToDepthPass(wNode* node );

void wXEffectsSetAmbientColor(wColor4s color);

void wXEffectsSetClearColor(wColor4s color);

void wXEffectsAddShadowLight(UInt32 shadowDimen,
                             wVector3f position,
                             wVector3f target,
                             wColor4f color,
                             Float32 lightNearDist ,
                             Float32 lightFarDist,
                             Float32 angleDeg);

UInt32 wXEffectsGetShadowLightsCount();

#ifdef __cplusplus
wTexture* wXEffectsGetShadowMapTexture(UInt32 resolution,
                                       bool secondary=false);
#else
wTexture* wXEffectsGetShadowMapTexture(UInt32 resolution,
                                       bool secondary);
#endif // __cplusplus

wTexture* wXEffectsGetDepthMapTexture();

void wXEffectsSetScreenRenderTargetResolution(wVector2u size);

void wXEffectsSetShadowLightPosition(UInt32 index,
                                     wVector3f position);

wVector3f wXEffectsGetShadowLightPosition(UInt32 index);

void wXEffectsSetShadowLightTarget(UInt32 index,
                                   wVector3f target);

wVector3f wXEffectsGetShadowLightTarget(UInt32 index);

void wXEffectsSetShadowLightColor(UInt32 index,
                                  wColor4f color);

wColor4f wXEffectsGetShadowLightColor(UInt32 index);

void wXEffectsSetShadowLightMapResolution(UInt32 index,
                                          UInt32 resolution);

UInt32 wXEffectsGetShadowLightMapResolution(UInt32 index);

Float32 wXEffectsGetShadowLightFarValue(UInt32 index);

///wAnimator///
wAnimator* wAnimatorFollowCameraCreate(wNode* node,
                                       wVector3f position);

#ifdef __cplusplus
wAnimator* wAnimatorCollisionResponseCreate(wSelector* selector,
                                            wNode* node,
                                            Float32 slidingValue = 0.0005f );
#else
wAnimator* wAnimatorCollisionResponseCreate(wSelector* selector,
                                            wNode* node,
                                            Float32 slidingValue);
#endif // __cplusplus

void wAnimatorCollisionResponseSetParameters(wAnimator* anim,
                                             wAnimatorCollisionResponse params);

void wAnimatorCollisionResponseGetParameters(wAnimator* anim,
                                             wAnimatorCollisionResponse* params);

wAnimator* wAnimatorDeletingCreate(wNode* node,
                                   Int32 delete_after );

#ifdef __cplusplus
wAnimator* wAnimatorFlyingCircleCreate(wNode* node,
                                       wVector3f pos,
                                       Float32 radius=100.0f,
                                       Float32 speed=0.001f,
                                       wVector3f direction={0.0f,1.0f,0.0f},
                                       Float32 startPos=0.0f,
                                       Float32 radiusEllipsoid=0.0f );
#else
wAnimator* wAnimatorFlyingCircleCreate(wNode* node,
                                       wVector3f pos,
                                       Float32 radius,
                                       Float32 speed,
                                       wVector3f direction,
                                       Float32 startPos,
                                       Float32 radiusEllipsoid);
#endif

wAnimator *wAnimatorFlyingStraightCreate(wNode* node,
                                         wVector3f startPoint,
                                         wVector3f endPoint,
                                         UInt32 time,
                                         bool loop );

wAnimator* wAnimatorRotationCreate(wNode* node,
                                   wVector3f pos);

wAnimator* wAnimatorSplineCreate(wNode* node,
                                 Int32 iPoints,
                                 wVector3f *points,
                                 Int32 time,
                                 Float32 speed,
                                 Float32 tightness);

wAnimator* wAnimatorFadingCreate(wNode* node,
                                 Int32 delete_after,
                                 Float32 scale );

void wAnimatorDestroy(wNode* node,
                      wAnimator* anim );

///wTpsCamera///
wNode* wTpsCameraCreate(const char* name);

void wTpsCameraDestroy(wNode* ctrl);

void wTpsCameraUpdate(wNode* ctrl);

void wTpsCameraSetTarget(wNode* ctrl,
                         wNode* node);

void wTpsCameraRotateHorizontal(wNode* ctrl,
                                Float32 rotVal);

void wTpsCameraRotateVertical(wNode* ctrl,
                              Float32 rotVal);

void wTpsCameraSetHorizontalRotation(wNode* ctrl,
                                     Float32 rotVal);

void wTpsCameraSetVerticalRotation(wNode* ctrl,
                                   Float32 rotVal);

void wTpsCameraZoomIn(wNode* ctrl);

void wTpsCameraZoomOut(wNode* ctrl);

wNode* wTpsCameraGetCamera(wNode* ctrl);

void wTpsCameraSetCurrentDistance(wNode* ctrl,
                                  Float32 dist);

void wTpsCameraSetRelativeTarget(wNode* ctrl,
                                 wVector3f target);

void wTpsCameraSetDefaultDistanceDirection(wNode* ctrl,
                                           wVector3f dir);

void wTpsCameraSetMaximalDistance(wNode* ctrl,
                                 Float32 value);

void wTpsCameraSetMinimalDistance(wNode* ctrl,
                                  Float32 value);

void wTpsCameraSetZoomStepSize(wNode* ctrl,
                               Float32 value);

void wTpsCameraSetHorizontalSpeed(wNode* ctrl,
                                  Float32 value);

void wTpsCameraSetVerticalSpeed(wNode* ctrl,
                                Float32 value);
///wFpsCamera///
#ifdef __cplusplus
wNode*  wFpsCameraCreate(Float32 rotateSpeed=100.0f,
                         Float32 moveSpeed=0.1f,
                         wKeyMap* keyMapArray=&wKeyMapDefault[0],
                         Int32 keyMapSize=8,
                         bool noVerticalMovement=false,
                         Float32 jumpSpeed=0.0f);
#else
wNode*  wFpsCameraCreate(Float32 rotateSpeed,
                         Float32 moveSpeed,
                         wKeyMap* keyMapArray,
                         Int32 keyMapSize,
                         bool noVerticalMovement,
                         Float32 jumpSpeed);
#endif // __cplusplus

Float32 wFpsCameraGetSpeed(wNode* camera);

void wFpsCameraSetSpeed(wNode* camera,
                        Float32 newSpeed);

Float32 wFpsCameraGetRotationSpeed(wNode* camera);

void wFpsCameraSetRotationSpeed(wNode* camera,
                                Float32 rotSpeed);

void wFpsCameraSetKeyMap(wNode* camera,
                         wKeyMap* map,
                         UInt32 count);

void wFpsCameraSetVerticalMovement(wNode* camera,
                                   bool value);

void wFpsCameraSetInvertMouse(wNode* camera,
                              bool value);

void wFpsCameraSetMaxVerticalAngle(wNode* camera,
                                   float newValue);

Float32 wFpsCameraGetMaxVerticalAngle(wNode* camera);

///wCamera///
wNode*  wCameraCreate(wVector3f pos,
                      wVector3f target);

wNode* wMayaCameraCreate(Float32 rotateSpeed,
                         Float32 zoomSpeed,
                         Float32 moveSpeed);

void wCameraSetTarget(wNode* camera,
                      wVector3f target);

wVector3f wCameraGetTarget(wNode* camera);

wVector3f wCameraGetUpDirection(wNode* camera);

void wCameraSetUpDirection(wNode* camera,
                           wVector3f upDir);

void wCameraGetOrientation(wNode* camera,
                           wVector3f* upDir,
                           wVector3f* forwardDir,
                           wVector3f* rightDir);

#ifdef __cplusplus
void wCameraSetClipDistance(wNode* camera,
                            Float32 farDistance,
                            Float32 nearDistance=1.f);
#else
void wCameraSetClipDistance(wNode* camera,
                            Float32 farDistance,
                            Float32 nearDistance);
#endif // __cplusplus

void wCameraSetActive(wNode* camera);

void wCameraSetFov(wNode* camera,
                   Float32 fov );

Float32 wCameraGetFov(wNode* camera);

void wCameraSetOrthogonal(wNode* camera,
                          wVector3f vec);

void wCameraRevolve(wNode* camera,
                    wVector3f angleDeg,
                    wVector3f offset);

void wCameraSetUpAtRightAngle(wNode* camera );

void wCameraSetAspectRatio(wNode* camera,
                           Float32 aspectRatio );

void wCameraSetInputEnabled(wNode* camera,
                            bool value);

bool wCameraIsInputEnabled(wNode* camera);

#ifdef __cplusplus
void wCameraSetCollisionWithScene(wNode* camera,
                                  wVector3f radius,
                                  wVector3f gravity={0,-10,0},
                                  wVector3f offset={0,0,0},
                                  Float32 slidingValue=0.0005f);
#else
void wCameraSetCollisionWithScene(wNode* camera,
                                  wVector3f radius,
                                  wVector3f gravity,
                                  wVector3f offset,
                                  Float32 slidingValue);
#endif // __cplusplus

///wRtsCamera///
#ifdef __cplusplus
wNode* wRtsCameraCreate(wVector3f pos,
                        wVector2f offsetX,
                        wVector2f offsetZ,
                        wVector2f offsetDistance={20.f,40.f},
                        wVector2f offsetAngle={60.f,88.f},
                        Float32 driftSpeed=100.f,
                        Float32 scrollSpeed=5.f,
                        Float32 mouseSpeed=5.f,
                        Float32 orbit=0.f,
                        wMouseButtons mouseButtonActive=wMB_RIGHT);///use only wMB_LEFT/wMB_RIGHT/wMB_MIDDLE
#else
wNode* wRtsCameraCreate(wVector3f pos,
                        wVector2f offsetX,
                        wVector2f offsetZ,
                        wVector2f offsetDistance,
                        wVector2f offsetAngle,
                        Float32 driftSpeed,
                        Float32 scrollSpeed,
                        Float32 mouseSpeed,
                        Float32 orbit,
                        wMouseButtons mouseButtonActive);
#endif // __cplusplus


///wCollision///
wSelector* wCollisionGroupCreate();

void wCollisionGroupAddCollision(wSelector* group,
                                 wSelector* selector );

void wCollisionGroupRemoveAll(wSelector* geoup );

void wCollisionGroupRemoveCollision(wSelector* group,
                                    wSelector* selector );

wSelector* wCollisionCreateFromMesh(wMesh* mesh,
                                    wNode* node,
                                    Int32 iframe );

wSelector* wCollisionCreateFromStaticMesh(wMesh* staticMesh,
                                        wNode* node);

wSelector* wCollisionCreateFromBatchingMesh(wMesh* mesh,
                                            wNode* node);

wSelector* wCollisionCreateFromMeshBuffer(wMeshBuffer* meshbuffer,
                                          wNode* node);

#ifdef __cplusplus
wSelector* wCollisionCreateFromOctreeMesh(wMesh* mesh,
                                          wNode* node,
                                          Int32 iframe=0);
#else
wSelector* wCollisionCreateFromOctreeMesh(wMesh* mesh,
                                          wNode* node,
                                          Int32 iframe);
#endif

wSelector* wCollisionCreateFromBox(wNode*  node );

wSelector* wCollisionCreateFromTerrain(wNode*  node,
                                       Int32 level_of_detail);

wNode* wCollisionGetNodeFromCamera(wNode* camera );

wNode* wCollisionGetNodeFromRay(wVector3f* vectorStart,
                                wVector3f* vectorEnd );

wNode* wCollisionGetNodeChildFromRay(wNode* node,
                                     Int32 id,
                                     bool recurse,
                                     wVector3f* vectorStart,
                                     wVector3f* vectorEnd);

wNode* wCollisionGetNodeAndPointFromRay(wVector3f* vectorStart,
                                        wVector3f* vectorEnd,
                                        wVector3f* colPoint,
                                        wVector3f* normal,
                                        Int32 id,
                                        wNode* rootNode );

#ifdef __cplusplus
wNode* wCollisionGetNodeFromScreen(wVector2i screenPos,
                                   Int32 idBitMask=0,
                                   bool bNoDebugObjects=false,
                                   wNode* root=0);
#else
wNode* wCollisionGetNodeFromScreen(wVector2i screenPos,
                                   Int32 idBitMask,
                                   bool bNoDebugObjects,
                                   wNode* root);
#endif // __cplusplus

wVector2i wCollisionGetScreenCoordFrom3dPosition(wVector3f pos);

void wCollisionGetRayFromScreenCoord(wNode* camera,
                                     wVector2i screenCoord,
                                     wVector3f* vectorStart,
                                     wVector3f* vectorEnd );

wVector3f wCollisionGet3dPositionFromScreen(wNode* camera,
                                            wVector2i screenPos,
                                            wVector3f normal,
                                            Float32 distanceFromOrigin);

wVector2f wCollisionGet2dPositionFromScreen(wNode* camera,
                                            wVector2i screenPos);

bool wCollisionGetPointFromRay(wSelector* ts,
                               wVector3f* vectorStart,
                               wVector3f* vectorEnd,
                               wVector3f* collisionPoint,
                               wVector3f* vectorNormal,
                               wTriangle* collisionTriangle,
                               wNode** collNode);

wNode* wCollisionGetNodeChildFromPoint(wNode* node,
                                       Int32 id,
                                       bool recurse,
                                       wVector3f* vectorPoint );

void wCollisionGetResultPosition(wSelector* selector,
								 wVector3f* ellipsoidPosition,
								 wVector3f* ellipsoidRadius,
								 wVector3f* velocity,
								 wVector3f* gravity,
								 Float32 slidingSpeed,
								 wVector3f* outPosition,
								 wVector3f* outHitPosition,
								 int* outFalling);

///wFile///
void wFileAddZipArchive(const char* cptrFile,
                        bool boIgnoreCase,
                        bool boIgnorePaths);

void wFileAddArchive(const char *cptrFile,
                     bool boIgnoreCase,
                     bool boIgnorePaths,
#ifdef __cplusplus
                     wFileArchiveType aType=wFAT_UNKNOWN,
                     const char* password=""
#else
                     wFileArchiveType aType,
                     const char* password
#endif // __cplusplus
);

void wFileSetWorkingDirectory(const char* cptrPath );

const char* wFileGetWorkingDirectory();

void wFileAddPakArchive(const char* cptrFile,
                        bool boIgnoreCase,
                        bool boIgnorePaths );

void wFileAddDirectory(const char* cptrFile,
                       bool boIgnoreCase,
                       bool boIgnorePaths );

bool wFileIsExist(const char* cptrFile );

const char* wFileGetAbsolutePath(const char* cptrPath);

const char* wFileGetRelativePath(const char* cptrPath,
                                 const char* directory);

///Get the base part of a filename, i.e. the name without the directory part.
///If no directory is prefixed, the full name is returned.
const char* wFileGetBaseName(const char* cptrPath,
                            bool keepExtension);

const char* wFileGetDirectory(const char*cptrPath);

///for read///
wFile* wFileOpenForRead(const char* cptrFile );

Int32 wFileRead(wFile* file,
              void* buffer,
              UInt32 sizeToRead);

Int64 wFileGetSize(wFile* file);

///for write///
wFile* wFileCreateForWrite(const char* cptrFile,
                           bool append );
Int32 wFileWrite(wFile* file,
               const void* buffer,
               UInt32 sizeToWrite);

///for read/write///
const char* wFileGetName(wFile* file);

Int64 wFileGetPos(wFile* file);

bool wFileSeek(wFile* file,
               Int64 finalPos,
               bool relativeMovement);

void wFileClose(wFile* file);

///XMLReader///
wXmlReader* wXmlReaderCreate(const char* cptrFile );

wXmlReader* wXMLReaderCreateUTF8(const char* cptrFile );

//Returns attribute count of the current XML node
UInt32 wXmlGetAttributesCount(wXmlReader* xml);

//Returns the value of an attribute
const wchar_t* wXmlGetAttributeNameByIdx(wXmlReader* xml,
                                         Int32 idx);

//Returns the value of an attribute
const wchar_t* wXmlGetAttributeValueByIdx(wXmlReader* xml,
                                          Int32 idx);

//Returns the value of an attribute
const wchar_t* wXmlGetAttributeValueByName(wXmlReader* xml,
                                           const wchar_t* name);

//Returns the value of an attribute as float
Float32 wXmlGetAttributeValueFloatByIdx(wXmlReader* xml,
                                      Int32 idx);

//Returns the value of an attribute as float
Float32 wXmlGetAttributeValueFloatByName(wXmlReader* xml,
                                       const wchar_t* name);

//Returns the value of an attribute as integer
Int32 wXmlGetAttributeValueIntByIdx(wXmlReader* xml,
                                  Int32 idx);

//Returns the value of an attribute as integer
Int32 wXmlGetAttributeValueIntByName(wXmlReader* xml,
                                   const wchar_t* name);

//Returns the value of an attribute in a safe way
const wchar_t* wXmlGetAttributeValueSafeByName(wXmlReader* xml,
                                               const wchar_t* name);

 //Returns the name of the current node
const wchar_t* wXmlGetNodeName(wXmlReader* xml);

//Returns data of the current node
const wchar_t* wXmlGetNodeData(wXmlReader* xml);

//Returns format of the source xml file
wTextFormat wXmlGetSourceFormat(wXmlReader* xml);

//Returns format of the strings returned by the parser
wTextFormat wXmlGetParserFormat(wXmlReader* xml);

//Returns the type of the current XML node
wXmlNodeType wXmlGetNodeType(wXmlReader* xml);

//Returns if an element is an empty element, like <foo />
bool wXmlIsEmptyElement(wXmlReader* xml);

//Reads forward to the next xml node
bool wXmlRead(wXmlReader* xml);

void wXmlReaderDestroy(wXmlReader* xml);

///XmlWriter///
wXmlWriter* wXmlWriterCreate(const char* cptrFile );
//Writes the closing tag for an element. Like "</foo>"
void wXmlWriteClosingTag(wXmlWriter* xml,
                         const wchar_t* name);

//Writes a comment into the xml file
void wXmlWriteComment(wXmlWriter* xml,
                      const wchar_t* comment);

//Writes a line break
void  wXmlWriteLineBreak(wXmlWriter* xml);

//Writes a text into the file
void wXmlWriteText(wXmlWriter* xml,
                   const wchar_t* file);

//Writes an xml 1.0 heade
void  wXmlWriteHeader(wXmlWriter* xml);

#ifdef __cplusplus
void wXmlWriteElement(wXmlWriter* xml,
                      const wchar_t* name,
                      bool empty,
                      const wchar_t* attr1Name=0,const wchar_t* attr1Value=0,
                      const wchar_t* attr2Name=0,const wchar_t* attr2Value=0,
                      const wchar_t* attr3Name=0,const wchar_t* attr3Value=0,
                      const wchar_t* attr4Name=0,const wchar_t* attr4Value=0,
                      const wchar_t* attr5Name=0,const wchar_t* attr5Value=0);
#else
void wXmlWriteElement(wXmlWriter* xml,
                      const wchar_t* name,
                      bool empty,
                      const wchar_t* attr1Name,const wchar_t* attr1Value,
                      const wchar_t* attr2Name,const wchar_t* attr2Value,
                      const wchar_t* attr3Name,const wchar_t* attr3Value,
                      const wchar_t* attr4Name,const wchar_t* attr4Value,
                      const wchar_t* attr5Name,const wchar_t* attr5Value);
#endif // __cplusplus

void wXmlWriterDestroy(wXmlWriter* xml);

///wInput///
///keyboard///
///Get character without waiting for Return to be pressed.
bool wInputWaitKey();

bool wInputIsKeyEventAvailable();

wKeyEvent* wInputReadKeyEvent();

bool wInputIsKeyUp(wKeyCode num);

bool wInputIsKeyHit(wKeyCode num);

bool wInputIsKeyPressed(wKeyCode num);

///mouse///
bool wInputIsMouseEventAvailable();

wMouseEvent* wInputReadMouseEvent();

void wInputSetCursorVisible(bool boShow );

bool wInputIsCursorVisible();

void wInputSetMousePosition(wVector2i* position);

void wInputGetMousePosition(wVector2i* position);

void wInputSetMouseLogicalPosition(wVector2f* position);

void wInputGetMouseLogicalPosition(wVector2f* position);

Float32 wInputGetMouseWheel();

void wInputGetMouseDelta(wVector2i* deltaPos);

bool wInputIsMouseUp(wMouseButtons num);

bool wInputIsMouseHit(wMouseButtons num);

bool wInputIsMousePressed(wMouseButtons num);

Int32 wInputGetMouseX();

Int32 wInputGetMouseY();

Int32 wInputGetMouseDeltaX();

Int32 wInputGetMouseDeltaY();

///joystick///
bool wInputActivateJoystick();

UInt32 wInputGetJoysitcksCount();

void wInputGetJoystickInfo(UInt32 joyIndex,
                           wJoystickInfo* joyInfo);

bool wInputIsJoystickEventAvailable();

wJoystickEvent* wInputReadJoystickEvent();


///wLight///
wNode* wLightCreate(wVector3f position,
                    wColor4f color,
                    Float32 radius);

void wLightSetAmbientColor(wNode* light,
                           wColor4f color);

wColor4f wLightGetAmbientColor(wNode* light);

void wLightSetSpecularColor(wNode* light,
                            wColor4f color);

wColor4f wLightGetSpecularColor(wNode* light);

void wLightSetAttenuation(wNode* light,
                          wVector3f  attenuation); //.x-constant, .y- linear, .z- quadratic

wVector3f wLightGetAttenuation(wNode* light);

void wLightSetCastShadows(wNode* light,
                          bool castShadows);

bool wLightIsCastShadows(wNode* light);

void wLightSetDiffuseColor(wNode* light,
                           wColor4f color);

wColor4f wLightGetDiffuseColor(wNode* light);

void wLightSetFallOff(wNode* light,
                      Float32 FallOff);

Float32 wLightGetFallOff(wNode* light);

void wLightSetInnerCone(wNode* light,
                        Float32 InnerCone);

Float32 wLightGetInnerCone(wNode* light);

void wLightSetOuterCone(wNode* light,
                        Float32 OuterCone);

Float32 wLightGetOuterCone(wNode* light);

void wLightSetRadius(wNode* light,
                     Float32 Radius );

Float32 wLightGetRadius(wNode* light);

void wLightSetType(wNode* light,
                   wLightType Type );

wLightType wLightGetType(wNode* light);

//Read-ONLY! Direction of the light.
//If Type is WLT_POINT, it is ignored.
//Changed via light scene node's rotation.
wVector3f wLightGetDirection(wNode* light);


///wBillboardGroup///
#ifdef __cplusplus
wNode* wBillboardGroupCreate(wVector3f position=wVECTOR3f_ZERO,
                             wVector3f rotation=wVECTOR3f_ZERO,
                             wVector3f scale=wVECTOR3f_ONE);
#else
wNode* wBillboardGroupCreate(wVector3f position,
                             wVector3f rotation,
                             wVector3f scale);
#endif // __cplusplus


void wBillboardGroupSetShadows(wNode* node,
                               wVector3f direction,
                               Float32 intensity,
                               Float32 ambient );

void wBillboardGroupResetShadows(wNode* node);

UInt32 wBillboardGroupGetSize(wNode* node );

wMeshBuffer* wBillboardGroupGetMeshBuffer(wNode *node);

wBillboard* wBillboardGroupGetFirstElement(wNode* node);

void wBillboardGroupUpdateForce(wNode* node );

wBillboard* wBillboardGroupAddElement(wNode* node,
                                 wVector3f position,
                                 wVector2f size,
                                 Float32 roll,
                                 wColor4s color);

wBillboard* wBillboardGroupAddElementByAxis(wNode* node,
                                       wVector3f position,
                                       wVector2f size,
                                       Float32 roll,
                                       wColor4s color,
                                       wVector3f axis);

void wBillboardGroupRemoveElement(wNode* node,
                               wBillboard* billboard);

///wBillboard///
wNode* wBillboardCreate(wVector3f position,
                        wVector2f size);

void wBillboardSetEnabledAxis(wNode* billboard,
                             wBillboardAxisParam param);

wBillboardAxisParam wBillboardGetEnabledAxis(wNode* billboard);

void wBillboardSetColor(wNode* node,
                        wColor4s topColor,
                        wColor4s bottomColor);

void wBillboardSetSize(wNode* node,
                       wVector2f size);

wNode* wBillboardCreateText(wVector3f position,
                            wVector2f size,
                            wFont* font,
                            const wchar_t* text,
                            wColor4s topColor,
                            wColor4s bottomColor);

///wSkyBox///
wNode* wSkyBoxCreate(wTexture* texture_up,
                     wTexture* texture_down,
                     wTexture* texture_left,
                     wTexture* texture_right,
                     wTexture* texture_front,
                     wTexture* texture_back );

///wSkyDome///
#ifdef __cplusplus
wNode* wSkyDomeCreate(wTexture* texture_file,
                      UInt32  horiRes,
                      UInt32  vertRes,
                      Float64  texturePercentage,
                      Float64  spherePercentage,
					  Float64 domeRadius=1000.0f);
#else
wNode* wSkyDomeCreate(wTexture* texture_file,
                      UInt32  horiRes,
                      UInt32  vertRes,
                      Float64  texturePercentage,
                      Float64  spherePercentage,
                      Float64 domeRadius);
#endif // __cplusplus


void wSkyDomeSetColor (wNode* dome,
                       wColor4s horizonColor,
                       wColor4s zenithColor);

void wSkyDomeSetColorBand(wNode* dome,
                          wColor4s horizonColor,
                          Int32 position,
                          Float32 fade,
                          bool additive );

void wSkyDomeSetColorPoint(wNode* dome,
                           wColor4s horizonColor,
                           wVector3f position,
                           Float32 radius,
                           Float32 fade,
                           bool additive );

///wLodManager///
wNode* wLodManagerCreate(UInt32 fadeScale,
                         bool useAlpha,
                         void (*callback)(UInt32, wNode*));

void wLodManagerAddMesh(wNode* node,
                        wMesh* mesh,
                        Float32 distance);

void wLodManagerSetMaterialMap(wNode* node,
                               wMaterialTypes source,
                               wMaterialTypes target );

///wZoneManager///
wNode* wZoneManagerCreate(Float32 initialNearDistance,
                          Float32 initialFarDistance );

void wZoneManagerSetProperties(wNode* node,
                               Float32 newNearDistance,
                               Float32 newFarDistance,
                               bool accumulateChildBoxes);

void wZoneManagerSetBoundingBox(wNode* node,
                                wVector3f position,
                                wVector3f size);

void wZoneManagerAddTerrain(wNode* node,
							wNode* terrainSource,
							const char* structureMap,
							const char* colorMap,
							const char* detailMap,
							wVector2i pos,
							Int32 sliceSize );

///wNode///
///primitives///
wNode* wNodeCreateEmpty();

#ifdef __cplusplus
wNode* wNodeCreateCube(Float32 size,
                       wColor4s color=wCOLOR4s_WHITE);
#else
wNode* wNodeCreateCube(Float32 size,
                       wColor4s color);
#endif // __cplusplus

#ifdef __cplusplus
wNode* wNodeCreateSphere(Float32 radius,
                         Int32 polyCount=32,
                         wColor4s color=wCOLOR4s_WHITE);
#else
wNode* wNodeCreateSphere(Float32 radius,
                         Int32 polyCount,
                         wColor4s color);
#endif // __cplusplus

#ifdef __cplusplus
wNode* wNodeCreateCylinder(UInt32 tesselation,
                           Float32 radius,
                           Float32 length,
                           wColor4s color=wCOLOR4s_WHITE);
#else
wNode* wNodeCreateCylinder(UInt32 tesselation,
                           Float32 radius,
                           Float32 length,
                           wColor4s color);
#endif // __cplusplus

#ifdef __cplusplus
wNode* wNodeCreateCone(UInt32 tesselation,
                       Float32 radius,
                       Float32 length,
                       wColor4s clorTop=wCOLOR4s_WHITE,
                       wColor4s clorBottom=wCOLOR4s_WHITE);
#else
wNode* wNodeCreateCone(UInt32 tesselation,
                       Float32 radius,
                       Float32 length,
                       wColor4s clorTop,
                       wColor4s clorBottom);
#endif // __cplusplus

#ifdef __cplusplus
wNode* wNodeCreatePlane(Float32 size,
                        UInt32 tileCount,
                        wColor4s color=wCOLOR4s_WHITE);
#else
wNode* wNodeCreatePlane(Float32 size,
                        UInt32 tileCount,
                        wColor4s color);
#endif // __cplusplus

wNode* wNodeCreateFromMesh(wMesh* mesh);

wNode* wNodeCreateFromStaticMesh(wMesh* mesh);

#ifdef __cplusplus
wNode* wNodeCreateFromMeshAsOctree(wMesh* vptrMesh,
                          Int32 minimalPolysPerNode=512,
                          bool alsoAddIfMeshPointerZero=false);
#else
wNode* wNodeCreateFromMeshAsOctree(wMesh* vptrMesh,
                          Int32 minimalPolysPerNode,
                          bool alsoAddIfMeshPointerZero);
#endif // __cplusplus

wNode* wNodeCreateFromBatchingMesh(wMesh* batchMesh);

#ifdef __cplusplus
wNode* wNodeCreateFromBatchingMeshAsOctree(wMesh* batchMesh,
                          Int32 minimalPolysPerNode=512,
                          bool alsoAddIfMeshPointerZero=false);
#else
wNode* wNodeCreateFromBatchingMeshAsOctree(wMesh* batchMesh,
                          Int32 minimalPolysPerNode,
                          bool alsoAddIfMeshPointerZero);
#endif // __cplusplus

void wNodeRemoveCollision(wNode* node,
                          wSelector* selector);

void wNodeAddCollision(wNode* node,
                       wSelector* selector);

///wWater///
#ifdef __cplusplus
wNode* wWaterSurfaceCreate(wMesh* mesh,
                           Float32 waveHeight=2.0f,
                           Float32 waveSpeed=300.0f,
                           Float32 waveLength=10.0f,
                           wVector3f position=wVECTOR3f_ZERO,
                           wVector3f rotation=wVECTOR3f_ZERO,
                           wVector3f scale=wVECTOR3f_ONE);
#else
wNode* wWaterSurfaceCreate(wMesh* mesh,
                           Float32 waveHeight,
                           Float32 waveSpeed,
                           Float32 waveLength,
                           wVector3f position,
                           wVector3f rotation,
                           wVector3f scale);
#endif // __cplusplus


///wRealWater///
wNode* wRealWaterSurfaceCreate( wTexture* bumpTexture,
                                wVector2f size,
								wVector2u renderSize);

void wRealWaterSetWindForce(wNode* water,
                            Float32 force);

void wRealWaterSetWindDirection(wNode* water,
                                wVector2f direction);

void wRealWaterSetWaveHeight(wNode* water,
                             Float32 height);

void wRealWaterSetColor(wNode* water,
                        wColor4f color);

void wRealWaterSetColorBlendFactor(wNode* water,
                                   Float32 factor);

///wClouds///
wNode* wCloudsCreate( wTexture* texture,
                      UInt32 lod,
                      UInt32 depth,
                      UInt32 density );

///wRealClouds///
wNode* wRealCloudsCreate(wTexture* txture,
                         wVector3f height,
                         wVector2f speed,
                         Float32 textureScale);

void wRealCloudsSetTextureTranslation(wNode* cloud,
                                      wVector2f speed);

wVector2f wRealCloudsGetTextureTranslation(wNode* cloud);

void wRealCloudsSetTextureScale(wNode* cloud,
                                Float32 scale);

Float32 wRealCloudsGetTextureScale(wNode* cloud);

void wRealCloudsSetCloudHeight(wNode* cloud,
                               wVector3f height);

wVector3f wRealCloudsGetCloudHeight(wNode* cloud);

void wRealCloudsSetCloudRadius(wNode* cloud,
                               wVector2f radius);

wVector2f wRealCloudsGetCloudRadius(wNode* cloud);

void wRealCloudsSetColors(wNode* cloud,
                          wColor4s centerColor,
                          wColor4s innerColor,
                          wColor4s outerColor);

void wRealCloudsGetColors(wNode* cloud,
                          wColor4s* centerColor,
                          wColor4s* innerColor,
                          wColor4s* outerColor);

///wLensFlare///
wNode* wLensFlareCreate(wTexture* texture);

void wLensFlareSetStrength(wNode* flare,
                           Float32 strength);

Float32 wLensFlareGetStrength(wNode* flare);

///wGrass///
wNode* wGrassCreate(wNode* terrain,
                    wVector2i position,
                    UInt32 patchSize,
                    Float32 fadeDistance,
                    bool crossed,
                    Float32 grassScale,
                    UInt32 maxDensity,
                    wVector2u dataPosition,
                    wImage* heightMap,
                    wImage* textureMap,
                    wImage* grassMap,
                    wTexture* grassTexture);

void wGrassSetDensity(wNode* grass,
                      UInt32 density,
                      Float32 distance );

void wGrassSetWind(wNode* grass,
                   Float32 strength,
                   Float32 res );

UInt32 wGrassGetDrawingCount(wNode* grass );

///wTreeGenerator/////
wNode* wTreeGeneratorCreate(const char* xmlFilePath);

void wTreeGeneratorDestroy(wNode* generator);

///wTree///
wNode* wTreeCreate(wNode* generator,Int32 seed, wTexture* billboardTexture);

void wTreeSetDistances(wNode* tree,Float32 midRange,Float32 farRange);

wNode* wTreeGetLeafNode(wNode* tree);

void wTreeSetLeafEnabled(wNode* tree, bool value);

bool wTreeIsLeafEnabled(wNode* tree);

wMeshBuffer* wTreeGetMeshBuffer(wNode* tree,
                                UInt32 idx);//0-HIGH meshbuffer,  1- MID meshbuffer

void wTreeSetBillboardVertexColor(wNode* tree,wColor4s color);

wColor4s wTreeGetBillboardVertexColor(wNode* tree);

///wWindGenerator
wNode* wWindGeneratorCreate();

void wWindGeneratorDestroy(wNode* windGenerator);

void wWindGeneratorSetStrength(wNode* windGenerator,Float32 strength);

Float32 wWindGeneratorGetStrength(wNode* windGenerator);

void wWindGeneratorSetRegularity(wNode* windGenerator,Float32 regularity);

Float32 wWindGeneratorGetRegularity(wNode* windGenerator);

wVector2f wWindGeneratorGetWind(wNode* windGenerator,wVector3f position,UInt32 timeMs);

///wBolt///
wNode* wBoltCreate();

void wBoltSetProperties(wNode* bolt,
                        wVector3f start,
                        wVector3f end,
                        UInt32 updateTime,
                        UInt32 height,
                        Float32 thickness,
                        UInt32 parts,
                        UInt32 bolts,
                        bool steddyend,
                        wColor4s color);
///wBeam///
wNode* wBeamCreate();

void wBeamSetSize(wNode* beam,
                  Float32 size );

void wBeamSetPosition(wNode* beam,
					  wVector3f start,
                      wVector3f end);

///wParticleSystem///
#ifdef __cplusplus
wNode* wParticleSystemCreate(bool defaultemitter=false,
                             wVector3f position=wVECTOR3f_ZERO,
                             wVector3f rotation=wVECTOR3f_ZERO,
                             wVector3f scale=wVECTOR3f_ONE);
#else
wNode* wParticleSystemCreate(bool defaultemitter,
                             wVector3f position,
                             wVector3f rotation,
                             wVector3f scale);
#endif // __cplusplus

wEmitter* wParticleSystemGetEmitter(wNode* ps);

void wParticleSystemSetEmitter(wNode* ps,
                               wEmitter* em);

void wParticleSystemRemoveAllAffectors(wNode* ps);

void wParticleSystemSetGlobal(wNode* ps,
                              bool value);

void wParticleSystemSetParticleSize(wNode* ps,
                                    wVector2f size);

void wParticleSystemClear(wNode* ps);

///wParticleBoxEmitter///
wEmitter* wParticleBoxEmitterCreate(wNode* ps);

#ifdef __cplusplus
void wParticleBoxEmitterSetBox(wEmitter* em,
                               wVector3f boxMin={-10,28,10},
                               wVector3f boxMax={10,30,10});
#else
void wParticleBoxEmitterSetBox(wEmitter* em,
                               wVector3f boxMin,
                               wVector3f boxMax);
#endif // __cplusplus

void wParticleBoxEmitterGetBox(wEmitter* em,
                               wVector3f* boxMin,
                               wVector3f* boxMax);

///wParticleCylinderEmitter///
wEmitter* wParticleCylinderEmitterCreate(wNode* ps,
                                         wVector3f center,
                                         Float32 radius,
                                         wVector3f normal,
                                         Float32 lenght);

void wParticleCylinderEmitterSetParameters(wEmitter* em,
                                           wParticleCylinderEmitter params);

void wParticleCylinderEmitterGetParameters(wEmitter* em,
                                           wParticleCylinderEmitter* params);

///wParticleMeshEmitter///
wEmitter* wParticleMeshEmitterCreate(wNode* ps,
                                     wNode* node);

void wParticleMeshEmitterSetParameters(wEmitter* em,
                                       wParticleMeshEmitter params);

void wParticleMeshEmitterGetParameters(wEmitter* em,
                                       wParticleMeshEmitter* params);

///wParticlePointEmitter///
wEmitter* wParticlePointEmitterCreate(wNode* ps);

///wParticleRingEmitter///
wEmitter* wParticleRingEmitterCreate(wNode* ps,
                                     wVector3f center,
                                     Float32 radius,
                                     Float32 ringThickness);

void wParticleRingEmitterSetParameters(wEmitter* em,
                                       wParticleRingEmitter params);

void wParticleRingEmitterGetParameters(wEmitter* em,
                                       wParticleRingEmitter* params);

///wParticleSphereEmitter///
wEmitter* wParticleSphereEmitterCreate(wNode* ps,
                                       wVector3f center,
                                       Float32 radius);

void wParticleSphereEmitterSetParameters(wEmitter* em,
                                         wParticleSphereEmitter params);

void wParticleSphereEmitterGetParameters(wEmitter* em,
                                         wParticleSphereEmitter* params);

///wParticleEmitter- FOR ALL///
void wParticleEmitterSetParameters(wEmitter* em,
                                   wParticleEmitter params);

void wParticleEmitterGetParameters(wEmitter* em,
                                   wParticleEmitter* params);

///wParticleAffector -FOR ALL///
void wParticleAffectorSetEnable(wAffector* foa,
                                bool enable );


bool wParticleAffectorIsEnable(wAffector* foa);

///wParticleFadeOutAffector///
wAffector* wParticleFadeOutAffectorCreate(wNode* ps);

#ifdef __cplusplus
void wParticleFadeOutAffectorSetTime(wAffector* paf,
                                     UInt32 fadeOutTime=1000);
#else
void wParticleFadeOutAffectorSetTime(wAffector* paf,
                                     UInt32 fadeOutTime);
#endif // __cplusplus

UInt32 wParticleFadeOutAffectorGetTime(wAffector* paf);

#ifdef __cplusplus
void wParticleFadeOutAffectorSetColor(wAffector* paf,
                                      wColor4s targetColor=wCOLOR4s_BLACK);
#else
void wParticleFadeOutAffectorSetColor(wAffector* paf,
                                      wColor4s targetColor);
#endif // __cplusplus

wColor4s wParticleFadeOutAffectorGetColor(wAffector* paf);

///wParticleGravityAffector///
wAffector* wParticleGravityAffectorCreate(wNode* ps);

#ifdef __cplusplus
void wParticleGravityAffectorSetGravity(wAffector* paf,
                                        wVector3f gravity={0,-0.03f,0});
#else
void wParticleGravityAffectorSetGravity(wAffector* paf,
                                        wVector3f gravity);
#endif // __cplusplus

wVector3f wParticleGravityAffectorGetGravity(wAffector* paf);

#ifdef __cplusplus
void wParticleGravityAffectorSetTimeLost(wAffector* paf,
                                         UInt32 timeForceLost=1000);
#else
void wParticleGravityAffectorSetTimeLost(wAffector* paf,
                                         UInt32 timeForceLost);
#endif // __cplusplus

UInt32 wParticleGravityAffectorGetTimeLost(wAffector* paf);

///wParticleAttractionAffector///
#ifdef __cplusplus
wAffector* wParticleAttractionAffectorCreate(wNode* ps,
                                             wVector3f point,
                                             Float32 speed=1.0f);
#else
wAffector* wParticleAttractionAffectorCreate(wNode* ps,
                                             wVector3f point,
                                             Float32 speed);
#endif // __cplusplus

void wParticleAttractionAffectorSetParameters(wAffector* paf,
                                              wParticleAttractionAffector params);

void wParticleAttractionAffectorGetParameters(wAffector* paf,
                                              wParticleAttractionAffector* params);

///wParticleRotationAffector///
wAffector*  wParticleRotationAffectorCreate(wNode* ps);

void wParticleRotationAffectorSetSpeed(wAffector* paf,
                                       wVector3f speed);

wVector3f wParticleRotationAffectorGetSpeed(wAffector* paf);

void wParticleRotationAffectorSetPivot(wAffector* paf,
                                       wVector3f pivotPoint);

wVector3f wParticleRotationAffectorGetPivot(wAffector* paf);

///wParticleStopAffector///
#ifdef __cplusplus
wAffector*  wParticleStopAffectorCreate(wNode* ps,
                                        wEmitter* em,
                                        UInt32 time=1000);
#else
wAffector*  wParticleStopAffectorCreate(wNode* ps,
                                        wEmitter* em,
                                        UInt32 time);
#endif // __cplusplus

void wParticleStopAffectorSetTime(wAffector* paf,
                                  UInt32 time);

UInt32 wParticleStopAffectorGetTime(wAffector* paf);

///wParticleColorMorphAffector///
wAffector*  wParticleColorMorphAffectorCreate(wNode* ps);

void wParticleColorAffectorSetParameters(wAffector* paf,
                                         wParticleColorMorphAffector params);

void wParticleColorAffectorGetParameters(wAffector* paf,
                                         wParticleColorMorphAffector* params);

///wParticlePushAffector///
wAffector*  wParticlePushAffectorCreate(wNode* ps);

void wParticlePushAffectorSetParameters(wAffector* paf,
                                        wParticlePushAffector params);

void wParticlePushAffectorGetParameters(wAffector* paf,
                                        wParticlePushAffector* params);

///wParticleSplineAffector///
wAffector*  wParticleSplineAffectorCreate(wNode* ps);

void wParticleSplineAffectorSetParameters(wAffector* paf,
                                          wParticleSplineAffector params);

void wParticleSplineAffectorGetParameters(wAffector* paf,
                                          wParticleSplineAffector* params);

///wParticleScaleAffector///
wAffector* wParticleScaleAffectorCreate(wNode* ps,
                                        wVector2f scaleTo);

///wNode///
void wNodeSetDecalsEnabled(wNode* node);

void wNodeSetParent(wNode* node,
                    wNode *parent );

wNode* wNodeGetParent(wNode* node );

void wNodeSetReadOnlyMaterials(wNode* node,
                               bool readonly);

bool wNodeIsReadOnlyMaterials(wNode* node);

wNode* wNodeGetFirstChild(wNode* node,
                          UInt32* iterator);
UInt32 wNodeGetChildsCount(wNode* node,UInt32* iterator);

wNode* wNodeGetNextChild(wNode* node,
                         UInt32* iterator);

bool wNodeIsLastChild(wNode* node,
                      UInt32* iterator);

void wNodeSetId(wNode* node,
                Int32 id);

Int32 wNodeGetId(wNode* node);

void wNodeSetName(wNode* node,
                  const char* name );

const char* wNodeGetName(wNode* node);

void wNodeSetUserData(wNode* node,
                      void* const newData);

void* wNodeGetUserData(wNode* node);

#ifdef __cplusplus
void wNodeSetDebugMode(wNode* node,
                       wDebugMode visible=wDM_FULL);
#else
void wNodeSetDebugMode(wNode* node,
                       wDebugMode visible);
#endif // __cplusplus

void wNodeSetDebugDataVisible(wNode* node,
                              bool value);

UInt32 wNodeGetMaterialsCount(wNode* node );

wMaterial* wNodeGetMaterial(wNode* node,
                            UInt32 matIndex );

void wNodeSetPosition(wNode* node,
                      wVector3f position);

wVector3f wNodeGetPosition(wNode* node);

wVector3f wNodeGetAbsolutePosition(wNode* node);

void wNodeSetRotation (wNode* node,
                       wVector3f rotation);

void wNodeSetAbsoluteRotation(wNode* node,
                              wVector3f rotation);

wVector3f wNodeGetRotation(wNode* node);

wVector3f wNodeGetAbsoluteRotation(wNode* node);

void wNodeTurn(wNode* Entity,
               wVector3f turn);

void wNodeMove(wNode* Entity,
               wVector3f direction);

void wNodeRotateToNode(wNode* Entity1,
                       wNode* Entity2);

Float32 wNodesGetBetweenDistance(wNode* nodeA,
                               wNode* nodeB );

bool wNodesAreIntersecting(wNode* nodeA,
                           wNode* nodeB );

bool wNodeIsPointInside(wNode* node,
                        wVector3f pos);

void wNodeDrawBoundingBox(wNode* node,
                          wColor4s color);

void wNodeGetBoundingBox(wNode* Node,
                         wVector3f* min,
                         wVector3f* max);

void wNodeGetTransformedBoundingBox(wNode* Node,
                                    wVector3f* min,
                                    wVector3f* max);

void wNodeSetScale(wNode* node,
                   wVector3f scale );

wVector3f wNodeGetScale(wNode* node);

wNode* wNodeDuplicate(wNode* entity);

wNode* wNodeGetJointByName(wNode* node,
                     const char *node_name );

wNode* wNodeGetJointById( wNode* node,UInt32 Id);

Int32 wNodeGetJointsCount( wNode* node);

void wNodeSetJointSkinningSpace(wNode* bone,
                                wBoneSkinningSpace space );

wBoneSkinningSpace wNodeGetJointSkinningSpace(wNode* bone);

void wNodeSetRenderFromIdentity(wNode* node, bool value);

#ifdef __cplusplus
void wNodeAddShadowVolume(wNode* node,
                          wMesh* mesh=0,
                          bool zfailMethod=true,
                          Float32 infinity=10000.f,
                          bool oldStyle=false);
#else
void wNodeAddShadowVolume(wNode* node,
                          wMesh* mesh,
                          bool zfailMethod,
                          Float32 infinity,
                          bool oldStyle);
#endif // __cplusplus

#ifdef __cplusplus
wNode* wNodeAddShadowVolumeFromMeshBuffer(wNode* nodeParent,
                                          wMeshBuffer* meshbuffer,
                                          bool zfailMethod=true,
                                          Float32 infinity=10000.f,
                                          bool oldStyle=false);
#else
wNode* wNodeAddShadowVolumeFromMeshBuffer(wNode* nodeParent,
                                          wMeshBuffer* meshbuffer,
                                          bool zfailMethod,
                                          Float32 infinity,
                                          bool oldStyle);
#endif // __cplusplus

void wNodeUpdateShadow(wNode* shadow);

void wNodeSetVisibility(wNode* node,
                        bool visible );

bool wNodeIsVisible(wNode* node);

bool wNodeIsInView(wNode* node);

void wNodeDestroy(wNode* node);

void wNodeSetMesh(wNode* node,
                  wMesh* mesh);

wMesh* wNodeGetMesh(wNode* node);

void wNodeSetRotationPositionChange(wNode* node,
                                    wVector3f angles,
                                    wVector3f offset,
                                    wVector3f* forwardStore,
                                    wVector3f* upStore,
                                    UInt32 numOffsets,
                                    wVector3f* offsetStore );

void wNodeSetCullingState(wNode* node,
                          wCullingState state);

wSceneNodeType wNodeGetType(wNode* node);

void wNodeSetAnimationRange(wNode* node,
                            wVector2i range);

void wNodePlayMD2Animation(wNode* node,
                           wMd2AnimationType iAnimation);

void wNodeSetAnimationSpeed(wNode* node,
                            Float32 fSpeed);

void wNodeSetAnimationFrame(wNode* node,
                            Float32 fFrame);

Float32 wNodeGetAnimationFrame(wNode* node);

void wNodeSetTransitionTime(wNode* node,
                            Float32 fTime);

void wNodeAnimateJoints(wNode* node);

void wNodeSetJointMode(wNode* node,
                       wJointMode mode);

void wNodeSetAnimationLoopMode(wNode* node,
                               bool value);

void wNodeDestroyAllAnimators(wNode* node);

UInt32 wNodeGetAnimatorsCount(wNode* node);

wAnimator* wNodeGetFirstAnimator(wNode* node);

wAnimator* wNodeGetLastAnimator(wNode* node);

wAnimator* wNodeGetAnimatorByIndex(wNode* node,
                                   UInt32 index);

void wNodeOnAnimate(wNode* node,UInt32 timeMs);

void wNodeDraw(wNode* node);

void wNodeUpdateAbsolutePosition (wNode* node);

///wMaterial///
void wMaterialSetTexture(wMaterial* material,
                         UInt32 texIdx,
                         wTexture* texture);

wTexture* wMaterialGetTexture(wMaterial* material,
                              UInt32 texIdx);

void wMaterialScaleTexture(wMaterial* material,
                           UInt32 texIdx,
                           wVector2f scale);

void wMaterialScaleTextureFromCenter(wMaterial* material,
                                     UInt32 texIdx,
                                     wVector2f scale);

void wMaterialTranslateTexture(wMaterial* material,
                               UInt32 texIdx,
                               wVector2f translate);

void wMaterialTranslateTextureTransposed(wMaterial* material,
                                         UInt32 texIdx,
                                         wVector2f translate);

void wMaterialRotateTexture(wMaterial* material,
                            UInt32 texIdx,
                            Float32 angle);

void wMaterialSetTextureWrapUMode(wMaterial* material,
                                  UInt32 texIdx,
                                  wTextureClamp value);

wTextureClamp wMaterialGetTextureWrapUMode(wMaterial* material,
                                           UInt32 texIdx);

void wMaterialSetTextureWrapVMode(wMaterial* material,
                                  UInt32 texIdx,
                                  wTextureClamp value);

wTextureClamp wMaterialGetTextureWrapVMode(wMaterial* material,
                                           UInt32 texIdx);

void wMaterialSetTextureLodBias(wMaterial* material,
                                UInt32 texIdx,
                                UInt32 lodBias);

UInt32 wMaterialGetTextureLodBias(wMaterial* material,
                                        UInt32 texIdx);

void wMaterialSetFlag(wMaterial* material,
                      wMaterialFlags Flag,
                      bool boValue);

bool wMaterialGetFlag(wMaterial* material,
                      wMaterialFlags matFlag);

void wMaterialSetType(wMaterial* material,
                      wMaterialTypes type );

void wMaterialSetShininess(wMaterial* material,
                           Float32 shininess);

Float32 wMaterialGetShininess(wMaterial* material);

void wMaterialSetVertexColoringMode(wMaterial* material,
                                    wColorMaterial colorMaterial );

wColorMaterial wMaterialGetVertexColoringMode(wMaterial* material);

void wMaterialSetSpecularColor(wMaterial* material,
                               wColor4s color);

wColor4s wMaterialGetSpecularColor(wMaterial* material);

void wMaterialSetDiffuseColor(wMaterial* material,
                              wColor4s color);

wColor4s wMaterialGetDiffuseColor(wMaterial* material);

void wMaterialSetAmbientColor(wMaterial* material,
                              wColor4s color);

wColor4s wMaterialGetAmbientColor(wMaterial* material);

void wMaterialSetEmissiveColor(wMaterial* material,
                               wColor4s color);

wColor4s wMaterialGetEmissiveColor(wMaterial* material);

void wMaterialSetTypeParameter(wMaterial* material,
                               Float32 param1);

Float32 wMaterialGetTypeParameter(wMaterial* material);

void wMaterialSetTypeParameter2(wMaterial* material,
                                Float32 param2);

Float32 wMaterialGetTypeParameter2(wMaterial* material);

void wMaterialSetBlendingMode(wMaterial* material,
                              const wBlendFactor blendSrc,
                              const wBlendFactor blendDest);

//wMaterialGetBlendingMode = wMaterialGetTypeParameter

void wMaterialSetLineThickness(wMaterial* material,
                               Float32 lineThickness );

Float32 wMaterialGetLineThickness(wMaterial* material);

void wMaterialSetColorMask(wMaterial* material,
                           wColorPlane value);

wColorPlane wMaterialGetColorMask(wMaterial* material);

void wMaterialSetAntiAliasingMode(wMaterial* material,
                                  wAntiAliasingMode mode);

wAntiAliasingMode wMaterialGetAntiAliasingMode(wMaterial* material);


///wShader///
bool wShaderCreateNamedVertexConstant(wShader* shader,
                                      const char* name,
                                      Int32 preset,
                                      const float* floats,
                                      Int32 count);

bool wShaderCreateNamedPixelConstant(wShader* shader,
                                     const char*	name,
                                     int	preset,
                                     const float* floats,
                                     int	count);

bool wShaderCreateAddressedVertexConstant(wShader* shader,
                                          Int32 address,
                                          int	preset,
                                          const float* floats,
                                          int	count);

bool wShaderCreateAddressedPixelConstant(wShader* shader,
                                         int	address,
                                         int	preset,
                                         const float* floats,
                                         int	count);

#ifdef __cplusplus
wShader* wShaderAddHighLevelMaterial(const char* vertexShaderProgram,
                                     const char*  vertexShaderEntryPointName,
                                     wVertexShaderVersion wVersion,
                                     const char* pixelShaderProgram,
                                     const char*  pixelShaderEntryPointName="main",
                                     wPixelShaderVersion pVersion=wPSV_1_1,
                                     wMaterialTypes materialType=wMT_SOLID,
                                     Int32 userData=0);
#else
wShader* wShaderAddHighLevelMaterial(const char* vertexShaderProgram,
                                     const char*  vertexShaderEntryPointName,
                                     wVertexShaderVersion wVersion,
                                     const char* pixelShaderProgram,
                                     const char*  pixelShaderEntryPointName,
                                     wPixelShaderVersion pVersion,
                                     wMaterialTypes materialType,
                                     Int32 userData);
#endif // __cplusplus

#ifdef __cplusplus
wShader* wShaderAddHighLevelMaterialFromFiles(const char* vertexShaderProgramFileName,
                                              const char*  vertexShaderEntryPointName,
                                              wVertexShaderVersion wVersion,
                                              const char * pixelShaderProgramFileName,
                                              const char*  pixelShaderEntryPointName="main",
                                              wPixelShaderVersion pVersion=wPSV_1_1,
                                              wMaterialTypes materialType=wMT_SOLID,
                                              Int32 userData=0);
#else
wShader* wShaderAddHighLevelMaterialFromFiles(const char* vertexShaderProgramFileName,
                                              const char*  vertexShaderEntryPointName,
                                              wVertexShaderVersion wVersion,
                                              const char * pixelShaderProgramFileName,
                                              const char*  pixelShaderEntryPointName,
                                              wPixelShaderVersion pVersion,
                                              wMaterialTypes materialType,
                                              Int32 userData);
#endif // __cplusplus

#ifdef __cplusplus
wShader* wShaderAddMaterial(const char*  vertexShaderProgram,
                            const char*  pixelShaderProgram,
                            wMaterialTypes materialType=wMT_SOLID,
                            Int32 userData=0);
#else
wShader* wShaderAddMaterial(const char*  vertexShaderProgram,
                            const char*  pixelShaderProgram,
                            wMaterialTypes materialType,
                            Int32 userData);
#endif // __cplusplus

#ifdef __cplusplus
wShader* wShaderAddMaterialFromFiles(const char*  vertexShaderProgramFileName,
                                     const char*  pixelShaderProgramFileName,
                                     wMaterialTypes materialType=wMT_SOLID,
                                     Int32 userData=0);
#else
wShader* wShaderAddMaterialFromFiles(const char*  vertexShaderProgramFileName,
                                     const char*  pixelShaderProgramFileName,
                                     wMaterialTypes materialType,
                                     Int32 userData);
#endif // __cplusplus


///with geometry shader
#ifdef __cplusplus
wShader* wShaderAddHighLevelMaterialEx(const char* vertexShaderProgram,
                                       const char*  vertexShaderEntryPointName,
                                       wVertexShaderVersion wVersion,
                                       const char* pixelShaderProgram,
                                       const char*  pixelShaderEntryPointName,
                                       wPixelShaderVersion pVersion,
                                       const char* geometryShaderProgram,
                                       const char*  geometryShaderEntryPointName="main",
                                       wGeometryShaderVersion gVersion=wGSV_4_0,
                                       wPrimitiveType inType=wPT_TRIANGLES,
                                       wPrimitiveType outType=wPT_TRIANGLE_STRIP,
                                       UInt32 verticesOut=0,
                                       wMaterialTypes materialType=wMT_SOLID,
                                       Int32 userData=0);
#else
wShader* wShaderAddHighLevelMaterialEx(const char* vertexShaderProgram,
                                       const char*  vertexShaderEntryPointName,
                                       wVertexShaderVersion wVersion,
                                       const char* pixelShaderProgram,
                                       const char*  pixelShaderEntryPointName,
                                       wPixelShaderVersion pVersion,
                                       const char* geometryShaderProgram,
                                       const char*  geometryShaderEntryPointName,
                                       wGeometryShaderVersion gVersion,
                                       wPrimitiveType inType,
                                       wPrimitiveType outType,
                                       UInt32 verticesOut,
                                       wMaterialTypes materialType,
                                       Int32 userData);
#endif // __cplusplus

///with geometry shader
#ifdef __cplusplus
wShader* wShaderAddHighLevelMaterialFromFilesEx(const char* vertexShaderProgramFileName,
                                                const char*  vertexShaderEntryPointName,
                                                wVertexShaderVersion wVersion,
                                                const char* pixelShaderProgramFileName,
                                                const char*  pixelShaderEntryPointName,
                                                wPixelShaderVersion pVersion,
                                                const char* geometryShaderProgram,
                                                const char*  geometryShaderEntryPointName="main",
                                                wGeometryShaderVersion gVersion=wGSV_4_0,
                                                wPrimitiveType inType=wPT_TRIANGLES,
                                                wPrimitiveType outType=wPT_TRIANGLE_STRIP,
                                                UInt32 verticesOut=0,
                                                wMaterialTypes materialType=wMT_SOLID,
                                                Int32 userData=0);
#else
wShader* wShaderAddHighLevelMaterialFromFilesEx(const char* vertexShaderProgramFileName,
                                                const char*  vertexShaderEntryPointName,
                                                wVertexShaderVersion wVersion,
                                                const char* pixelShaderProgramFileName,
                                                const char*  pixelShaderEntryPointName,
                                                wPixelShaderVersion pVersion,
                                                const char* geometryShaderProgram,
                                                const char*  geometryShaderEntryPointName,
                                                wGeometryShaderVersion gVersion,
                                                wPrimitiveType inType,
                                                wPrimitiveType outType,
                                                UInt32 verticesOut,
                                                wMaterialTypes materialType,
                                                Int32 userData);
#endif // __cplusplus

///wMesh///
#ifdef __cplusplus
wMesh* wMeshLoad(const char* cptrFile, bool ToTangents=false);
#else
wMesh* wMeshLoad(const char* cptrFile, bool ToTangents);
#endif // __cplusplus

wMesh* wMeshCreate(const char *cptrMeshName);

void wMeshAddMeshBuffer(wMesh* mesh,
                        wMeshBuffer* meshbuffer);

wMesh* wMeshCreateSphere(const char* name,
                         Float32 radius,
                         Int32 polyCount );

wMesh* wMeshCreateCube();

Int32 wMeshSave(wMesh* mesh,
              wMeshFileFormat type,
              const char* filename);//return 0/1/2/3  3- успешно

void wMeshDestroy(wMesh* mesh);

bool wMeshSetName(wMesh* mesh,
                  const char* name);

const char* wMeshGetName(wMesh* mesh);

wAnimatedMeshType wMeshGetType(wMesh* mesh);

void wMeshFlipSurface(wMesh* mesh);

#ifdef __cplusplus
void wMeshMakePlanarTextureMapping(wMesh* mesh,
                                   Float32 resolution=0.001f);
#else
void wMeshMakePlanarTextureMapping(wMesh* mesh,
                                   Float32 resolution);
#endif // __cplusplus

#ifdef __cplusplus
void wMeshMakePlanarTextureMappingAdvanced(wMesh* mesh,
                                           Float32 resolutionH,
                                           Float32 resolutionV,
                                           UInt8 axis,
                                           wVector3f offset=wVECTOR3f_ZERO);
#else
void wMeshMakePlanarTextureMappingAdvanced(wMesh* mesh,
                                           Float32 resolutionH,
                                           Float32 resolutionV,
                                           UInt8 axis,
                                           wVector3f offset);
#endif

wMesh* wMeshCreateStaticWithTangents(wMesh* aMesh);

#ifdef __cplusplus
void wMeshRecalculateNormals(wMesh* mesh,
                             bool smooth=false,
                             bool angleWeighted=false);
#else
void wMeshRecalculateNormals(wMesh* mesh,
                             bool smooth,
                             bool angleWeighted);
#endif // __cplusplus

#ifdef __cplusplus
void wMeshRecalculateTangents(wMesh* mesh,
                              bool recalculateNormals=false,
                              bool smooth=false,
                              bool angleWeighted=false);
#else
void wMeshRecalculateTangents(wMesh* mesh,
                              bool recalculateNormals,
                              bool smooth,
                              bool angleWeighted);
#endif // __cplusplus

#ifdef __cplusplus
wMesh* wMeshCreateHillPlane(const char* meshname,
                            wVector2f tilesSize,
                            wVector2i tilesCount,
                            wMaterial* material=0,
                            Float32 hillHeight=0,
                            wVector2f countHills=wVECTOR2f_ZERO,
                            wVector2f texRepeatCount=wVECTOR2f_ONE);
#else
wMesh* wMeshCreateHillPlane(const char* meshname,
                            wVector2f tilesSize,
                            wVector2i tilesCount,
                            wMaterial* material,
                            Float32 hillHeight,
                            wVector2f countHills,
                            wVector2f texRepeatCount);
#endif // __cplusplus

#ifdef __cplusplus
wMesh* wMeshCreateArrow(const char* name,
                        wColor4s cylinderColor=wCOLOR4s_WHITE,
                        wColor4s coneColor=wCOLOR4s_WHITE,
                        UInt32 tesselationCylinder=4,
                        UInt32 tesselationCone=8,
                        Float32 height=1.0f,
                        Float32 heightCylinder=0.05f,
                        Float32 widthCylinder=0.05f,
                        Float32 widthCone=0.3f);
#else
wMesh* wMeshCreateArrow(const char* name,
                        wColor4s cylinderColor,
                        wColor4s coneColor,
                        UInt32 tesselationCylinder,
                        UInt32 tesselationCone,
                        Float32 height,
                        Float32 heightCylinder,
                        Float32 widthCylinder,
                        Float32 widthCone);
#endif

wMesh* wMeshCreateBatching();

#ifdef __cplusplus
void wMeshAddToBatching(wMesh* meshBatch,
                        wMesh* mesh,
                        wVector3f position=wVECTOR3f_ZERO,
                        wVector3f rotation=wVECTOR3f_ZERO,
                        wVector3f scale=wVECTOR3f_ONE);
#else
void wMeshAddToBatching(wMesh* meshBatch,
                        wMesh* mesh,
                        wVector3f position,
                        wVector3f rotation,
                        wVector3f scale);
#endif // __cplusplus

void wMeshUpdateBatching(wMesh* meshBatch);

void wMeshFinalizeBatching(wMesh* meshBatch);

void wMeshClearBatching(wMesh* meshBatch);

void wMeshDestroyBatching(wMesh* meshBatch);

#ifdef __cplusplus
void wMeshEnableHardwareAcceleration(wMesh* mesh,
                                     UInt32 iFrame=0);
#else
void wMeshEnableHardwareAcceleration(wMesh* mesh,
                                     UInt32 iFrame);
#endif // __cplusplus

UInt32 wMeshGetFramesCount(wMesh* mesh );

#ifdef __cplusplus
UInt32 wMeshGetIndicesCount(wMesh* mesh,
                                  UInt32 iFrame=0,
                                  UInt32 iMeshBuffer=0);
#else
UInt32 wMeshGetIndicesCount(wMesh* mesh,
                                  UInt32 iFrame,
                                  UInt32 iMeshBuffer);
#endif // __cplusplus

#ifdef __cplusplus
UInt16* wMeshGetIndices(wMesh* mesh,
                                UInt32 iFrame=0,
                                UInt32 iMeshBuffer=0);
#else
UInt16* wMeshGetIndices(wMesh* mesh,
                                UInt32 iFrame,
                                UInt32 iMeshBuffer);
#endif // __cplusplus

#ifdef __cplusplus
void wMeshSetIndices(wMesh* mesh,
                     UInt32 iFrame,
                     UInt16* indicies,
                     UInt32 iMeshBuffer=0);
#else
void wMeshSetIndices(wMesh* mesh,
                     UInt32 iFrame,
                     UInt16* indicies,
                     UInt32 iMeshBuffer);
#endif // __cplusplus

#ifdef __cplusplus
UInt32 wMeshGetVerticesCount(wMesh* mesh,
                             UInt32 iFrame=0,
                             UInt32 iMeshBuffer=0);
#else
UInt32 wMeshGetVerticesCount(wMesh* mesh,
                             UInt32 iFrame,
                             UInt32 iMeshBuffer);
#endif // __cplusplus

#ifdef __cplusplus
void wMeshGetVertices(wMesh* mesh,
                      UInt32 iFrame,
                      wVert* verts,
                      UInt32 iMeshBuffer=0);
#else
void wMeshGetVertices(wMesh* mesh,
                      UInt32 iFrame,
                      wVert* verts,
                      UInt32 iMeshBuffer);
#endif // __cplusplus

#ifdef __cplusplus
UInt32* wMeshGetVerticesMemory(wMesh* mesh,
                               UInt32 iFrame=0,
                                     UInt32 iMeshBuffer=0);
#else
UInt32* wMeshGetVerticesMemory(wMesh* mesh,
                                     UInt32 iFrame,
                                     UInt32 iMeshBuffer);
#endif // __cplusplus

#ifdef __cplusplus
void wMeshSetVertices(wMesh* mesh,
                      UInt32 iFrame,
                      wVert* verts,
                      UInt32 iMeshBuffer=0);
#else
void wMeshSetVertices(wMesh* mesh,
                      UInt32 iFrame,
                      wVert* verts,
                      UInt32 iMeshBuffer);
#endif // __cplusplus

#ifdef __cplusplus
void wMeshSetScale( wMesh* mesh,
                    Float32 scale,
					UInt32 iFrame=0,
					UInt32 iMeshBuffer=0,
					wMesh* sourceMesh=0);
#else
void wMeshSetScale( wMesh* mesh,
                    Float32 scale,
					UInt32 iFrame,
					UInt32 iMeshBuffer,
					wMesh* sourceMesh);
#endif // __cplusplus

void wMeshSetRotation(wMesh* mesh,
                      wVector3f rot);

#ifdef __cplusplus
void wMeshSetVerticesColors(wMesh* mesh,
                            UInt32 iFrame,
                            wColor4s* verticesColor,
                            UInt32 groupCount=0,
                            UInt32* startPos=0,
                            UInt32* endPos=0,
                            UInt32 iMeshBuffer=0);
#else
void wMeshSetVerticesColors(wMesh* mesh,
                            UInt32 iFrame,
                            wColor4s* verticesColor,
                            UInt32 groupCount,
                            UInt32* startPos,
                            UInt32* endPos,
                            UInt32 iMeshBuffer);
#endif // __cplusplus


void wMeshSetVerticesAlpha(wMesh* mesh,
                           UInt32 iFrame,
                           UInt8 value);

#ifdef __cplusplus
void wMeshSetVerticesCoords(wMesh* mesh,
                            UInt32 iFrame,
                            wVector3f* vertexCoord,
                            UInt32 groupCount=0,
                            UInt32* startPos=0,
                            UInt32* endPos=0,
                            UInt32 iMeshBuffer=0);
#else
void wMeshSetVerticesCoords(wMesh* mesh,
                            UInt32 iFrame,
                            wVector3f* vertexCoord,
                            UInt32 groupCount,
                            UInt32* startPos,
                            UInt32* endPos,
                            UInt32 iMeshBuffer);
#endif // __cplusplus

#ifdef __cplusplus
void wMeshSetVerticesSingleColor(wMesh* mesh,
                                 UInt32 iFrame,
                                 wColor4s verticesColor,
                                 UInt32 groupCount=0,
                                 UInt32* startPos=0,
                                 UInt32* endPos=0,
                                 UInt32 iMeshBuffer=0);
#else
void wMeshSetVerticesSingleColor(wMesh* mesh,
                                 UInt32 iFrame,
                                 wColor4s verticesColor,
                                 UInt32 groupCount,
                                 UInt32* startPos,
                                 UInt32* endPos,
                                 UInt32 iMeshBuffer);
#endif // __cplusplus

void wMeshGetBoundingBox (wMesh* mesh,
                          wVector3f* min,
                          wVector3f* max);

wMesh* wMeshDuplicate(wMesh* src);

void wMeshFit(wMesh* src,
              wVector3f pivot,
              wVector3f* delta);

bool wMeshIsEmpty(wMesh* mesh);

#ifdef __cplusplus
UInt32 wMeshGetBuffersCount(wMesh* mesh,
                                  UInt32 iFrame=0);
#else
UInt32 wMeshGetBuffersCount(wMesh* mesh,
                                  UInt32 iFrame);
#endif // __cplusplus

wMeshBuffer* wMeshGetBuffer(wMesh* mesh,
                            UInt32 iFrame,
                            UInt32 index);

///wMeshBuffer///
wMeshBuffer* wMeshBufferCreate(
             UInt32 iVertexCount,
             wVert* vVertices,
             UInt32 iIndicesCount,
             UInt16* usIndices);

/*wMeshBuffer* wMeshBufferCreateFromMeshJoint(wMesh* mesh,
                                            Int32 jointIndex);*/

void wMeshBufferDestroy(wMeshBuffer* buf);

#ifdef __cplusplus
void wMeshBufferAddToBatching(wMesh* meshBatch,
                              wMeshBuffer* buffer,
                              wVector3f position=wVECTOR3f_ZERO,
                              wVector3f rotation=wVECTOR3f_ZERO,
                              wVector3f scale=wVECTOR3f_ONE);
#else
void wMeshBufferAddToBatching(wMesh* meshBatch,
                              wMeshBuffer* buffer,
                              wVector3f position,
                              wVector3f rotation,
                              wVector3f scale);
#endif // __cplusplus

wMaterial* wMeshBufferGetMaterial(wMeshBuffer* buf);

///wBsp///
///Get BSP Entity List///
UInt32* wBspGetEntityList(wMesh* const mesh);

///Get BSP Entity List size///
Int32 wBspGetEntityListSize(UInt32* entityList);

///Get First (vec.x) and Last (vec.y) BSP Entity Index///
wVector2i wBspGetEntityIndexByName(void* entityList,
                                   const char* EntityName);
///Name BSP Entity From Index
const char* wBspGetEntityNameByIndex(UInt32* entityList,
                                     UInt32 number);
///Mesh from BSP Brush///
wMesh* wBspGetEntityMeshFromBrush(wMesh* bspMesh,
                                  UInt32* entityList,
                                  Int32 index);
///BSP VarGroup///
UInt32* wBspGetVarGroupByIndex(UInt32* entityList,
                                     Int32 index);

UInt32 wBspGetVarGroupSize(UInt32* entityList,
                                 Int32 index);

wVector3f wBspGetVarGroupValueAsVec(UInt32* group,
                                    const char* strName,
                                    UInt32 parsePos);

Float32 wBspGetVarGroupValueAsFloat(UInt32* group,
                                  const  char* strName,
                                  UInt32 parsePos);

const char* wBspGetVarGroupValueAsString(UInt32* group,
                                         const char* strName);
/*
UInt32 wBspGetVarGroupVariableSize(UInt32* group);


UInt32* wBspGetVariableFromVarGroup(UInt32* group,
                                          Int32 index);

const char* wBspGetVariableName(UInt32* variable);

const char* wBspGetVariableContent(UInt32* variable);

wVector3f wBspGetVariableValueAsVec(UInt32* variable,
                                    UInt32 parsePos);

Float32 wBspGetVariableValueAsFloat(UInt32* variable,
                                  UInt32 parsePos);
*/

#ifdef __cplusplus
wNode* wBspCreateFromMesh (wMesh* const mesh,
                           bool isTangent,
                           bool isOctree,
                           const char* fileEntity=0,
                           bool isLoadShaders=true,
                           UInt32 PolysPerNode=512);
#else
wNode* wBspCreateFromMesh (wMesh* const mesh,
                           bool isTangent,
                           bool isOctree,
                           const char* fileEntity,
                           bool isLoadShaders,
                           UInt32 PolysPerNode);
#endif // __cplusplus

///Occlusion Query
void wOcclusionQueryAddNode(wNode* node);

void wOcclusionQueryAddMesh(wNode* node,wMesh* mesh);

void wOcclusionQueryUpdate(wNode* node,bool block);

void wOcclusionQueryRun(wNode* node,bool visible);

void wOcclusionQueryUpdateAll(bool block);

void wOcclusionQueryRunAll(bool visible);

void wOcclusionQueryRemoveNode(wNode* node);

void wOcclusionQueryRemoveAll();

UInt32 wOcclusionQueryGetResult(wNode* node);

///wSphericalTerrain///
wNode* wSphericalTerrainCreate( const char *cptrFile0,
								const char *cptrFile1,
								const char *cptrFile2,
								const char *cptrFile3,
								const char *cptrFile4,
								const char *cptrFile5,
								wVector3f position,
								wVector3f rotation,
								wVector3f scale,
								wColor4s color,
								Int32 smootFactor,
								bool spherical,
								Int32 maxLOD,
								wTerrainPatchSize patchSize);

void wSphericalTerrainSetTextures(wNode* terrain,
                                  wTexture* textureTop,
                                  wTexture* textureFront,
                                  wTexture* textureBack,
                                  wTexture* textureLeft,
                                  wTexture* textureRight,
                                  wTexture* textureBottom,
                                  UInt32 materialIndex);

void wSphericalTerrainLoadVertexColor(wNode* terrain,
                                      wImage* imageTop,
                                      wImage* imageFront,
                                      wImage* imageBack,
                                      wImage* imageLeft,
                                      wImage* imageRight,
                                      wImage* imageBottom );

wVector3f wSphericalTerrainGetSurfacePosition(wNode* terrain,
                                              Int32 face,
                                              wVector2f logicalPos);

wVector3f wSphericalTerrainGetSurfaceAngle(wNode* terrain,
                                           Int32 face,
                                           wVector2f logicalPos);

wVector2f wSphericalTerrainGetSurfaceLogicalPosition(wNode* terrain,
                                                     wVector3f position,
                                                     int* face);

///wTerrain///
#ifdef __cplusplus
wNode* wTerrainCreate(const char* cptrFile,
                        wVector3f position=wVECTOR3f_ZERO,
						wVector3f rotation=wVECTOR3f_ZERO,
						wVector3f scale=wVECTOR3f_ONE,
						wColor4s color=wCOLOR4s_WHITE,
						Int32 smoothing=0,
						Int32 maxLOD=5,
						wTerrainPatchSize patchSize=wTPS_17);
#else
wNode* wTerrainCreate(const char* cptrFile,
                        wVector3f position,
						wVector3f rotation,
						wVector3f scale,
						wColor4s color,
						Int32 smoothing,
						Int32 maxLOD,
						wTerrainPatchSize patchSize);
#endif


void wTerrainScaleDetailTexture(wNode* terrain,
                                wVector2f scale);

Float32 wTerrainGetHeight(wNode* terrain,
                        wVector2f positionXZ);

///wTiledTerrain///
#ifdef __cplusplus
wNode* wTiledTerrainCreate(wImage* image,
                           Int32 tileSize,
                           wVector2i dataSize,
                           wVector3f position=wVECTOR3f_ZERO,
                           wVector3f rotation=wVECTOR3f_ZERO,
                           wVector3f scale=wVECTOR3f_ONE,
                           wColor4s color=wCOLOR4s_WHITE,
                           Int32 smoothing=0,
                           Int32 maxLOD=5,
                           wTerrainPatchSize patchSize=wTPS_17);
#else
wNode* wTiledTerrainCreate(wImage* image,
                           Int32 tileSize,
                           wVector2i dataSize,
                           wVector3f position,
                           wVector3f rotation,
                           wVector3f scale,
                           wColor4s color,
                           Int32 smoothing,
                           Int32 maxLOD,
                           wTerrainPatchSize patchSize );
#endif // __cplusplus

void wTiledTerrainAddTile(wNode* terrain,
                          wNode* neighbour,
                          wTiledTerrainEdge edge);

void wTiledTerrainSetTileStructure(wNode* terrain,
                                   wImage* image,
                                   wVector2i data);

void wTiledTerrainSetTileColor(wNode* terrain,
                               wImage* image,
                               wVector2i data);

///wSoundBuffer
wSoundBuffer* wSoundBufferLoad(const char* filePath);

wSoundBuffer* wSoundBufferLoadFromMemory(const char* data,
                                           Int32 length,
                                           const char* extension);

void wSoundBufferDestroy(wSoundBuffer* buf);

///wSound///
wSound* wSoundLoad(const char* filePath,
                   bool stream);

wSound* wSoundLoadFromMemory(const char* name,
                             const char* data,
                             Int32 length,
                             const char* extension);

wSound* wSoundLoadFromRaw(const char* name, const char* data,
                          Int32 length,
                          UInt32 frequency,
                          wAudioFormats format);

wSound* wSoundCreateFromBuffer(wSoundBuffer* buf);

bool wSoundIsPlaying(wSound* sound);

bool wSoundIsPaused(wSound* sound);

bool wSoundIsStopped(wSound* sound);

void wSoundSetVelocity(wSound* sound,
                       wVector3f velocity);

wVector3f wSoundGetVelocity(wSound* sound);

void wSoundSetDirection(wSound* sound,
                        wVector3f direction);

wVector3f wSoundGetDirection(wSound* sound);

void wSoundSetVolume(wSound* sound,
                     Float32 value);

Float32 wSoundGetVolume(wSound* sound);

void wSoundSetMaxVolume(wSound* sound,
                        Float32 value);

Float32 wSoundGetMaxVolume(wSound* sound);

void wSoundSetMinVolume(wSound* sound,
                        Float32 value);

Float32 wSoundGetMinVolume(wSound* sound);

void wSoundSetPitch(wSound* sound,
                    Float32 value);

Float32 wSoundGetPitch(wSound* sound);

void wSoundSetRollOffFactor(wSound* sound,
                            Float32 value);

Float32 wSoundGetRollOffFactor(wSound* sound);

void wSoundSetStrength(wSound* sound,
                       Float32 value);

Float32 wSoundGetStrength(wSound* sound);

void wSoundSetMinDistance(wSound* sound,
                          Float32 value);

Float32 wSoundGetMinDistance(wSound* sound);

void wSoundSetMaxDistance(wSound* sound,
                          Float32 Value);

Float32 wSoundGetMaxDistance(wSound* sound);

void wSoundSetInnerConeAngle(wSound* sound,
                             Float32 Value);

Float32 wSoundGetInnerConeAngle(wSound* sound);

void wSoundSetOuterConeAngle(wSound* sound,
                             Float32 Value);

Float32 wSoundGetOuterConeAngle(wSound* sound);

void wSoundSetOuterConeVolume(wSound* sound,
                              Float32 Value);

Float32 wSoundGetOuterConeVolume(wSound* sound);

void wSoundSetDopplerStrength(wSound* sound,
                              Float32 Value);

Float32 wSoundGetDopplerStrength(wSound* sound);

void wSoundSetDopplerVelocity(wSound* sound,
                              wVector3f velocity);

wVector3f wSoundGetDopplerVelocity(wSound* sound);

Float32 wSoundCalculateGain(wSound* sound);

void wSoundSetRelative(wSound* sound,
                       bool value);

bool wSoundIsRelative(wSound* sound);

bool wSoundPlay(wSound* sound,
                bool loop);

void wSoundStop(wSound* sound);

void wSoundPause(wSound* sound);

void wSoundSetLoopMode(wSound* sound,
                       bool value);

bool wSoundIsLooping(wSound* sound);

bool wSoundIsValid(wSound* sound);

#ifdef __cplusplus
bool wSoundSeek(wSound* sound,
                Float32 seconds,
                bool relative = false);
#else
bool wSoundSeek(wSound* sound,
                Float32 seconds,
                bool relative);
#endif // __cplusplus

void wSoundUpdate(wSound* sound);

Float32 wSoundGetTotalAudioTime(wSound* sound);

Int32 wSoundGetTotalAudioSize(wSound* sound);

Int32 wSoundGetCompressedAudioSize(wSound* sound);

Float32 wSoundGetCurrentAudioTime(wSound* sound);

Int32 wSoundGetCurrentAudioPosition(wSound* sound);

Int32 wSoundGetCurrentCompressedAudioPosition(wSound* sound);

UInt32 wSoundGetNumEffectSlotsAvailable(wSound* sound);

///sound effects///
bool wSoundAddEffect(wSound* sound,
                     UInt32 slot,
                     wSoundEffect* effect);

void wSoundRemoveEffect(wSound* sound,
                        UInt32 slot);

wSoundEffect* wSoundCreateEffect();

bool wSoundIsEffectValid(wSoundEffect* effect);

bool wSoundIsEffectSupported(wSoundEffectType type);

UInt32 wSoundGetMaxEffectsSupported();

void wSoundSetEffectType(wSoundEffect* effect,
                         wSoundEffectType type);

wSoundEffectType wSoundGetEffectType(wSoundEffect* effect);

void wSoundSetEffectAutowahParameters(wSoundEffect* effect,
                                      wAutowahParameters param);

void wSoundSetEffectChorusParameters(wSoundEffect* effect,
                                     wChorusParameters param);

void wSoundSetEffectCompressorParameters(wSoundEffect* effect,
                                         wCompressorParameters param);

 void wSoundSetEffectDistortionParameters(wSoundEffect* effect,
                                          wDistortionParameters param);

void wSoundSetEffectEaxReverbParameters(wSoundEffect* effect,
                                        wEaxReverbParameters param);

void wSoundSetEffectEchoParameters(wSoundEffect* effect,
                                   wEchoParameters param);

void wSoundSetEffectEqualizerParameters(wSoundEffect* effect,
                                        wEqualizerParameters param);

void wSoundSetEffectFlangerParameters(wSoundEffect* effect,
                                      wFlangerParameters param);

void wSoundSetEffectFrequencyShiftParameters(wSoundEffect* effect,
                                             wFrequencyShiftParameters param);

void wSoundSetEffectPitchShifterParameters(wSoundEffect* effect,
                                           wPitchShifterParameters param);

void wSoundSetEffectReverbParameters(wSoundEffect* effect,
                                     wReverbParameters param);

void wSoundSetEffectRingModulatorParameters(wSoundEffect* effect,
                                            wRingModulatorParameters param);

void wSoundSetEffectVocalMorpherParameters(wSoundEffect* effect,
                                           wVocalMorpherParameters param);

///Sound filters///
wSoundFilter* wSoundCreateFilter();

bool wSoundIsFilterValid(wSoundFilter* filter);

bool wSoundAddFilter(wSound* sound,
                     wSoundFilter* filter);

void wSoundRemoveFilter(wSound* sound);

bool wSoundIsFilterSupported(wSoundFilterType type);

void wSoundSetFilterType(wSoundFilter* filter,
                         wSoundFilterType type);

wSoundFilterType wSoundGetFilterType(wSoundFilter* filter);

void wSoundSetFilterVolume(wSoundFilter* filter,
                           Float32 volume);

Float32 wSoundGetFilterVolume(wSoundFilter* filter);

void wSoundSetFilterHighFrequencyVolume(wSoundFilter* filter,
                                        Float32 volumeHF);

Float32 wSoundGetFilterHighFrequencyVolume(wSoundFilter* filter);

void wSoundSetFilterLowFrequencyVolume(wSoundFilter* filter,
                                       Float32 volumeLF);

Float32 wSoundGetFilterLowFrequencyVolume(wSoundFilter* filter);

///wVideo///
wVideo* wVideoLoad(const char* fileName);

void wVideoPlay(wVideo* player);

bool wVideoIsPlaying(wVideo* player);

void wVideoRewind(wVideo* player);

void wVideoSetLoopMode(wVideo* player,
                       bool looping);

bool wVideoIsLooping(wVideo* player);

wGuiObject* wVideoCreateTargetImage(wVideo* player,
                                    wVector2i position);

wTexture* wVideoGetTargetTexture(wVideo* player);

wSound* wVideoGetSoundNode(wVideo* player);

void wVideoUpdate(wVideo* player,
                  UInt32 timeMs);

void wVideoPause(wVideo* player);

bool wVideoIsPaused(wVideo* player);

bool wVideoIsAtEnd(wVideo* player);

bool wVideoIsEmpty(wVideo* player);

Int64 wVideoGetFramePosition(wVideo* player);

UInt32 wVideoGetTimePosition(wVideo* player);

wVector2i wVideoGetFrameSize(wVideo* player);

Int32 wVideoGetQuality(wVideo* player);

void wVideoDestroy(wVideo* player);

///wDecal///
wNode* wDecalCreateFromRay(wTexture* texture,
                    wVector3f startRay,
                    wVector3f endRay,
                    Float32 dimension,
                    Float32 textureRotation,
                    Float32 lifeTime,
                    Float32 visibleDistance);

wNode* wDecalCreateFromPoint(wTexture* texture,
                    wVector3f position,
                    wVector3f normal,
                    Float32 dimension,
                    Float32 textureRotation,
                    Float32 lifeTime,
                    Float32 visibleDistance);

Float32 wDecalGetLifeTime(wNode* node);

void  wDecalSetLifeTime(wNode* node,
                        Float32 lifeTime);

Float32 wDecalGetMaxVisibleDistance(wNode* node);

void  wDecalSetMaxVisibleDistance(wNode* node,
                                  Float32 distance);

void  wDecalSetFadeOutParams(wNode* node,
                             const bool isfadeOut,
                             Float32 time);

wMaterial* wDecalGetMaterial(wNode* decal);

void wDecalsClear();///destroy all + disable new

void wDecalsDestroyAll();

void wDecalsCombineAll();

Int32 wDecalsGetCount();

///wNetPacket///
#ifdef __cplusplus
wPacket* wNetPacketCreate(UInt64 id,
						  bool inOrder=true,
                          bool reliable=true,
						  UInt64 priority=100);
#else
wPacket* wNetPacketCreate(UInt64 id,
						  bool inOrder,
                          bool reliable,
                          UInt64 priority);
#endif // __cplusplus

void wNetPacketWriteUInt(wPacket* msg,
						 UInt32 value);

void wNetPacketWriteInt(wPacket* msg,
						Int32 value);

void wNetPacketWriteFloat(wPacket* msg,
                          Float32 value);

void wNetPacketWriteString(wPacket* msg,
                          const char* newString);

UInt32 wNetPacketReadUint(Int32 numPacket);

Int32 wNetPacketReadInt(Int32 numPacket);

Float32 wNetPacketReadFloat(Int32 numPacket);

const char* wNetPacketReadString(Int32 numPacket);

const char* wNetPacketReadMessage(Int32 numPacket);

UInt64 wNetPacketGetId(Int32 numPacket);

const char* wNetPacketGetClientIp(Int32 numPacket);

void* wNetPacketGetClientPtr(Int32 numPacket);

UInt16 wNetPacketGetClientPort(Int32 numPacket);

///wNetManager///
 void wNetManagerSetVerbose(bool value);

void wNetManagerSetMessageId(UInt64 newId);

UInt64 wNetManagerGetMessageId();

void wNetManagerDestroyAllPackets();

Int32 wNetManagerGetPacketsCount();

///wNetServer///
#ifdef __cplusplus
bool wNetServerCreate(UInt16 port,
                      Int32 mode,
                      Int32 maxClientsCount=-1);
#else
bool wNetServerCreate(UInt16 port,
                      Int32 mode,
                      Int32 maxClientsCount);
#endif // __cplusplus

#ifdef __cplusplus
void wNetServerUpdate(Int32 sleepMs=100,
                      Int32 countIteration=100,
                      Int32 maxMSecsToWait=-1);
#else
void wNetServerUpdate(Int32 sleepMs,
					  Int32 countIteration,
                      Int32 maxMSecsToWait);
#endif // __cplusplus

void wNetServerClear();

void wNetServerSendPacket(void* destPtr,
                          wPacket* msg);

void wNetServerBroadcastMessage(const char* text);

void wNetServerAcceptNewConnections(bool value);

#ifdef __cplusplus
void wNetServerStop(Int32 msTime=100);
#else
void wNetServerStop(Int32 msTime);
#endif // __cplusplus

Int32 wNetServerGetClientsCount();

void wNetServerKickClient(void* clientPtr);

void wNetServerUnKickClient(void* clientPtr);

void wNetServerClearBannedList();

///wNetClient///
#ifdef __cplusplus
bool wNetClientCreate(const char* address,
                      UInt16 port,
                      Int32 mode,
                      Int32 maxMSecsToWait=500);
#else
bool wNetClientCreate(const char* address,
                      UInt16 port,
                      Int32 mode,
                      Int32 maxMSecsToWait);
#endif // __cplusplus

#ifdef __cplusplus
void wNetClientUpdate(Int32 maxMessagesToProcess=100,
                      Int32 countIteration=100,
                      Int32 maxMSecsToWait=-1);
#else
void wNetClientUpdate(Int32 maxMessagesToProcess,
                      Int32 countIteration,
                      Int32 maxMSecsToWait);
#endif // __cplusplus

#ifdef __cplusplus
void wNetClientDisconnect(Int32 maxMSecsToWait=500);
#else
void wNetClientDisconnect(Int32 maxMSecsToWait);
#endif // __cplusplus

#ifdef __cplusplus
void wNetClientStop(Int32 maxMSecsToWait=500);
#else
void wNetClientStop(Int32 maxMSecsToWait);
#endif // __cplusplus

bool wNetClientIsConnected();

void wNetClientSendMessage(const char* text);

void wNetClientSendPacket(wPacket* msg);

///wPhys///
bool wPhysStart();

void wPhysUpdate(Float32 timeStep);

void wPhysStop();

void wPhysSetGravity(wVector3f gravity);

void wPhysSetWorldSize(wVector3f size);

void wPhysSetSolverModel(wPhysSolverModel model);

void wPhysSetFrictionModel(wPhysFrictionModel model);

void wPhysDestroyAllBodies();

void wPhysDestroyAllJoints();

Int32 wPhysGetBodiesCount();

Int32 wPhysGetJointsCount();

wNode* wPhysGetBodyPicked(wVector2i position,
                          bool mouseLeftKey);

wNode* wPhysGetBodyFromRay(wVector3f start,
                           wVector3f end);

wNode* wPhysGetBodyFromScreenCoords(wVector2i position);

wNode* wPhysGetBodyByName(const char* name);

wNode* wPhysGetBodyById(Int32 Id);

wNode* wPhysGetBodyByIndex(Int32 idx);

wNode* wPhysGetJointByName(const char* name);

wNode* wPhysGetJointById(Int32 Id);

wNode* wPhysGetJointByIndex(Int32 idx);

///wPhysBody///
wNode* wPhysBodyCreateNull();

wNode* wPhysBodyCreateCube(wVector3f size,
                           Float32 Mass);

wNode* wPhysBodyCreateSphere(wVector3f radius,
                             Float32 Mass);

wNode* wPhysBodyCreateCone(Float32 radius,
                           Float32 height,
                           Float32 mass,
                           bool Offset);

wNode* wPhysBodyCreateCylinder(Float32 radius,
                               Float32 height,
                               Float32 mass,
                               bool Offset);

#ifdef __cplusplus
wNode* wPhysBodyCreateCapsule(Float32 radius,
                              Float32 height,
                              Float32 mass,
                              bool Offset=false);
#else
wNode* wPhysBodyCreateCapsule(Float32 radius,
                              Float32 height,
                              Float32 mass,
                              bool Offset);
#endif // __cplusplus

wNode* wPhysBodyCreateHull(wNode* mesh,
                           Float32 mass);

wNode* wPhysBodyCreateTree(wNode* mesh);

wNode* wPhysBodyCreateTreeBsp(wMesh* mesh,
                              wNode* node);

wNode* wPhysBodyCreateTerrain(wNode* mesh,
                              Int32 LOD);

wNode* wPhysBodyCreateHeightField(wNode* mesh);

wNode* wPhysBodyCreateWaterSurface(wVector3f size,
                                   Float32 FluidDensity,
                                   Float32 LinearViscosity,
                                   Float32 AngulaViscosity);

wNode* wPhysBodyCreateCompound(wNode** nodes,
                               Int32 CountNodes,
                               Float32 mass);

///=> такой функции НЕ ТРЕБУЕТСЯ, так как
///так как здесь работает wNodeDestroy()
///void wPhysBodyDestroy(void* body);

void wPhysBodySetName(wNode* body,
                      const char* name);

const char* wPhysBodyGetName(wNode* body);

void wPhysBodySetFreeze(wNode* body,
                        bool freeze);

bool wPhysBodyIsFreeze(wNode* body);

void wPhysBodySetMaterial(wNode* body,
                          Int32 MatId);

Int32 wPhysBodyGetMaterial(wNode* body);

void wPhysBodySetGravity(wNode* body,
                        wVector3f gravity);

wVector3f wPhysBodyGetGravity(wNode* body);

void wPhysBodySetMass(wNode* body,
                      Float32 NewMass);

Float32 wPhysBodyGetMass(wNode* body);

void wPhysBodySetCenterOfMass(wNode* body,
                              wVector3f center);

wVector3f wPhysBodyGetCenterOfMass(wNode* body);

void wPhysBodySetMomentOfInertia(wNode* body,
                                 wVector3f value);

wVector3f wPhysBodyGetMomentOfInertia(wNode* body);

void wPhysBodySetAutoSleep(wNode* body,
                           bool value);

bool wPhysBodyIsAutoSleep(wNode* body);

void wPhysBodySetLinearVelocity(wNode* body,
                                wVector3f velocity);

wVector3f wPhysBodyGetLinearVelocity(wNode* body);

void wPhysBodySetAngularVelocity(wNode* body,
                                 wVector3f velocity);

wVector3f wPhysBodyGetAngularVelocity(wNode* body);

void wPhysBodySetLinearDamping(wNode* body,
                               Float32 linearDamp);

Float32 wPhysBodyGetLinearDamping(wNode* body);

void wPhysBodySetAngularDamping(wNode* body,
                                wVector3f damping);

wVector3f wPhysBodyGetAngularDamping(wNode* body);

void wPhysBodyAddImpulse(wNode* body,
                         wVector3f velosity,
                         wVector3f position);

void wPhysBodyAddForce(wNode* body,
                       wVector3f force);

void wPhysBodyAddTorque(wNode* body,
                        wVector3f torque);

bool wPhysBodiesIsCollide(wNode* body1,
                          wNode* body2);

wVector3f wPhysBodiesGetCollisionPoint(wNode* body1,
                                       wNode* body2);

wVector3f wPhysBodiesGetCollisionNormal(wNode* body1,
                                        wNode* body2);

void wPhysBodyDraw(wNode* body);

///wPhysJoint///
wNode* wPhysJointCreateBall(wVector3f position,
                            wVector3f pinDir,
                            wNode* body1,
                            wNode* body2);

wNode* wPhysJointCreateHinge(wVector3f position,
                             wVector3f pinDir,
                             wNode* body1,
                             wNode* body2);

wNode* wPhysJointCreateSlider(wVector3f position,
                              wVector3f pinDir,
                              wNode* body1,
                              wNode* body2);

wNode* wPhysJointCreateCorkScrew(wVector3f position,
                                 wVector3f pinDir,
                                 wNode* body1,
                                 wNode* body2);

wNode* wPhysJointCreateUpVector(wVector3f position,
                                wNode* body);

void wPhysJointSetName(wNode* joint,
                       const char* name);

const char* wPhysJointGetName(wNode* joint);

void wPhysJointSetCollisionState(wNode* joint,
                                 bool isCollision);

bool wPhysJointIsCollision(wNode* Joint);

void wPhysJointSetBallLimits(wNode* joint,
                             Float32 MaxConeAngle,
                             wVector2f twistAngles);

void wPhysJointSetHingeLimits(wNode* joint,
                              wVector2f anglesLimits);

void wPhysJointSetSliderLimits(wNode* joint,
                               wVector2f anglesLimits);

void wPhysJointSetCorkScrewLinearLimits(wNode* joint,
                                        wVector2f distLimits);

void wPhysJointSetCorkScrewAngularLimits(wNode* joint,
                                         wVector2f distLimits);

///wPhysPlayerController///
wNode* wPhysPlayerControllerCreate(wVector3f position,
                                   wNode* body,
                                   Float32 maxStairStepFactor,
                                   Float32 cushion);

void wPhysPlayerControllerSetVelocity(wNode* joint,
                                      Float32 forwardSpeed,
                                      Float32 sideSpeed,
                                      Float32 heading);

///wPhysVehicle
wNode* wPhysVehicleCreate(Int32 tiresCount,
                          wPhysVehicleType rayCastType,
                          wNode* CarBody);

Int32 wPhysVehicleAddTire(wNode* Car,
                          wNode* UserData,
                          wPhysVehicleTireType tireType,
                          wVector3f position,
                          Float32 Mass,
                          Float32 Radius,
                          Float32 Width,
                          Float32 SLenght,
                          Float32 SConst,
                          Float32 SDamper);

Float32 wPhysVehicleGetSpeed(wNode* Car);

Int32 wPhysVehicleGetTiresCount(wNode* Car);

void wPhysVehicleSetBrake(wNode* Car,
                          bool value);

bool wPhysVehicleIsBrake(wNode* Car);

bool wPhysVehicleIsAllTiresCollided(wNode* Car);

void wPhysVehicleSetSteering(wNode* Car,
                             Float32 angle);///-1.......+1

Float32 wPhysVehicleGetSteering(wNode* Car);/// return value: -1......+1

void wPhysVehicleSetTireMaxSteerAngle(wNode* Car,
                                      Int32 tireIndex,
                                      Float32 angleDeg);

Float32 wPhysVehicleGetTireMaxSteerAngle(wNode* Car,
                                         Int32 tireIndex);

wNode* wPhysVehicleGetBody(wNode* Car);

void wPhysVehicleSetMotorValue(wNode* Car,
                              Float32 value);

Float32 wPhysVehicleGetMotorValue(wNode* Car);

///Vehicle Tires
wVector3f wPhysVehicleGetTireLocalPosition(wNode* Car,
                                           Int32 tireIndex);

Float32 wPhysVehicleGetTireUpDownPosition(wNode* Car,
                                          Int32 tireIndex);

/*bool wPhysVehicleIsTireOnAir(wNode* Car,
                             Int32 tireIndex);*/

Float32 wPhysVehicleGetTireAngularVelocity(wNode* Car,
                                           Int32 tireIndex);

Float32 wPhysVehicleGetTireSpeed(wNode* Car,
                                 Int32 tireIndex);

wVector3f wPhysVehicleGetTireContactPoint(wNode* Car,
                                          Int32 tireIndex);

wVector3f wPhysVehicleGetTireContactNormal(wNode* Car,
                                           Int32 tireIndex);

bool wPhysVehicleIsTireBrake(wNode* Car,
                             Int32 tireIndex);

wPhysVehicleTireType wPhysVehicleGetTireType(wNode* Car,
                                             Int32 tireIndex);

void wPhysVehicleSetTireType(wNode* Car,
                             Int32 tireIndex,
                             wPhysVehicleTireType tireType);

void wPhysVehicleSetTireBrakeForce(wNode* Car,
                                   Int32 tireIndex,
                                   Float32 value);

Float32 wPhysVehicleGetTireBrakeForce(wNode* Car,
                                      Int32 tireIndex);

void wPhysVehicleSetTireBrakeLateralFriction(wNode* Car,
                                             Int32 tireIndex,
                                             Float32 value);

Float32 wPhysVehicleGetTireBrakeLateralFriction(wNode* Car,
                                                Int32 tireIndex);

void wPhysVehicleSetTireBrakeLongitudinalFriction(wNode* Car,
                                                  Int32 tireIndex,
                                                  Float32 value);

Float32 wPhysVehicleGetTireBrakeLongitudinalFriction(wNode* Car,
                                                     Int32 tireIndex);

void wPhysVehicleSetTireLateralFriction(wNode* Car,
                                        Int32 tireIndex,
                                        Float32 value);

Float32 wPhysVehicleGetTireLateralFriction(wNode* Car,
                                           Int32 tireIndex);

void wPhysVehicleSetTireLongitudinalFriction(wNode* Car,
                                             Int32 tireIndex,
                                             Float32 value);

Float32 wPhysVehicleGetTireLongitudinalFriction(wNode* Car,
                                                Int32 tireIndex);

void wPhysVehicleSetTireMass(wNode* Car,
                             Int32 tireIndex,
                             Float32 mass);

Float32 wPhysVehicleGetTireMass(wNode* Car,
                                Int32 tireIndex);

void wPhysVehicleSetTireRadius(wNode* Car,
                               Int32 tireIndex,
                               Float32 radius);

Float32 wPhysVehicleGetTireRadius(wNode* Car,
                                  Int32 tireIndex);

void wPhysVehicleSetTireWidth(wNode* Car,
                              Int32 tireIndex,
                              Float32 width);

Float32 wPhysVehicleGetTireWidth(wNode* Car,
                                 Int32 tireIndex);

void  wPhysVehicleSetTireSpringConst(wNode* Car,
                                     Int32 tireIndex,
                                     Float32 value);

Float32 wPhysVehicleGetTireSpringConst(wNode* Car,
                                       Int32 tireIndex);

void wPhysVehicleSetTireSpringDamper(wNode* Car,
                                     Int32 tireIndex,
                                     Float32 value);

Float32 wPhysVehicleGetTireSpringDamper(wNode* Car,
                                        Int32 tireIndex);

void wPhysVehicleSetTireSuspensionLenght(wNode* Car,
                                         Int32 tireIndex,
                                         Float32 value);

Float32 wPhysVehicleGetTireSuspensionLenght(wNode* Car,
                                            Int32 tireIndex);

void wPhysVehicleSetTireUserData(wNode* Car,
                                 Int32 tireIndex,
                                 wNode* userData);

wNode* wPhysVehicleGetTireUserData(wNode* Car,
                                   Int32 tireIndex);

void wPhysVehicleSetTireMotorForce(wNode* Car,
                                   Int32 tireIndex,
                                   Float32 value);

Float32 wPhysVehicleGetTireMotorForce(wNode* Car,
                                      Int32 tireIndex);

void wPhysVehicleSetTireTurnForceHelper(wNode* Car,
                                        Int32 tireIndex,
                                        Float32 value);

Float32 wPhysVehicleGetTireTurnForceHelper(wNode* Car,
                                           Int32 tireIndex);
void wPhysVehicleSetTireSpinTorqueFactor(wNode* Car,
                                         Int32 tireIndex,
                                         Float32 value);

Float32 wPhysVehicleGetTireSpinTorqueFactor(wNode* Car,
                                            Int32 tireIndex);

void wPhysVehicleSetTireTorquePosition(wNode* Car,
                                       Int32 tireIndex,
                                       wVector3f position);

wVector3f  wPhysVehicleGetTireTorquePosition(wNode* Car,
                                             Int32 tireIndex);

Float32 wPhysVehicleGetTireLoad(wNode* Car,
                              Int32 tireIndex);

void wPhysVehicleSetTireSpinForce(wNode* Car,
                                  Int32 tireIndex,
                                  Float32 value);

Float32 wPhysVehicleGetTireSpinForce(wNode* Car,
                                     Int32 tireIndex);

/*///wPhysRagDoll
UInt32* wPhysRagDollCreate(wNode* node,
                           Int32 bonesCount,
                           wPhysRagDollBoneParameters* params);

Int32 wPhysRagDollGetBonesCount(wNode* ragdoll);

void wPhysRagDollSetBoneCollisionState(wNode* ragdoll,
                                       Int32 boneIndex,
                                       Int32 state);

void wPhysRagDollSetBoneConeLimits(wNode* ragdoll,
                                   Int32 boneIndex,
                                   Float32 angle);

void wPhysRagDollSetBoneTwistLimits(wNode* ragdoll,
                                    Int32 boneIndex,
                                    Float32 minAngle,
                                    Float32 maxAngle);

void wPhysRagDollDraw(wNode* ragdoll);*/

///wPhysMaterial///
int	 wPhysMaterialCreate();

void wPhysMaterialSetElasticity(Int32 matId1,
                                Int32 matId2,
                                Float32 Elasticity);

void wPhysMaterialSetFriction(Int32 matId1,
                              Int32 matId2,
                              Float32 StaticFriction,
                              Float32 KineticFriction);

void wPhysMaterialSetContactSound(Int32 matId1,
                                  Int32 matId2,
                                  wSound* soundNode);

void  wPhysMaterialSetSoftness(Int32 matId1,
                               Int32 matId2,
                               Float32 Softness);

void wPhysMaterialSetCollidable(Int32 matId1,
                                Int32 matId2,
                                bool isCollidable);

///wGui///
void wGuiDrawAll();

void wGuiDestroyAll();

bool wGuiIsEventAvailable();

wGuiEvent* wGuiReadEvent();

#ifdef __cplusplus
bool wGuiLoad(const char* fileName,
              wGuiObject* start=0);
#else
bool wGuiLoad(const char* fileName,
              wGuiObject* start);
#endif // __cplusplus

#ifdef __cplusplus
bool wGuiSave(const char* fileName,
              wGuiObject* start=0);
#else
bool wGuiSave(const char* fileName,
              wGuiObject* start);
#endif // __cplusplus

wGuiObject* wGuiGetSkin();

void wGuiSetSkin(wGuiObject* skin);

const wchar_t* wGuiGetLastSelectedFile();

///Returns the element which holds the focus.
wGuiObject* wGuiGetObjectFocused();

///Returns the element which was last under the mouse cursor.
wGuiObject* wGuiGetObjectHovered();

wGuiObject* wGuiGetRootNode();

wGuiObject* wGuiGetObjectById(Int32 id,
                              bool searchchildren);

wGuiObject* wGuiGetObjectByName(const char* name,
                                bool searchchildren);

///wGuiObject///
void wGuiObjectDestroy(wGuiObject* element );

void wGuiObjectSetParent(wGuiObject* element,
                         wGuiObject* parent);

wGuiObject* wGuiObjectGetParent(wGuiObject* element);

void wGuiObjectSetRelativePosition(wGuiObject* element,
                                   wVector2i position);

void wGuiObjectSetRelativeSize(wGuiObject* element,
                               wVector2i size);

wVector2i wGuiObjectGetRelativePosition(wGuiObject* element);

wVector2i wGuiObjectGetRelativeSize(wGuiObject* element);

wVector2i wGuiObjectGetAbsolutePosition(wGuiObject* element);

wVector2i wGuiObjectGetAbsoluteClippedPosition(wGuiObject* element);

wVector2i wGuiObjectGetAbsoluteClippedSize(wGuiObject* element);

///Sets whether the element will ignore its parent's clipping rectangle.
void wGuiObjectSetClippingMode(wGuiObject* element,
                               bool value);

bool wGuiObjectIsClipped(wGuiObject* element);

void wGuiObjectSetMaxSize(wGuiObject* element,
                          wVector2i size);

void wGuiObjectSetMinSize(wGuiObject* element,
                          wVector2i size);

void wGuiObjectSetAlignment(wGuiObject* element,
                            wGuiAlignment left,
                            wGuiAlignment right,
                            wGuiAlignment top,
                            wGuiAlignment bottom);

void wGuiObjectUpdateAbsolutePosition(wGuiObject* element);

///Возвращает гуи-объект -потомок element-а, который находится на пересечении
///с точкой экрана position
///Если нужен любой объект, то в качестве element-а нужно
///поставить root=wGuiGetRootNode()
///Примечание: Элемент root имеет размер ВСЕГО экрана
wGuiObject* wGuiObjectGetFromScreenPos(wGuiObject* element,
                                       wVector2i position);

///Персекается ли объект с точкой экрана position
bool wGuiObjectIsPointInside(wGuiObject* element,
                             wVector2i position);

void wGuiObjectDestroyChild(wGuiObject* element,
                            wGuiObject* child);

///Можно вызывать вместо wGuiDrawAll() для конкретного элемента
void wGuiObjectDraw(wGuiObject* element);

void wGuiObjectMoveTo(wGuiObject* element,
                      wVector2i position);

void wGuiObjectSetVisible(wGuiObject* element,
                          bool value);

bool wGuiObjectIsVisible(wGuiObject* element);

/// Устанавливает, был ли этот элемент управления создан
/// как часть родительского элемента.
/// Например, если полоса прокрутки является частью списка.
/// Подразделы не сохраняются на диск при вызове wGuiSave()
void wGuiObjectSetSubObject(wGuiObject* element,
                            bool value);

/// Вовзращает, был ли этот элемент управления создан
/// как часть родительского элемента.
bool wGuiObjectIsSubObject(wGuiObject* element);

void wGuiObjectSetTabStop(wGuiObject* element,
                          bool value);

///Returns true if this element can be focused by navigating with the tab key.
bool wGuiObjectIsTabStop(wGuiObject* element);

///Sets the priority of focus when using the tab key to navigate between a group of elements.
void wGuiObjectSetTabOrder(wGuiObject* element,
                           Int32 index);

Int32 wGuiObjectGetTabOrder(wGuiObject* element);

///If set to true, the focus will visit this element when using the tab key to cycle through elements.
void wGuiObjectSetTabGroup(wGuiObject* element,
                           bool value);

bool wGuiObjectIsTabGroup(wGuiObject* element);

void wGuiObjectSetEnable(wGuiObject* element,
                         bool value);

bool wGuiObjectIsEnabled(wGuiObject* element);

void wGuiObjectSetText(wGuiObject* element,
                       const wchar_t* text);

const wchar_t* wGuiObjectGetText(wGuiObject* element);

///Sets the new caption of this element.
void wGuiObjectSetToolTipText(wGuiObject* element,
                              const wchar_t* text);

const wchar_t* wGuiObjectGetToolTipText(wGuiObject* element);

void wGuiObjectSetId(wGuiObject* element,
                     Int32 id);

Int32 wGuiObjectGetId(wGuiObject* element);

void wGuiObjectSetName(wGuiObject* element,
                       const char* name);

bool wGuiObjectIsHovered(wGuiObject* el);

const char* wGuiObjectGetName(wGuiObject* element);

///Ищет  среди "детей" объекта искомого по его Id
///Если требуется найти ЛЮБОЙ ГУИ-объект сцены,
///нужно в качестве элемента указать root=wGuiGetRootNode()
wGuiObject* wGuiObjectGetChildById(wGuiObject* element,
                                   Int32 id,
                                   bool searchchildren);

wGuiObject* wGuiObjectGetChildByName(wGuiObject* element,
                                     const char* name,
                                     bool searchchildren);

bool wGuiObjectIsChildOf(wGuiObject* element,
                         wGuiObject* child);

bool wGuiObjectBringToFront(wGuiObject* element,
                            wGuiObject* subElement);

wGuiElementType wGuiObjectGetType(wGuiObject* element);

const char* wGuiObjectGetTypeName(wGuiObject* element);

bool wGuiObjectHasType(wGuiObject* element,
                       wGuiElementType type);

bool wGuiObjectSetFocus(wGuiObject* element);

bool wGuiObjectRemoveFocus(wGuiObject* element);

bool wGuiObjectIsFocused(wGuiObject* element);

void wGuiObjectReadFromXml(wGuiObject* node,
                           wXmlReader* reader);

void wGuiObjectWriteToXml(wGuiObject* node,
                          wXmlWriter* writer);

///wGuiSkin///
wGuiObject* wGuiSkinCreate(wGuiSkinSpace type);

wColor4s wGuiSkinGetColor(wGuiObject* skin,
                          wGuiDefaultColor elementType);

void wGuiSkinSetColor(wGuiObject* skin,
                      wGuiDefaultColor elementType,
                      wColor4s color);

void wGuiSkinSetSize(wGuiObject* skin,
                     wGuiDefaultSize sizeType,
                     Int32 newSize);

Int32 wGuiSkinGetSize(wGuiObject* skin,
                    wGuiDefaultSize sizeType);

const wchar_t* wGuiSkinGetDefaultText(wGuiObject* skin,
                                      wGuiDefaultText txt);

void wGuiSkinSetDefaultText(wGuiObject* skin,
                            wGuiDefaultText txt,
                            const wchar_t* newText);

#ifdef __cplusplus
void wGuiSkinSetFont(wGuiObject* skin,
                     wFont* font,
                     wGuiDefaultFont fntType=wGDF_DEFAULT);
#else
void wGuiSkinSetFont(wGuiObject* skin,
                     wFont* font,
                     wGuiDefaultFont fntType);
#endif // __cplusplus

#ifdef __cplusplus
wFont* wGuiSkinGetFont(wGuiObject* skin,
                       wGuiDefaultFont fntType=wGDF_DEFAULT);
#else
wFont* wGuiSkinGetFont(wGuiObject* skin,
                       wGuiDefaultFont fntType);
#endif // __cplusplus

void wGuiSkinSetSpriteBank(wGuiObject* skin,
							wGuiObject* bank);

wGuiObject* wGuiSkinGetSpriteBank(wGuiObject* skin);

void wGuiSkinSetIcon(wGuiObject* skin,
                     wGuiDefaultIcon icn,
                     UInt32 index);

UInt32 wGuiSkinGetIcon(wGuiObject* skin,
                             wGuiDefaultIcon icn);

wGuiSkinSpace wGuiSkinGetType(wGuiObject* skin);

///wGuiWindow///
wGuiObject* wGuiWindowCreate(const wchar_t* wcptrTitle,
                             wVector2i minPos,
                             wVector2i maxPos,
                             bool modal);

wGuiObject* wGuiWindowGetButtonClose(wGuiObject* win);

wGuiObject* wGuiWindowGetButtonMinimize(wGuiObject* win);

wGuiObject* wGuiWindowGetButtonMaximize(wGuiObject* win);

void wGuiWindowSetDraggable(wGuiObject* win,
                            bool value);

bool wGuiWindowIsDraggable(wGuiObject* win);

void wGuiWindowSetDrawBackground(wGuiObject* win,
                                 bool value);

bool wGuiWindowIsDrawBackground(wGuiObject* win);

void wGuiWindowSetDrawTitleBar(wGuiObject* win,
                               bool value);

bool wGuiWindowIsDrawTitleBar(wGuiObject* win);

///wGuiLabel
#ifdef __cplusplus
wGuiObject* wGuiLabelCreate(const wchar_t* wcptrText,
                            wVector2i minPos,
                            wVector2i maxPos,
                            bool boBorder=false,
                            bool boWordWrap=true);
#else
wGuiObject* wGuiLabelCreate(const wchar_t * wcptrText,
                            wVector2i minPos,
                            wVector2i maxPos,
                            bool boBorder,
                            bool boWordWrap);
#endif // __cplusplus

wVector2i wGuiLabelGetTextSize(wGuiObject* txt);

void wGuiLabelSetOverrideFont(wGuiObject* obj,
                              wFont* font);

wFont* wGuiLabelGetOverrideFont(wGuiObject* obj);

wFont* wGuiLabelGetActiveFont(wGuiObject* obj);

void wGuiLabelEnableOverrideColor(wGuiObject* obj,
                                             bool value);

bool wGuiLabelIsOverrideColor(wGuiObject* obj);

void wGuiLabelSetOverrideColor(wGuiObject* obj,
                                           wColor4s color);

wColor4s wGuiLabelGetOverrideColor(wGuiObject* obj);

void wGuiLabelSetDrawBackground(wGuiObject* obj,
                                           bool value);

bool wGuiLabelIsDrawBackGround(wGuiObject* obj);

void wGuiLabelSetDrawBorder(wGuiObject* obj,
                                       bool value);

bool wGuiLabelIsDrawBorder(wGuiObject* obj) ;

void wGuiLabelSetTextAlignment(wGuiObject* obj,
                                          wGuiAlignment Horizontalvalue,
                                          wGuiAlignment Verticalvalue);

void wGuiLabelSetWordWrap(wGuiObject* obj,
                                      bool value);

bool wGuiLabelIsWordWrap(wGuiObject* obj);

void wGuiLabelSetBackgroundColor(wGuiObject* obj,
                                 wColor4s color);

wColor4s wGuiLabelGetBackgroundColor(wGuiObject* obj);

///wGuiButton
wGuiObject* wGuiButtonCreate(wVector2i minPos,
                             wVector2i maxPos,
							 const wchar_t* wcptrLabel,
                             const wchar_t* wcptrTip);

void wGuiButtonSetImage(wGuiObject* btn,
                        wTexture* img);

void wGuiButtonSetImageFromRect(wGuiObject* btn,
                                wTexture* img,
                                wVector2i* minRect,
                                wVector2i* maxRect);

void wGuiButtonSetPressedImage(wGuiObject* btn,
                               wTexture* img);


void wGuiButtonSetPressedImageFromRect(wGuiObject* btn,
                                       wTexture* img,
                                       wVector2i* minRect,
                                       wVector2i* maxRect);

void wGuiButtonSetSpriteBank(wGuiObject* btn,
                             wGuiObject* bank);

void wGuiButtonSetSprite(wGuiObject* btn,
                         wGuiButtonState state,
                         Int32 index,
                         wColor4s color,
                         bool loop);

void wGuiButtonSetPush(wGuiObject* btn,
                       bool value);

bool wGuiButtonIsPushed(wGuiObject* btn);

void wGuiButtonSetPressed(wGuiObject* btn,
                          bool value);

bool wGuiButtonIsPressed(wGuiObject* btn);

void wGuiButtonUseAlphaChannel(wGuiObject* btn,
                               bool value);

bool wGuiButtonIsUsedAlphaChannel(wGuiObject* btn);

void wGuiButtonEnableScaleImage(wGuiObject* btn,
                                bool value);

bool wGuiButtonIsScaledImage(wGuiObject* btn);

void wGuiButtonSetOverrideFont(wGuiObject* obj,
                               wFont* font);

wFont* wGuiButtonGetOverrideFont(wGuiObject* obj);

wFont* wGuiButtonGetActiveFont(wGuiObject* obj);

void wGuiButtonSetDrawBorder(wGuiObject* obj,
                             bool value);

bool wGuiButtonIsDrawBorder(wGuiObject* obj);

///wGuiButtonGroup///
wGuiObject* wGuiButtonGroupCreate(wVector2i minPos,
                                  wVector2i maxPos);

Int32 wGuiButtonGroupAddButton(wGuiObject* group,
                             wGuiObject* button);

Int32 wGuiButtonGroupInsertButton(wGuiObject* group,
                                wGuiObject* button,
                                UInt32 index);

wGuiObject* wGuiButtonGroupGetButton(wGuiObject* group,
                                     UInt32 index);

bool wGuiButtonGroupRemoveButton(wGuiObject* group,
                                 UInt32 index);

void wGuiButtonGroupRemoveAll(wGuiObject* group);

UInt32 wGuiButtonGroupGetSize(wGuiObject* group);

Int32 wGuiButtonGroupGetSelectedIndex(wGuiObject* group);

void wGuiButtonGroupSetSelectedIndex(wGuiObject* group,
                                     Int32 index);

void wGuiButtonGroupClearSelection(wGuiObject* group);

void wGuiButtonGroupSetBackgroundColor(wGuiObject* group,
                                       wColor4s color);

void wGuiButtonGroupDrawBackground(wGuiObject* group,
                                   bool value);
///wGuiListBox///
wGuiObject* wGuiListBoxCreate(wVector2i minPos,
                              wVector2i maxPos,
                              bool background);

UInt32 wGuiListBoxGetItemsCount(wGuiObject* lbox);

const wchar_t* wGuiListBoxGetItemByIndex(wGuiObject* lbox,
                                         UInt32 id);

UInt32 wGuiListBoxAddItem(wGuiObject* lbox,
                                const wchar_t* text);

UInt32 wGuiListBoxAddItemWithIcon(wGuiObject* lbox,
                                        const wchar_t* text,
                                        Int32 icon);

void wGuiListBoxRemoveItem(wGuiObject* lbox,
                           UInt32 index);

void wGuiListBoxRemoveAll(wGuiObject* lbox);

void wGuiListBoxSetItem(wGuiObject* lbox,
                        UInt32 index,
                        const wchar_t* text,
                        Int32 icon);

void wGuiListBoxInsertItem(wGuiObject* lbox,
                           UInt32 index,
                           const wchar_t* text,
                           Int32 icon);

Int32 wGuiListBoxGetItemIcon(wGuiObject* lbox,
                           UInt32 index);

UInt32 wGuiListBoxGetSelectedIndex(wGuiObject* lbox);

void wGuiListBoxSelectItemByIndex(wGuiObject* lbox,
                                  UInt32 index);

void wGuiListBoxSelectItemByText(wGuiObject* lbox,
                                 const wchar_t* item);

void wGuiListBoxSwapItems(wGuiObject* lbox,
                          UInt32 index1,
                          UInt32 index2);

void wGuiListBoxSetItemsHeight(wGuiObject* lbox,
                               Int32 height);

void wGuiListBoxSetAutoScrolling(wGuiObject* lbox,
                                 bool scroll);

bool wGuiListBoxIsAutoScrolling(wGuiObject* lbox);

void wGuiListBoxSetItemColor(wGuiObject* lbox,
                             UInt32 index,
                             wColor4s color);

void wGuiListBoxSetElementColor(wGuiObject* lbox,
                                UInt32 index,
                                wGuiListboxColor colorType,
                                wColor4s color);

void wGuiListBoxClearItemColor(wGuiObject* lbox,
                               UInt32 index);

void wListBoxClearElementColor(wGuiObject* lbox,
                               UInt32 index,
                               wGuiListboxColor colorType);

wColor4s wGuiListBoxGetElementColor(wGuiObject* lbox,
                                    UInt32 index,
                                    wGuiListboxColor colorType);

bool wGuiListBoxHasElementColor(wGuiObject* lbox,
                                UInt32 index,
                                wGuiListboxColor colorType);

wColor4s wGuiListBoxGetDefaultColor(wGuiObject* lbox,
                                    wGuiListboxColor colorType);

void wGuiListBoxSetDrawBackground(wGuiObject* obj,
                                  bool value);

///wGuiScrollBar
wGuiObject* wGuiScrollBarCreate(bool Horizontal,
                                wVector2i minPos,
                                wVector2i maxPos);

void wGuiScrollBarSetMaxValue(wGuiObject* scroll,
                              Int32 max);

Int32 wGuiScrollBarGetMaxValue(wGuiObject* scroll);

void wGuiScrollBarSetMinValue(wGuiObject* scroll,
                              Int32 min);

Int32 wGuiScrollBarGetMinValue(wGuiObject* scroll);

void wGuiScrollBarSetValue(wGuiObject* scroll,
                           Int32 value);

Int32 wGuiScrollBarGetValue(wGuiObject* scroll);

void wGuiScrollBarSetSmallStep(wGuiObject* scroll,
                               Int32 step);

Int32 wGuiScrollBarGetSmallStep(wGuiObject* scroll);

void wGuiScrollBarSetLargeStep(wGuiObject* scroll,
                               Int32 step);

Int32 wGuiScrollBarGetLargeStep(wGuiObject* scroll);

///wGuiEditBox
wGuiObject* wGuiEditBoxCreate(const wchar_t* wcptrText,
                              wVector2i minPos,
                              wVector2i maxPos);

void wGuiEditBoxSetMultiLine(wGuiObject* box,
                             bool value);

bool wGuiEditBoxIsMultiLine(wGuiObject* box);

void wGuiEditBoxSetAutoScrolling(wGuiObject* box,
                                 bool value);

bool wGuiEditBoxIsAutoScrolling(wGuiObject* box);

void wGuiEditBoxSetPasswordMode(wGuiObject* box,
                                bool value);

bool wGuiEditBoxIsPasswordMode(wGuiObject* box);

wVector2i wGuiEditBoxGetTextSize(wGuiObject* box);

///Sets the maximum amount of characters which may be entered in the box.
void wGuiEditBoxSetCharactersLimit(wGuiObject* box,
                                   UInt32 max);

UInt32 wGuiEditGetCharactersLimit(wGuiObject* box);

void wGuiEditBoxSetOverrideFont(wGuiObject* obj,
                                wFont* font);

wFont* wGuiEditBoxGetOverrideFont(wGuiObject* obj);

wFont* wGuiEditBoxGetActiveFont(wGuiObject* obj);

void wGuiEditBoxEnableOverrideColor(wGuiObject* obj,
                                    bool value);

bool wGuiEditBoxIsOverrideColor(wGuiObject* obj);

void wGuiEditBoxSetOverrideColor(wGuiObject* obj,
                                 wColor4s color);

wColor4s wGuiEditBoxGetOverrideColor(wGuiObject* obj);

void wGuiEditBoxSetDrawBackground(wGuiObject* obj,
                                  bool value);

void wGuiEditBoxSetDrawBorder(wGuiObject* obj,
                              bool value);

bool wGuiEditBoxIsDrawBorder(wGuiObject* obj);

void wGuiEditBoxSetTextAlignment(wGuiObject* obj,
                                 wGuiAlignment Horizontalvalue,
                                 wGuiAlignment Verticalvalue);

void wGuiEditBoxSetWordWrap(wGuiObject* obj,
                            bool value);

bool wGuiEditBoxIsWordWrap(wGuiObject* obj);

///wGuiImage///
wGuiObject* wGuiImageCreate(wTexture* texture,
                            wVector2i size,
                            bool useAlpha);

void wGuiImageSet(wGuiObject* img,
                  wTexture* tex);

wTexture* wGuiImageGet(wGuiObject* img);

void wGuiImageSetColor(wGuiObject* img,
                       wColor4s color);

wColor4s wGuiImageGetColor(wGuiObject* img);

void wGuiImageSetScaling(wGuiObject* img,
                         bool scale);

bool wGuiImageIsScaled(wGuiObject* img);

void wGuiImageUseAlphaChannel(wGuiObject* img,
                              bool use);

bool wGuiImageIsUsedAlphaChannel(wGuiObject* img);

///wGuiFader///
wGuiObject* wGuiFaderCreate(wVector2i minPos,
                            wVector2i maxPos);

void wGuiFaderSetColor(wGuiObject* fader,
                       wColor4s color);

wColor4s wGuiFaderGetColor(wGuiObject* fader);

void wGuiFaderSetColorExt(wGuiObject* fader,
                          wColor4s colorSrc,
                          wColor4s colorDest);

void wGuiFaderFadeIn(wGuiObject* fader,
                     UInt32 timeMs);

void wGuiFaderFadeOut(wGuiObject* fader,
                      UInt32 timeMs);

bool wGuiFaderIsReady(wGuiObject* fader);

///wGuiCheckBox///
wGuiObject* wGuiCheckBoxCreate(const wchar_t* wcptrText,
                               wVector2i minPos,
                               wVector2i maxPos,
                               bool checked);

void wGuiCheckBoxCheck(wGuiObject* box,
                       bool checked);

bool wGuiCheckBoxIsChecked(wGuiObject* box);

/// Sets whether to draw the background
void wGuiCheckBoxSetDrawBackground(wGuiObject* box,
                                   bool value);

/// Checks if background drawing is enabled
///return true if background drawing is enabled, false otherwise
bool wGuiCheckBoxIsDrawBackground(wGuiObject* box);

/// Sets whether to draw the border
void wGuiCheckBoxSetDrawBorder(wGuiObject* box,
                               bool value);

/// Checks if border drawing is enabled
///return true if border drawing is enabled, false otherwise
bool wGuiCheckBoxIsDrawBorder(wGuiObject* box);

void wGuiCheckBoxSetFilled(wGuiObject* box,
                               bool value);

bool wGuiCheckBoxIsFilled(wGuiObject* box);

///wGuiFileOpenDialog
/*Warning:
    When the user selects a folder this does change the current working directory

This element can create the following events of type wGuiEventType:

        wGET_DIRECTORY_SELECTED
        wGET_FILE_SELECTED
        wGET_FILE_CHOOSE_DIALOG_CANCELLED
*/
wGuiObject* wGuiFileOpenDialogCreate(const wchar_t* wcptrLabel,
                                     bool modal);

///Returns the filename of the selected file. Returns NULL, if no file was selected.
const wchar_t* wGuiFileOpenDialogGetFile(wGuiObject* dialog);

///Returns the directory of the selected file. Returns NULL, if no directory was selected.
const char* wGuiFileOpenDialogGetDirectory(wGuiObject* dialog);

///wGuiComboBox///
wGuiObject* wGuiComboBoxCreate(wVector2i minPos,
                               wVector2i maxPos);

UInt32 wGuiComboBoxGetItemsCount(wGuiObject* combo);

const wchar_t* wGuiComboBoxGetItemByIndex(wGuiObject* combo,
                                          UInt32 idx);

UInt32 wGuiComboBoxGetItemDataByIndex(wGuiObject* combo,
                                            UInt32 idx);

Int32 wGuiComboBoxGetIndexByItemData(wGuiObject* combo,
                                   UInt32 data);

UInt32 wGuiComboBoxAddItem(wGuiObject* combo,
                                 const wchar_t* text,
                                 UInt32 data);

void wGuiComboBoxRemoveItem(wGuiObject* combo,
                            UInt32 idx);

void wGuiComboBoxRemoveAll(wGuiObject* combo);

///Returns id of selected item. returns -1 if no item is selected.
Int32 wGuiComboBoxGetSelected(wGuiObject* combo);

void wGuiComboBoxSetSelected(wGuiObject* combo,
                             UInt32 idx);

void wGuiComboBoxSetMaxSelectionRows(wGuiObject* combo,
                                     UInt32 max);

UInt32 wGuiComboBoxGetMaxSelectionRows(wGuiObject* combo);

void wGuiComboBoxSetTextAlignment(wGuiObject* obj,
                                            wGuiAlignment Horizontalvalue,
                                            wGuiAlignment Verticalvalue);

///wGuiContextMenu///
wGuiObject* wGuiContextMenuCreate(wVector2i minPos,
                                  wVector2i maxPos);

void wGuiContextMenuSetCloseHandling(wGuiObject* cmenu,
                                     wContextMenuClose onClose);

wContextMenuClose wGuiContextMenuGetCloseHandling(wGuiObject* cmenu);

UInt32 wGuiContextMenuGetItemsCount(wGuiObject* cmenu);

#ifdef __cplusplus
UInt32 wGuiContextMenuAddItem(wGuiObject* cmenu,
                                    const wchar_t* text,
                                    Int32 commandId=-1,
                                    bool enabled=true,
                                    bool hasSubMenu=false,
                                    bool checked=false,
                                    bool autoChecking=false);
#else
UInt32 wGuiContextMenuAddItem(wGuiObject* cmenu,
                                    const wchar_t* text,
                                    Int32 commandId,
                                    bool enabled,
                                    bool hasSubMenu,
                                    bool checked,
                                    bool autoChecking);
#endif // __cplusplus

#ifdef __cplusplus
UInt32 wGuiContextMenuInsertItem(wGuiObject* cmenu,
                                       UInt32 idx,
                                       const wchar_t* text,
                                       Int32 commandId=-1,
                                       bool enabled=true,
                                       bool hasSubMenu=false,
                                       bool checked=false,
                                       bool autoChecking=false);
#else
UInt32 wGuiContextMenuInsertItem(wGuiObject* cmenu,
                                       UInt32 idx,
                                       const wchar_t* text,
                                       Int32 commandId,
                                       bool enabled,
                                       bool hasSubMenu,
                                       bool checked,
                                       bool autoChecking);
#endif // __cplusplus

void wGuiContextMenuAddSeparator(wGuiObject* cmenu);

const wchar_t* wGuiContextMenuGetItemText(wGuiObject* cmenu,
                                          UInt32 idx);

void wGuiContextMenuSetItemText(wGuiObject* cmenu,
                                UInt32 idx,
                                const wchar_t* text);

void wGuiContextMenuSetItemEnabled(wGuiObject* cmenu,
                                   UInt32 idx,
                                   bool value);

bool wGuiContextMenuIsItemEnabled(wGuiObject* cmenu,
                                  UInt32 idx);

void wGuiContextMenuSetItemChecked(wGuiObject* cmenu,
                                   UInt32 idx,
                                   bool value);

bool wGuiContextMenuIsItemChecked(wGuiObject* cmenu,
                                  UInt32 idx);

void wGuiContextMenuRemoveItem(wGuiObject* cmenu,
                               UInt32 idx);

void wGuiContextMenuRemoveAll(wGuiObject* cmenu);

Int32 wGuiContextMenuGetSelectedItem(wGuiObject* cmenu);

Int32 wGuiContextMenuGetItemCommandId(wGuiObject* cmenu,
                                    UInt32 idx);

#ifdef __cplusplus
Int32 wGuiContextMenuFindItem(wGuiObject* cmenu,
                            Int32 id,
                            UInt32 idx=0);
#else
Int32 wGuiContextMenuFindItem(wGuiObject* cmenu,
                            Int32 id,
                            UInt32 idx);
#endif // __cplusplus

void wGuiContextMenuSetItemCommandId(wGuiObject* cmenu,
                                     UInt32 idx,
                                     Int32 id);

wGuiObject* wGuiContextMenuGetSubMenu(wGuiObject* cmenu,
                                      UInt32 idx);

void wGuiContextMenuSetAutoChecking(wGuiObject* cmenu,
                                    UInt32 idx,
                                    bool autoChecking);

bool wGuiContextMenuIsAutoChecked(wGuiObject* cmenu,
                                  UInt32 idx);

///When an eventparent is set it receives events instead of the usual parent element.
void wGuiContextMenuSetEventParent(wGuiObject* cmenu,
                                   wGuiObject* parent);

///wGuiMenu///
///Adds a menu to the environment.This is like the menu you can find on top of most windows in modern graphical user interfaces.
///Для работы с меню подходят все команды
///из раздела wGuiContextMenu///
wGuiObject* wGuiMenuCreate();

///wGuiModalScreen///
///Adds a modal screen.
///This control stops its parent's members from being able to receive input until its last child is removed,
/// it then deletes itself.
wGuiObject* wGuiModalScreenCreate();

///wGuiSpinBox///
#ifdef __cplusplus
wGuiObject* wGuiSpinBoxCreate(const wchar_t * wcptrText,
                              wVector2i minPos,
                              wVector2i maxPos,
                              bool border=true);
#else
wGuiObject* wGuiSpinBoxCreate(const wchar_t * wcptrText,
                              wVector2i minPos,
                              wVector2i maxPos,
                              bool border);
#endif // __cplusplus

wGuiObject* wGuiSpinBoxGetEditBox(wGuiObject* box);

void wGuiSpinBoxSetValue(wGuiObject* spin,
                         Float32 value);

Float32 wGuiSpinBoxGetValue(wGuiObject* spin);

void wGuiSpinBoxSetRange(wGuiObject* spin,
                         wVector2f range);

Float32 wGuiSpinBoxGetMin(wGuiObject* spin);

Float32 wGuiSpinBoxGetMax(wGuiObject* spin);

void wGuiSpinBoxSetStepSize(wGuiObject* spin,
                            Float32 step);

Float32 wGuiSpinBoxGetStepSize(wGuiObject* spin);

void wGuiSpinBoxSetDecimalPlaces(wGuiObject* spin,
                                 Int32 places);

///wGuiTab///
wGuiObject* wGuiTabCreate(wVector2i minPos,
                          wVector2i maxPos );

Int32 wGuiTabGetNumber(wGuiObject* tab);

void wGuiTabSetTextColor(wGuiObject* tab,
                         wColor4s color);

wColor4s wGuiTabGetTextColor(wGuiObject* tab);

void wGuiTabSetDrawBackground(wGuiObject* tab,
                              bool value);

bool wGuiTabIsDrawBackground(wGuiObject* obj);

void wGuiTabSetBackgroundColor(wGuiObject* tab,
                               wColor4s color);

wColor4s wGuiTabGetBackgroundColor(wGuiObject* tab);

///wGuiTabControl///
#ifdef __cplusplus
wGuiObject* wGuiTabControlCreate(wVector2i minPos,
                                 wVector2i maxPos,
                                 bool background=false,
                                 bool border=true);
#else
wGuiObject* wGuiTabControlCreate(wVector2i minPos,
                                 wVector2i maxPos,
                                 bool background,
                                 bool border);
#endif // __cplusplus

Int32 wGuiTabControlGetTabsCount(wGuiObject* control);

#ifdef __cplusplus
wGuiObject* wGuiTabControlAddTab(wGuiObject* control,
                                 const wchar_t* caption,
                                 Int32 id=-1);
#else
wGuiObject* wGuiTabControlAddTab(wGuiObject* control,
                                 const wchar_t* caption,
                                 Int32 id);
#endif // __cplusplus

#ifdef __cplusplus
wGuiObject* wGuiTabControlInsertTab(wGuiObject* control,
                                    UInt32 idx,
                                    const wchar_t* caption,
                                    Int32 id=-1);
#else
wGuiObject* wGuiTabControlInsertTab(wGuiObject* control,
                                    UInt32 idx,
                                    const wchar_t* caption,
                                    Int32 id);
#endif // __cplusplus

wGuiObject* wGuiTabControlGetTab(wGuiObject* control,
                                 Int32 idx);

bool wGuiTabControlSetActiveTabByIndex(wGuiObject* control,
                                       Int32 idx);

bool wGuiTabControlSetActiveTab(wGuiObject* control,
                                wGuiObject* tab);

Int32 wGuiTabControlGetActiveTab(wGuiObject* control);

Int32 wGuiTabControlGetTabFromPos(wGuiObject* control,
                                wVector2i position);

void wGuiTabControlRemoveTab(wGuiObject* control,
                             Int32 idx);

void wGuiTabControlRemoveAll(wGuiObject* control);

void wGuiTabControlSetTabHeight(wGuiObject* control,
                                Int32 height);

Int32 wGuiTabControlGetTabHeight(wGuiObject* control);

void wGuiTabControlSetTabMaxWidth(wGuiObject* control,
                                  Int32 width);

Int32 wGuiTabControlGetTabMaxWidth(wGuiObject* control);

void wGuiTabControlSetVerticalAlignment(wGuiObject* control,
                                        wGuiAlignment al);

wGuiAlignment wGuiTabControlGetVerticalAlignment(wGuiObject* control);

void wGuiTabControlSetTabExtraWidth(wGuiObject* control,
                                    Int32 extraWidth);

Int32 wGuiTabControlGetTabExtraWidth(wGuiObject* control);

///wGuiTable///
#ifdef __cplusplus
wGuiObject* wGuiTableCreate(wVector2i minPos,
                            wVector2i maxPos,
                            bool background=false);
#else
wGuiObject* wGuiTableCreate(wVector2i minPos,
                            wVector2i maxPos,
                            bool background);
#endif // __cplusplus

#ifdef __cplusplus
void wGuiTableAddColumn(wGuiObject* table,
                        wchar_t* caption,
                        Int32 columnIndex=-1);
#else
void wGuiTableAddColumn(wGuiObject* table,
                        wchar_t* caption,
                        Int32 columnIndex);
#endif // __cplusplus

void wGuiTableRemoveColumn(wGuiObject* table,
                           UInt32 columnIndex);

Int32 wGuiTableGetColumnsCount(wGuiObject* table);

#ifdef __cplusplus
bool wGuiTableSetActiveColumn(wGuiObject* table,
                              Int32 idx,
                              bool doOrder=false);
#else
bool wGuiTableSetActiveColumn(wGuiObject* table,
                              Int32 idx,
                              bool doOrder);
#endif // __cplusplus

Int32 wGuiTableGetActiveColumn(wGuiObject* table);

wGuiOrderingMode wGuiTableGetActiveColumnOrdering(wGuiObject* table);

void wGuiTableSetColumnWidth(wGuiObject* table,
                             UInt32 columnIndex,
                             UInt32 width);

void wGuiTableSetColumnsResizable(wGuiObject* table,
                                  bool resizible);

bool wGuiTableIsColumnsResizable(wGuiObject* table);

Int32 wGuiTableGetSelected(wGuiObject* table);

void wGuiTableSetSelectedByIndex(wGuiObject* table,
                                 Int32 index);

Int32 wGuiTableGetRowsCount(wGuiObject* table);

UInt32 wGuiTableAddRow(wGuiObject* table,
                             UInt32 rowIndex);

void wGuiTableRemoveRow(wGuiObject* table,
                        UInt32 rowIndex);

void wGuiTableClearRows(wGuiObject* table);

void wGuiTableSwapRows(wGuiObject* table,
                       UInt32 rowIndexA,
                       UInt32 rowIndexB);

void wGuiTableSetOrderRows(wGuiObject* table,
                           Int32 columnIndex,
                           wGuiOrderingMode mode);

void wGuiTableSetCellText(wGuiObject* table,
                          UInt32 rowIndex,
                          UInt32 columnIndex,
                          const wchar_t* text,
                          wColor4s color);

void wGuiTableSetCellData(wGuiObject* table,
                          UInt32 rowIndex,
                          UInt32 columnIndex,
                          UInt32* data);

void wGuiTableSetCellColor(wGuiObject* table,
                           UInt32 rowIndex,
                           UInt32 columnIndex,
                           wColor4s color);

const wchar_t* wGuiTableGetCellText(wGuiObject* table,
                                    UInt32 rowIndex,
                                    UInt32 columnIndex );

UInt32* wGuiTableGetCellData(wGuiObject* table,
                                   UInt32 rowIndex,
                                   UInt32 columnIndex );

void wGuiTableSetDrawFlags(wGuiObject* table,
                           wGuiTableDrawFlags flags);

wGuiTableDrawFlags wGuiTableGetDrawFlags(wGuiObject* table);

void wGuiTableSetOverrideFont(wGuiObject* obj,
                                         wFont* font);

wFont* wGuiTableGetOverrideFont(wGuiObject* obj);

wFont* wGuiTableGetActiveFont(wGuiObject* obj);

Int32 wGuiTableGetItemHeight(wGuiObject* obj);

wGuiObject* wGuiTableGetVerticalScrollBar(wGuiObject* obj);

wGuiObject* wGuiTableGetHorizontalScrollBar(wGuiObject* obj);

void wGuiTableSetDrawBackground(wGuiObject* obj, bool value);

bool wGuiTableIsDrawBackground(wGuiObject* obj);

///wGuiToolBar///
wGuiObject* wGuiToolBarCreate();

#ifdef __cplusplus
wGuiObject* wGuiToolBarAddButton(wGuiObject* bar,
                                 const wchar_t* text,
                                 const wchar_t* tooltiptext,
                                 wTexture* img=0,
                                 wTexture* pressedImg=0,
                                 bool isPushButton=false,
                                 bool useAlphaChannel=true);
#else
wGuiObject* wGuiToolBarAddButton(wGuiObject* bar,
                                 const wchar_t* text,
                                 const wchar_t* tooltiptext,
                                 wTexture* img,
                                 wTexture* pressedImg,
                                 bool isPushButton,
                                 bool useAlphaChannel);
#endif // __cplusplus


///wGuiMessageBox
#ifdef __cplusplus
wGuiObject* wGuiMessageBoxCreate(const wchar_t* wcptrTitle,
                                 const wchar_t* wcptrTCaption=0,
                                 bool modal=true,
                                 wGuiMessageBoxFlags flags=wGMBF_OK,
                                 wTexture* image=0);
#else
wGuiObject* wGuiMessageBoxCreate(const wchar_t * wcptrTitle,
                                 const wchar_t* wcptrTCaption,
                                 bool modal,
                                 wGuiMessageBoxFlags flags,
                                 wTexture* image);
#endif // __cplusplus

///wGuiTree///
///Create a tree view element.
wGuiObject* wGuiTreeCreate(wVector2i minPos,
                           wVector2i maxPos,
                           bool background,
                           bool barvertical,
                           bool barhorizontal);

///returns the root node (not visible) from the tree.
wGuiObject* wGuiTreeGetRoot(wGuiObject* tree);

///returns the selected node of the tree or 0 if none is selected
wGuiObject* wGuiTreeGetSelected(wGuiObject* tree);

///sets if the tree lines are visible
void wGuiTreeSetLinesVisible(wGuiObject* tree,
                             bool visible);

///returns true if the tree lines are visible
bool wGuiTreeIsLinesVisible(wGuiObject* tree);

///Sets the font which should be used as icon font.
void wGuiTreeSetIconFont(wGuiObject* tree,
                         wFont* font);

///Sets the image list which should be used for the image and selected image of every node.
void wGuiTreeSetImageList(wGuiObject* tree,
                         wGuiObject* list);

///Returns the image list which is used for the nodes.
wGuiObject* wGuiTreeGetImageList(wGuiObject* tree);

///Sets if the image is left of the icon. Default is true.
void wGuiTreeSetImageLeftOfIcon(wGuiObject* tree,
                                bool bLeftOf);

///Returns if the Image is left of the icon. Default is true.
bool wGuiTreeIsImageLeftOfIcon(wGuiObject* tree);

///Returns the node which is associated to the last event.
wGuiObject* wGuiTreeGetLastEventNode(wGuiObject* tree);

///wGuiTreeNode///
///returns the owner (Gui tree) of this node
wGuiObject* wGuiTreeNodeGetOwner(wGuiObject* node);

wGuiObject* wGuiTreeNodeGetParent(wGuiObject* node);

///returns the text of the node
const wchar_t* wGuiTreeNodeGetText(wGuiObject* node);

///sets the text of the node
void wGuiTreeNodeSetText(wGuiObject* node,
                         const wchar_t* text);

///sets the icon text of the node
void wGuiTreeNodeSetIcon(wGuiObject* node,
                         const wchar_t* icon);

///returns the icon text of the node
const wchar_t* wGuiTreeNodeGetIcon(wGuiObject* node);

///sets the image index of the node
void wGuiTreeNodeSetImageIndex(wGuiObject* node,
                               UInt32 imageIndex);

///returns the image index of the node
UInt32 wGuiTreeNodeGetImageIndex(wGuiObject* node);

///sets the image index of the node
void wGuiTreeNodeSetSelectedImageIndex(wGuiObject* node,
                                       UInt32 imageIndex);

///returns the image index of the node
UInt32 wGuiTreeNodeGetSelectedImageIndex(wGuiObject* node);

///sets the user data (UInt32*) of this node
void  wGuiTreeNodeSetData(wGuiObject* node,
                          UInt32* data);

///returns the user data (UInt32*) of this node
UInt32* wGuiTreeNodeGetData(wGuiObject* node);

///sets the user data2 of this node
void wGuiTreeNodeSetData2(wGuiObject* node,
                          UInt32* data2);

///returns the user data2 of this node
UInt32* wGuiTreeNodeGetData2(wGuiObject* node);

///returns the child item count
UInt32 wGuiTreeNodeGetChildsCount(wGuiObject* node);

///Remove a child node.
void wGuiTreeNodeRemoveChild(wGuiObject* node,
                             wGuiObject* child);

///removes all children (recursive) from this node
void wGuiTreeNodeRemoveChildren(wGuiObject* node);

///returns true if this node has child nodes
bool wGuiTreeNodeHasChildren(wGuiObject* node);

///Adds a new node behind the last child node.
#ifdef __cplusplus
wGuiObject* wGuiTreeNodeAddChildBack(wGuiObject* node,
                                     const wchar_t* text,
                                     const wchar_t* icon=0,
                                     Int32 imageIndex=-1,
                                     Int32 selectedImageIndex=-1,
                                     void* data=0,
                                     UInt32* data2=0);
#else
wGuiObject* wGuiTreeNodeAddChildBack(wGuiObject* node,
                                     const wchar_t* text,
                                     const wchar_t* icon,
                                     Int32 imageIndex,
                                     Int32 selectedImageIndex,
                                     void* data,
                                     UInt32* data2);
#endif // __cplusplus


///Adds a new node before the first child node.
#ifdef __cplusplus
wGuiObject* wGuiTreeNodeAddChildFront(wGuiObject* node,
                                      const wchar_t* text,
                                      const wchar_t* icon=0,
                                      Int32 imageIndex=-1,
                                      Int32 selectedImageIndex=-1,
                                      void* data=0,
                                      UInt32* data2=0);
#else
wGuiObject* wGuiTreeNodeAddChildFront(wGuiObject* node,
                                      const wchar_t* text,
                                      const wchar_t* icon,
                                      Int32 imageIndex,
                                      Int32 selectedImageIndex,
                                      void* data,
                                      UInt32* data2);
#endif // __cplusplus

///Adds a new node behind the other node.
#ifdef __cplusplus
wGuiObject* wGuiTreeNodeInsertChildAfter(wGuiObject* node,
                                         wGuiObject* other,
                                         const wchar_t* text,
                                         const wchar_t* icon=0,
                                         Int32 imageIndex=-1,
                                         Int32 selectedImageIndex=-1,
                                         void* data=0,
                                         UInt32* data2=0);
#else
wGuiObject* wGuiTreeNodeInsertChildAfter(wGuiObject* node,
                                         wGuiObject* other,
                                         const wchar_t* text,
                                         const wchar_t* icon,
                                         Int32 imageIndex,
                                         Int32 selectedImageIndex,
                                         void* data,
                                         UInt32* data2);
#endif // __cplusplus

///Adds a new node before the other node.
#ifdef __cplusplus
wGuiObject* wGuiTreeNodeInsertChildBefore(wGuiObject* node,
                                          wGuiObject* other,
                                          const wchar_t* text,
                                          const wchar_t* icon=0,
                                          Int32 imageIndex=-1,
                                          Int32 selectedImageIndex=-1,
                                          void* data=0,
                                          UInt32* data2=0);
#else
wGuiObject* wGuiTreeNodeInsertChildBefore(wGuiObject* node,
                                          wGuiObject* other,
                                          const wchar_t* text,
                                          const wchar_t* icon,
                                          Int32 imageIndex,
                                          Int32 selectedImageIndex,
                                          void* data,
                                          UInt32* data2);
#endif // __cplusplus

///Return the first child node from this node.
wGuiObject* wGuiTreeNodeGetFirstChild(wGuiObject* node);

///Return the last child node from this node.
wGuiObject* wGuiTreeNodeGetLastChild(wGuiObject* node);

///Returns the previous sibling node from this node.
wGuiObject* wGuiTreeNodeGetPrevSibling(wGuiObject* node);

///Returns the next sibling node from this node.
wGuiObject* wGuiTreeNodeGetNextSibling(wGuiObject* node);

///Returns the next visible (expanded, may be out of scrolling) node from this node.
wGuiObject* wGuiTreeNodeGetNextVisible(wGuiObject* node);

///Moves a child node one position up.
bool wGuiTreeNodeMoveChildUp(wGuiObject* node,
                             wGuiObject* child);

///Moves a child node one position down.
bool wGuiTreeNodeMoveChildDown(wGuiObject* node,
                               wGuiObject* child);

///Sets if the node is expanded.
void wGuiTreeNodeSetExpanded(wGuiObject* node,
                             bool expanded);

///Returns true if the node is expanded (children are visible).
bool wGuiTreeNodeIsExpanded(wGuiObject* node);

///Sets this node as selected
void wGuiTreeNodeSetSelected(wGuiObject* node,
                             bool selected);

///Returns true if the node is currently selected.
bool  wGuiTreeNodeIsSelected(wGuiObject* node);

///Returns true if this node is the root node.
bool wGuiTreeNodeIsRoot(wGuiObject* node);

///Returns the level of this node.
///The root node has level 0.
///Direct children of the root has level 1 ...
Int32 wGuiTreeNodeGetLevel(wGuiObject* node);

///Returns true if this node is visible (all parents are expanded)
bool wGuiTreeNodeIsVisible(wGuiObject* node);

///wGuiImageList///
wGuiObject* wGuiImageListCreate(wTexture* texture,
                                wVector2i size,
                                bool useAlphaChannel);

void wGuiImageListDraw(wGuiObject* list,
                       Int32 index,
                       wVector2i pos,
                       wVector2i clipPos,
                       wVector2i clipSize);

Int32 wGuiImageListGetCount(wGuiObject* list);

wVector2i wGuiImageListGetSize(wGuiObject* list);

///wGuiColorSelectDialog///
wGuiObject* wGuiColorSelectDialogCreate(const wchar_t* title,
                                        bool modal);

///wGuiMeshViewer///
wGuiObject* wGuiMeshViewerCreate(wVector2i minPos,
                                 wVector2i maxPos,
                                 const wchar_t* text);

void wGuiMeshViewerSetMesh(wGuiObject* viewer,
                           wMesh* mesh);

wMesh* wGuiMeshViewerGetMesh(wGuiObject* viewer);

void wGuiMeshViewerSetMaterial(wGuiObject* viewer,
                               wMaterial* material);

wMaterial* wGuiMeshViewerGetMaterial(wGuiObject* viewer);

///wGuiSpriteBank///
///Returns pointer to the sprite bank with the specified file name.
///Loads the bank if it was not loaded before.
wGuiObject* wGuiSpriteBankLoad(const char* file);

wGuiObject* wGuiSpriteBankCreate(const char* name);

///Adds a texture to the sprite bank.
void wGuiSpriteBankAddTexture(wGuiObject* bank,
                              wTexture* texture);

///Changes one of the textures in the sprite bank
void wGuiSpriteBankSetTexture(wGuiObject* bank,
                              UInt32 index,
                              wTexture* texture);

///Add the texture and use it for a single non-animated sprite.
///The texture and the corresponding rectangle and sprite will all be added
/// to the end of each array. returns the index of the sprite or -1 on failure
Int32 wGuiSpriteBankAddSprite(wGuiObject* bank,
                            wTexture* texture);

wTexture* wGuiSpriteBankGetTexture(wGuiObject* bank,
                                   UInt32 index);

UInt32 wGuiSpriteBankGetTexturesCount(wGuiObject* bank);

void wGuiSpriteBankRemoveAll(wGuiObject* bank);

#ifdef __cplusplus
void wGuiSpriteBankDrawSprite(wGuiObject* bank,
                              UInt32 index,
                              wVector2i position,
                              wVector2i* clipPosition=0,
                              wVector2i* clipSize=0,
                              wColor4s color=wCOLOR4s_WHITE,
                              UInt32 starttime=0,
                              UInt32 currenttime=0,
                              bool loop=true,
                              bool center=false);
#else
void wGuiSpriteBankDrawSprite(wGuiObject* bank,
                              UInt32 index,
                              wVector2i position,
                              wVector2i* clipPosition,
                              wVector2i* clipSize,
                              wColor4s color,
                              UInt32 starttime,
                              UInt32 currenttime,
                              bool loop,
                              bool center);
#endif // __cplusplus

#ifdef __cplusplus
void wGuiSpriteBankDrawSpriteBatch(wGuiObject* bank,
                                   UInt32* indexArray,
                                   UInt32 idxArrayCount,
                                   wVector2i* positionArray,
                                   UInt32 posArrayCount,
                                   wVector2i* clipPosition=0,
                                   wVector2i* clipSize=0,
                                   wColor4s color=wCOLOR4s_WHITE,
                                   UInt32 starttime=0,
                                   UInt32 currenttime=0,
                                   bool loop=true,
                                   bool center=false);
#else
void wGuiSpriteBankDrawSpriteBatch(wGuiObject* bank,
                                   UInt32* indexArray,
                                   UInt32 idxArrayCount,
                                   wVector2i* positionArray,
                                   UInt32 posArrayCount,
                                   wVector2i* clipPosition,
                                   wVector2i* clipSize,
                                   wColor4s color,
                                   UInt32 startTime,
                                   UInt32 currentTime,
                                   bool loop,
                                   bool center);
#endif // __cplusplus

 ///wGuiCheckGroup
wGuiObject* wGuiCheckBoxGroupCreate(wVector2i minPos,
                                    wVector2i maxPos);

Int32 wGuiCheckBoxGroupAddCheckBox(wGuiObject* group,
                                 wGuiObject* check);

Int32 wGuiCheckBoxGroupInsertCheckBox(wGuiObject* group,
                                    wGuiObject* check,
                                    UInt32 index);

wGuiObject* wGuiCheckBoxGroupGetCheckBox(wGuiObject* group,
                                         UInt32 index);

Int32 wGuiCheckBoxGroupGetIndex(wGuiObject* group,
                              wGuiObject* check);

Int32 wGuiCheckBoxGroupGetSelectedIndex(wGuiObject* group);

bool wGuiCheckBoxGroupRemoveCheckBox(wGuiObject* group,
                                     UInt32 index);

void wGuiCheckBoxGroupRemoveAll(wGuiObject* group);

UInt32 wGuiCheckBoxGroupGetSize(wGuiObject* group);

void wGuiCheckBoxGroupSelectCheckBox(wGuiObject* group,
                                     Int32 index);

void wGuiCheckBoxGroupClearSelection(wGuiObject* group);

void wGuiCheckBoxGroupSetBackgroundColor(wGuiObject* obj,
                                         wColor4s color);

void wGuiCheckBoxGroupDrawBackground(wGuiObject* obj,
                                    bool value);

///wGuiProgressBar///
#ifdef __cplusplus
wGuiObject* wGuiProgressBarCreate(wVector2i minPos,
                                  wVector2i maxPos,
                                  bool isHorizontal=true);
#else
wGuiObject* wGuiProgressBarCreate(wVector2i minPos,
                                  wVector2i maxPos,
                                  bool isHorizontal);
#endif // __cplusplus


void wGuiProgressBarSetPercentage(wGuiObject* bar,
                                  UInt32 percent);

UInt32 wGuiProgressBarGetPercentage(wGuiObject* bar);

void wGuiProgressBarSetDirection(wGuiObject* bar,
                                 bool isHorizontal);

bool wGuiProgressBarIsHorizontal(wGuiObject* bar);

void wGuiProgressBarSetBorderSize(wGuiObject* bar,
                                  UInt32 size);

void wGuiProgressBarSetSize(wGuiObject* bar,
                            wVector2u size);

void wGuiProgressBarSetFillColor(wGuiObject* bar,
                              wColor4s color);

wColor4s wGuiProgressBarGetFillColor(wGuiObject* bar);

void wGuiProgressBarSetTextColor(wGuiObject* bar,
                                 wColor4s color);

void wGuiProgressBarShowText(wGuiObject* bar,
                             bool value);

bool wGuiProgressBarIsShowText(wGuiObject* bar);

void wGuiProgressBarSetFillTexture(wGuiObject* bar,
                                   wTexture* tex);

void wGuiProgressBarSetBackTexture(wGuiObject* bar,
                                   wTexture* tex);

void wGuiProgressBarSetFont(wGuiObject* bar,
                            wFont* font);

void wGuiProgressBarSetBackgroundColor(wGuiObject* bar,
                                       wColor4s color);

void wGuiProgressBarSetBorderColor(wGuiObject* obj,
                                   wColor4s color);

///wGuiTextArea///
#ifdef __cplusplus
wGuiObject* wGuiTextAreaCreate(wVector2i minPos,
                               wVector2i maxPos,
                               Int32 maxLines=1024);
#else
wGuiObject* wGuiTextAreaCreate(wVector2i minPos,
                               wVector2i maxPos,
                               Int32 maxLines);
#endif // __cplusplus

void wGuiTextAreaSetBorderSize(wGuiObject* tarea,
                               UInt32 size);

void wGuiTextAreaSetAutoScroll(wGuiObject* tarea,
                               bool value);

void wGuiTextAreaSetPadding(wGuiObject* tarea,
                            UInt32 padding);

void wGuiTextAreaSetBackTexture(wGuiObject* tarea,
                                wTexture* tex);

void wGuiTextAreaSetWrapping(wGuiObject* tarea,
                             bool value);

void wGuiTextAreaSetFont(wGuiObject* tarea,
                         wFont* font);

#ifdef __cplusplus
void wGuiTextAreaAddLine(wGuiObject* tarea,
                         const wchar_t* text,
                         UInt32 lifeTime,
                         wColor4s color=wCOLOR4s_BLACK,
                         wTexture* icon=0,
                         Int32 iconMode=0); //iconMode=0/1
#else
void wGuiTextAreaAddLine(wGuiObject* tarea,
                         const wchar_t* text,
                         UInt32 lifeTime,
                         wColor4s color,
                         wTexture* icon,
                         Int32 iconMode);
#endif // __cplusplus

void wGuiTextAreaRemoveAll(wGuiObject* tarea);

void wGuiTextAreaSetBackgroundColor(wGuiObject* tarea,
                                    wColor4s color);

void wGuiTextAreaSetBorderColor(wGuiObject* tarea,
                                           wColor4s color);

///wGuiCEditor///
wGuiObject* wGuiCEditorCreate(const wchar_t* wcptrText,
						 wVector2i minPos,
						 wVector2i maxPos,
						 bool border);

void wGuiCEditorSetHScrollVisible(wGuiObject* box,
                                  bool value);

void wGuiCEditorSetText(wGuiObject* box,
                        const wchar_t* text);

void wGuiCEditorSetColors(wGuiObject* box,
                          wColor4s backColor,
                          wColor4s lineColor,
                          wColor4s textColor);

void wGuiCEditorSetLinesCountVisible(wGuiObject* box,
                                     bool value);

bool wGuiCEditorIsLinesCountVisible(wGuiObject* box);

void wGuiCEditorSetElementText(wGuiObject* box,
                               UInt32 index,
                               const wchar_t* text);

void wGuiCEditorSetSelectionColors(wGuiObject* box,
                                   wColor4s backColor,
                                   wColor4s textColor,
                                   wColor4s back2Color);

void wGuiCEditorRemoveText(wGuiObject* box);

void wGuiCEditorAddKeyword(wGuiObject* box,
                           const char* word,
                           wColor4s color,
                           bool matchCase);

void wGuiCEditorAddLineKeyword(wGuiObject* box,
                               const char* word,
                               wColor4s color,
                               bool matchCase);

void wGuiCEditorAddGroupKeyword(wGuiObject* box,
                                const char* word,
                                const char* endKeyword,
                                wColor4s color,
                                bool matchCase);

void wGuiCEditorBoxAddKeywordInfo(wGuiObject* box,
                                  Int32 size,
                                  Int32 type);

void wGuiCEditorBoxRemoveAllKeywords(wGuiObject* box);

void wGuiCEditorBoxAddCppKeywords(wGuiObject* box,
                                  wColor4s key,
                                  wColor4s string,
                                  wColor4s comment);

void wGuiCEditorAddLuaKeywords(wGuiObject* box,
                               wColor4s key,
                               wColor4s string,
                               wColor4s comment);

void wGuiCEditorAddFbKeywords(wGuiObject* box,
                              wColor4s key,
                              wColor4s string,
                              wColor4s comment);

void wGuiCEditorReplaceText(wGuiObject* box,
                            Int32 start,
                            Int32 end,
                            const wchar_t* text);

void wGuiCEditorPressReturn(wGuiObject* box);

void wGuiCEditorAddText(wGuiObject* box,
                        const wchar_t* addText);

const wchar_t* wGuiCEditorGetText(wGuiObject* box);

void wGuiCEditorSetLineToggleVisible(wGuiObject* box,
                                     bool value);

void wGuiCEditorSetContextMenuText(wGuiObject* box,
                                   const wchar_t* cut_text,
                                   const wchar_t* copy_text,
                                   const wchar_t* paste_text,
                                   const wchar_t* del_text,
                                   const wchar_t* redo_text,
                                   const wchar_t* undo_text,
                                   const wchar_t* btn_text);

void wGuiCEditorBoxCopy(wGuiObject* box);

void wGuiCEditorCut(wGuiObject* box);

void wGuiCEditorPaste(wGuiObject* box);

void wGuiCEditorUndo(wGuiObject* box);

void wGuiCEditorRedo(wGuiObject* box);

wFont* wGuiCEditorGetOverrideFont(wGuiObject* obj);

wFont* wGuiCEditorGetActiveFont(wGuiObject* obj);

void wGuiCEditorEnableOverrideColor(wGuiObject* obj,
                                    bool value);

bool wGuiCEditorIsOverrideColor(wGuiObject* obj);

void wGuiCEditorSetOverrideColor(wGuiObject* obj,
                                 wColor4s color);

wColor4s wGuiCEditorGetOverrideColor(wGuiObject* obj);

void wGuiCEditorSetDrawBackground(wGuiObject* obj,
                                  bool value);

bool wGuiCEditorIsDrawBackGround(wGuiObject* obj);

void wGuiCEditorSetDrawBorder(wGuiObject* obj,
                              bool value);

bool wGuiCEditorIsDrawBorder(wGuiObject* obj);

void wGuiCEditorSetTextAlignment(wGuiObject* obj,
                                wGuiAlignment Horizontalvalue,
                                wGuiAlignment Verticalvalue);

 void wGuiCEditorSetWordWrap(wGuiObject* obj,
                             bool value);

bool wGuiCEditorIsWordWrap(wGuiObject* obj);

void wGuiCEditorSetBackgroundColor(wGuiObject* obj,
                                   wColor4s color);
								   
#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __WORLDSIM3D_LINK_H_INCLUDED
