#ifndef OBJECT_3D_H
#define OBJECT_3D_H


#include "constants.h"
#include "global.h"
#include "base_object.h"
#include "res_in_mem.h"

#include "WorldSim3D.h"


class TObject3D : public TObject
{
public:
    static vector<TObject3D*> objects;

    wVector3f  pivotOffset = {};       /* смещение опорной точки */
    wNode*     node      = nullptr;
    wNode*     solidBox  = nullptr;
    wNode*     billboard = nullptr;
    wSelector* physBody  = nullptr;

protected:
    bool isCollisionAdded = false;      /* добавлена ли коллизия к коллайдеру мира */

public:
    TObject3D(ScrResourceInMem* index);
    virtual ~TObject3D();

    virtual void init(string className = "", string objectName = "", bool numerate = true)
    {
        TObject::init(className, objectName, numerate);
    }
    virtual void update() { TObject::update(); }

    /* установить видимость объекта */
    virtual void setVisibility(bool isVisible);

    virtual void setFrame(int frame);

    /* управление коллизионной коробкой объекта */
    virtual void enableCollision();
    virtual void disableCollision();
    virtual inline bool isEnabledCollision();

    /* manipulations with object */
    /* position */
    virtual void  setPosition(wVector3f& pos);
    virtual void  setPosition(wVector2f& pos);
    virtual void  setPositionX(float& x);
    virtual void  setPositionY(float& y);
    virtual void  setPositionZ(float& z);
    //wVector3f getPosition();
    virtual void  getPosition(vector<double> &pos);
    virtual float getPositionX();
    virtual float getPositionY();
    virtual float getPositionZ();
    virtual void  move(wVector3f& moveVector);
    /* rotation */
    virtual void  setRotation(wVector3f& rot);
//    void  setRotation(float& rot);
    virtual void  setRotationX(float& x);
    virtual void  setRotationY(float& y);
    virtual void  setRotationZ(float& z);
//    wVector3f getRotation();
    virtual void  getRotation(vector<double> &rot);
    virtual float getRotationX();
    virtual float getRotationY();
    virtual float getRotationZ();
    virtual void  rotate(wVector3f& rot);
    /* size */
    //wVector3f getSize();
    virtual void  getSize(vector<double> &size);
    virtual float getSizeX();
    virtual float getSizeY();
    virtual float getSizeZ();
    /* scale */
    virtual void  setScale(wVector3f& scale);
    virtual void  setScale(wVector2f& scale);
    virtual void  setScaleX(float& x);
    virtual void  setScaleY(float& y);
    virtual void  setScaleZ(float& z);
    //wVector3f getScale();
    virtual void  getScale(vector<double> &scale);
    virtual float getScaleX();
    virtual float getScaleY();
    virtual float getScaleZ();
};

#endif // OBJECT_3D_H
