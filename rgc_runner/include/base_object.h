#ifndef BASE_OBJECT_H
#define BASE_OBJECT_H

#include <map>
#include <vector>
#include <string>
#include <limits>

#include "js_util.h"
#include "res_in_mem.h"

#include "WorldSim3D.h"



using namespace std;


class TLaser
{
private:
    static vector<TLaser*> lasers;
    wVector3f start, end;
    double timeForUnvis;

public:
    TLaser(wVector3f start, wVector3f end)
    {
        this->start = start;
        this->end   = end;
        this->timeForUnvis = 0.0;

        if (lasers.capacity() == 0)
            lasers.reserve(256);

        lasers.push_back(this);
    }

    ~TLaser()
    {
        if (lasers.size() > 1)
        {
            for (vector<TLaser*>::iterator it = lasers.begin();
                 it != lasers.end();
                 ++it)
            {
                if (*it == this)
                {
                    *it = lasers.back();
                    break;
                }
            }
        }

        lasers.pop_back();
    }

    static void show()
    {
        for (auto laser : lasers)
        {
            laser->timeForUnvis += g_DeltaTime;
            w3dDrawLine(laser->start, laser->end, wCOLOR4s_DARKRED);

            /* самоуничтожиться */
            if (laser->timeForUnvis > 3.0)
                delete laser;
        }
    }
};


enum TUserVarType
{
    uvtNone = 0,
    uvtNumber,
    uvtBool,
    uvtString,
    uvtVector
};

class TUserVar
{
public:
    TUserVarType   type;
    double         dVal;
    bool           bVal;
    string         sVal;
    vector<double> vVal;

    TUserVar(TUserVarType type = uvtNone,
             double dVal = 0.0,
             bool bVal = false,
             string sVal = "",
             vector<double> vVal = {})
    {
        this->type = type;
        this->dVal = dVal;
        this->bVal = bVal;
        this->sVal = sVal;
        this->vVal = vVal;
    }

    void set(double val)
    {
        this->type = uvtNumber;
        this->dVal = val;
        this->bVal = false;
        this->sVal.clear();
        this->vVal.clear();
    }
    void set(bool val)
    {
        this->type = uvtBool;
        this->dVal = 0;
        this->bVal = val;
        this->sVal.clear();
        this->vVal.clear();
    }
    void set(char* val)
    {
        this->type = uvtString;
        this->dVal = 0;
        this->bVal = false;
        this->sVal = val;
        this->vVal.clear();
    }
    void set(string val)
    {
        set(val.c_str());
    }
    void set(vector<double> val)
    {
        this->type = uvtVector;
        this->dVal = 0;
        this->bVal = false;
        this->sVal.clear();
        this->vVal = val;
    }

    ~TUserVar()
    {
        this->vVal.clear();
    }
};


/* базовый класс */
class TBaseObject
{
public:
    string name = "";               /* имя                    */
    int    id = 0;                  /* идентификатор объекта  */
    TBaseObject()  { this->id = (int)this; }
    virtual ~TBaseObject() {}
};

/*  */
class TObject : public TBaseObject
{
public:
    static vector<TObject*> objects;     /* массив всех объектов в игре */

    ScrResourceInMem* index = nullptr;

    TScriptJS* script         = nullptr;   // полный скрипт
    TScriptJS* initCommand    = nullptr;   // команда object.onInit
    TScriptJS* updateCommand  = nullptr;   // команда object.onUpdate
    TScriptJS* destroyCommand = nullptr;   // команда object.onDestroy

    int    curFrame = -1;        // текущий кадр
    //float  animSpeed;          // скорость анимации

    bool       isVisible = true;        /* видимость объекта      */
    map<string, TUserVar*> userVars;    /* пользовательские значения */

    TObject(ScrResourceInMem* index);
    virtual ~TObject();

    virtual void init(string className = "", string objectName = "", bool numerate = true);
    virtual void update();

    /* управление кадрами */
    virtual void setFrame(int frame);
    virtual int getFrame()          {return curFrame;}
    virtual int getFramesCount();

    /* управление коллизионной коробкой объекта */
    virtual void enableCollision() {}
    virtual void disableCollision() {}
    virtual bool isEnabledCollision() { return false; }

    /* установить видимость объекта */
    virtual void setVisibility(bool isVisible);

    /* adders */
    void addVar(const string& key, int val);
    void addVar(const string& key, double val);
    void addVar(const string& key, bool val);
    void addVar(const string& key, const char* val);
    void addVar(const string& key, const string& val);
    void addVar(const string& key, vector<double>& val);

    /* setters */
    void setVar(const string& key, double val);
    void setVar(const string& key, bool val);
    void setVar(const string& key, char* val);
    void setVar(const string& key, string val);
    void setVar(const string& key, vector<double> val);

    /* getters */
    double getVarNumber(const string& key);
    bool getVarBool(const string& key);
    string getVarString(const string& key);
    vector<double> getVarVector(const string& key);

    /** delete userVars value */
    void removeVar(const string& key);

    /** clear userVars */
    inline void clearVars();

    /* check type */
    bool isVarNumber(const string& key);
    bool isVarBool(const string& key);
    bool isVarString(const string& key);
    bool isVarVector(const string& key);

    /* manipulations with object */
    /* position */
    virtual void      setPosition(wVector3f& pos) {}
    virtual void      setPosition(wVector2f& pos) {}
    virtual void      setPositionX(float& x) {}
    virtual void      setPositionY(float& y) {}
    virtual void      setPositionZ(float& z) {}
    //virtual wVector3f getPosition()  { return wVECTOR3f_ZERO; }
    virtual void      getPosition(vector<double>& pos) { pos.clear(); }
    virtual float     getPositionX() { return 0; }
    virtual float     getPositionY() { return 0; }
    virtual float     getPositionZ() { return 0; }
    virtual void      move(wVector3f& moveVector) {}
    /* rotation */
    virtual void      setRotation(wVector3f& rot) {}
    virtual void      setRotation(float& rot) {}
    virtual void      setRotationX(float& x)  {}
    virtual void      setRotationY(float& y)  {}
    virtual void      setRotationZ(float& z)  {}
    //virtual wVector3f getRotation()  { return wVECTOR3f_ZERO; }
    virtual void      getRotation(vector<double>& rot) { rot.clear(); }
    virtual float     getRotationX() { return 0; }
    virtual float     getRotationY() { return 0; }
    virtual float     getRotationZ() { return 0; }
    virtual void      rotate(wVector3f& rot) {}
    /* size */
    //virtual wVector3f getSize()  { return wVECTOR3f_ZERO; }
    virtual void      getSize(vector<double>& size) { size.clear(); }
    virtual float     getSizeX() { return 0; }
    virtual float     getSizeY() { return 0; }
    virtual float     getSizeZ() { return 0; }
    /* scale */
    virtual void      setScale(wVector3f& scale) {}
    virtual void      setScale(wVector2f& scale) {}
    virtual void      setScaleX(float& x) {}
    virtual void      setScaleY(float& y) {}
    virtual void      setScaleZ(float& z) {}
    //virtual wVector3f getScale()  { return wVECTOR3f_ZERO; }
    virtual void      getScale(vector<double>& scale) { scale.clear(); }
    virtual float     getScaleX() { return 0; }
    virtual float     getScaleY() { return 0; }
    virtual float     getScaleZ() { return 0; }
};

/** Итератор для обхода по словарю */
typedef map<string, TUserVar*>::iterator TUserVarIterator;

/** указатель на обрабатываемый скриптом на данный момент объект */
extern int g_ObjectId;
extern vector<TObject*> g_ObjectsForInit;   // таблица объектов,которые надо инициализировать
extern vector<TObject*> g_ObjectsForDelete; // таблица объектов,которые надо удалить

/** Инициализация выделенных объектов */
void InitObjects();
/** удаление объектов, помеченных на удаление */
void DeleteObjects();

void RegisterObjectJSFunctions();



/* unit-тесты */

/** проверка работы пользовательских переменных */
//#define UNIT_TESTS
#ifdef UNIT_TESTS
static void unitTestObjectUserVars()
{
    /* 1. add new elements */
    TObject* object = new TObject();
    object->addVar("first", 10.0);
    object->addVar("second", "Jopka");
    object->addVar("third", true);
    object->addVar("forth", vector<double>{8,1,2});

    /* 2. get values */
    vector<double> vect = object->getVarVector("forth");
    cout << "first: "  << object->getVarNumber("first" ) << endl <<
            "second: " << object->getVarString("second") << endl <<
            "third: "  << object->getVarBool  ("third" ) << endl <<
            "forth: ["  << vect[0] << ", " << vect[1] << ", " << vect[2] << "]" << endl;
    assert(object->getVarNumber("first" ) == 10.0);
    assert(object->getVarString("second") == "Jopka");
    assert(object->getVarBool  ("third" ) == true);

    /* 3. delete element */
    cout << endl << "delete first element" << endl;
    object->removeVar("first");
    cout << "first: "  << object->getVarNumber("first") << endl;
    assert(object->getVarNumber("first") == numeric_limits<double>::max());

    /* 4. re-add element */
    object->addVar("first", 1.0);
    object->addVar("first", "now it is string!");
    cout << "first:"  << object->getVarString("first") << endl;
    assert(object->getVarString("first") == "now it is string!");

    /* 5. check type of elements */
    cout << "first is number? - "  << object->isVarNumber("first")  << endl;
    cout << "first is string? - "  << object->isVarString("first")  << endl;
    cout << "second is string? - " << object->isVarString("second") << endl;
    cout << "third is bool? - "    << object->isVarBool("third")    << endl;
    cout << "forth is vector? - "  << object->isVarVector("forth")  << endl;
    assert(object->isVarNumber("first") == false);
    assert(object->isVarString("first") == true);

    delete object;

    cout << "\n\n----------All tests were pass!----------\n\n";
}
#endif // UNIT_TESTS

#endif // BASE_OBJECT_H
