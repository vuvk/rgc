#ifndef PLAYER_H
#define PLAYER_H


#include "WorldSim3D.h"

#include "object3d.h"
#include "res_in_mem.h"


class TPlayer : public TObject3D
{
public:
    wAnimator* physAnim = nullptr;

    TPlayer(float x = 0.0, float y = 0.0, float z = 0.0);
    ~TPlayer();

    void init(string className, string objectName, bool numerate)
    {
        TObject::init("PlayerClass", "Player", false);
    }

    void createCollider();
    void destroyCollider();
};

class TCamera : public TObject3D
{
public:
    float pitch = 0.0;
    float yaw   = 0.0;
    float roll  = 0.0;

    TCamera(float x = 0.0, float y = 0.0, float z = 0.0);

    void init(string className, string objectName, bool numerate)
    {
        TObject::init("CameraClass", "Camera", false);
    }

    void updateAngles();

    void setPitch(float deg);
    void setYaw(float deg);
    /*
    void setRoll(float deg);
    */
};

extern TPlayer* g_Player;
extern TCamera* g_Camera;
extern ScrResourceInMem g_PlayerResource;
extern ScrResourceInMem g_CameraResource;

void RegisterPlayerJSFunctions();
void RegisterCameraJSFunctions();

#endif // PLAYER_H
