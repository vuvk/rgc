#ifndef WEAPON_IN_HAND_H
#define WEAPON_IN_HAND_H

#include <vector>
#include "WorldSim3D.h"
#include "object2d.h"

/** информация об оружии в принципе */
class TWeaponInMem : public ScrResourceInMem
{
public:
    int      fireFrame;
    std::string fireSound;
    uint8_t  weaponAlign;
    double   damage,
             accuracy,
             distance;
    uint32_t curAmmo,
             maxAmmo;
    bool     infiniteAmmo;
    int      bulletId,
             fireType,
             ammoTypeId;

    TWeaponInMem() : ScrResourceInMem()
    {
        type = rtWeaponInHand;
        TWeaponInMem::clear();
    }

    virtual void clear()
    {
        fireFrame = 0;
        ammoTypeId = -1;
        fireSound = "";
        weaponAlign = 0;
        damage = 0;
        accuracy = 100;
        distance = 100;
        curAmmo = 0;
        maxAmmo = 0;
        infiniteAmmo = false;
        bulletId = 0;
        fireType = 0;

        ScrResourceInMem::clear();
    }
};

class TWeapon: public TObject2D
{
public:
    static vector<TWeapon*> objects;

    bool isAvailable = false;    // есть ли это оружие у игрока

    TWeapon(TWeaponInMem* index);
    virtual ~TWeapon();

    virtual void init(string className = "", string objectName = "", bool numerate = true)
    {
        TObject::init("Weapon", "weapon");
    }
/*
    void update()
    {
        if (!this->isVisible || this->index == nullptr)
            return;

        draw();
        TObject2D::update();
    }
*/
};


extern vector<TWeaponInMem> g_Weapons;
extern int g_ActiveWeapon;          /** активное в текущий момент оружие   */


void InitWeapons();
bool LoadWeaponsPack();
void WeaponUpdate();

void RegisterWeaponsJSFunctions();

#endif // WEAPON_IN_HAND_H
