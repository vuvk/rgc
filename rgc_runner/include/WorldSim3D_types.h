#ifndef __WORLDSIM3D_TYPES_H_INCLUDED
#define __WORLDSIM3D_TYPES_H_INCLUDED

#ifdef __cplusplus
	#include <iostream>
	#include <cstring>
	#include <cmath>
#else   // pureC
    #include <math.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <stdint.h>
    #include <stdbool.h>
    #include <string.h>
    #include <wchar.h>
    #include <stdint.h>
#endif  // __cplusplus

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>

#define FLOAT_DEFAULTVALUE 7285624.0f

/*typedef uint64_t UInt64;
typedef uint32_t UInt32;
typedef uint16_t UInt16;
typedef uint8_t UInt8;

typedef int64_t Int64;
typedef int32_t Int32;
typedef int16_t Int16;
typedef int8_t Int8;*/

#define WSTR_BUFFER_SIZE 256
#if defined(_WIN32) || defined(WIN32) || defined(_WIN64) || defined(WIN64)
    #define swprint(output, format, ...) swprintf(output, format,__VA_ARGS__)
#else
    //#if defined(__linux) || defined(__linux__)
        #define swprint(output, format, ...) swprintf(output, WSTR_BUFFER_SIZE, format, __VA_ARGS__)
    //#endif // defined
#endif

typedef unsigned long int UInt64;
typedef unsigned int UInt32;
typedef unsigned short UInt16;
typedef unsigned char UInt8;

typedef signed long int Int64;
typedef signed int Int32;
typedef signed short Int16;
typedef signed char Int8;


typedef double Float64;
typedef float Float32;

typedef UInt32 wImage;
typedef UInt32 wTexture;
typedef UInt32 wFont;

typedef UInt32 wGuiObject;

typedef UInt32 wMesh;
typedef UInt32 wMeshBuffer;

typedef UInt32 wNode;

typedef UInt32 wMaterial;
typedef UInt32 wSelector;
typedef UInt32 wEmitter;
typedef UInt32 wAffector;
typedef UInt32 wAnimator;

typedef UInt32 wXmlReader;
typedef UInt32 wXmlWriter;

typedef UInt32 wFile;

typedef UInt32 wSoundEffect;
typedef UInt32 wSoundFilter;
typedef UInt32 wSound;
typedef UInt32 wSoundBuffer;
typedef UInt32 wVideo;

typedef UInt32 wPostEffect;

typedef UInt32 wPacket;

//' Mouse events
typedef enum
{
    wMET_LMOUSE_PRESSED_DOWN = 0,
    wMET_RMOUSE_PRESSED_DOWN,
    wMET_MMOUSE_PRESSED_DOWN,
    wMET_LMOUSE_LEFT_UP,
    wMET_RMOUSE_LEFT_UP,
    wMET_MMOUSE_LEFT_UP,
    wMET_MOUSE_MOVED,
    wMET_MOUSE_WHEEL,
    wMET_LMOUSE_DOUBLE_CLICK,
    wMET_RMOUSE_DOUBLE_CLICK,
    wMET_MMOUSE_DOUBLE_CLICK,
    wMET_LMOUSE_TRIPLE_CLICK,
    wMET_RMOUSE_TRIPLE_CLICK,
    wMET_MMOUSE_TRIPLE_CLICK,
    wMET_COUNT
}wMouseEventType;

typedef enum
{
    wMB_LEFT    = 0x01,
    wMB_RIGHT   = 0x02,
    wMB_MIDDLE  = 0x04,
    wMB_EXTRA1  = 0x08,//not used
    wMB_EXTRA2  = 0x10,//not used
    wMB_FORCE_32_BIT = 0x7fffffff//not for use!
}wMouseButtons;

typedef struct
{
    Int32 x;
    Int32 y;
}
wVector2i;

static const wVector2i wVECTOR2i_ZERO={0,0};
static const wVector2i wVECTOR2i_ONE={1,1};

typedef struct
{
    wMouseEventType action;
    Float32 delta;
    wVector2i position;
    bool isShift;
    bool isControl;
}
wMouseEvent;

typedef enum
{
		wKC_UNKNOWN          = 0x0,
		wKC_LBUTTON          = 0x01,  // Left mouse button
		wKC_RBUTTON          = 0x02,  // Right mouse button
		wKC_CANCEL           = 0x03,  // Control-break processing
		wKC_MBUTTON          = 0x04,  // Middle mouse button (three-button mouse)
		wKC_XBUTTON1         = 0x05,  // Windows 2000/XP: X1 mouse button
		wKC_XBUTTON2         = 0x06,  // Windows 2000/XP: X2 mouse button
		wKC_BACK             = 0x08,  // BACKSPACE key
		wKC_TAB              = 0x09,  // TAB key
		wKC_CLEAR            = 0x0C,  // CLEAR key
		wKC_RETURN           = 0x0D,  // ENTER key
		wKC_SHIFT            = 0x10,  // SHIFT key
		wKC_CONTROL          = 0x11,  // CTRL key
		wKC_MENU             = 0x12,  // ALT key
		wKC_PAUSE            = 0x13,  // PAUSE key
		wKC_CAPITAL          = 0x14,  // CAPS LOCK key
		wKC_KANA             = 0x15,  // IME Kana mode
		wKC_HANGUEL          = 0x15,  // IME Hanguel mode (maintained for compatibility use KEY_HANGUL)
		wKC_HANGUL           = 0x15,  // IME Hangul mode
		wKC_JUNJA            = 0x17,  // IME Junja mode
		wKC_FINAL            = 0x18,  // IME final mode
		wKC_HANJA            = 0x19,  // IME Hanja mode
		wKC_KANJI            = 0x19,  // IME Kanji mode
		wKC_ESCAPE           = 0x1B,  // ESC key
		wKC_CONVERT          = 0x1C,  // IME convert
		wKC_NONCONVERT       = 0x1D,  // IME nonconvert
		wKC_ACCEPT           = 0x1E,  // IME accept
		wKC_MODECHANGE       = 0x1F,  // IME mode change request
		wKC_SPACE            = 0x20,  // SPACEBAR
		wKC_PRIOR            = 0x21,  // PAGE UP key
		wKC_NEXT             = 0x22,  // PAGE DOWN key
		wKC_END              = 0x23,  // END key
		wKC_HOME             = 0x24,  // HOME key
		wKC_LEFT             = 0x25,  // LEFT ARROW key
		wKC_UP               = 0x26,  // UP ARROW key
		wKC_RIGHT            = 0x27,  // RIGHT ARROW key
		wKC_DOWN             = 0x28,  // DOWN ARROW key
		wKC_SELECT           = 0x29,  // SELECT key
		wKC_PRINT            = 0x2A,  // PRINT key
		wKC_EXECUT           = 0x2B,  // EXECUTE key
		wKC_SNAPSHOT         = 0x2C,  // PRINT SCREEN key
		wKC_INSERT           = 0x2D,  // INS key
		wKC_DELETE           = 0x2E,  // DEL key
		wKC_HELP             = 0x2F,  // HELP key
		wKC_KEY_0            = 0x30,  // 0 key
		wKC_KEY_1            = 0x31,  // 1 key
		wKC_KEY_2            = 0x32,  // 2 key
		wKC_KEY_3            = 0x33,  // 3 key
		wKC_KEY_4            = 0x34,  // 4 key
		wKC_KEY_5            = 0x35,  // 5 key
		wKC_KEY_6            = 0x36,  // 6 key
		wKC_KEY_7            = 0x37,  // 7 key
		wKC_KEY_8            = 0x38,  // 8 key
		wKC_KEY_9            = 0x39,  // 9 key
		wKC_KEY_A            = 0x41,  // A key
		wKC_KEY_B            = 0x42,  // B key
		wKC_KEY_C            = 0x43,  // C key
		wKC_KEY_D            = 0x44,  // D key
		wKC_KEY_E            = 0x45,  // E key
		wKC_KEY_F            = 0x46,  // F key
		wKC_KEY_G            = 0x47,  // G key
		wKC_KEY_H            = 0x48,  // H key
		wKC_KEY_I            = 0x49,  // I key
		wKC_KEY_J            = 0x4A,  // J key
		wKC_KEY_K            = 0x4B,  // K key
		wKC_KEY_L            = 0x4C,  // L key
		wKC_KEY_M            = 0x4D,  // M key
		wKC_KEY_N            = 0x4E,  // N key
		wKC_KEY_O            = 0x4F,  // O key
		wKC_KEY_P            = 0x50,  // P key
		wKC_KEY_Q            = 0x51,  // Q key
		wKC_KEY_R            = 0x52,  // R key
		wKC_KEY_S            = 0x53,  // S key
		wKC_KEY_T            = 0x54,  // T key
		wKC_KEY_U            = 0x55,  // U key
		wKC_KEY_V            = 0x56,  // V key
		wKC_KEY_W            = 0x57,  // W key
		wKC_KEY_X            = 0x58,  // X key
		wKC_KEY_Y            = 0x59,  // Y key
		wKC_KEY_Z            = 0x5A,  // Z key
		wKC_LWIN             = 0x5B,  // Left Windows key (Microsoft® Natural® keyboard)
		wKC_RWIN             = 0x5C,  // Right Windows key (Natural keyboard)
		wKC_APPS             = 0x5D,  // Applications key (Natural keyboard)
		wKC_SLEEP            = 0x5F,  // Computer Sleep key
		wKC_NUMPAD0          = 0x60,  // Numeric keypad 0 key
		wKC_NUMPAD1          = 0x61,  // Numeric keypad 1 key
		wKC_NUMPAD2          = 0x62,  // Numeric keypad 2 key
		wKC_NUMPAD3          = 0x63,  // Numeric keypad 3 key
		wKC_NUMPAD4          = 0x64,  // Numeric keypad 4 key
		wKC_NUMPAD5          = 0x65,  // Numeric keypad 5 key
		wKC_NUMPAD6          = 0x66,  // Numeric keypad 6 key
		wKC_NUMPAD7          = 0x67,  // Numeric keypad 7 key
		wKC_NUMPAD8          = 0x68,  // Numeric keypad 8 key
		wKC_NUMPAD9          = 0x69,  // Numeric keypad 9 key
		wKC_MULTIPLY         = 0x6A,  // Multiply key
		wKC_ADD              = 0x6B,  // Add key
		wKC_SEPARATOR        = 0x6C,  // Separator key
		wKC_SUBTRACT         = 0x6D,  // Subtract key
		wKC_DECIMAL          = 0x6E,  // Decimal key
		wKC_DIVIDE           = 0x6F,  // Divide key
 		wKC_F1               = 0x70,  // F1 key
		wKC_F2               = 0x71,  // F2 key
		wKC_F3               = 0x72,  // F3 key
		wKC_F4               = 0x73,  // F4 key
		wKC_F5               = 0x74,  // F5 key
		wKC_F6               = 0x75,  // F6 key
		wKC_F7               = 0x76,  // F7 key
		wKC_F8               = 0x77,  // F8 key
		wKC_F9               = 0x78,  // F9 key
		wKC_F10              = 0x79,  // F10 key
		wKC_F11              = 0x7A,  // F11 key
		wKC_F12              = 0x7B,  // F12 key
		wKC_F13              = 0x7C,  // F13 key
		wKC_F14              = 0x7D,  // F14 key
		wKC_F15              = 0x7E,  // F15 key
		wKC_F16              = 0x7F,  // F16 key
		wKC_F17              = 0x80,  // F17 key
		wKC_F18              = 0x81,  // F18 key
		wKC_F19              = 0x82,  // F19 key
		wKC_F20              = 0x83,  // F20 key
		wKC_F21              = 0x84,  // F21 key
		wKC_F22              = 0x85,  // F22 key
		wKC_F23              = 0x86,  // F23 key
		wKC_F24              = 0x87,  // F24 key
		wKC_NUMLOCK          = 0x90,  // NUM LOCK key
		wKC_SCROLL           = 0x91,  // SCROLL LOCK key
		wKC_LSHIFT           = 0xA0,  // Left SHIFT key
		wKC_RSHIFT           = 0xA1,  // Right SHIFT key
		wKC_LCONTROL         = 0xA2,  // Left CONTROL key
		wKC_RCONTROL         = 0xA3,  // Right CONTROL key
		wKC_LMENU            = 0xA4,  // Left MENU key
		wKC_RMENU            = 0xA5,  // Right MENU key
		wKC_BROWSER_BACK     = 0xA6,  // Browser Back key
		wKC_BROWSER_FORWARD  = 0xA7,  // Browser Forward key
		wKC_BROWSER_REFRESH  = 0xA8,  // Browser Refresh key
		wKC_BROWSER_STOP     = 0xA9,  // Browser Stop key
		wKC_BROWSER_SEARCH   = 0xAA,  // Browser Search key
		wKC_BROWSER_FAVORITES =0xAB,  // Browser Favorites key
		wKC_BROWSER_HOME     = 0xAC,  // Browser Start and Home key
		wKC_VOLUME_MUTE      = 0xAD,  // Volume Mute key
		wKC_VOLUME_DOWN      = 0xAE,  // Volume Down key
		wKC_VOLUME_UP        = 0xAF,  // Volume Up key
		wKC_MEDIA_NEXT_TRACK = 0xB0,  // Next Track key
		wKC_MEDIA_PREV_TRACK = 0xB1,  // Previous Track key
		wKC_MEDIA_STOP       = 0xB2,  // Stop Media key
		wKC_MEDIA_PLAY_PAUSE = 0xB3,  // Play/Pause Media key
		wKC_OEM_1            = 0xBA,  // for US    ";:"
		wKC_PLUS             = 0xBB,  // Plus Key   "+"
		wKC_COMMA            = 0xBC,  // Comma Key  ","
		wKC_MINUS            = 0xBD,  // Minus Key  "-"
		wKC_PERIOD           = 0xBE,  // Period Key "."
		wKC_OEM_2            = 0xBF,  // for US    "/?"
		wKC_OEM_3            = 0xC0,  // for US    "`~"
		wKC_OEM_4            = 0xDB,  // for US    "[{"
		wKC_OEM_5            = 0xDC,  // for US    "\|"
		wKC_OEM_6            = 0xDD,  // for US    "]}"
		wKC_OEM_7            = 0xDE,  // for US    "'""
		wKC_OEM_8            = 0xDF,  // None
		wKC_OEM_AX           = 0xE1,  // for Japan "AX"
		wKC_OEM_102          = 0xE2,  // "<>" or "\|"
		wKC_ATTN             = 0xF6,  // Attn key
		wKC_CRSEL            = 0xF7,  // CrSel key
		wKC_EXSEL            = 0xF8,  // ExSel key
		wKC_EREOF            = 0xF9,  // Erase EOF key
		wKC_PLAY             = 0xFA,  // Play key
		wKC_ZOOM             = 0xFB,  // Zoom key
		wKC_PA1              = 0xFD,  // PA1 key
		wKC_OEM_CLEAR        = 0xFE,  // Clear key
		wKC_NONE			 = 0xFF,  // usually no key mapping, but some laptops use it for fn key

		wKC_KEY_CODES_COUNT  = 0x100 // this is not a key, but the amount of keycodes there are.
}wKeyCode;

typedef enum
{
   wKD_UP=0,
   wKD_DOWN
}wKeyDirection;

typedef struct
{
    wKeyCode key;
    wKeyDirection direction;
    //wchar_t keyChar; //Character corresponding to the key (0, if not a character)
    bool isShift;
    bool isControl;
}
wKeyEvent;

typedef enum
{
    wKA_MOVE_FORWARD = 0,
    wKA_MOVE_BACKWARD,
    wKA_STRAFE_LEFT,
    wKA_STRAFE_RIGHT,
    wKA_JUMP_UP,
    wKA_COUNT,
    wKA_FORCE_32BIT = 0x7fffffff
}wKeyAction;

typedef struct
{
    wKeyAction Action;
    wKeyCode KeyCode;
}wKeyMap;

static
wKeyMap wKeyMapDefault[8]={ {wKA_MOVE_FORWARD,  wKC_KEY_W},
                            {wKA_MOVE_FORWARD,  wKC_UP   },
                            {wKA_MOVE_BACKWARD, wKC_KEY_S},
                            {wKA_MOVE_BACKWARD, wKC_DOWN },
                            {wKA_STRAFE_LEFT,   wKC_KEY_A},
                            {wKA_STRAFE_LEFT,   wKC_LEFT },
                            {wKA_STRAFE_RIGHT,  wKC_KEY_D},
                            {wKA_STRAFE_RIGHT,  wKC_RIGHT} };

typedef enum
{
    wJPH_PRESENT,//A hat is definitely present.
    wJPH_ABSENT, //A hat is definitely not present.
    wJPH_UNKNOWN //The presence or absence of a hat cannot be determined.
}wJoystickPovHat;

typedef struct
{
    //Note: with a Linux device, the POV hat (if any) will use two axes.
    //These will be included in this count.
    UInt32 Axes;//The number of axes that the joystick has, i.e. X, Y, Z, R, U, V.

    UInt32 Buttons;//The number of buttons that the joystick has.

    //This is an internal WS3D index; it does not map directly to any particular hardware joystick.
    UInt8 joyId;//The ID of the joystick.

    //char joyName0[256];//
    const char* joyName;

    //A Windows device will identify the presence or absence or the POV hat.
    //A Linux device cannot, and will always return wJPH_UNKNOWN.
    //Mac OSX not supported!
    wJoystickPovHat PovHat;//An indication of whether the joystick has a POV hat.

}wJoystickInfo;

enum
{
    wNUMBER_OF_BUTTONS=32,
    wAXIS_X=0,
    wAXIS_Y,
    wAXIS_Z,
    wAXIS_R,
    wAXIS_U,
    wAXIS_V,
    wNUMBER_OF_AXES
};

typedef struct
{
    UInt8 joyId;//The ID of the joystick which generated this event.
    bool ButtonStates[wNUMBER_OF_BUTTONS];//A helper function to check if a button is pressed
    signed short Axis[wNUMBER_OF_AXES];
    UInt16 POV;

}wJoystickEvent;

//' Event types
typedef enum
{
    wET_GUI_EVENT            = 0,
    wET_MOUSE_INPUT_EVENT,
    wET_KEY_INPUT_EVENT,
    wET_JOYSTICK_INPUT_EVENT,
    wET_LOG_TEXT_EVENT,
    wET_USER_EVENT,
    wET_FORCE_32_BIT = 0x7fffffff
}wEventType;

typedef enum
{
    wGCT_ELEMENT_FOCUS_LOST,//A gui element has lost its focus.GUIEvent.Caller is losing the focus to GUIEvent.Element.
                            //If the event is absorbed then the focus will not be changed.
    wGCT_ELEMENT_FOCUSED,//A gui element has got the focus.
                          //If the event is absorbed then the focus will not be changed.
    wGCT_ELEMENT_HOVERED,//The mouse cursor hovered over a gui element.
                        //If an element has sub-elements you also get this message for the subelements
    wGCT_ELEMENT_LEFT,//The mouse cursor left the hovered element.
                    //If an element has sub-elements you also get this message for the subelements
    wGCT_ELEMENT_CLOSED,//An element would like to close.
                        //Windows and context menus use this event when they would like to close, this can be cancelled by absorbing the event.
    wGCT_BUTTON_CLICKED, //A button was clicked.

    wGCT_SCROLL_BAR_CHANGED,//A scrollbar has changed its position.

    wGCT_CHECKBOX_CHANGED,//A checkbox has changed its check state.

    wGCT_LISTBOX_CHANGED,//A new item in a listbox was selected.
                        //NOTE: You also get this event currently when the same item was clicked again after more than 500 ms.
    wGCT_LISTBOX_SELECTED_AGAIN, //An item in the listbox was selected, which was already selected.
                       //NOTE: You get the event currently only if the item was clicked again within 500 ms or selected by "enter" or "space".
    wGCT_FILE_SELECTED, //A file has been selected in the file dialog.
    wGCT_DIRECTORY_SELECTED, //A directory has been selected in the file dialog.
    wGCT_FILE_CHOOSE_DIALOG_CANCELLED,//A file open dialog has been closed without choosing a file.
    wGCT_MESSAGEBOX_YES,//'Yes' was clicked on a messagebox
    wGCT_MESSAGEBOX_NO,//'No' was clicked on a messagebox
    wGCT_MESSAGEBOX_OK,//'OK' was clicked on a messagebox
    wGCT_MESSAGEBOX_CANCEL,//'Cancel' was clicked on a messagebox
    wGCT_EDITBOX_ENTER,//In an editbox 'ENTER' was pressed.
    wGCT_EDITBOX_CHANGED,//The text in an editbox was changed. This does not include automatic changes in text-breaking.
    wGCT_EDITBOX_MARKING_CHANGED,//The marked area in an editbox was changed.
    wGCT_TAB_CHANGED,//The tab was changed in an tab control.
    wGCT_MENU_ITEM_SELECTED,//A menu item was selected in a (context) menu.
    wGCT_COMBO_BOX_CHANGED,//The selection in a combo box has been changed.
    wGCT_SPINBOX_CHANGED,//The value of a spin box has changed.
    wGCT_TABLE_CHANGED,//A table has changed.
    wGCT_TABLE_HEADER_CHANGED,
    wGCT_TABLE_SELECTED_AGAIN,
    wGCT_TREEVIEW_NODE_DESELECT,//A tree view node lost selection. See IGUITreeView::getLastEventNode().
    wGCT_TREEVIEW_NODE_SELECT,//A tree view node was selected. See IGUITreeView::getLastEventNode().
    wGCT_TREEVIEW_NODE_EXPAND,//A tree view node was expanded. See IGUITreeView::getLastEventNode().
    wGCT_TREEVIEW_NODE_COLLAPSE,//A tree view node was collapsed. See IGUITreeView::getLastEventNode().
    wGCT_RADIOBUTTONGROUP_CHANGED,//new
	wGCT_RADIOCHECKBOXGROUP_CHANGED,//new
    wGCT_COUNT //No real event. Just for convenience to get number of events.
}wGuiCallerType;

typedef struct
{
	Int32	id;
	const char*  name;
	wGuiCallerType  event;
	wVector2i position;
}
wGuiEvent;

typedef enum
{
    wGMBF_OK            =  0x1, //Flag for the ok button.
    wGMBF_CANCEL        =  0x2,//Flag for the cancel button.
    wGMBF_YES           =  0x4,//Flag for the yes button.
    wGMBF_NO            =  0x8,//Flag for the no button.
    wGMBF_FORCE_32BIT   =  0x7fffffff//This value is not used. It only forces this enumeration to compile in 32 bit.
}wGuiMessageBoxFlags;

typedef enum
{
	wFT_EXP,
	wFT_LINEAR,
	wFT_EXP2

}wFogType;

typedef struct
{
    Float32 x;
    Float32 y;
}
wVector2f;

static const wVector2f wVECTOR2f_ZERO={0,0};
static const wVector2f wVECTOR2f_ONE={1.0f,1.0f};

typedef struct
{
    UInt32 x;
    UInt32 y;
}
wVector2u;

static const wVector2u wVECTOR2u_ZERO={0,0};
static const wVector2u wVECTOR2u_ONE={1,1};

static const wVector2u wDEFAULT_SCREENSIZE={800,600};

typedef struct
{
    Float32 x;
    Float32 y;
    Float32 z;
}
wVector3f;

static const wVector3f wVECTOR3f_ZERO       = {0,0,0};
static const wVector3f wVECTOR3f_ONE        = {1.0f,1.0f,1.0f};
static const wVector3f wVECTOR3f_UP         = {0.0f,1.0f,0.0f};
static const wVector3f wVECTOR3f_DOWN       = {0.0f,-1.0f,0.0f};
static const wVector3f wVECTOR3f_FORWARD    = { 0.0f, 0.0f, -1.0f};
static const wVector3f wVECTOR3f_BACKWARD   = { 0.0f, 0.0f, 1.0f};
static const wVector3f wVECTOR3f_RIGHT      = { 1.0f, 0.0f,  0.0f};
static const wVector3f wVECTOR3f_LEFT       = {-1.0f, 0.0f,  0.0f};

typedef struct
{
    Int32 x;
    Int32 y;
    Int32 z;
}
wVector3i;

static const wVector3i wVECTOR3i_ZERO={0,0,0};
static const wVector3i wVECTOR3i_ONE={1,1,1};
static const wVector3i wVECTOR3i_UP         = {0,1,0};
static const wVector3i wVECTOR3i_DOWN       = {0,-1,0};
static const wVector3i wVECTOR3i_FORWARD    = { 0,0,-1};
static const wVector3i wVECTOR3i_BACKWARD   = {0,0,1};
static const wVector3i wVECTOR3i_RIGHT      = { 1,0,0};
static const wVector3i wVECTOR3i_LEFT       = {-1,0,0};

typedef struct
{
    UInt32 x;
    UInt32 y;
    UInt32 z;
}
wVector3u;

static const wVector3u wVECTOR3u_ZERO={0,0,0};
static const wVector3u wVECTOR3u_ONE={1,1,1};

typedef struct
{
    UInt16 alpha;
    UInt16 red;
    UInt16 green;
    UInt16 blue;
}
wColor4s;

static const wColor4s wCOLOR4s_ZERO={0,0,0,0};

static const wColor4s wCOLOR4s_WHITE={255,255,255,255};

static const wColor4s wCOLOR4s_DARKGREY={255,64,64,64};
static const wColor4s wCOLOR4s_GREY={255,128,128,128};
static const wColor4s wCOLOR4s_SILVER={255,192,192,192};

static const wColor4s wCOLOR4s_BLACK={255,0,0,0};

static const wColor4s wCOLOR4s_RED={255,255,0,0};
static const wColor4s wCOLOR4s_DARKRED={255,140,0,0};
static const wColor4s wCOLOR4s_MAROON={255,128,0,0};

static const wColor4s wCOLOR4s_GREEN={255,0,255,0};
static const wColor4s wCOLOR4s_LIME={255,250,128,114};
static const wColor4s wCOLOR4s_DARKGREEN={255,0,100,0};
static const wColor4s wCOLOR4s_OLIVE={255,240,128,128};

static const wColor4s wCOLOR4s_BLUE={255,0,0,255};
static const wColor4s wCOLOR4s_DARKBLUE={255,0,0,139};
static const wColor4s wCOLOR4s_NAVY={255,0,0,128};
static const wColor4s wCOLOR4s_SKYBLUE={255,135,206,235};

static const wColor4s wCOLOR4s_MAGENTA={255,255,0,255};
static const wColor4s wCOLOR4s_PINK={255,255,192,203};
static const wColor4s wCOLOR4s_DEEPPINK={255,255,20,147};
static const wColor4s wCOLOR4s_INDIGO={255,75,0,130};

static const wColor4s wCOLOR4s_YELLOW={255,255,255,0};
static const wColor4s wCOLOR4s_GOLD={255,255,215,0};
static const wColor4s wCOLOR4s_KHAKI={255,245,230,140};

static const wColor4s wCOLOR4s_ORANGE={255,255,68,0};
static const wColor4s wCOLOR4s_DARKORANGE={255,255,140,0};
static const wColor4s wCOLOR4s_ORANGERED={255,255,69,0};

typedef struct
{
    Float32 alpha;
    Float32 red;
    Float32 green;
    Float32 blue;
}wColor4f;

static const wColor4f wCOLOR4f_WHITE={1.0f,1.0f,1.0f,1.0f};
static const wColor4f wCOLOR4f_BLACK={1.0f,0.0f,0.0f,0.0f};

typedef struct
{
    UInt16 red;
    UInt16 green;
    UInt16 blue;
}
wColor3s;

static const wColor3s wCOLOR3s_WHITE={255,255,255};
static const wColor3s wCOLOR3s_BLACK={0,0,0};

typedef struct
{
    Float32 red;
    Float32 green;
    Float32 blue;
}
wColor3f;

static const wColor3f wCOLOR3f_WHITE={1.0f,1.0f,1.0f};
static const wColor3f wCOLOR3f_BLACK={0,0,0};

typedef struct
{
    wVector3f  vertPos;
    wVector3f  vertNormal;
    wColor4s   vertColor;     // The 32bit ARGB color of the vertex
    wVector2f  texCoords;
}
wVert;

///STRUCTURES FOR PARTICLE EMITTERS///
typedef struct
{
#ifdef __cplusplus
    wVector3f direction={0,0.03f,0};
    UInt32 minParticlesPerSecond=5;
    UInt32 maxParticlesPerSecond=10;
    wColor4s minStartColor={255,0,0,0};
    wColor4s maxStartColor={255,255,255,255};
    UInt32 lifeTimeMin=2000;
    UInt32 lifeTimeMax=4000;
    Int32 maxAnglesDegrees=0;
    wVector2f minStartSize={5,5};
    wVector2f maxStartSize={5,5};
#else
    wVector3f direction;
    UInt32 minParticlesPerSecond;
    UInt32 maxParticlesPerSecond;
    wColor4s minStartColor;
    wColor4s maxStartColor;
    UInt32 lifeTimeMin;
    UInt32 lifeTimeMax;
    Int32 maxAnglesDegrees;
    wVector2f minStartSize;
    wVector2f maxStartSize;
#endif // __cplusplus
}wParticleEmitter;

typedef struct
{
    wVector3f center;
    Float32 length;
    wVector3f normal;
#ifdef __cplusplus
    bool getOutlineOnly=false;
#else
    bool getOutlineOnly;
#endif // __cplusplus
    Float32 radius;
}wParticleCylinderEmitter;

typedef struct
{
#ifdef __cplusplus
    wMesh* mesh;
    bool useNormalDirection=true;
    Float32 normalDirectionModifier;
    bool everyMeshVertex = true;
#else
    wMesh* mesh;
    bool useNormalDirection;
    Float32 normalDirectionModifier;
    bool everyMeshVertex;
#endif // __cplusplus
}wParticleMeshEmitter;

typedef struct
{
    wVector3f center;
    Float32 radius;
    Float32 ringThickness;
}wParticleRingEmitter;

typedef struct
{
    wVector3f center;
    Float32 radius;
}wParticleSphereEmitter;

///STRUCTURES FOR PARTICLE AFFECTORS///
typedef struct
{
#ifdef __cplusplus
    wVector3f point;
    bool attract=true;
    bool affectX=true;
    bool affectY=true;
    bool affectZ=true;
#else
    wVector3f point;
    bool attract;
    bool affectX;
    bool affectY;
    bool affectZ;
#endif // __cplusplus
}wParticleAttractionAffector;

typedef struct
{
    wColor4s* colorsList;
    UInt32 colorsCount;
    UInt32* timesList;
    UInt32 timesCount;
#ifdef __cplusplus
    bool smooth=false;
#else
    bool smooth;
#endif // __cplusplus
}wParticleColorMorphAffector;

typedef struct
{
    Float32 furthestDistance;
	Float32 nearestDistance;
	Float32 columnDistance;
	wVector3f center;
	wVector3f strength;
	bool distant;
}wParticlePushAffector;

typedef struct
{
	wVector3f* points;
	UInt32 pointsCount;
	Float32 speed;
	Float32 tightness;
	Float32 attraction;
	bool deleteAtFinalPoint;
}wParticleSplineAffector;

typedef struct
{
	wVector3f pointA;
	wVector3f pointB;
	wVector3f pointC;
}wTriangle;
const wTriangle wTRIANGLE3f_ZERO={0,0,0};

///STRUCTURE FOR NODE ANIMATORS///
///COLLISION RESPONSE ANIMATOR///
typedef struct
{
    ///read/write//
    wSelector* world;
    wNode* targetNode;
#ifdef __cplusplus
    wVector3f ellipsoidRadius={30.0f,60.0f,30.0f};
    wVector3f gravity={0.0f,-10.0f,0.0f};
    bool animateTarget;
    wVector3f ellipsoidTranslation={0.0f,0.0f,0.0f};
#else
    wVector3f ellipsoidRadius;
    wVector3f gravity;
    bool animateTarget;
    wVector3f ellipsoidTranslation;
#endif // __cplusplus

	///only for read//
    wVector3f collisionPoint;
    wVector3f collisionResultPosition;
    wTriangle collisionTriangle;
    wNode* collisionNode;
    bool isFalling;
    bool collisionOccured;
}wAnimatorCollisionResponse;

/* storage for information pertaining to a shader constant
 */
//typedef struct wConstant wConstant;
typedef struct tag_wConstant
{
	struct tag_wConstant*	next;
	const char*	name;
	Int32			address;
	Int32           preset;
	const Float32*	data;
	Int32			count;
} wConstant;

typedef enum
{
    wDRT_NULL = 0,            //' a NULL device with no display
    wDRT_SOFTWARE,            //' WorldSim3Ds default software renderer
    wDRT_BURNINGS_VIDEO,      //     ' An improved quality software renderer
    wDRT_OPENGL,              //' hardware accelerated OpenGL renderer
    wDRT_DIRECT3D9,           //' hardware accelerated DirectX 9 renderer
    wDRT_CHOICE_CONSOLE=6
}wDriverTypes;

typedef enum
{
	wDT_BEST,      //'This selection allows Irrlicht to choose the best device from the ones available.
					  //'without opening a window. It can render the output of the software drivers to the console as ASCII.
					  //'It only supports mouse and keyboard in Windows operating systems.
	wDT_WIN32,       //'A device native to Microsoft Windows. This device uses the Win32 API and works in all versions of Windows.
	wDT_WINCE,       //'A device native to Windows CE devices.This device works on Windows Mobile, Pocket PC and Microsoft SmartPhone devices
	wDT_X11,         //'A device native to Unix style operating systems. This device uses the X11 windowing system and works in Linux,
	                  //'Solaris, FreeBSD, OSX and other operating systems which support X11.
	wDT_OSX,         //'A device native to Mac OSX. This device uses Apple's Cocoa API and works in Mac OSX 10.2 and above.
	wDT_SDL,         //'A device which uses Simple DirectMedia Layer. The SDL device works under all platforms supported by SDL
	wDT_FRAMEBUFFER, //'A device for raw framebuffer access.Best used with embedded devices and mobile systems.
					  //'Does not need X11 or other graphical subsystems. May support hw-acceleration via OpenGL-ES for FBDirect
	wDT_CONSOLE      //'A simple text only device supported by all platforms. This device allows applications to run from the command line
}wDeviceTypes;

//' Vertex shader program versions
typedef enum
{
    wVSV_1_1 = 0,
    wVSV_2_0,
    wVSV_2_a,
    wVSV_3_0
}wVertexShaderVersion;

//' Pixel shader program versions
typedef enum
{
    wPSV_1_1 = 0,
    wPSV_1_2,
    wPSV_1_3,
    wPSV_1_4,
    wPSV_2_0,
    wPSV_2_a,
    wPSV_2_b,
    wPSV_3_0
}wPixelShaderVersion;

typedef enum
{
    wGSV_4_0 = 0,
    wGSV_COUNT
}wGeometryShaderVersion;

//For Geometry shaders
typedef enum
{
    wPT_POINTS, //All vertices are non-connected points.
    wPT_LINE_STRIP,//All vertices form a single connected line.
    wPT_LINE_LOOP,//Just as LINE_STRIP, but the last and the first vertex is also connected.
    wPT_LINES,//Every two vertices are connected creating n/2 lines.
    wPT_TRIANGLE_STRIP,//After the first two vertices each vertex defines a new triangle. Always the two last and the new one form a new triangle.
    wPT_TRIANGLE_FAN,//After the first two vertices each vertex defines a new triangle. All around the common first vertex.
    wPT_TRIANGLES,//Explicitly set all vertices for each triangle.
    wPT_QUAD_STRIP,//After the first two vertices each further tw vetices create a quad with the preceding two.
    wPT_QUADS,//Every four vertices create a quad.
    wPT_POLYGON,//Just as LINE_LOOP, but filled.
    wPT_POINT_SPRITES,//}wPrimitiveType
    wPT_COUNT//Not for use!!!
}wPrimitiveType;

typedef enum
{
    wSC_NO_PRESET = 0,
    wSC_INVERSE_WORLD,
    wSC_WORLD_VIEW_PROJECTION,
    wSC_CAMERA_POSITION,
    wSC_TRANSPOSED_WORLD
}wShaderConstants;

const Float32 wShaderConstZero   =0;
const Float32 wShaderConstOne    =1;
const Float32 wShaderConstTwo    =2;
const Float32 wShaderConstThree  =3;

typedef enum
{
	//! Is driver able to render to a surface?
	wVDF_RENDER_TO_TARGET = 0,
	//! Is hardeware transform and lighting supported?
	wVDF_HARDWARE_TL,
	//! Are multiple textures per material possible?
	wVDF_MULTITEXTURE,
	//! Is driver able to render with a bilinear filter applied?
	wVDF_BILINEAR_FILTER,
	//! Can the driver handle mip maps?
	wVDF_MIP_MAP,
	//! Can the driver update mip maps automatically?
	wVDF_MIP_MAP_AUTO_UPDATE,
	//! Are stencilbuffers switched on and does the device support stencil buffers?
	wVDF_STENCIL_BUFFER,
	//! Is Vertex Shader 1.1 supported?
	wVDF_VERTEX_SHADER_1_1,
	//! Is Vertex Shader 2.0 supported?
	wVDF_VERTEX_SHADER_2_0,
	//! Is Vertex Shader 3.0 supported?
	wVDF_VERTEX_SHADER_3_0,
	//! Is Pixel Shader 1.1 supported?
	wVDF_PIXEL_SHADER_1_1,
	//! Is Pixel Shader 1.2 supported?
	wVDF_PIXEL_SHADER_1_2,
	//! Is Pixel Shader 1.3 supported?
	wVDF_PIXEL_SHADER_1_3,
	//! Is Pixel Shader 1.4 supported?
	wVDF_PIXEL_SHADER_1_4,
	//! Is Pixel Shader 2.0 supported?
	wVDF_PIXEL_SHADER_2_0,
	//! Is Pixel Shader 3.0 supported?
	wVDF_PIXEL_SHADER_3_0,
	//! Are ARB vertex programs v1.0 supported?
	wVDF_ARB_VERTEX_PROGRAM_1,
	//! Are ARB fragment programs v1.0 supported?
	wVDF_ARB_FRAGMENT_PROGRAM_1,
	//! Is GLSL supported?
	wVDF_ARB_GLSL,
	//! Is HLSL supported?
	wVDF_HLSL,
	//! Are non-square textures supported?
	wVDF_TEXTURE_NSQUARE,
	//! Are non-power-of-two textures supported?
	wVDF_TEXTURE_NPOT,
	//! Are framebuffer objects supported?
	wVDF_FRAMEBUFFER_OBJECT,
	//! Are vertex buffer objects supported?
	wVDF_VERTEX_BUFFER_OBJECT,
	//! Supports Alpha To Coverage
	wVDF_ALPHA_TO_COVERAGE,
	//! Supports Color masks (disabling color planes in output)
	wVDF_COLOR_MASK,
	//! Supports multiple render targets at once
	wVDF_MULTIPLE_RENDER_TARGETS,
	//! Supports separate blend settings for multiple render targets
	wVDF_MRT_BLEND,
	//! Supports separate color masks for multiple render targets
	wVDF_MRT_COLOR_MASK,
	//! Supports separate blend functions for multiple render targets
	wVDF_MRT_BLEND_FUNC,
	//! Supports geometry shaders
	wVDF_GEOMETRY_SHADER,
	//! Supports occlusion queries
	wVDF_OCCLUSION_QUERY,
	//! Supports polygon offset/depth bias for avoiding z-fighting
	wVDF_POLYGON_OFFSET,
	//! Support for different blend functions. Without, only ADD is available
	wVDF_BLEND_OPERATIONS,
	//! Support for separate blending for RGB and Alpha.
	wVDF_BLEND_SEPARATE,
	//! Support for texture coord transformation via texture matrix
	wVDF_TEXTURE_MATRIX,
	//! Support for DXTn compressed textures.
	wVDF_TEXTURE_COMPRESSED_DXT,
	//! Support for PVRTC compressed textures.
	wVDF_TEXTURE_COMPRESSED_PVRTC,
	//! Support for PVRTC2 compressed textures.
	wVDF_TEXTURE_COMPRESSED_PVRTC2,
	//! Support for ETC1 compressed textures.
	wVDF_TEXTURE_COMPRESSED_ETC1,
	//! Support for ETC2 compressed textures.
	wVDF_TEXTURE_COMPRESSED_ETC2,
	//! Support for cube map textures.
	wVDF_TEXTURE_CUBEMAP,
	//! Only used for counting the elements of this enum
	wVDF_COUNT
}wVideoFeatureQuery;

typedef enum
{
    wLL_DEBUG=0, //'Used for printing information helpful in debugging.
	wLL_INFORMATION=1, //'Useful information to print. For example hardware infos or something started/stopped.
  	wLL_WARNING=2, //'Warnings that something isn't as expected and can cause oddities.
	wLL_ERROR=3, //'Something did go wrong.
	wLL_NONE=4, //'Logs with wLL_NONE will never be filtered. And used as filter it will remove all logging except wLL_NONE messages.
}wLoggingLevel;

typedef enum
{
    wMF_WIREFRAME = 0,       //' Render as wireframe outline.
    wMF_POINTCLOUD,         //' Draw a point cloud instead of polygons.
    wMF_GOURAUD_SHADING,     //' Render smoothly across polygons.
    wMF_LIGHTING,           //' Material is effected by lighting.
    wMF_ZBUFFER,            //' Enable z-buffer.
    wMF_ZWRITE_ENABLE,       //' Can write as well as read z-buffer.
    wMF_BACK_FACE_CULLING,   //' Cull polygons facing away.
    wMF_FRONT_FACE_CULLING,  //' Cull polygons facing front.
    wMF_BILINEAR_FILTER,     //' Enable bilinear filtering.
    wMF_TRILINEAR_FILTER,    //' Enable trilinear filtering.
    wMF_ANISOTROPIC_FILTER,  //' Reduce blur in distant textures.
    wMF_FOG_ENABLE,          //' Enable fogging in the distance.
    wMF_NORMALIZE_NORMALS,   //' Use when scaling dynamically lighted models.
    wMF_TEXTURE_WRAP,        //' Gives access to all layers texture wrap settings. Overwrites separate layer settings.
    wMF_ANTI_ALIASING,       //' Anti-aliasing mode.
    wMF_COLOR_MASK,          //' ColorMask bits, for enabling the color planes.
    wMF_COLOR_MATERIAL,      //' ColorMaterial enum for vertex color interpretation.
}wMaterialFlags;

typedef enum
{
    wMT_SOLID = 0,
    wMT_SOLID_2_LAYER,
    wMT_LIGHTMAP,
    wMT_LIGHTMAP_ADD,
    wMT_LIGHTMAP_M2,
    wMT_LIGHTMAP_M4,
    wMT_LIGHTMAP_LIGHTING,
    wMT_LIGHTMAP_LIGHTING_M2,
    wMT_LIGHTMAP_LIGHTING_M4,
    wMT_DETAIL_MAP,
    wMT_SPHERE_MAP,
    wMT_REFLECTION_2_LAYER,
    wMT_TRANSPARENT_ADD_COLOR,
    wMT_TRANSPARENT_ALPHA_CHANNEL,
    wMT_TRANSPARENT_ALPHA_CHANNEL_REF,
    wMT_TRANSPARENT_VERTEX_ALPHA,
    wMT_TRANSPARENT_REFLECTION_2_LAYER,
    wMT_NORMAL_MAP_SOLID,
    wMT_NORMAL_MAP_TRANSPARENT_ADD_COLOR,
    wMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA,
    wMT_PARALLAX_MAP_SOLID,
    wMT_PARALLAX_MAP_TRANSPARENT_ADD_COLOR,
    wMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA,
    wMT_ONETEXTURE_BLEND,
    wMT_FOUR_DETAIL_MAP,
    wMT_TRANSPARENT_ADD_ALPHA_CHANNEL_REF,
    wMT_TRANSPARENT_ADD_ALPHA_CHANNEL,
    wMT_FORCE_32BIT = 0x7fffffff
} wMaterialTypes;

typedef enum
{
    wCM_NONE = 0,            //' Dont use vertex color for lighting
    wCM_DIFFUSE,             //' Use vertex color for diffuse light, (default)
    wCM_AMBIENT,             //' Use vertex color for ambient light
    wCM_EMISSIVE,            //' Use vertex color for emissive light
    wCM_SPECULAR,            //' Use vertex color for specular light
    wCM_DIFFUSE_AND_AMBIENT //' Use vertex color for both diffuse and ambient light
}wColorMaterial;

typedef enum
{
    wBF_ZERO = 0,
    wBF_ONE,
    wBF_DST_COLOR,
    wBF_ONE_MINUS_DST_COLOR,
    wBF_SRC_COLOR,
    wBF_ONE_MINUS_SRC_COLOR,
    wBF_SRC_ALPHA,
    wBF_ONE_MINUS_SRC_ALPHA,
    wBF_DST_ALPHA,
    wBF_ONE_MINUS_DST_ALPHA,
    wBF_SRC_ALPHA_SATURATE,
}wBlendFactor;

typedef enum
{
    wBO_SCREEN=0,
    wBO_ADD,
    wBO_SUBTRACT,
    wBO_MULTIPLY,
    wBO_DIVIDE
}wBlendOperation;

typedef enum
{
    wTCF_ALWAYS_16_BIT          = 0x00000001,  //' Forces the driver to create 16 bit textures always, independent of which format the file on disk has. When choosing this you may loose some color detail, but gain much speed and memory. 16 bit textures can be transferred twice as fast as 32 bit textures and only use half of the space in memory. When using this flag, it does not make sense to use the flags wTCF_ALWAYS_32_BIT, wTCF_OPTIMIZED_FOR_QUALITY, or wTCF_OPTIMIZED_FOR_SPEED at the same time.
    wTCF_ALWAYS_32_BIT          = 0x00000002,  //' Forces the driver to create 32 bit textures always, independent of which format the file on disk has. Please note that some drivers (like the software device) will ignore this, because they are only able to create and use 16 bit textures. When using this flag, it does not make sense to use the flags wTCF_ALWAYS_16_BIT, wTCF_OPTIMIZED_FOR_QUALITY, or wTCF_OPTIMIZED_FOR_SPEED at the same time.
    wTCF_OPTIMIZED_FOR_QUALITY  = 0x00000004,  //' Lets the driver decide in which format the textures are created and tries to make the textures look as good as possible. Usually it simply chooses the format in which the texture was stored on disk. When using this flag, it does not make sense to use the flags wTCF_ALWAYS_16_BIT, wTCF_ALWAYS_32_BIT, or wTCF_OPTIMIZED_FOR_SPEED at the same time.
    wTCF_OPTIMIZED_FOR_SPEED    = 0x00000008,  //' Lets the driver decide in which format the textures are created and tries to create them maximizing render speed. When using this flag, it does not make sense to use the flags wTCF_ALWAYS_16_BIT, wTCF_ALWAYS_32_BIT, or wTCF_OPTIMIZED_FOR_QUALITY, at the same time.
    wTCF_CREATE_MIP_MAPS        = 0x00000010, //' Automatically creates mip map levels for the textures.
    wTCF_NO_ALPHA_CHANNEL       = 0x00000020, //' Discard any alpha layer and use non-alpha color format.
    wTCF_ALLOW_NON_POWER_2      = 0x00000040 //' Allow non power of two dimention textures
}wTextureCreationFlag;

typedef enum
{
    wTC_REPEAT,
	wTC_CLAMP,
	wTC_CLAMP_TO_EDGE,
	wTC_CLAMP_TO_BORDER,
	wTC_MIRROR,
	wTC_MIRROR_CLAMP,
	wTC_MIRROR_CLAMP_TO_EDGE,
	wTC_MIRROR_CLAMP_TO_BORDER
}wTextureClamp;

typedef enum
{
   	wCP_NONE=0,
	wCP_ALPHA=1,
	wCP_RED=2,
	wCP_GREEN=4,
	wCP_BLUE=8,
	wCP_RGB=14,
	wCP_ALL=15
}wColorPlane;

//'Antialiasing mode for matrials
typedef enum
{
    wAAM_OFF = 0,
	wAAM_SIMPLE = 1,
	wAAM_QUALITY = 3,
	wAAM_LINE_SMOOTH = 4,
	wAAM_POINT_SMOOTH = 8,
	wAAM_FULL_BASIC = 15,
	wAAM_ALPHA_TO_COVERAGE = 16
}wAntiAliasingMode;

typedef enum
{
  	wCS_OFF=0,
	wCS_BOX=1,
	wCS_FRUSTUM_BOX=2,
	wCS_FRUSTUM_SPHERE=4
}wCullingState;

typedef enum
{
         		//' for irr-file reader info
	wSNT_CUBE=1,             		//' "cube"
	wSNT_SPHERE=2,           		//' "sphere"
	wSNT_TEXT=3,             		//' "text"
	wSNT_WATER_SURFACE=4,    		//' "waterSurface"
	wSNT_TERRAIN=5,          		//' "terrain"
	wSNT_SKY_BOX=6,          		//' "skyBox"
	wSNT_SKY_DOME=7,         		//' "skyDome"
	wSNT_SHADOW_VOLUME=8,    		//' "shadowVolume"
	wSNT_OCTREE=9,           		//' "octree"   ,   "octTree"
	wSNT_MESH =10,           		//' "mesh"
	wSNT_LIGHT=11,           		//' "light"
	wSNT_EMPTY=12,           		//' "empty"
	wSNT_DUMMY_TRANSFORMATION=13,  //' "dummyTreansormation"
	wSNT_CAMERA=14,                //' "camera"
	wSNT_BILLBOARD=15,             //' "billBoard"
	wSNT_ANIMATED_MESH=16,         //' "animatedMesh"
	wSNT_PARTICLE_SYSTEM=17,       //' "particleSystem"
	wSNT_VOLUME_LIGHT=18,          //' "volumeLight"
	//'for version <=1.4.x irr files
	wSNT_CAMERA_MAYA=19,           //' "cameraMaya"
	wSNT_CAMERA_FPS=20,            //' "cameraFPS"
	wSNT_Q3SHADER_SCENE_NODE=21,   //' "quacke3Shader"
	//'added
	wSNT_UNKNOWN=22,               //' "unknown"
	wSNT_ANY=23
}wSceneNodeType;

typedef enum
{
 	wXNT_NONE,			//(No xml node. This is usually the node if you did not read anything yet)
	wXNT_ELEMENT,
	wXNT_ELEMENT_END,
	wXNT_TEXT,
	wXNT_COMMENT,
	wXNT_CDATA,
	wXNT_UNKNOWN
}wXmlNodeType;

typedef enum
{
  	//' ASCII, file without byte order mark, or not a text file
	wTF_ASCII,
	//' UTF-8 format
	wTF_UTF8,
	//'UTF-16 format, big endian
	wTF_UTF16_BE,
	//' UTF-16 format, little endian
	wTF_UTF16_LE,
	//' UTF-32 format, big endian
	wTF_UTF32_BE,
	//'UTF-32 format, little endian
	wTF_UTF32_LE
}wTextFormat;

typedef enum
{
   	wFT_NONE,
	wFT_4PCF,
	wFT_8PCF,
	wFT_12PCF,
	wFT_16PCF,
	wFT_COUNT,
}wFilterType;

typedef enum
{
  	wSM_RECEIVE,
	wSM_CAST,
	wSM_BOTH,
	wSM_EXCLUDE,
	wSM_COUNT
}wShadowMode;

typedef enum
{
    wCF_A1R5G5B5 = 0,
    wCF_R5G6B5,
    wCF_R8G8B8,
    wCF_A8R8G8B8
}wColorFormat;

typedef enum
{
     wMAT_STAND,
     wMAT_RUN,
     wMAT_ATTACK,
     wMAT_PAIN_A,
     wMAT_PAIN_B,
     wMAT_PAIN_C,
     wMAT_JUMP,
     wMAT_FLIP,
     wMAT_SALUTE,
     wMAT_FALLBACK,
     wMAT_WAVE,
     wMAT_POINT,
     wMAT_CROUCH_STAND,
     wMAT_CROUCH_WALK,
     wMAT_CROUCH_ATTACK,
     wMAT_CROUCH_PAIN,
     wMAT_CROUCH_DEATH,
     wMAT_DEATH_FALLBACK,
     wMAT_DEATH_FALLFORWARD,
     wMAT_DEATH_FALLBACKSLOW,
     wMAT_BOOM
}wMd2AnimationType;

typedef enum
{
     wJM_NONE = 0,
     wJM_READ,
     wJM_CONTROL
}wJointMode;

typedef enum
{
   	wBSS_LOCAL,
	wBSS_GLOBAL,
	wBSS_COUNT
}wBoneSkinningSpace;

typedef enum
{
    wMFF_WS_MESH = 0,
    wMFF_COLLADA ,
    wMFF_STL
}wMeshFileFormat;

typedef enum
{
    wAMT_UNKNOWN,///Unknown animated mesh type.
    wAMT_MD2,//Quake 2 MD2 model file.
    wAMT_MD3,//Quake 3 MD3 model file.
    wAMT_OBJ,//Maya .obj static model.
    wAMT_BSP,//Quake 3 .bsp static Map.
    wAMT_3DS,//3D Studio .3ds file
    wAMT_MY3D,//My3D Mesh, the file format by Zhuck Dimitry.
    wAMT_LMTS,//Pulsar LMTools .lmts file. This Irrlicht loader was written by Jonas Petersen.
    wAMT_CSM,//Cartography Shop .csm file. This loader was created by Saurav Mohapatra.
    wAMT_OCT,//.oct file for Paul Nette's FSRad or from Murphy McCauley's Blender .oct exporter.
            //The oct file format contains 3D geometry and lightmaps and can be loaded directly by Irrlicht
    wAMT_MDL_HALFLIFE,//Halflife MDL model file.
    wAMT_SKINNED//generic skinned mesh
}wAnimatedMeshType;

typedef enum
{
    wPSM_EXACT      =0,//точный обсчет
    wPSM_ADAPTIVE   =1,//уменьшение точности обсчета в пользу производительности
    wPSM_LINEAR     =2,
    wPSM_LINEAR2    =4,
}wPhysSolverModel;

typedef enum
{
    wPFM_ZERO       =0,
    wPFM_ONE        =1
}wPhysFrictionModel;

typedef enum
{
    wPVT_RAYCAST_WORLD  =0,
    wPVT_RAYCAST_CONVEX =1
}wPhysVehicleType;

typedef enum
{
    wPVTT_STEER         =0,///Поворачиваемое колесо
    wPVTT_ACCEL         =1,///Ведущее колесо
    wPVTT_ACCEL_STEER   =2,///Поворачиваемое и ведущее
    wPVTT_ONLYWEEL      =3///Просто колесо
}wPhysVehicleTireType;

typedef enum
{
	wPRBCT_BOX=0,
	wPRBCT_SPHERE,
	wPRBCT_CAPSULE,
	wPRBCT_HULL
}wPhysRagDollBoneCollisionType;

typedef struct
{
    char* boneName;
    wPhysRagDollBoneCollisionType type;
    Float32 mass;
    Float32 coneAngle;
    Float32 minTwistAngle;
    Float32 maxTwistAngle;
    Float32 pitch;
    Float32 yaw;
    Float32 roll;
    Int32 collideWithNonImmidiateBodies;
}wPhysRagDollBoneParameters;

typedef enum
{
     wGA_UPPERLEFT=0,
     wGA_LOWERRIGHT,
     wGA_CENTER,
     wGA_SCALE
}wGuiAlignment;

typedef enum
{
    wGET_BUTTON,
    wGET_CHECK_BOX,
    wGET_COMBO_BOX,
    wGET_CONTEXT_MENU,
    wGET_MENU,
    wGET_EDIT_BOX,
    wGET_FILE_OPEN_DIALOG,
    wGET_COLOR_SELECT_DIALOG,
    wGET_IN_OUT_FADER,
    wGET_IMAGE,
    wGET_LIST_BOX,
    wGET_MESH_VIEWER,
    wGET_MESSAGE_BOX,
    wGET_MODAL_SCREEN,
    wGET_SCROLL_BAR,
    wGET_SPIN_BOX,
    wGET_STATIC_TEXT,
    wGET_TAB,
    wGET_TAB_CONTROL,
    wGET_TABLE,
    wGET_TOOL_BAR,
    wGET_TREE_VIEW,
    wGET_WINDOW,
    wGET_ELEMENT,
    wGET_ROOT,
    wGET_COUNT,
    wGET_FORCE_32_BIT
}wGuiElementType;

typedef enum
{
    //'Do not use ordering
	wGCO_NONE=0,
	//'Send a wGET_TABLE_HEADER_CHANGED message when a column header is clicked.
	wGCO_CUSTOM=1,
	//'Sort it ascending by it's ascii value like: a,b,c,...
	wGCO_ASCENDING=2,
	//'Sort it descending by it's ascii value like: z,x,y,...
	wGCO_DESCENDING=3,
	//'Sort it ascending on first click, descending on next, etc
	wGCO_FLIP_ASCENDING_DESCENDING=4,
	//'Not used as mode, only to get maximum value for this enum
	wGCO_COUNT=5
}wGuiColumnOrdering;

typedef enum
{
	wGLC_TEXT=0,
	wGLC_TEXT_HIGHLIGHT,
	wGLC_ICON,
	wGLC_ICON_HIGHLIGHT,
	wGLC_COUNT
}wGuiListboxColor;

typedef enum
{
	wGDC_3D_DARK_SHADOW = 0,
	wGDC_3D_SHADOW,
	wGDC_3D_FACE,
	wGDC_3D_HIGH_LIGHT,
	wGDC_3D_LIGHT,
	wGDC_ACTIVE_BORDER,
	wGDC_ACTIVE_CAPTION,
	wGDC_APP_WORKSPACE,
	wGDC_BUTTON_TEXT,
	wGDC_GRAY_TEXT,
	wGDC_HIGH_LIGHT,
	wGDC_HIGH_LIGHT_TEXT,
	wGDC_INACTIVE_BORDER,
	wGDC_INACTIVE_CAPTION,
	wGDC_TOOLTIP,
	wGDC_TOOLTIP_BACKGROUND,
	wGDC_SCROLLBAR,
	wGDC_WINDOW,
	wGDC_WINDOW_SYMBOL,
	wGDC_ICON,
	wGDC_ICON_HIGH_LIGHT,
	wGDC_COUNT
}wGuiDefaultColor;

typedef enum
{
	wCMC_IGNORE = 0,
	wCMC_REMOVE,
	wCMC_HIDE
}wContextMenuClose;

typedef enum
{
	wGOM_NONE=0,
	wGOM_ASCENDING,
	wGOM_DESCENDING,
	wGOM_COUNT
}wGuiOrderingMode;

typedef enum
{
   	wGTDF_ROWS = 1,
	wGTDF_COLUMNS = 2,
	wGTDF_ACTIVE_ROW = 4,
	wGTDF_COUNT
}wGuiTableDrawFlags;

typedef enum
{
	wGSS_WINDOWS_CLASSIC=0,
	wGSS_WINDOWS_METALLIC,
	wGSS_BURNING_SKIN,
	wGSS_UNKNOWN,
	wGSS_COUNT
}wGuiSkinSpace;

typedef enum
{
	wGDS_SCROLLBAR_SIZE = 0,
	wGDS_MENU_HEIGHT,
	wGDS_WINDOW_BUTTON_WIDTH,
	wGDS_CHECK_BOX_WIDTH,
	wGDS_MESSAGE_BOX_WIDTH,
	wGDS_MESSAGE_BOX_HEIGHT,
	wGDS_BUTTON_WIDTH,
	wGDS_BUTTON_HEIGHT,
	wGDS_TEXT_DISTANCE_X,
	wGDS_TEXT_DISTANCE_Y,
	wGDS_TITLEBARTEXT_DISTANCE_X,
	wGDS_TITLEBARTEXT_DISTANCE_Y,
	wGDS_MESSAGE_BOX_GAP_SPACE,
	wGDS_MESSAGE_BOX_MIN_TEXT_WIDTH,
	wGDS_MESSAGE_BOX_MAX_TEXT_WIDTH,
	wGDS_MESSAGE_BOX_MIN_TEXT_HEIGHT,
	wGDS_MESSAGE_BOX_MAX_TEXT_HEIGHT,
	wGDS_BUTTON_PRESSED_IMAGE_OFFSET_X,
	wGDS_BUTTON_PRESSED_IMAGE_OFFSET_Y,
	wGDS_BUTTON_PRESSED_TEXT_OFFSET_X,
	wGDS_BUTTON_PRESSED_TEXT_OFFSET_Y,
	wGDS_BUTTON_PRESSED_SPRITE_OFFSET_X,
	wGDS_BUTTON_PRESSED_SPRITE_OFFSET_Y,
	wGDS_COUNT
}wGuiDefaultSize;

typedef enum
{
	wGDT_MSG_BOX_OK = 0,
	wGDT_MSG_BOX_CANCEL=1,
	wGDT_MSG_BOX_YES=2,
	wGDT_MSG_BOX_NO=3,
	wGDT_WINDOW_CLOSE=4,
	wGDT_WINDOW_MAXIMIZE=5,
	wGDT_WINDOW_MINIMIZE=6,
	wGDT_WINDOW_RESTORE=7,
	wGDT_COUNT=8
}wGuiDefaultText;

typedef enum
{
	wGDF_DEFAULT=0,
	wGDF_BUTTON,
	wGDF_WINDOW,
	wGDF_MENU,
	wGDF_TOOLTIP,
	wGDF_COUNT
}wGuiDefaultFont;

typedef enum
{
    //'The button is not pressed
	wGBS_BUTTON_UP=0,
	//'The button is currently pressed down
	wGBS_BUTTON_DOWN=1,
	//'The mouse cursor is over the button
	wGBS_BUTTON_MOUSE_OVER=2,
	//'The mouse cursor is not over the button
	wGBS_BUTTON_MOUSE_OFF=3,
	//'The button has the focus
	wGBS_BUTTON_FOCUSED=4,
	//'The button doesn't have the focus
	wGBS_BUTTON_NOT_FOCUSED=5,
	//'not used, counts the number of enumerated items
	wGBS_COUNT
}wGuiButtonState;

typedef enum
{
	wGDI_WINDOW_MAXIMIZE = 0,
	wGDI_WINDOW_RESTORE,
	wGDI_WINDOW_CLOSE,
	wGDI_WINDOW_MINIMIZE,
	wGDI_WINDOW_RESIZE,
	wGDI_CURSOR_UP,
	wGDI_CURSOR_DOWN,
	wGDI_CURSOR_LEFT,
	wGDI_CURSOR_RIGHT,
	wGDI_MENU_MORE,
	wGDI_CHECK_BOX_CHECKED,
	wGDI_DROP_DOWN,
	wGDI_SMALL_CURSOR_UP,
	wGDI_SMALL_CURSOR_DOWN,
	wGDI_RADIO_BUTTON_CHECKED,
	wGDI_MORE_LEFT,
	wGDI_MORE_RIGHT,
	wGDI_MORE_UP,
	wGDI_MORE_DOWN,
	wGDI_EXPAND,
	wGDI_COLLAPSE,
	wGDI_FILE,
	wGDI_DIRECTORY,
	wGDI_COUNT
}wGuiDefaultIcon;

typedef enum
{
    wLT_POINT = 0,
    wLT_SPOT,
    wLT_DIRECTIONAL
}wLightType;

typedef enum
{
    wDM_OFF = 0,
    wDM_BBOX = 1,
    wDM_NORMALS = 2,
    wDM_SKELETON = 4,
    wDM_MESH_WIRE_OVERLAY = 8,
    wDM_HALF_TRANSPARENCY = 16,
    wDM_BBOX_BUFFERS = 32,
    wDM_FULL = 0xffffffff
}wDebugMode;

typedef enum
{
    wTPS_9 = 9,                    //' patch size of 9, at most, use 4 levels of detail with this patch size.
    wTPS_17 = 17,                 // ' patch size of 17, at most, use 5 levels of detail with this patch size.
    wTPS_33 = 33,                 // ' patch size of 33, at most, use 6 levels of detail with this patch size.
    wTPS_65 = 65,                  //' patch size of 65, at most, use 7 levels of detail with this patch size.
    wTPS_129 = 129               //' patch size of 129, at most, use 8 levels of detail with this patch size.
}wTerrainPatchSize;

typedef enum
{
    wTTE_TOP=0,
    wTTE_BOTTOM,
    wTTE_LEFT,
    wTTE_RIGHT
}wTiledTerrainEdge;

typedef enum
{
   wPEQ_CRUDE=0,
   wPEQ_FAST=1,
   wPEQ_DEFAULT=2,
   wPEQ_GOOD=3,
   wPEQ_BEST=4
}wPostEffectQuality;

typedef enum
{
	wPEI_CUSTOM = 0, // () Do not use - used internally
	wPEI_DIRECT=1, // () Does nothing to the input - useful for anti-aliasing
	wPEI_PUNCH=2, // (dx,dy,cx,cy)Applies a punch effect to the input, centred at (cx,cy) with strength (dx,dy)
	wPEI_PIXELATE=3, // (w,h) Pixellates the input into w x h sized chunks (units in the range 0-1) Note: this does NOT use full antialiasing - only the centre pixel of each block is sampled.
	wPEI_PIXELATEBANDS=4, // (w,h,m) As PP_PIXELATE, but also darkens every other row (multiplies colour by m)
	wPEI_DARKEN=5, // (mult) Multiplies rgb by mult and maintains black (0) = black (0)
	wPEI_LIGHTEN=6, // (mult) Multiplies rgb by mult and maintains white (1) = white (1)
	wPEI_RANGE=7, // (low,high) Changes contrast so that low -> 0, high -> 1
	wPEI_POSTERIZE=8, // (levels) Reduces the colours by "rounding" them to the levels. i.e. levels = 2 means each channel is either 0.0 or 1.0. levels = 3 means 0.0, 0.5 or 1.0, etc.
	wPEI_INVERT=9, // () Inverts the rgba channels
	wPEI_TINT=10, // (r,g,b,m) Converts the pixels to monochrome using a simple weighting then applies a tint. maintains black = black, white = white. Finally merges with original (m=0 -> no change, m=1 -> full tint)
	wPEI_CURVES=11, // (r,g,b) Applies a colour curve to the rgb channels, maintaining black = black and white = white. Values of 1.0 are no change, > 1.0 raises colour presence
	wPEI_GREYSCALE=12, // (power) = PP_TINT( power, power, power, 1.0 )
	wPEI_SEPIA=13, // () = PP_TINT( 2.0, 1.0, 0.7, 1.0 )
	wPEI_SATURATE=14, // (amount) = PP_TINT( 1.0, 1.0, 1.0, 1.0-amount ) 1.0 = no change, > 1.0 = saturate, < 1.0 = desaturate. Negative values will invert the colours, but not the luminosiry, can make interesting effects
	wPEI_VIGNETTE=15, // (power,start,end)Applies a black vignette around the input. Set power to 2 for a standard circle, or a higher value for a more rectangular shape. Lower values will make star-like patterns.
	wPEI_NOISE=16, // (amount) Adds psudo-random monochromatic noise to each pixel. Each frame uses different random numbers. Random function is crude.
	wPEI_COLORNOISE=17, // (amount) As above, but r,g,b channels are seperate
	wPEI_PURENOISE=18, // (amount) As PP_NOISE, but ignores input. Renders as if on a grey background. Useful as a generator
	wPEI_HBLUR=19, // (distance) Applies a simple horizontal linear blur filter using 5 samples
	wPEI_VBLUR=20, // (distance) As PP_HBLUR but vertical
	wPEI_HSHARPEN=21, // (d,mult) A horizontal sharpen; raises contrast around edges
	wPEI_VSHARPEN=22, // (d,mult) As PP_HSHARPEN but vertical
	wPEI_BIBLUR=23, // (dx,dy) A simultaneous horizontal &amp; vertical blur. A better effect, but cannot take advantage of parallel processing, so usually slower than HBLUR+VBLUR.
	wPEI_HBLURDOFFAR=24, // (near,far,d) Applies a depth of field, blurier further away. Needs depth in alpha channel, like PP_DEPTH and PP_OCCLUSION
	wPEI_VBLURDOFFAR=25, // (near,far,d) As PP_HBLURDOFFAR but vertical
	wPEI_HBLURDOFNEAR=26,// (near,far,d) Applies a depth of field, blurier close-up. Needs depth in alpha channel, like PP_DEPTH and PP_OCCLUSION
	wPEI_VBLURDOFNEAR=27,// (near,far,d) As PP_HBLURDOFNEAR but vertical
	wPEI_LINEARBLUR=28, // (dx,dy) As PP_HBLUR, but applies along the line dx,dy
	wPEI_RADIALBLUR=29, // (cx,cy,dx,dy)Applies a radial blur from (cx,cy) with a size of dx at (cx+1,cy) and dy at (cx,cy+1)
	wPEI_RADIALBEAM=30, // (cx,cy,dx,dy)Applies a radial blur from (cx,cy) with a size of dx at (cx+1,cy) and dy at (cx,cy+1) with an additive effect to make beams
	wPEI_ROTATIONALBLUR=31, // (cx,cy,dx,dy)Applies a rotational blur around (cx,cy)
	wPEI_OVERLAY=32, // (mult) Output = Texture1 + Texture2 * mult
	wPEI_OVERLAYNEG=33, // (mult) Output = Texture1 - (1 - Texture2) * mult !WARNING: Due to no EMT_TRANSPARENT_SUBTRACT_COLOR option, this uses a SLOW method. Will cause a performance hit if Texture2 is non-static.
	wPEI_MOTIONBLUR=34, // (sharp) Retains a memory of past renders, low sharp = long trails. Recommended sharp ~= 0.1
	wPEI_HAZE=35, // (dist,opac,speed,scale) Adds a heat haze, using the red channel of texture2 as heat, with 0 = cold, 1 = hot
	wPEI_HAZEDEPTH=36, // (dist,opac,speed,scale) Adds a heat haze, using the red channel of texture2 as heat, with 0 = cold, 1 = hot and the green channel as the z-depth
	wPEI_DEPTH=37, // () Renders depth (alpha) as greyscale, lighter = further away
	wPEI_OCCLUSION=38, // (mult) Taken from the Irrlicht forums and heavily mutilated, uses alpha channel to judge depth

	// Composite effects

	wPEI_BLUR=39, // (distance) = HBLUR(distance) + VBLUR(distance)
	wPEI_SHARPEN=40,// (distance,mult) = HSHARPEN(distance,mult) + VSHARPEN(distance,mult)
	wPEI_BLURDOFFAR=41, // (near,far,distance) = HBLURDOFFAR(near,far,distance) + VBLURDOFFAR(near,far,distance)
	wPEI_BLURDOFNEAR=42, // (near,far,distance) = HBLURDOFNEAR(near,far,distance) + VBLURDOFNEAR(near,far,distance)
	wPEI_BLURDOF=43, // (b1,f1,f2,b2,dist) = BLURDOFFAR(f2,b2,dist) + HBLURDOFNEAR(b1,f1,dist) + VBLURDOFNEAR(b1,f1,dist)
	wPEI_BLOOM=44, // (cut,distance,light) = LIGHTEN(cut) + BLUR(distance) + OVERLAY(light)
	wPEI_GLOOM=45, // (cut,distance,dark) = DARKEN(cut) + BLUR(distance) + OVERLAYNEG(dark) !WARNING: uses OVERLAYNEG which is slow!
	wPEI_NIGHTVISION=46, // (max,distance,noise) = RANGE(-0.5,max) + BLUR(distance) + NOISE(noise) + TINT(0.2,2.0,0.5)
	wPEI_MONITOR=47, // (vig,bulge,noise,sat,pixel,rowm) = TINT(1.0,0.9,0.8,1.0-sat) + NOISE(noise) + PIXELATEBANDS(pixel,pixel,rowm) + PUNCH(bulge,bulge,0.5,0.5) + VIGNETTE(4.0,0.0,1.0/vig)
	wPEI_WATERCOLOR=48, // (bright,blur,levels,sharp,mult,noise) = NOISE( noise ) + CURVES( bright, bright, bright ) + BIBLUR( blur, blur ) + POSTERIZE( levels ) + SHARPEN( sharp, mult )
    wPEI_COUNT=49 //Not for use!!!
}wPostEffectId;

typedef struct tag_wBillboard
{
    wVector3f  Position;
    wVector2f Size;
    Float32 Roll;
    wVector3f  Axis;
    Int32 HasAxis;
    Int32 sColor;//not for use
    UInt32 alpha;
    UInt32 red;
    UInt32 green;
    UInt32 blue;

    UInt32 vertexIndex;
    struct tag_wBillboard* sprev;
    struct tag_wBillboard* snext;
}wBillboard;

typedef enum {
	wCFC_BLACK,
	wCFC_BLUE,
	wCFC_GREEN,
	wCFC_CYAN,
	wCFC_RED,
	wCFC_MAGENTA,
	wCFC_BROWN,
	wCFC_GREY,
	wCFC_DARKGREY,
	wCFC_LIGHTBLUE,
	wCFC_LIGHTGREEN,
	wCFC_LIGHTCYAN,
	wCFC_LIGHTRED,
	wCFC_LIGHTMAGENTA,
	wCFC_YELLOW,
	wCFC_WHITE,
	wCFC_COUNT
}wConsoleFontColor;

typedef enum
{
	wCBC_BLACK,
	wCBC_BLUE,
	wCBC_GREEN,
	wCBC_CYAN,
	wCBC_RED,
	wCBC_MAGENTA,
	wCBC_YELLOW,
	wCBC_WHITE,
	wCBC_COUNT
}wConsoleBackColor;

typedef enum
{
    wWD_SUNDAY,
    wWD_MONDAY,
    wWD_TUESDAY,
    wWD_WEDNESDAY,
    wWD_THURSDAY,
    wWD_FRIDAY,
    wWD_SATURDAY
}wWeekDay;

typedef struct
{
    UInt32 Day;
    UInt32 Hour;
    bool IsDST;
    UInt32 Minute;
    UInt32 Month;
    UInt32 Second;
    wWeekDay Weekday;
    Int32 Year;
    UInt32 Yearday;

}wRealTimeDate;

typedef struct
{
    bool isEnablePitch;    // вращение по X
    bool isEnableYaw;      // вращение по Y
    bool isEnableRoll;     //вращение по Z
} wBillboardAxisParam;

typedef enum
{
    wFAT_ZIP, //A PKZIP archive.
    wFAT_GZIP,//A gzip archive.
    wFAT_FOLDER,//A virtual directory.
    wFAT_PAK,//An ID Software PAK archive.
    wFAT_NPK,//A Nebula Device archive.
    wFAT_TAR,//A Tape ARchive.
    wFAT_WAD,//A wad Archive, Quake2, Halflife.
    wFAT_UNKNOWN//The type of this archive is unknown.
}wFileArchiveType;

#ifdef __cplusplus
typedef struct
{
	//! Type of the device.
	/** This setting decides the windowing system used by the device, most device types are native
	to a specific operating system and so may not be available.
	wDT_WIN32 is only available on Windows desktops,
	wDT_WINCE is only available on Windows mobile devices,
	wDT_COCOA is only available on Mac OSX,
	wDT_X11 is available on Linux, Solaris, BSD and other operating systems which use X11,
	wDT_SDL is available on most systems if compiled in,
	wDT_CONSOLE is usually available but can only render to text,
	wDT_BEST will select the best available device for your operating system.
	Default: wDT_BEST. */

	wDeviceTypes DeviceType=wDT_BEST;

	//! Type of video driver used to render graphics.
	/** This can currently be wDT_NULL, wDT_SOFTWARE,
	wDT_BURNINGSVIDEO, wDT_DIRECT3D9, and wDT_OPENGL.*/
	wDriverTypes DriverType=wDRT_BURNINGS_VIDEO;

	//! Size of the window or the video mode in fullscreen mode. Default: 800x600
	wVector2u WindowSize=wDEFAULT_SCREENSIZE;

	//! Position of the window on-screen. Default: (-1, -1) or centered.
	wVector2i WindowPosition={-1,-1};

	//! Minimum Bits per pixel of the color buffer in fullscreen mode. Ignored if windowed mode. Default: 32.
	UInt8 Bits=32;

	//! Minimum Bits per pixel of the depth buffer. Default: 24.
	UInt8 ZBufferBits=24;

	//! Should be set to true if the device should run in fullscreen.
	/** Otherwise the device runs in windowed mode. Default: false. */
	bool Fullscreen=false;

	//! Specifies if the stencil buffer should be enabled.
	/** Set this to true, if you want the engine be able to draw
	stencil buffer shadows. Note that not all drivers are able to
	use the stencil buffer, hence it can be ignored during device
	creation. Without the stencil buffer no shadows will be drawn.
	Default: true. */
	bool Stencilbuffer=true;

	//! Specifies vertical synchronization.
	/** If set to true, the driver will wait for the vertical
	retrace period, otherwise not. May be silently ignored.
	Default: false */
	bool Vsync=false;

	//! Specifies if the device should use fullscreen anti aliasing
	/** Makes sharp/pixelated edges softer, but requires more
	performance. Also, 2D elements might look blurred with this
	switched on. The resulting rendering quality also depends on
	the hardware and driver you are using, your program might look
	different on different hardware with this. So if you are
	writing a game/application with AntiAlias switched on, it would
	be a good idea to make it possible to switch this option off
	again by the user.
	The value is the maximal antialiasing factor requested for
	the device. The creation method will automatically try smaller
	values if no window can be created with the given value.
	Value one is usually the same as 0 (disabled), but might be a
	special value on some platforms. On D3D devices it maps to
	NONMASKABLE.
	Default value: 0 - disabled */
	wAntiAliasingMode AntiAlias=wAAM_OFF;

	//! Flag to enable proper sRGB and linear color handling
	/** In most situations, it is desirable to have the color handling in
	non-linear sRGB color space, and only do the intermediate color
	calculations in linear RGB space. If this flag is enabled, the device and
	driver try to assure that all color input and output are color corrected
	and only the internal color representation is linear. This means, that
	the color output is properly gamma-adjusted to provide the brighter
	colors for monitor display. And that blending and lighting give a more
	natural look, due to proper conversion from non-linear colors into linear
	color space for blend operations. If this flag is enabled, all texture colors
	(which are usually in sRGB space) are correctly displayed. However vertex colors
	and other explicitly set values have to be manually encoded in linear color space.
	Default value: false. */
	bool HandleSRGB=false;

	//! Whether the main framebuffer uses an alpha channel.
	/** In some situations it might be desirable to get a color
	buffer with an alpha channel, e.g. when rendering into a
	transparent window or overlay. If this flag is set the device
	tries to create a framebuffer with alpha channel.
	If this flag is set, only color buffers with alpha channel
	are considered. Otherwise, it depends on the actual hardware
	if the colorbuffer has an alpha channel or not.
	Default value: false */
	bool WithAlphaChannel=false;

	//! Whether the main framebuffer uses doublebuffering.
	/** This should be usually enabled, in order to avoid render
	artifacts on the visible framebuffer. However, it might be
	useful to use only one buffer on very small devices. If no
	doublebuffering is available, the drivers will fall back to
	single buffers. Default value: true */
	bool Doublebuffer=true;

	//! Specifies if the device should ignore input events
	/** This is only relevant when using external I/O handlers.
	External windows need to take care of this themselves.
	Currently only supported by X11.
	Default value: false */
	bool IgnoreInput=false;

	//! Specifies if the device should use stereo buffers
	/** Some high-end gfx cards support two framebuffers for direct
	support of stereoscopic output devices. If this flag is set the
	device tries to create a stereo context.
	Currently only supported by OpenGL.
	Default value: false */
	bool Stereobuffer=false;

	//! Specifies if the device should use high precision FPU setting
	/** This is only relevant for DirectX Devices, which switch to
	low FPU precision by default for performance reasons. However,
	this may lead to problems with the other computations of the
	application. In this case setting this flag to true should help
	- on the expense of performance loss, though.
	Default value: false */
	bool HighPrecisionFPU=false;

	//! Window Id.
	/** If this is set to a value other than 0, the WorldSim3D Engine
	will be created in an already existing window.
	For Windows, set this to the HWND of the window you want.
	For iOS, assign UIView to this variable.
	The windowSize and FullScreen options will be ignored when using
	the WindowId parameter. Default this is set to 0.
	To make WorldSim3D run inside the custom window, you still will
	have to draw WorldSim3D on your own.

	Instead of this, you can also simply use your own message loop
	using GetMessage, DispatchMessage and whatever. Calling
	wEngineRunning() will cause WorldSim3D to dispatch messages
	internally too.  You need not call wEngineRunning() if you want to
	do your own message dispatching loop, but WorldSim3D will not be
	able to fetch user input then and you have to do it on your own
	using the window messages, DirectInput, or whatever. Also,
	you'll have to increment the WorldSim3D timer.
	However, there is no need to draw the picture this often. Just
	do it how you like. */
	void* WindowId=0;

	//! Specifies the logging level used in the logging interface.
	/** The default value is wLL_INFORMATION. You can access the logger interface
	later on from the WorldSim3D and set another level.
	But if you need more or less logging information already from device creation,
	then you have to change it here.
	*/
	wLoggingLevel LoggingLevel=wLL_INFORMATION;

	//! Allows to select which graphic card is used for rendering when more than one card is in the system.
	/** So far only supported on D3D */
	UInt32 DisplayAdapter=0;

	//! Create the driver multithreaded.
	/** Default is false. Enabling this can slow down your application.
	Note that this does _not_ make WorldSim3D threadsafe, but only the underlying driver-API for the graphiccard.
	So far only supported on D3D. */
	bool DriverMultithreaded=false;

	//! Enables use of high performance timers on Windows platform.
	/** When performance timers are not used, standard GetTickCount()
	is used instead which usually has worse resolution, but also less
	problems with speed stepping and other techniques.
	*/
	bool UsePerformanceTimer=true;

	//! Define some private data storage.
	/** Used when platform devices need access to OS specific data structures etc.
	This is only used for Android at th emoment in order to access the native
	Java RE. */

	void *PrivateData=0;
	//! Set the path where default-shaders to simulate the fixed-function pipeline can be found.
	/** This is about the shaders which can be found in media/Shaders by default. It's only necessary
	to set when using OGL-ES 2.0 */
	const char* OGLES2ShaderPath="../../Assets/Shaders";

} wEngineCreationParameters;
#else
typedef struct
{
	wDeviceTypes DeviceType;
	wDriverTypes DriverType;
	wVector2u WindowSize;
	wVector2i WindowPosition;
	UInt8 Bits;
	UInt8 ZBufferBits;
	bool Fullscreen;
	bool Stencilbuffer;
	bool Vsync;
	wAntiAliasingMode AntiAlias;
	bool HandleSRGB;
	bool WithAlphaChannel;
	bool Doublebuffer;
	bool IgnoreInput;
	bool Stereobuffer;
	bool HighPrecisionFPU;
	void* WindowId;
	wLoggingLevel LoggingLevel;
	UInt32 DisplayAdapter;
	bool DriverMultithreaded;
	bool UsePerformanceTimer;
	void *PrivateData;
	const char* OGLES2ShaderPath;

} wEngineCreationParameters;
#endif // __cplusplus

typedef enum
{
    wL_RU=0,
    wL_EN=1
}wLanguage;

///// OPEN AL  ENUMS //////
typedef enum
{
	wSET_NULL=0,
	wSET_EAX_REVERB,
	wSET_REVERB,
	wSET_CHORUS,
	wSET_DISTORTION,
	wSET_ECHO,
	wSET_FLANGER,
	wSET_FREQUENCY_SHIFTER,
	wSET_VOCAL_MORPHER,
	wSET_PITCH_SHIFTER,
	wSET_RING_MODULATOR,
	wSET_AUTOWAH,
	wSET_COMPRESSOR,
	wSET_EQUALIZER,
	wSET_COUNT
}wSoundEffectType;

typedef enum
{
    wSFT_NULL=0,
	wSFT_LOWPASS,
	wSFT_HIGHPASS,
	wSFT_BANDPASS,
	wSFT_COUNT
}wSoundFilterType;

typedef enum
{
	wAF_8BIT_MONO,
	wAF_8BIT_STEREO,
	wAF_16BIT_MONO,
	wAF_16BIT_STEREO
}wAudioFormats;

//! Contains parameters for the EAX Reverb Effect.  This effect tries to simulate how sound behaves in different environments.
typedef struct tag_wEaxReverbParameters
{
#ifdef __cplusplus
	tag_wEaxReverbParameters(
		Float32 density = 1.0f,
		Float32 diffusion = 1.0f,
		Float32 gain = 0.32f,
		Float32 gainHF = 0.89f,
		Float32 gainLF = 0.0f,
		Float32 decayTime = 1.49f,
		Float32 decayHFRatio = 0.83f,
		Float32 decayLFRatio = 1.0f,
		Float32 reflectionsGain = 0.05f,
		Float32 reflectionsDelay = 0.007f,
		Float32 reflectionsPanX=0.0f,
		Float32 reflectionsPanY=0.0f,
		Float32 reflectionsPanZ=0.0f,
		Float32 lateReverbGain = 1.26f,
		Float32 lateReverbDelay = 0.011f,
		Float32 lateReverbPanX=0.0f,
		Float32 lateReverbPanY=0.0f,
		Float32 lateReverbPanZ=0.0f,
		Float32 echoTime = 0.25f,
		Float32 echoDepth = 0.0f,
		Float32 modulationTime = 0.25f,
		Float32 modulationDepth = 0.0f,
		Float32 airAbsorptionGainHF = 0.994f,
		Float32 hFReference = 5000.0f,
		Float32 lFReference = 250.0f,
		Float32 roomRolloffFactor = 0.0f,
		bool decayHFLimit = true) :
		Density(density), Diffusion(diffusion), Gain(gain), GainHF(gainHF), GainLF(gainLF),
		DecayTime(decayTime), DecayHFRatio(decayHFRatio), DecayLFRatio(decayLFRatio),
		ReflectionsGain(reflectionsGain), ReflectionsDelay(reflectionsDelay),
		ReflectionsPanX(reflectionsPanX),ReflectionsPanY(reflectionsPanY),ReflectionsPanZ(reflectionsPanZ),LateReverbGain(lateReverbGain),
		LateReverbDelay(lateReverbDelay), LateReverbPanX(lateReverbPanX),LateReverbPanY(lateReverbPanY),LateReverbPanZ(lateReverbPanZ),
		EchoTime(echoTime), EchoDepth(echoDepth),ModulationTime(modulationTime),
		ModulationDepth(modulationDepth), AirAbsorptionGainHF(airAbsorptionGainHF),
		HFReference(hFReference), LFReference(lFReference),
		RoomRolloffFactor(roomRolloffFactor), DecayHFLimit(decayHFLimit) { }
#endif // __cplusplus

		//! Reverb Modal Density controls the coloration of the late reverb. Lowering the value adds
		//! more coloration to the late reverb.
		//! Range: 0.0 to 1.0
		Float32 Density;

		//! The Reverb Diffusion property controls the echo density in the reverberation decay. It's set by
		//! default to 1.0, which provides the highest density. Reducing diffusion gives the reverberation a
		//! more "grainy" character that is especially noticeable with percussive sound sources. If you set a
		//! diffusion value of 0.0, the later reverberation sounds like a succession of distinct echoes.
		//! Range: 0.0 to 1.0
		Float32 Diffusion;

		//! The Reverb Gain property is the master volume control for the reflected sound (both early
		//! reflections and reverberation) that the reverb effect adds to all sound sources. It sets the
		//! maximum amount of reflections and reverberation added to the final sound mix. The value of the
		//! Reverb Gain property ranges from 1.0 (0db) (the maximum amount) to 0.0 (-100db) (no reflected
		//! sound at all).
		//! Range: 0.0 to 1.0
		Float32 Gain;

		//! The Reverb Gain HF property further tweaks reflected sound by attenuating it at high frequencies.
		//! It controls a low-pass filter that applies globally to the reflected sound of all sound sources
		//! feeding the particular instance of the reverb effect. The value of the Reverb Gain HF property
		//! ranges from 1.0 (0db) (no filter) to 0.0 (-100db) (virtually no reflected sound). HF Reference sets
		//! the frequency at which the value of this property is measured.
		//! Range: 0.0 to 1.0
		Float32 GainHF;

		//! The Reverb Gain LF property further tweaks reflected sound by attenuating it at low frequencies.
		//! It controls a high-pass filter that applies globally to the reflected sound of all sound sources
		//! feeding the particular instance of the reverb effect. The value of the Reverb Gain LF property
		//! ranges from 1.0 (0db) (no filter) to 0.0 (-100db) (virtually no reflected sound). LF Reference sets
		//! the frequency at which the value of this property is measured.
		//! Range: 0.0 to 1.0
		Float32 GainLF;

		//! The Decay Time property sets the reverberation decay time. It ranges from 0.1 (typically a small
		//! room with very dead surfaces) to 20.0 (typically a large room with very live surfaces).
		//! Range: 0.1 to 20.0
		Float32 DecayTime;

		//! The Decay HF Ratio property adjusts the spectral quality of the Decay Time parameter. It is the
		//! ratio of high-frequency decay time relative to the time set by Decay Time. The Decay HF Ratio
		//! value 1.0 is neutral: the decay time is equal for all frequencies. As Decay HF Ratio increases
		//! above 1.0, the high-frequency decay time increases so it's longer than the decay time at mid
		//! frequencies. You hear a more brilliant reverberation with a longer decay at high frequencies. As
		//! the Decay HF Ratio value decreases below 1.0, the high-frequency decay time decreases so it's
		//! shorter than the decay time of the mid frequencies. You hear a more natural reverberation.
		//! Range: 0.1 to 20.0
		Float32 DecayHFRatio;

		//! The Decay LF Ratio property adjusts the spectral quality of the Decay Time parameter. It is the
		//! ratio of low-frequency decay time relative to the time set by Decay Time. The Decay LF Ratio
		//! value 1.0 is neutral: the decay time is equal for all frequencies. As Decay LF Ratio increases
		//! above 1.0, the low-frequency decay time increases so it's longer than the decay time at mid
		//! frequencies. You hear a more booming reverberation with a longer decay at low frequencies. As
		//! the Decay LF Ratio value decreases below 1.0, the low-frequency decay time decreases so it's
		//! shorter than the decay time of the mid frequencies. You hear a more tinny reverberation.
		//! Range: 0.1 to 20.0
		Float32 DecayLFRatio;

		//! The Reflections Gain property controls the overall amount of initial reflections relative to the Gain
		//! property. (The Gain property sets the overall amount of reflected sound: both initial reflections
		//! and later reverberation.) The value of Reflections Gain ranges from a maximum of 3.16 (+10 dB)
		//! to a minimum of 0.0 (-100 dB) (no initial reflections at all), and is corrected by the value of the
		//! Gain property. The Reflections Gain property does not affect the subsequent reverberation decay.
		//! Range: 0.0 to 3.16
		Float32 ReflectionsGain;

		//! The Reflections Delay property is the amount of delay between the arrival time of the direct path
		//! from the source to the first reflection from the source. It ranges from 0 to 300 milliseconds. You
		//! can reduce or increase Reflections Delay to simulate closer or more distant reflective surfaces—
		//! and therefore control the perceived size of the room.
		//! Range: 0.0 to 0.3
		Float32 ReflectionsDelay;

		//! The Reflections Pan property is a 3D vector that controls the spatial distribution of the cluster of
		//! early reflections. The direction of this vector controls the global direction of the reflections, while
		//! its magnitude controls how focused the reflections are towards this direction.
		//! It is important to note that the direction of the vector is interpreted in the coordinate system of the
		//! user, without taking into account the orientation of the virtual listener. For instance, assuming a
		//! four-point loudspeaker playback system, setting Reflections Pan to (0, 0, 0.7) means that the
		//! reflections are panned to the front speaker pair, whereas as setting of (0, 0, -0.7) pans the
		//! reflections towards the rear speakers. These vectors follow the a left-handed co-ordinate system,
		//! unlike OpenAL uses a right-handed co-ordinate system.
		//! If the magnitude of Reflections Pan is zero (the default setting), the early reflections come evenly
		//! from all directions. As the magnitude increases, the reflections become more focused in the
		//! direction pointed to by the vector. A magnitude of 1.0 would represent the extreme case, where
		//! all reflections come from a single direction.
		//cVector3 ReflectionsPan;
		Float32 ReflectionsPanX;
        Float32 ReflectionsPanY;
        Float32 ReflectionsPanZ;
		//! The Late Reverb Gain property controls the overall amount of later reverberation relative to the
		//! Gain property. (The Gain property sets the overall amount of both initial reflections and later
		//! reverberation.) The value of Late Reverb Gain ranges from a maximum of 10.0 (+20 dB) to a
		//! minimum of 0.0 (-100 dB) (no late reverberation at all).
		//! Range: 0.0 to 10.0
		Float32 LateReverbGain;

		//! The Late Reverb Delay property defines the begin time of the late reverberation relative to the
		//! time of the initial reflection (the first of the early reflections). It ranges from 0 to 100 milliseconds.
		//! Reducing or increasing Late Reverb Delay is useful for simulating a smaller or larger room.
		//! Range: 0.0 to 0.1
		Float32 LateReverbDelay;

		//! The Late Reverb Pan property is a 3D vector that controls the spatial distribution of the late
		//! reverb. The direction of this vector controls the global direction of the reverb, while its magnitude
		//! controls how focused the reverb are towards this direction. The details under Reflections Pan,
		//! above, also apply to Late Reverb Pan.
		//cVector3 LateReverbPan;
		Float32 LateReverbPanX;
		Float32 LateReverbPanY;
		Float32 LateReverbPanZ;

		//! Echo Time controls the rate at which the cyclic echo repeats itself along the
		//! reverberation decay. For example, the default setting for Echo Time is 250 ms. causing the echo
		//! to occur 4 times per second. Therefore, if you were to clap your hands in this type of
		//! environment, you will hear four repetitions of clap per second.
		//! Range: 0.075 to 0.25
		Float32 EchoTime;

		//! Echo Depth introduces a cyclic echo in the reverberation decay, which will be noticeable with
		//! transient or percussive sounds. A larger value of Echo Depth will make this effect more
		//! prominent.
		//! Together with Reverb Diffusion, Echo Depth will control how long the echo effect will persist along
		//! the reverberation decay. In a more diffuse environment, echoes will wash out more quickly after
		//! the direct sound. In an environment that is less diffuse, you will be able to hear a larger number
		//! of repetitions of the echo, which will wash out later in the reverberation decay. If Diffusion is set
		//! to 0.0 and Echo Depth is set to 1.0, the echo will persist distinctly until the end of the
		//! reverberation decay.
		//! Range: 0.0 to 1.0
		Float32 EchoDepth;

		//! Using these two properties below, you can create a pitch modulation in the reverberant sound. This will
		//! be most noticeable applied to sources that have tonal color or pitch. You can use this to make
		//! some trippy effects! Modulation Time controls the speed of the vibrato (rate of periodic changes in pitch).
		//! Range: 0.004 to 4.0
		Float32 ModulationTime;

		//! Modulation Depth controls the amount of pitch change. Low values of Diffusion will contribute to
		//! reinforcing the perceived effect by reducing the mixing of overlapping reflections in the
		//! reverberation decay.
		//! Range: 0.0 to 1.0
		Float32 ModulationDepth;

		//! The Air Absorption Gain HF property controls the distance-dependent attenuation at high
		//! frequencies caused by the propagation medium. It applies to reflected sound only. You can use
		//! Air Absorption Gain HF to simulate sound transmission through foggy air, dry air, smoky
		//! atmosphere, and so on. The default value is 0.994 (-0.05 dB) per meter, which roughly
		//! corresponds to typical condition of atmospheric humidity, temperature, and so on. Lowering the
		//! value simulates a more absorbent medium (more humidity in the air, for example); raising the
		//! value simulates a less absorbent medium (dry desert air, for example).
		//! Range: 0.892 to 1.0
		Float32 AirAbsorptionGainHF;

		//! The properties HF Reference and LF Reference determine respectively the frequencies at which
		//! the high-frequency effects and the low-frequency effects created by EAX Reverb properties are
		//! measured, for example Decay HF Ratio and Decay LF Ratio.
		//! Note that it is necessary to maintain a factor of at least 10 between these two reference
		//! frequencies so that low frequency and high frequency properties can be accurately controlled and
		//! will produce independent effects. In other words, the LF Reference value should be less than
		//! 1/10 of the HF Reference value.
		//! Range: 1000.0 to 20000.0
		Float32 HFReference;

		//! See HFReference.
		//! Range: 20.0 to 1000.0
		Float32 LFReference;

		//! The Room Rolloff Factor property is one of two methods available to attenuate the reflected
		//! sound (containing both reflections and reverberation) according to source-listener distance. It's
		//! defined the same way as OpenAL's Rolloff Factor, but operates on reverb sound instead of
		//! direct-path sound. Setting the Room Rolloff Factor value to 1.0 specifies that the reflected sound
		//! will decay by 6 dB every time the distance doubles. Any value other than 1.0 is equivalent to a
		//! scaling factor applied to the quantity specified by ((Source listener distance) - (Reference
		//! Distance)). Reference Distance is an OpenAL source parameter that specifies the inner border
		//! for distance rolloff effects: if the source comes closer to the listener than the reference distance,
		//! the direct-path sound isn't increased as the source comes closer to the listener, and neither is the
		//! reflected sound.
		//! The default value of Room Rolloff Factor is 0.0 because, by default, the Effects Extension reverb
		//! effect naturally manages the reflected sound level automatically for each sound source to
		//! simulate the natural rolloff of reflected sound vs. distance in typical rooms.
		//! Range: 0.0 to 10.0
		Float32 RoomRolloffFactor;

		//! When this flag is set, the high-frequency decay time automatically stays below a limit value that's
		//! derived from the setting of the property Air Absorption Gain HF. This limit applies regardless of
		//! the setting of the property Decay HF Ratio, and the limit doesn't affect the value of Decay HF
		//! Ratio. This limit, when on, maintains a natural sounding reverberation decay by allowing you to
		//! increase the value of Decay Time without the risk of getting an unnaturally long decay time at
		//! high frequencies. If this flag is set to false, high-frequency decay time isn't automatically
		//! limited.
		bool DecayHFLimit;
} wEaxReverbParameters;

//! Similar to the above EAX Reverb Effect, but has less features, meaning it may be better supported on lower end hardware.
typedef struct tag_wReverbParameters
{
#ifdef __cplusplus
    tag_wReverbParameters(
		Float32 density = 1.0f,
		Float32 diffusion = 1.0f,
		Float32 gain = 0.32f,
		Float32 gainHF = 0.89f,
		Float32 decayTime = 1.49f,
		Float32 decayHFRatio = 0.83f,
		Float32 reflectionsGain = 0.05f,
		Float32 reflectionsDelay = 0.007f,
		Float32 lateReverbGain = 1.26f,
		Float32 lateReverbDelay = 0.011f,
		Float32 airAbsorptionGainHF = 0.994f,
		Float32 roomRolloffFactor = 0.0f,
		bool decayHFLimit = true) :
		Density(density), Diffusion(diffusion), Gain(gain), GainHF(gainHF),
		DecayTime(decayTime), DecayHFRatio(decayHFRatio),
		ReflectionsGain(reflectionsGain), ReflectionsDelay(reflectionsDelay),
		LateReverbGain(lateReverbGain), LateReverbDelay(lateReverbDelay),
		AirAbsorptionGainHF(airAbsorptionGainHF), RoomRolloffFactor(roomRolloffFactor),
		DecayHFLimit(decayHFLimit) { }
#endif // __cplusplus

		//! Reverb Modal Density controls the coloration of the late reverb. Lowering the value adds more
		//! coloration to the late reverb.
		//! Range: 0.0 to 1.0
		Float32 Density;

		//! The Reverb Diffusion property controls the echo density in the reverberation decay. It's set by
		//! default to 1.0, which provides the highest density. Reducing diffusion gives the reverberation a
		//! more "grainy" character that is especially noticeable with percussive sound sources. If you set a
		//! diffusion value of 0.0, the later reverberation sounds like a succession of distinct echoes.
		//! Range: 0.0 to 1.0
		Float32 Diffusion;

		//! The Reverb Gain property is the master volume control for the reflected sound (both early
		//! reflections and reverberation) that the reverb effect adds to all sound sources. It sets the
		//! maximum amount of reflections and reverberation added to the final sound mix. The value of the
		//! Reverb Gain property ranges from 1.0 (0db) (the maximum amount) to 0.0 (-100db) (no reflected
		//! sound at all).
		//! Range: 0.0 to 1.0
		Float32 Gain;

		//! The Reverb Gain HF property further tweaks reflected sound by attenuating it at high frequencies.
		//! It controls a low-pass filter that applies globally to the reflected sound of all sound sources
		//! feeding the particular instance of the reverb effect. The value of the Reverb Gain HF property
		//! ranges from 1.0 (0db) (no filter) to 0.0 (-100db) (virtually no reflected sound).
		//! Range: 0.0 to 1.0
		Float32 GainHF;

		//! The Decay Time property sets the reverberation decay time. It ranges from 0.1 (typically a small
		//! room with very dead surfaces) to 20.0 (typically a large room with very live surfaces).
		//! Range: 0.1 to 20.0
		Float32 DecayTime;

		//! The Decay HF Ratio property sets the spectral quality of the Decay Time parameter. It is the
		//! ratio of high-frequency decay time relative to the time set by Decay Time. The Decay HF Ratio
		//! value 1.0 is neutral: the decay time is equal for all frequencies. As Decay HF Ratio increases
		//! above 1.0, the high-frequency decay time increases so it's longer than the decay time at low
		//! frequencies. You hear a more brilliant reverberation with a longer decay at high frequencies. As
		//! the Decay HF Ratio value decreases below 1.0, the high-frequency decay time decreases so it's
		//! shorter than the decay time of the low frequencies. You hear a more natural reverberation.
		//! Range: 0.1 to 2.0
		Float32 DecayHFRatio;

		//! The Reflections Gain property controls the overall amount of initial reflections relative to the Gain
		//! property. (The Gain property sets the overall amount of reflected sound: both initial reflections
		//! and later reverberation.) The value of Reflections Gain ranges from a maximum of 3.16 (+10 dB)
		//! to a minimum of 0.0 (-100 dB) (no initial reflections at all), and is corrected by the value of the
		//! Gain property. The Reflections Gain property does not affect the subsequent reverberation
		//! decay.
		//! Range: 0.0 to 3.16
		Float32 ReflectionsGain;

		//! The Reflections Delay property is the amount of delay between the arrival time of the direct path
		//! from the source to the first reflection from the source. It ranges from 0 to 300 milliseconds. You
		//! can reduce or increase Reflections Delay to simulate closer or more distant reflective surfaces—
		//! and therefore control the perceived size of the room.
		//! Range: 0.0 to 0.3
		Float32 ReflectionsDelay;

		//! The Late Reverb Gain property controls the overall amount of later reverberation relative to the
		//! Gain property. (The Gain property sets the overall amount of both initial reflections and later
		//! reverberation.) The value of Late Reverb Gain ranges from a maximum of 10.0 (+20 dB) to a
		//! minimum of 0.0 (-100 dB) (no late reverberation at all).
		//! Range: 0.0 to 10.0
		Float32 LateReverbGain;

		//! The Late Reverb Delay property defines the begin time of the late reverberation relative to the
		//! time of the initial reflection (the first of the early reflections). It ranges from 0 to 100 milliseconds.
		//! Reducing or increasing Late Reverb Delay is useful for simulating a smaller or larger room.
		//! Range: 0.0 to 0.1
		Float32 LateReverbDelay;

		//! The Air Absorption Gain HF property controls the distance-dependent attenuation at high
		//! frequencies caused by the propagation medium. It applies to reflected sound only. You can use
		//! Air Absorption Gain HF to simulate sound transmission through foggy air, dry air, smoky
		//! atmosphere, and so on. The default value is 0.994 (-0.05 dB) per meter, which roughly
		//! corresponds to typical condition of atmospheric humidity, temperature, and so on. Lowering the
		//! value simulates a more absorbent medium (more humidity in the air, for example); raising the
		//! value simulates a less absorbent medium (dry desert air, for example).
		//! Range: 0.892 to 1.0
		Float32 AirAbsorptionGainHF;

		//! The Room Rolloff Factor property is one of two methods available to attenuate the reflected
		//! sound (containing both reflections and reverberation) according to source-listener distance. It's
		//! defined the same way as OpenAL's Rolloff Factor, but operates on reverb sound instead of
		//! direct-path sound. Setting the Room Rolloff Factor value to 1.0 specifies that the reflected sound
		//! will decay by 6 dB every time the distance doubles. Any value other than 1.0 is equivalent to a
		//! scaling factor applied to the quantity specified by ((Source listener distance) - (Reference
		//! Distance)). Reference Distance is an OpenAL source parameter that specifies the inner border
		//! for distance rolloff effects: if the source comes closer to the listener than the reference distance,
		//! the direct-path sound isn't increased as the source comes closer to the listener, and neither is the
		//! reflected sound.
		//! The default value of Room Rolloff Factor is 0.0 because, by default, the Effects Extension reverb
		//! effect naturally manages the reflected sound level automatically for each sound source to
		//! simulate the natural rolloff of reflected sound vs. distance in typical rooms.
		//! Range: 0.0 to 10.0
		Float32 RoomRolloffFactor;

		//! When this flag is set, the high-frequency decay time automatically stays below a limit value that's
		//! derived from the setting of the property Air Absorption Gain HF. This limit applies regardless of
		//! the setting of the property Decay HF Ratio, and the limit doesn't affect the value of Decay HF
		//! Ratio. This limit, when on, maintains a natural sounding reverberation decay by allowing you to
		//! increase the value of Decay Time without the risk of getting an unnaturally long decay time at
		//! high frequencies. If this flag is set to false, high-frequency decay time isn't automatically
		//! limited.
		bool DecayHFLimit;
} wReverbParameters;

//! The chorus effect essentially replays the input audio accompanied by another slightly delayed version of the signal, creating a "doubling" effect.

typedef enum
{
    wCWF_SINUSOID,
    wCWF_TRIANGLE,
    wCWF_COUNT
} ChorusWaveform;

typedef struct tag_wChorusParameters
{
#ifdef __cplusplus
	tag_wChorusParameters(
		ChorusWaveform waveform = wCWF_TRIANGLE,
		Int32 phase = 90,
		Float32 rate = 1.1f,
		Float32 depth = 0.1f,
		Float32 feedback = 0.25f,
		Float32 delay = 0.016f) :
		Waveform(waveform), Phase(phase), Rate(rate), Depth(depth), Feedback(feedback),
		Delay(delay) { }
#endif // __cplusplus

		//! This property sets the waveform shape of the LFO that controls the delay time of the delayed signals.
		ChorusWaveform Waveform;

		//! This property controls the phase difference between the left and right LFO's. At zero degrees the
		//! two LFOs are synchronized. Use this parameter to create the illusion of an expanded stereo field
		//! of the output signal.
		//! Range: -180 to 180
		Int32 Phase;

		//! This property sets the modulation rate of the LFO that controls the delay time of the delayed signals.
		//! Range: 0.0 to 10.0
		Float32 Rate;

		//! This property controls the amount by which the delay time is modulated by the LFO.
		//! Range: 0.0 to 1.0
		Float32 Depth;

		//! This property controls the amount of processed signal that is fed back to the input of the chorus
		//! effect. Negative values will reverse the phase of the feedback signal. At full magnitude the
		//! identical sample will repeat endlessly. At lower magnitudes the sample will repeat and fade out
		//! over time. Use this parameter to create a "cascading" chorus effect.
		//! Range: -1.0 to 1.0
		Float32 Feedback;

		//! This property controls the average amount of time the sample is delayed before it is played back,
		//! and with feedback, the amount of time between iterations of the sample. Larger values lower the
		//! pitch. Smaller values make the chorus sound like a flanger, but with different frequency
		//! characteristics.
		//! Range: 0.0 to 0.016
		Float32 Delay;
} wChorusParameters;

//! The distortion effect simulates turning up (overdriving) the gain stage on a guitar amplifier or adding a distortion pedal to an instrument's output.
typedef struct tag_wDistortionParameters
{
#ifdef __cplusplus
    tag_wDistortionParameters(
		Float32 edge = 0.2f,
		Float32 gain = 0.05f,
		Float32 lowpassCutoff = 8000.0f,
		Float32 eqCenter = 3600.0f,
		Float32 eqBandwidth = 3600.0f) :
		Edge(edge), Gain(gain), LowpassCutoff(lowpassCutoff), EqCenter(eqCenter),
		EqBandwidth(eqBandwidth) { }
#endif // __cplusplus

		//! This property controls the shape of the distortion. The higher the value for Edge, the "dirtier" and "fuzzier" the effect.
		//! Range: 0.0 to 1.0
		Float32 Edge;

		//! This property allows you to attenuate the distorted sound.
		//! Range: 0.01 to 1.0
		Float32 Gain;

		//! Input signal can have a low pass filter applied, to limit the amount of high frequency signal feeding into the distortion effect.
		//! Range: 80.0 to 24000.0
		Float32 LowpassCutoff;

		//! This property controls the frequency at which the post-distortion attenuation (Gain) is active.
		//! Range: 80.0 to 24000.0
		Float32 EqCenter;

		//! This property controls the bandwidth of the post-distortion attenuation.
		//! Range: 80.0 to 24000.0
		Float32 EqBandwidth;
} wDistortionParameters;

//! The echo effect generates discrete, delayed instances of the input signal.
typedef struct tag_wEchoParameters
{
#ifdef __cplusplus
    tag_wEchoParameters(
		Float32 delay = 0.1f,
		Float32 lRDelay = 0.1f,
		Float32 damping = 0.5f,
		Float32 feedback = 0.5f,
		Float32 spread = -1.0f) :
		Delay(delay), LRDelay(lRDelay), Damping(damping), Feedback(feedback),
		Spread(spread) { }
#endif // __cplusplus

		//! This property controls the delay between the original sound and the first "tap", or echo instance.
		//! Range: 0.0 to 0.207
		Float32 Delay;

		//! This property controls the delay between the first "tap" and the second "tap".
		//! Range: 0.0 to 0.404
		Float32 LRDelay;

		//! This property controls the amount of high frequency damping applied to each echo. As the sound
		//! is subsequently fed back for further echoes, damping results in an echo which progressively gets
		//! softer in tone as well as intensity.
		//! Range: 0.0 to 0.99
		Float32 Damping;

		//! This property controls the amount of feedback the output signal fed back into the input. Use this
		//! parameter to create "cascading" echoes. At full magnitude, the identical sample will repeat
		//! endlessly. Below full magnitude, the sample will repeat and fade.
		//! Range: 0.0 to 1.0
		Float32 Feedback;

		//! This property controls how hard panned the individual echoes are. With a value of 1.0, the first
		//! "tap" will be panned hard left, and the second "tap" hard right. A value of -1.0 gives the opposite
		//! result. Settings nearer to 0.0 result in less emphasized panning.
		//! Range: -1.0 to 1.0
		Float32 Spread;
} wEchoParameters;

//! The flanger effect creates a "tearing" or "whooshing" sound (like a jet flying overhead).
typedef enum
{
    wFWF_SINUSOID,
	wFWF_TRIANGLE,
	wFWF_COUNT
} FlangerWaveform;

typedef struct tag_wFlangerParameters
{
#ifdef __cplusplus
    tag_wFlangerParameters(
		FlangerWaveform waveform = wFWF_TRIANGLE,
		Int32 phase = 0,
		Float32 rate = 0.27f,
		Float32 depth = 1.0f,
		Float32 feedback = -0.5f,
		Float32 delay = 0.002f) :
		Waveform(waveform), Phase(phase), Rate(rate), Depth(depth), Feedback(feedback),
		Delay(delay) { }
#endif // __cplusplus

		//! Selects the shape of the LFO waveform that controls the amount of the delay of the sampled signal.
		FlangerWaveform Waveform;

		//! This changes the phase difference between the left and right LFO's. At zero degrees the two LFOs are synchronized.
		//! Range: -180 to 180
		Int32 Phase;

		//! The number of times per second the LFO controlling the amount of delay repeats. Higher values increase the pitch modulation.
		//! Range: 0.0 to 10.0
		Float32 Rate;

		//! The ratio by which the delay time is modulated by the LFO. Use this parameter to increase the pitch modulation.
		//! Range: 0.0 to 1.0
		Float32 Depth;

		//! This is the amount of the output signal level fed back into the effect's input.
		//! A negative value will reverse the phase of the feedback signal. Use this parameter
		//! to create an "intense metallic" effect. At full magnitude, the identical sample will
		//! repeat endlessly. At less than full magnitude, the sample will repeat and fade out over time.
		//! Range: -1.0 to 1.0
		Float32 Feedback;

		//! The average amount of time the sample is delayed before it is played back; with feedback, the amount of time between iterations of the sample.
		//! Range: 0.0 to 0.004
		Float32 Delay;
} wFlangerParameters;

//! The frequency shifter is a single-sideband modulator, which translates all the component frequencies of the input signal by an equal amount.
typedef enum
{
    wSD_DOWN,
	wSD_UP,
	wSD_OFF,
	wSD_COUNT
} ShiftDirection;

typedef struct tag_wFrequencyShiftParameters
{
#ifdef __cplusplus
    tag_wFrequencyShiftParameters(
		Float32 frequency = 0.0f,
		ShiftDirection left = wSD_DOWN,
		ShiftDirection right = wSD_DOWN) :
		Frequency(frequency), Left(left), Right(right) { }
#endif // __cplusplus

		//! This is the carrier frequency. For carrier frequencies below the audible range, the singlesideband
		//! modulator may produce phaser effects, spatial effects or a slight pitch-shift. As the
		//! carrier frequency increases, the timbre of the sound is affected; a piano or guitar note becomes
		//! like a bell's chime, and a human voice sounds extraterrestrial!
		//! Range: 0.0 to 24000.0
		Float32 Frequency;

		//! These select which internal signals are added together to produce the output. Different
		//! combinations of values will produce slightly different tonal and spatial effects.
		ShiftDirection Left;

		//! These select which internal signals are added together to produce the output. Different
		//! combinations of values will produce slightly different tonal and spatial effects.
		ShiftDirection Right;
} wFrequencyShiftParameters;

//! The vocal morpher consists of a pair of 4-band formant filters, used to impose vocal tract effects upon the input signal.
typedef enum
{
    wMP_A,
    wMP_E,
    wMP_I,
    wMP_O,
    wMP_U,
    wMP_AA,
    wMP_AE,
    wMP_AH,
    wMP_AO,
    wMP_EH,
    wMP_ER,
    wMP_IH,
    wMP_IY,
    wMP_UH,
    wMP_UW,
    wMP_B,
    wMP_D,
    wMP_F,
	wMP_G,
    wMP_J,
	wMP_K,
	wMP_L,
	wMP_M,
	wMP_N,
	wMP_P,
	wMP_R,
	wMP_S,
	wMP_T,
	wMP_V,
	wMP_Z,
	wMP_COUNT
} MorpherPhoneme;

typedef enum
{
    wMWF_SINUSOID,
    wMWF_TRIANGLE,
    wMWF_SAW,
    wMWF_COUNT
} MorpherWaveform;

typedef struct tag_wVocalMorpherParameters
{
#ifdef __cplusplus
	tag_wVocalMorpherParameters(
		MorpherPhoneme phonemeA = wMP_A,
		MorpherPhoneme phonemeB = wMP_ER,
		Int32 phonemeACoarseTune = 0,
		Int32 phonemeBCoarseTune = 0,
		MorpherWaveform waveform = wMWF_SINUSOID,
		Float32 rate = 1.41f) :
		PhonemeA(phonemeA), PhonemeB(phonemeB), PhonemeACoarseTune(phonemeACoarseTune),
		PhonemeBCoarseTune(phonemeBCoarseTune), Waveform(waveform), Rate(rate) { }
#endif // __cplusplus

		//! If both parameters are set to the same phoneme, that determines the filtering effect that will be
		//! heard. If these two parameters are set to different phonemes, the filtering effect will morph
		//! between the two settings at a rate specified by Rate.
		MorpherPhoneme PhonemeA;

		//! If both parameters are set to the same phoneme, that determines the filtering effect that will be
		//! heard. If these two parameters are set to different phonemes, the filtering effect will morph
		//! between the two settings at a rate specified by Rate.
		MorpherPhoneme PhonemeB;

		//! This is used to adjust the pitch of phoneme filter A in 1-semitone increments.
		//! Range: -24 to 24
		Int32 PhonemeACoarseTune;

		//! This is used to adjust the pitch of phoneme filter B in 1-semitone increments.
		//! Range: -24 to 24
		Int32 PhonemeBCoarseTune;

		//! This controls the shape of the low-frequency oscillator used to morph between the two phoneme filters.
		MorpherWaveform Waveform;

		//! This controls the frequency of the low-frequency oscillator used to morph between the two phoneme filters.
		//! Range: 0.0 to 10.0
		Float32 Rate;
} wVocalMorpherParameters;

//! The pitch shifter applies time-invariant pitch shifting to the input signal, over a one octave range and controllable at a semi-tone and cent resolution.
typedef struct tag_wPitchShifterParameters
{
#ifdef __cplusplus
    tag_wPitchShifterParameters(
		Int32 coarseTune = 12,
		Int32 fineTune = 0) :
		CoarseTune(coarseTune), FineTune(fineTune) { }
#endif // __cplusplus

		//! This sets the number of semitones by which the pitch is shifted. There are 12 semitones per
		//! octave. Negative values create a downwards shift in pitch, positive values pitch the sound
		//! upwards.
		//! Range: -12 to 12
		Int32 CoarseTune;

		//! This sets the number of cents between Semitones a pitch is shifted. A Cent is 1/100th of a
		//! Semitone. Negative values create a downwards shift in pitch, positive values pitch the sound
		//! upwards.
		//! Range: -50 to 50
		Int32 FineTune;
} wPitchShifterParameters;

//! The ring modulator multiplies an input signal by a carrier signal in the time domain, resulting in tremolo or inharmonic effects.
typedef enum
{
    wMDWF_SINUSOID,
	wMDWF_SAW,
	wMDWF_SQUARE,
	wMDWF_COUNT
} ModulatorWaveform;

typedef struct tag_wRingModulatorParameters
{
#ifdef __cplusplus
	tag_wRingModulatorParameters(
		Float32 frequency = 440.0f,
		Float32 highPassCutoff = 800.0f,
		//ModulatorWaveform waveform = EMW_SINUSOID) :
		ModulatorWaveform waveform = wMDWF_SINUSOID) :
		Frequency(frequency), HighPassCutoff(highPassCutoff), Waveform(waveform) { }
#endif // __cplusplus

		//! This is the frequency of the carrier signal. If the carrier signal is slowly varying (less than 20 Hz),
		//! the result is a tremolo (slow amplitude variation) effect. If the carrier signal is in the audio range,
		//! audible upper and lower sidebands begin to appear, causing an inharmonic effect. The carrier
		//! signal itself is not heard in the output.
		//! Range: 0.0 to 8000.0
		Float32 Frequency;

		//! This controls the cutoff frequency at which the input signal is high-pass filtered before being ring
		//! modulated. If the cutoff frequency is 0, the entire signal will be ring modulated. If the cutoff
		//! frequency is high, very little of the signal (only those parts above the cutoff) will be ring
		//! modulated.
		//! Range: 0.0 to 24000.0
		Float32 HighPassCutoff;

		//! This controls which waveform is used as the carrier signal. Traditional ring modulator and
		//! tremolo effects generally use a sinusoidal carrier. Sawtooth and square waveforms are may
		//! cause unpleasant aliasing.
		ModulatorWaveform Waveform;
} wRingModulatorParameters;

//! The Auto-wah effect emulates the sound of a wah-wah pedal used with an electric guitar, or a mute on a brass instrument.
typedef struct tag_wAutowahParameters
{
#ifdef __cplusplus
    tag_wAutowahParameters(
		Float32 attackTime = 0.06f,
		Float32 releaseTime = 0.06f,
		Float32 resonance = 1000.0f,
		Float32 peakGain = 11.22f) :
		AttackTime(attackTime), ReleaseTime(releaseTime), Resonance(resonance),
		PeakGain(peakGain) { }
#endif // __cplusplus

		//! This property controls the time the filtering effect takes to sweep from minimum to maximum center frequency when it is triggered by input signal.
		//! Range: 0.0001 to 1.0
		Float32 AttackTime;

		//! This property controls the time the filtering effect takes to sweep from maximum back to base center frequency, when the input signal ends.
		//! Range: 0.0001 to 1.0
		Float32 ReleaseTime;

		//! This property controls the resonant peak, sometimes known as emphasis or Q, of the auto-wah
		//! band-pass filter. Resonance occurs when the effect boosts the frequency content of the sound
		//! around the point at which the filter is working. A high value promotes a highly resonant, sharp
		//! sounding effect.
		//! Range: 2.0 to 1000.0
		Float32 Resonance;

		//! This property controls the input signal level at which the band-pass filter will be fully opened.
		//! Range: 0.00003 to 31621.0
		Float32 PeakGain;
} wAutowahParameters;

//! The Automatic Gain Control effect performs the same task as a studio compressor, evening out the audio dynamic range of an input sound.
typedef struct tag_wCompressorParameters
{
#ifdef __cplusplus
    tag_wCompressorParameters(
		bool active = true) :
		Active(active) { }
#endif // __cplusplus

		//! The Compressor can only be switched on and off – it cannot be adjusted.
		bool Active;
} wCompressorParameters;

//! The OpenAL Effects Extension EQ is very flexible, providing tonal control over four different adjustable frequency ranges.
typedef struct tag_wEqualizerParameters
{
#ifdef __cplusplus
    tag_wEqualizerParameters(
        Float32 lowGain = 1.0f,
		Float32 lowCutoff = 200.0f,
		Float32 mid1Gain = 1.0f,
		Float32 mid1Center = 500.0f,
		Float32 mid1Width = 1.0f,
		Float32 mid2Gain = 1.0f,
		Float32 mid2Center = 3000.0f,
		Float32 mid2Width = 1.0f,
		Float32 highGain = 1.0f,
		Float32 highCutoff = 6000.0f) :
		LowGain(lowGain), LowCutoff(lowCutoff), Mid1Gain(mid1Gain),
		Mid1Center(mid1Center), Mid1Width(mid1Width), Mid2Gain(mid2Gain),
		Mid2Center(mid2Center), Mid2Width(mid2Width), HighGain(highGain),
		HighCutoff(highCutoff) { }
#endif // __cplusplus

		//! This property controls amount of cut or boost on the low frequency range.
		//! Range: 0.126 to 7.943
		Float32 LowGain;

		//! This property controls the low frequency below which signal will be cut off.
		//! Range: 50.0 to 800.0
		Float32 LowCutoff;

		//! This property allows you to cut / boost signal on the "mid1" range.
		//! Range: 0.126 to 7.943
		Float32 Mid1Gain;

		//! This property sets the center frequency for the "mid1" range.
		//! Range: 200.0 to 3000.0
		Float32 Mid1Center;

		//! This property controls the width of the "mid1" range.
		//! Range: 0.01 to 1.0
		Float32 Mid1Width;

		//! This property allows you to cut / boost signal on the "mid2" range.
		//! Range: 0.126 to 7.943
		Float32 Mid2Gain;

		//! This property sets the center frequency for the "mid2" range.
		//! Range: 1000.0 to 8000.0
		Float32 Mid2Center;

		//! This property controls the width of the "mid2" range.
		//! Range: 0.01 to 1.0
		Float32 Mid2Width;

		//! This property allows you to cut / boost the signal at high frequencies.
		//! Range: 0.126 to 7.943
		Float32 HighGain;

		//! This property controls the high frequency above which signal will be cut off.
		//! Range: 4000.0 to 16000.0
		Float32 HighCutoff;
} wEqualizerParameters;

typedef struct tag_wShader
{
	//wMaterialTypes material_type;
	//Нельзя менять!
	Int32 material_type;
	void*	irrShaderCallBack;
	struct tag_wShader*	next_shader;
} wShader;

#endif // __WORLDSIM3D_TYPES_H_INCLUDED

