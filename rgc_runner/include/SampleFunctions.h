#ifndef SAMPLEFUNCTIONS_H_INCLUDED
#define SAMPLEFUNCTIONS_H_INCLUDED

#include "WorldSim3D.h"

///Custom utils
bool CheckFilePath(char* path);

#ifdef __cplusplus
void PrintWithColor(char* text,
					wConsoleFontColor color=wCFC_RED,
					bool waitKey=false);
#else
void PrintWithColor(char* text,
					wConsoleFontColor color,
					bool waitKey);
#endif // __cplusplus

///Quake map utils
void ParseQuakeMap(wMesh* BSPMesh);
void SetQuakeShadersVisible(bool value);

///Второй вариант парсинга bsp-карты
void ParseQuakeMap2(wMesh* BSPMesh);

///For load resources
wMesh* wMeshLoadEx(char* path);

///For animated meshes
void wMeshesUpdateTangentsAndBinormal();

///For color texture
wTexture* CreateColorTexture(wColor4s tColor, const char* name = "colorTexture");

void OutlineNode(wNode* node,Float32 Width,wColor4s lineColor);

wNode* CreateBullet(Float32 speed,wTexture* texture);

bool MousePick(bool isPick);

void CreateGround(Float32 scale);

wSound* wSoundLoadEx(const char* pathFile,const char* extFile);

wSoundBuffer* wSoundBufferLoadEx(const char* pathFile,const char* extFile);

///For shaders
wMaterialTypes ShaderGetMaterialType(wShader* shader);

///Normal mapping for bsp
#ifdef __cplusplus
void wMeshPrepareToNormalMapping(wMesh* mesh,
                                 wMaterialTypes matType=wMT_PARALLAX_MAP_SOLID,
                                 UInt32 srcTextureLayer=0,
                                 UInt32 dstTextureLayer=1,
                                 Float32 matParam=0.0035f,
                                 Float32 amplitude=9.f,
                                 Float32 blurRadius=1.5f,
                                 Float32 contrast=10.f,
                                 Int32 brightness=60);
#else
void wMeshPrepareToNormalMapping(wMesh* mesh,
                                 wMaterialTypes matType,
                                 UInt32 srcTextureLayer,
                                 UInt32 dstTextureLayer,
                                 Float32 matParam,
                                 Float32 amplitude,
                                 Float32 blurRadius,
                                 Float32 contrast,
                                 Int32 brightness);
#endif // __cplusplus

#ifdef __cplusplus
typedef struct
{
    public:
     UInt32 size();
     UInt32* get(UInt32 idx);
     Int32 getIndex(UInt32* element);
     bool add(UInt32* element);
     bool insert(UInt32* element, UInt32 idx);
     bool remove(UInt32* element,bool isNodeDestroy=false);
     bool remove(UInt32 idx,bool isNodeDestroy=false);
     void clear(bool isNodeDestroy=false);
     private:
     UInt32** Array=0;
     UInt32   ArraySize=0;
 }wArray;

#else

typedef UInt32 wArray;
wArray* wArrayCreate();
UInt32 wArrayGetSize(wArray* array);
void* wArrayGetElement(wArray* array, UInt32 idx);
Int32 wArrayGetIndex(wArray* array, void* element);
bool wArrayAddElement(wArray* array, void* element);
bool wArrayInsertElement(wArray* array, void* element, UInt32 idx);
bool wArrayRemoveElement(wArray* array, UInt32* element, bool isNodeDestroy);
bool wArrayRemoveElementByIndex(wArray* array, UInt32 idx, bool isNodeDestroy);
void wArrayClear(wArray* array, bool isNodeDestroy);
void wArrayDestroy(wArray** array, bool isNodeDestroy);

#endif // __cplusplus

#ifdef __cplusplus
typedef struct
{
    wGuiObject* label=0;
    wGuiObject* scroll=0;
    bool horizontal=true;
    const wchar_t* caption=L"";
    bool isFloat=false;

    void Init(wVector2i position,wVector2i size,const wchar_t* capt,Int32 id,Float32 maxValue,Float32 currentValue);
    void SetValueAsFloat(Float32 value);
    Float32 getValueAsFloat();
    void SetValueAsInt(Int32 value);
    Int32 getValueAsInt();
    void Draw();
}
wScrollbar;
#endif // __cplusplus

 #endif // SAMPLEFUNCTIONS_H_INCLUDED
