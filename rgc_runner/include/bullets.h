#ifndef BULLETS_H
#define BULLETS_H

#include <vector>
#include "WorldSim3D.h"
#include "object3d_billboard.h"
#include "res_in_mem.h"

/** пуля-эталон, загруженная из пака */
class TBulletInMem : public ScrResourceInMem
{
public:
    double speed = 0;         // скорость движения
    double distance = 0;      // макс. дистанция выстрела
    double lifeTime = 0;      // время жизни снаряда
    int    lifeFrameLast = 0; // последний кадр анимации жизни

    TBulletInMem() : ScrResourceInMem()
    {
        type = rtBullet;
    }
};

/** нода пули, создаваемая на карте */
class TBulletNode : public TBillboard
{
public:
    // создание ноды
    TBulletNode (TBulletInMem* index);

    virtual void init(string className = "", string objectName = "", bool numerate = true)
    {
        TObject::init("Bullet", "bullet", true);
    }
};

extern vector<TBulletInMem> g_Bullets;

void InitBullets();
bool LoadBulletsPack();

#endif // BULLETS_H
