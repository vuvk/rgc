#ifndef _KEYS_H
#define _KEYS_H

#include <vector>

#include "WorldSim3D.h"
#include "object3d_billboard.h"
#include "res_in_mem.h"

/** ключ-эталон, загруженный из пака */
class TKeyInMem : public ScrResourceInMem
{
public:
    std::string pickUpSound;      // имя звука подбора

    TKeyInMem() : ScrResourceInMem()
    {
        type = rtKey;
    }
};

/** нода ключа, создаваемая на карте */
class TKeyNode : public TBillboard
{
public:
    static int count;

    // создание ноды
    TKeyNode (TKeyInMem* index);

    virtual void init(string className = "", string objectName = "", bool numerate = true)
    {
        TObject::init("Key", "key", true);
    }
};

extern vector<TKeyInMem> g_Keys;

void InitKeys();
bool LoadKeysPack();

#endif // _KEYS_H
