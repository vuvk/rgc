﻿cmake_minimum_required(VERSION 2.8)
set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -m32")
#set(CMAKE_INSTALL_RPATH_USE_LINK_PATH FALSE)

#set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -g")
#set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -Wall -O2 -s")

project(n3d_runner)

# куда выплёвывать проект и расположение динамических либ
set(BINARY_DIR "${PROJECT_SOURCE_DIR}/build")

# путь до инклудов
set (INCLUDE_DIR "${PROJECT_SOURCE_DIR}/include")
include_directories(
	"${INCLUDE_DIR}/"
	"${INCLUDE_DIR}/AL/"
	"${INCLUDE_DIR}/alure/"
	"${INCLUDE_DIR}/jerryscript/"
	)
	
# инклуды
set(HEADERS 
#	"${INCLUDE_DIR}/AL/al.h"
#	"${INCLUDE_DIR}/AL/alc.h"	
#	"${INCLUDE_DIR}/alure/alure2.h"		
#	"${INCLUDE_DIR}/jerryscript/jerryscript.h"

	"${INCLUDE_DIR}/base_object.h"
	"${INCLUDE_DIR}/bullets.h"
	"${INCLUDE_DIR}/constants.h"
	"${INCLUDE_DIR}/doors.h"
	"${INCLUDE_DIR}/file_system.h"
	"${INCLUDE_DIR}/global.h"
	"${INCLUDE_DIR}/input.h"
	"${INCLUDE_DIR}/js_util.h"
	"${INCLUDE_DIR}/keys.h"
	"${INCLUDE_DIR}/maps.h"
	"${INCLUDE_DIR}/object2d.h"
	"${INCLUDE_DIR}/object3d.h"
	"${INCLUDE_DIR}/object3d_billboard.h"
	"${INCLUDE_DIR}/physfs.h"
	"${INCLUDE_DIR}/player.h"
	"${INCLUDE_DIR}/res_in_mem.h"
	"${INCLUDE_DIR}/res_manager.h"
	"${INCLUDE_DIR}/SampleFunctions.h"
	"${INCLUDE_DIR}/audio_system.h"
	"${INCLUDE_DIR}/audio_buffer.h"
	"${INCLUDE_DIR}/audio_source.h"
	"${INCLUDE_DIR}/sound.h"
	"${INCLUDE_DIR}/sprites.h"
	"${INCLUDE_DIR}/textures.h"
	"${INCLUDE_DIR}/util.h"
	"${INCLUDE_DIR}/weapon_item.h"
	"${INCLUDE_DIR}/weapons.h"
	"${INCLUDE_DIR}/WorldSim3D.h"
	"${INCLUDE_DIR}/audio_listener.h"
	"${INCLUDE_DIR}/ammo.h"
	)

# Установить переменную с путем до src
set(SOURCE_DIR "${PROJECT_SOURCE_DIR}/src")

# расположение статических библиотек
set(LIBRARIES_DIR "${PROJECT_SOURCE_DIR}/lib")

# где находятся библиотеки для линковки
link_directories(
	${LIBRARIES_DIR} 
	${BINARY_DIR}
	)

# Установка переменной со списком исходников для исполняемого файла
set(SOURCE_EXE
	"${SOURCE_DIR}/WorldSim3D_runtime.cpp"
	"${SOURCE_DIR}/SampleFunctions.cpp"
	"${SOURCE_DIR}/res_manager_runtime.cpp"
	"${SOURCE_DIR}/global.cpp"
	"${SOURCE_DIR}/file_system.cpp"
	"${SOURCE_DIR}/audio_system.cpp"
	"${SOURCE_DIR}/audio_buffer.cpp"
	"${SOURCE_DIR}/audio_source.cpp"
	"${SOURCE_DIR}/sound.cpp"
	"${SOURCE_DIR}/util.cpp"
	"${SOURCE_DIR}/js_util.cpp"
	"${SOURCE_DIR}/input.cpp"
	"${SOURCE_DIR}/res_in_mem.cpp"
	"${SOURCE_DIR}/base_object.cpp"
	"${SOURCE_DIR}/object2d.cpp"
	"${SOURCE_DIR}/object3d.cpp"
	"${SOURCE_DIR}/object3d_billboard.cpp"

	"${SOURCE_DIR}/bullets.cpp"
	"${SOURCE_DIR}/doors.cpp"
	"${SOURCE_DIR}/keys.cpp"
	"${SOURCE_DIR}/maps.cpp"
	"${SOURCE_DIR}/player.cpp"
	"${SOURCE_DIR}/sprites.cpp"
	"${SOURCE_DIR}/textures.cpp"
	"${SOURCE_DIR}/weapon_item.cpp"
	"${SOURCE_DIR}/weapons.cpp"
	"${SOURCE_DIR}/main.cpp"
	"${SOURCE_DIR}/audio_listener.cpp"
	"${SOURCE_DIR}/ammo.cpp"
	)
	
# Создает исполняемый файл с именем проекта
add_executable(${PROJECT_NAME} ${SOURCE_EXE})

# линковки
#add_library(jerry-core 	STATIC IMPORTED)
#add_library(jerry-ext 	STATIC IMPORTED)
#add_library(jerry-port-default STATIC IMPORTED)
#add_library(physfs 		STATIC IMPORTED)
#add_library(openal 		STATIC IMPORTED)
#add_library(alure2_s 	STATIC IMPORTED)
#set_property(TARGET jerry-core  PROPERTY IMPORTED_LOCATION ${LIBRARIES_DIR}/libjerry-core.a)
#set_property(TARGET jerry-ext   PROPERTY IMPORTED_LOCATION ${LIBRARIES_DIR}/libjerry-ext.a)
#set_property(TARGET jerry-port-default PROPERTY IMPORTED_LOCATION ${LIBRARIES_DIR}/libjerry-port-default.a)
#set_property(TARGET physfs      PROPERTY IMPORTED_LOCATION ${LIBRARIES_DIR}/libphysfs.a)
#set_property(TARGET openal      PROPERTY IMPORTED_LOCATION ${LIBRARIES_DIR}/libopenal.a)
#set_property(TARGET alure2_s    PROPERTY IMPORTED_LOCATION ${LIBRARIES_DIR}/libalure2_s.a)

#add_library(WS3DCoreLib SHARED IMPORTED)
#set_property(TARGET WS3DCoreLib PROPERTY IMPORTED_LOCATION ${BINARY_DIR}/WS3DCoreLib.dll)


if (WIN32)
    set(DYNLOAD_LIB	)
    set (ALURE_LIB ALURE32)
endif (WIN32)

if (UNIX)
    set(DYNLOAD_LIB dl)
    set (ALURE_LIB alure)
endif (UNIX)

target_link_libraries(${PROJECT_NAME} 
#	dumb
#	FLAC
#	gmp
#	ogg
#	opus
#	opusfile
#	sndfile
#	vorbis
#	vorbisfile
#   alure2_s
#	res_manager
#	WS3DCoreLib
#	OpenAL32
#	libalure2

    jerry-core
    jerry-libm
    jerry-ext
    jerry-port-default
    openal
    physfs
    ${ALURE_LIB}
    ${DYNLOAD_LIB}
	)
