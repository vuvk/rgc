var keyId, keyName;
var pos;

var pickUpSound, pickUpSoundName;
var animSpeed, animDelay;
var frame, maxFrame;

/**
 *  Initialization of object
 */
this.onInit = function() {
	keyId = objectGetId();
	keyName = objectGetVar(keyId, "name");
	pos = new Vector3f(objectGetPosition(keyId));
	
	pickUpSoundName = objectGetVar(keyId, "pickUpSound");
	pickUpSound = soundCreate(pickUpSoundName);
	
	animSpeed = objectGetVar(keyId, "animationSpeed");
	animDelay = 0.0;
	
	frame = 0;
	maxFrame = objectGetFramesCount(keyId) - 1;
	objectSetFrame(keyId, frame);	
}

/**
 *  Update event
 */
this.onUpdate = function() {
	if (g_PauseState || Level.deltaTime == 0.0)
		return;
	
	/* update animation */
	if (animSpeed != 0.0) {		
		if (animDelay < 1.0)
			animDelay += Level.deltaTime * animSpeed;
		else {
			animDelay -= 1.0;
			++frame;

			if (frame > maxFrame) 
				frame = 0;
			
			objectSetFrame(keyId, frame);
		}
	}
	
	if (distanceBetweenPoints(pos.x, pos.z, Player.pos.x, Player.pos.z) <= 0.2) {
		print("Now I have a '" + keyName + "'!");
		objectAddVarBool(Player.id, keyName, true);
		soundPlayOnce(pickUpSound);
		objectDestroy(keyId);
	}
}

/**
 *  Destroy event
 */
this.onDestroy = function() {
}