
var weaponId, weaponName, weaponNumber;
var ammoType, curAmmo, infiniteAmmo;
var pos;

var pickUpSound, pickUpSoundName;
var animSpeed, animDelay;
var frame, maxFrame;


/**
 *  Initialization of object
 */
this.onInit = function() {
	weaponId = objectGetId();
	weaponName = objectGetVar(weaponId, "name");
	weaponNumber = objectGetVar(weaponId, "weaponNumber");
	ammoType = objectGetVar(weaponId, "ammoType");
	curAmmo = objectGetVar(weaponId, "curAmmo");
	infiniteAmmo = objectGetVar(weaponId, "infiniteAmmo");
	pos = new Vector3f(objectGetPosition(weaponId));
	
	print(weaponName, ' ', ammoType + ' ' + curAmmo);
	
	pickUpSoundName = objectGetVar(weaponId, "pickUpSound");
	pickUpSound = soundCreate(pickUpSoundName);
	
	animSpeed = objectGetVar(weaponId, "animationSpeed");
	animDelay = 0.0;
	
	frame = 0;
	maxFrame = objectGetFramesCount(weaponId) - 1;
	objectSetFrame(weaponId, frame);	
}


/**
 *  Update event
 */
this.onUpdate = function() {
	if (g_PauseState || Level.deltaTime == 0.0)
		return;
	
	if (distanceBetweenPoints(pos.x, pos.z, Player.pos.x, Player.pos.z) <= 0.2) {		
		print("Now I have a '" + weaponName + "' - " + weaponNumber + " weapon!");
		
		if (!weaponIsAvailable(weaponNumber)) {
			weaponSetAvailable(weaponNumber, true);
			weaponSetActive(weaponNumber);
		}
		
		if (!infiniteAmmo) {
			/* add variable for ammo type of it */
			if (!objectExistVar(Player.id, ammoType)) {
				objectAddVarNumber(Player.id, ammoType, 0);
			}
			
			/* add ammo for player */
			var playerAmmoVolume = objectGetVar(Player.id, ammoType);
			playerAmmoVolume += curAmmo;
			objectSetVar(Player.id, ammoType, playerAmmoVolume);

			print(ammoType + ' ' + playerAmmoVolume);
		}
		
		soundPlayOnce(pickUpSound);
		objectDestroy(weaponId);
	}
		
	/* update animation */
	if (animSpeed == 0.0)
		return;
	
	if (animDelay < 1.0)
		animDelay += Level.deltaTime * animSpeed;
	else {
		animDelay -= 1.0;
		++frame;

		if (frame > maxFrame) 
			frame = 0;
		
		objectSetFrame(weaponId, frame);
	}
}


/**
 *  Destroy
 */
this.onDestroy = function() {
}