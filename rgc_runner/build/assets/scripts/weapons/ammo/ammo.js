var ammoId, ammoName, ammoType;
var volume;
var pos;

var pickUpSound, pickUpSoundName;
var animSpeed, animDelay;
var frame, maxFrame;

/**
 *  Initialization of object
 */
this.onInit = function() {
	ammoId = objectGetId();
	ammoName = objectGetVar(ammoId, "name");
	ammoType = objectGetVar(ammoId, "ammoType");
	volume = objectGetVar(ammoId, "volume");
	pos = new Vector3f(objectGetPosition(ammoId));
	
	pickUpSoundName = objectGetVar(ammoId, "pickUpSound");
	pickUpSound = soundCreate(pickUpSoundName);
	
	animSpeed = objectGetVar(ammoId, "animationSpeed");
	animDelay = 0.0;
	
	frame = 0;
	maxFrame = objectGetFramesCount(ammoId) - 1;
	objectSetFrame(ammoId, frame);	
}

/**
 *  Update event
 */
this.onUpdate = function() {
	if (g_PauseState || Level.deltaTime == 0.0)
		return;
	
	/* update animation */
	if (animSpeed != 0.0) {		
		if (animDelay < 1.0)
			animDelay += Level.deltaTime * animSpeed;
		else {
			animDelay -= 1.0;
			++frame;

			if (frame > maxFrame) 
				frame = 0;
			
			objectSetFrame(ammoId, frame);
		}
	}
	
	if (distanceBetweenPoints(pos.x, pos.z, Player.pos.x, Player.pos.z) <= 0.2) {
		print("Now I pick up '" + ammoName + "'!");
		
		/* add variable for ammo type of it */
		if (!objectExistVar(Player.id, ammoType)) {
			objectAddVarNumber(Player.id, ammoType, 0);
		}
		
		/* add ammo for player */
		var playerAmmoVolume = objectGetVar(Player.id, ammoType);
		playerAmmoVolume += volume;
		objectSetVar(Player.id, ammoType, playerAmmoVolume);	
		
		print(ammoType + ' ' + playerAmmoVolume);
		
		soundPlayOnce(pickUpSound);
		objectDestroy(ammoId);
	}
}

/**
 *  Destroy event
 */
this.onDestroy = function() {
}