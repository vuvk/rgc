
var bulletId, bulletName;
var pos, newPos, oldPos;
var uCol, lCol, rCol, dCol; /* Up, Left, Right and Down Collision Points */

var animSpeed, animDelay;
var frame, maxFrame, lifeFrameLast;

var maxLifeTime, lifeTime;
var maxDistance, distance;

var isLive = true;

var direction;
var moveSpeed = 0.0;
var damage = 0.0;

/**
 *  Initialization of object
 */
this.onInit = function() {
	lifeTime = 0.0;
	distance = 0.0;
	animDelay = 0.0;	
	frame = 0;
	
	bulletId = objectGetId();
	bulletName = objectGetVar(bulletId, "name");
	pos = new Vector3f(objectGetPosition(bulletId));
	newPos = oldPos = pos;
	direction = new Vector3f(objectGetVar(bulletId, "direction"));
	moveSpeed = objectGetVar(bulletId, "speed");
	maxLifeTime = objectGetVar(bulletId, "lifeTime");
	maxDistance = objectGetVar(bulletId, "distance");
	damage = objectGetVar(bulletId, "damage");
	
//	print('maxlife = ' + maxLifeTime);
//	print('maxDistance = ' + maxDistance);
//	print('bullet id = ' + bulletId);

	/* Collision Points on perimeter */
	var size = new Vector3f(objectGetSize(bulletId));
	//print('size = ' + size.x);
	var halfWidth  = size.x / 2.0;
	var halfHeight = size.y / 2.0;
	//uCol = upVector.mul(halfWidth);
	//dCol = downVector.mul(halfWidth);
	lCol = (direction.cross(upVector)).normalize().mul(halfWidth);
	rCol = lCol.mul(-1.0);
	uCol = (direction.cross(lCol)).normalize().mul(halfHeight);
	dCol = (direction.cross(rCol)).normalize().mul(halfHeight);
	//dCol = new Vector3f(uCol.x, -uCol.y, uCol.z);
	
	animSpeed = objectGetVar(bulletId, "animationSpeed");
	maxFrame = objectGetFramesCount(bulletId) - 1;
	lifeFrameLast = objectGetVar(bulletId, "lifeFrameLast");
	objectSetFrame(bulletId, frame);
}

function checkCollision(startVector, endVector) {
	var objectId = objectGetByRay(startVector.x, startVector.y, startVector.z, endVector.x, endVector.y, endVector.z);	
	var collisionPoint = new Vector3f(0.0, 0.0, 0.0);
	
	/* collision exists */
	if (!isNaN(objectId)) {
		/* it is not a wall */
		if (objectId > 0)
		{
			var health = objectGetVar(objectId, "health");
			if (health > 0) {
				health -= damage;
				objectSetVar(objectId, "health", health);
			}
		}
			
		/* set new position near collision point */
		collisionPoint = new Vector3f(intersectPointGetByRay());
		var distance = distanceBetweenVectors(oldPos, collisionPoint);
		var moveDir = direction.mul(distance);
		newPos = oldPos.add(moveDir);	
		
		/* destroy bullet */
		isLive = false;
		frame = lifeFrameLast;
	}
}


/**
 *  Update event
 */
this.onUpdate = function() {	
	if (g_PauseState || Level.deltaTime == 0.0)
		return;
	
	if (isLive) {
		var speed = Level.deltaTime * moveSpeed;
		var moveDir = direction.mul(speed);
		
		newPos = oldPos.add(moveDir);	
		
		/* check collision on center */
		checkCollision(oldPos, newPos);	
		
		/* check up point */
		if (isLive) {
			var up = newPos.add(uCol);
			checkCollision(oldPos, up);
		}
		
		/* check down point */
		if (isLive) {
			var dn = newPos.add(dCol);
			checkCollision(oldPos, dn);	
		}
		
		/* check left point */
		if (isLive) {
			var lt = newPos.add(lCol);
			checkCollision(oldPos, lt);
		}
		
		/* check right point */
		if (isLive) {
			var rt = newPos.add(rCol);
			checkCollision(oldPos, rt);	
		}			
		
		objectSetPosition(bulletId, newPos.x, newPos.y, newPos.z);		
		
		distance += speed;
		lifeTime += Level.deltaTime;
		if ((maxDistance > 0 && distance >= maxDistance) || 
			(lifeTime >= maxLifeTime && maxLifeTime > 0)) {				
			isLive = false;
			frame = lifeFrameLast;			
		}
	}
	
	/* animation */
	if (animDelay < 1.0)
		animDelay += animSpeed * Level.deltaTime;
	else {
		animDelay -= 1.0;
		++frame;
		
		/* live animation */
		if (isLive) {
			if (frame > lifeFrameLast) {
				frame = 0;
				animDelay = 0.0;
			}			
		}
		/* splash animation */
		else {
			/* stop animation and destroy */
			if (frame > maxFrame) {			
				objectDestroy(bulletId);
				return;				
			}	
		}	
		
		objectSetFrame(bulletId, frame);	
	}
	
	oldPos = newPos;	
}


/**
 *  Destroy event
 */
this.onDestroy = function() {
}