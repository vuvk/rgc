/* globals */
this.id;
this.pos;
this.rotSpeed;


/**
 *  Initialization of object
 */
this.onInit = function() {
	this.id = objectGetId();
	this.pos = new Vector3f();
	this.rotSpeed  = 0.5;

	/* set unvisible cursor */
	Mouse.setCursorVisible(false);
	
	
	objectAddVar(this.id, "bool1", true);
	objectAddVar(this.id, "bool2", false);
	objectAddVar(this.id, "string1", "Hello, vasya!");
	objectAddVar(this.id, "number1", 10);
	objectAddVar(this.id, "number2", 10, 666);
	objectAddVar(this.id, "number3", 10, 666, "jopa");
	objectAddVar(this.id, "vector1", 10, 666, 13);
	
	print("bool1 is bool?     - " + objectIsVarBool(this.id, "bool1")     + " \tvalue: " + objectGetVar(this.id, "bool1"));	
	print("bool2 is bool?     - " + objectIsVarBool(this.id, "bool2")     + " \tvalue: " + objectGetVar(this.id, "bool2"));	
	print("string1 is string? - " + objectIsVarString(this.id, "string1") + " \tvalue: " + objectGetVar(this.id, "string1"));	
	print("number1 is number? - " + objectIsVarNumber(this.id, "number1") + " \tvalue: " + objectGetVar(this.id, "number1"));	
	print("number2 is number? - " + objectIsVarNumber(this.id, "number2") + " \tvalue: " + objectGetVar(this.id, "number2"));	
	print("number3 is number? - " + objectIsVarNumber(this.id, "number3") + " \tvalue: " + objectGetVar(this.id, "number3"));	
	print("vector1 is vector? - " + objectIsVarVector(this.id, "vector1") + " \tvalue: " + objectGetVar(this.id, "vector1"));	
}


/**
 *  Update event
 */
this.onUpdate = function() {
	if (g_PauseState || Level.deltaTime == 0.0)
		return;	
	
	objectSetPosition(this.id, Player.pos.x, Player.pos.y + Player.height, Player.pos.z);

	if (Mouse.isEventAvailable()) {
		var halfWidth  = windowGetWidth()  / 2;
		var halfHeight = windowGetHeight() / 2;
		var dX, dY, dZ;
		var pitch = cameraGetPitch();
		var yaw   = cameraGetYaw();
		
		dX = (Mouse.getX() - halfWidth ) * this.rotSpeed;
		dY = (Mouse.getY() - halfHeight) * this.rotSpeed;	
		
		if (dX >  10.0) dX =  10.0;
		if (dX < -10.0) dX = -10.0;
		if (dY >  10.0) dY =  10.0;
		if (dY < -10.0) dY = -10.0;	

		yaw   += dX;
		pitch -= dY;
		if ((pitch > 45.0 ) && (pitch < 180.0)) pitch = 45.0;
		if ((pitch < 315.0) && (pitch > 180.0)) pitch = 315.0;

		Mouse.setPosition(halfWidth, halfHeight);

		cameraSetPitch(pitch);
		cameraSetYaw  (yaw);
		cameraUpdate();
	}
}


/**
 *  Destroy event
 */
this.onDestroy = function() {
}