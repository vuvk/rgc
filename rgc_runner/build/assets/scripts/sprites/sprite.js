
var spriteId;
var name;

var pos;

var newX,
	oldX;
var onNew = false;

var health;
var destroyable;
var deathSound, deathSoundName;

var animSpeed, animDelay;
var frame, maxFrame;
var playOnce;
var deleteOnLastFrame;
var isAnimPlay;

/**
 *  Initialization of object
 */
this.onInit = function() {	
	spriteId = objectGetId();
	name = objectGetVar(spriteId, "name");
	
	pos = new Vector3f(objectGetPosition(spriteId));
	
	health = objectGetVar(spriteId, "health");
	destroyable = objectGetVar(spriteId, "destroyable");
	if (destroyable) {
		objectEnableCollision(spriteId);
		
		deathSoundName = objectGetVar(spriteId, "deathSound");
		deathSound = soundCreate(deathSoundName);
	}
	playOnce = objectGetVar(spriteId, "playOnce");
	deleteOnLastFrame = objectGetVar(spriteId, "deleteOnLastFrame");
	
	animSpeed = objectGetVar(spriteId, "animationSpeed");
	animDelay = 0.0;
	isAnimPlay = !destroyable;
	
	frame = 0;
	maxFrame = objectGetFramesCount(spriteId) - 1;
	objectSetFrame(spriteId, frame);	
		
	newX = pos.x + 1;
	oldX = pos.x;
}


/**
 *  Update event
 */
this.onUpdate = function() {	
	if (g_PauseState || Level.deltaTime == 0.0)
		return;
	
	if (health > 0.0)
	{
		pos = new Vector3f(objectGetPosition(spriteId));
		if (!onNew)
		{
			if (pos.x < newX)
			{
				pos.x += Level.deltaTime;
			}
			else
				onNew = true;
		}
		else
		{
			if (pos.x > oldX)
			{
				pos.x -= Level.deltaTime;
			}
			else
				onNew = false;
		}
		objectSetPositionX(spriteId, pos.x);
	}
	
	/* update animation */
	if (animSpeed == 0.0)
		return;
	
	if (destroyable && !isAnimPlay) {
		health = objectGetVar(spriteId, "health");
		if (health <= 0.0) {
			isAnimPlay = true;
			
			/* play 3d sound and delete its */
			soundSetPosition(deathSound, pos.x, pos.y, pos.z);
			//soundSetGain(deathSound, 5.0);
			soundPlayOnce(deathSound);
		}
	}
	
	if (isAnimPlay) {
		if (Level.deltaTime > 0.0) {
			if (animDelay < 1.0)
				animDelay += Level.deltaTime * animSpeed;
			else {
				animDelay -= 1.0;
				++frame;

				if (frame > maxFrame) {
					frame = 0;
					
					if (destroyable) {
						frame = maxFrame;
						destroyable = false;
						isAnimPlay = false;
						animSpeed = 0.0;
						animDelay = 0.0;
						
						objectSetVar(spriteId, "destroyable",    destroyable);
						objectSetVar(spriteId, "animationSpeed", animSpeed  );
						objectDisableCollision(spriteId);					
					}
					
					if (playOnce) {
						frame = maxFrame;
						isAnimPlay = false;
						animSpeed = 0.0;
						animDelay = 0.0;	
						objectSetVar(spriteId, "animationSpeed", animSpeed  );					
					}
					
					if (deleteOnLastFrame)
						objectDestroy(spriteId);
				}
				
				objectSetFrame(spriteId, frame);
			}
		}
	}	
}


/**
 *  Destroy
 */
this.onDestroy = function() {
}
