var doorId;

var pos, start, end, dir;
var moveVector;

var isOpened = false;
var isMoving = false;

var distToPlayer;

var needKeySound1, needKeySound2;

var openSpeed;
var stayOpened;
var needKey;

var startSound, startSoundName,
	moveSound,  moveSoundName, 
	stopSound,  stopSoundName;

var animSpeed, animDelay;
var frame, maxFrame;

/* time of stay on opened position */
var  delay = 1.0;
var _delay = 0.0;

function startOpenDoor() {
	isMoving = true;
	//print("NOW OPENING!");
	soundPlay(startSound);
	soundStop(moveSound);
	soundStop(stopSound);
	
	/* calculate new move direction */
	moveVector = (end.sub(start)).normalize();
}

function startCloseDoor() {
	isMoving = true;
	_delay = 0;		
	//print("NOW CLOSING!");
	soundPlay(startSound);
	soundStop(moveSound);
	soundStop(stopSound);
	
	/* calculate new move direction */
	moveVector = (start.sub(end)).normalize();
}

function stopDoor(positionForStop) {
	isMoving = false;
		
	objectSetPosition(doorId, positionForStop.x, positionForStop.y, positionForStop.z);
	soundStop(startSound);
	soundStop(moveSound);
	soundPlay(stopSound);
}

/**
 *  Initialization of object
 */
this.onInit = function() {
	doorId = objectGetId();

	/* get start position */
	pos = new Vector3f(objectGetPosition(doorId));

	/* save start position */
	start = new Vector3f(pos.x, pos.y, pos.z);

	/* check direction */
	var isVertical   = ((mapIsPlaceFree(pos.x - 1, -pos.z)) && (mapIsPlaceFree(pos.x + 1, -pos.z)));
	var isHorizontal = ((mapIsPlaceFree(pos.x, -pos.z - 1)) && (mapIsPlaceFree(pos.x, -pos.z + 1)));

	openSpeed  = objectGetVar(doorId, "openSpeed" );
	stayOpened = objectGetVar(doorId, "stayOpened");
	needKey    = objectGetVar(doorId, "needKey"   );
	
	var moveTo = 0.95;
	if (openSpeed < 0) 
		moveTo = -moveTo;

	/* save direction for openning */
	if (isHorizontal) {
		end = new Vector3f(pos.x - moveTo, pos.y, pos.z);
	}
	else
		if (isVertical) {
			end = new Vector3f(pos.x, pos.y, pos.z + moveTo);
		}		

	/* move direction */
	dir = new Vector3f();
	
	/* sounds */
	startSoundName = objectGetVar(doorId, "startSound");
	moveSoundName  = objectGetVar(doorId, "moveSound");
	stopSoundName  = objectGetVar(doorId, "stopSound");
	
	startSound = soundCreate(startSoundName, pos.x, pos.y, pos.z);
	moveSound  = soundCreate(moveSoundName,  pos.x, pos.y, pos.z);
	stopSound  = soundCreate(stopSoundName,  pos.x, pos.y, pos.z);
	
	needKeySound1 = soundCreate("need_key1");
	needKeySound2 = soundCreate("need_key2");
	
	animSpeed = objectGetVar(doorId, "animationSpeed");
	animDelay = 0.0;
	
	frame = 0;
	maxFrame = objectGetFramesCount(doorId) - 1;
	objectSetFrame(doorId, frame);	
}

/**
 *  Update event
 */
this.onUpdate = function() {
	if (g_PauseState || Level.deltaTime == 0.0)
		return;
	
	/* update animation */
	if (animSpeed != 0.0) {		
		if (animDelay < 1.0)
			animDelay += Level.deltaTime * animSpeed;
		else {
			animDelay -= 1.0;
			++frame;

			if (frame > maxFrame) 
				frame = 0;
			
			objectSetFrame(doorId, frame);
		}
	}
	
	if (!isMoving) {
		distToPlayer = distanceBetweenPoints(start.x, start.z, Player.pos.x, Player.pos.z);
		
		if (!isOpened) {	
			if (distToPlayer <= 1.0 && objectIsInView(doorId))
				if (Keyboard.isEventAvailable() && Keyboard.isKeyHit(VK_SPACE)) {
					if (!needKey) {		
						startOpenDoor();
					}
					else {
						var keyName = objectGetVar(doorId, "keyName");
						var keyExists = objectGetVar(Player.id, keyName);
						
						if (keyExists == true) {
							needKey = false;
							startOpenDoor();
						}
						else {
							var message = objectGetVar(doorId, "message");
							print("Message: '" + message + "'");
							
							if (!soundIsPlaying(needKeySound1) && !soundIsPlaying(needKeySound2)) {
								var chance = Math.floor(Math.random() * 2); 
								switch (chance) {
									case 0 : soundPlay(needKeySound1); break;
									case 1 : soundPlay(needKeySound2); break;
								}
							}
						}
					}
				}
		}
		else {	// is opened
			if (!stayOpened) {
				if (distToPlayer > 2.0) {   // wait when player is far
					if (_delay < delay)
						_delay += Level.deltaTime;
					else {
						startCloseDoor();
					}
				}
				else
					_delay = 0;
			}
		}
	}

	if (isMoving && Level.deltaTime > 0.0) {		
		/* loop moving sound */
		if (!soundIsPlaying(startSound) && !soundIsPlaying(moveSound)) {
			soundStop(startSound);
			soundPlay(moveSound, true);
			soundStop(stopSound);
		}
		
		var moveStep = openSpeed * Level.deltaTime;
		if (!isOpened) {		
			var distToEnd = distanceBetweenVectors(pos, end);
			if (distToEnd > Math.abs(moveStep)) {
				var dir = moveVector.mul(moveStep);
				if (openSpeed > 0)
					pos = pos.add(dir);
				else
					pos = pos.sub(dir);
				objectSetPosition(doorId, pos.x, pos.y, pos.z);
			}
			else {
				/* STOP! */
				isOpened = true;
				stopDoor(end);
			}
		}
		else {
			distToPlayer = distanceBetweenPoints(start.x, start.z, Player.pos.x, Player.pos.z);
			if (distToPlayer <= 2.0) {// open the closing door as the player approaches
				isOpened = false;
				startOpenDoor();
			}
			var distToStart = distanceBetweenVectors(pos, start);
			if (distToStart > Math.abs(moveStep)) {
				var dir = moveVector.mul(moveStep);
				if (openSpeed > 0)
					pos = pos.add(dir);
				else
					pos = pos.sub(dir);
				objectSetPosition(doorId, pos.x, pos.y, pos.z);
			}
			else {
				/* STOP! */
				isOpened = false;
				stopDoor(start);
			}		
		}		
	}	
}

/**
 *  Destroy event
 */
this.onDestroy = function() {
}