#include "WorldSim3D.h"
#include "SampleFunctions.h"


///****************************************************************///
bool CheckFilePath(char* path)
{
	if (!wFileIsExist(path))
    {
		wEngineStop(true);
		//'cls
		Int32 def=wConsoleSaveDefaultColors();
		wConsoleSetFontColor(wCFC_RED);
		printf("\nFile << %s >> not found!\n",path);
		wConsoleSetFontColor(wCFC_YELLOW);
		printf("Warning! Check the paths to your resources!\n");
		wConsoleResetColors(def);
		printf("\n");
		printf("Press any key to exit...\n");
		wInputWaitKey();
		return false;
    }
    return true;
}

void PrintWithColor(char* text,
					wConsoleFontColor color,
					bool waitKey)
{
	Int32 def=wConsoleSaveDefaultColors();
	wConsoleSetFontColor(color);
	printf("%s\n",text);
	wConsoleResetColors(def);
	if (waitKey)
    {
 		printf("\n");
		printf("Press any key...\n");
		wInputWaitKey();
    }
}

static void FindAndSetEntity(UInt32* entityList, char* entityName)
{
    UInt32* varGroup;
    wVector2i entityIndex;
    ///Для сплайн-аниматора по двум точкам
    wVector3f points[2];
	///скорость работы сплайн-аниматора
	Float32 splineSpeed=1.0f;
	///смещение модели по вертикали
	Int32 splineDelta=4;
    ///для сплайн-аниматора
	Float32 Tightness=0.5;
 	///Скорость работы rotate-аниматора
	Float32 rotSpeed=1.5;
    wNode* node=0;
    wMesh* mesh=0;
    ///позиция установки объекта
    wVector3f pos;
    ///угол поворота
    Float32 angle = 0;
    //имя модели
    const char* modelName;
    UInt32 parsePos=0;
    Int32 count=0;
    wchar_t tempBuffer[256] = {0};
    bool isModel=true;
    int i;

    entityIndex=wBspGetEntityIndexByName(entityList,entityName);

    if(entityIndex.x==-1) return;

    printf("\nFind and set entity: ");
    printf(entityName);
    printf("\nfirstIndex=");
    printf("%d\n",entityIndex.x);
    printf("lastIndex=");
    printf("%d\n",entityIndex.y);

    for(i=entityIndex.x;i<entityIndex.y+1;++i)
    {
         varGroup=wBspGetVarGroupByIndex(entityList,i);

         parsePos=0;

         pos=wBspGetVarGroupValueAsVec(varGroup,"origin",parsePos);

         if(strcmp(entityName, "info_player_start") == 0)
         {
             isModel=false;

             swprintf(tempBuffer, L"PLAYER START");

             wVector2f size={128.0f,12.0f};
             wColor4s topColor={255,255,0,0};
             wColor4s bottomColor={255,64,0,0};

             wNode* BillBoard=wBillboardCreateText(pos,
                                                      size,
                                                      wFontGetDefault(),
                                                      tempBuffer,
                                                      topColor,
                                                      bottomColor);
           };

           if(strcmp(entityName, "info_player_deathmatch") == 0)
           {
                isModel=false;

                swprintf(tempBuffer, L"%s %d", L"PLAYER DEATHMATCH No ", count+1);

                parsePos=0;

                angle=wBspGetVarGroupValueAsFloat(varGroup,"angle",parsePos);

                wVector2f size={128.0f,12.0f};
                wColor4s topColor={255,64,64,0};
                wColor4s bottomColor={255,255,255,0};

                wNode* BillBoard=wBillboardCreateText(pos,
                                                      size,
                                                      wFontGetDefault(),
                                                      tempBuffer,
                                                      topColor,
                                                      bottomColor);

                if(i==entityIndex.y)
                {
                    Float32 radians=wMathDegToRad(angle);
                    wNode* cam=wSceneGetActiveCamera();
                    pos.y+=10;
                    wNodeSetPosition(cam,pos);
                    pos.x+=100*sin(radians);
                    pos.z+=100*cos(radians);
                    wCameraSetTarget(cam,pos);
                    pos.x=0;
                    pos.y=0;
                    pos.z=-50;
                    wNodeMove(cam,pos);
                };
                ++count;
            };

            if(strcmp(entityName, "misc_model") == 0)
            {
                modelName=wBspGetVarGroupValueAsString(varGroup,"model");
            };

            if((strcmp(entityName, "weapon_railgun") == 0) ||
               (strcmp(entityName, "weapon_plasmagun") == 0) ||
               (strcmp(entityName, "weapon_rocketlauncher") == 0) ||
               (strcmp(entityName, "weapon_shotgun") == 0) ||
               (strcmp(entityName, "ammo_rockets") == 0) ||
               (strcmp(entityName, "ammo_shells") == 0))
            {

              modelName=entityName;

              if(strcmp(entityName, "weapon_railgun") == 0)
                    mesh=wMeshLoadEx("models/weapons2/railgun/railgun.md3");
              if(strcmp(entityName, "weapon_plasmagun") == 0)
                    mesh=wMeshLoadEx("models/weapons2/plasma/plasma.md3");
              if(strcmp(entityName, "weapon_rocketlauncher") == 0)
                    mesh=wMeshLoadEx("models/weapons2/rocketl/rocketl.md3");
              if(strcmp(entityName, "weapon_shotgun") == 0)
                    mesh=wMeshLoadEx("models/weapons2/shotgun/shotgun.md3");
              if(strcmp(entityName, "ammo_rockets") == 0)
                    mesh=wMeshLoadEx("models/powerups/ammo/rocketam.md3");
              if(strcmp(entityName, "ammo_shells") == 0)
                    mesh=wMeshLoadEx("models/weapons2/shells/m_shell.md3");

              if(mesh)
                    node=wNodeCreateFromMesh(mesh);

                if(node)
                {
                    wNodeSetPosition(node,pos);
                    wAnimatorRotationCreate(node,(wVector3f){0,rotSpeed,0});

                    points[0].x=pos.x;
                    points[0].y=pos.y+splineDelta;
                    points[0].z=pos.z;
                    points[1].x=pos.x;
                    points[1].y=pos.y-splineDelta;
                    points[1].z=pos.z;
                    wAnimatorSplineCreate(node,2,&points[0],1,splineSpeed,Tightness);

                    wNodeSetName(node,modelName);

                    if(strcmp(entityName, "ammo_shells") == 0)
                        wNodeSetScale(node,(wVector3f){5,5,5});

                };//end if
            };//end if

            if(isModel)
                printf(modelName);

            printf("\n");
            printf("Position: ");
            printf("%g, %g, %g\n",pos.x,pos.y,pos.z);
            printf("Angle: ");
            printf("%g\n",angle);

     };//end for

}

///Процедура парсинга BSP-карты
void ParseQuakeMap(wMesh* BSPMesh)
{
    UInt32* varGroup;

    UInt32* list=wBspGetEntityList(BSPMesh);

    FindAndSetEntity(list, "info_player_start");

    FindAndSetEntity(list, "info_player_deathmatch");

    FindAndSetEntity(list, "misc_model");

    FindAndSetEntity(list, "weapon_plasmagun");

    FindAndSetEntity(list, "weapon_railgun");

    FindAndSetEntity(list, "weapon_rocketlauncher");

    FindAndSetEntity(list, "weapon_shotgun");

    FindAndSetEntity(list, "ammo_rockets");

    FindAndSetEntity(list, "ammo_shells");

}

/// вспомогательная функция
static void SetEntityObject(const char* name,char* file,wVector3f pos)
{
    wMesh* mesh=0;
    wNode* node=0;
    wVector3f points[2];
	Float32 splineSpeed=1.0f;
	Int32 splineDelta=4;
	Float32 Tightness=0.5;
	Float32 rotSpeed=1.5;

    mesh=wMeshLoadEx(file);
    if(mesh)
     node=wNodeCreateFromMesh(mesh);
    if(node)
      {
          wNodeSetPosition(node,pos);
          wAnimatorRotationCreate(node,(wVector3f){0,rotSpeed,0});

          points[0].x=pos.x;
          points[0].y=pos.y+splineDelta;
          points[0].z=pos.z;
          points[1].x=pos.x;
          points[1].y=pos.y-splineDelta;
          points[1].z=pos.z;
          wAnimatorSplineCreate(node,2,&points[0],1,splineSpeed,Tightness);

          wNodeSetName(node,name);

       };//end if
}

///Другой вариант парсинга BSP-карты
void ParseQuakeMap2(wMesh* BSPMesh)
{
    UInt32* varGroup=0;
    wchar_t tempBuffer[256] = {0};

    UInt32* list=wBspGetEntityList(BSPMesh);
    for(Int32 i=0;i<wBspGetEntityListSize(list);++i)
    {
        varGroup=wBspGetVarGroupByIndex(list,i);
        const char* name=wBspGetVarGroupValueAsString(varGroup,"classname");

         if(!strcmp(name,"info_player_deathmatch"))
        {
             swprintf(tempBuffer, L"%s", L"PLAYER DEATHMATCH");
             wVector3f pos=wBspGetVarGroupValueAsVec(varGroup,"origin",0);
             Float32 angle=wBspGetVarGroupValueAsFloat(varGroup,"angle",0);

             wVector2f size={128.0f,12.0f};
             wColor4s topColor={255,64,64,0};
             wColor4s bottomColor={255,255,255,0};

             wNode* BillBoard=wBillboardCreateText(pos,
                                                      size,
                                                      wFontGetDefault(),
                                                      tempBuffer,
                                                      topColor,
                                                      bottomColor);
              Float32 radians=wMathDegToRad(angle);
              wNode* cam=wSceneGetActiveCamera();
              pos.y+=10;
              wNodeSetPosition(cam,pos);
              pos.x+=100*sin(radians);
              pos.z+=100*cos(radians);
              wCameraSetTarget(cam,pos);
              pos.x=0;
              pos.y=0;
              pos.z=-50;
              wNodeMove(cam,pos);
        }

        if(!strcmp(name,"weapon_plasmagun"))
        {
             wVector3f pos=wBspGetVarGroupValueAsVec(varGroup,"origin",0);
             SetEntityObject("weapon_plasmagun","models/weapons2/plasma/plasma.md3",pos);
        }

        if(!strcmp(name,"weapon_shotgun"))
        {
             wVector3f pos=wBspGetVarGroupValueAsVec(varGroup,"origin",0);
             SetEntityObject("weapon_shotgun","models/weapons2/shotgun/shotgun.md3",pos);
        }

        if(!strcmp(name,"weapon_rocketlauncher"))
        {
             wVector3f pos=wBspGetVarGroupValueAsVec(varGroup,"origin",0);
             SetEntityObject("weapon_rocketlauncher","models/weapons2/rocketl/rocketl.md3",pos);
        }

        if(!strcmp(name,"weapon_railgun"))
        {
             wVector3f pos=wBspGetVarGroupValueAsVec(varGroup,"origin",0);
             SetEntityObject("weapon_railgun","models/weapons2/railgun/railgun.md3",pos);
        }

    }
}

void SetQuakeShadersVisible(bool value)
{
    wNode* root=wSceneGetRootNode();
    UInt32 iter;
    wNode* node=wNodeGetFirstChild(root,&iter);

    while(!wNodeIsLastChild(root,&iter))
    {
        node=wNodeGetNextChild(root,&iter);
        if(node!=0)
        {
            if(wNodeGetType(node)==wSNT_Q3SHADER_SCENE_NODE)
            {
                wNodeSetVisibility(node,value);
            };
        };
    };
}

wMesh* wMeshLoadEx(char* path)
{
    wMesh* mesh=0;
    if(wSceneIsMeshLoaded(path))
    {
        mesh=wSceneGetMeshByName(path);
        if(mesh!=0)
            return mesh;
    }
    if(wFileIsExist(path))
        mesh=wMeshLoad(path, false);
    return mesh;
}

void wMeshesUpdateTangentsAndBinormal()
{
    for(UInt32 i=0;i<wSceneGetMeshesCount();++i)
    {
        wMesh* mesh=wSceneGetMeshByIndex(i);
        if(wMeshGetType(mesh)==wAMT_SKINNED||wAMT_MD2||wAMT_MD3||wAMT_MDL_HALFLIFE)
        {
            wMeshUpdateTangentsAndBinormals(mesh);
        }
    }
}

wTexture* CreateColorTexture(wColor4s tColor, const char* name)
{
    wTexture* t=wTextureCreate(name, (wVector2i){1,1}, wCF_A8R8G8B8);
    UInt32* lock=wTextureLock(t);
    *lock=wUtilColor4sToUInt(tColor);
    wTextureUnlock(t);
    return t;
}

void OutlineNode(wNode* node,Float32 Width,wColor4s lineColor)
{
		if(!node)return;
		bool vis=wNodeIsVisible(node);
		wNodeSetVisibility(node,true);
		UInt32 mCount=wNodeGetMaterialsCount(node);
		typedef struct
		{
		    wColor4s diffuseColor;
		    wColor4s emissiveColor;
		    wColor4s ambientColor;
		    wColor4s specularColor;
		    bool backfaceCulling;
		    bool frontfaceCulling;
		    Float32 thickness;
		    bool lighting;
		    bool wireframe;
		}wMaterialInfo;

        wMaterialInfo curMat[mCount];

        wMaterialInfo matInfo;

		matInfo.diffuseColor     = (wColor4s){lineColor.alpha,lineColor.red,lineColor.green,lineColor.blue};
		matInfo.specularColor    = (wColor4s){lineColor.alpha,lineColor.red,lineColor.green,lineColor.blue};
		matInfo.ambientColor     = (wColor4s){lineColor.alpha,lineColor.red,lineColor.green,lineColor.blue};
		matInfo.emissiveColor    = (wColor4s){lineColor.alpha,lineColor.red,lineColor.green,lineColor.blue};
		matInfo.backfaceCulling  = false;
		matInfo.frontfaceCulling = true;
		matInfo.lighting = true;
		matInfo.thickness = Width;
		matInfo.wireframe = true;

		for(Int32 i = 0;i<mCount;++i)
		{
			wMaterial* mat              =   wNodeGetMaterial(node,i);
			curMat[i].diffuseColor      =   wMaterialGetDiffuseColor(mat);
			curMat[i].specularColor     =   wMaterialGetSpecularColor(mat);
			curMat[i].ambientColor      =   wMaterialGetAmbientColor(mat);
			curMat[i].emissiveColor     =   wMaterialGetEmissiveColor(mat);
			curMat[i].thickness         =   wMaterialGetLineThickness(mat);
            curMat[i].wireframe         =   wMaterialGetFlag(mat,wMF_WIREFRAME);
            curMat[i].backfaceCulling   =   wMaterialGetFlag(mat,wMF_BACK_FACE_CULLING);
            curMat[i].frontfaceCulling  =   wMaterialGetFlag(mat,wMF_FRONT_FACE_CULLING);
            curMat[i].lighting          =   wMaterialGetFlag(mat,wMF_LIGHTING);

            wMaterialSetDiffuseColor(mat,matInfo.diffuseColor);
            wMaterialSetSpecularColor(mat,matInfo.specularColor);
            wMaterialSetAmbientColor(mat,matInfo.ambientColor);
            wMaterialSetEmissiveColor(mat,matInfo.emissiveColor);
            wMaterialSetLineThickness(mat,matInfo.thickness);
            wMaterialSetFlag(mat,wMF_WIREFRAME,matInfo.wireframe);
            wMaterialSetFlag(mat,wMF_LIGHTING,matInfo.lighting);
            wMaterialSetFlag(mat,wMF_FRONT_FACE_CULLING,matInfo.frontfaceCulling);
            wMaterialSetFlag(mat,wMF_BACK_FACE_CULLING,matInfo.backfaceCulling);
		}

		wNodeDraw(node);

        for(Int32 i=0;i<mCount;++i)
        {
            wMaterial* mat = wNodeGetMaterial(node,i);
            wMaterialSetDiffuseColor(mat,curMat[i].diffuseColor);
            wMaterialSetSpecularColor(mat,curMat[i].specularColor);
            wMaterialSetAmbientColor(mat,curMat[i].ambientColor);
            wMaterialSetEmissiveColor(mat,curMat[i].emissiveColor);
            wMaterialSetLineThickness(mat,curMat[i].thickness);
            wMaterialSetFlag(mat,wMF_WIREFRAME,curMat[i].wireframe);
            wMaterialSetFlag(mat,wMF_LIGHTING,curMat[i].lighting);
            wMaterialSetFlag(mat,wMF_FRONT_FACE_CULLING,curMat[i].frontfaceCulling);
            wMaterialSetFlag(mat,wMF_BACK_FACE_CULLING,curMat[i].backfaceCulling);
        }

		wNodeSetVisibility(node,vis);
}

wNode* CreateBullet(Float32 speed,wTexture* texture)
{

	wNode* camera=wSceneGetActiveCamera();
	if(!camera)return 0;
	//'Position of the target.
	wVector3f target,position;
	target=wCameraGetTarget(camera);
	position=wNodeGetAbsolutePosition(camera);
	//'Create a sphere.
	wNode* bulletNode=wNodeCreateSphere(10.f, 16, wCOLOR4s_WHITE);
	wMaterial* mat=wNodeGetMaterial(bulletNode,0);
	wMaterialSetFlag(mat,wMF_LIGHTING,false);
	wMaterialSetTexture(mat,0,texture);
	//'Create a body type field.
	wVector3f radius={10,10,10};
	wNode* bulletBody =wPhysBodyCreateSphere(radius,5.f);
	wNodeSetParent(bulletBody,bulletNode);
	wNodeSetPosition(bulletBody,position);
	wPhysBodySetName(bulletBody,"bullet");

	target.x-=position.x;
	target.y-=position.y;
	target.z-=position.z;
	target=wMathVector3fNormalize(target);
	target.x*=speed;
	target.y*=speed;
	target.z*=speed;
	wPhysBodySetLinearVelocity(bulletBody,target);
	target.x=wMathRandomRange(-10,10);
	target.y=wMathRandomRange(-10,10);
	target.z=wMathRandomRange(-10,10);
	wPhysBodySetAngularVelocity(bulletBody,target);
    return bulletBody;
}

bool MousePick(bool isPick)
{
    wVector2i mPos;
    wInputGetMousePosition(&mPos);
    wNode* tempNode=wPhysGetBodyPicked(mPos,isPick);
    if(tempNode)
    {
        if(isPick)
        {
            if(wPhysBodyGetMass(tempNode)>0)
            {
                wPhysBodyDraw(tempNode);
                return true;
            }
        }
    }
    return false;
}

void CreateGround(Float32 scale)
{
    wMaterial* material=0;
	Int32 matGround=wPhysMaterialCreate();
	wPhysMaterialSetFriction(matGround,matGround,0.9f,0.9f);

	wMesh* mesh[6];
	wNode* body[6];
	wTexture* tex=wTextureLoad("../../Assets/Textures/rockwall.bmp");
	wTexture* tex2=wTextureLoad("../../Assets/Textures/brick_1.jpg");

    ///1
	mesh[0] = wNodeCreateCube(1, wCOLOR4s_WHITE);
	wNodeSetScale(mesh[0],{512*scale,20,1*scale});
	wNodeSetPosition(mesh[0],{0*scale,10,256*scale});
	material=wNodeGetMaterial(mesh[0],0);
	wMaterialSetTexture(material,0,tex2);
	wMaterialSetFlag(material,wMF_LIGHTING,false);
	wMaterialScaleTexture(material,0,{4*scale,2*scale});

	body[0] = wPhysBodyCreateCube({512*scale,20,1*scale},0);
	wNodeSetParent(body[0],mesh[0]);
	wNodeSetPosition(body[0],{0*scale,10,256*scale});
	wPhysBodySetMaterial(body[0],matGround);

	///2
	mesh[1] = wNodeCreateCube(1, wCOLOR4s_WHITE);
	wNodeSetScale(mesh[1],{1*scale,20,512*scale});
	wNodeSetPosition(mesh[1],{256*scale,10,0*scale});
	material=wNodeGetMaterial(mesh[1],0);
	wMaterialSetTexture(material,0,tex2);
	wMaterialSetFlag(material,wMF_LIGHTING,false);
	wMaterialScaleTexture(material,0,{4*scale,2*scale});

	body[1] = wPhysBodyCreateCube({1*scale,20,512*scale},0);
	wNodeSetParent(body[1],mesh[1]);
	wNodeSetPosition(body[1],{256*scale,10,0*scale});
	wPhysBodySetMaterial(body[1],matGround);

	///3
	mesh[2] = wNodeCreateCube(1, wCOLOR4s_WHITE);
	wNodeSetScale(mesh[2],{512*scale,20,1*scale});
	wNodeSetPosition(mesh[2],{0*scale,10,-256*scale});
	material=wNodeGetMaterial(mesh[2],0);
	wMaterialSetTexture(material,0,tex2);
	wMaterialSetFlag(material,wMF_LIGHTING,false);
	wMaterialScaleTexture(material,0,{4*scale,2*scale});

	body[2] = wPhysBodyCreateCube({512*scale,20,1*scale},0);
	wNodeSetParent(body[2],mesh[2]);
	wNodeSetPosition(body[2],{0*scale,10,-256*scale});
	wPhysBodySetMaterial(body[2],matGround);

	///4
	mesh[3] = wNodeCreateCube(1, wCOLOR4s_WHITE);
	wNodeSetScale(mesh[3],{1*scale,20,512*scale});
	wNodeSetPosition(mesh[3],{-256*scale,10,0*scale});
	material=wNodeGetMaterial(mesh[3],0);
	wMaterialSetTexture(material,0,tex2);
	wMaterialSetFlag(material,wMF_LIGHTING,false);
	wMaterialScaleTexture(material,0,{4*scale,2*scale});

	body[3] = wPhysBodyCreateCube({1*scale,20,512*scale},0);
	wNodeSetParent(body[3],mesh[3]);
	wNodeSetPosition(body[3],{-256*scale,10,0*scale});
	wPhysBodySetMaterial(body[3],matGround);

	///5
	mesh[4] = wNodeCreateCube(1, wCOLOR4s_WHITE);
	wNodeSetScale(mesh[4],{512*scale,1,512*scale});
	wNodeSetPosition(mesh[4],{0*scale,0,0*scale});
	material=wNodeGetMaterial(mesh[4],0);
	wMaterialSetTexture(material,0,tex);
	wMaterialSetFlag(material,wMF_LIGHTING,false);
	wMaterialScaleTexture(material,0,{(8*scale),(8*scale)});

	body[4] = wPhysBodyCreateCube({512*scale,1,512*scale},0);
	wNodeSetParent(body[4],mesh[4]);
	wNodeSetPosition(body[4],{0*scale,0,0*scale});
	wPhysBodySetMaterial(body[4],matGround);

	wPhysBodySetName(body[4],"ground");

	wNodeSetName(mesh[4],"ground");
}
#define _NO_SOUND_
#ifndef _NO_SOUND_
wSound* wSoundLoadEx(const char* pathFile,const char* extFile)
{
    wFile* snd                      =0;
    Int32  sndLen                   =0;
    void*  sndBuf                   =0;
    wSound* sound                   =0;
    snd=wFileOpenForRead(pathFile);
    sndLen=wFileGetSize(snd);
    sndBuf=calloc(sndLen,sizeof(const char));
    wFileRead(snd,sndBuf,sndLen);
    sound=wSoundLoadFromMemory(wFileGetAbsolutePath(pathFile),(const char*)sndBuf,sndLen,extFile);
    free(sndBuf);
    wFileClose(snd);
    return sound;
}

wSoundBuffer* wSoundBufferLoadEx(const char* pathFile,const char* extFile)
{
    wFile* snd                      =0;
    Int32  sndLen                   =0;
    void*  sndBuf                   =0;
    wSoundBuffer* soundBuffer       =0;
    snd=wFileOpenForRead(pathFile);
    sndLen=wFileGetSize(snd);
    sndBuf=calloc(sndLen,sizeof(const char));
    wFileRead(snd,sndBuf,sndLen);
    soundBuffer=wSoundBufferLoadFromMemory((const char*)sndBuf,sndLen,extFile);
    free(sndBuf);
    wFileClose(snd);
    return soundBuffer;
}
#endif

///For shaders
wMaterialTypes ShaderGetMaterialType(wShader* shader)
{
    return (wMaterialTypes)shader->material_type;
}

///Normal mapping for bsp
void wMeshPrepareToNormalMapping(wMesh* mesh,
                                 wMaterialTypes matType,
                                 UInt32 srcTextureLayer,
                                 UInt32 dstTextureLayer,
                                 Float32 matParam,
                                 Float32 amplitude,
                                 Float32 blurRadius,
                                 Float32 contrast,
                                 Int32 brightness)
{
    if(!mesh)return;
    if(wMeshIsEmpty(mesh))return;
    wMaterial* material=0;
    wMeshBuffer* meshBuffer=0;
    char tmpBuf[256]={0};
    if(srcTextureLayer>6)srcTextureLayer=6;
    if(dstTextureLayer>7)dstTextureLayer=7;
    UInt32 bufCount=wMeshGetBuffersCount(mesh,0);
    printf("\nMeshBuffers count: %d\n",bufCount);
    for(Int32 i=0;i<bufCount;++i)
    {
        meshBuffer=wMeshGetBuffer(mesh,0,i);
        material=wMeshBufferGetMaterial(meshBuffer);
        wTexture* texture=wMaterialGetTexture(material,srcTextureLayer);
        if(texture)
        {
            const char* tName=wTextureGetFullName(texture);
            printf("\nTexture name: %s\n",tName);
            sprintf(tmpBuf,"%s_copy",tName);
            wTexture* texture_copy=wTextureCopy(texture,tmpBuf);
            wTextureSetBrightness(&texture_copy,brightness);
            wTextureSetBlur(&texture_copy,blurRadius);
            wTextureSetContrast(&texture_copy,contrast);
            wTextureMakeNormalMap(texture_copy,amplitude);
            wMaterialSetTexture(material,dstTextureLayer,texture_copy);
        }
        wMaterialSetTypeParameter(material,matParam);
        wMaterialSetType(material,matType);
    }
}
///=====================================///
#ifdef __cplusplus
    UInt32 wArray::size()
    {
        return ArraySize;
    }

    UInt32* wArray::get(UInt32 idx)
    {
        if(idx>ArraySize-1)return 0;
        return Array[idx];
    }

    Int32 wArray::getIndex(UInt32* element)
    {
        for(Int32 i=0;i<ArraySize;++i)
        {
            if(Array[i]==element)
                return i;
        }
        return -1;
    }

    bool wArray::add(UInt32* element)
    {

        Array=(UInt32**)realloc(Array,(ArraySize+1)*sizeof(UInt32*));
        if(Array!=NULL)
        {
            ArraySize+=1;
            Array[ArraySize-1]=element;
            return true;
        }
         return false;
    }

    bool wArray::insert(UInt32* element, UInt32 idx)
    {
        if(idx>ArraySize-1)
            return add(element);
        else
        {
            UInt32** temp=(UInt32**)calloc(ArraySize+1,sizeof(UInt32*));
            if(temp==NULL) return false;
            for(Int32 i=0;i<idx;++i)
            {
                temp[i]=Array[i];
            }
            temp[idx]=element;
            for(Int32 i=idx;i<ArraySize;++i)
            {
                temp[i+1]=Array[i];
            }
            free(Array);
            Array=temp;
            ArraySize+=1;
            return true;
        }
    }

    bool wArray::remove(UInt32* element,bool isNodeDestroy)
    {
        for(Int32 i=0;i<size();++i)
        {
            if(Array[i]==element)
                return remove(i,isNodeDestroy);
        }
        return false;
    }

    bool wArray::remove(UInt32 idx,bool isNodeDestroy)
    {
        if(idx>ArraySize-1)return false;

        UInt32** temp=(UInt32**)calloc(ArraySize-1,sizeof(UInt32*));

        if(temp==NULL) return false;

        for(Int32 i=0;i<idx;++i)
        {
            temp[i]=Array[i];
        }
        for(Int32 i=idx;i<ArraySize-1;++i)
        {
            temp[i]=Array[i+1];
        }

        if(isNodeDestroy)
        {
            wNode* node=get(idx);
            wNodeDestroy(node);
        }

        free(Array);
        Array=temp;
        ArraySize-=1;
        return true;
    }

    void wArray::clear(bool isNodeDestroy)
    {
        if(isNodeDestroy)
        {
            for(Int32 i=0;i<ArraySize;++i)
            {
                wNode* node=get(i);
                wNodeDestroy(node);
            }
        }
        free(Array);
        ArraySize=0;
    }
#else
typedef struct
{
    UInt32** array;
    UInt32   arraySize;
} wArray_t;

wArray* wArrayCreate()
{
    return calloc(sizeof(wArray_t), 1);
}

UInt32 wArrayGetSize(wArray* array)
{
    if (array == NULL)
        return 0;

    return ((wArray_t*)array)->arraySize;
}

void* wArrayGetElement(wArray* array, UInt32 idx)
{
    if (array == NULL)
        return NULL;

    wArray_t* arr = array;

    if (idx > arr->arraySize - 1)
        return NULL;

    return arr->array[idx];
}

Int32 wArrayGetIndex(wArray* array, void* element)
{
    if (array == NULL)
        return -1;

    wArray_t* arr = array;

    for (int i = 0; i < arr->arraySize; ++i)
    {
        if (arr->array[i] == element)
            return i;
    }

    return -1;
}

bool wArrayAddElement(wArray* array, void* element)
{
    if (array == NULL)
        return false;

    wArray_t* arr = array;

    arr->array = realloc(arr->array,(arr->arraySize + 1) * sizeof(UInt32*));
    if (arr->array != NULL)
    {
        ++arr->arraySize;
        arr->array[arr->arraySize - 1] = element;
        return true;
    }

    return false;
}

bool wArrayInsertElement(wArray* array, void* element, UInt32 idx)
{
    if (array == NULL)
        return false;

    wArray_t* arr = array;

    if (idx > arr->arraySize - 1)
        return wArrayAddElement(array, element);
    else
    {
        UInt32** temp = calloc(arr->arraySize + 1, sizeof(UInt32*));
        if (temp == NULL)
            return false;

        for (int i = 0; i < idx; ++i)
        {
            temp[i] = arr->array[i];
        }

        temp[idx] = element;

        for (int i = idx; i < arr->arraySize; ++i)
        {
            temp[i + 1] = arr->array[i];
        }

        free(arr->array);
        arr->array = temp;
        ++arr->arraySize;

        return true;
    }

    return false;
}

bool wArrayRemoveElement(wArray* array, UInt32* element, bool isNodeDestroy)
{
    if (array == NULL)
        return false;

    wArray_t* arr = array;

    for (int i = 0; i < arr->arraySize; ++i)
    {
        if (arr->array[i] == element)
            return wArrayRemoveElementByIndex(array, i, isNodeDestroy);
    }

    return false;
}

bool wArrayRemoveElementByIndex(wArray* array, UInt32 idx, bool isNodeDestroy)
{
    if (array == NULL)
        return false;

    wArray_t* arr = array;

    if (idx > arr->arraySize - 1)
        return false;

    UInt32** temp = calloc(arr->arraySize - 1, sizeof(UInt32*));
    if (temp == NULL)
        return false;

    for (int i = 0; i < idx; ++i)
    {
        temp[i] = arr->array[i];
    }

    for (int i = idx; i < arr->arraySize - 1; ++i)
    {
        temp[i] = arr->array[i + 1];
    }

    if (isNodeDestroy)
    {
        wNode* node = arr->array[idx];
        wNodeDestroy(node);
    }

    free(arr->array);
    arr->array = temp;
    --arr->arraySize;

    return true;
}

void wArrayClear(wArray* array, bool isNodeDestroy)
{
    if (array == NULL)
        return;

    wArray_t* arr = array;

    if (isNodeDestroy)
    {
        for (int i = 0; i < arr->arraySize; ++i)
        {
            wNode* node = arr->array[i];
            wNodeDestroy(node);
        }
    }

    free(arr->array);
    arr->array = NULL;
    arr->arraySize = 0;
}

void wArrayDestroy(wArray** array, bool isNodeDestroy)
{
    if (array == NULL || *array == NULL)
        return;

    wArrayClear(*array, isNodeDestroy);

    free(*array);
    *array = NULL;
}
#endif //__cplusplus

///wScrollbar
void wScrollbar::Init(wVector2i position,wVector2i size,const wchar_t* capt,Int32 id,Float32 maxValue,Float32 currentValue)
{
        wVector2i fromPos,toPos;
        fromPos.x=position.x;
        fromPos.y=position.y;
        if(this->horizontal)
        {
            toPos.x=fromPos.x+size.x;
            toPos.y=fromPos.y+size.y;
        }
        else
        {
            toPos.x=fromPos.x+size.y;
            toPos.y=fromPos.y+size.x;
        }
        if(maxValue<0)maxValue*=-1;
        if(currentValue<0)currentValue*=-1;
        if(currentValue>maxValue)
            currentValue=maxValue;
        this->caption=capt;
        this->scroll=wGuiScrollBarCreate(horizontal,fromPos,toPos);
        if(this->isFloat)maxValue*=100;
        wGuiScrollBarSetMaxValue(this->scroll,wMathCeil(maxValue));
        wGuiScrollBarSetMinValue(this->scroll,0);
        if(this->isFloat)currentValue*=100;
        wGuiScrollBarSetValue(this->scroll,wMathCeil(currentValue));
        wGuiScrollBarSetSmallStep(this->scroll,1);
        wGuiScrollBarSetLargeStep(this->scroll,1);
        wGuiObjectSetId(this->scroll,id);

        if(!this->horizontal)
        {
            wVector2i temp;
            temp.x=toPos.y;
            temp.y=toPos.x;
            toPos.x=temp.x;
            toPos.y=temp.y;
        }
        fromPos.y-=30;
        toPos.y=fromPos.y+30;
        this->label=wGuiLabelCreate(this->caption,fromPos,toPos, false, true);

}

void wScrollbar::SetValueAsFloat(Float32 value)
{
        wGuiScrollBarSetValue(this->scroll,Int32(100*value));
}

Float32 wScrollbar::getValueAsFloat()
{
        return Float32(wGuiScrollBarGetValue(this->scroll)/100.f);
}

void wScrollbar::SetValueAsInt(Int32 value)
{
        wGuiScrollBarSetValue(this->scroll,value);
}

Int32 wScrollbar::getValueAsInt()
{
        return wGuiScrollBarGetValue(this->scroll);
}

void wScrollbar::Draw()
{
        wchar_t tmpBuf[256]={0};
        if(this->isFloat)
        {
            Float32 value=Float32(wGuiScrollBarGetValue(this->scroll)/100.f);
            swprintf(tmpBuf,L"%s [%.3f]",this->caption,value);
        }
        else
        {
            Int32 value=wGuiScrollBarGetValue(this->scroll);
            swprintf(tmpBuf,L"%s [%d]",this->caption,value);
        }

        wGuiObjectSetText(this->label,tmpBuf);
        wGuiObjectDraw(this->label);
        wGuiObjectDraw(this->scroll);
}
