#include <typeinfo>
#include <ctime>

#include "base_object.h"
#include "object2d.h"
#include "object3d.h"
#include "object3d_billboard.h"
#include "sprites.h"
#include "doors.h"
#include "keys.h"
#include "weapons.h"
#include "weapon_item.h"
#include "bullets.h"
#include "ammo.h"
#include "player.h"

int g_ObjectId = 0;
vector<TObject*> g_ObjectsForInit;   // таблица объектов,которые надо инициализировать
vector<TObject*> g_ObjectsForDelete; // таблица объектов,которые надо удалить

/* статические поля надо где-нибудь определять. Ох уж этот C++ */
vector<TObject*> TObject::objects = {};

vector<TLaser*> TLaser::lasers;

TObject::TObject(ScrResourceInMem* index) : TBaseObject()
{
    this->index = index;

    //objects.push_back(this);

    if (index)
    {
        this->name = index->name;
        addVar("name", index->name);
        addVar("animationSpeed", (double)index->animSpeed);

        this->script           = new TScriptJS();
        this->script->fileName = index->script.fileName;
        this->script->text     = index->script.text;
        //this->script->setId();

        /* если кадры не подгружены, то пора это сделать */
        if (!index->isLoaded())
            index->loadFrames();
    }

    TObject::objects.push_back(this);
}

TObject::~TObject()
{
    clearVars();

    /* уменьшаем список доступных объектов */
    if (objects.size() > 0)
    {
        for (vector<TObject*>::iterator it = TObject::objects.begin();
             it != TObject::objects.end();
             ++it)
        {
            if (*it == this)
            {
                objects.erase(it);
                break;
            }
        }
    }

    delete script;
    delete initCommand;
    delete updateCommand;
    if (destroyCommand)
    {
        destroyCommand->run();
        delete destroyCommand;
    }
}

void TObject::init(string className, string objectName, bool numerate)
{
    if (script)
        script->clear();
    else
        script = new TScriptJS();

    if (!index)
        return;

    //*script = index->script;
    script->setId();

    //g_MutexNodes.lock_nothrow();
    g_ObjectId = this->id;

    // заворачиваем код в вид:
    // function Object0(){ текст_скрипта }
    // var object0 = new Object0();
    if (!index->isClassCreated)
        index->init(className, numerate);

    if (objectName == "")
        objectName = "object";
    if (numerate)
    {
        objectName += to_string(script->id);
    }

    script->fileName = "script for '" + this->name + "'";
    script->text = objectName + " = new " + index->className + "();";
    //script.compile();
    script->run();

    cout << index->className << " " << objectName << endl;

    // запоминаем команды для инициализации и обновления объекта
    if (initCommand)
        initCommand->clear();
    else
        initCommand = new TScriptJS();
    initCommand->fileName = this->name + " init command";
    initCommand->text     = objectName + ".onInit();";
    initCommand->compile();
    initCommand->run();

    if (updateCommand)
        updateCommand->clear();
    else
        updateCommand = new TScriptJS();
    updateCommand->fileName = this->name + " update command";
    updateCommand->text     = objectName + ".onUpdate();";
    updateCommand->compile();

    if (destroyCommand)
        destroyCommand->clear();
    else
        destroyCommand = new TScriptJS();
    destroyCommand->fileName = this->name + " destroy command";
    //destroyCommand->text    = objectName + ".onDestroy(); delete(" + objectName + "); delete(" + className + ");";
    destroyCommand->text = objectName + ".onDestroy(); delete " + objectName + ";";
    //destroyCommand->compile();

    // g_MutexNodes.unlock_nothrow();
}

void TObject::update()
{
    if (!isVisible)
        return;

    if (updateCommand != nullptr)
    {
        //g_MutexNodes.lock_nothrow();
        g_ObjectId = this->id;
        updateCommand->run();
        //g_MutexNodes.unlock_nothrow();
    }
}

void TObject::setFrame(int frame)
{
    if (index == nullptr)
        return;

    if (frame >= 0 && frame < index->frames.size())
        curFrame = frame;
}

int TObject::getFramesCount()
{
    if (index == nullptr)
        return 0;

    return index->frames.size();
}

void TObject::setVisibility(bool isVisible)
{
    this->isVisible = isVisible;
}

/* adders */
void TObject::addVar(const string& key, int val)
{
    addVar(key, (double)val);
}
void TObject::addVar(const string& key, double val)
{
    TUserVarIterator it = userVars.find(key);
    if (it == userVars.end())
    {
        TUserVar* var = new TUserVar(uvtNumber, val);
        userVars.insert(make_pair(key, var));
    }
    else
    {
        it->second->set(val);
    }
}
void TObject::addVar(const string& key, bool val)
{
    TUserVarIterator it = userVars.find(key);
    if (it == userVars.end())
    {
        TUserVar* var = new TUserVar(uvtBool, 0.0, val);
        userVars.insert(make_pair(key, var));
    }
    else
    {
        it->second->set(val);
    }
}
void TObject::addVar(const string& key, const char* val)
{
    TUserVarIterator it = userVars.find(key);
    if (it == userVars.end())
    {
        TUserVar* var = new TUserVar(uvtString, 0.0, false, val);
        userVars.insert(make_pair(key, var));
    }
    else
    {
        it->second->set(val);
    }
}
void TObject::addVar(const string& key, const string& val)
{
    addVar(key, val.c_str());
}
void TObject::addVar(const string& key, vector<double>& val)
{
    TUserVarIterator it = userVars.find(key);
    if (it == userVars.end())
    {
        TUserVar* var = new TUserVar(uvtVector, 0.0, false, "", val);
        userVars.insert(make_pair(key, var));
    }
    else
    {
        it->second->set(val);
    }
}

/* setters */
void TObject::setVar(const string& key, double val)
{
    TUserVarIterator it = userVars.find(key);
    if (it != userVars.end())
    {
        it->second->set(val);
    }
}
void TObject::setVar(const string& key, bool val)
{
    TUserVarIterator it = userVars.find(key);
    if (it != userVars.end())
    {
        it->second->set(val);
    }
}
void TObject::setVar(const string& key, char* val)
{
    TUserVarIterator it = userVars.find(key);
    if (it != userVars.end())
    {
        it->second->set(val);
    }
}
void TObject::setVar(const string& key, string val)
{
    setVar(key, (char*)val.c_str());
}
void TObject::setVar(const string& key, vector<double> val)
{
    TUserVarIterator it = userVars.find(key);
    if (it != userVars.end())
    {
        it->second->set(val);
    }
}

/* getters */
double TObject::getVarNumber(const string& key)
{
    TUserVarIterator it = userVars.find(key);
    if (it != userVars.end())
    {
        if (it->second->type == uvtNumber)
            return it->second->dVal;
    }
    return numeric_limits<double>::max();
}
bool TObject::getVarBool(const string& key)
{
    TUserVarIterator it = userVars.find(key);
    if (it != userVars.end())
    {
        if (it->second->type == uvtBool)
            return it->second->bVal;
    }
    return false;
}
string TObject::getVarString(const string& key)
{
    TUserVarIterator it = userVars.find(key);
    if (it != userVars.end())
    {
        if (it->second->type == uvtString)
            return it->second->sVal;
    }
    return "undefined";
}
vector<double> TObject::getVarVector(const string& key)
{
    TUserVarIterator it = userVars.find(key);
    if (it != userVars.end())
    {
        if (it->second->type == uvtVector)
            return it->second->vVal;
    }
    double max = numeric_limits<double>::max();
    return {max, max, max};
}

/* delete userVars value */
void TObject::removeVar(const string& key)
{
    this->userVars.erase(key);
}

/** clear userVars */
inline void TObject::clearVars()
{
    this->userVars.clear();
}

/* check type */
bool TObject::isVarNumber(const string& key)
{
    TUserVarIterator it = userVars.find(key);
    return (it != userVars.end() &&
            it->second->type == uvtNumber);
}
bool TObject::isVarBool(const string& key)
{
    TUserVarIterator it = userVars.find(key);
    return (it != userVars.end() &&
            it->second->type == uvtBool);
}
bool TObject::isVarString(const string& key)
{
    TUserVarIterator it = userVars.find(key);
    return (it != userVars.end() &&
            it->second->type == uvtString);
}
bool TObject::isVarVector(const string& key)
{
    TUserVarIterator it = userVars.find(key);
    return (it != userVars.end() &&
            it->second->type == uvtVector);
}


/**
 * IMPLEMENTATION
 * */
void InitObjects()
{
    if (g_ObjectsForInit.size() == 0)
        return;

    for (auto object : g_ObjectsForInit)
    {
        if (object != nullptr)
        {
            object->init();
        }
    }

    g_ObjectsForInit.resize(0);
}

void DeleteObjects()
{
    if (g_ObjectsForDelete.size() == 0)
        return;

    for (auto obj : g_ObjectsForDelete)
    {
        if (obj == nullptr)
            continue;

        delete (obj);
    }

    g_ObjectsForDelete.resize(0);
    JS_GC();
}



/**
 *  JerryScript
 **/


/** работа с объектами */

JS_FUNCTION(js_objectCreate)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];
    if (!jerry_value_is_string(arg0))
        return jerry_create_number(0);

    wVector3f position = wVECTOR3f_ZERO;
    string    name;

    /* если заданы координаты после имени объекта, то подтянуть их */
    if (args_cnt > 3)
    {
        jerry_value_t arg1 = args_p[1];
        jerry_value_t arg2 = args_p[2];
        jerry_value_t arg3 = args_p[3];

        if (jerry_value_is_number(arg1) &&
            jerry_value_is_number(arg2) &&
            jerry_value_is_number(arg3))
        {
            position.x = jerry_get_number_value(arg1) * BLOCK_SIZE;
            position.y = jerry_get_number_value(arg2) * BLOCK_SIZE;
            position.z = jerry_get_number_value(arg3) * BLOCK_SIZE;
        }
    }

    name = JS_GetStringValue(arg0);
//    cout << "start find for '" << name << "'" << endl;
//    cout << "size of resources " << ScrResourceInMem::scrResourcesInMem.size() << std::endl;

    /* ищем по имени объект в списке доступных загруженных */
    for (vector<ScrResourceInMem*>::iterator it = ScrResourceInMem::scrResourcesInMem.begin();
         it != ScrResourceInMem::scrResourcesInMem.end();
         ++it)
    {
//        std::cout << "resource name is '" + (*it)->name << std::endl;
        if ((*it)->name == name)
        {
            /* если кадры не подгружены, то пора это сделать */
            //if (!(*it)->isLoaded())
            //    (*it)->loadFrames();

            /* текстуры не подгрузились - нечего тогда и создавать */
            if ((*it)->frames.size() == 0)
                return jerry_create_number(0);

            /* создаем объект и взвращаем его */
            TObject* object = nullptr;

            /* создаем в зависимости от типа ресурса */
            switch ((*it)->type)
            {
                case rtSprite:
                    object = new TSpriteNode((TSpriteInMem*)(*it));
                    break;

                case rtKey:
                    object = new TKeyNode((TKeyInMem*)(*it));
                    break;

                case rtBullet:
                    object = new TBulletNode((TBulletInMem*)(*it));
                    break;

                case rtWeaponItem:
                    object = new TWeaponItemNode((TWeaponItemInMem*)(*it));
                    break;

                case rtAmmo:
                    object = new TAmmoNode((TAmmoInMem*)(*it));
                    break;
            }

            if (object)
            {
                cout << "'" << name << "' created!" << endl;
                object->setPosition(position);
                g_ObjectsForInit.push_back(object);
                return jerry_create_number(object->id);
            }
            else
                return jerry_create_number(0);
        }
    }

    return jerry_create_number(0);
}

JS_FUNCTION(js_objectSetEnable)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_boolean(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        bool isVisible = (jerry_get_boolean_value(arg1) != 0) ? true : false;
        obj->setVisibility(isVisible);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectIsEnable)
{
    if (args_cnt < 1)
        return jerry_create_boolean(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_boolean(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        return jerry_create_boolean(obj->isVisible);
    }

    return jerry_create_boolean(0);
}
JS_FUNCTION(js_objectDestroy)
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        obj->setVisibility(false);
        g_ObjectsForDelete.push_back(obj);
    }

    return jerry_create_undefined();
}


/* СЕТТЕРЫ */
/* POSITION */
JS_FUNCTION(js_objectSetPosition)
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];
    jerry_value_t arg3;
    if (args_cnt >= 4)
        arg3 = args_p[3];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1) ||
        !jerry_value_is_number(arg2) || (args_cnt >= 4 && !jerry_value_is_number(arg3)))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        wVector3f pos = { jerry_get_number_value(arg1), jerry_get_number_value(arg2), 0 };
        if (args_cnt >= 4)
            pos.z = jerry_get_number_value(arg3);
        obj->setPosition(pos);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectSetPositionX)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        float x = jerry_get_number_value(arg1);
        obj->setPositionX(x);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectSetPositionY)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        float y = jerry_get_number_value(arg1);
        obj->setPositionY(y);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectSetPositionZ)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        float z = jerry_get_number_value(arg1);
        obj->setPositionZ(z);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectMove)
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];
    jerry_value_t arg3;
    if (args_cnt >= 4)
        arg3 = args_p[3];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1) ||
        !jerry_value_is_number(arg2) || (args_cnt >= 4 && !jerry_value_is_number(arg3)))
        return jerry_create_undefined();

    double id_d = jerry_get_number_value(arg0);
    int    id_i = (int)id_d;
    TBaseObject* pos = (TBaseObject*)((void*)id_i);
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        wVector3f moveVector = { jerry_get_number_value(arg1),
                                 jerry_get_number_value(arg2),
                                 0 };
        if (args_cnt >= 4)
            moveVector.z = jerry_get_number_value(arg3);
        obj->move(moveVector);
    }

    return jerry_create_undefined();
}


/* SCALE */
JS_FUNCTION(js_objectSetScale)
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];
    jerry_value_t arg3;
    if (args_cnt >= 4)
        arg3 = args_p[3];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1) ||
        !jerry_value_is_number(arg2) || (args_cnt >= 4 && !jerry_value_is_number(arg3)))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        wVector3f scale = { jerry_get_number_value(arg1), jerry_get_number_value(arg2), 0 };
        if (args_cnt >= 4)
            scale.z = jerry_get_number_value(arg3);
        obj->setScale(scale);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectSetScaleX)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        float x = jerry_get_number_value(arg1);
        obj->setScaleX(x);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectSetScaleY)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        float y = jerry_get_number_value(arg1);
        obj->setScaleY(y);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectSetScaleZ)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        float z = jerry_get_number_value(arg1);
        obj->setScaleY(z);
    }

    return jerry_create_undefined();
}


/* ROTATION */
JS_FUNCTION(js_objectSetRotation)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2;
    jerry_value_t arg3;
    if (args_cnt >= 4)
    {
        arg2 = args_p[2];
        arg3 = args_p[3];
    }

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1) ||
        (args_cnt >= 4 &&(!jerry_value_is_number(arg2) || !jerry_value_is_number(arg3))))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        wVector3f rot = { jerry_get_number_value(arg1), 0, 0 };
        if (args_cnt >= 3) rot.y = jerry_get_number_value(arg2);
        if (args_cnt >= 4) rot.z = jerry_get_number_value(arg3);

        obj->setRotation(rot);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectSetRotationX)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        float x = jerry_get_number_value(arg1);
        obj->setRotationX(x);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectSetRotationY)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        float y = jerry_get_number_value(arg1);
        obj->setRotationY(y);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectSetRotationZ)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        float z = jerry_get_number_value(arg1);
        obj->setRotationZ(z);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectRotate)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2;
    jerry_value_t arg3;
    if (args_cnt >= 4)
    {
        arg2 = args_p[2];
        arg3 = args_p[3];
    }

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1) ||
        (args_cnt >= 4 &&(!jerry_value_is_number(arg2) || !jerry_value_is_number(arg3))))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        wVector3f rot = { jerry_get_number_value(arg1), 0, 0 };
        if (args_cnt >= 3) rot.y = jerry_get_number_value(arg2);
        if (args_cnt >= 4) rot.z = jerry_get_number_value(arg3);

        obj->rotate(rot);
    }

    return jerry_create_undefined();
}


/* ГЕТТЕРЫ */
JS_FUNCTION(js_objectGetId)
{
    return jerry_create_number(g_ObjectId);
}

/* POSITION */
JS_FUNCTION(js_objectGetPosition)
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        vector<double> pos;
        obj->getPosition(pos);
        return JS_PushDoubleArray(pos);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectGetPositionX)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        return jerry_create_number(obj->getPositionX());
    }

    return jerry_create_number(0);
}
JS_FUNCTION(js_objectGetPositionY)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        return jerry_create_number(obj->getPositionY());
    }

    return jerry_create_number(0);
}
JS_FUNCTION(js_objectGetPositionZ)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        return jerry_create_number(obj->getPositionZ());
    }

    return jerry_create_number(0);
}


/* SCALE */
JS_FUNCTION(js_objectGetScale)
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        vector<double> scale;
        obj->getScale(scale);
        return JS_PushDoubleArray(scale);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectGetScaleX)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        return jerry_create_number(obj->getScaleX());
    }

    return jerry_create_number(0);
}
JS_FUNCTION(js_objectGetScaleY)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        return jerry_create_number(obj->getScaleY());
    }

    return jerry_create_number(0);
}
JS_FUNCTION(js_objectGetScaleZ)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        return jerry_create_number(obj->getScaleZ());
    }

    return jerry_create_number(0);
}


/* SIZE */
JS_FUNCTION(js_objectGetSize)
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        vector<double> size;
        obj->getSize(size);
        return JS_PushDoubleArray(size);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectGetSizeX)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        return jerry_create_number(obj->getSizeX());
    }

    return jerry_create_number(0);
}
JS_FUNCTION(js_objectGetSizeY)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        return jerry_create_number(obj->getSizeY());
    }

    return jerry_create_number(0);
}
JS_FUNCTION(js_objectGetSizeZ)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        return jerry_create_number(obj->getSizeZ());
    }

    return jerry_create_number(0);
}


/* ROTATION */
JS_FUNCTION(js_objectGetRotation)
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        vector<double> rot;
        obj->getRotation(rot);
        return JS_PushDoubleArray(rot);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectGetRotationX)
{
    if (args_cnt < 1)
        return jerry_create_number_nan();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number_nan();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        return jerry_create_number(obj->getRotationX());
    }

    return jerry_create_number_nan();
}
JS_FUNCTION(js_objectGetRotationY)
{
    if (args_cnt < 1)
        return jerry_create_number_nan();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number_nan();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        return jerry_create_number(obj->getRotationY());
    }

    return jerry_create_number_nan();
}
JS_FUNCTION(js_objectGetRotationZ)
{
    if (args_cnt < 1)
        return jerry_create_number_nan();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number_nan();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        return jerry_create_number(obj->getRotationZ());
    }

    return jerry_create_number_nan();
}


/* USERDATA */

/* adders */
JS_FUNCTION(js_objectAddVar)
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];
    jerry_value_t arg3, arg4;

    if (args_cnt > 3)
    {
        arg3 = args_p[3];
        if (args_cnt > 4)
            arg4 = args_p[4];
    }

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        string key = JS_GetStringValue(arg1);

        /* вектор? */
        if (args_cnt > 4 &&
            jerry_value_is_number(arg2) &&
            jerry_value_is_number(arg3) &&
            jerry_value_is_number(arg4))
        {
            std::vector<double> vVal;
            vVal.push_back(jerry_get_number_value(arg2));
            vVal.push_back(jerry_get_number_value(arg3));
            vVal.push_back(jerry_get_number_value(arg4));
            obj->addVar(key, vVal);
        }
        else if (jerry_value_is_number(arg2))   /* число? */
        {
            obj->addVar(key, jerry_get_number_value(arg2));
        }
        else if (jerry_value_is_boolean(arg2))   /* булеан? */
        {
            obj->addVar(key, jerry_get_boolean_value(arg2));
        }
        else if (jerry_value_is_string(arg2))   /* строка? */
        {
            obj->addVar(key, JS_GetStringValue(arg2));
        }
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectAddVarNumber)
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1) || !jerry_value_is_number(arg2))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        string key = JS_GetStringValue(arg1);
        double dVal = jerry_get_number_value(arg2);

        obj->addVar(key, dVal);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectAddVarBool)
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1) || !jerry_value_is_boolean(arg2))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        string key = JS_GetStringValue(arg1);
        bool   bVal = (jerry_get_boolean_value(arg2) != 0);

        obj->addVar(key, bVal);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectAddVarString)
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1) || !jerry_value_is_string(arg2))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        string key  = JS_GetStringValue(arg1);
        string sVal = JS_GetStringValue(arg2);

        obj->addVar(key, sVal);
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectAddVarVector)
{
    if (args_cnt < 4)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];
    jerry_value_t arg3 = args_p[3];
    jerry_value_t arg4;
    if (args_cnt >= 5)
        arg4 = args_p[4];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1) ||
        !jerry_value_is_number(arg2) || !jerry_value_is_number(arg3) ||
        (args_cnt >= 5 && !jerry_value_is_number(arg4)))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        string key = JS_GetStringValue(arg1);
        vector<double> vVal;
        if (args_cnt >= 5)
            vVal = { jerry_get_number_value(arg2),
                     jerry_get_number_value(arg3),
                     jerry_get_number_value(arg4) };
        else
            vVal = { jerry_get_number_value(arg2),
                     jerry_get_number_value(arg3) };

        obj->addVar(key, vVal);
    }

    return jerry_create_undefined();
}

/* setters */
JS_FUNCTION(js_objectSetVar)
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        string key = JS_GetStringValue(arg1);

        TUserVarIterator it = obj->userVars.find(key);
        if (it != obj->userVars.end())
        {
            switch (it->second->type)
            {
                default:
                case uvtNone :
                    break;

                case uvtNumber :
                    if (jerry_value_is_number(arg2))
                        obj->setVar(key, jerry_get_number_value(arg2));
                    break;

                case uvtBool :
                    if (jerry_value_is_boolean(arg2))
                        obj->setVar(key, (jerry_get_boolean_value(arg2) != 0));
                    break;

                case uvtString :
                    if (jerry_value_is_string(arg2))
                    {
                        string sVal = JS_GetStringValue(arg2);
                        obj->setVar(key, sVal);
                    }
                    break;

                case uvtVector :
                    if (args_cnt >= 5)
                    {
                        jerry_value_t arg3 = args_p[3];
                        jerry_value_t arg4 = args_p[4];
                        if (jerry_value_is_number(arg3) && jerry_value_is_number(arg4))
                        {
                            vector<double> vVal = { jerry_get_number_value(arg2),
                                                    jerry_get_number_value(arg3),
                                                    jerry_get_number_value(arg4) };
                            obj->setVar(key, vVal);
                        }
                    }
                    break;
            }
        }
    }

    return jerry_create_undefined();
}

/* getters */
JS_FUNCTION(js_objectGetVar)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        string key = JS_GetStringValue(arg1);

        TUserVarIterator it = obj->userVars.find(key);
        if (it != obj->userVars.end())
        {
            switch (it->second->type)
            {
                default:
                case uvtNone :
                    return jerry_create_undefined();

                case uvtNumber :
                    return jerry_create_number(obj->getVarNumber(key));

                case uvtBool :
                    return jerry_create_boolean(obj->getVarBool(key));

                case uvtString :
                {
                    string s = obj->getVarString(key);
                    return jerry_create_string_sz((jerry_char_t*)s.c_str(), s.size());
                }

                case uvtVector :
                {
                    vector<double> vVal = obj->getVarVector(key);
                    return JS_PushDoubleArray(vVal);
                }
            }
        }
    }

    return jerry_create_undefined();
}

/* remove */
JS_FUNCTION(js_objectRemoveVar)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        string key = JS_GetStringValue(arg1);
        obj->removeVar(key);
    }

    return 0;
}

JS_FUNCTION(js_objectExistVar)
{
    if (args_cnt < 2)
        return jerry_create_boolean(false);

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_boolean(false);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        std::string key = JS_GetStringValue(arg1);
        TUserVarIterator it = obj->userVars.find(key);
        return jerry_create_boolean(it != obj->userVars.end());
    }

    return jerry_create_boolean(false);
}

/* clear data */
JS_FUNCTION(js_objectClearVars)
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        obj->clearVars();
    }

    return 0;
}

/* check type */
JS_FUNCTION(js_objectIsVarNumber)
{
    if (args_cnt < 2)
        return jerry_create_boolean(0);

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_boolean(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        string key = JS_GetStringValue(arg1);
        return jerry_create_boolean(obj->isVarNumber(key));
    }

    return jerry_create_boolean(0);
}
JS_FUNCTION(js_objectIsVarBool)
{
    if (args_cnt < 2)
        return jerry_create_boolean(0);

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_boolean(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        string key = JS_GetStringValue(arg1);
        return jerry_create_boolean(obj->isVarBool(key));
    }

    return jerry_create_boolean(0);
}
JS_FUNCTION(js_objectIsVarString)
{
    if (args_cnt < 2)
        return jerry_create_boolean(0);

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_boolean(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        string key = JS_GetStringValue(arg1);
        return jerry_create_boolean(obj->isVarString(key));
    }

    return jerry_create_boolean(0);
}
JS_FUNCTION(js_objectIsVarVector)
{
    if (args_cnt < 2)
        return jerry_create_boolean(0);

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_boolean(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        string key = JS_GetStringValue(arg1);
        return jerry_create_boolean(obj->isVarVector(key));
    }

    return jerry_create_boolean(0);
}


/* UTIL */
JS_FUNCTION(js_objectIsInView)
{
    if (args_cnt < 1)
        return jerry_create_boolean(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_boolean(0);

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj)
    {
        TObject3D* obj3d = dynamic_cast<TObject3D*>(obj);
        if (obj3d)
        {
            wNode* node = obj3d->node;
            if (node != nullptr)
            {
                return jerry_create_boolean(wNodeIsInView(node));
            }
        }
    }

    return jerry_create_boolean(0);
}
JS_FUNCTION(js_distanceBetweenObjects)
{
    if (args_cnt < 2)
        return jerry_create_number_nan();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_number_nan();

    TBaseObject* pos0 = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TBaseObject* pos1 = (TBaseObject*)(int(jerry_get_number_value(arg1)));
    TObject* obj0 = dynamic_cast<TObject*>(pos0);
    TObject* obj1 = dynamic_cast<TObject*>(pos1);
    if (obj0 && obj1)
    {
        TObject3D* obj3d0 = dynamic_cast<TObject3D*>(obj0);
        TObject3D* obj3d1 = dynamic_cast<TObject3D*>(obj1);
        if (obj3d0 && obj3d1)
        {
            wNode* node0 = obj3d0->node;
            wNode* node1 = obj3d1->node;
            if (node0 != nullptr && node1 != nullptr)
            {
                return jerry_create_number(wNodesGetBetweenDistance(node0, node1));
            }
        }
        else
        {
            TObject2D* obj2d0 = dynamic_cast<TObject2D*>(obj0);
            TObject2D* obj2d1 = dynamic_cast<TObject2D*>(obj1);
            if (obj2d0 && obj2d1)
            {
                double distance = sqrt(sqr(obj2d1->position.x - obj2d0->position.x) +
                                       sqr(obj2d1->position.y - obj2d0->position.y));
                return jerry_create_number(distance);
            }
        }
    }

    return jerry_create_number_nan();
}


/* ANIMATION */
/* setters */
JS_FUNCTION(js_objectSetFrame)
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        obj->setFrame(jerry_get_number_value(arg1));
    }

    return jerry_create_undefined();
}
/* getters */
JS_FUNCTION(js_objectGetFrame)
{
    if (args_cnt < 1)
        return jerry_create_number_nan();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number_nan();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        return jerry_create_number(obj->getFrame());
    }

    return jerry_create_number_nan();
}
JS_FUNCTION(js_objectGetFramesCount)
{
    if (args_cnt < 1)
        return jerry_create_number_nan();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number_nan();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        return jerry_create_number(obj->getFramesCount());
    }

    return jerry_create_number_nan();
}


/* COLLISION */
/* setters */
JS_FUNCTION(js_objectEnableCollision)
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        obj->enableCollision();
    }

    return jerry_create_undefined();
}
JS_FUNCTION(js_objectDisableCollision)
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    TBaseObject* pos = (TBaseObject*)(int(jerry_get_number_value(arg0)));
    TObject* obj = dynamic_cast<TObject*>(pos);
    if (obj != nullptr)
    {
        obj->disableCollision();
    }

    return jerry_create_undefined();
}

/* getters */
JS_FUNCTION(js_objectGetByRay)
{
    if (args_cnt < 6)
        return jerry_create_number_nan();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];
    jerry_value_t arg3 = args_p[3];
    jerry_value_t arg4 = args_p[4];
    jerry_value_t arg5 = args_p[5];

    if (!jerry_value_is_number(arg0) ||
        !jerry_value_is_number(arg1) ||
        !jerry_value_is_number(arg2) ||
        !jerry_value_is_number(arg3) ||
        !jerry_value_is_number(arg4) ||
        !jerry_value_is_number(arg5))
        return jerry_create_number_nan();

    double x1 = jerry_get_number_value(arg0);
    double y1 = jerry_get_number_value(arg1);
    double z1 = jerry_get_number_value(arg2);
    double x2 = jerry_get_number_value(arg3);
    double y2 = jerry_get_number_value(arg4);
    double z2 = jerry_get_number_value(arg5);

    wVector3f start = { x1, y1, z1 };
    wVector3f end   = { x2, y2, z2 };

    /* видимый лазер */
    //new TLaser({start.x, start.y - 0.1, start.z},
    //           {end.x,   end.y   - 0.1, end.z  });
    //new TLaser(start, end);

    wTriangle tris;
    wNode* node;
    if (wCollisionGetPointFromRay(g_WorldCollider, &start, &end, &g_IntersectPoint, &g_IntersectNormal, &tris, &node))
    {
        wVector3f offset;
        wVector3f point;

        // для стопроцентного определения объекта немного сдвинем точку "вовнутрь"
        offset = wMathVector3fNormalize(g_IntersectNormal);
        offset.x *= -0.01;
        offset.y *= -0.01;
        offset.z *= -0.01;
        point = wMathVector3fAdd(g_IntersectPoint, offset);

        if (g_SolidObjects != nullptr)
        {
            wNode* target = wCollisionGetNodeChildFromPoint(g_SolidObjects, 0, false, &point);
            if (target != nullptr)
            {
                void* ptr = wNodeGetUserData(target);
                if (ptr != nullptr)
                {
                    int id = *((int*)ptr);
                    if (id > 0)
                    {
                        return jerry_create_number(id);
                    }
                }
            }
        }

        // столкновение с миром
        return jerry_create_number(0);
    }

    return jerry_create_number_nan();
}
JS_FUNCTION(js_intersectPointGetByRay)
{
    vector<double> point = { g_IntersectPoint.x, g_IntersectPoint.y, g_IntersectPoint.z };
    return JS_PushDoubleArray(point);
}
JS_FUNCTION(js_intersectNormalGetByRay)
{
    vector<double> point = { g_IntersectNormal.x, g_IntersectNormal.y, g_IntersectNormal.z };
    return JS_PushDoubleArray(point);
}


void RegisterObjectJSFunctions()
{
    /* global */
    JS_RegisterCFunction(js_objectCreate,    "objectCreate"   );
    JS_RegisterCFunction(js_objectGetId,     "objectGetId"    );
    JS_RegisterCFunction(js_objectDestroy,   "objectDestroy"  );
    JS_RegisterCFunction(js_objectSetEnable, "objectSetEnable");
    JS_RegisterCFunction(js_objectIsEnable,  "objectIsEnable" );

    /* animation */
    JS_RegisterCFunction(js_objectSetFrame,       "objectSetFrame"      );
    JS_RegisterCFunction(js_objectGetFrame,       "objectGetFrame"      );
    JS_RegisterCFunction(js_objectGetFramesCount, "objectGetFramesCount");

    /* collision */
    JS_RegisterCFunction(js_objectEnableCollision,   "objectEnableCollision"  );
    JS_RegisterCFunction(js_objectDisableCollision,  "objectDisableCollision" );
    JS_RegisterCFunction(js_objectGetByRay,          "objectGetByRay"         );
    JS_RegisterCFunction(js_intersectPointGetByRay,  "intersectPointGetByRay" );
    JS_RegisterCFunction(js_intersectNormalGetByRay, "intersectNormalGetByRay");

    /* util */
    JS_RegisterCFunction(js_objectIsInView,         "objectIsInView"        );
    JS_RegisterCFunction(js_distanceBetweenObjects, "distanceBetweenObjects");

    /* position */
    JS_RegisterCFunction(js_objectSetPosition,  "objectSetPosition" );
    JS_RegisterCFunction(js_objectSetPositionX, "objectSetPositionX");
    JS_RegisterCFunction(js_objectSetPositionY, "objectSetPositionY");
    JS_RegisterCFunction(js_objectSetPositionZ, "objectSetPositionZ");
    JS_RegisterCFunction(js_objectGetPosition,  "objectGetPosition" );
    JS_RegisterCFunction(js_objectGetPositionX, "objectGetPositionX");
    JS_RegisterCFunction(js_objectGetPositionY, "objectGetPositionY");
    JS_RegisterCFunction(js_objectGetPositionZ, "objectGetPositionZ");
    JS_RegisterCFunction(js_objectMove,         "objectMove"        );

    /* scale */
    JS_RegisterCFunction(js_objectSetScale,  "objectSetScale" );
    JS_RegisterCFunction(js_objectSetScaleX, "objectSetScaleX");
    JS_RegisterCFunction(js_objectSetScaleY, "objectSetScaleY");
    JS_RegisterCFunction(js_objectSetScaleZ, "objectSetScaleZ");
    JS_RegisterCFunction(js_objectGetScale,  "objectGetScale" );
    JS_RegisterCFunction(js_objectGetScaleX, "objectGetScaleX");
    JS_RegisterCFunction(js_objectGetScaleY, "objectGetScaleY");
    JS_RegisterCFunction(js_objectGetScaleZ, "objectGetScaleZ");

    /* size */
    JS_RegisterCFunction(js_objectGetSize,   "objectGetSize" );
    JS_RegisterCFunction(js_objectGetSizeX,  "objectGetSizeX");
    JS_RegisterCFunction(js_objectGetSizeY,  "objectGetSizeY");
    JS_RegisterCFunction(js_objectGetSizeZ,  "objectGetSizeZ");

    /* rotation */
    JS_RegisterCFunction(js_objectSetRotation,  "objectSetRotation" );
    JS_RegisterCFunction(js_objectSetRotationX, "objectSetRotationX");
    JS_RegisterCFunction(js_objectSetRotationY, "objectSetRotationY");
    JS_RegisterCFunction(js_objectSetRotationX, "objectSetRotationZ");
    JS_RegisterCFunction(js_objectGetRotationX, "objectGetRotation" );
    JS_RegisterCFunction(js_objectGetRotationX, "objectGetRotationX");
    JS_RegisterCFunction(js_objectGetRotationY, "objectGetRotationY");
    JS_RegisterCFunction(js_objectGetRotationX, "objectGetRotationZ");
    JS_RegisterCFunction(js_objectRotate,       "objectRotate"      );

    /* user data */
    JS_RegisterCFunction(js_objectAddVar,       "objectAddVar"      );
    JS_RegisterCFunction(js_objectAddVarNumber, "objectAddVarNumber");
    JS_RegisterCFunction(js_objectAddVarBool,   "objectAddVarBool"  );
    JS_RegisterCFunction(js_objectAddVarString, "objectAddVarString");
    JS_RegisterCFunction(js_objectAddVarVector, "objectAddVarVector");
    JS_RegisterCFunction(js_objectSetVar,       "objectSetVar"      );
    JS_RegisterCFunction(js_objectGetVar,       "objectGetVar"      );
    JS_RegisterCFunction(js_objectRemoveVar,    "objectRemoveVar"   );
    JS_RegisterCFunction(js_objectExistVar,     "objectExistVar"    );
    JS_RegisterCFunction(js_objectClearVars,    "objectClearVars"   );
    JS_RegisterCFunction(js_objectIsVarNumber,  "objectIsVarNumber" );
    JS_RegisterCFunction(js_objectIsVarBool,    "objectIsVarBool"   );
    JS_RegisterCFunction(js_objectIsVarString,  "objectIsVarString" );
    JS_RegisterCFunction(js_objectIsVarVector,  "objectIsVarVector" );
}
