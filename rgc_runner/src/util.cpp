#include "util.h"
#include "constants.h"
#include "global.h"
#include "res_manager.h"
#include "file_system.h"

using namespace FileSystem;


uint32_t ARGB1555toARGB8888(uint16_t c)
{
    const uint32_t a = c&0x8000,
                   r = c&0x7C00,
                   g = c&0x03E0,
                   b = c&0x1F;
    const uint32_t rgb = (r << 9) | (g << 6) | (b << 3);

    return (a*0x1FE00) | rgb | ((rgb >> 5) & 0x070707);
}

uint16_t ARGB8888toARGB1555(uint32_t c)
{
    return (uint16_t)((((c>>16)&0x8000) | ((c>>9)&0x7C00) | ((c>>6)&0x03E0) | ((c>>3)&0x1F)));
}

wTexture* LoadTextureFromMemory(const std::string name, wVector2i size, int sizeForRead, void* buffer)
{
    if (buffer == nullptr)
        return nullptr;

    wTexture* loadTexture = nullptr;
    void* loadTextureBits = nullptr;

    // при софтверном рендере нужно преобразовать формат в ARGB1555
    if (g_Renderer == wDriverTypes::wDRT_SOFTWARE/* || g_Renderer == wDRT_BURNINGS_VIDEO*/)
    {
        int32_t  c32;
        int16_t* c16 = nullptr;

        loadTexture = wTextureCreate((char*)(name.c_str()), size, wColorFormat::wCF_A1R5G5B5);
        c16 = (int16_t*)wTextureLock(loadTexture);
        for (int i = 0, count = sizeForRead / sizeof(int32_t);
             i < count;
             ++i, ++c16)
        {
            c32 = ((int32_t*)buffer)[i];
            // если альфа ноль, то значит область прозрачна
            if ((c32 >> 24) == 0)
            {
                *c16 = 0;
            }
            else
            {
                *c16 = ARGB8888toARGB1555(c32);
            }
        }
        wTextureUnlock(loadTexture);
    }
    // при всех остальных рендерах просто загрузить текстуру как она есть
    else
    {
        loadTexture = wTextureCreate((char*)name.c_str(), size, wCF_A8R8G8B8);
        loadTextureBits = wTextureLock(loadTexture);
        memcpy(loadTextureBits, buffer, sizeForRead);
        wTextureUnlock(loadTexture);
    }

    return loadTexture;
}

wTexture* LoadTexture(const std::string fileName, const std::string textureName)
{
    wTexture* txr   = nullptr;
    char*    buffer = nullptr;
    uint64_t bufferSize;
    uint16_t w, h;
    uint64_t textureSize;

    File::readFull(fileName, &buffer, &bufferSize);
    if (buffer)
    {
        // читаем размеры
        w = *((uint16_t*)buffer);
        h = *((uint16_t*)(buffer + 2));
        textureSize = w * h * sizeof(uint32_t);

        txr = LoadTextureFromMemory(textureName, {w, h}, textureSize, buffer + sizeof(w) + sizeof(h));
    }
    free(buffer);

    //wTextureSave(txr, (fileName + ".jpg").c_str());

    return txr;
}

std::string ExtractPath(const std::string path)
{
    std::string str = path.c_str();

    /* переводим путь в linux-style */
    int pos = str.find_first_of("\\");
    while (pos != string::npos)
    {
        str.replace(pos, 1, "/");
        pos = str.find_first_of("\\");
    }

    /* теперь удаляем имя файла */
    pos = str.find_last_of("/");
    if (pos != string::npos)
    {
        str.erase(pos + 1, str.size() - pos - 1);
    }
    else
        str = "./";

    return str;
}

std::string ExtractFileName(const std::string path)
{
    std::string str = path.c_str();

    /* переводим путь в linux-style */
    int pos = str.find_first_of("\\");
    while (pos != string::npos)
    {
        str.replace(pos, 1, "/");
        pos = str.find_first_of("\\");
    }

    /* теперь удаляем всё до имени файла */
    pos = str.find_last_of("/");
    if (pos != string::npos)
    {
        str.erase(0, ++pos);
    }
    else
        str = "";

    return str;
}
