#include "input.h"
#include "js_util.h"
#include "WorldSim3D.h"

/*
 * INTERPRETATION
 * */

JS_FUNCTION(js_keyboardIsEventAvailable)
{
    return jerry_create_boolean(wInputIsKeyEventAvailable());
}

JS_FUNCTION(js_keyboardIsKeyHit)
{
    jerry_value_t arg0 = args_p[0];
    if (args_cnt == 0 || !jerry_value_is_number(arg0))
    {
        return jerry_create_boolean(0);
    }

    int key = jerry_get_number_value(arg0);

    return jerry_create_boolean(wInputIsKeyHit((wKeyCode)key));
}

JS_FUNCTION(js_keyboardIsKeyPressed)
{
    jerry_value_t arg0 = args_p[0];
    if (args_cnt == 0 || !jerry_value_is_number(arg0))
    {
        return jerry_create_boolean(0);
    }

    int key = jerry_get_number_value(arg0);

    return jerry_create_boolean(wInputIsKeyPressed((wKeyCode)key));
}

JS_FUNCTION(js_keyboardIsKeyReleased)
{
    jerry_value_t arg0 = args_p[0];
    if (args_cnt == 0 || !jerry_value_is_number(arg0))
    {
        return jerry_create_boolean(0);
    }

    int key = jerry_get_number_value(arg0);

    return jerry_create_boolean(wInputIsKeyUp((wKeyCode)key));
}

void RegisterKeyboardJSFunctions()
{
    jerry_value_t object = jerry_create_object ();

    JS_RegisterCFunctionForObject(object, "Keyboard", js_keyboardIsEventAvailable, "isEventAvailable");
    JS_RegisterCFunctionForObject(object, "Keyboard", js_keyboardIsKeyHit,         "isKeyHit"        );
    JS_RegisterCFunctionForObject(object, "Keyboard", js_keyboardIsKeyPressed,     "isKeyPressed"    );
    JS_RegisterCFunctionForObject(object, "Keyboard", js_keyboardIsKeyReleased,    "isKeyReleased"   );

    jerry_release_value (object);
}


/** MOUSE */

JS_FUNCTION(js_mouseSetCursorVisible)
{
    if (args_cnt > 0)
    {
        jerry_value_t arg0 = args_p[0];
        if (jerry_value_is_boolean(arg0))
        {
            wInputSetCursorVisible(jerry_get_boolean_value(arg0) != 0);
        }
    }

    return jerry_create_undefined();
}

JS_FUNCTION(js_mouseIsCursorVisible)
{
    return jerry_create_boolean(wInputIsCursorVisible());
}

JS_FUNCTION(js_mouseIsEventAvailable)
{
    return jerry_create_boolean(wInputIsMouseEventAvailable());
}

JS_FUNCTION(js_mouseIsButtonHit)
{
    jerry_value_t arg0 = args_p[0];
    if (args_cnt == 0 || !jerry_value_is_number(arg0))
    {
        return jerry_create_boolean(0);
    }

    int key = jerry_get_number_value(arg0);

    return jerry_create_boolean(wInputIsMouseHit((wMouseButtons)key));
}

JS_FUNCTION(js_mouseIsButtonPressed)
{
    jerry_value_t arg0 = args_p[0];
    if (args_cnt == 0 || !jerry_value_is_number(arg0))
    {
        return jerry_create_boolean(0);
    }

    int key = jerry_get_number_value(arg0);

    return jerry_create_boolean(wInputIsMousePressed((wMouseButtons)key));
}

JS_FUNCTION(js_mouseIsButtonReleased)
{
    jerry_value_t arg0 = args_p[0];
    if (args_cnt == 0 || !jerry_value_is_number(arg0))
    {
        return jerry_create_boolean(0);
    }

    int key = jerry_get_number_value(arg0);

    return jerry_create_boolean(wInputIsMouseUp((wMouseButtons)key));
}

JS_FUNCTION(js_mouseSetLogicalPosition)
{
    if (args_cnt > 1)
    {
        jerry_value_t arg0 = args_p[0];
        jerry_value_t arg1 = args_p[1];
        if (jerry_value_is_number(arg0) && jerry_value_is_number(arg1))
        {
            wVector2f pos = {jerry_get_number_value(arg0), jerry_get_number_value(arg1)};
            wInputSetMouseLogicalPosition(&pos);
        }
    }
    return jerry_create_undefined();
}

JS_FUNCTION(js_mouseSetPosition)
{
    if (args_cnt > 1)
    {
        jerry_value_t arg0 = args_p[0];
        jerry_value_t arg1 = args_p[1];
        if (jerry_value_is_number(arg0) && jerry_value_is_number(arg1))
        {
            wVector2i pos = {jerry_get_number_value(arg0),
                             jerry_get_number_value(arg1)};
            wInputSetMousePosition(&pos);
        }
    }
    return jerry_create_undefined();
}

JS_FUNCTION(js_mouseGetX)
{
    wVector2i pos;
    wInputGetMousePosition(&pos);

    return jerry_create_number(pos.x);
}

JS_FUNCTION(js_mouseGetY)
{
    wVector2i pos;
    wInputGetMousePosition(&pos);

    return jerry_create_number(pos.y);
}

JS_FUNCTION(js_mouseGetDeltaX)
{
    return jerry_create_number(wInputGetMouseDeltaX());
}

JS_FUNCTION(js_mouseGetDeltaY)
{
    return jerry_create_number(wInputGetMouseDeltaY());
}

void RegisterMouseJSFunctions()
{
    jerry_value_t object = jerry_create_object ();

    JS_RegisterCFunctionForObject(object, "Mouse", js_mouseSetCursorVisible,   "setCursorVisible"  );
    JS_RegisterCFunctionForObject(object, "Mouse", js_mouseIsCursorVisible,    "isCursorVisible"   );
    JS_RegisterCFunctionForObject(object, "Mouse", js_mouseIsEventAvailable,   "isEventAvailable"  );
    JS_RegisterCFunctionForObject(object, "Mouse", js_mouseIsButtonHit,        "isButtonHit"       );
    JS_RegisterCFunctionForObject(object, "Mouse", js_mouseIsButtonPressed,    "isButtonPressed"   );
    JS_RegisterCFunctionForObject(object, "Mouse", js_mouseIsButtonReleased,   "isButtonReleased"  );
    JS_RegisterCFunctionForObject(object, "Mouse", js_mouseSetLogicalPosition, "setLogicalPosition");
    JS_RegisterCFunctionForObject(object, "Mouse", js_mouseSetPosition,        "setPosition"       );
    JS_RegisterCFunctionForObject(object, "Mouse", js_mouseGetX,               "getX"              );
    JS_RegisterCFunctionForObject(object, "Mouse", js_mouseGetY,               "getY"              );
    JS_RegisterCFunctionForObject(object, "Mouse", js_mouseGetDeltaX,          "getDeltaX"         );
    JS_RegisterCFunctionForObject(object, "Mouse", js_mouseGetDeltaY,          "getDeltaY"         );

    jerry_release_value (object);
}
