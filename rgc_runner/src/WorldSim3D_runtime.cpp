#include "WorldSim3D_types.h"

#include "runtime_loader.h"
#include <cstdio>


#if defined(_WIN32) || defined(WIN32)
	#define wLIBRARY_NAME "WS3DCoreLib.dll"
#elif  defined(_WIN64) || defined(WIN64)
	#define wLIBRARY_NAME "WS3DCoreLib64.dll"
#else
	#define wLIBRARY_NAME "./libWS3DCoreLib.so"
#endif


static void* ws3dHandle = NULL;


#ifdef __cplusplus
extern "C"
{
#endif // __cplusplus
///wConsole///
void (*wConsoleSetFontColor)(wConsoleFontColor c);

void (*wConsoleSetBackColor)(wConsoleBackColor c);

Int32 (*wConsoleSaveDefaultColors)();

void (*wConsoleResetColors)(Int32 defValues);


///wTexture//
wTexture* (*wTextureLoad)(const char* cptrFile );

wTexture* (*wTextureCreateRenderTarget)(wVector2i size);

wTexture* (*wTextureCreate)(const char* name,
								   wVector2i size,
								   wColorFormat format );

void (*wTextureDestroy)(wTexture* texture );

UInt32* (*wTextureLock)(wTexture* texture );

void (*wTextureUnlock)(wTexture* texture );

void (*wTextureSave)(wTexture* texture,
							const char* file);

wImage* (*wTextureConvertToImage)(wTexture* texture);

void (*wTextureGetInformation)(wTexture* texture,
									  wVector2u* size ,
									  UInt32* pitch,
									  wColorFormat* format );

void (*wTextureMakeNormalMap)(wTexture* texture,
									 Float32 amplitude );

Int32 (*wTexturesSetBlendMode)(wTexture* texturedest,
									  wTexture* texturesrc,
									  wVector2i offset,
									  wBlendOperation operation );

void (*wTextureSetColorKey)(wTexture*texture,
								   wColor4s key);

void (*wTextureSetGray)(wTexture** texture);

void (*wTextureSetAlpha)(wTexture** texture,
								UInt32 value);

void (*wTextureSetInverse)(wTexture** texture);

void (*wTextureSetBrightness)(wTexture** texture,
									 UInt32 value);

wTexture* (*wTextureCopy)(wTexture* texture,
								 const char* name);

void (*wTextureSetContrast)(wTexture** texture,
								   Float32 value);

wTexture* (*wTextureFlip)(wTexture** texture,
								 Int32 mode);

void (*wTextureSetBlur)(wTexture** texture,
							   Float32 radius);

const char* (*wTextureGetFullName)(wTexture* texture );

const char* (*wTextureGetInternalName)(wTexture* texture );

void (*wTextureDraw)(wTexture* texture,
							wVector2i pos,
							bool useAlphaChannel,
							wColor4s color);

void (*wTextureDrawEx)(wTexture* texture,
							  wVector2i pos,
							  wVector2f scale,
							  bool useAlphaChannel);

void (*wTextureDrawMouseCursor)(wTexture* texture);

void (*wTextureDrawElement)(wTexture* texture,
								   wVector2i pos,
								   wVector2i fromPos,
								   wVector2i toPos,
								   bool useAlphaChannel,
								   wColor4s color);

void (*wTextureDrawElementStretch)(wTexture* texture,
										  wVector2i destFromPos,
										  wVector2i destToPos,
										  wVector2i sourceFromPos,
										  wVector2i sourceToPos,
										  bool useAlphaChannel);

void (*wTextureDrawAdvanced)(wTexture* texture,
									wVector2i pos,
									wVector2i rotPoint,
									Float32 rotation,
									wVector2f scale,
									bool useAlphaChannel,
									wColor4s color,
									wAntiAliasingMode aliasMode,
									bool bFilter,
									bool tFilter,
									bool aFilter);

void (*wTextureDrawElementAdvanced)(wTexture* texture,
										   wVector2i pos,
										   wVector2i fromPos,
										   wVector2i toPos,
										   wVector2i rotPoint,
										   Float32 rotAngleDeg,
										   wVector2f scale,
										   bool useAlphaChannel,
										   wColor4s color,
										   wAntiAliasingMode aliasMode,
										   bool bilinearFilter,
										   bool trilinearFilter,
										   bool anisotropFilter);

///w2d///
void  (*w2dDrawRect)(wVector2i minPos,
							wVector2i maxPos,
							wColor4s color);

void (*w2dDrawRectWithGradient)(wVector2i minPos,
									   wVector2i maxPos,
									   wColor4s colorLeftUp,
									   wColor4s colorRightUp,
									   wColor4s colorLeftDown,
									   wColor4s colorRightDown);

void (*w2dDrawRectOutline)(wVector2i minPos,
								  wVector2i maxPos,
								  wColor4s color);

void (*w2dDrawLine)(wVector2i fromPos,
						   wVector2i toPos,
						   wColor4s color);

void (*w2dDrawPixel)(wVector2i pos,
							wColor4s color);

void  (*w2dDrawPolygon)(wVector2i pos,
							   Float32 Radius,
							   wColor4s color,
							   Int32 vertexCount);

///w3d///
void (*w3dDrawLine)(wVector3f start,
						   wVector3f end,
						   wColor4s color);

void (*w3dDrawBox)(wVector3f minPoint,
						  wVector3f maxPoint,
						  wColor4s color);

void (*w3dDrawTriangle)(wTriangle triangle,
							   wColor4s color);

///wFont///
wFont* (*wFontLoad)(const char* fontPath );

wFont* (*wFontAddToFont)(const char* fontPath,
								wFont* destFont );

wFont* (*wFontGetDefault)();

void (*wFontDraw)(wFont* font,
						 const wchar_t* wcptrText,
						 wVector2i fromPos,
						 wVector2i toPos,
						 wColor4s color);

void (*wFontDestroy)(wFont* font);

wVector2u (*wFontGetTextSize)(wFont* font,
									 const wchar_t* text);

void (*wFontSetKerningSize)(wFont* font,
								   wVector2u kerning);

wVector2u (*wFontGetKerningSize)(wFont* font);

Int32 (*wFontGetCharacterFromPos)(wFont* font,
										 const wchar_t* text,
										 Int32 xPixel);

void (*wFontSetInvisibleCharacters)(wFont* font,
										   const wchar_t *s);
wFont* (*wFontLoadFromTTF)(char * fontPath,
								  UInt32 size,
								  bool antialias,
								  bool transparency);

void (*wFontDrawAsTTF)(wFont* font,
							  const wchar_t* wcptrText,
							  wVector2i fromPos,
							  wVector2i toPos,
							  wColor4s color,
							  bool hcenter,
							  bool vcenter);

///wImage//////
wImage* (*wImageLoad)(const char* cptrFile );

bool (*wImageSave)(wImage* img,
						  const char* file);

wImage* (*wImageCreate)(wVector2i size,
							   wColorFormat format );

void (*wImageDestroy)(wImage* image );

UInt32* (*wImageLock)(wImage* image );

void (*wImageUnlock)(wImage* image );

wTexture* (*wImageConvertToTexture)(wImage* img,
										   const char* name);

wColor4s (*wImageGetPixelColor)(wImage* img,
									   wVector2u pos);

void (*wImageSetPixelColor)(wImage* img,
								   wVector2u pos,
								   wColor4s color,
								   bool blend);

void (*wImageGetInformation)(wImage* image,
									wVector2u* size,
									UInt32* pitch,
									wColorFormat* format);

///wTimer///
Float32 (*wTimerGetDelta)();

UInt32 (*wTimerGetTime)();

wRealTimeDate (*wTimerGetRealTimeAndDate)();

//Returns current real time in milliseconds of the system
UInt32 (*wTimerGetRealTime)();

//set the current time in milliseconds//
void (*wTimerSetTime)(UInt32 newTime );

//Returns if the virtual timer is currently stopped
bool (*wTimerIsStopped)();

//Sets the speed of the timer
void (*wTimerSetSpeed)(Float32 speed);

//Starts the virtual time
void (*wTimerStart)();

//Stops the virtual timer
void (*wTimerStop)();

//Advances the virtual time
void (*wTimerTick)();

///wLog///
void (*wLogSetLevel)(wLoggingLevel level);

void (*wLogSetFile)(const char* path);

void (*wLogClear)(const char* path);

void (*wLogWrite)(const wchar_t* hint,
						 const wchar_t* text,
						 const char* path,
						 UInt32 mode);

///wSystem////
void (*wSystemSetClipboardText)(const char* text);

const char* (*wSystemGetClipboardText)();

UInt32 (*wSystemGetProcessorSpeed)();

UInt32  (*wSystemGetTotalMemory)();

UInt32  (*wSystemGetAvailableMemory)();

wVector2i (*wSystemGetMaxTextureSize)();

bool (*wSystemIsTextureFormatSupported)(wColorFormat format);

void (*wSystemSetTextureCreationFlag)(wTextureCreationFlag flag,
											 bool value );

bool (*wSystemIsTextureCreationFlag)(wTextureCreationFlag flag);

wTexture* (*wSystemCreateScreenShot)(wVector2u minPos,
											wVector2u maxPos);

bool (*wSystemSaveScreenShot)(const char* file );

///Get the current operation system version as string.
const char*	(*wSystemGetVersion)();

///Check if a driver type is supported by the engine.
///Even if true is returned the driver may not be available for
///a configuration requested when creating the device.
bool (*wSystemIsDriverSupported)(wDriverTypes testDriver);


///wDisplay///
///Get the graphics card vendor name.
const char* (*wDisplayGetVendor)();

Int32 (*wDisplayModesGetCount)();

Int32 (*wDisplayModeGetDepth)(Int32 modeNumber);

wVector2u (*wDisplayModeGetResolution)(Int32 ModeNumber);

wVector2u (*wDisplayGetCurrentResolution)();

Int32 (*wDisplayGetCurrentDepth)();

///Set the current Gamma Value for the Display.
void (*wDisplaySetGammaRamp)(wColor3f gamma,float brightness,float contrast);

void (*wDisplayGetGammaRamp)(wColor3f* gamma,float* brightness,float* contrast);

bool (*wDisplaySetDepth)(UInt32 depth);

///Возвращает нормализованный вектор
wVector3f (*wMathVector3fNormalize)(wVector3f source);

///Возвращает длину вектора
Float32 (*wMathVector3fGetLength)(wVector3f vector);

///Get the rotations that would make a (0,0,1) direction vector
///point in the same direction as this direction vector.
wVector3f (*wMathVector3fGetHorizontalAngle)(wVector3f vector);

///Возвращает инвертированный вектор (все координаты меняют знак)
wVector3f (*wMathVector3fInvert)(wVector3f vector);

///Суммирует два вектора
wVector3f (*wMathVector3fAdd)(wVector3f vector1,
									 wVector3f vector2);

///Вычитает из вектора 1 вектор 2
wVector3f (*wMathVector3fSubstract)(wVector3f vector1,
										   wVector3f vector2);

///Векторное произведение векторов
wVector3f (*wMathVector3fCrossProduct)(wVector3f vector1,
											  wVector3f vector2);
///Скалярное произведение векторов
Float32 (*wMathVector3fDotProduct)(wVector3f vector1,
										  wVector3f vector2);

///Определяет кратчайшее расстояние между векторами
Float32 (*wMathVector3fGetDistanceFrom)(wVector3f vector1,
											   wVector3f vector2);

///Возвращает интерполированный вектор
wVector3f (*wMathVector3fInterpolate)(wVector3f vector1,
											 wVector3f vector2,
											 Float64 d);

///Возвращает случайное число из интервала (first, last)
Float64 (*wMathRandomRange)(Float64 first,
								   Float64 last);

///Из градусов- в радианы///
Float32 (*wMathDegToRad)(Float32 degrees);

///Из радиан- в градусы
Float32 (*wMathRadToDeg)(Float32 radians);

///Математически правильное округление///
Float32 (*wMathRound)(Float32 value);

///Округление в большую сторону///
Int32 (*wMathCeil)(Float32 value);

///Округление в меньшую сторону///
Int32 (*wMathFloor)(Float32 value);


///returns if a equals b, taking possible rounding errors into account
bool (*wMathFloatEquals)(Float32 value1,
								Float32 value2,
								Float32 tolerance);

///returns if a equals b, taking an explicit rounding tolerance into account
bool (*wMathIntEquals)(Int32 value1,
							  Int32 value2,
							  Int32 tolerance);

///returns if a equals b, taking an explicit rounding tolerance into account
bool (*wMathUIntEquals)(UInt32 value1,
							   UInt32 value2,
							   UInt32 tolerance);

///returns if a equals zero, taking rounding errors into account
bool (*wMathFloatIsZero)(Float32 value,
								Float32 tolerance);

///returns if a equals zero, taking rounding errors into account
bool (*wMathIntIsZero)(Int32 value,
							  Int32 tolerance);

///returns if a equals zero, taking rounding errors into account
bool (*wMathUIntIsZero)(UInt32 value,
							   UInt32 tolerance);

///Возвращает больший Float32 из двух///
Float32 (*wMathFloatMax2)(Float32 value1,
								 Float32 value2);

///Возвращает больший Float32 из трех///
Float32 (*wMathFloatMax3)(Float32 value1,
								 Float32 value2,
								 Float32 value3);

///Возвращает больший Int32 из двух///
Float32 (*wMathIntMax2)(Int32 value1,
							   Int32 value2);

///Возвращает больший Int32 из трех///
Float32 (*wMathIntMax3)(Int32 value1,
							   Int32 value2,
							   Int32 value3);

///Возвращает меньший Float32 из двух///
Float32 (*wMathFloatMin2)(Float32 value1,
								 Float32 value2);

///Возвращает меньший Float32 из трех///
Float32 (*wMathFloatMin3)(Float32 value1,
								 Float32 value2,
								 Float32 value3);

///Возвращает меньший Int32 из двух///
Float32 (*wMathIntMin2)(Int32 value1,
							   Int32 value2);

///Возвращает меньший Int32 из трех///
Float32 (*wMathIntMin3)(Int32 value1,
							   Int32 value2,
							   Int32 value3);

///wUtil///
const char* (*wUtilVector3fToStr)(wVector3f vector,
										 const char* s,
										 bool addNullChar);

const char* (*wUtilVector2fToStr)(wVector2f vector,
										 const char* s,
										 bool addNullChar);

const char* (*wUtilColor4sToStr)(wColor4s color,
										const char* s,
										bool addNullChar);

const char* (*wUtilColor4fToStr)(wColor4f color,
										const char* s,
										bool addNullChar);

UInt32 (*wUtilColor4sToUInt)(wColor4s color);

UInt32 (*wUtilColor4fToUInt)(wColor4f color);

wColor4s (*wUtilUIntToColor4s)(UInt32 color);

wColor4f (*wUtilUIntToColor4f)(UInt32 color);

///Convert a simple string of base 10 digits into a signed 32 bit integer.
Int32 (*wUtilStrToInt)(const char* str);

const char* (*wUtilIntToStr)(Int32 value,
									bool addNullChar);

///Converts a sequence of digits into a whole positive floating point value.
///Only digits 0 to 9 are parsed.
///Parsing stops at any other character, including sign characters or a decimal point.
Float32 (*wUtilStrToFloat)(const char* str);

const char* (*wUtilFloatToStr)(Float32 value,
									  bool addNullChar);

///Convert a simple string of base 10 digits into an unsigned 32 bit integer.
UInt32 (*wUtilStrToUInt)(const char* str);

const char* (*wUtilUIntToStr)(UInt32 value,
									 bool addNullChar);

///swaps the content of the passed parameters
void (*wUtilSwapInt)(int* value1,
							int* value2);

///swaps the content of the passed parameters
void (*wUtilSwapUInt)(UInt32* value1,
							 UInt32* value2);

///swaps the content of the passed parameters
void (*wUtilSwapFloat)(float* value1,
							  float* value2);

///Конвертирует расширенную строку в С-строку
const char* (*wUtilWideStrToStr)(const wchar_t* str);

///Конвертирует С-строку в расширенную строку
const wchar_t* (*wUtilStrToWideStr)(const char* str);

///Добавляет символ конца строки
void (*wUtilStrAddNullChar)(const char** str);

///Добавляет символ конца строки к расширенной строке
void (*wUtilWideStrAddNullChar)(const wchar_t** str);

///wEngine///
void (*wEngineSetClipboardText)(const wchar_t* text);

const wchar_t* (*wEngineGetClipboardText)();

bool (*wEngineLoadCreationParameters)(wEngineCreationParameters* outParams,
											 const char* xmlFilePath);

bool (*wEngineSaveCreationParameters)(wEngineCreationParameters* inParams,
											 const char* xmlFilePath);

bool (*wEngineStartWithGui)(const wchar_t* captionText,
								   const char* fontPath,
								   const char* logo,
								   wLanguage lang,
								   wEngineCreationParameters* outParams,
								   const char* inXmlConfig);

bool (*wEngineStart)(wDriverTypes iDevice,
							wVector2u size,
							UInt32 iBPP,
							bool boFullscreen,
							bool boShadows,
							bool boCaptureEvents,
							bool vsync);

void (*wEngineCloseByEsc)();

bool (*wEngineStartAdvanced)(wEngineCreationParameters params);

void (*wEngineSetTransparentZWrite) (bool value);

bool (*wEngineRunning)();

void (*wEngineSleep)(UInt32 Ms,
							bool pauseTimer);

///Cause the device to temporarily pause execution and let other processes run.
void (*wEngineYield)();

void (*wEngineSetViewPort)(wVector2i fromPos,
								  wVector2i toPos);

bool (*wEngineIsQueryFeature)(wVideoFeatureQuery  feature);

void (*wEngineDisableFeature)(wVideoFeatureQuery feature,
									 bool flag);

bool (*wEngineStop)(bool closeDevice);

void (*wEngineSetFPS)(UInt32 limit);

wMaterial* (*wEngineGetGlobalMaterial)();

/*
Get the 2d override material for altering its values.
The 2d override materual allows to alter certain render states of the 2d methods.
Not all members of SMaterial are honored, especially not MaterialType and Textures.
Moreover, the zbuffer is always ignored, and lighting is always off.
All other flags can be changed, though some might have to effect in most cases.
Please note that you have to enable/disable this effect with enableInitMaterial2D().
This effect is costly, as it increases the number of state changes considerably.
Always reset the values when done.
*/
wMaterial* (*wEngineGet2dMaterial)();

void (*wEngineSet2dMaterial)(bool value);

Int32 (*wEngineGetFPS)();

void (*wEngineShowLogo)(bool value);

///wScene/////
bool (*wSceneBegin) (wColor4s color);

bool (*wSceneBeginAdvanced)(wColor4s backColor,
								   bool clearBackBuffer,
								   bool clearZBuffer);

void (*wSceneLoad)(const char* filename );

void (*wSceneSave)(const char* filename );

void (*wSceneDrawAll)();

bool (*wSceneEnd)();

void (*wSceneDrawToTexture) (wTexture* renderTarget );

void (*wSceneSetRenderTarget)(wTexture*renderTarget,
									 wColor4s backColor,
									 bool clearBackBuffer,
									 bool clearZBuffer);

void (*wSceneSetAmbientLight)(wColor4f color);

wColor4f (*wSceneGetAmbientLight)();

void (*wSceneSetShadowColor)(wColor4s color);

wColor4s (*wSceneGetShadowColor)();

void (*wSceneSetFog)(wColor4s color,
							wFogType fogtype,
							Float32 start,
							Float32 end,
							Float32 density,
							bool pixelFog,
							bool rangeFog);

void (*wSceneGetFog)(wColor4s* color,
							wFogType* fogtype,
							float* start,
							float* end,
							float* density,
							bool*  pixelFog,
							bool*  rangeFog);

wNode* (*wSceneGetActiveCamera)();

///Поиск текстуры по АБСОЛЮТНОМУ пути
///Если требуется искать по относительному пути,
///используйте сначала wFileGetAbsolutePath
wTexture* (*wSceneGetTextureByName)(const char* name);

// When animating a mesh by "Morphing" or "Skeletal Animation" such as "*.md3", "*.x" and "*.b3d" using "Shaders" for rendering we can improve the final render if we "Cyclically Update" the "Tangents" and "Binormals"..
// We presume that our meshes are, among others, textured with a "NORMAL MAP" used by the "Shader" (cg, hlsl, or glsl etc) in calculating diffuse and specular.
// We also have one or more lights used by the shader.

// Update TANGENTS & BINORMALS at every frame for a skinned animation..

// We dont want to do this for meshes like levels etc..
// We also dont want to do it for Rotating, Scaled and translated meshes..(we can however, as a bonus, scale, rotate and translate these)
// Only for animated skinned and morph based meshes..
// This is loose code that works. If anyone can improve it for the engine itself that would be great..
// You'll probably ID possible improvements immediately!

// At every N'th Frame we loop through all the vertices..
// 1. In the loop we Access the VERTEX of POINT A of the "INDEXED TRIANGLE"..
// 2. We interrogate the "OTHER TWO" VERTICES (which thankfully do change at each frame) for their Positions, Normals, and UV Coords to
//    Genertate a "BRAND NEW" (animated) TANGENT and BINORMAL. (We may want to calculate the the "Binormal" in the SHADER to save time)
// 3. We REWRITE the Tangent and Binormal for our SELECTED TRIANGLE POINT.
// 4. We DO THE SAME for POINTS B and C..
//

//  GENERATE "LIVING" TANGENTS & BINBORMALS
//  REMEMBER!
//  WE NEED "LOOP THROUGH ALL ITS BUFFERS"
//  WE NEED "LOOP THROUGH ALL THOSE BUFFER VERTICES"
// Possible types of (animated) meshes.
// Enumerator:
// 1  EAMT_MD2            Quake 2 MD2 model file..
// 2  EAMT_MD3            Quake 3 MD3 model file..
// 10 EAMT_MDL_HALFLIFE   Halflife MDL model file..
// Below is what an item type must be for it to qualify for Tangent Updates..
// 11 EAMT_SKINNED        generic skinned mesh "*.x" "*.b3d" etc.. (see Morphed too!)
//
// We want to change tangents for skinned meshes only so we must determine which ones are "Skinned"..
// This may change if we add and remove meshes during runtime..

void (*wMeshUpdateTangentsAndBinormals)(wMesh* mesh);

void (*wSceneDestroyAllTextures)();

void (*wSceneDestroyAllNodes)();

///Можно для поиска меша использовать относительный путь
wMesh* (*wSceneGetMeshByName)(const char* name);

wMesh* (*wSceneGetMeshByIndex)(unsigned int index);

UInt32 (*wSceneGetMeshesCount)();

void (*wSceneDestroyAllMeshes)();

bool (*wSceneIsMeshLoaded)(const char* filePath);

void (*wSceneDestroyAllUnusedMeshes)();

UInt32 (*wSceneGetPrimitivesDrawn)();

UInt32 (*wSceneGetNodesCount)();

wNode* (*wSceneGetNodeById)(Int32 id );

wNode* (*wSceneGetNodeByName)(const char* name );

wNode* (*wSceneGetRootNode)();

///wWindow///
void (*wWindowSetCaption)(const wchar_t* wcptrText);

void (*wWindowGetSize)(wVector2u* size);

bool (*wWindowIsFullscreen)();

bool (*wWindowIsResizable)();

bool (*wWindowIsActive)();

bool (*wWindowIsFocused)();

bool (*wWindowIsMinimized)();

void (*wWindowMaximize)();

void (*wWindowMinimize)();

void (*wWindowRestore)();

void (*wWindowSetResizable)(bool resizable);

void (*wWindowMove)(wVector2u pos);

void (*wWindowPlaceToCenter)();

void (*wWindowResize)(wVector2u newSize);

void (*wWindowSetFullscreen)(bool value);

bool (*wWindowSetDepth)(UInt32 depth);
///wPostEffect///
wPostEffect* (*wPostEffectCreate)(wPostEffectId effectnum,
										 wPostEffectQuality quality,
										 Float32 value1,
										 Float32 value2,
										 Float32 value3,
										 Float32 value4,
										 Float32 value5,
										 Float32 value6,
										 Float32 value7,
										 Float32 value8);

void (*wPostEffectDestroy)(wPostEffect* ppEffect);

void (*wPostEffectSetParameters)(wPostEffect* ppEffect,
										Float32 para1,
										Float32 para2,
										Float32 para3,
										Float32 para4,
										Float32 para5,
										Float32 para6,
										Float32 para7,
										Float32 para8);

void (*wPostEffectsDestroyAll)();

///wXEffects///
void (*wXEffectsStart)(bool vsm,
							  bool softShadows,
							  bool bitDepth32,
							  wColor4s color);


void (*wXEffectsEnableDepthPass)(bool enable);

void (*wXEffectsAddPostProcessingFromFile)(const char* name,
												  Int32 effectType);

void (*wXEffectsSetPostProcessingUserTexture)(wTexture* texture );

void (*wXEffectsAddShadowToNode)(wNode* node,
										wFilterType filterType,
										wShadowMode shadowType);

void (*wXEffectsRemoveShadowFromNode)(wNode* node );

void (*wXEffectsExcludeNodeFromLightingCalculations)(wNode* node );

void (*wXEffectsAddNodeToDepthPass)(wNode* node );

void (*wXEffectsSetAmbientColor)(wColor4s color);

void (*wXEffectsSetClearColor)(wColor4s color);

void (*wXEffectsAddShadowLight)(UInt32 shadowDimen,
									   wVector3f position,
									   wVector3f target,
									   wColor4f color,
									   Float32 lightNearDist ,
									   Float32 lightFarDist,
									   Float32 angleDeg);

UInt32 (*wXEffectsGetShadowLightsCount)();

wTexture* (*wXEffectsGetShadowMapTexture)(UInt32 resolution,
												 bool secondary);

wTexture* (*wXEffectsGetDepthMapTexture)();

void (*wXEffectsSetScreenRenderTargetResolution)(wVector2u size);

void (*wXEffectsSetShadowLightPosition)(UInt32 index,
											   wVector3f position);

wVector3f (*wXEffectsGetShadowLightPosition)(UInt32 index);

void (*wXEffectsSetShadowLightTarget)(UInt32 index,
											 wVector3f target);

wVector3f (*wXEffectsGetShadowLightTarget)(UInt32 index);

void (*wXEffectsSetShadowLightColor)(UInt32 index,
											wColor4f color);

wColor4f (*wXEffectsGetShadowLightColor)(UInt32 index);

void (*wXEffectsSetShadowLightMapResolution)(UInt32 index,
													UInt32 resolution);

UInt32 (*wXEffectsGetShadowLightMapResolution)(UInt32 index);

Float32 (*wXEffectsGetShadowLightFarValue)(UInt32 index);

///wAnimator///
wAnimator* (*wAnimatorFollowCameraCreate)(wNode* node,
												 wVector3f position);

wAnimator* (*wAnimatorCollisionResponseCreate)(wSelector* selector,
													  wNode* node,
													  Float32 slidingValue);

void (*wAnimatorCollisionResponseSetParameters)(wAnimator* anim,
													   wAnimatorCollisionResponse params);

void (*wAnimatorCollisionResponseGetParameters)(wAnimator* anim,
													   wAnimatorCollisionResponse* params);

wAnimator* (*wAnimatorDeletingCreate)(wNode* node,
											 Int32 delete_after );

wAnimator* (*wAnimatorFlyingCircleCreate)(wNode* node,
												 wVector3f pos,
												 Float32 radius,
												 Float32 speed,
												 wVector3f direction,
												 Float32 startPos,
												 Float32 radiusEllipsoid);

wAnimator *(*wAnimatorFlyingStraightCreate)(wNode* node,
												   wVector3f startPoint,
												   wVector3f endPoint,
												   UInt32 time,
												   bool loop );

wAnimator* (*wAnimatorRotationCreate)(wNode* node,
											 wVector3f pos);

wAnimator* (*wAnimatorSplineCreate)(wNode* node,
										   Int32 iPoints,
										   wVector3f *points,
										   Int32 time,
										   Float32 speed,
										   Float32 tightness);

wAnimator* (*wAnimatorFadingCreate)(wNode* node,
										   Int32 delete_after,
										   Float32 scale );

void (*wAnimatorDestroy)(wNode* node,
								wAnimator* anim );

///wTpsCamera///
wNode* (*wTpsCameraCreate)(const char* name);

void (*wTpsCameraDestroy)(wNode* ctrl);

void (*wTpsCameraUpdate)(wNode* ctrl);

void (*wTpsCameraSetTarget)(wNode* ctrl,
								   wNode* node);

void (*wTpsCameraRotateHorizontal)(wNode* ctrl,
										  Float32 rotVal);

void (*wTpsCameraRotateVertical)(wNode* ctrl,
										Float32 rotVal);

void (*wTpsCameraSetHorizontalRotation)(wNode* ctrl,
											   Float32 rotVal);

void (*wTpsCameraSetVerticalRotation)(wNode* ctrl,
											 Float32 rotVal);

void (*wTpsCameraZoomIn)(wNode* ctrl);

void (*wTpsCameraZoomOut)(wNode* ctrl);

wNode* (*wTpsCameraGetCamera)(wNode* ctrl);

void (*wTpsCameraSetCurrentDistance)(wNode* ctrl,
											Float32 dist);

void (*wTpsCameraSetRelativeTarget)(wNode* ctrl,
										   wVector3f target);

void (*wTpsCameraSetDefaultDistanceDirection)(wNode* ctrl,
													 wVector3f dir);

void (*wTpsCameraSetMaximalDistance)(wNode* ctrl,
											Float32 value);

void (*wTpsCameraSetMinimalDistance)(wNode* ctrl,
											Float32 value);

void (*wTpsCameraSetZoomStepSize)(wNode* ctrl,
										 Float32 value);

void (*wTpsCameraSetHorizontalSpeed)(wNode* ctrl,
											Float32 value);

void (*wTpsCameraSetVerticalSpeed)(wNode* ctrl,
										  Float32 value);
///wFpsCamera///
wNode*  (*wFpsCameraCreate)(Float32 rotateSpeed,
								   Float32 moveSpeed,
								   wKeyMap* keyMapArray,
								   Int32 keyMapSize,
								   bool noVerticalMovement,
								   Float32 jumpSpeed);

Float32 (*wFpsCameraGetSpeed)(wNode* camera);

void (*wFpsCameraSetSpeed)(wNode* camera,
								  Float32 newSpeed);

Float32 (*wFpsCameraGetRotationSpeed)(wNode* camera);

void (*wFpsCameraSetRotationSpeed)(wNode* camera,
										  Float32 rotSpeed);

void (*wFpsCameraSetKeyMap)(wNode* camera,
								   wKeyMap* map,
								   UInt32 count);

void (*wFpsCameraSetVerticalMovement)(wNode* camera,
											 bool value);

void (*wFpsCameraSetInvertMouse)(wNode* camera,
										bool value);

void (*wFpsCameraSetMaxVerticalAngle)(wNode* camera,
											 float newValue);

Float32 (*wFpsCameraGetMaxVerticalAngle)(wNode* camera);

///wCamera///
wNode*  (*wCameraCreate)(wVector3f pos,
								wVector3f target);

wNode* (*wMayaCameraCreate)(Float32 rotateSpeed,
								   Float32 zoomSpeed,
								   Float32 moveSpeed);

void (*wCameraSetTarget)(wNode* camera,
								wVector3f target);

wVector3f (*wCameraGetTarget)(wNode* camera);

wVector3f (*wCameraGetUpDirection)(wNode* camera);

void (*wCameraSetUpDirection)(wNode* camera,
									 wVector3f upDir);

void (*wCameraGetOrientation)(wNode* camera,
									 wVector3f* upDir,
									 wVector3f* forwardDir,
									 wVector3f* rightDir);

void (*wCameraSetClipDistance)(wNode* camera,
									  Float32 farDistance,
									  Float32 nearDistance);

void (*wCameraSetActive)(wNode* camera);

void (*wCameraSetFov)(wNode* camera,
							 Float32 fov );

Float32 (*wCameraGetFov)(wNode* camera);

void (*wCameraSetOrthogonal)(wNode* camera,
									wVector3f vec);

void (*wCameraRevolve)(wNode* camera,
							  wVector3f angleDeg,
							  wVector3f offset);

void (*wCameraSetUpAtRightAngle)(wNode* camera );

void (*wCameraSetAspectRatio)(wNode* camera,
									 Float32 aspectRatio );

void (*wCameraSetInputEnabled)(wNode* camera,
									  bool value);

bool (*wCameraIsInputEnabled)(wNode* camera);

void (*wCameraSetCollisionWithScene)(wNode* camera,
											wVector3f radius,
											wVector3f gravity,
											wVector3f offset,
											Float32 slidingValue);

///wRtsCamera///
wNode* (*wRtsCameraCreate)(wVector3f pos,
								  wVector2f offsetX,
								  wVector2f offsetZ,
								  wVector2f offsetDistance,
								  wVector2f offsetAngle,
								  Float32 driftSpeed,
								  Float32 scrollSpeed,
								  Float32 mouseSpeed,
								  Float32 orbit,
								  wMouseButtons mouseButtonActive);


///wCollision///
wSelector* (*wCollisionGroupCreate)();

void (*wCollisionGroupAddCollision)(wSelector* group,
										   wSelector* selector );

void (*wCollisionGroupRemoveAll)(wSelector* geoup );

void (*wCollisionGroupRemoveCollision)(wSelector* group,
											  wSelector* selector );

wSelector* (*wCollisionCreateFromMesh)(wMesh* mesh,
											  wNode* node,
											  Int32 iframe );

wSelector* (*wCollisionCreateFromStaticMesh)(wMesh* staticMesh,
													wNode* node);

wSelector* (*wCollisionCreateFromBatchingMesh)(wMesh* mesh,
													  wNode* node);

wSelector* (*wCollisionCreateFromMeshBuffer)(wMeshBuffer* meshbuffer,
													wNode* node);

wSelector* (*wCollisionCreateFromOctreeMesh)(wMesh* mesh,
													wNode* node,
													Int32 iframe);

wSelector* (*wCollisionCreateFromBox)(wNode*  node );

wSelector* (*wCollisionCreateFromTerrain)(wNode*  node,
												 Int32 level_of_detail);

wNode* (*wCollisionGetNodeFromCamera)(wNode* camera );

wNode* (*wCollisionGetNodeFromRay)(wVector3f* vectorStart,
										  wVector3f* vectorEnd );

wNode* (*wCollisionGetNodeChildFromRay)(wNode* node,
											   Int32 id,
											   bool recurse,
											   wVector3f* vectorStart,
											   wVector3f* vectorEnd);

wNode* (*wCollisionGetNodeAndPointFromRay)(wVector3f* vectorStart,
												  wVector3f* vectorEnd,
												  wVector3f* colPoint,
												  wVector3f* normal,
												  Int32 id,
												  wNode* rootNode );

wNode* (*wCollisionGetNodeFromScreen)(wVector2i screenPos,
											 Int32 idBitMask,
											 bool bNoDebugObjects,
											 wNode* root);

wVector2i (*wCollisionGetScreenCoordFrom3dPosition)(wVector3f pos);

void (*wCollisionGetRayFromScreenCoord)(wNode* camera,
											   wVector2i screenCoord,
											   wVector3f* vectorStart,
											   wVector3f* vectorEnd );

wVector3f (*wCollisionGet3dPositionFromScreen)(wNode* camera,
													  wVector2i screenPos,
													  wVector3f normal,
													  Float32 distanceFromOrigin);

wVector2f (*wCollisionGet2dPositionFromScreen)(wNode* camera,
													  wVector2i screenPos);

bool (*wCollisionGetPointFromRay)(wSelector* ts,
										 wVector3f* vectorStart,
										 wVector3f* vectorEnd,
										 wVector3f* collisionPoint,
										 wVector3f* vectorNormal,
										 wTriangle* collisionTriangle,
										 wNode** collNode);

wNode* (*wCollisionGetNodeChildFromPoint)(wNode* node,
												 Int32 id,
												 bool recurse,
												 wVector3f* vectorPoint );

void (*wCollisionGetResultPosition)(wSelector* selector,
										   wVector3f* ellipsoidPosition,
										   wVector3f* ellipsoidRadius,
										   wVector3f* velocity,
										   wVector3f* gravity,
										   Float32 slidingSpeed,
										   wVector3f* outPosition,
										   wVector3f* outHitPosition,
										   int* outFalling);

///wFile///
void (*wFileAddZipArchive)(const char* cptrFile,
								  bool boIgnoreCase,
								  bool boIgnorePaths);

void (*wFileAddArchive)(const char *cptrFile,
							   bool boIgnoreCase,
							   bool boIgnorePaths,
							   wFileArchiveType aType,
							   const char* password
);

void (*wFileSetWorkingDirectory)(const char* cptrPath );

const char* (*wFileGetWorkingDirectory)();

void (*wFileAddPakArchive)(const char* cptrFile,
								  bool boIgnoreCase,
								  bool boIgnorePaths );

void (*wFileAddDirectory)(const char* cptrFile,
								 bool boIgnoreCase,
								 bool boIgnorePaths );

bool (*wFileIsExist)(const char* cptrFile );

const char* (*wFileGetAbsolutePath)(const char* cptrPath);

const char* (*wFileGetRelativePath)(const char* cptrPath,
										   const char* directory);

///Get the base part of a filename, i.e. the name without the directory part.
///If no directory is prefixed, the full name is returned.
const char* (*wFileGetBaseName)(const char* cptrPath,
									   bool keepExtension);

const char* (*wFileGetDirectory)(const char*cptrPath);

///for read///
wFile* (*wFileOpenForRead)(const char* cptrFile );

Int32 (*wFileRead)(wFile* file,
						  void* buffer,
						  UInt32 sizeToRead);

Int64 (*wFileGetSize)(wFile* file);

///for write///
wFile* (*wFileCreateForWrite)(const char* cptrFile,
									 bool append );
Int32 (*wFileWrite)(wFile* file,
						   const void* buffer,
						   UInt32 sizeToWrite);

///for read/write///
const char* (*wFileGetName)(wFile* file);

Int64 (*wFileGetPos)(wFile* file);

bool (*wFileSeek)(wFile* file,
						 Int64 finalPos,
						 bool relativeMovement);

void (*wFileClose)(wFile* file);

///XMLReader///
wXmlReader* (*wXmlReaderCreate)(const char* cptrFile );

wXmlReader* (*wXMLReaderCreateUTF8)(const char* cptrFile );

//Returns attribute count of the current XML node
UInt32 (*wXmlGetAttributesCount)(wXmlReader* xml);

//Returns the value of an attribute
const wchar_t* (*wXmlGetAttributeNameByIdx)(wXmlReader* xml,
												   Int32 idx);

//Returns the value of an attribute
const wchar_t* (*wXmlGetAttributeValueByIdx)(wXmlReader* xml,
													Int32 idx);

//Returns the value of an attribute
const wchar_t* (*wXmlGetAttributeValueByName)(wXmlReader* xml,
													 const wchar_t* name);

//Returns the value of an attribute as float
Float32 (*wXmlGetAttributeValueFloatByIdx)(wXmlReader* xml,
												  Int32 idx);

//Returns the value of an attribute as float
Float32 (*wXmlGetAttributeValueFloatByName)(wXmlReader* xml,
												   const wchar_t* name);

//Returns the value of an attribute as integer
Int32 (*wXmlGetAttributeValueIntByIdx)(wXmlReader* xml,
											  Int32 idx);

//Returns the value of an attribute as integer
Int32 (*wXmlGetAttributeValueIntByName)(wXmlReader* xml,
											   const wchar_t* name);

//Returns the value of an attribute in a safe way
const wchar_t* (*wXmlGetAttributeValueSafeByName)(wXmlReader* xml,
														 const wchar_t* name);

//Returns the name of the current node
const wchar_t* (*wXmlGetNodeName)(wXmlReader* xml);

//Returns data of the current node
const wchar_t* (*wXmlGetNodeData)(wXmlReader* xml);

//Returns format of the source xml file
wTextFormat (*wXmlGetSourceFormat)(wXmlReader* xml);

//Returns format of the strings returned by the parser
wTextFormat (*wXmlGetParserFormat)(wXmlReader* xml);

//Returns the type of the current XML node
wXmlNodeType (*wXmlGetNodeType)(wXmlReader* xml);

//Returns if an element is an empty element, like <foo />
bool (*wXmlIsEmptyElement)(wXmlReader* xml);

//Reads forward to the next xml node
bool (*wXmlRead)(wXmlReader* xml);

void (*wXmlReaderDestroy)(wXmlReader* xml);

///XmlWriter///
wXmlWriter* (*wXmlWriterCreate)(const char* cptrFile );
//Writes the closing tag for an element. Like "</foo>"
void (*wXmlWriteClosingTag)(wXmlWriter* xml,
								   const wchar_t* name);

//Writes a comment into the xml file
void (*wXmlWriteComment)(wXmlWriter* xml,
								const wchar_t* comment);

//Writes a line break
void  (*wXmlWriteLineBreak)(wXmlWriter* xml);

//Writes a text into the file
void (*wXmlWriteText)(wXmlWriter* xml,
							 const wchar_t* file);

//Writes an xml 1.0 heade
void  (*wXmlWriteHeader)(wXmlWriter* xml);

void (*wXmlWriteElement)(wXmlWriter* xml,
								const wchar_t* name,
								bool empty,
								const wchar_t* attr1Name,const wchar_t* attr1Value,
								const wchar_t* attr2Name,const wchar_t* attr2Value,
								const wchar_t* attr3Name,const wchar_t* attr3Value,
								const wchar_t* attr4Name,const wchar_t* attr4Value,
								const wchar_t* attr5Name,const wchar_t* attr5Value);

void (*wXmlWriterDestroy)(wXmlWriter* xml);

///wInput///
///keyboard///
///Get character without waiting for Return to be pressed.
bool (*wInputWaitKey)();

bool (*wInputIsKeyEventAvailable)();

wKeyEvent* (*wInputReadKeyEvent)();

bool (*wInputIsKeyUp)(wKeyCode num);

bool (*wInputIsKeyHit)(wKeyCode num);

bool (*wInputIsKeyPressed)(wKeyCode num);

///mouse///
bool (*wInputIsMouseEventAvailable)();

wMouseEvent* (*wInputReadMouseEvent)();

void (*wInputSetCursorVisible)(bool boShow );

bool (*wInputIsCursorVisible)();

void (*wInputSetMousePosition)(wVector2i* position);

void (*wInputGetMousePosition)(wVector2i* position);

void (*wInputSetMouseLogicalPosition)(wVector2f* position);

void (*wInputGetMouseLogicalPosition)(wVector2f* position);

Float32 (*wInputGetMouseWheel)();

void (*wInputGetMouseDelta)(wVector2i* deltaPos);

bool (*wInputIsMouseUp)(wMouseButtons num);

bool (*wInputIsMouseHit)(wMouseButtons num);

bool (*wInputIsMousePressed)(wMouseButtons num);

Int32 (*wInputGetMouseX)();

Int32 (*wInputGetMouseY)();

Int32 (*wInputGetMouseDeltaX)();

Int32 (*wInputGetMouseDeltaY)();

///joystick///
bool (*wInputActivateJoystick)();

UInt32 (*wInputGetJoysitcksCount)();

void (*wInputGetJoystickInfo)(UInt32 joyIndex,
									 wJoystickInfo* joyInfo);

bool (*wInputIsJoystickEventAvailable)();

wJoystickEvent* (*wInputReadJoystickEvent)();


///wLight///
wNode* (*wLightCreate)(wVector3f position,
							  wColor4f color,
							  Float32 radius);

void (*wLightSetAmbientColor)(wNode* light,
									 wColor4f color);

wColor4f (*wLightGetAmbientColor)(wNode* light);

void (*wLightSetSpecularColor)(wNode* light,
									  wColor4f color);

wColor4f (*wLightGetSpecularColor)(wNode* light);

void (*wLightSetAttenuation)(wNode* light,
									wVector3f  attenuation); //.x-constant, .y- linear, .z- quadratic

wVector3f (*wLightGetAttenuation)(wNode* light);

void (*wLightSetCastShadows)(wNode* light,
									bool castShadows);

bool (*wLightIsCastShadows)(wNode* light);

void (*wLightSetDiffuseColor)(wNode* light,
									 wColor4f color);

wColor4f (*wLightGetDiffuseColor)(wNode* light);

void (*wLightSetFallOff)(wNode* light,
								Float32 FallOff);

Float32 (*wLightGetFallOff)(wNode* light);

void (*wLightSetInnerCone)(wNode* light,
								  Float32 InnerCone);

Float32 (*wLightGetInnerCone)(wNode* light);

void (*wLightSetOuterCone)(wNode* light,
								  Float32 OuterCone);

Float32 (*wLightGetOuterCone)(wNode* light);

void (*wLightSetRadius)(wNode* light,
							   Float32 Radius );

Float32 (*wLightGetRadius)(wNode* light);

void (*wLightSetType)(wNode* light,
							 wLightType Type );

wLightType (*wLightGetType)(wNode* light);

//Read-ONLY! Direction of the light.
//If Type is WLT_POINT, it is ignored.
//Changed via light scene node's rotation.
wVector3f (*wLightGetDirection)(wNode* light);


///wBillboardGroup///
wNode* (*wBillboardGroupCreate)(wVector3f position,
									   wVector3f rotation,
									   wVector3f scale);


void (*wBillboardGroupSetShadows)(wNode* node,
										 wVector3f direction,
										 Float32 intensity,
										 Float32 ambient );

void (*wBillboardGroupResetShadows)(wNode* node);

UInt32 (*wBillboardGroupGetSize)(wNode* node );

wMeshBuffer* (*wBillboardGroupGetMeshBuffer)(wNode *node);

wBillboard* (*wBillboardGroupGetFirstElement)(wNode* node);

void (*wBillboardGroupUpdateForce)(wNode* node );

wBillboard* (*wBillboardGroupAddElement)(wNode* node,
												wVector3f position,
												wVector2f size,
												Float32 roll,
												wColor4s color);

wBillboard* (*wBillboardGroupAddElementByAxis)(wNode* node,
													  wVector3f position,
													  wVector2f size,
													  Float32 roll,
													  wColor4s color,
													  wVector3f axis);

void (*wBillboardGroupRemoveElement)(wNode* node,
											wBillboard* billboard);

///wBillboard///
wNode* (*wBillboardCreate)(wVector3f position,
								  wVector2f size);

void (*wBillboardSetEnabledAxis)(wNode* billboard,
										wBillboardAxisParam param);

wBillboardAxisParam (*wBillboardGetEnabledAxis)(wNode* billboard);

void (*wBillboardSetColor)(wNode* node,
								  wColor4s topColor,
								  wColor4s bottomColor);

void (*wBillboardSetSize)(wNode* node,
								 wVector2f size);

wNode* (*wBillboardCreateText)(wVector3f position,
									  wVector2f size,
									  wFont* font,
									  const wchar_t* text,
									  wColor4s topColor,
									  wColor4s bottomColor);

///wSkyBox///
wNode* (*wSkyBoxCreate)(wTexture* texture_up,
							   wTexture* texture_down,
							   wTexture* texture_left,
							   wTexture* texture_right,
							   wTexture* texture_front,
							   wTexture* texture_back );

///wSkyDome///
wNode* (*wSkyDomeCreate)(wTexture* texture_file,
								UInt32  horiRes,
								UInt32  vertRes,
								Float64  texturePercentage,
								Float64  spherePercentage,
								Float64 domeRadius);


void (*wSkyDomeSetColor) (wNode* dome,
								 wColor4s horizonColor,
								 wColor4s zenithColor);

void (*wSkyDomeSetColorBand)(wNode* dome,
									wColor4s horizonColor,
									Int32 position,
									Float32 fade,
									bool additive );

void (*wSkyDomeSetColorPoint)(wNode* dome,
									 wColor4s horizonColor,
									 wVector3f position,
									 Float32 radius,
									 Float32 fade,
									 bool additive );

///wLodManager///
wNode* (*wLodManagerCreate)(UInt32 fadeScale,
								   bool useAlpha,
								   void (*callback)(UInt32, wNode*));

void (*wLodManagerAddMesh)(wNode* node,
								  wMesh* mesh,
								  Float32 distance);

void (*wLodManagerSetMaterialMap)(wNode* node,
										 wMaterialTypes source,
										 wMaterialTypes target );

///wZoneManager///
wNode* (*wZoneManagerCreate)(Float32 initialNearDistance,
									Float32 initialFarDistance );

void (*wZoneManagerSetProperties)(wNode* node,
										 Float32 newNearDistance,
										 Float32 newFarDistance,
										 bool accumulateChildBoxes);

void (*wZoneManagerSetBoundingBox)(wNode* node,
										  wVector3f position,
										  wVector3f size);

void (*wZoneManagerAddTerrain)(wNode* node,
									  wNode* terrainSource,
									  const char* structureMap,
									  const char* colorMap,
									  const char* detailMap,
									  wVector2i pos,
									  Int32 sliceSize );

///wNode///
///primitives///
wNode* (*wNodeCreateEmpty)();

wNode* (*wNodeCreateCube)(Float32 size,
								 wColor4s color);

wNode* (*wNodeCreateSphere)(Float32 radius,
								   Int32 polyCount,
								   wColor4s color);

wNode* (*wNodeCreateCylinder)(UInt32 tesselation,
									 Float32 radius,
									 Float32 length,
									 wColor4s color);

wNode* (*wNodeCreateCone)(UInt32 tesselation,
								 Float32 radius,
								 Float32 length,
								 wColor4s clorTop,
								 wColor4s clorBottom);

wNode* (*wNodeCreatePlane)(Float32 size,
								  UInt32 tileCount,
								  wColor4s color);

wNode* (*wNodeCreateFromMesh)(wMesh* mesh);

wNode* (*wNodeCreateFromStaticMesh)(wMesh* mesh);

wNode* (*wNodeCreateFromMeshAsOctree)(wMesh* vptrMesh,
											 Int32 minimalPolysPerNode,
											 bool alsoAddIfMeshPointerZero);

wNode* (*wNodeCreateFromBatchingMesh)(wMesh* batchMesh);

wNode* (*wNodeCreateFromBatchingMeshAsOctree)(wMesh* batchMesh,
													 Int32 minimalPolysPerNode,
													 bool alsoAddIfMeshPointerZero);

void (*wNodeRemoveCollision)(wNode* node,
									wSelector* selector);

void (*wNodeAddCollision)(wNode* node,
								 wSelector* selector);

///wWater///
wNode* (*wWaterSurfaceCreate)(wMesh* mesh,
									 Float32 waveHeight,
									 Float32 waveSpeed,
									 Float32 waveLength,
									 wVector3f position,
									 wVector3f rotation,
									 wVector3f scale);


///wRealWater///
wNode* (*wRealWaterSurfaceCreate)( wTexture* bumpTexture,
										  wVector2f size,
										  wVector2u renderSize);

void (*wRealWaterSetWindForce)(wNode* water,
									  Float32 force);

void (*wRealWaterSetWindDirection)(wNode* water,
										  wVector2f direction);

void (*wRealWaterSetWaveHeight)(wNode* water,
									   Float32 height);

void (*wRealWaterSetColor)(wNode* water,
								  wColor4f color);

void (*wRealWaterSetColorBlendFactor)(wNode* water,
											 Float32 factor);

///wClouds///
wNode* (*wCloudsCreate)( wTexture* texture,
								UInt32 lod,
								UInt32 depth,
								UInt32 density );

///wRealClouds///
wNode* (*wRealCloudsCreate)(wTexture* txture,
								   wVector3f height,
								   wVector2f speed,
								   Float32 textureScale);

void (*wRealCloudsSetTextureTranslation)(wNode* cloud,
												wVector2f speed);

wVector2f (*wRealCloudsGetTextureTranslation)(wNode* cloud);

void (*wRealCloudsSetTextureScale)(wNode* cloud,
										  Float32 scale);

Float32 (*wRealCloudsGetTextureScale)(wNode* cloud);

void (*wRealCloudsSetCloudHeight)(wNode* cloud,
										 wVector3f height);

wVector3f (*wRealCloudsGetCloudHeight)(wNode* cloud);

void (*wRealCloudsSetCloudRadius)(wNode* cloud,
										 wVector2f radius);

wVector2f (*wRealCloudsGetCloudRadius)(wNode* cloud);

void (*wRealCloudsSetColors)(wNode* cloud,
									wColor4s centerColor,
									wColor4s innerColor,
									wColor4s outerColor);

void (*wRealCloudsGetColors)(wNode* cloud,
									wColor4s* centerColor,
									wColor4s* innerColor,
									wColor4s* outerColor);

///wLensFlare///
wNode* (*wLensFlareCreate)(wTexture* texture);

void (*wLensFlareSetStrength)(wNode* flare,
									 Float32 strength);

Float32 (*wLensFlareGetStrength)(wNode* flare);

///wGrass///
wNode* (*wGrassCreate)(wNode* terrain,
							  wVector2i position,
							  UInt32 patchSize,
							  Float32 fadeDistance,
							  bool crossed,
							  Float32 grassScale,
							  UInt32 maxDensity,
							  wVector2u dataPosition,
							  wImage* heightMap,
							  wImage* textureMap,
							  wImage* grassMap,
							  wTexture* grassTexture);

void (*wGrassSetDensity)(wNode* grass,
								UInt32 density,
								Float32 distance );

void (*wGrassSetWind)(wNode* grass,
							 Float32 strength,
							 Float32 res );

UInt32 (*wGrassGetDrawingCount)(wNode* grass );

///wTreeGenerator/////
wNode* (*wTreeGeneratorCreate)(const char* xmlFilePath);

void (*wTreeGeneratorDestroy)(wNode* generator);

///wTree///
wNode* (*wTreeCreate)(wNode* generator,Int32 seed, wTexture* billboardTexture);

void (*wTreeSetDistances)(wNode* tree,Float32 midRange,Float32 farRange);

wNode* (*wTreeGetLeafNode)(wNode* tree);

void (*wTreeSetLeafEnabled)(wNode* tree, bool value);

bool (*wTreeIsLeafEnabled)(wNode* tree);

wMeshBuffer* (*wTreeGetMeshBuffer)(wNode* tree,
										  UInt32 idx);//0-HIGH meshbuffer,  1- MID meshbuffer

void (*wTreeSetBillboardVertexColor)(wNode* tree,wColor4s color);

wColor4s (*wTreeGetBillboardVertexColor)(wNode* tree);

///wWindGenerator
wNode* (*wWindGeneratorCreate)();

void (*wWindGeneratorDestroy)(wNode* windGenerator);

void (*wWindGeneratorSetStrength)(wNode* windGenerator,Float32 strength);

Float32 (*wWindGeneratorGetStrength)(wNode* windGenerator);

void (*wWindGeneratorSetRegularity)(wNode* windGenerator,Float32 regularity);

Float32 (*wWindGeneratorGetRegularity)(wNode* windGenerator);

wVector2f (*wWindGeneratorGetWind)(wNode* windGenerator,wVector3f position,UInt32 timeMs);

///wBolt///
wNode* (*wBoltCreate)();

void (*wBoltSetProperties)(wNode* bolt,
								  wVector3f start,
								  wVector3f end,
								  UInt32 updateTime,
								  UInt32 height,
								  Float32 thickness,
								  UInt32 parts,
								  UInt32 bolts,
								  bool steddyend,
								  wColor4s color);
///wBeam///
wNode* (*wBeamCreate)();

void (*wBeamSetSize)(wNode* beam,
							Float32 size );

void (*wBeamSetPosition)(wNode* beam,
								wVector3f start,
								wVector3f end);

///wParticleSystem///
wNode* (*wParticleSystemCreate)(bool defaultemitter,
									   wVector3f position,
									   wVector3f rotation,
									   wVector3f scale);

wEmitter* (*wParticleSystemGetEmitter)(wNode* ps);

void (*wParticleSystemSetEmitter)(wNode* ps,
										 wEmitter* em);

void (*wParticleSystemRemoveAllAffectors)(wNode* ps);

void (*wParticleSystemSetGlobal)(wNode* ps,
										bool value);

void (*wParticleSystemSetParticleSize)(wNode* ps,
											  wVector2f size);

void (*wParticleSystemClear)(wNode* ps);

///wParticleBoxEmitter///
wEmitter* (*wParticleBoxEmitterCreate)(wNode* ps);

void (*wParticleBoxEmitterSetBox)(wEmitter* em,
										 wVector3f boxMin,
										 wVector3f boxMax);

void (*wParticleBoxEmitterGetBox)(wEmitter* em,
										 wVector3f* boxMin,
										 wVector3f* boxMax);

///wParticleCylinderEmitter///
wEmitter* (*wParticleCylinderEmitterCreate)(wNode* ps,
												   wVector3f center,
												   Float32 radius,
												   wVector3f normal,
												   Float32 lenght);

void (*wParticleCylinderEmitterSetParameters)(wEmitter* em,
													 wParticleCylinderEmitter params);

void (*wParticleCylinderEmitterGetParameters)(wEmitter* em,
													 wParticleCylinderEmitter* params);

///wParticleMeshEmitter///
wEmitter* (*wParticleMeshEmitterCreate)(wNode* ps,
											   wNode* node);

void (*wParticleMeshEmitterSetParameters)(wEmitter* em,
												 wParticleMeshEmitter params);

void (*wParticleMeshEmitterGetParameters)(wEmitter* em,
												 wParticleMeshEmitter* params);

///wParticlePointEmitter///
wEmitter* (*wParticlePointEmitterCreate)(wNode* ps);

///wParticleRingEmitter///
wEmitter* (*wParticleRingEmitterCreate)(wNode* ps,
											   wVector3f center,
											   Float32 radius,
											   Float32 ringThickness);

void (*wParticleRingEmitterSetParameters)(wEmitter* em,
												 wParticleRingEmitter params);

void (*wParticleRingEmitterGetParameters)(wEmitter* em,
												 wParticleRingEmitter* params);

///wParticleSphereEmitter///
wEmitter* (*wParticleSphereEmitterCreate)(wNode* ps,
												 wVector3f center,
												 Float32 radius);

void (*wParticleSphereEmitterSetParameters)(wEmitter* em,
												   wParticleSphereEmitter params);

void (*wParticleSphereEmitterGetParameters)(wEmitter* em,
												   wParticleSphereEmitter* params);

///wParticleEmitter- FOR ALL///
void (*wParticleEmitterSetParameters)(wEmitter* em,
											 wParticleEmitter params);

void (*wParticleEmitterGetParameters)(wEmitter* em,
											 wParticleEmitter* params);

///wParticleAffector -FOR ALL///
void (*wParticleAffectorSetEnable)(wAffector* foa,
										  bool enable );


bool (*wParticleAffectorIsEnable)(wAffector* foa);

///wParticleFadeOutAffector///
wAffector* (*wParticleFadeOutAffectorCreate)(wNode* ps);

void (*wParticleFadeOutAffectorSetTime)(wAffector* paf,
											   UInt32 fadeOutTime);

UInt32 (*wParticleFadeOutAffectorGetTime)(wAffector* paf);

void (*wParticleFadeOutAffectorSetColor)(wAffector* paf,
												wColor4s targetColor);

wColor4s (*wParticleFadeOutAffectorGetColor)(wAffector* paf);

///wParticleGravityAffector///
wAffector* (*wParticleGravityAffectorCreate)(wNode* ps);

void (*wParticleGravityAffectorSetGravity)(wAffector* paf,
												  wVector3f gravity);

wVector3f (*wParticleGravityAffectorGetGravity)(wAffector* paf);

void (*wParticleGravityAffectorSetTimeLost)(wAffector* paf,
												   UInt32 timeForceLost);

UInt32 (*wParticleGravityAffectorGetTimeLost)(wAffector* paf);

///wParticleAttractionAffector///
wAffector* (*wParticleAttractionAffectorCreate)(wNode* ps,
													   wVector3f point,
													   Float32 speed);

void (*wParticleAttractionAffectorSetParameters)(wAffector* paf,
														wParticleAttractionAffector params);

void (*wParticleAttractionAffectorGetParameters)(wAffector* paf,
														wParticleAttractionAffector* params);

///wParticleRotationAffector///
wAffector*  (*wParticleRotationAffectorCreate)(wNode* ps);

void (*wParticleRotationAffectorSetSpeed)(wAffector* paf,
												 wVector3f speed);

wVector3f (*wParticleRotationAffectorGetSpeed)(wAffector* paf);

void (*wParticleRotationAffectorSetPivot)(wAffector* paf,
												 wVector3f pivotPoint);

wVector3f (*wParticleRotationAffectorGetPivot)(wAffector* paf);

///wParticleStopAffector///
wAffector*  (*wParticleStopAffectorCreate)(wNode* ps,
												  wEmitter* em,
												  UInt32 time);

void (*wParticleStopAffectorSetTime)(wAffector* paf,
											UInt32 time);

UInt32 (*wParticleStopAffectorGetTime)(wAffector* paf);

///wParticleColorMorphAffector///
wAffector*  (*wParticleColorMorphAffectorCreate)(wNode* ps);

void (*wParticleColorAffectorSetParameters)(wAffector* paf,
												   wParticleColorMorphAffector params);

void (*wParticleColorAffectorGetParameters)(wAffector* paf,
												   wParticleColorMorphAffector* params);

///wParticlePushAffector///
wAffector*  (*wParticlePushAffectorCreate)(wNode* ps);

void (*wParticlePushAffectorSetParameters)(wAffector* paf,
												  wParticlePushAffector params);

void (*wParticlePushAffectorGetParameters)(wAffector* paf,
												  wParticlePushAffector* params);

///wParticleSplineAffector///
wAffector*  (*wParticleSplineAffectorCreate)(wNode* ps);

void (*wParticleSplineAffectorSetParameters)(wAffector* paf,
													wParticleSplineAffector params);

void (*wParticleSplineAffectorGetParameters)(wAffector* paf,
													wParticleSplineAffector* params);

///wParticleScaleAffector///
wAffector* (*wParticleScaleAffectorCreate)(wNode* ps,
												  wVector2f scaleTo);

///wNode///
void (*wNodeSetDecalsEnabled)(wNode* node);

void (*wNodeSetParent)(wNode* node,
							  wNode *parent );

wNode* (*wNodeGetParent)(wNode* node );

void (*wNodeSetReadOnlyMaterials)(wNode* node,
										 bool readonly);

bool (*wNodeIsReadOnlyMaterials)(wNode* node);

wNode* (*wNodeGetFirstChild)(wNode* node,
									UInt32* iterator);
UInt32 (*wNodeGetChildsCount)(wNode* node,UInt32* iterator);

wNode* (*wNodeGetNextChild)(wNode* node,
								   UInt32* iterator);

bool (*wNodeIsLastChild)(wNode* node,
								UInt32* iterator);

void (*wNodeSetId)(wNode* node,
						  Int32 id);

Int32 (*wNodeGetId)(wNode* node);

void (*wNodeSetName)(wNode* node,
							const char* name );

const char* (*wNodeGetName)(wNode* node);

void (*wNodeSetUserData)(wNode* node,
								void* const newData);

void* (*wNodeGetUserData)(wNode* node);

void (*wNodeSetDebugMode)(wNode* node,
								 wDebugMode visible);

void (*wNodeSetDebugDataVisible)(wNode* node,
										bool value);

UInt32 (*wNodeGetMaterialsCount)(wNode* node );

wMaterial* (*wNodeGetMaterial)(wNode* node,
									  UInt32 matIndex );

void (*wNodeSetPosition)(wNode* node,
								wVector3f position);

wVector3f (*wNodeGetPosition)(wNode* node);

wVector3f (*wNodeGetAbsolutePosition)(wNode* node);

void (*wNodeSetRotation) (wNode* node,
								 wVector3f rotation);

void (*wNodeSetAbsoluteRotation)(wNode* node,
										wVector3f rotation);

wVector3f (*wNodeGetRotation)(wNode* node);

wVector3f (*wNodeGetAbsoluteRotation)(wNode* node);

void (*wNodeTurn)(wNode* Entity,
						 wVector3f turn);

void (*wNodeMove)(wNode* Entity,
						 wVector3f direction);

void (*wNodeRotateToNode)(wNode* Entity1,
								 wNode* Entity2);

Float32 (*wNodesGetBetweenDistance)(wNode* nodeA,
										   wNode* nodeB );

bool (*wNodesAreIntersecting)(wNode* nodeA,
									 wNode* nodeB );

bool (*wNodeIsPointInside)(wNode* node,
								  wVector3f pos);

void (*wNodeDrawBoundingBox)(wNode* node,
									wColor4s color);

void (*wNodeGetBoundingBox)(wNode* Node,
								   wVector3f* min,
								   wVector3f* max);

void (*wNodeGetTransformedBoundingBox)(wNode* Node,
											  wVector3f* min,
											  wVector3f* max);

void (*wNodeSetScale)(wNode* node,
							 wVector3f scale );

wVector3f (*wNodeGetScale)(wNode* node);

wNode* (*wNodeDuplicate)(wNode* entity);

wNode* (*wNodeGetJointByName)(wNode* node,
									 const char *node_name );

wNode* (*wNodeGetJointById)( wNode* node,UInt32 Id);

Int32 (*wNodeGetJointsCount)( wNode* node);

void (*wNodeSetJointSkinningSpace)(wNode* bone,
										  wBoneSkinningSpace space );

wBoneSkinningSpace (*wNodeGetJointSkinningSpace)(wNode* bone);

void (*wNodeSetRenderFromIdentity)(wNode* node, bool value);

void (*wNodeAddShadowVolume)(wNode* node,
									wMesh* mesh,
									bool zfailMethod,
									Float32 infinity,
									bool oldStyle);

wNode* (*wNodeAddShadowVolumeFromMeshBuffer)(wNode* nodeParent,
													wMeshBuffer* meshbuffer,
													bool zfailMethod,
													Float32 infinity,
													bool oldStyle);

void (*wNodeUpdateShadow)(wNode* shadow);

void (*wNodeSetVisibility)(wNode* node,
								  bool visible );

bool (*wNodeIsVisible)(wNode* node);

bool (*wNodeIsInView)(wNode* node);

void (*wNodeDestroy)(wNode* node);

void (*wNodeSetMesh)(wNode* node,
							wMesh* mesh);

wMesh* (*wNodeGetMesh)(wNode* node);

void (*wNodeSetRotationPositionChange)(wNode* node,
											  wVector3f angles,
											  wVector3f offset,
											  wVector3f* forwardStore,
											  wVector3f* upStore,
											  UInt32 numOffsets,
											  wVector3f* offsetStore );

void (*wNodeSetCullingState)(wNode* node,
									wCullingState state);

wSceneNodeType (*wNodeGetType)(wNode* node);

void (*wNodeSetAnimationRange)(wNode* node,
									  wVector2i range);

void (*wNodePlayMD2Animation)(wNode* node,
									 wMd2AnimationType iAnimation);

void (*wNodeSetAnimationSpeed)(wNode* node,
									  Float32 fSpeed);

void (*wNodeSetAnimationFrame)(wNode* node,
									  Float32 fFrame);

Float32 (*wNodeGetAnimationFrame)(wNode* node);

void (*wNodeSetTransitionTime)(wNode* node,
									  Float32 fTime);

void (*wNodeAnimateJoints)(wNode* node);

void (*wNodeSetJointMode)(wNode* node,
								 wJointMode mode);

void (*wNodeSetAnimationLoopMode)(wNode* node,
										 bool value);

void (*wNodeDestroyAllAnimators)(wNode* node);

UInt32 (*wNodeGetAnimatorsCount)(wNode* node);

wAnimator* (*wNodeGetFirstAnimator)(wNode* node);

wAnimator* (*wNodeGetLastAnimator)(wNode* node);

wAnimator* (*wNodeGetAnimatorByIndex)(wNode* node,
											 UInt32 index);

void (*wNodeOnAnimate)(wNode* node,UInt32 timeMs);

void (*wNodeDraw)(wNode* node);

void (*wNodeUpdateAbsolutePosition) (wNode* node);

///wMaterial///
void (*wMaterialSetTexture)(wMaterial* material,
								   UInt32 texIdx,
								   wTexture* texture);

wTexture* (*wMaterialGetTexture)(wMaterial* material,
										UInt32 texIdx);

void (*wMaterialScaleTexture)(wMaterial* material,
									 UInt32 texIdx,
									 wVector2f scale);

void (*wMaterialScaleTextureFromCenter)(wMaterial* material,
											   UInt32 texIdx,
											   wVector2f scale);

void (*wMaterialTranslateTexture)(wMaterial* material,
										 UInt32 texIdx,
										 wVector2f translate);

void (*wMaterialTranslateTextureTransposed)(wMaterial* material,
												   UInt32 texIdx,
												   wVector2f translate);

void (*wMaterialRotateTexture)(wMaterial* material,
									  UInt32 texIdx,
									  Float32 angle);

void (*wMaterialSetTextureWrapUMode)(wMaterial* material,
											UInt32 texIdx,
											wTextureClamp value);

wTextureClamp (*wMaterialGetTextureWrapUMode)(wMaterial* material,
													 UInt32 texIdx);

void (*wMaterialSetTextureWrapVMode)(wMaterial* material,
											UInt32 texIdx,
											wTextureClamp value);

wTextureClamp (*wMaterialGetTextureWrapVMode)(wMaterial* material,
													 UInt32 texIdx);

void (*wMaterialSetTextureLodBias)(wMaterial* material,
										  UInt32 texIdx,
										  UInt32 lodBias);

UInt32 (*wMaterialGetTextureLodBias)(wMaterial* material,
											UInt32 texIdx);

void (*wMaterialSetFlag)(wMaterial* material,
								wMaterialFlags Flag,
								bool boValue);

bool (*wMaterialGetFlag)(wMaterial* material,
								wMaterialFlags matFlag);

void (*wMaterialSetType)(wMaterial* material,
								wMaterialTypes type );

void (*wMaterialSetShininess)(wMaterial* material,
									 Float32 shininess);

Float32 (*wMaterialGetShininess)(wMaterial* material);

void (*wMaterialSetVertexColoringMode)(wMaterial* material,
											  wColorMaterial colorMaterial );

wColorMaterial (*wMaterialGetVertexColoringMode)(wMaterial* material);

void (*wMaterialSetSpecularColor)(wMaterial* material,
										 wColor4s color);

wColor4s (*wMaterialGetSpecularColor)(wMaterial* material);

void (*wMaterialSetDiffuseColor)(wMaterial* material,
										wColor4s color);

wColor4s (*wMaterialGetDiffuseColor)(wMaterial* material);

void (*wMaterialSetAmbientColor)(wMaterial* material,
										wColor4s color);

wColor4s (*wMaterialGetAmbientColor)(wMaterial* material);

void (*wMaterialSetEmissiveColor)(wMaterial* material,
										 wColor4s color);

wColor4s (*wMaterialGetEmissiveColor)(wMaterial* material);

void (*wMaterialSetTypeParameter)(wMaterial* material,
										 Float32 param1);

Float32 (*wMaterialGetTypeParameter)(wMaterial* material);

void (*wMaterialSetTypeParameter2)(wMaterial* material,
										  Float32 param2);

Float32 (*wMaterialGetTypeParameter2)(wMaterial* material);

void (*wMaterialSetBlendingMode)(wMaterial* material,
										const wBlendFactor blendSrc,
										const wBlendFactor blendDest);

//wMaterialGetBlendingMode = wMaterialGetTypeParameter

void (*wMaterialSetLineThickness)(wMaterial* material,
										 Float32 lineThickness );

Float32 (*wMaterialGetLineThickness)(wMaterial* material);

void (*wMaterialSetColorMask)(wMaterial* material,
									 wColorPlane value);

wColorPlane (*wMaterialGetColorMask)(wMaterial* material);

void (*wMaterialSetAntiAliasingMode)(wMaterial* material,
											wAntiAliasingMode mode);

wAntiAliasingMode (*wMaterialGetAntiAliasingMode)(wMaterial* material);


///wShader///
bool (*wShaderCreateNamedVertexConstant)(wShader* shader,
												const char* name,
												Int32 preset,
												const float* floats,
												Int32 count);

bool (*wShaderCreateNamedPixelConstant)(wShader* shader,
											   const char*	name,
											   int	preset,
											   const float* floats,
											   int	count);

bool (*wShaderCreateAddressedVertexConstant)(wShader* shader,
													Int32 address,
													int	preset,
													const float* floats,
													int	count);

bool (*wShaderCreateAddressedPixelConstant)(wShader* shader,
												   int	address,
												   int	preset,
												   const float* floats,
												   int	count);

wShader* (*wShaderAddHighLevelMaterial)(const char* vertexShaderProgram,
											   const char*  vertexShaderEntryPointName,
											   wVertexShaderVersion wVersion,
											   const char* pixelShaderProgram,
											   const char*  pixelShaderEntryPointName,
											   wPixelShaderVersion pVersion,
											   wMaterialTypes materialType,
											   Int32 userData);

wShader* (*wShaderAddHighLevelMaterialFromFiles)(const char* vertexShaderProgramFileName,
														const char*  vertexShaderEntryPointName,
														wVertexShaderVersion wVersion,
														const char * pixelShaderProgramFileName,
														const char*  pixelShaderEntryPointName,
														wPixelShaderVersion pVersion,
														wMaterialTypes materialType,
														Int32 userData);

wShader* (*wShaderAddMaterial)(const char*  vertexShaderProgram,
									  const char*  pixelShaderProgram,
									  wMaterialTypes materialType,
									  Int32 userData);

wShader* (*wShaderAddMaterialFromFiles)(const char*  vertexShaderProgramFileName,
											   const char*  pixelShaderProgramFileName,
											   wMaterialTypes materialType,
											   Int32 userData);


///with geometry shader
wShader* (*wShaderAddHighLevelMaterialEx)(const char* vertexShaderProgram,
												 const char*  vertexShaderEntryPointName,
												 wVertexShaderVersion wVersion,
												 const char* pixelShaderProgram,
												 const char*  pixelShaderEntryPointName,
												 wPixelShaderVersion pVersion,
												 const char* geometryShaderProgram,
												 const char*  geometryShaderEntryPointName,
												 wGeometryShaderVersion gVersion,
												 wPrimitiveType inType,
												 wPrimitiveType outType,
												 UInt32 verticesOut,
												 wMaterialTypes materialType,
												 Int32 userData);

///with geometry shader
wShader* (*wShaderAddHighLevelMaterialFromFilesEx)(const char* vertexShaderProgramFileName,
														  const char*  vertexShaderEntryPointName,
														  wVertexShaderVersion wVersion,
														  const char* pixelShaderProgramFileName,
														  const char*  pixelShaderEntryPointName,
														  wPixelShaderVersion pVersion,
														  const char* geometryShaderProgram,
														  const char*  geometryShaderEntryPointName,
														  wGeometryShaderVersion gVersion,
														  wPrimitiveType inType,
														  wPrimitiveType outType,
														  UInt32 verticesOut,
														  wMaterialTypes materialType,
														  Int32 userData);

///wMesh///
wMesh* (*wMeshLoad)(const char* cptrFile, bool ToTangents);

wMesh* (*wMeshCreate)(const char *cptrMeshName);

void (*wMeshAddMeshBuffer)(wMesh* mesh,
								  wMeshBuffer* meshbuffer);

wMesh* (*wMeshCreateSphere)(const char* name,
								   Float32 radius,
								   Int32 polyCount );

wMesh* (*wMeshCreateCube)();

Int32 (*wMeshSave)(wMesh* mesh,
						  wMeshFileFormat type,
						  const char* filename);//return 0/1/2/3  3- успешно

void (*wMeshDestroy)(wMesh* mesh);

bool (*wMeshSetName)(wMesh* mesh,
							const char* name);

const char* (*wMeshGetName)(wMesh* mesh);

wAnimatedMeshType (*wMeshGetType)(wMesh* mesh);

void (*wMeshFlipSurface)(wMesh* mesh);

void (*wMeshMakePlanarTextureMapping)(wMesh* mesh,
											 Float32 resolution);

void (*wMeshMakePlanarTextureMappingAdvanced)(wMesh* mesh,
													 Float32 resolutionH,
													 Float32 resolutionV,
													 UInt8 axis,
													 wVector3f offset);

wMesh* (*wMeshCreateStaticWithTangents)(wMesh* aMesh);

void (*wMeshRecalculateNormals)(wMesh* mesh,
									   bool smooth,
									   bool angleWeighted);

void (*wMeshRecalculateTangents)(wMesh* mesh,
										bool recalculateNormals,
										bool smooth,
										bool angleWeighted);

wMesh* (*wMeshCreateHillPlane)(const char* meshname,
									  wVector2f tilesSize,
									  wVector2i tilesCount,
									  wMaterial* material,
									  Float32 hillHeight,
									  wVector2f countHills,
									  wVector2f texRepeatCount);

wMesh* (*wMeshCreateArrow)(const char* name,
								  wColor4s cylinderColor,
								  wColor4s coneColor,
								  UInt32 tesselationCylinder,
								  UInt32 tesselationCone,
								  Float32 height,
								  Float32 heightCylinder,
								  Float32 widthCylinder,
								  Float32 widthCone);

wMesh* (*wMeshCreateBatching)();

void (*wMeshAddToBatching)(wMesh* meshBatch,
								  wMesh* mesh,
								  wVector3f position,
								  wVector3f rotation,
								  wVector3f scale);

void (*wMeshUpdateBatching)(wMesh* meshBatch);

void (*wMeshFinalizeBatching)(wMesh* meshBatch);

void (*wMeshClearBatching)(wMesh* meshBatch);

void (*wMeshDestroyBatching)(wMesh* meshBatch);

void (*wMeshEnableHardwareAcceleration)(wMesh* mesh,
											   UInt32 iFrame);

UInt32 (*wMeshGetFramesCount)(wMesh* mesh );

UInt32 (*wMeshGetIndicesCount)(wMesh* mesh,
									  UInt32 iFrame,
									  UInt32 iMeshBuffer);

UInt16* (*wMeshGetIndices)(wMesh* mesh,
								  UInt32 iFrame,
								  UInt32 iMeshBuffer);

void (*wMeshSetIndices)(wMesh* mesh,
							   UInt32 iFrame,
							   UInt16* indicies,
							   UInt32 iMeshBuffer);

UInt32 (*wMeshGetVerticesCount)(wMesh* mesh,
									   UInt32 iFrame,
									   UInt32 iMeshBuffer);

void (*wMeshGetVertices)(wMesh* mesh,
								UInt32 iFrame,
								wVert* verts,
								UInt32 iMeshBuffer);

UInt32* (*wMeshGetVerticesMemory)(wMesh* mesh,
										 UInt32 iFrame,
										 UInt32 iMeshBuffer);

void (*wMeshSetVertices)(wMesh* mesh,
								UInt32 iFrame,
								wVert* verts,
								UInt32 iMeshBuffer);

void (*wMeshSetScale)( wMesh* mesh,
							  Float32 scale,
							  UInt32 iFrame,
							  UInt32 iMeshBuffer,
							  wMesh* sourceMesh);

void (*wMeshSetRotation)(wMesh* mesh,
								wVector3f rot);

void (*wMeshSetVerticesColors)(wMesh* mesh,
									  UInt32 iFrame,
									  wColor4s* verticesColor,
									  UInt32 groupCount,
									  UInt32* startPos,
									  UInt32* endPos,
									  UInt32 iMeshBuffer);


void (*wMeshSetVerticesAlpha)(wMesh* mesh,
									 UInt32 iFrame,
									 UInt8 value);

void (*wMeshSetVerticesCoords)(wMesh* mesh,
									  UInt32 iFrame,
									  wVector3f* vertexCoord,
									  UInt32 groupCount,
									  UInt32* startPos,
									  UInt32* endPos,
									  UInt32 iMeshBuffer);

void (*wMeshSetVerticesSingleColor)(wMesh* mesh,
										   UInt32 iFrame,
										   wColor4s verticesColor,
										   UInt32 groupCount,
										   UInt32* startPos,
										   UInt32* endPos,
										   UInt32 iMeshBuffer);

void (*wMeshGetBoundingBox) (wMesh* mesh,
									wVector3f* min,
									wVector3f* max);

wMesh* (*wMeshDuplicate)(wMesh* src);

void (*wMeshFit)(wMesh* src,
						wVector3f pivot,
						wVector3f* delta);

bool (*wMeshIsEmpty)(wMesh* mesh);

UInt32 (*wMeshGetBuffersCount)(wMesh* mesh,
									  UInt32 iFrame);

wMeshBuffer* (*wMeshGetBuffer)(wMesh* mesh,
									  UInt32 iFrame,
									  UInt32 index);

///wMeshBuffer///
wMeshBuffer* (*wMeshBufferCreate)(
		UInt32 iVertexCount,
		wVert* vVertices,
		UInt32 iIndicesCount,
		UInt16* usIndices);

/*wMeshBuffer* wMeshBufferCreateFromMeshJoint(wMesh* mesh,
                                            Int32 jointIndex);*/

void (*wMeshBufferDestroy)(wMeshBuffer* buf);

void (*wMeshBufferAddToBatching)(wMesh* meshBatch,
										wMeshBuffer* buffer,
										wVector3f position,
										wVector3f rotation,
										wVector3f scale);

wMaterial* (*wMeshBufferGetMaterial)(wMeshBuffer* buf);

///wBsp///
///Get BSP Entity List///
UInt32* (*wBspGetEntityList)(wMesh* const mesh);

///Get BSP Entity List size///
Int32 (*wBspGetEntityListSize)(UInt32* entityList);

///Get First (vec.x) and Last (vec.y) BSP Entity Index///
wVector2i (*wBspGetEntityIndexByName)(void* entityList,
											 const char* EntityName);
///Name BSP Entity From Index
const char* (*wBspGetEntityNameByIndex)(UInt32* entityList,
											   UInt32 number);
///Mesh from BSP Brush///
wMesh* (*wBspGetEntityMeshFromBrush)(wMesh* bspMesh,
											UInt32* entityList,
											Int32 index);
///BSP VarGroup///
UInt32* (*wBspGetVarGroupByIndex)(UInt32* entityList,
										 Int32 index);

UInt32 (*wBspGetVarGroupSize)(UInt32* entityList,
									 Int32 index);

wVector3f (*wBspGetVarGroupValueAsVec)(UInt32* group,
											  const char* strName,
											  UInt32 parsePos);

Float32 (*wBspGetVarGroupValueAsFloat)(UInt32* group,
											  const  char* strName,
											  UInt32 parsePos);

const char* (*wBspGetVarGroupValueAsString)(UInt32* group,
												   const char* strName);
/*
UInt32 wBspGetVarGroupVariableSize(UInt32* group);


UInt32* wBspGetVariableFromVarGroup(UInt32* group,
                                          Int32 index);

const char* wBspGetVariableName(UInt32* variable);

const char* wBspGetVariableContent(UInt32* variable);

wVector3f wBspGetVariableValueAsVec(UInt32* variable,
                                    UInt32 parsePos);

Float32 wBspGetVariableValueAsFloat(UInt32* variable,
                                  UInt32 parsePos);
*/

wNode* (*wBspCreateFromMesh) (wMesh* const mesh,
									 bool isTangent,
									 bool isOctree,
									 const char* fileEntity,
									 bool isLoadShaders,
									 UInt32 PolysPerNode);

///Occlusion Query
void (*wOcclusionQueryAddNode)(wNode* node);

void (*wOcclusionQueryAddMesh)(wNode* node,wMesh* mesh);

void (*wOcclusionQueryUpdate)(wNode* node,bool block);

void (*wOcclusionQueryRun)(wNode* node,bool visible);

void (*wOcclusionQueryUpdateAll)(bool block);

void (*wOcclusionQueryRunAll)(bool visible);

void (*wOcclusionQueryRemoveNode)(wNode* node);

void (*wOcclusionQueryRemoveAll)();

UInt32 (*wOcclusionQueryGetResult)(wNode* node);

///wSphericalTerrain///
wNode* (*wSphericalTerrainCreate)( const char *cptrFile0,
										  const char *cptrFile1,
										  const char *cptrFile2,
										  const char *cptrFile3,
										  const char *cptrFile4,
										  const char *cptrFile5,
										  wVector3f position,
										  wVector3f rotation,
										  wVector3f scale,
										  wColor4s color,
										  Int32 smootFactor,
										  bool spherical,
										  Int32 maxLOD,
										  wTerrainPatchSize patchSize);

void (*wSphericalTerrainSetTextures)(wNode* terrain,
											wTexture* textureTop,
											wTexture* textureFront,
											wTexture* textureBack,
											wTexture* textureLeft,
											wTexture* textureRight,
											wTexture* textureBottom,
											UInt32 materialIndex);

void (*wSphericalTerrainLoadVertexColor)(wNode* terrain,
												wImage* imageTop,
												wImage* imageFront,
												wImage* imageBack,
												wImage* imageLeft,
												wImage* imageRight,
												wImage* imageBottom );

wVector3f (*wSphericalTerrainGetSurfacePosition)(wNode* terrain,
														Int32 face,
														wVector2f logicalPos);

wVector3f (*wSphericalTerrainGetSurfaceAngle)(wNode* terrain,
													 Int32 face,
													 wVector2f logicalPos);

wVector2f (*wSphericalTerrainGetSurfaceLogicalPosition)(wNode* terrain,
															   wVector3f position,
															   int* face);

///wTerrain///
wNode* (*wTerrainCreate)(const char* cptrFile,
								wVector3f position,
								wVector3f rotation,
								wVector3f scale,
								wColor4s color,
								Int32 smoothing,
								Int32 maxLOD,
								wTerrainPatchSize patchSize);


void (*wTerrainScaleDetailTexture)(wNode* terrain,
										  wVector2f scale);

Float32 (*wTerrainGetHeight)(wNode* terrain,
									wVector2f positionXZ);

///wTiledTerrain///
wNode* (*wTiledTerrainCreate)(wImage* image,
									 Int32 tileSize,
									 wVector2i dataSize,
									 wVector3f position,
									 wVector3f rotation,
									 wVector3f scale,
									 wColor4s color,
									 Int32 smoothing,
									 Int32 maxLOD,
									 wTerrainPatchSize patchSize );

void (*wTiledTerrainAddTile)(wNode* terrain,
									wNode* neighbour,
									wTiledTerrainEdge edge);

void (*wTiledTerrainSetTileStructure)(wNode* terrain,
											 wImage* image,
											 wVector2i data);

void (*wTiledTerrainSetTileColor)(wNode* terrain,
										 wImage* image,
										 wVector2i data);

///wSoundBuffer
wSoundBuffer* (*wSoundBufferLoad)(const char* filePath);

wSoundBuffer* (*wSoundBufferLoadFromMemory)(const char* data,
												   Int32 length,
												   const char* extension);

void (*wSoundBufferDestroy)(wSoundBuffer* buf);

///wSound///
wSound* (*wSoundLoad)(const char* filePath,
							 bool stream);

wSound* (*wSoundLoadFromMemory)(const char* name,
									   const char* data,
									   Int32 length,
									   const char* extension);

wSound* (*wSoundLoadFromRaw)(const char* name, const char* data,
									Int32 length,
									UInt32 frequency,
									wAudioFormats format);

wSound* (*wSoundCreateFromBuffer)(wSoundBuffer* buf);

bool (*wSoundIsPlaying)(wSound* sound);

bool (*wSoundIsPaused)(wSound* sound);

bool (*wSoundIsStopped)(wSound* sound);

void (*wSoundSetVelocity)(wSound* sound,
								 wVector3f velocity);

wVector3f (*wSoundGetVelocity)(wSound* sound);

void (*wSoundSetDirection)(wSound* sound,
								  wVector3f direction);

wVector3f (*wSoundGetDirection)(wSound* sound);

void (*wSoundSetVolume)(wSound* sound,
							   Float32 value);

Float32 (*wSoundGetVolume)(wSound* sound);

void (*wSoundSetMaxVolume)(wSound* sound,
								  Float32 value);

Float32 (*wSoundGetMaxVolume)(wSound* sound);

void (*wSoundSetMinVolume)(wSound* sound,
								  Float32 value);

Float32 (*wSoundGetMinVolume)(wSound* sound);

void (*wSoundSetPitch)(wSound* sound,
							  Float32 value);

Float32 (*wSoundGetPitch)(wSound* sound);

void (*wSoundSetRollOffFactor)(wSound* sound,
									  Float32 value);

Float32 (*wSoundGetRollOffFactor)(wSound* sound);

void (*wSoundSetStrength)(wSound* sound,
								 Float32 value);

Float32 (*wSoundGetStrength)(wSound* sound);

void (*wSoundSetMinDistance)(wSound* sound,
									Float32 value);

Float32 (*wSoundGetMinDistance)(wSound* sound);

void (*wSoundSetMaxDistance)(wSound* sound,
									Float32 Value);

Float32 (*wSoundGetMaxDistance)(wSound* sound);

void (*wSoundSetInnerConeAngle)(wSound* sound,
									   Float32 Value);

Float32 (*wSoundGetInnerConeAngle)(wSound* sound);

void (*wSoundSetOuterConeAngle)(wSound* sound,
									   Float32 Value);

Float32 (*wSoundGetOuterConeAngle)(wSound* sound);

void (*wSoundSetOuterConeVolume)(wSound* sound,
										Float32 Value);

Float32 (*wSoundGetOuterConeVolume)(wSound* sound);

void (*wSoundSetDopplerStrength)(wSound* sound,
										Float32 Value);

Float32 (*wSoundGetDopplerStrength)(wSound* sound);

void (*wSoundSetDopplerVelocity)(wSound* sound,
										wVector3f velocity);

wVector3f (*wSoundGetDopplerVelocity)(wSound* sound);

Float32 (*wSoundCalculateGain)(wSound* sound);

void (*wSoundSetRelative)(wSound* sound,
								 bool value);

bool (*wSoundIsRelative)(wSound* sound);

bool (*wSoundPlay)(wSound* sound,
						  bool loop);

void (*wSoundStop)(wSound* sound);

void (*wSoundPause)(wSound* sound);

void (*wSoundSetLoopMode)(wSound* sound,
								 bool value);

bool (*wSoundIsLooping)(wSound* sound);

bool (*wSoundIsValid)(wSound* sound);

bool (*wSoundSeek)(wSound* sound,
						  Float32 seconds,
						  bool relative);

void (*wSoundUpdate)(wSound* sound);

Float32 (*wSoundGetTotalAudioTime)(wSound* sound);

Int32 (*wSoundGetTotalAudioSize)(wSound* sound);

Int32 (*wSoundGetCompressedAudioSize)(wSound* sound);

Float32 (*wSoundGetCurrentAudioTime)(wSound* sound);

Int32 (*wSoundGetCurrentAudioPosition)(wSound* sound);

Int32 (*wSoundGetCurrentCompressedAudioPosition)(wSound* sound);

UInt32 (*wSoundGetNumEffectSlotsAvailable)(wSound* sound);

///sound effects///
bool (*wSoundAddEffect)(wSound* sound,
							   UInt32 slot,
							   wSoundEffect* effect);

void (*wSoundRemoveEffect)(wSound* sound,
								  UInt32 slot);

wSoundEffect* (*wSoundCreateEffect)();

bool (*wSoundIsEffectValid)(wSoundEffect* effect);

bool (*wSoundIsEffectSupported)(wSoundEffectType type);

UInt32 (*wSoundGetMaxEffectsSupported)();

void (*wSoundSetEffectType)(wSoundEffect* effect,
								   wSoundEffectType type);

wSoundEffectType (*wSoundGetEffectType)(wSoundEffect* effect);

void (*wSoundSetEffectAutowahParameters)(wSoundEffect* effect,
												wAutowahParameters param);

void (*wSoundSetEffectChorusParameters)(wSoundEffect* effect,
											   wChorusParameters param);

void (*wSoundSetEffectCompressorParameters)(wSoundEffect* effect,
												   wCompressorParameters param);

 void (*wSoundSetEffectDistortionParameters)(wSoundEffect* effect,
													wDistortionParameters param);

void (*wSoundSetEffectEaxReverbParameters)(wSoundEffect* effect,
												  wEaxReverbParameters param);

void (*wSoundSetEffectEchoParameters)(wSoundEffect* effect,
											 wEchoParameters param);

void (*wSoundSetEffectEqualizerParameters)(wSoundEffect* effect,
												  wEqualizerParameters param);

void (*wSoundSetEffectFlangerParameters)(wSoundEffect* effect,
												wFlangerParameters param);

void (*wSoundSetEffectFrequencyShiftParameters)(wSoundEffect* effect,
													   wFrequencyShiftParameters param);

void (*wSoundSetEffectPitchShifterParameters)(wSoundEffect* effect,
													 wPitchShifterParameters param);

void (*wSoundSetEffectReverbParameters)(wSoundEffect* effect,
											   wReverbParameters param);

void (*wSoundSetEffectRingModulatorParameters)(wSoundEffect* effect,
													  wRingModulatorParameters param);

void (*wSoundSetEffectVocalMorpherParameters)(wSoundEffect* effect,
													 wVocalMorpherParameters param);

///Sound filters///
wSoundFilter* (*wSoundCreateFilter)();

bool (*wSoundIsFilterValid)(wSoundFilter* filter);

bool (*wSoundAddFilter)(wSound* sound,
							   wSoundFilter* filter);

void (*wSoundRemoveFilter)(wSound* sound);

bool (*wSoundIsFilterSupported)(wSoundFilterType type);

void (*wSoundSetFilterType)(wSoundFilter* filter,
								   wSoundFilterType type);

wSoundFilterType (*wSoundGetFilterType)(wSoundFilter* filter);

void (*wSoundSetFilterVolume)(wSoundFilter* filter,
									 Float32 volume);

Float32 (*wSoundGetFilterVolume)(wSoundFilter* filter);

void (*wSoundSetFilterHighFrequencyVolume)(wSoundFilter* filter,
												  Float32 volumeHF);

Float32 (*wSoundGetFilterHighFrequencyVolume)(wSoundFilter* filter);

void (*wSoundSetFilterLowFrequencyVolume)(wSoundFilter* filter,
												 Float32 volumeLF);

Float32 (*wSoundGetFilterLowFrequencyVolume)(wSoundFilter* filter);

///wVideo///
wVideo* (*wVideoLoad)(const char* fileName);

void (*wVideoPlay)(wVideo* player);

bool (*wVideoIsPlaying)(wVideo* player);

void (*wVideoRewind)(wVideo* player);

void (*wVideoSetLoopMode)(wVideo* player,
								 bool looping);

bool (*wVideoIsLooping)(wVideo* player);

wGuiObject* (*wVideoCreateTargetImage)(wVideo* player,
											  wVector2i position);

wTexture* (*wVideoGetTargetTexture)(wVideo* player);

wSound* (*wVideoGetSoundNode)(wVideo* player);

void (*wVideoUpdate)(wVideo* player,
							UInt32 timeMs);

void (*wVideoPause)(wVideo* player);

bool (*wVideoIsPaused)(wVideo* player);

bool (*wVideoIsAtEnd)(wVideo* player);

bool (*wVideoIsEmpty)(wVideo* player);

Int64 (*wVideoGetFramePosition)(wVideo* player);

UInt32 (*wVideoGetTimePosition)(wVideo* player);

wVector2i (*wVideoGetFrameSize)(wVideo* player);

Int32 (*wVideoGetQuality)(wVideo* player);

void (*wVideoDestroy)(wVideo* player);

///wDecal///
wNode* (*wDecalCreateFromRay)(wTexture* texture,
									 wVector3f startRay,
									 wVector3f endRay,
									 Float32 dimension,
									 Float32 textureRotation,
									 Float32 lifeTime,
									 Float32 visibleDistance);

wNode* (*wDecalCreateFromPoint)(wTexture* texture,
									   wVector3f position,
									   wVector3f normal,
									   Float32 dimension,
									   Float32 textureRotation,
									   Float32 lifeTime,
									   Float32 visibleDistance);

Float32 (*wDecalGetLifeTime)(wNode* node);

void  (*wDecalSetLifeTime)(wNode* node,
								  Float32 lifeTime);

Float32 (*wDecalGetMaxVisibleDistance)(wNode* node);

void  (*wDecalSetMaxVisibleDistance)(wNode* node,
											Float32 distance);

void  (*wDecalSetFadeOutParams)(wNode* node,
									   const bool isfadeOut,
									   Float32 time);

wMaterial* (*wDecalGetMaterial)(wNode* decal);

void (*wDecalsClear)();///destroy all + disable new

void (*wDecalsDestroyAll)();

void (*wDecalsCombineAll)();

Int32 (*wDecalsGetCount)();

///wNetPacket///
wPacket* (*wNetPacketCreate)(UInt64 id,
									bool inOrder,
									bool reliable,
									UInt64 priority);

void (*wNetPacketWriteUInt)(wPacket* msg,
								   UInt32 value);

void (*wNetPacketWriteInt)(wPacket* msg,
								  Int32 value);

void (*wNetPacketWriteFloat)(wPacket* msg,
									Float32 value);

void (*wNetPacketWriteString)(wPacket* msg,
									 const char* newString);

UInt32 (*wNetPacketReadUint)(Int32 numPacket);

Int32 (*wNetPacketReadInt)(Int32 numPacket);

Float32 (*wNetPacketReadFloat)(Int32 numPacket);

const char* (*wNetPacketReadString)(Int32 numPacket);

const char* (*wNetPacketReadMessage)(Int32 numPacket);

UInt64 (*wNetPacketGetId)(Int32 numPacket);

const char* (*wNetPacketGetClientIp)(Int32 numPacket);

void* (*wNetPacketGetClientPtr)(Int32 numPacket);

UInt16 (*wNetPacketGetClientPort)(Int32 numPacket);

///wNetManager///
 void (*wNetManagerSetVerbose)(bool value);

void (*wNetManagerSetMessageId)(UInt64 newId);

UInt64 (*wNetManagerGetMessageId)();

void (*wNetManagerDestroyAllPackets)();

Int32 (*wNetManagerGetPacketsCount)();

///wNetServer///
bool (*wNetServerCreate)(UInt16 port,
								Int32 mode,
								Int32 maxClientsCount);

void (*wNetServerUpdate)(Int32 sleepMs,
								Int32 countIteration,
								Int32 maxMSecsToWait);

void (*wNetServerClear)();

void (*wNetServerSendPacket)(void* destPtr,
									wPacket* msg);

void (*wNetServerBroadcastMessage)(const char* text);

void (*wNetServerAcceptNewConnections)(bool value);

void (*wNetServerStop)(Int32 msTime);

Int32 (*wNetServerGetClientsCount)();

void (*wNetServerKickClient)(void* clientPtr);

void (*wNetServerUnKickClient)(void* clientPtr);

void (*wNetServerClearBannedList)();

///wNetClient///
bool (*wNetClientCreate)(const char* address,
								UInt16 port,
								Int32 mode,
								Int32 maxMSecsToWait);

void (*wNetClientUpdate)(Int32 maxMessagesToProcess,
								Int32 countIteration,
								Int32 maxMSecsToWait);

void (*wNetClientDisconnect)(Int32 maxMSecsToWait);

void (*wNetClientStop)(Int32 maxMSecsToWait);

bool (*wNetClientIsConnected)();

void (*wNetClientSendMessage)(const char* text);

void (*wNetClientSendPacket)(wPacket* msg);

///wPhys///
bool (*wPhysStart)();

void (*wPhysUpdate)(Float32 timeStep);

void (*wPhysStop)();

void (*wPhysSetGravity)(wVector3f gravity);

void (*wPhysSetWorldSize)(wVector3f size);

void (*wPhysSetSolverModel)(wPhysSolverModel model);

void (*wPhysSetFrictionModel)(wPhysFrictionModel model);

void (*wPhysDestroyAllBodies)();

void (*wPhysDestroyAllJoints)();

Int32 (*wPhysGetBodiesCount)();

Int32 (*wPhysGetJointsCount)();

wNode* (*wPhysGetBodyPicked)(wVector2i position,
									bool mouseLeftKey);

wNode* (*wPhysGetBodyFromRay)(wVector3f start,
									 wVector3f end);

wNode* (*wPhysGetBodyFromScreenCoords)(wVector2i position);

wNode* (*wPhysGetBodyByName)(const char* name);

wNode* (*wPhysGetBodyById)(Int32 Id);

wNode* (*wPhysGetBodyByIndex)(Int32 idx);

wNode* (*wPhysGetJointByName)(const char* name);

wNode* (*wPhysGetJointById)(Int32 Id);

wNode* (*wPhysGetJointByIndex)(Int32 idx);

///wPhysBody///
wNode* (*wPhysBodyCreateNull)();

wNode* (*wPhysBodyCreateCube)(wVector3f size,
									 Float32 Mass);

wNode* (*wPhysBodyCreateSphere)(wVector3f radius,
									   Float32 Mass);

wNode* (*wPhysBodyCreateCone)(Float32 radius,
									 Float32 height,
									 Float32 mass,
									 bool Offset);

wNode* (*wPhysBodyCreateCylinder)(Float32 radius,
										 Float32 height,
										 Float32 mass,
										 bool Offset);

wNode* (*wPhysBodyCreateCapsule)(Float32 radius,
										Float32 height,
										Float32 mass,
										bool Offset);

wNode* (*wPhysBodyCreateHull)(wNode* mesh,
									 Float32 mass);

wNode* (*wPhysBodyCreateTree)(wNode* mesh);

wNode* (*wPhysBodyCreateTreeBsp)(wMesh* mesh,
										wNode* node);

wNode* (*wPhysBodyCreateTerrain)(wNode* mesh,
										Int32 LOD);

wNode* (*wPhysBodyCreateHeightField)(wNode* mesh);

wNode* (*wPhysBodyCreateWaterSurface)(wVector3f size,
											 Float32 FluidDensity,
											 Float32 LinearViscosity,
											 Float32 AngulaViscosity);

wNode* (*wPhysBodyCreateCompound)(wNode** nodes,
										 Int32 CountNodes,
										 Float32 mass);

///=> такой функции НЕ ТРЕБУЕТСЯ, так как
///так как здесь работает wNodeDestroy()
///void wPhysBodyDestroy(void* body);

void (*wPhysBodySetName)(wNode* body,
								const char* name);

const char* (*wPhysBodyGetName)(wNode* body);

void (*wPhysBodySetFreeze)(wNode* body,
								  bool freeze);

bool (*wPhysBodyIsFreeze)(wNode* body);

void (*wPhysBodySetMaterial)(wNode* body,
									Int32 MatId);

Int32 (*wPhysBodyGetMaterial)(wNode* body);

void (*wPhysBodySetGravity)(wNode* body,
								   wVector3f gravity);

wVector3f (*wPhysBodyGetGravity)(wNode* body);

void (*wPhysBodySetMass)(wNode* body,
								Float32 NewMass);

Float32 (*wPhysBodyGetMass)(wNode* body);

void (*wPhysBodySetCenterOfMass)(wNode* body,
										wVector3f center);

wVector3f (*wPhysBodyGetCenterOfMass)(wNode* body);

void (*wPhysBodySetMomentOfInertia)(wNode* body,
										   wVector3f value);

wVector3f (*wPhysBodyGetMomentOfInertia)(wNode* body);

void (*wPhysBodySetAutoSleep)(wNode* body,
									 bool value);

bool (*wPhysBodyIsAutoSleep)(wNode* body);

void (*wPhysBodySetLinearVelocity)(wNode* body,
										  wVector3f velocity);

wVector3f (*wPhysBodyGetLinearVelocity)(wNode* body);

void (*wPhysBodySetAngularVelocity)(wNode* body,
										   wVector3f velocity);

wVector3f (*wPhysBodyGetAngularVelocity)(wNode* body);

void (*wPhysBodySetLinearDamping)(wNode* body,
										 Float32 linearDamp);

Float32 (*wPhysBodyGetLinearDamping)(wNode* body);

void (*wPhysBodySetAngularDamping)(wNode* body,
										  wVector3f damping);

wVector3f (*wPhysBodyGetAngularDamping)(wNode* body);

void (*wPhysBodyAddImpulse)(wNode* body,
								   wVector3f velosity,
								   wVector3f position);

void (*wPhysBodyAddForce)(wNode* body,
								 wVector3f force);

void (*wPhysBodyAddTorque)(wNode* body,
								  wVector3f torque);

bool (*wPhysBodiesIsCollide)(wNode* body1,
									wNode* body2);

wVector3f (*wPhysBodiesGetCollisionPoint)(wNode* body1,
												 wNode* body2);

wVector3f (*wPhysBodiesGetCollisionNormal)(wNode* body1,
												  wNode* body2);

void (*wPhysBodyDraw)(wNode* body);

///wPhysJoint///
wNode* (*wPhysJointCreateBall)(wVector3f position,
									  wVector3f pinDir,
									  wNode* body1,
									  wNode* body2);

wNode* (*wPhysJointCreateHinge)(wVector3f position,
									   wVector3f pinDir,
									   wNode* body1,
									   wNode* body2);

wNode* (*wPhysJointCreateSlider)(wVector3f position,
										wVector3f pinDir,
										wNode* body1,
										wNode* body2);

wNode* (*wPhysJointCreateCorkScrew)(wVector3f position,
										   wVector3f pinDir,
										   wNode* body1,
										   wNode* body2);

wNode* (*wPhysJointCreateUpVector)(wVector3f position,
										  wNode* body);

void (*wPhysJointSetName)(wNode* joint,
								 const char* name);

const char* (*wPhysJointGetName)(wNode* joint);

void (*wPhysJointSetCollisionState)(wNode* joint,
										   bool isCollision);

bool (*wPhysJointIsCollision)(wNode* Joint);

void (*wPhysJointSetBallLimits)(wNode* joint,
									   Float32 MaxConeAngle,
									   wVector2f twistAngles);

void (*wPhysJointSetHingeLimits)(wNode* joint,
										wVector2f anglesLimits);

void (*wPhysJointSetSliderLimits)(wNode* joint,
										 wVector2f anglesLimits);

void (*wPhysJointSetCorkScrewLinearLimits)(wNode* joint,
												  wVector2f distLimits);

void (*wPhysJointSetCorkScrewAngularLimits)(wNode* joint,
												   wVector2f distLimits);

///wPhysPlayerController///
wNode* (*wPhysPlayerControllerCreate)(wVector3f position,
											 wNode* body,
											 Float32 maxStairStepFactor,
											 Float32 cushion);

void (*wPhysPlayerControllerSetVelocity)(wNode* joint,
												Float32 forwardSpeed,
												Float32 sideSpeed,
												Float32 heading);

///wPhysVehicle
wNode* (*wPhysVehicleCreate)(Int32 tiresCount,
									wPhysVehicleType rayCastType,
									wNode* CarBody);

Int32 (*wPhysVehicleAddTire)(wNode* Car,
									wNode* UserData,
									wPhysVehicleTireType tireType,
									wVector3f position,
									Float32 Mass,
									Float32 Radius,
									Float32 Width,
									Float32 SLenght,
									Float32 SConst,
									Float32 SDamper);

Float32 (*wPhysVehicleGetSpeed)(wNode* Car);

Int32 (*wPhysVehicleGetTiresCount)(wNode* Car);

void (*wPhysVehicleSetBrake)(wNode* Car,
									bool value);

bool (*wPhysVehicleIsBrake)(wNode* Car);

bool (*wPhysVehicleIsAllTiresCollided)(wNode* Car);

void (*wPhysVehicleSetSteering)(wNode* Car,
									   Float32 angle);///-1.......+1

Float32 (*wPhysVehicleGetSteering)(wNode* Car);/// return value: -1......+1

void (*wPhysVehicleSetTireMaxSteerAngle)(wNode* Car,
												Int32 tireIndex,
												Float32 angleDeg);

Float32 (*wPhysVehicleGetTireMaxSteerAngle)(wNode* Car,
												   Int32 tireIndex);

wNode* (*wPhysVehicleGetBody)(wNode* Car);

void (*wPhysVehicleSetMotorValue)(wNode* Car,
										 Float32 value);

Float32 (*wPhysVehicleGetMotorValue)(wNode* Car);

///Vehicle Tires
wVector3f (*wPhysVehicleGetTireLocalPosition)(wNode* Car,
													 Int32 tireIndex);

Float32 (*wPhysVehicleGetTireUpDownPosition)(wNode* Car,
													Int32 tireIndex);

/*bool wPhysVehicleIsTireOnAir(wNode* Car,
                             Int32 tireIndex);*/

Float32 (*wPhysVehicleGetTireAngularVelocity)(wNode* Car,
													 Int32 tireIndex);

Float32 (*wPhysVehicleGetTireSpeed)(wNode* Car,
										   Int32 tireIndex);

wVector3f (*wPhysVehicleGetTireContactPoint)(wNode* Car,
													Int32 tireIndex);

wVector3f (*wPhysVehicleGetTireContactNormal)(wNode* Car,
													 Int32 tireIndex);

bool (*wPhysVehicleIsTireBrake)(wNode* Car,
									   Int32 tireIndex);

wPhysVehicleTireType (*wPhysVehicleGetTireType)(wNode* Car,
													   Int32 tireIndex);

void (*wPhysVehicleSetTireType)(wNode* Car,
									   Int32 tireIndex,
									   wPhysVehicleTireType tireType);

void (*wPhysVehicleSetTireBrakeForce)(wNode* Car,
											 Int32 tireIndex,
											 Float32 value);

Float32 (*wPhysVehicleGetTireBrakeForce)(wNode* Car,
												Int32 tireIndex);

void (*wPhysVehicleSetTireBrakeLateralFriction)(wNode* Car,
													   Int32 tireIndex,
													   Float32 value);

Float32 (*wPhysVehicleGetTireBrakeLateralFriction)(wNode* Car,
														  Int32 tireIndex);

void (*wPhysVehicleSetTireBrakeLongitudinalFriction)(wNode* Car,
															Int32 tireIndex,
															Float32 value);

Float32 (*wPhysVehicleGetTireBrakeLongitudinalFriction)(wNode* Car,
															   Int32 tireIndex);

void (*wPhysVehicleSetTireLateralFriction)(wNode* Car,
												  Int32 tireIndex,
												  Float32 value);

Float32 (*wPhysVehicleGetTireLateralFriction)(wNode* Car,
													 Int32 tireIndex);

void (*wPhysVehicleSetTireLongitudinalFriction)(wNode* Car,
													   Int32 tireIndex,
													   Float32 value);

Float32 (*wPhysVehicleGetTireLongitudinalFriction)(wNode* Car,
														  Int32 tireIndex);

void (*wPhysVehicleSetTireMass)(wNode* Car,
									   Int32 tireIndex,
									   Float32 mass);

Float32 (*wPhysVehicleGetTireMass)(wNode* Car,
										  Int32 tireIndex);

void (*wPhysVehicleSetTireRadius)(wNode* Car,
										 Int32 tireIndex,
										 Float32 radius);

Float32 (*wPhysVehicleGetTireRadius)(wNode* Car,
											Int32 tireIndex);

void (*wPhysVehicleSetTireWidth)(wNode* Car,
										Int32 tireIndex,
										Float32 width);

Float32 (*wPhysVehicleGetTireWidth)(wNode* Car,
										   Int32 tireIndex);

void  (*wPhysVehicleSetTireSpringConst)(wNode* Car,
											   Int32 tireIndex,
											   Float32 value);

Float32 (*wPhysVehicleGetTireSpringConst)(wNode* Car,
												 Int32 tireIndex);

void (*wPhysVehicleSetTireSpringDamper)(wNode* Car,
											   Int32 tireIndex,
											   Float32 value);

Float32 (*wPhysVehicleGetTireSpringDamper)(wNode* Car,
												  Int32 tireIndex);

void (*wPhysVehicleSetTireSuspensionLenght)(wNode* Car,
												   Int32 tireIndex,
												   Float32 value);

Float32 (*wPhysVehicleGetTireSuspensionLenght)(wNode* Car,
													  Int32 tireIndex);

void (*wPhysVehicleSetTireUserData)(wNode* Car,
										   Int32 tireIndex,
										   wNode* userData);

wNode* (*wPhysVehicleGetTireUserData)(wNode* Car,
											 Int32 tireIndex);

void (*wPhysVehicleSetTireMotorForce)(wNode* Car,
											 Int32 tireIndex,
											 Float32 value);

Float32 (*wPhysVehicleGetTireMotorForce)(wNode* Car,
												Int32 tireIndex);

void (*wPhysVehicleSetTireTurnForceHelper)(wNode* Car,
												  Int32 tireIndex,
												  Float32 value);

Float32 (*wPhysVehicleGetTireTurnForceHelper)(wNode* Car,
													 Int32 tireIndex);
void (*wPhysVehicleSetTireSpinTorqueFactor)(wNode* Car,
												   Int32 tireIndex,
												   Float32 value);

Float32 (*wPhysVehicleGetTireSpinTorqueFactor)(wNode* Car,
													  Int32 tireIndex);

void (*wPhysVehicleSetTireTorquePosition)(wNode* Car,
												 Int32 tireIndex,
												 wVector3f position);

wVector3f  (*wPhysVehicleGetTireTorquePosition)(wNode* Car,
													   Int32 tireIndex);

Float32 (*wPhysVehicleGetTireLoad)(wNode* Car,
										  Int32 tireIndex);

void (*wPhysVehicleSetTireSpinForce)(wNode* Car,
											Int32 tireIndex,
											Float32 value);

Float32 (*wPhysVehicleGetTireSpinForce)(wNode* Car,
											   Int32 tireIndex);

/*///wPhysRagDoll
UInt32* wPhysRagDollCreate(wNode* node,
                           Int32 bonesCount,
                           wPhysRagDollBoneParameters* params);

Int32 wPhysRagDollGetBonesCount(wNode* ragdoll);

void wPhysRagDollSetBoneCollisionState(wNode* ragdoll,
                                       Int32 boneIndex,
                                       Int32 state);

void wPhysRagDollSetBoneConeLimits(wNode* ragdoll,
                                   Int32 boneIndex,
                                   Float32 angle);

void wPhysRagDollSetBoneTwistLimits(wNode* ragdoll,
                                    Int32 boneIndex,
                                    Float32 minAngle,
                                    Float32 maxAngle);

void wPhysRagDollDraw(wNode* ragdoll);*/

///wPhysMaterial///
int	 (*wPhysMaterialCreate)();

void (*wPhysMaterialSetElasticity)(Int32 matId1,
										  Int32 matId2,
										  Float32 Elasticity);

void (*wPhysMaterialSetFriction)(Int32 matId1,
										Int32 matId2,
										Float32 StaticFriction,
										Float32 KineticFriction);

void (*wPhysMaterialSetContactSound)(Int32 matId1,
											Int32 matId2,
											wSound* soundNode);

void  (*wPhysMaterialSetSoftness)(Int32 matId1,
										 Int32 matId2,
										 Float32 Softness);

void (*wPhysMaterialSetCollidable)(Int32 matId1,
										  Int32 matId2,
										  bool isCollidable);

///wGui///
void (*wGuiDrawAll)();

void (*wGuiDestroyAll)();

bool (*wGuiIsEventAvailable)();

wGuiEvent* (*wGuiReadEvent)();

bool (*wGuiLoad)(const char* fileName,
						wGuiObject* start);

bool (*wGuiSave)(const char* fileName,
						wGuiObject* start);

wGuiObject* (*wGuiGetSkin)();

void (*wGuiSetSkin)(wGuiObject* skin);

const wchar_t* (*wGuiGetLastSelectedFile)();

///Returns the element which holds the focus.
wGuiObject* (*wGuiGetObjectFocused)();

///Returns the element which was last under the mouse cursor.
wGuiObject* (*wGuiGetObjectHovered)();

wGuiObject* (*wGuiGetRootNode)();

wGuiObject* (*wGuiGetObjectById)(Int32 id,
										bool searchchildren);

wGuiObject* (*wGuiGetObjectByName)(const char* name,
										  bool searchchildren);

///wGuiObject///
void (*wGuiObjectDestroy)(wGuiObject* element );

void (*wGuiObjectSetParent)(wGuiObject* element,
								   wGuiObject* parent);

wGuiObject* (*wGuiObjectGetParent)(wGuiObject* element);

void (*wGuiObjectSetRelativePosition)(wGuiObject* element,
											 wVector2i position);

void (*wGuiObjectSetRelativeSize)(wGuiObject* element,
										 wVector2i size);

wVector2i (*wGuiObjectGetRelativePosition)(wGuiObject* element);

wVector2i (*wGuiObjectGetRelativeSize)(wGuiObject* element);

wVector2i (*wGuiObjectGetAbsolutePosition)(wGuiObject* element);

wVector2i (*wGuiObjectGetAbsoluteClippedPosition)(wGuiObject* element);

wVector2i (*wGuiObjectGetAbsoluteClippedSize)(wGuiObject* element);

///Sets whether the element will ignore its parent's clipping rectangle.
void (*wGuiObjectSetClippingMode)(wGuiObject* element,
										 bool value);

bool (*wGuiObjectIsClipped)(wGuiObject* element);

void (*wGuiObjectSetMaxSize)(wGuiObject* element,
									wVector2i size);

void (*wGuiObjectSetMinSize)(wGuiObject* element,
									wVector2i size);

void (*wGuiObjectSetAlignment)(wGuiObject* element,
									  wGuiAlignment left,
									  wGuiAlignment right,
									  wGuiAlignment top,
									  wGuiAlignment bottom);

void (*wGuiObjectUpdateAbsolutePosition)(wGuiObject* element);

///Возвращает гуи-объект -потомок element-а, который находится на пересечении
///с точкой экрана position
///Если нужен любой объект, то в качестве element-а нужно
///поставить root=wGuiGetRootNode()
///Примечание: Элемент root имеет размер ВСЕГО экрана
wGuiObject* (*wGuiObjectGetFromScreenPos)(wGuiObject* element,
												 wVector2i position);

///Персекается ли объект с точкой экрана position
bool (*wGuiObjectIsPointInside)(wGuiObject* element,
									   wVector2i position);

void (*wGuiObjectDestroyChild)(wGuiObject* element,
									  wGuiObject* child);

///Можно вызывать вместо wGuiDrawAll() для конкретного элемента
void (*wGuiObjectDraw)(wGuiObject* element);

void (*wGuiObjectMoveTo)(wGuiObject* element,
								wVector2i position);

void (*wGuiObjectSetVisible)(wGuiObject* element,
									bool value);

bool (*wGuiObjectIsVisible)(wGuiObject* element);

/// Устанавливает, был ли этот элемент управления создан
/// как часть родительского элемента.
/// Например, если полоса прокрутки является частью списка.
/// Подразделы не сохраняются на диск при вызове wGuiSave()
void (*wGuiObjectSetSubObject)(wGuiObject* element,
									  bool value);

/// Вовзращает, был ли этот элемент управления создан
/// как часть родительского элемента.
bool (*wGuiObjectIsSubObject)(wGuiObject* element);

void (*wGuiObjectSetTabStop)(wGuiObject* element,
									bool value);

///Returns true if this element can be focused by navigating with the tab key.
bool (*wGuiObjectIsTabStop)(wGuiObject* element);

///Sets the priority of focus when using the tab key to navigate between a group of elements.
void (*wGuiObjectSetTabOrder)(wGuiObject* element,
									 Int32 index);

Int32 (*wGuiObjectGetTabOrder)(wGuiObject* element);

///If set to true, the focus will visit this element when using the tab key to cycle through elements.
void (*wGuiObjectSetTabGroup)(wGuiObject* element,
									 bool value);

bool (*wGuiObjectIsTabGroup)(wGuiObject* element);

void (*wGuiObjectSetEnable)(wGuiObject* element,
								   bool value);

bool (*wGuiObjectIsEnabled)(wGuiObject* element);

void (*wGuiObjectSetText)(wGuiObject* element,
								 const wchar_t* text);

const wchar_t* (*wGuiObjectGetText)(wGuiObject* element);

///Sets the new caption of this element.
void (*wGuiObjectSetToolTipText)(wGuiObject* element,
										const wchar_t* text);

const wchar_t* (*wGuiObjectGetToolTipText)(wGuiObject* element);

void (*wGuiObjectSetId)(wGuiObject* element,
							   Int32 id);

Int32 (*wGuiObjectGetId)(wGuiObject* element);

void (*wGuiObjectSetName)(wGuiObject* element,
								 const char* name);

bool (*wGuiObjectIsHovered)(wGuiObject* el);

const char* (*wGuiObjectGetName)(wGuiObject* element);

///Ищет  среди "детей" объекта искомого по его Id
///Если требуется найти ЛЮБОЙ ГУИ-объект сцены,
///нужно в качестве элемента указать root=wGuiGetRootNode()
wGuiObject* (*wGuiObjectGetChildById)(wGuiObject* element,
											 Int32 id,
											 bool searchchildren);

wGuiObject* (*wGuiObjectGetChildByName)(wGuiObject* element,
											   const char* name,
											   bool searchchildren);

bool (*wGuiObjectIsChildOf)(wGuiObject* element,
								   wGuiObject* child);

bool (*wGuiObjectBringToFront)(wGuiObject* element,
									  wGuiObject* subElement);

wGuiElementType (*wGuiObjectGetType)(wGuiObject* element);

const char* (*wGuiObjectGetTypeName)(wGuiObject* element);

bool (*wGuiObjectHasType)(wGuiObject* element,
								 wGuiElementType type);

bool (*wGuiObjectSetFocus)(wGuiObject* element);

bool (*wGuiObjectRemoveFocus)(wGuiObject* element);

bool (*wGuiObjectIsFocused)(wGuiObject* element);

void (*wGuiObjectReadFromXml)(wGuiObject* node,
									 wXmlReader* reader);

void (*wGuiObjectWriteToXml)(wGuiObject* node,
									wXmlWriter* writer);

///wGuiSkin///
wGuiObject* (*wGuiSkinCreate)(wGuiSkinSpace type);

wColor4s (*wGuiSkinGetColor)(wGuiObject* skin,
									wGuiDefaultColor elementType);

void (*wGuiSkinSetColor)(wGuiObject* skin,
								wGuiDefaultColor elementType,
								wColor4s color);

void (*wGuiSkinSetSize)(wGuiObject* skin,
							   wGuiDefaultSize sizeType,
							   Int32 newSize);

Int32 (*wGuiSkinGetSize)(wGuiObject* skin,
								wGuiDefaultSize sizeType);

const wchar_t* (*wGuiSkinGetDefaultText)(wGuiObject* skin,
												wGuiDefaultText txt);

void (*wGuiSkinSetDefaultText)(wGuiObject* skin,
									  wGuiDefaultText txt,
									  const wchar_t* newText);

void (*wGuiSkinSetFont)(wGuiObject* skin,
							   wFont* font,
							   wGuiDefaultFont fntType);

wFont* (*wGuiSkinGetFont)(wGuiObject* skin,
								 wGuiDefaultFont fntType);

void (*wGuiSkinSetSpriteBank)(wGuiObject* skin,
									 wGuiObject* bank);

wGuiObject* (*wGuiSkinGetSpriteBank)(wGuiObject* skin);

void (*wGuiSkinSetIcon)(wGuiObject* skin,
							   wGuiDefaultIcon icn,
							   UInt32 index);

UInt32 (*wGuiSkinGetIcon)(wGuiObject* skin,
								 wGuiDefaultIcon icn);

wGuiSkinSpace (*wGuiSkinGetType)(wGuiObject* skin);

///wGuiWindow///
wGuiObject* (*wGuiWindowCreate)(const wchar_t* wcptrTitle,
									   wVector2i minPos,
									   wVector2i maxPos,
									   bool modal);

wGuiObject* (*wGuiWindowGetButtonClose)(wGuiObject* win);

wGuiObject* (*wGuiWindowGetButtonMinimize)(wGuiObject* win);

wGuiObject* (*wGuiWindowGetButtonMaximize)(wGuiObject* win);

void (*wGuiWindowSetDraggable)(wGuiObject* win,
									  bool value);

bool (*wGuiWindowIsDraggable)(wGuiObject* win);

void (*wGuiWindowSetDrawBackground)(wGuiObject* win,
										   bool value);

bool (*wGuiWindowIsDrawBackground)(wGuiObject* win);

void (*wGuiWindowSetDrawTitleBar)(wGuiObject* win,
										 bool value);

bool (*wGuiWindowIsDrawTitleBar)(wGuiObject* win);

///wGuiLabel
wGuiObject* (*wGuiLabelCreate)(const wchar_t * wcptrText,
									  wVector2i minPos,
									  wVector2i maxPos,
									  bool boBorder,
									  bool boWordWrap);

wVector2i (*wGuiLabelGetTextSize)(wGuiObject* txt);

void (*wGuiLabelSetOverrideFont)(wGuiObject* obj,
										wFont* font);

wFont* (*wGuiLabelGetOverrideFont)(wGuiObject* obj);

wFont* (*wGuiLabelGetActiveFont)(wGuiObject* obj);

void (*wGuiLabelEnableOverrideColor)(wGuiObject* obj,
											bool value);

bool (*wGuiLabelIsOverrideColor)(wGuiObject* obj);

void (*wGuiLabelSetOverrideColor)(wGuiObject* obj,
										 wColor4s color);

wColor4s (*wGuiLabelGetOverrideColor)(wGuiObject* obj);

void (*wGuiLabelSetDrawBackground)(wGuiObject* obj,
										  bool value);

bool (*wGuiLabelIsDrawBackGround)(wGuiObject* obj);

void (*wGuiLabelSetDrawBorder)(wGuiObject* obj,
									  bool value);

bool (*wGuiLabelIsDrawBorder)(wGuiObject* obj) ;

void (*wGuiLabelSetTextAlignment)(wGuiObject* obj,
										 wGuiAlignment Horizontalvalue,
										 wGuiAlignment Verticalvalue);

void (*wGuiLabelSetWordWrap)(wGuiObject* obj,
									bool value);

bool (*wGuiLabelIsWordWrap)(wGuiObject* obj);

void (*wGuiLabelSetBackgroundColor)(wGuiObject* obj,
										   wColor4s color);

wColor4s (*wGuiLabelGetBackgroundColor)(wGuiObject* obj);

///wGuiButton
wGuiObject* (*wGuiButtonCreate)(wVector2i minPos,
									   wVector2i maxPos,
									   const wchar_t* wcptrLabel,
									   const wchar_t* wcptrTip);

void (*wGuiButtonSetImage)(wGuiObject* btn,
								  wTexture* img);

void (*wGuiButtonSetImageFromRect)(wGuiObject* btn,
										  wTexture* img,
										  wVector2i* minRect,
										  wVector2i* maxRect);

void (*wGuiButtonSetPressedImage)(wGuiObject* btn,
										 wTexture* img);


void (*wGuiButtonSetPressedImageFromRect)(wGuiObject* btn,
												 wTexture* img,
												 wVector2i* minRect,
												 wVector2i* maxRect);

void (*wGuiButtonSetSpriteBank)(wGuiObject* btn,
									   wGuiObject* bank);

void (*wGuiButtonSetSprite)(wGuiObject* btn,
								   wGuiButtonState state,
								   Int32 index,
								   wColor4s color,
								   bool loop);

void (*wGuiButtonSetPush)(wGuiObject* btn,
								 bool value);

bool (*wGuiButtonIsPushed)(wGuiObject* btn);

void (*wGuiButtonSetPressed)(wGuiObject* btn,
									bool value);

bool (*wGuiButtonIsPressed)(wGuiObject* btn);

void (*wGuiButtonUseAlphaChannel)(wGuiObject* btn,
										 bool value);

bool (*wGuiButtonIsUsedAlphaChannel)(wGuiObject* btn);

void (*wGuiButtonEnableScaleImage)(wGuiObject* btn,
										  bool value);

bool (*wGuiButtonIsScaledImage)(wGuiObject* btn);

void (*wGuiButtonSetOverrideFont)(wGuiObject* obj,
										 wFont* font);

wFont* (*wGuiButtonGetOverrideFont)(wGuiObject* obj);

wFont* (*wGuiButtonGetActiveFont)(wGuiObject* obj);

void (*wGuiButtonSetDrawBorder)(wGuiObject* obj,
									   bool value);

bool (*wGuiButtonIsDrawBorder)(wGuiObject* obj);

///wGuiButtonGroup///
wGuiObject* (*wGuiButtonGroupCreate)(wVector2i minPos,
											wVector2i maxPos);

Int32 (*wGuiButtonGroupAddButton)(wGuiObject* group,
										 wGuiObject* button);

Int32 (*wGuiButtonGroupInsertButton)(wGuiObject* group,
											wGuiObject* button,
											UInt32 index);

wGuiObject* (*wGuiButtonGroupGetButton)(wGuiObject* group,
											   UInt32 index);

bool (*wGuiButtonGroupRemoveButton)(wGuiObject* group,
										   UInt32 index);

void (*wGuiButtonGroupRemoveAll)(wGuiObject* group);

UInt32 (*wGuiButtonGroupGetSize)(wGuiObject* group);

Int32 (*wGuiButtonGroupGetSelectedIndex)(wGuiObject* group);

void (*wGuiButtonGroupSetSelectedIndex)(wGuiObject* group,
											   Int32 index);

void (*wGuiButtonGroupClearSelection)(wGuiObject* group);

void (*wGuiButtonGroupSetBackgroundColor)(wGuiObject* group,
												 wColor4s color);

void (*wGuiButtonGroupDrawBackground)(wGuiObject* group,
											 bool value);
///wGuiListBox///
wGuiObject* (*wGuiListBoxCreate)(wVector2i minPos,
										wVector2i maxPos,
										bool background);

UInt32 (*wGuiListBoxGetItemsCount)(wGuiObject* lbox);

const wchar_t* (*wGuiListBoxGetItemByIndex)(wGuiObject* lbox,
												   UInt32 id);

UInt32 (*wGuiListBoxAddItem)(wGuiObject* lbox,
									const wchar_t* text);

UInt32 (*wGuiListBoxAddItemWithIcon)(wGuiObject* lbox,
											const wchar_t* text,
											Int32 icon);

void (*wGuiListBoxRemoveItem)(wGuiObject* lbox,
									 UInt32 index);

void (*wGuiListBoxRemoveAll)(wGuiObject* lbox);

void (*wGuiListBoxSetItem)(wGuiObject* lbox,
								  UInt32 index,
								  const wchar_t* text,
								  Int32 icon);

void (*wGuiListBoxInsertItem)(wGuiObject* lbox,
									 UInt32 index,
									 const wchar_t* text,
									 Int32 icon);

Int32 (*wGuiListBoxGetItemIcon)(wGuiObject* lbox,
									   UInt32 index);

UInt32 (*wGuiListBoxGetSelectedIndex)(wGuiObject* lbox);

void (*wGuiListBoxSelectItemByIndex)(wGuiObject* lbox,
											UInt32 index);

void (*wGuiListBoxSelectItemByText)(wGuiObject* lbox,
										   const wchar_t* item);

void (*wGuiListBoxSwapItems)(wGuiObject* lbox,
									UInt32 index1,
									UInt32 index2);

void (*wGuiListBoxSetItemsHeight)(wGuiObject* lbox,
										 Int32 height);

void (*wGuiListBoxSetAutoScrolling)(wGuiObject* lbox,
										   bool scroll);

bool (*wGuiListBoxIsAutoScrolling)(wGuiObject* lbox);

void (*wGuiListBoxSetItemColor)(wGuiObject* lbox,
									   UInt32 index,
									   wColor4s color);

void (*wGuiListBoxSetElementColor)(wGuiObject* lbox,
										  UInt32 index,
										  wGuiListboxColor colorType,
										  wColor4s color);

void (*wGuiListBoxClearItemColor)(wGuiObject* lbox,
										 UInt32 index);

void (*wListBoxClearElementColor)(wGuiObject* lbox,
										 UInt32 index,
										 wGuiListboxColor colorType);

wColor4s (*wGuiListBoxGetElementColor)(wGuiObject* lbox,
											  UInt32 index,
											  wGuiListboxColor colorType);

bool (*wGuiListBoxHasElementColor)(wGuiObject* lbox,
										  UInt32 index,
										  wGuiListboxColor colorType);

wColor4s (*wGuiListBoxGetDefaultColor)(wGuiObject* lbox,
											  wGuiListboxColor colorType);

void (*wGuiListBoxSetDrawBackground)(wGuiObject* obj,
											bool value);

///wGuiScrollBar
wGuiObject* (*wGuiScrollBarCreate)(bool Horizontal,
										  wVector2i minPos,
										  wVector2i maxPos);

void (*wGuiScrollBarSetMaxValue)(wGuiObject* scroll,
										Int32 max);

Int32 (*wGuiScrollBarGetMaxValue)(wGuiObject* scroll);

void (*wGuiScrollBarSetMinValue)(wGuiObject* scroll,
										Int32 min);

Int32 (*wGuiScrollBarGetMinValue)(wGuiObject* scroll);

void (*wGuiScrollBarSetValue)(wGuiObject* scroll,
									 Int32 value);

Int32 (*wGuiScrollBarGetValue)(wGuiObject* scroll);

void (*wGuiScrollBarSetSmallStep)(wGuiObject* scroll,
										 Int32 step);

Int32 (*wGuiScrollBarGetSmallStep)(wGuiObject* scroll);

void (*wGuiScrollBarSetLargeStep)(wGuiObject* scroll,
										 Int32 step);

Int32 (*wGuiScrollBarGetLargeStep)(wGuiObject* scroll);

///wGuiEditBox
wGuiObject* (*wGuiEditBoxCreate)(const wchar_t* wcptrText,
										wVector2i minPos,
										wVector2i maxPos);

void (*wGuiEditBoxSetMultiLine)(wGuiObject* box,
									   bool value);

bool (*wGuiEditBoxIsMultiLine)(wGuiObject* box);

void (*wGuiEditBoxSetAutoScrolling)(wGuiObject* box,
										   bool value);

bool (*wGuiEditBoxIsAutoScrolling)(wGuiObject* box);

void (*wGuiEditBoxSetPasswordMode)(wGuiObject* box,
										  bool value);

bool (*wGuiEditBoxIsPasswordMode)(wGuiObject* box);

wVector2i (*wGuiEditBoxGetTextSize)(wGuiObject* box);

///Sets the maximum amount of characters which may be entered in the box.
void (*wGuiEditBoxSetCharactersLimit)(wGuiObject* box,
											 UInt32 max);

UInt32 (*wGuiEditGetCharactersLimit)(wGuiObject* box);

void (*wGuiEditBoxSetOverrideFont)(wGuiObject* obj,
										  wFont* font);

wFont* (*wGuiEditBoxGetOverrideFont)(wGuiObject* obj);

wFont* (*wGuiEditBoxGetActiveFont)(wGuiObject* obj);

void (*wGuiEditBoxEnableOverrideColor)(wGuiObject* obj,
											  bool value);

bool (*wGuiEditBoxIsOverrideColor)(wGuiObject* obj);

void (*wGuiEditBoxSetOverrideColor)(wGuiObject* obj,
										   wColor4s color);

wColor4s (*wGuiEditBoxGetOverrideColor)(wGuiObject* obj);

void (*wGuiEditBoxSetDrawBackground)(wGuiObject* obj,
											bool value);

void (*wGuiEditBoxSetDrawBorder)(wGuiObject* obj,
										bool value);

bool (*wGuiEditBoxIsDrawBorder)(wGuiObject* obj);

void (*wGuiEditBoxSetTextAlignment)(wGuiObject* obj,
										   wGuiAlignment Horizontalvalue,
										   wGuiAlignment Verticalvalue);

void (*wGuiEditBoxSetWordWrap)(wGuiObject* obj,
									  bool value);

bool (*wGuiEditBoxIsWordWrap)(wGuiObject* obj);

///wGuiImage///
wGuiObject* (*wGuiImageCreate)(wTexture* texture,
									  wVector2i size,
									  bool useAlpha);

void (*wGuiImageSet)(wGuiObject* img,
							wTexture* tex);

wTexture* (*wGuiImageGet)(wGuiObject* img);

void (*wGuiImageSetColor)(wGuiObject* img,
								 wColor4s color);

wColor4s (*wGuiImageGetColor)(wGuiObject* img);

void (*wGuiImageSetScaling)(wGuiObject* img,
								   bool scale);

bool (*wGuiImageIsScaled)(wGuiObject* img);

void (*wGuiImageUseAlphaChannel)(wGuiObject* img,
										bool use);

bool (*wGuiImageIsUsedAlphaChannel)(wGuiObject* img);

///wGuiFader///
wGuiObject* (*wGuiFaderCreate)(wVector2i minPos,
									  wVector2i maxPos);

void (*wGuiFaderSetColor)(wGuiObject* fader,
								 wColor4s color);

wColor4s (*wGuiFaderGetColor)(wGuiObject* fader);

void (*wGuiFaderSetColorExt)(wGuiObject* fader,
									wColor4s colorSrc,
									wColor4s colorDest);

void (*wGuiFaderFadeIn)(wGuiObject* fader,
							   UInt32 timeMs);

void (*wGuiFaderFadeOut)(wGuiObject* fader,
								UInt32 timeMs);

bool (*wGuiFaderIsReady)(wGuiObject* fader);

///wGuiCheckBox///
wGuiObject* (*wGuiCheckBoxCreate)(const wchar_t* wcptrText,
										 wVector2i minPos,
										 wVector2i maxPos,
										 bool checked);

void (*wGuiCheckBoxCheck)(wGuiObject* box,
								 bool checked);

bool (*wGuiCheckBoxIsChecked)(wGuiObject* box);

/// Sets whether to draw the background
void (*wGuiCheckBoxSetDrawBackground)(wGuiObject* box,
											 bool value);

/// Checks if background drawing is enabled
///return true if background drawing is enabled, false otherwise
bool (*wGuiCheckBoxIsDrawBackground)(wGuiObject* box);

/// Sets whether to draw the border
void (*wGuiCheckBoxSetDrawBorder)(wGuiObject* box,
										 bool value);

/// Checks if border drawing is enabled
///return true if border drawing is enabled, false otherwise
bool (*wGuiCheckBoxIsDrawBorder)(wGuiObject* box);

void (*wGuiCheckBoxSetFilled)(wGuiObject* box,
									 bool value);

bool (*wGuiCheckBoxIsFilled)(wGuiObject* box);

///wGuiFileOpenDialog
/*Warning:
    When the user selects a folder this does change the current working directory

This element can create the following events of type wGuiEventType:

        wGET_DIRECTORY_SELECTED
        wGET_FILE_SELECTED
        wGET_FILE_CHOOSE_DIALOG_CANCELLED
*/
wGuiObject* (*wGuiFileOpenDialogCreate)(const wchar_t* wcptrLabel,
											   bool modal);

///Returns the filename of the selected file. Returns NULL, if no file was selected.
const wchar_t* (*wGuiFileOpenDialogGetFile)(wGuiObject* dialog);

///Returns the directory of the selected file. Returns NULL, if no directory was selected.
const char* (*wGuiFileOpenDialogGetDirectory)(wGuiObject* dialog);

///wGuiComboBox///
wGuiObject* (*wGuiComboBoxCreate)(wVector2i minPos,
										 wVector2i maxPos);

UInt32 (*wGuiComboBoxGetItemsCount)(wGuiObject* combo);

const wchar_t* (*wGuiComboBoxGetItemByIndex)(wGuiObject* combo,
													UInt32 idx);

UInt32 (*wGuiComboBoxGetItemDataByIndex)(wGuiObject* combo,
												UInt32 idx);

Int32 (*wGuiComboBoxGetIndexByItemData)(wGuiObject* combo,
											   UInt32 data);

UInt32 (*wGuiComboBoxAddItem)(wGuiObject* combo,
									 const wchar_t* text,
									 UInt32 data);

void (*wGuiComboBoxRemoveItem)(wGuiObject* combo,
									  UInt32 idx);

void (*wGuiComboBoxRemoveAll)(wGuiObject* combo);

///Returns id of selected item. returns -1 if no item is selected.
Int32 (*wGuiComboBoxGetSelected)(wGuiObject* combo);

void (*wGuiComboBoxSetSelected)(wGuiObject* combo,
									   UInt32 idx);

void (*wGuiComboBoxSetMaxSelectionRows)(wGuiObject* combo,
											   UInt32 max);

UInt32 (*wGuiComboBoxGetMaxSelectionRows)(wGuiObject* combo);

void (*wGuiComboBoxSetTextAlignment)(wGuiObject* obj,
											wGuiAlignment Horizontalvalue,
											wGuiAlignment Verticalvalue);

///wGuiContextMenu///
wGuiObject* (*wGuiContextMenuCreate)(wVector2i minPos,
											wVector2i maxPos);

void (*wGuiContextMenuSetCloseHandling)(wGuiObject* cmenu,
											   wContextMenuClose onClose);

wContextMenuClose (*wGuiContextMenuGetCloseHandling)(wGuiObject* cmenu);

UInt32 (*wGuiContextMenuGetItemsCount)(wGuiObject* cmenu);

UInt32 (*wGuiContextMenuAddItem)(wGuiObject* cmenu,
										const wchar_t* text,
										Int32 commandId,
										bool enabled,
										bool hasSubMenu,
										bool checked,
										bool autoChecking);

UInt32 (*wGuiContextMenuInsertItem)(wGuiObject* cmenu,
										   UInt32 idx,
										   const wchar_t* text,
										   Int32 commandId,
										   bool enabled,
										   bool hasSubMenu,
										   bool checked,
										   bool autoChecking);

void (*wGuiContextMenuAddSeparator)(wGuiObject* cmenu);

const wchar_t* (*wGuiContextMenuGetItemText)(wGuiObject* cmenu,
													UInt32 idx);

void (*wGuiContextMenuSetItemText)(wGuiObject* cmenu,
										  UInt32 idx,
										  const wchar_t* text);

void (*wGuiContextMenuSetItemEnabled)(wGuiObject* cmenu,
											 UInt32 idx,
											 bool value);

bool (*wGuiContextMenuIsItemEnabled)(wGuiObject* cmenu,
											UInt32 idx);

void (*wGuiContextMenuSetItemChecked)(wGuiObject* cmenu,
											 UInt32 idx,
											 bool value);

bool (*wGuiContextMenuIsItemChecked)(wGuiObject* cmenu,
											UInt32 idx);

void (*wGuiContextMenuRemoveItem)(wGuiObject* cmenu,
										 UInt32 idx);

void (*wGuiContextMenuRemoveAll)(wGuiObject* cmenu);

Int32 (*wGuiContextMenuGetSelectedItem)(wGuiObject* cmenu);

Int32 (*wGuiContextMenuGetItemCommandId)(wGuiObject* cmenu,
												UInt32 idx);

Int32 (*wGuiContextMenuFindItem)(wGuiObject* cmenu,
										Int32 id,
										UInt32 idx);

void (*wGuiContextMenuSetItemCommandId)(wGuiObject* cmenu,
											   UInt32 idx,
											   Int32 id);

wGuiObject* (*wGuiContextMenuGetSubMenu)(wGuiObject* cmenu,
												UInt32 idx);

void (*wGuiContextMenuSetAutoChecking)(wGuiObject* cmenu,
											  UInt32 idx,
											  bool autoChecking);

bool (*wGuiContextMenuIsAutoChecked)(wGuiObject* cmenu,
											UInt32 idx);

///When an eventparent is set it receives events instead of the usual parent element.
void (*wGuiContextMenuSetEventParent)(wGuiObject* cmenu,
											 wGuiObject* parent);

///wGuiMenu///
///Adds a menu to the environment.This is like the menu you can find on top of most windows in modern graphical user interfaces.
///Для работы с меню подходят все команды
///из раздела wGuiContextMenu///
wGuiObject* (*wGuiMenuCreate)();

///wGuiModalScreen///
///Adds a modal screen.
///This control stops its parent's members from being able to receive input until its last child is removed,
/// it then deletes itself.
wGuiObject* (*wGuiModalScreenCreate)();

///wGuiSpinBox///
wGuiObject* (*wGuiSpinBoxCreate)(const wchar_t * wcptrText,
										wVector2i minPos,
										wVector2i maxPos,
										bool border);

wGuiObject* (*wGuiSpinBoxGetEditBox)(wGuiObject* box);

void (*wGuiSpinBoxSetValue)(wGuiObject* spin,
								   Float32 value);

Float32 (*wGuiSpinBoxGetValue)(wGuiObject* spin);

void (*wGuiSpinBoxSetRange)(wGuiObject* spin,
								   wVector2f range);

Float32 (*wGuiSpinBoxGetMin)(wGuiObject* spin);

Float32 (*wGuiSpinBoxGetMax)(wGuiObject* spin);

void (*wGuiSpinBoxSetStepSize)(wGuiObject* spin,
									  Float32 step);

Float32 (*wGuiSpinBoxGetStepSize)(wGuiObject* spin);

void (*wGuiSpinBoxSetDecimalPlaces)(wGuiObject* spin,
										   Int32 places);

///wGuiTab///
wGuiObject* (*wGuiTabCreate)(wVector2i minPos,
									wVector2i maxPos );

Int32 (*wGuiTabGetNumber)(wGuiObject* tab);

void (*wGuiTabSetTextColor)(wGuiObject* tab,
								   wColor4s color);

wColor4s (*wGuiTabGetTextColor)(wGuiObject* tab);

void (*wGuiTabSetDrawBackground)(wGuiObject* tab,
										bool value);

bool (*wGuiTabIsDrawBackground)(wGuiObject* obj);

void (*wGuiTabSetBackgroundColor)(wGuiObject* tab,
										 wColor4s color);

wColor4s (*wGuiTabGetBackgroundColor)(wGuiObject* tab);

///wGuiTabControl///
wGuiObject* (*wGuiTabControlCreate)(wVector2i minPos,
										   wVector2i maxPos,
										   bool background,
										   bool border);

Int32 (*wGuiTabControlGetTabsCount)(wGuiObject* control);

wGuiObject* (*wGuiTabControlAddTab)(wGuiObject* control,
										   const wchar_t* caption,
										   Int32 id);

wGuiObject* (*wGuiTabControlInsertTab)(wGuiObject* control,
											  UInt32 idx,
											  const wchar_t* caption,
											  Int32 id);

wGuiObject* (*wGuiTabControlGetTab)(wGuiObject* control,
										   Int32 idx);

bool (*wGuiTabControlSetActiveTabByIndex)(wGuiObject* control,
												 Int32 idx);

bool (*wGuiTabControlSetActiveTab)(wGuiObject* control,
										  wGuiObject* tab);

Int32 (*wGuiTabControlGetActiveTab)(wGuiObject* control);

Int32 (*wGuiTabControlGetTabFromPos)(wGuiObject* control,
											wVector2i position);

void (*wGuiTabControlRemoveTab)(wGuiObject* control,
									   Int32 idx);

void (*wGuiTabControlRemoveAll)(wGuiObject* control);

void (*wGuiTabControlSetTabHeight)(wGuiObject* control,
										  Int32 height);

Int32 (*wGuiTabControlGetTabHeight)(wGuiObject* control);

void (*wGuiTabControlSetTabMaxWidth)(wGuiObject* control,
											Int32 width);

Int32 (*wGuiTabControlGetTabMaxWidth)(wGuiObject* control);

void (*wGuiTabControlSetVerticalAlignment)(wGuiObject* control,
												  wGuiAlignment al);

wGuiAlignment (*wGuiTabControlGetVerticalAlignment)(wGuiObject* control);

void (*wGuiTabControlSetTabExtraWidth)(wGuiObject* control,
											  Int32 extraWidth);

Int32 (*wGuiTabControlGetTabExtraWidth)(wGuiObject* control);

///wGuiTable///
wGuiObject* (*wGuiTableCreate)(wVector2i minPos,
									  wVector2i maxPos,
									  bool background);

void (*wGuiTableAddColumn)(wGuiObject* table,
								  wchar_t* caption,
								  Int32 columnIndex);

void (*wGuiTableRemoveColumn)(wGuiObject* table,
									 UInt32 columnIndex);

Int32 (*wGuiTableGetColumnsCount)(wGuiObject* table);

bool (*wGuiTableSetActiveColumn)(wGuiObject* table,
										Int32 idx,
										bool doOrder);

Int32 (*wGuiTableGetActiveColumn)(wGuiObject* table);

wGuiOrderingMode (*wGuiTableGetActiveColumnOrdering)(wGuiObject* table);

void (*wGuiTableSetColumnWidth)(wGuiObject* table,
									   UInt32 columnIndex,
									   UInt32 width);

void (*wGuiTableSetColumnsResizable)(wGuiObject* table,
											bool resizible);

bool (*wGuiTableIsColumnsResizable)(wGuiObject* table);

Int32 (*wGuiTableGetSelected)(wGuiObject* table);

void (*wGuiTableSetSelectedByIndex)(wGuiObject* table,
										   Int32 index);

Int32 (*wGuiTableGetRowsCount)(wGuiObject* table);

UInt32 (*wGuiTableAddRow)(wGuiObject* table,
								 UInt32 rowIndex);

void (*wGuiTableRemoveRow)(wGuiObject* table,
								  UInt32 rowIndex);

void (*wGuiTableClearRows)(wGuiObject* table);

void (*wGuiTableSwapRows)(wGuiObject* table,
								 UInt32 rowIndexA,
								 UInt32 rowIndexB);

void (*wGuiTableSetOrderRows)(wGuiObject* table,
									 Int32 columnIndex,
									 wGuiOrderingMode mode);

void (*wGuiTableSetCellText)(wGuiObject* table,
									UInt32 rowIndex,
									UInt32 columnIndex,
									const wchar_t* text,
									wColor4s color);

void (*wGuiTableSetCellData)(wGuiObject* table,
									UInt32 rowIndex,
									UInt32 columnIndex,
									UInt32* data);

void (*wGuiTableSetCellColor)(wGuiObject* table,
									 UInt32 rowIndex,
									 UInt32 columnIndex,
									 wColor4s color);

const wchar_t* (*wGuiTableGetCellText)(wGuiObject* table,
											  UInt32 rowIndex,
											  UInt32 columnIndex );

UInt32* (*wGuiTableGetCellData)(wGuiObject* table,
									   UInt32 rowIndex,
									   UInt32 columnIndex );

void (*wGuiTableSetDrawFlags)(wGuiObject* table,
									 wGuiTableDrawFlags flags);

wGuiTableDrawFlags (*wGuiTableGetDrawFlags)(wGuiObject* table);

void (*wGuiTableSetOverrideFont)(wGuiObject* obj,
										wFont* font);

wFont* (*wGuiTableGetOverrideFont)(wGuiObject* obj);

wFont* (*wGuiTableGetActiveFont)(wGuiObject* obj);

Int32 (*wGuiTableGetItemHeight)(wGuiObject* obj);

wGuiObject* (*wGuiTableGetVerticalScrollBar)(wGuiObject* obj);

wGuiObject* (*wGuiTableGetHorizontalScrollBar)(wGuiObject* obj);

void (*wGuiTableSetDrawBackground)(wGuiObject* obj, bool value);

bool (*wGuiTableIsDrawBackground)(wGuiObject* obj);

///wGuiToolBar///
wGuiObject* (*wGuiToolBarCreate)();

wGuiObject* (*wGuiToolBarAddButton)(wGuiObject* bar,
										   const wchar_t* text,
										   const wchar_t* tooltiptext,
										   wTexture* img,
										   wTexture* pressedImg,
										   bool isPushButton,
										   bool useAlphaChannel);


///wGuiMessageBox
wGuiObject* (*wGuiMessageBoxCreate)(const wchar_t * wcptrTitle,
										   const wchar_t* wcptrTCaption,
										   bool modal,
										   wGuiMessageBoxFlags flags,
										   wTexture* image);

///wGuiTree///
///Create a tree view element.
wGuiObject* (*wGuiTreeCreate)(wVector2i minPos,
									 wVector2i maxPos,
									 bool background,
									 bool barvertical,
									 bool barhorizontal);

///returns the root node (not visible) from the tree.
wGuiObject* (*wGuiTreeGetRoot)(wGuiObject* tree);

///returns the selected node of the tree or 0 if none is selected
wGuiObject* (*wGuiTreeGetSelected)(wGuiObject* tree);

///sets if the tree lines are visible
void (*wGuiTreeSetLinesVisible)(wGuiObject* tree,
									   bool visible);

///returns true if the tree lines are visible
bool (*wGuiTreeIsLinesVisible)(wGuiObject* tree);

///Sets the font which should be used as icon font.
void (*wGuiTreeSetIconFont)(wGuiObject* tree,
								   wFont* font);

///Sets the image list which should be used for the image and selected image of every node.
void (*wGuiTreeSetImageList)(wGuiObject* tree,
									wGuiObject* list);

///Returns the image list which is used for the nodes.
wGuiObject* (*wGuiTreeGetImageList)(wGuiObject* tree);

///Sets if the image is left of the icon. Default is true.
void (*wGuiTreeSetImageLeftOfIcon)(wGuiObject* tree,
										  bool bLeftOf);

///Returns if the Image is left of the icon. Default is true.
bool (*wGuiTreeIsImageLeftOfIcon)(wGuiObject* tree);

///Returns the node which is associated to the last event.
wGuiObject* (*wGuiTreeGetLastEventNode)(wGuiObject* tree);

///wGuiTreeNode///
///returns the owner (Gui tree) of this node
wGuiObject* (*wGuiTreeNodeGetOwner)(wGuiObject* node);

wGuiObject* (*wGuiTreeNodeGetParent)(wGuiObject* node);

///returns the text of the node
const wchar_t* (*wGuiTreeNodeGetText)(wGuiObject* node);

///sets the text of the node
void (*wGuiTreeNodeSetText)(wGuiObject* node,
								   const wchar_t* text);

///sets the icon text of the node
void (*wGuiTreeNodeSetIcon)(wGuiObject* node,
								   const wchar_t* icon);

///returns the icon text of the node
const wchar_t* (*wGuiTreeNodeGetIcon)(wGuiObject* node);

///sets the image index of the node
void (*wGuiTreeNodeSetImageIndex)(wGuiObject* node,
										 UInt32 imageIndex);

///returns the image index of the node
UInt32 (*wGuiTreeNodeGetImageIndex)(wGuiObject* node);

///sets the image index of the node
void (*wGuiTreeNodeSetSelectedImageIndex)(wGuiObject* node,
												 UInt32 imageIndex);

///returns the image index of the node
UInt32 (*wGuiTreeNodeGetSelectedImageIndex)(wGuiObject* node);

///sets the user data (UInt32*) of this node
void  (*wGuiTreeNodeSetData)(wGuiObject* node,
									UInt32* data);

///returns the user data (UInt32*) of this node
UInt32* (*wGuiTreeNodeGetData)(wGuiObject* node);

///sets the user data2 of this node
void (*wGuiTreeNodeSetData2)(wGuiObject* node,
									UInt32* data2);

///returns the user data2 of this node
UInt32* (*wGuiTreeNodeGetData2)(wGuiObject* node);

///returns the child item count
UInt32 (*wGuiTreeNodeGetChildsCount)(wGuiObject* node);

///Remove a child node.
void (*wGuiTreeNodeRemoveChild)(wGuiObject* node,
									   wGuiObject* child);

///removes all children (recursive) from this node
void (*wGuiTreeNodeRemoveChildren)(wGuiObject* node);

///returns true if this node has child nodes
bool (*wGuiTreeNodeHasChildren)(wGuiObject* node);

///Adds a new node behind the last child node.
wGuiObject* (*wGuiTreeNodeAddChildBack)(wGuiObject* node,
											   const wchar_t* text,
											   const wchar_t* icon,
											   Int32 imageIndex,
											   Int32 selectedImageIndex,
											   void* data,
											   UInt32* data2);


///Adds a new node before the first child node.
wGuiObject* (*wGuiTreeNodeAddChildFront)(wGuiObject* node,
												const wchar_t* text,
												const wchar_t* icon,
												Int32 imageIndex,
												Int32 selectedImageIndex,
												void* data,
												UInt32* data2);

///Adds a new node behind the other node.
wGuiObject* (*wGuiTreeNodeInsertChildAfter)(wGuiObject* node,
												   wGuiObject* other,
												   const wchar_t* text,
												   const wchar_t* icon,
												   Int32 imageIndex,
												   Int32 selectedImageIndex,
												   void* data,
												   UInt32* data2);

///Adds a new node before the other node.
wGuiObject* (*wGuiTreeNodeInsertChildBefore)(wGuiObject* node,
													wGuiObject* other,
													const wchar_t* text,
													const wchar_t* icon,
													Int32 imageIndex,
													Int32 selectedImageIndex,
													void* data,
													UInt32* data2);

///Return the first child node from this node.
wGuiObject* (*wGuiTreeNodeGetFirstChild)(wGuiObject* node);

///Return the last child node from this node.
wGuiObject* (*wGuiTreeNodeGetLastChild)(wGuiObject* node);

///Returns the previous sibling node from this node.
wGuiObject* (*wGuiTreeNodeGetPrevSibling)(wGuiObject* node);

///Returns the next sibling node from this node.
wGuiObject* (*wGuiTreeNodeGetNextSibling)(wGuiObject* node);

///Returns the next visible (expanded, may be out of scrolling) node from this node.
wGuiObject* (*wGuiTreeNodeGetNextVisible)(wGuiObject* node);

///Moves a child node one position up.
bool (*wGuiTreeNodeMoveChildUp)(wGuiObject* node,
									   wGuiObject* child);

///Moves a child node one position down.
bool (*wGuiTreeNodeMoveChildDown)(wGuiObject* node,
										 wGuiObject* child);

///Sets if the node is expanded.
void (*wGuiTreeNodeSetExpanded)(wGuiObject* node,
									   bool expanded);

///Returns true if the node is expanded (children are visible).
bool (*wGuiTreeNodeIsExpanded)(wGuiObject* node);

///Sets this node as selected
void (*wGuiTreeNodeSetSelected)(wGuiObject* node,
									   bool selected);

///Returns true if the node is currently selected.
bool  (*wGuiTreeNodeIsSelected)(wGuiObject* node);

///Returns true if this node is the root node.
bool (*wGuiTreeNodeIsRoot)(wGuiObject* node);

///Returns the level of this node.
///The root node has level 0.
///Direct children of the root has level 1 ...
Int32 (*wGuiTreeNodeGetLevel)(wGuiObject* node);

///Returns true if this node is visible (all parents are expanded)
bool (*wGuiTreeNodeIsVisible)(wGuiObject* node);

///wGuiImageList///
wGuiObject* (*wGuiImageListCreate)(wTexture* texture,
										  wVector2i size,
										  bool useAlphaChannel);

void (*wGuiImageListDraw)(wGuiObject* list,
								 Int32 index,
								 wVector2i pos,
								 wVector2i clipPos,
								 wVector2i clipSize);

Int32 (*wGuiImageListGetCount)(wGuiObject* list);

wVector2i (*wGuiImageListGetSize)(wGuiObject* list);

///wGuiColorSelectDialog///
wGuiObject* (*wGuiColorSelectDialogCreate)(const wchar_t* title,
												  bool modal);

///wGuiMeshViewer///
wGuiObject* (*wGuiMeshViewerCreate)(wVector2i minPos,
										   wVector2i maxPos,
										   const wchar_t* text);

void (*wGuiMeshViewerSetMesh)(wGuiObject* viewer,
									 wMesh* mesh);

wMesh* (*wGuiMeshViewerGetMesh)(wGuiObject* viewer);

void (*wGuiMeshViewerSetMaterial)(wGuiObject* viewer,
										 wMaterial* material);

wMaterial* (*wGuiMeshViewerGetMaterial)(wGuiObject* viewer);

///wGuiSpriteBank///
///Returns pointer to the sprite bank with the specified file name.
///Loads the bank if it was not loaded before.
wGuiObject* (*wGuiSpriteBankLoad)(const char* file);

wGuiObject* (*wGuiSpriteBankCreate)(const char* name);

///Adds a texture to the sprite bank.
void (*wGuiSpriteBankAddTexture)(wGuiObject* bank,
										wTexture* texture);

///Changes one of the textures in the sprite bank
void (*wGuiSpriteBankSetTexture)(wGuiObject* bank,
										UInt32 index,
										wTexture* texture);

///Add the texture and use it for a single non-animated sprite.
///The texture and the corresponding rectangle and sprite will all be added
/// to the end of each array. returns the index of the sprite or -1 on failure
Int32 (*wGuiSpriteBankAddSprite)(wGuiObject* bank,
										wTexture* texture);

wTexture* (*wGuiSpriteBankGetTexture)(wGuiObject* bank,
											 UInt32 index);

UInt32 (*wGuiSpriteBankGetTexturesCount)(wGuiObject* bank);

void (*wGuiSpriteBankRemoveAll)(wGuiObject* bank);

void (*wGuiSpriteBankDrawSprite)(wGuiObject* bank,
										UInt32 index,
										wVector2i position,
										wVector2i* clipPosition,
										wVector2i* clipSize,
										wColor4s color,
										UInt32 starttime,
										UInt32 currenttime,
										bool loop,
										bool center);

void (*wGuiSpriteBankDrawSpriteBatch)(wGuiObject* bank,
											 UInt32* indexArray,
											 UInt32 idxArrayCount,
											 wVector2i* positionArray,
											 UInt32 posArrayCount,
											 wVector2i* clipPosition,
											 wVector2i* clipSize,
											 wColor4s color,
											 UInt32 startTime,
											 UInt32 currentTime,
											 bool loop,
											 bool center);

///wGuiCheckGroup
wGuiObject* (*wGuiCheckBoxGroupCreate)(wVector2i minPos,
											  wVector2i maxPos);

Int32 (*wGuiCheckBoxGroupAddCheckBox)(wGuiObject* group,
											 wGuiObject* check);

Int32 (*wGuiCheckBoxGroupInsertCheckBox)(wGuiObject* group,
												wGuiObject* check,
												UInt32 index);

wGuiObject* (*wGuiCheckBoxGroupGetCheckBox)(wGuiObject* group,
												   UInt32 index);

Int32 (*wGuiCheckBoxGroupGetIndex)(wGuiObject* group,
										  wGuiObject* check);

Int32 (*wGuiCheckBoxGroupGetSelectedIndex)(wGuiObject* group);

bool (*wGuiCheckBoxGroupRemoveCheckBox)(wGuiObject* group,
											   UInt32 index);

void (*wGuiCheckBoxGroupRemoveAll)(wGuiObject* group);

UInt32 (*wGuiCheckBoxGroupGetSize)(wGuiObject* group);

void (*wGuiCheckBoxGroupSelectCheckBox)(wGuiObject* group,
											   Int32 index);

void (*wGuiCheckBoxGroupClearSelection)(wGuiObject* group);

void (*wGuiCheckBoxGroupSetBackgroundColor)(wGuiObject* obj,
												   wColor4s color);

void (*wGuiCheckBoxGroupDrawBackground)(wGuiObject* obj,
											   bool value);

///wGuiProgressBar///
wGuiObject* (*wGuiProgressBarCreate)(wVector2i minPos,
											wVector2i maxPos,
											bool isHorizontal);


void (*wGuiProgressBarSetPercentage)(wGuiObject* bar,
											UInt32 percent);

UInt32 (*wGuiProgressBarGetPercentage)(wGuiObject* bar);

void (*wGuiProgressBarSetDirection)(wGuiObject* bar,
										   bool isHorizontal);

bool (*wGuiProgressBarIsHorizontal)(wGuiObject* bar);

void (*wGuiProgressBarSetBorderSize)(wGuiObject* bar,
											UInt32 size);

void (*wGuiProgressBarSetSize)(wGuiObject* bar,
									  wVector2u size);

void (*wGuiProgressBarSetFillColor)(wGuiObject* bar,
										   wColor4s color);

wColor4s (*wGuiProgressBarGetFillColor)(wGuiObject* bar);

void (*wGuiProgressBarSetTextColor)(wGuiObject* bar,
										   wColor4s color);

void (*wGuiProgressBarShowText)(wGuiObject* bar,
									   bool value);

bool (*wGuiProgressBarIsShowText)(wGuiObject* bar);

void (*wGuiProgressBarSetFillTexture)(wGuiObject* bar,
											 wTexture* tex);

void (*wGuiProgressBarSetBackTexture)(wGuiObject* bar,
											 wTexture* tex);

void (*wGuiProgressBarSetFont)(wGuiObject* bar,
									  wFont* font);

void (*wGuiProgressBarSetBackgroundColor)(wGuiObject* bar,
												 wColor4s color);

void (*wGuiProgressBarSetBorderColor)(wGuiObject* obj,
											 wColor4s color);

///wGuiTextArea///
wGuiObject* (*wGuiTextAreaCreate)(wVector2i minPos,
										 wVector2i maxPos,
										 Int32 maxLines);

void (*wGuiTextAreaSetBorderSize)(wGuiObject* tarea,
										 UInt32 size);

void (*wGuiTextAreaSetAutoScroll)(wGuiObject* tarea,
										 bool value);

void (*wGuiTextAreaSetPadding)(wGuiObject* tarea,
									  UInt32 padding);

void (*wGuiTextAreaSetBackTexture)(wGuiObject* tarea,
										  wTexture* tex);

void (*wGuiTextAreaSetWrapping)(wGuiObject* tarea,
									   bool value);

void (*wGuiTextAreaSetFont)(wGuiObject* tarea,
								   wFont* font);

void (*wGuiTextAreaAddLine)(wGuiObject* tarea,
								   const wchar_t* text,
								   UInt32 lifeTime,
								   wColor4s color,
								   wTexture* icon,
								   Int32 iconMode);

void (*wGuiTextAreaRemoveAll)(wGuiObject* tarea);

void (*wGuiTextAreaSetBackgroundColor)(wGuiObject* tarea,
											  wColor4s color);

void (*wGuiTextAreaSetBorderColor)(wGuiObject* tarea,
										  wColor4s color);

///wGuiCEditor///
wGuiObject* (*wGuiCEditorCreate)(const wchar_t* wcptrText,
										wVector2i minPos,
										wVector2i maxPos,
										bool border);

void (*wGuiCEditorSetHScrollVisible)(wGuiObject* box,
											bool value);

void (*wGuiCEditorSetText)(wGuiObject* box,
								  const wchar_t* text);

void (*wGuiCEditorSetColors)(wGuiObject* box,
									wColor4s backColor,
									wColor4s lineColor,
									wColor4s textColor);

void (*wGuiCEditorSetLinesCountVisible)(wGuiObject* box,
											   bool value);

bool (*wGuiCEditorIsLinesCountVisible)(wGuiObject* box);

void (*wGuiCEditorSetElementText)(wGuiObject* box,
										 UInt32 index,
										 const wchar_t* text);

void (*wGuiCEditorSetSelectionColors)(wGuiObject* box,
											 wColor4s backColor,
											 wColor4s textColor,
											 wColor4s back2Color);

void (*wGuiCEditorRemoveText)(wGuiObject* box);

void (*wGuiCEditorAddKeyword)(wGuiObject* box,
									 const char* word,
									 wColor4s color,
									 bool matchCase);

void (*wGuiCEditorAddLineKeyword)(wGuiObject* box,
										 const char* word,
										 wColor4s color,
										 bool matchCase);

void (*wGuiCEditorAddGroupKeyword)(wGuiObject* box,
										  const char* word,
										  const char* endKeyword,
										  wColor4s color,
										  bool matchCase);

void (*wGuiCEditorBoxAddKeywordInfo)(wGuiObject* box,
											Int32 size,
											Int32 type);

void (*wGuiCEditorBoxRemoveAllKeywords)(wGuiObject* box);

void (*wGuiCEditorBoxAddCppKeywords)(wGuiObject* box,
											wColor4s key,
											wColor4s string,
											wColor4s comment);

void (*wGuiCEditorAddLuaKeywords)(wGuiObject* box,
										 wColor4s key,
										 wColor4s string,
										 wColor4s comment);

void (*wGuiCEditorAddFbKeywords)(wGuiObject* box,
										wColor4s key,
										wColor4s string,
										wColor4s comment);

void (*wGuiCEditorReplaceText)(wGuiObject* box,
									  Int32 start,
									  Int32 end,
									  const wchar_t* text);

void (*wGuiCEditorPressReturn)(wGuiObject* box);

void (*wGuiCEditorAddText)(wGuiObject* box,
								  const wchar_t* addText);

const wchar_t* (*wGuiCEditorGetText)(wGuiObject* box);

void (*wGuiCEditorSetLineToggleVisible)(wGuiObject* box,
											   bool value);

void (*wGuiCEditorSetContextMenuText)(wGuiObject* box,
											 const wchar_t* cut_text,
											 const wchar_t* copy_text,
											 const wchar_t* paste_text,
											 const wchar_t* del_text,
											 const wchar_t* redo_text,
											 const wchar_t* undo_text,
											 const wchar_t* btn_text);

void (*wGuiCEditorBoxCopy)(wGuiObject* box);

void (*wGuiCEditorCut)(wGuiObject* box);

void (*wGuiCEditorPaste)(wGuiObject* box);

void (*wGuiCEditorUndo)(wGuiObject* box);

void (*wGuiCEditorRedo)(wGuiObject* box);

wFont* (*wGuiCEditorGetOverrideFont)(wGuiObject* obj);

wFont* (*wGuiCEditorGetActiveFont)(wGuiObject* obj);

void (*wGuiCEditorEnableOverrideColor)(wGuiObject* obj,
											  bool value);

bool (*wGuiCEditorIsOverrideColor)(wGuiObject* obj);

void (*wGuiCEditorSetOverrideColor)(wGuiObject* obj,
										   wColor4s color);

wColor4s (*wGuiCEditorGetOverrideColor)(wGuiObject* obj);

void (*wGuiCEditorSetDrawBackground)(wGuiObject* obj,
											bool value);

bool (*wGuiCEditorIsDrawBackGround)(wGuiObject* obj);

void (*wGuiCEditorSetDrawBorder)(wGuiObject* obj,
										bool value);

bool (*wGuiCEditorIsDrawBorder)(wGuiObject* obj);

void (*wGuiCEditorSetTextAlignment)(wGuiObject* obj,
										   wGuiAlignment Horizontalvalue,
										   wGuiAlignment Verticalvalue);

 void (*wGuiCEditorSetWordWrap)(wGuiObject* obj,
									   bool value);

bool (*wGuiCEditorIsWordWrap)(wGuiObject* obj);

void (*wGuiCEditorSetBackgroundColor)(wGuiObject* obj,
											 wColor4s color);

#ifdef __cplusplus
}
#endif // __cplusplus

void wLibraryLoad()
{
	ws3dHandle = load_library(wLIBRARY_NAME);
	if (!ws3dHandle)
	{
		printf("Error load '%s'!\n", wLIBRARY_NAME);
		return;
	}

	*(void**) (&w2dDrawLine) = load_function(ws3dHandle, "w2dDrawLine");
	*(void**) (&w2dDrawPixel) = load_function(ws3dHandle, "w2dDrawPixel");
	*(void**) (&w2dDrawPolygon) = load_function(ws3dHandle, "w2dDrawPolygon");
	*(void**) (&w2dDrawRect) = load_function(ws3dHandle, "w2dDrawRect");
	*(void**) (&w2dDrawRectOutline) = load_function(ws3dHandle, "w2dDrawRectOutline");
	*(void**) (&w2dDrawRectWithGradient) = load_function(ws3dHandle, "w2dDrawRectWithGradient");
	*(void**) (&w3dDrawBox) = load_function(ws3dHandle, "w3dDrawBox");
	*(void**) (&w3dDrawLine) = load_function(ws3dHandle, "w3dDrawLine");
	*(void**) (&w3dDrawTriangle) = load_function(ws3dHandle, "w3dDrawTriangle");
	*(void**) (&wAnimatorCollisionResponseCreate) = load_function(ws3dHandle, "wAnimatorCollisionResponseCreate");
	*(void**) (&wAnimatorCollisionResponseGetParameters) = load_function(ws3dHandle, "wAnimatorCollisionResponseGetParameters");
	*(void**) (&wAnimatorCollisionResponseSetParameters) = load_function(ws3dHandle, "wAnimatorCollisionResponseSetParameters");
	*(void**) (&wAnimatorDeletingCreate) = load_function(ws3dHandle, "wAnimatorDeletingCreate");
	*(void**) (&wAnimatorDestroy) = load_function(ws3dHandle, "wAnimatorDestroy");
	*(void**) (&wAnimatorFadingCreate) = load_function(ws3dHandle, "wAnimatorFadingCreate");
	*(void**) (&wAnimatorFlyingCircleCreate) = load_function(ws3dHandle, "wAnimatorFlyingCircleCreate");
	*(void**) (&wAnimatorFlyingStraightCreate) = load_function(ws3dHandle, "wAnimatorFlyingStraightCreate");
	*(void**) (&wAnimatorFollowCameraCreate) = load_function(ws3dHandle, "wAnimatorFollowCameraCreate");
	*(void**) (&wAnimatorRotationCreate) = load_function(ws3dHandle, "wAnimatorRotationCreate");
	*(void**) (&wAnimatorSplineCreate) = load_function(ws3dHandle, "wAnimatorSplineCreate");
	*(void**) (&wBeamCreate) = load_function(ws3dHandle, "wBeamCreate");
	*(void**) (&wBeamSetPosition) = load_function(ws3dHandle, "wBeamSetPosition");
	*(void**) (&wBeamSetSize) = load_function(ws3dHandle, "wBeamSetSize");
	*(void**) (&wBillboardCreate) = load_function(ws3dHandle, "wBillboardCreate");
	*(void**) (&wBillboardCreateText) = load_function(ws3dHandle, "wBillboardCreateText");
	*(void**) (&wBillboardGetEnabledAxis) = load_function(ws3dHandle, "wBillboardGetEnabledAxis");
	*(void**) (&wBillboardGroupAddElement) = load_function(ws3dHandle, "wBillboardGroupAddElement");
	*(void**) (&wBillboardGroupAddElementByAxis) = load_function(ws3dHandle, "wBillboardGroupAddElementByAxis");
	*(void**) (&wBillboardGroupCreate) = load_function(ws3dHandle, "wBillboardGroupCreate");
	*(void**) (&wBillboardGroupGetFirstElement) = load_function(ws3dHandle, "wBillboardGroupGetFirstElement");
	*(void**) (&wBillboardGroupGetMeshBuffer) = load_function(ws3dHandle, "wBillboardGroupGetMeshBuffer");
	*(void**) (&wBillboardGroupGetSize) = load_function(ws3dHandle, "wBillboardGroupGetSize");
	*(void**) (&wBillboardGroupRemoveElement) = load_function(ws3dHandle, "wBillboardGroupRemoveElement");
	*(void**) (&wBillboardGroupResetShadows) = load_function(ws3dHandle, "wBillboardGroupResetShadows");
	*(void**) (&wBillboardGroupSetShadows) = load_function(ws3dHandle, "wBillboardGroupSetShadows");
	*(void**) (&wBillboardGroupUpdateForce) = load_function(ws3dHandle, "wBillboardGroupUpdateForce");
	*(void**) (&wBillboardSetColor) = load_function(ws3dHandle, "wBillboardSetColor");
	*(void**) (&wBillboardSetEnabledAxis) = load_function(ws3dHandle, "wBillboardSetEnabledAxis");
	*(void**) (&wBillboardSetSize) = load_function(ws3dHandle, "wBillboardSetSize");
	*(void**) (&wBoltCreate) = load_function(ws3dHandle, "wBoltCreate");
	*(void**) (&wBoltSetProperties) = load_function(ws3dHandle, "wBoltSetProperties");
	*(void**) (&wBspCreateFromMesh) = load_function(ws3dHandle, "wBspCreateFromMesh");
	*(void**) (&wBspGetEntityIndexByName) = load_function(ws3dHandle, "wBspGetEntityIndexByName");
	*(void**) (&wBspGetEntityList) = load_function(ws3dHandle, "wBspGetEntityList");
	*(void**) (&wBspGetEntityListSize) = load_function(ws3dHandle, "wBspGetEntityListSize");
	*(void**) (&wBspGetEntityMeshFromBrush) = load_function(ws3dHandle, "wBspGetEntityMeshFromBrush");
	*(void**) (&wBspGetEntityNameByIndex) = load_function(ws3dHandle, "wBspGetEntityNameByIndex");
	*(void**) (&wBspGetVarGroupByIndex) = load_function(ws3dHandle, "wBspGetVarGroupByIndex");
	*(void**) (&wBspGetVarGroupSize) = load_function(ws3dHandle, "wBspGetVarGroupSize");
	*(void**) (&wBspGetVarGroupValueAsFloat) = load_function(ws3dHandle, "wBspGetVarGroupValueAsFloat");
	*(void**) (&wBspGetVarGroupValueAsString) = load_function(ws3dHandle, "wBspGetVarGroupValueAsString");
	*(void**) (&wBspGetVarGroupValueAsVec) = load_function(ws3dHandle, "wBspGetVarGroupValueAsVec");
	*(void**) (&wCameraCreate) = load_function(ws3dHandle, "wCameraCreate");
	*(void**) (&wCameraGetFov) = load_function(ws3dHandle, "wCameraGetFov");
	*(void**) (&wCameraGetOrientation) = load_function(ws3dHandle, "wCameraGetOrientation");
	*(void**) (&wCameraGetTarget) = load_function(ws3dHandle, "wCameraGetTarget");
	*(void**) (&wCameraGetUpDirection) = load_function(ws3dHandle, "wCameraGetUpDirection");
	*(void**) (&wCameraIsInputEnabled) = load_function(ws3dHandle, "wCameraIsInputEnabled");
	*(void**) (&wCameraRevolve) = load_function(ws3dHandle, "wCameraRevolve");
	*(void**) (&wCameraSetActive) = load_function(ws3dHandle, "wCameraSetActive");
	*(void**) (&wCameraSetAspectRatio) = load_function(ws3dHandle, "wCameraSetAspectRatio");
	*(void**) (&wCameraSetClipDistance) = load_function(ws3dHandle, "wCameraSetClipDistance");
	*(void**) (&wCameraSetCollisionWithScene) = load_function(ws3dHandle, "wCameraSetCollisionWithScene");
	*(void**) (&wCameraSetFov) = load_function(ws3dHandle, "wCameraSetFov");
	*(void**) (&wCameraSetInputEnabled) = load_function(ws3dHandle, "wCameraSetInputEnabled");
	*(void**) (&wCameraSetOrthogonal) = load_function(ws3dHandle, "wCameraSetOrthogonal");
	*(void**) (&wCameraSetTarget) = load_function(ws3dHandle, "wCameraSetTarget");
	*(void**) (&wCameraSetUpAtRightAngle) = load_function(ws3dHandle, "wCameraSetUpAtRightAngle");
	*(void**) (&wCameraSetUpDirection) = load_function(ws3dHandle, "wCameraSetUpDirection");
	*(void**) (&wCloudsCreate) = load_function(ws3dHandle, "wCloudsCreate");
	*(void**) (&wCollisionCreateFromBatchingMesh) = load_function(ws3dHandle, "wCollisionCreateFromBatchingMesh");
	*(void**) (&wCollisionCreateFromBox) = load_function(ws3dHandle, "wCollisionCreateFromBox");
	*(void**) (&wCollisionCreateFromMesh) = load_function(ws3dHandle, "wCollisionCreateFromMesh");
	*(void**) (&wCollisionCreateFromMeshBuffer) = load_function(ws3dHandle, "wCollisionCreateFromMeshBuffer");
	*(void**) (&wCollisionCreateFromOctreeMesh) = load_function(ws3dHandle, "wCollisionCreateFromOctreeMesh");
	*(void**) (&wCollisionCreateFromStaticMesh) = load_function(ws3dHandle, "wCollisionCreateFromStaticMesh");
	*(void**) (&wCollisionCreateFromTerrain) = load_function(ws3dHandle, "wCollisionCreateFromTerrain");
	*(void**) (&wCollisionGet2dPositionFromScreen) = load_function(ws3dHandle, "wCollisionGet2dPositionFromScreen");
	*(void**) (&wCollisionGet3dPositionFromScreen) = load_function(ws3dHandle, "wCollisionGet3dPositionFromScreen");
	*(void**) (&wCollisionGetNodeAndPointFromRay) = load_function(ws3dHandle, "wCollisionGetNodeAndPointFromRay");
	*(void**) (&wCollisionGetNodeChildFromPoint) = load_function(ws3dHandle, "wCollisionGetNodeChildFromPoint");
	*(void**) (&wCollisionGetNodeChildFromRay) = load_function(ws3dHandle, "wCollisionGetNodeChildFromRay");
	*(void**) (&wCollisionGetNodeFromCamera) = load_function(ws3dHandle, "wCollisionGetNodeFromCamera");
	*(void**) (&wCollisionGetNodeFromRay) = load_function(ws3dHandle, "wCollisionGetNodeFromRay");
	*(void**) (&wCollisionGetNodeFromScreen) = load_function(ws3dHandle, "wCollisionGetNodeFromScreen");
	*(void**) (&wCollisionGetPointFromRay) = load_function(ws3dHandle, "wCollisionGetPointFromRay");
	*(void**) (&wCollisionGetRayFromScreenCoord) = load_function(ws3dHandle, "wCollisionGetRayFromScreenCoord");
	*(void**) (&wCollisionGetResultPosition) = load_function(ws3dHandle, "wCollisionGetResultPosition");
	*(void**) (&wCollisionGetScreenCoordFrom3dPosition) = load_function(ws3dHandle, "wCollisionGetScreenCoordFrom3dPosition");
	*(void**) (&wCollisionGroupAddCollision) = load_function(ws3dHandle, "wCollisionGroupAddCollision");
	*(void**) (&wCollisionGroupCreate) = load_function(ws3dHandle, "wCollisionGroupCreate");
	*(void**) (&wCollisionGroupRemoveAll) = load_function(ws3dHandle, "wCollisionGroupRemoveAll");
	*(void**) (&wCollisionGroupRemoveCollision) = load_function(ws3dHandle, "wCollisionGroupRemoveCollision");
	*(void**) (&wConsoleResetColors) = load_function(ws3dHandle, "wConsoleResetColors");
	*(void**) (&wConsoleSaveDefaultColors) = load_function(ws3dHandle, "wConsoleSaveDefaultColors");
	*(void**) (&wConsoleSetBackColor) = load_function(ws3dHandle, "wConsoleSetBackColor");
	*(void**) (&wConsoleSetFontColor) = load_function(ws3dHandle, "wConsoleSetFontColor");
	*(void**) (&wDecalCreateFromPoint) = load_function(ws3dHandle, "wDecalCreateFromPoint");
	*(void**) (&wDecalCreateFromRay) = load_function(ws3dHandle, "wDecalCreateFromRay");
	*(void**) (&wDecalGetLifeTime) = load_function(ws3dHandle, "wDecalGetLifeTime");
	*(void**) (&wDecalGetMaterial) = load_function(ws3dHandle, "wDecalGetMaterial");
	*(void**) (&wDecalGetMaxVisibleDistance) = load_function(ws3dHandle, "wDecalGetMaxVisibleDistance");
	*(void**) (&wDecalSetFadeOutParams) = load_function(ws3dHandle, "wDecalSetFadeOutParams");
	*(void**) (&wDecalSetLifeTime) = load_function(ws3dHandle, "wDecalSetLifeTime");
	*(void**) (&wDecalSetMaxVisibleDistance) = load_function(ws3dHandle, "wDecalSetMaxVisibleDistance");
	*(void**) (&wDecalsClear) = load_function(ws3dHandle, "wDecalsClear");
	*(void**) (&wDecalsCombineAll) = load_function(ws3dHandle, "wDecalsCombineAll");
	*(void**) (&wDecalsDestroyAll) = load_function(ws3dHandle, "wDecalsDestroyAll");
	*(void**) (&wDecalsGetCount) = load_function(ws3dHandle, "wDecalsGetCount");
	*(void**) (&wDisplayGetCurrentDepth) = load_function(ws3dHandle, "wDisplayGetCurrentDepth");
	*(void**) (&wDisplayGetCurrentResolution) = load_function(ws3dHandle, "wDisplayGetCurrentResolution");
	*(void**) (&wDisplayGetGammaRamp) = load_function(ws3dHandle, "wDisplayGetGammaRamp");
	*(void**) (&wDisplayGetVendor) = load_function(ws3dHandle, "wDisplayGetVendor");
	*(void**) (&wDisplayModeGetDepth) = load_function(ws3dHandle, "wDisplayModeGetDepth");
	*(void**) (&wDisplayModeGetResolution) = load_function(ws3dHandle, "wDisplayModeGetResolution");
	*(void**) (&wDisplayModesGetCount) = load_function(ws3dHandle, "wDisplayModesGetCount");
	*(void**) (&wDisplaySetDepth) = load_function(ws3dHandle, "wDisplaySetDepth");
	*(void**) (&wDisplaySetGammaRamp) = load_function(ws3dHandle, "wDisplaySetGammaRamp");
	*(void**) (&wEngineCloseByEsc) = load_function(ws3dHandle, "wEngineCloseByEsc");
	*(void**) (&wEngineDisableFeature) = load_function(ws3dHandle, "wEngineDisableFeature");
	*(void**) (&wEngineGet2dMaterial) = load_function(ws3dHandle, "wEngineGet2dMaterial");
	*(void**) (&wEngineGetClipboardText) = load_function(ws3dHandle, "wEngineGetClipboardText");
	*(void**) (&wEngineGetFPS) = load_function(ws3dHandle, "wEngineGetFPS");
	*(void**) (&wEngineGetGlobalMaterial) = load_function(ws3dHandle, "wEngineGetGlobalMaterial");
	*(void**) (&wEngineIsQueryFeature) = load_function(ws3dHandle, "wEngineIsQueryFeature");
	*(void**) (&wEngineLoadCreationParameters) = load_function(ws3dHandle, "wEngineLoadCreationParameters");
	*(void**) (&wEngineRunning) = load_function(ws3dHandle, "wEngineRunning");
	*(void**) (&wEngineSaveCreationParameters) = load_function(ws3dHandle, "wEngineSaveCreationParameters");
	*(void**) (&wEngineSet2dMaterial) = load_function(ws3dHandle, "wEngineSet2dMaterial");
	*(void**) (&wEngineSetClipboardText) = load_function(ws3dHandle, "wEngineSetClipboardText");
	*(void**) (&wEngineSetFPS) = load_function(ws3dHandle, "wEngineSetFPS");
	*(void**) (&wEngineSetTransparentZWrite) = load_function(ws3dHandle, "wEngineSetTransparentZWrite");
	*(void**) (&wEngineSetViewPort) = load_function(ws3dHandle, "wEngineSetViewPort");
	*(void**) (&wEngineShowLogo) = load_function(ws3dHandle, "wEngineShowLogo");
	*(void**) (&wEngineSleep) = load_function(ws3dHandle, "wEngineSleep");
	*(void**) (&wEngineStart) = load_function(ws3dHandle, "wEngineStart");
	*(void**) (&wEngineStartAdvanced) = load_function(ws3dHandle, "wEngineStartAdvanced");
	*(void**) (&wEngineStartWithGui) = load_function(ws3dHandle, "wEngineStartWithGui");
	*(void**) (&wEngineStop) = load_function(ws3dHandle, "wEngineStop");
	*(void**) (&wEngineYield) = load_function(ws3dHandle, "wEngineYield");
	*(void**) (&wFileAddArchive) = load_function(ws3dHandle, "wFileAddArchive");
	*(void**) (&wFileAddDirectory) = load_function(ws3dHandle, "wFileAddDirectory");
	*(void**) (&wFileAddPakArchive) = load_function(ws3dHandle, "wFileAddPakArchive");
	*(void**) (&wFileAddZipArchive) = load_function(ws3dHandle, "wFileAddZipArchive");
	*(void**) (&wFileClose) = load_function(ws3dHandle, "wFileClose");
	*(void**) (&wFileCreateForWrite) = load_function(ws3dHandle, "wFileCreateForWrite");
	*(void**) (&wFileGetAbsolutePath) = load_function(ws3dHandle, "wFileGetAbsolutePath");
	*(void**) (&wFileGetBaseName) = load_function(ws3dHandle, "wFileGetBaseName");
	*(void**) (&wFileGetDirectory) = load_function(ws3dHandle, "wFileGetDirectory");
	*(void**) (&wFileGetName) = load_function(ws3dHandle, "wFileGetName");
	*(void**) (&wFileGetPos) = load_function(ws3dHandle, "wFileGetPos");
	*(void**) (&wFileGetRelativePath) = load_function(ws3dHandle, "wFileGetRelativePath");
	*(void**) (&wFileGetSize) = load_function(ws3dHandle, "wFileGetSize");
	*(void**) (&wFileGetWorkingDirectory) = load_function(ws3dHandle, "wFileGetWorkingDirectory");
	*(void**) (&wFileIsExist) = load_function(ws3dHandle, "wFileIsExist");
	*(void**) (&wFileOpenForRead) = load_function(ws3dHandle, "wFileOpenForRead");
	*(void**) (&wFileRead) = load_function(ws3dHandle, "wFileRead");
	*(void**) (&wFileSeek) = load_function(ws3dHandle, "wFileSeek");
	*(void**) (&wFileSetWorkingDirectory) = load_function(ws3dHandle, "wFileSetWorkingDirectory");
	*(void**) (&wFileWrite) = load_function(ws3dHandle, "wFileWrite");
	*(void**) (&wFontAddToFont) = load_function(ws3dHandle, "wFontAddToFont");
	*(void**) (&wFontDestroy) = load_function(ws3dHandle, "wFontDestroy");
	*(void**) (&wFontDraw) = load_function(ws3dHandle, "wFontDraw");
	*(void**) (&wFontDrawAsTTF) = load_function(ws3dHandle, "wFontDrawAsTTF");
	*(void**) (&wFontGetCharacterFromPos) = load_function(ws3dHandle, "wFontGetCharacterFromPos");
	*(void**) (&wFontGetDefault) = load_function(ws3dHandle, "wFontGetDefault");
	*(void**) (&wFontGetKerningSize) = load_function(ws3dHandle, "wFontGetKerningSize");
	*(void**) (&wFontGetTextSize) = load_function(ws3dHandle, "wFontGetTextSize");
	*(void**) (&wFontLoad) = load_function(ws3dHandle, "wFontLoad");
	*(void**) (&wFontLoadFromTTF) = load_function(ws3dHandle, "wFontLoadFromTTF");
	*(void**) (&wFontSetInvisibleCharacters) = load_function(ws3dHandle, "wFontSetInvisibleCharacters");
	*(void**) (&wFontSetKerningSize) = load_function(ws3dHandle, "wFontSetKerningSize");
	*(void**) (&wFpsCameraCreate) = load_function(ws3dHandle, "wFpsCameraCreate");
	*(void**) (&wFpsCameraGetMaxVerticalAngle) = load_function(ws3dHandle, "wFpsCameraGetMaxVerticalAngle");
	*(void**) (&wFpsCameraGetRotationSpeed) = load_function(ws3dHandle, "wFpsCameraGetRotationSpeed");
	*(void**) (&wFpsCameraGetSpeed) = load_function(ws3dHandle, "wFpsCameraGetSpeed");
	*(void**) (&wFpsCameraSetInvertMouse) = load_function(ws3dHandle, "wFpsCameraSetInvertMouse");
	*(void**) (&wFpsCameraSetKeyMap) = load_function(ws3dHandle, "wFpsCameraSetKeyMap");
	*(void**) (&wFpsCameraSetMaxVerticalAngle) = load_function(ws3dHandle, "wFpsCameraSetMaxVerticalAngle");
	*(void**) (&wFpsCameraSetRotationSpeed) = load_function(ws3dHandle, "wFpsCameraSetRotationSpeed");
	*(void**) (&wFpsCameraSetSpeed) = load_function(ws3dHandle, "wFpsCameraSetSpeed");
	*(void**) (&wFpsCameraSetVerticalMovement) = load_function(ws3dHandle, "wFpsCameraSetVerticalMovement");
	*(void**) (&wGrassCreate) = load_function(ws3dHandle, "wGrassCreate");
	*(void**) (&wGrassGetDrawingCount) = load_function(ws3dHandle, "wGrassGetDrawingCount");
	*(void**) (&wGrassSetDensity) = load_function(ws3dHandle, "wGrassSetDensity");
	*(void**) (&wGrassSetWind) = load_function(ws3dHandle, "wGrassSetWind");
	*(void**) (&wGuiButtonCreate) = load_function(ws3dHandle, "wGuiButtonCreate");
	*(void**) (&wGuiButtonEnableScaleImage) = load_function(ws3dHandle, "wGuiButtonEnableScaleImage");
	*(void**) (&wGuiButtonGetActiveFont) = load_function(ws3dHandle, "wGuiButtonGetActiveFont");
	*(void**) (&wGuiButtonGetOverrideFont) = load_function(ws3dHandle, "wGuiButtonGetOverrideFont");
	*(void**) (&wGuiButtonGroupAddButton) = load_function(ws3dHandle, "wGuiButtonGroupAddButton");
	*(void**) (&wGuiButtonGroupClearSelection) = load_function(ws3dHandle, "wGuiButtonGroupClearSelection");
	*(void**) (&wGuiButtonGroupCreate) = load_function(ws3dHandle, "wGuiButtonGroupCreate");
	*(void**) (&wGuiButtonGroupDrawBackground) = load_function(ws3dHandle, "wGuiButtonGroupDrawBackground");
	*(void**) (&wGuiButtonGroupGetButton) = load_function(ws3dHandle, "wGuiButtonGroupGetButton");
	*(void**) (&wGuiButtonGroupGetSelectedIndex) = load_function(ws3dHandle, "wGuiButtonGroupGetSelectedIndex");
	*(void**) (&wGuiButtonGroupGetSize) = load_function(ws3dHandle, "wGuiButtonGroupGetSize");
	*(void**) (&wGuiButtonGroupInsertButton) = load_function(ws3dHandle, "wGuiButtonGroupInsertButton");
	*(void**) (&wGuiButtonGroupRemoveAll) = load_function(ws3dHandle, "wGuiButtonGroupRemoveAll");
	*(void**) (&wGuiButtonGroupRemoveButton) = load_function(ws3dHandle, "wGuiButtonGroupRemoveButton");
	*(void**) (&wGuiButtonGroupSetBackgroundColor) = load_function(ws3dHandle, "wGuiButtonGroupSetBackgroundColor");
	*(void**) (&wGuiButtonGroupSetSelectedIndex) = load_function(ws3dHandle, "wGuiButtonGroupSetSelectedIndex");
	*(void**) (&wGuiButtonIsDrawBorder) = load_function(ws3dHandle, "wGuiButtonIsDrawBorder");
	*(void**) (&wGuiButtonIsPressed) = load_function(ws3dHandle, "wGuiButtonIsPressed");
	*(void**) (&wGuiButtonIsPushed) = load_function(ws3dHandle, "wGuiButtonIsPushed");
	*(void**) (&wGuiButtonIsScaledImage) = load_function(ws3dHandle, "wGuiButtonIsScaledImage");
	*(void**) (&wGuiButtonIsUsedAlphaChannel) = load_function(ws3dHandle, "wGuiButtonIsUsedAlphaChannel");
	*(void**) (&wGuiButtonSetDrawBorder) = load_function(ws3dHandle, "wGuiButtonSetDrawBorder");
	*(void**) (&wGuiButtonSetImage) = load_function(ws3dHandle, "wGuiButtonSetImage");
	*(void**) (&wGuiButtonSetImageFromRect) = load_function(ws3dHandle, "wGuiButtonSetImageFromRect");
	*(void**) (&wGuiButtonSetOverrideFont) = load_function(ws3dHandle, "wGuiButtonSetOverrideFont");
	*(void**) (&wGuiButtonSetPressed) = load_function(ws3dHandle, "wGuiButtonSetPressed");
	*(void**) (&wGuiButtonSetPressedImage) = load_function(ws3dHandle, "wGuiButtonSetPressedImage");
	*(void**) (&wGuiButtonSetPressedImageFromRect) = load_function(ws3dHandle, "wGuiButtonSetPressedImageFromRect");
	*(void**) (&wGuiButtonSetPush) = load_function(ws3dHandle, "wGuiButtonSetPush");
	*(void**) (&wGuiButtonSetSprite) = load_function(ws3dHandle, "wGuiButtonSetSprite");
	*(void**) (&wGuiButtonSetSpriteBank) = load_function(ws3dHandle, "wGuiButtonSetSpriteBank");
	*(void**) (&wGuiButtonUseAlphaChannel) = load_function(ws3dHandle, "wGuiButtonUseAlphaChannel");
	*(void**) (&wGuiCEditorAddFbKeywords) = load_function(ws3dHandle, "wGuiCEditorAddFbKeywords");
	*(void**) (&wGuiCEditorAddGroupKeyword) = load_function(ws3dHandle, "wGuiCEditorAddGroupKeyword");
	*(void**) (&wGuiCEditorAddKeyword) = load_function(ws3dHandle, "wGuiCEditorAddKeyword");
	*(void**) (&wGuiCEditorAddLineKeyword) = load_function(ws3dHandle, "wGuiCEditorAddLineKeyword");
	*(void**) (&wGuiCEditorAddLuaKeywords) = load_function(ws3dHandle, "wGuiCEditorAddLuaKeywords");
	*(void**) (&wGuiCEditorAddText) = load_function(ws3dHandle, "wGuiCEditorAddText");
	*(void**) (&wGuiCEditorBoxAddCppKeywords) = load_function(ws3dHandle, "wGuiCEditorBoxAddCppKeywords");
	*(void**) (&wGuiCEditorBoxAddKeywordInfo) = load_function(ws3dHandle, "wGuiCEditorBoxAddKeywordInfo");
	*(void**) (&wGuiCEditorBoxCopy) = load_function(ws3dHandle, "wGuiCEditorBoxCopy");
	*(void**) (&wGuiCEditorBoxRemoveAllKeywords) = load_function(ws3dHandle, "wGuiCEditorBoxRemoveAllKeywords");
	*(void**) (&wGuiCEditorCreate) = load_function(ws3dHandle, "wGuiCEditorCreate");
	*(void**) (&wGuiCEditorCut) = load_function(ws3dHandle, "wGuiCEditorCut");
	*(void**) (&wGuiCEditorEnableOverrideColor) = load_function(ws3dHandle, "wGuiCEditorEnableOverrideColor");
	*(void**) (&wGuiCEditorGetActiveFont) = load_function(ws3dHandle, "wGuiCEditorGetActiveFont");
	*(void**) (&wGuiCEditorGetOverrideColor) = load_function(ws3dHandle, "wGuiCEditorGetOverrideColor");
	*(void**) (&wGuiCEditorGetOverrideFont) = load_function(ws3dHandle, "wGuiCEditorGetOverrideFont");
	*(void**) (&wGuiCEditorGetText) = load_function(ws3dHandle, "wGuiCEditorGetText");
	*(void**) (&wGuiCEditorIsDrawBackGround) = load_function(ws3dHandle, "wGuiCEditorIsDrawBackGround");
	*(void**) (&wGuiCEditorIsDrawBorder) = load_function(ws3dHandle, "wGuiCEditorIsDrawBorder");
	*(void**) (&wGuiCEditorIsLinesCountVisible) = load_function(ws3dHandle, "wGuiCEditorIsLinesCountVisible");
	*(void**) (&wGuiCEditorIsOverrideColor) = load_function(ws3dHandle, "wGuiCEditorIsOverrideColor");
	*(void**) (&wGuiCEditorIsWordWrap) = load_function(ws3dHandle, "wGuiCEditorIsWordWrap");
	*(void**) (&wGuiCEditorPaste) = load_function(ws3dHandle, "wGuiCEditorPaste");
	*(void**) (&wGuiCEditorPressReturn) = load_function(ws3dHandle, "wGuiCEditorPressReturn");
	*(void**) (&wGuiCEditorRedo) = load_function(ws3dHandle, "wGuiCEditorRedo");
	*(void**) (&wGuiCEditorRemoveText) = load_function(ws3dHandle, "wGuiCEditorRemoveText");
	*(void**) (&wGuiCEditorReplaceText) = load_function(ws3dHandle, "wGuiCEditorReplaceText");
	*(void**) (&wGuiCEditorSetBackgroundColor) = load_function(ws3dHandle, "wGuiCEditorSetBackgroundColor");
	*(void**) (&wGuiCEditorSetColors) = load_function(ws3dHandle, "wGuiCEditorSetColors");
	*(void**) (&wGuiCEditorSetContextMenuText) = load_function(ws3dHandle, "wGuiCEditorSetContextMenuText");
	*(void**) (&wGuiCEditorSetDrawBackground) = load_function(ws3dHandle, "wGuiCEditorSetDrawBackground");
	*(void**) (&wGuiCEditorSetDrawBorder) = load_function(ws3dHandle, "wGuiCEditorSetDrawBorder");
	*(void**) (&wGuiCEditorSetElementText) = load_function(ws3dHandle, "wGuiCEditorSetElementText");
	*(void**) (&wGuiCEditorSetHScrollVisible) = load_function(ws3dHandle, "wGuiCEditorSetHScrollVisible");
	*(void**) (&wGuiCEditorSetLineToggleVisible) = load_function(ws3dHandle, "wGuiCEditorSetLineToggleVisible");
	*(void**) (&wGuiCEditorSetLinesCountVisible) = load_function(ws3dHandle, "wGuiCEditorSetLinesCountVisible");
	*(void**) (&wGuiCEditorSetOverrideColor) = load_function(ws3dHandle, "wGuiCEditorSetOverrideColor");
	*(void**) (&wGuiCEditorSetSelectionColors) = load_function(ws3dHandle, "wGuiCEditorSetSelectionColors");
	*(void**) (&wGuiCEditorSetText) = load_function(ws3dHandle, "wGuiCEditorSetText");
	*(void**) (&wGuiCEditorSetTextAlignment) = load_function(ws3dHandle, "wGuiCEditorSetTextAlignment");
	*(void**) (&wGuiCEditorSetWordWrap) = load_function(ws3dHandle, "wGuiCEditorSetWordWrap");
	*(void**) (&wGuiCEditorUndo) = load_function(ws3dHandle, "wGuiCEditorUndo");
	*(void**) (&wGuiCheckBoxCheck) = load_function(ws3dHandle, "wGuiCheckBoxCheck");
	*(void**) (&wGuiCheckBoxCreate) = load_function(ws3dHandle, "wGuiCheckBoxCreate");
	*(void**) (&wGuiCheckBoxGroupAddCheckBox) = load_function(ws3dHandle, "wGuiCheckBoxGroupAddCheckBox");
	*(void**) (&wGuiCheckBoxGroupClearSelection) = load_function(ws3dHandle, "wGuiCheckBoxGroupClearSelection");
	*(void**) (&wGuiCheckBoxGroupCreate) = load_function(ws3dHandle, "wGuiCheckBoxGroupCreate");
	*(void**) (&wGuiCheckBoxGroupDrawBackground) = load_function(ws3dHandle, "wGuiCheckBoxGroupDrawBackground");
	*(void**) (&wGuiCheckBoxGroupGetCheckBox) = load_function(ws3dHandle, "wGuiCheckBoxGroupGetCheckBox");
	*(void**) (&wGuiCheckBoxGroupGetIndex) = load_function(ws3dHandle, "wGuiCheckBoxGroupGetIndex");
	*(void**) (&wGuiCheckBoxGroupGetSelectedIndex) = load_function(ws3dHandle, "wGuiCheckBoxGroupGetSelectedIndex");
	*(void**) (&wGuiCheckBoxGroupGetSize) = load_function(ws3dHandle, "wGuiCheckBoxGroupGetSize");
	*(void**) (&wGuiCheckBoxGroupInsertCheckBox) = load_function(ws3dHandle, "wGuiCheckBoxGroupInsertCheckBox");
	*(void**) (&wGuiCheckBoxGroupRemoveAll) = load_function(ws3dHandle, "wGuiCheckBoxGroupRemoveAll");
	*(void**) (&wGuiCheckBoxGroupRemoveCheckBox) = load_function(ws3dHandle, "wGuiCheckBoxGroupRemoveCheckBox");
	*(void**) (&wGuiCheckBoxGroupSelectCheckBox) = load_function(ws3dHandle, "wGuiCheckBoxGroupSelectCheckBox");
	*(void**) (&wGuiCheckBoxGroupSetBackgroundColor) = load_function(ws3dHandle, "wGuiCheckBoxGroupSetBackgroundColor");
	*(void**) (&wGuiCheckBoxIsChecked) = load_function(ws3dHandle, "wGuiCheckBoxIsChecked");
	*(void**) (&wGuiCheckBoxIsDrawBackground) = load_function(ws3dHandle, "wGuiCheckBoxIsDrawBackground");
	*(void**) (&wGuiCheckBoxIsDrawBorder) = load_function(ws3dHandle, "wGuiCheckBoxIsDrawBorder");
	*(void**) (&wGuiCheckBoxIsFilled) = load_function(ws3dHandle, "wGuiCheckBoxIsFilled");
	*(void**) (&wGuiCheckBoxSetDrawBackground) = load_function(ws3dHandle, "wGuiCheckBoxSetDrawBackground");
	*(void**) (&wGuiCheckBoxSetDrawBorder) = load_function(ws3dHandle, "wGuiCheckBoxSetDrawBorder");
	*(void**) (&wGuiCheckBoxSetFilled) = load_function(ws3dHandle, "wGuiCheckBoxSetFilled");
	*(void**) (&wGuiColorSelectDialogCreate) = load_function(ws3dHandle, "wGuiColorSelectDialogCreate");
	*(void**) (&wGuiComboBoxAddItem) = load_function(ws3dHandle, "wGuiComboBoxAddItem");
	*(void**) (&wGuiComboBoxCreate) = load_function(ws3dHandle, "wGuiComboBoxCreate");
	*(void**) (&wGuiComboBoxGetIndexByItemData) = load_function(ws3dHandle, "wGuiComboBoxGetIndexByItemData");
	*(void**) (&wGuiComboBoxGetItemByIndex) = load_function(ws3dHandle, "wGuiComboBoxGetItemByIndex");
	*(void**) (&wGuiComboBoxGetItemDataByIndex) = load_function(ws3dHandle, "wGuiComboBoxGetItemDataByIndex");
	*(void**) (&wGuiComboBoxGetItemsCount) = load_function(ws3dHandle, "wGuiComboBoxGetItemsCount");
	*(void**) (&wGuiComboBoxGetMaxSelectionRows) = load_function(ws3dHandle, "wGuiComboBoxGetMaxSelectionRows");
	*(void**) (&wGuiComboBoxGetSelected) = load_function(ws3dHandle, "wGuiComboBoxGetSelected");
	*(void**) (&wGuiComboBoxRemoveAll) = load_function(ws3dHandle, "wGuiComboBoxRemoveAll");
	*(void**) (&wGuiComboBoxRemoveItem) = load_function(ws3dHandle, "wGuiComboBoxRemoveItem");
	*(void**) (&wGuiComboBoxSetMaxSelectionRows) = load_function(ws3dHandle, "wGuiComboBoxSetMaxSelectionRows");
	*(void**) (&wGuiComboBoxSetSelected) = load_function(ws3dHandle, "wGuiComboBoxSetSelected");
	*(void**) (&wGuiComboBoxSetTextAlignment) = load_function(ws3dHandle, "wGuiComboBoxSetTextAlignment");
	*(void**) (&wGuiContextMenuAddItem) = load_function(ws3dHandle, "wGuiContextMenuAddItem");
	*(void**) (&wGuiContextMenuAddSeparator) = load_function(ws3dHandle, "wGuiContextMenuAddSeparator");
	*(void**) (&wGuiContextMenuCreate) = load_function(ws3dHandle, "wGuiContextMenuCreate");
	*(void**) (&wGuiContextMenuFindItem) = load_function(ws3dHandle, "wGuiContextMenuFindItem");
	*(void**) (&wGuiContextMenuGetCloseHandling) = load_function(ws3dHandle, "wGuiContextMenuGetCloseHandling");
	*(void**) (&wGuiContextMenuGetItemCommandId) = load_function(ws3dHandle, "wGuiContextMenuGetItemCommandId");
	*(void**) (&wGuiContextMenuGetItemText) = load_function(ws3dHandle, "wGuiContextMenuGetItemText");
	*(void**) (&wGuiContextMenuGetItemsCount) = load_function(ws3dHandle, "wGuiContextMenuGetItemsCount");
	*(void**) (&wGuiContextMenuGetSelectedItem) = load_function(ws3dHandle, "wGuiContextMenuGetSelectedItem");
	*(void**) (&wGuiContextMenuGetSubMenu) = load_function(ws3dHandle, "wGuiContextMenuGetSubMenu");
	*(void**) (&wGuiContextMenuInsertItem) = load_function(ws3dHandle, "wGuiContextMenuInsertItem");
	*(void**) (&wGuiContextMenuIsAutoChecked) = load_function(ws3dHandle, "wGuiContextMenuIsAutoChecked");
	*(void**) (&wGuiContextMenuIsItemChecked) = load_function(ws3dHandle, "wGuiContextMenuIsItemChecked");
	*(void**) (&wGuiContextMenuIsItemEnabled) = load_function(ws3dHandle, "wGuiContextMenuIsItemEnabled");
	*(void**) (&wGuiContextMenuRemoveAll) = load_function(ws3dHandle, "wGuiContextMenuRemoveAll");
	*(void**) (&wGuiContextMenuRemoveItem) = load_function(ws3dHandle, "wGuiContextMenuRemoveItem");
	*(void**) (&wGuiContextMenuSetAutoChecking) = load_function(ws3dHandle, "wGuiContextMenuSetAutoChecking");
	*(void**) (&wGuiContextMenuSetCloseHandling) = load_function(ws3dHandle, "wGuiContextMenuSetCloseHandling");
	*(void**) (&wGuiContextMenuSetEventParent) = load_function(ws3dHandle, "wGuiContextMenuSetEventParent");
	*(void**) (&wGuiContextMenuSetItemChecked) = load_function(ws3dHandle, "wGuiContextMenuSetItemChecked");
	*(void**) (&wGuiContextMenuSetItemCommandId) = load_function(ws3dHandle, "wGuiContextMenuSetItemCommandId");
	*(void**) (&wGuiContextMenuSetItemEnabled) = load_function(ws3dHandle, "wGuiContextMenuSetItemEnabled");
	*(void**) (&wGuiContextMenuSetItemText) = load_function(ws3dHandle, "wGuiContextMenuSetItemText");
	*(void**) (&wGuiDestroyAll) = load_function(ws3dHandle, "wGuiDestroyAll");
	*(void**) (&wGuiDrawAll) = load_function(ws3dHandle, "wGuiDrawAll");
	*(void**) (&wGuiEditBoxCreate) = load_function(ws3dHandle, "wGuiEditBoxCreate");
	*(void**) (&wGuiEditBoxEnableOverrideColor) = load_function(ws3dHandle, "wGuiEditBoxEnableOverrideColor");
	*(void**) (&wGuiEditBoxGetActiveFont) = load_function(ws3dHandle, "wGuiEditBoxGetActiveFont");
	*(void**) (&wGuiEditBoxGetOverrideColor) = load_function(ws3dHandle, "wGuiEditBoxGetOverrideColor");
	*(void**) (&wGuiEditBoxGetOverrideFont) = load_function(ws3dHandle, "wGuiEditBoxGetOverrideFont");
	*(void**) (&wGuiEditBoxGetTextSize) = load_function(ws3dHandle, "wGuiEditBoxGetTextSize");
	*(void**) (&wGuiEditBoxIsAutoScrolling) = load_function(ws3dHandle, "wGuiEditBoxIsAutoScrolling");
	*(void**) (&wGuiEditBoxIsDrawBorder) = load_function(ws3dHandle, "wGuiEditBoxIsDrawBorder");
	*(void**) (&wGuiEditBoxIsMultiLine) = load_function(ws3dHandle, "wGuiEditBoxIsMultiLine");
	*(void**) (&wGuiEditBoxIsOverrideColor) = load_function(ws3dHandle, "wGuiEditBoxIsOverrideColor");
	*(void**) (&wGuiEditBoxIsPasswordMode) = load_function(ws3dHandle, "wGuiEditBoxIsPasswordMode");
	*(void**) (&wGuiEditBoxIsWordWrap) = load_function(ws3dHandle, "wGuiEditBoxIsWordWrap");
	*(void**) (&wGuiEditBoxSetAutoScrolling) = load_function(ws3dHandle, "wGuiEditBoxSetAutoScrolling");
	*(void**) (&wGuiEditBoxSetCharactersLimit) = load_function(ws3dHandle, "wGuiEditBoxSetCharactersLimit");
	*(void**) (&wGuiEditBoxSetDrawBackground) = load_function(ws3dHandle, "wGuiEditBoxSetDrawBackground");
	*(void**) (&wGuiEditBoxSetDrawBorder) = load_function(ws3dHandle, "wGuiEditBoxSetDrawBorder");
	*(void**) (&wGuiEditBoxSetMultiLine) = load_function(ws3dHandle, "wGuiEditBoxSetMultiLine");
	*(void**) (&wGuiEditBoxSetOverrideColor) = load_function(ws3dHandle, "wGuiEditBoxSetOverrideColor");
	*(void**) (&wGuiEditBoxSetOverrideFont) = load_function(ws3dHandle, "wGuiEditBoxSetOverrideFont");
	*(void**) (&wGuiEditBoxSetPasswordMode) = load_function(ws3dHandle, "wGuiEditBoxSetPasswordMode");
	*(void**) (&wGuiEditBoxSetTextAlignment) = load_function(ws3dHandle, "wGuiEditBoxSetTextAlignment");
	*(void**) (&wGuiEditBoxSetWordWrap) = load_function(ws3dHandle, "wGuiEditBoxSetWordWrap");
	*(void**) (&wGuiEditGetCharactersLimit) = load_function(ws3dHandle, "wGuiEditGetCharactersLimit");
	*(void**) (&wGuiFaderCreate) = load_function(ws3dHandle, "wGuiFaderCreate");
	*(void**) (&wGuiFaderFadeIn) = load_function(ws3dHandle, "wGuiFaderFadeIn");
	*(void**) (&wGuiFaderFadeOut) = load_function(ws3dHandle, "wGuiFaderFadeOut");
	*(void**) (&wGuiFaderGetColor) = load_function(ws3dHandle, "wGuiFaderGetColor");
	*(void**) (&wGuiFaderIsReady) = load_function(ws3dHandle, "wGuiFaderIsReady");
	*(void**) (&wGuiFaderSetColor) = load_function(ws3dHandle, "wGuiFaderSetColor");
	*(void**) (&wGuiFaderSetColorExt) = load_function(ws3dHandle, "wGuiFaderSetColorExt");
	*(void**) (&wGuiFileOpenDialogCreate) = load_function(ws3dHandle, "wGuiFileOpenDialogCreate");
	*(void**) (&wGuiFileOpenDialogGetDirectory) = load_function(ws3dHandle, "wGuiFileOpenDialogGetDirectory");
	*(void**) (&wGuiFileOpenDialogGetFile) = load_function(ws3dHandle, "wGuiFileOpenDialogGetFile");
	*(void**) (&wGuiGetLastSelectedFile) = load_function(ws3dHandle, "wGuiGetLastSelectedFile");
	*(void**) (&wGuiGetObjectById) = load_function(ws3dHandle, "wGuiGetObjectById");
	*(void**) (&wGuiGetObjectByName) = load_function(ws3dHandle, "wGuiGetObjectByName");
	*(void**) (&wGuiGetObjectFocused) = load_function(ws3dHandle, "wGuiGetObjectFocused");
	*(void**) (&wGuiGetObjectHovered) = load_function(ws3dHandle, "wGuiGetObjectHovered");
	*(void**) (&wGuiGetRootNode) = load_function(ws3dHandle, "wGuiGetRootNode");
	*(void**) (&wGuiGetSkin) = load_function(ws3dHandle, "wGuiGetSkin");
	*(void**) (&wGuiImageCreate) = load_function(ws3dHandle, "wGuiImageCreate");
	*(void**) (&wGuiImageGet) = load_function(ws3dHandle, "wGuiImageGet");
	*(void**) (&wGuiImageGetColor) = load_function(ws3dHandle, "wGuiImageGetColor");
	*(void**) (&wGuiImageIsScaled) = load_function(ws3dHandle, "wGuiImageIsScaled");
	*(void**) (&wGuiImageIsUsedAlphaChannel) = load_function(ws3dHandle, "wGuiImageIsUsedAlphaChannel");
	*(void**) (&wGuiImageListCreate) = load_function(ws3dHandle, "wGuiImageListCreate");
	*(void**) (&wGuiImageListDraw) = load_function(ws3dHandle, "wGuiImageListDraw");
	*(void**) (&wGuiImageListGetCount) = load_function(ws3dHandle, "wGuiImageListGetCount");
	*(void**) (&wGuiImageListGetSize) = load_function(ws3dHandle, "wGuiImageListGetSize");
	*(void**) (&wGuiImageSet) = load_function(ws3dHandle, "wGuiImageSet");
	*(void**) (&wGuiImageSetColor) = load_function(ws3dHandle, "wGuiImageSetColor");
	*(void**) (&wGuiImageSetScaling) = load_function(ws3dHandle, "wGuiImageSetScaling");
	*(void**) (&wGuiImageUseAlphaChannel) = load_function(ws3dHandle, "wGuiImageUseAlphaChannel");
	*(void**) (&wGuiIsEventAvailable) = load_function(ws3dHandle, "wGuiIsEventAvailable");
	*(void**) (&wGuiLabelCreate) = load_function(ws3dHandle, "wGuiLabelCreate");
	*(void**) (&wGuiLabelEnableOverrideColor) = load_function(ws3dHandle, "wGuiLabelEnableOverrideColor");
	*(void**) (&wGuiLabelGetActiveFont) = load_function(ws3dHandle, "wGuiLabelGetActiveFont");
	*(void**) (&wGuiLabelGetBackgroundColor) = load_function(ws3dHandle, "wGuiLabelGetBackgroundColor");
	*(void**) (&wGuiLabelGetOverrideColor) = load_function(ws3dHandle, "wGuiLabelGetOverrideColor");
	*(void**) (&wGuiLabelGetOverrideFont) = load_function(ws3dHandle, "wGuiLabelGetOverrideFont");
	*(void**) (&wGuiLabelGetTextSize) = load_function(ws3dHandle, "wGuiLabelGetTextSize");
	*(void**) (&wGuiLabelIsDrawBackGround) = load_function(ws3dHandle, "wGuiLabelIsDrawBackGround");
	*(void**) (&wGuiLabelIsDrawBorder) = load_function(ws3dHandle, "wGuiLabelIsDrawBorder");
	*(void**) (&wGuiLabelIsOverrideColor) = load_function(ws3dHandle, "wGuiLabelIsOverrideColor");
	*(void**) (&wGuiLabelIsWordWrap) = load_function(ws3dHandle, "wGuiLabelIsWordWrap");
	*(void**) (&wGuiLabelSetBackgroundColor) = load_function(ws3dHandle, "wGuiLabelSetBackgroundColor");
	*(void**) (&wGuiLabelSetDrawBackground) = load_function(ws3dHandle, "wGuiLabelSetDrawBackground");
	*(void**) (&wGuiLabelSetDrawBorder) = load_function(ws3dHandle, "wGuiLabelSetDrawBorder");
	*(void**) (&wGuiLabelSetOverrideColor) = load_function(ws3dHandle, "wGuiLabelSetOverrideColor");
	*(void**) (&wGuiLabelSetOverrideFont) = load_function(ws3dHandle, "wGuiLabelSetOverrideFont");
	*(void**) (&wGuiLabelSetTextAlignment) = load_function(ws3dHandle, "wGuiLabelSetTextAlignment");
	*(void**) (&wGuiLabelSetWordWrap) = load_function(ws3dHandle, "wGuiLabelSetWordWrap");
	*(void**) (&wGuiListBoxAddItem) = load_function(ws3dHandle, "wGuiListBoxAddItem");
	*(void**) (&wGuiListBoxAddItemWithIcon) = load_function(ws3dHandle, "wGuiListBoxAddItemWithIcon");
	*(void**) (&wGuiListBoxClearItemColor) = load_function(ws3dHandle, "wGuiListBoxClearItemColor");
	*(void**) (&wGuiListBoxCreate) = load_function(ws3dHandle, "wGuiListBoxCreate");
	*(void**) (&wGuiListBoxGetDefaultColor) = load_function(ws3dHandle, "wGuiListBoxGetDefaultColor");
	*(void**) (&wGuiListBoxGetElementColor) = load_function(ws3dHandle, "wGuiListBoxGetElementColor");
	*(void**) (&wGuiListBoxGetItemByIndex) = load_function(ws3dHandle, "wGuiListBoxGetItemByIndex");
	*(void**) (&wGuiListBoxGetItemIcon) = load_function(ws3dHandle, "wGuiListBoxGetItemIcon");
	*(void**) (&wGuiListBoxGetItemsCount) = load_function(ws3dHandle, "wGuiListBoxGetItemsCount");
	*(void**) (&wGuiListBoxGetSelectedIndex) = load_function(ws3dHandle, "wGuiListBoxGetSelectedIndex");
	*(void**) (&wGuiListBoxHasElementColor) = load_function(ws3dHandle, "wGuiListBoxHasElementColor");
	*(void**) (&wGuiListBoxInsertItem) = load_function(ws3dHandle, "wGuiListBoxInsertItem");
	*(void**) (&wGuiListBoxIsAutoScrolling) = load_function(ws3dHandle, "wGuiListBoxIsAutoScrolling");
	*(void**) (&wGuiListBoxRemoveAll) = load_function(ws3dHandle, "wGuiListBoxRemoveAll");
	*(void**) (&wGuiListBoxRemoveItem) = load_function(ws3dHandle, "wGuiListBoxRemoveItem");
	*(void**) (&wGuiListBoxSelectItemByIndex) = load_function(ws3dHandle, "wGuiListBoxSelectItemByIndex");
	*(void**) (&wGuiListBoxSelectItemByText) = load_function(ws3dHandle, "wGuiListBoxSelectItemByText");
	*(void**) (&wGuiListBoxSetAutoScrolling) = load_function(ws3dHandle, "wGuiListBoxSetAutoScrolling");
	*(void**) (&wGuiListBoxSetDrawBackground) = load_function(ws3dHandle, "wGuiListBoxSetDrawBackground");
	*(void**) (&wGuiListBoxSetElementColor) = load_function(ws3dHandle, "wGuiListBoxSetElementColor");
	*(void**) (&wGuiListBoxSetItem) = load_function(ws3dHandle, "wGuiListBoxSetItem");
	*(void**) (&wGuiListBoxSetItemColor) = load_function(ws3dHandle, "wGuiListBoxSetItemColor");
	*(void**) (&wGuiListBoxSetItemsHeight) = load_function(ws3dHandle, "wGuiListBoxSetItemsHeight");
	*(void**) (&wGuiListBoxSwapItems) = load_function(ws3dHandle, "wGuiListBoxSwapItems");
	*(void**) (&wGuiLoad) = load_function(ws3dHandle, "wGuiLoad");
	*(void**) (&wGuiMenuCreate) = load_function(ws3dHandle, "wGuiMenuCreate");
	*(void**) (&wGuiMeshViewerCreate) = load_function(ws3dHandle, "wGuiMeshViewerCreate");
	*(void**) (&wGuiMeshViewerGetMaterial) = load_function(ws3dHandle, "wGuiMeshViewerGetMaterial");
	*(void**) (&wGuiMeshViewerGetMesh) = load_function(ws3dHandle, "wGuiMeshViewerGetMesh");
	*(void**) (&wGuiMeshViewerSetMaterial) = load_function(ws3dHandle, "wGuiMeshViewerSetMaterial");
	*(void**) (&wGuiMeshViewerSetMesh) = load_function(ws3dHandle, "wGuiMeshViewerSetMesh");
	*(void**) (&wGuiMessageBoxCreate) = load_function(ws3dHandle, "wGuiMessageBoxCreate");
	*(void**) (&wGuiModalScreenCreate) = load_function(ws3dHandle, "wGuiModalScreenCreate");
	*(void**) (&wGuiObjectBringToFront) = load_function(ws3dHandle, "wGuiObjectBringToFront");
	*(void**) (&wGuiObjectDestroy) = load_function(ws3dHandle, "wGuiObjectDestroy");
	*(void**) (&wGuiObjectDestroyChild) = load_function(ws3dHandle, "wGuiObjectDestroyChild");
	*(void**) (&wGuiObjectDraw) = load_function(ws3dHandle, "wGuiObjectDraw");
	*(void**) (&wGuiObjectGetAbsoluteClippedPosition) = load_function(ws3dHandle, "wGuiObjectGetAbsoluteClippedPosition");
	*(void**) (&wGuiObjectGetAbsoluteClippedSize) = load_function(ws3dHandle, "wGuiObjectGetAbsoluteClippedSize");
	*(void**) (&wGuiObjectGetAbsolutePosition) = load_function(ws3dHandle, "wGuiObjectGetAbsolutePosition");
	*(void**) (&wGuiObjectGetChildById) = load_function(ws3dHandle, "wGuiObjectGetChildById");
	*(void**) (&wGuiObjectGetChildByName) = load_function(ws3dHandle, "wGuiObjectGetChildByName");
	*(void**) (&wGuiObjectGetFromScreenPos) = load_function(ws3dHandle, "wGuiObjectGetFromScreenPos");
	*(void**) (&wGuiObjectGetId) = load_function(ws3dHandle, "wGuiObjectGetId");
	*(void**) (&wGuiObjectGetName) = load_function(ws3dHandle, "wGuiObjectGetName");
	*(void**) (&wGuiObjectGetParent) = load_function(ws3dHandle, "wGuiObjectGetParent");
	*(void**) (&wGuiObjectGetRelativePosition) = load_function(ws3dHandle, "wGuiObjectGetRelativePosition");
	*(void**) (&wGuiObjectGetRelativeSize) = load_function(ws3dHandle, "wGuiObjectGetRelativeSize");
	*(void**) (&wGuiObjectGetTabOrder) = load_function(ws3dHandle, "wGuiObjectGetTabOrder");
	*(void**) (&wGuiObjectGetText) = load_function(ws3dHandle, "wGuiObjectGetText");
	*(void**) (&wGuiObjectGetToolTipText) = load_function(ws3dHandle, "wGuiObjectGetToolTipText");
	*(void**) (&wGuiObjectGetType) = load_function(ws3dHandle, "wGuiObjectGetType");
	*(void**) (&wGuiObjectGetTypeName) = load_function(ws3dHandle, "wGuiObjectGetTypeName");
	*(void**) (&wGuiObjectHasType) = load_function(ws3dHandle, "wGuiObjectHasType");
	*(void**) (&wGuiObjectIsChildOf) = load_function(ws3dHandle, "wGuiObjectIsChildOf");
	*(void**) (&wGuiObjectIsClipped) = load_function(ws3dHandle, "wGuiObjectIsClipped");
	*(void**) (&wGuiObjectIsEnabled) = load_function(ws3dHandle, "wGuiObjectIsEnabled");
	*(void**) (&wGuiObjectIsFocused) = load_function(ws3dHandle, "wGuiObjectIsFocused");
	*(void**) (&wGuiObjectIsHovered) = load_function(ws3dHandle, "wGuiObjectIsHovered");
	*(void**) (&wGuiObjectIsPointInside) = load_function(ws3dHandle, "wGuiObjectIsPointInside");
	*(void**) (&wGuiObjectIsSubObject) = load_function(ws3dHandle, "wGuiObjectIsSubObject");
	*(void**) (&wGuiObjectIsTabGroup) = load_function(ws3dHandle, "wGuiObjectIsTabGroup");
	*(void**) (&wGuiObjectIsTabStop) = load_function(ws3dHandle, "wGuiObjectIsTabStop");
	*(void**) (&wGuiObjectIsVisible) = load_function(ws3dHandle, "wGuiObjectIsVisible");
	*(void**) (&wGuiObjectMoveTo) = load_function(ws3dHandle, "wGuiObjectMoveTo");
	*(void**) (&wGuiObjectReadFromXml) = load_function(ws3dHandle, "wGuiObjectReadFromXml");
	*(void**) (&wGuiObjectRemoveFocus) = load_function(ws3dHandle, "wGuiObjectRemoveFocus");
	*(void**) (&wGuiObjectSetAlignment) = load_function(ws3dHandle, "wGuiObjectSetAlignment");
	*(void**) (&wGuiObjectSetClippingMode) = load_function(ws3dHandle, "wGuiObjectSetClippingMode");
	*(void**) (&wGuiObjectSetEnable) = load_function(ws3dHandle, "wGuiObjectSetEnable");
	*(void**) (&wGuiObjectSetFocus) = load_function(ws3dHandle, "wGuiObjectSetFocus");
	*(void**) (&wGuiObjectSetId) = load_function(ws3dHandle, "wGuiObjectSetId");
	*(void**) (&wGuiObjectSetMaxSize) = load_function(ws3dHandle, "wGuiObjectSetMaxSize");
	*(void**) (&wGuiObjectSetMinSize) = load_function(ws3dHandle, "wGuiObjectSetMinSize");
	*(void**) (&wGuiObjectSetName) = load_function(ws3dHandle, "wGuiObjectSetName");
	*(void**) (&wGuiObjectSetParent) = load_function(ws3dHandle, "wGuiObjectSetParent");
	*(void**) (&wGuiObjectSetRelativePosition) = load_function(ws3dHandle, "wGuiObjectSetRelativePosition");
	*(void**) (&wGuiObjectSetRelativeSize) = load_function(ws3dHandle, "wGuiObjectSetRelativeSize");
	*(void**) (&wGuiObjectSetSubObject) = load_function(ws3dHandle, "wGuiObjectSetSubObject");
	*(void**) (&wGuiObjectSetTabGroup) = load_function(ws3dHandle, "wGuiObjectSetTabGroup");
	*(void**) (&wGuiObjectSetTabOrder) = load_function(ws3dHandle, "wGuiObjectSetTabOrder");
	*(void**) (&wGuiObjectSetTabStop) = load_function(ws3dHandle, "wGuiObjectSetTabStop");
	*(void**) (&wGuiObjectSetText) = load_function(ws3dHandle, "wGuiObjectSetText");
	*(void**) (&wGuiObjectSetToolTipText) = load_function(ws3dHandle, "wGuiObjectSetToolTipText");
	*(void**) (&wGuiObjectSetVisible) = load_function(ws3dHandle, "wGuiObjectSetVisible");
	*(void**) (&wGuiObjectUpdateAbsolutePosition) = load_function(ws3dHandle, "wGuiObjectUpdateAbsolutePosition");
	*(void**) (&wGuiObjectWriteToXml) = load_function(ws3dHandle, "wGuiObjectWriteToXml");
	*(void**) (&wGuiProgressBarCreate) = load_function(ws3dHandle, "wGuiProgressBarCreate");
	*(void**) (&wGuiProgressBarGetFillColor) = load_function(ws3dHandle, "wGuiProgressBarGetFillColor");
	*(void**) (&wGuiProgressBarGetPercentage) = load_function(ws3dHandle, "wGuiProgressBarGetPercentage");
	*(void**) (&wGuiProgressBarIsHorizontal) = load_function(ws3dHandle, "wGuiProgressBarIsHorizontal");
	*(void**) (&wGuiProgressBarIsShowText) = load_function(ws3dHandle, "wGuiProgressBarIsShowText");
	*(void**) (&wGuiProgressBarSetBackTexture) = load_function(ws3dHandle, "wGuiProgressBarSetBackTexture");
	*(void**) (&wGuiProgressBarSetBackgroundColor) = load_function(ws3dHandle, "wGuiProgressBarSetBackgroundColor");
	*(void**) (&wGuiProgressBarSetBorderColor) = load_function(ws3dHandle, "wGuiProgressBarSetBorderColor");
	*(void**) (&wGuiProgressBarSetBorderSize) = load_function(ws3dHandle, "wGuiProgressBarSetBorderSize");
	*(void**) (&wGuiProgressBarSetDirection) = load_function(ws3dHandle, "wGuiProgressBarSetDirection");
	*(void**) (&wGuiProgressBarSetFillColor) = load_function(ws3dHandle, "wGuiProgressBarSetFillColor");
	*(void**) (&wGuiProgressBarSetFillTexture) = load_function(ws3dHandle, "wGuiProgressBarSetFillTexture");
	*(void**) (&wGuiProgressBarSetFont) = load_function(ws3dHandle, "wGuiProgressBarSetFont");
	*(void**) (&wGuiProgressBarSetPercentage) = load_function(ws3dHandle, "wGuiProgressBarSetPercentage");
	*(void**) (&wGuiProgressBarSetSize) = load_function(ws3dHandle, "wGuiProgressBarSetSize");
	*(void**) (&wGuiProgressBarSetTextColor) = load_function(ws3dHandle, "wGuiProgressBarSetTextColor");
	*(void**) (&wGuiProgressBarShowText) = load_function(ws3dHandle, "wGuiProgressBarShowText");
	*(void**) (&wGuiReadEvent) = load_function(ws3dHandle, "wGuiReadEvent");
	*(void**) (&wGuiSave) = load_function(ws3dHandle, "wGuiSave");
	*(void**) (&wGuiScrollBarCreate) = load_function(ws3dHandle, "wGuiScrollBarCreate");
	*(void**) (&wGuiScrollBarGetLargeStep) = load_function(ws3dHandle, "wGuiScrollBarGetLargeStep");
	*(void**) (&wGuiScrollBarGetMaxValue) = load_function(ws3dHandle, "wGuiScrollBarGetMaxValue");
	*(void**) (&wGuiScrollBarGetMinValue) = load_function(ws3dHandle, "wGuiScrollBarGetMinValue");
	*(void**) (&wGuiScrollBarGetSmallStep) = load_function(ws3dHandle, "wGuiScrollBarGetSmallStep");
	*(void**) (&wGuiScrollBarGetValue) = load_function(ws3dHandle, "wGuiScrollBarGetValue");
	*(void**) (&wGuiScrollBarSetLargeStep) = load_function(ws3dHandle, "wGuiScrollBarSetLargeStep");
	*(void**) (&wGuiScrollBarSetMaxValue) = load_function(ws3dHandle, "wGuiScrollBarSetMaxValue");
	*(void**) (&wGuiScrollBarSetMinValue) = load_function(ws3dHandle, "wGuiScrollBarSetMinValue");
	*(void**) (&wGuiScrollBarSetSmallStep) = load_function(ws3dHandle, "wGuiScrollBarSetSmallStep");
	*(void**) (&wGuiScrollBarSetValue) = load_function(ws3dHandle, "wGuiScrollBarSetValue");
	*(void**) (&wGuiSetSkin) = load_function(ws3dHandle, "wGuiSetSkin");
	*(void**) (&wGuiSkinCreate) = load_function(ws3dHandle, "wGuiSkinCreate");
	*(void**) (&wGuiSkinGetColor) = load_function(ws3dHandle, "wGuiSkinGetColor");
	*(void**) (&wGuiSkinGetDefaultText) = load_function(ws3dHandle, "wGuiSkinGetDefaultText");
	*(void**) (&wGuiSkinGetFont) = load_function(ws3dHandle, "wGuiSkinGetFont");
	*(void**) (&wGuiSkinGetIcon) = load_function(ws3dHandle, "wGuiSkinGetIcon");
	*(void**) (&wGuiSkinGetSize) = load_function(ws3dHandle, "wGuiSkinGetSize");
	*(void**) (&wGuiSkinGetSpriteBank) = load_function(ws3dHandle, "wGuiSkinGetSpriteBank");
	*(void**) (&wGuiSkinGetType) = load_function(ws3dHandle, "wGuiSkinGetType");
	*(void**) (&wGuiSkinSetColor) = load_function(ws3dHandle, "wGuiSkinSetColor");
	*(void**) (&wGuiSkinSetDefaultText) = load_function(ws3dHandle, "wGuiSkinSetDefaultText");
	*(void**) (&wGuiSkinSetFont) = load_function(ws3dHandle, "wGuiSkinSetFont");
	*(void**) (&wGuiSkinSetIcon) = load_function(ws3dHandle, "wGuiSkinSetIcon");
	*(void**) (&wGuiSkinSetSize) = load_function(ws3dHandle, "wGuiSkinSetSize");
	*(void**) (&wGuiSkinSetSpriteBank) = load_function(ws3dHandle, "wGuiSkinSetSpriteBank");
	*(void**) (&wGuiSpinBoxCreate) = load_function(ws3dHandle, "wGuiSpinBoxCreate");
	*(void**) (&wGuiSpinBoxGetEditBox) = load_function(ws3dHandle, "wGuiSpinBoxGetEditBox");
	*(void**) (&wGuiSpinBoxGetMax) = load_function(ws3dHandle, "wGuiSpinBoxGetMax");
	*(void**) (&wGuiSpinBoxGetMin) = load_function(ws3dHandle, "wGuiSpinBoxGetMin");
	*(void**) (&wGuiSpinBoxGetStepSize) = load_function(ws3dHandle, "wGuiSpinBoxGetStepSize");
	*(void**) (&wGuiSpinBoxGetValue) = load_function(ws3dHandle, "wGuiSpinBoxGetValue");
	*(void**) (&wGuiSpinBoxSetDecimalPlaces) = load_function(ws3dHandle, "wGuiSpinBoxSetDecimalPlaces");
	*(void**) (&wGuiSpinBoxSetRange) = load_function(ws3dHandle, "wGuiSpinBoxSetRange");
	*(void**) (&wGuiSpinBoxSetStepSize) = load_function(ws3dHandle, "wGuiSpinBoxSetStepSize");
	*(void**) (&wGuiSpinBoxSetValue) = load_function(ws3dHandle, "wGuiSpinBoxSetValue");
	*(void**) (&wGuiSpriteBankAddSprite) = load_function(ws3dHandle, "wGuiSpriteBankAddSprite");
	*(void**) (&wGuiSpriteBankAddTexture) = load_function(ws3dHandle, "wGuiSpriteBankAddTexture");
	*(void**) (&wGuiSpriteBankCreate) = load_function(ws3dHandle, "wGuiSpriteBankCreate");
	*(void**) (&wGuiSpriteBankDrawSprite) = load_function(ws3dHandle, "wGuiSpriteBankDrawSprite");
	*(void**) (&wGuiSpriteBankDrawSpriteBatch) = load_function(ws3dHandle, "wGuiSpriteBankDrawSpriteBatch");
	*(void**) (&wGuiSpriteBankGetTexture) = load_function(ws3dHandle, "wGuiSpriteBankGetTexture");
	*(void**) (&wGuiSpriteBankGetTexturesCount) = load_function(ws3dHandle, "wGuiSpriteBankGetTexturesCount");
	*(void**) (&wGuiSpriteBankLoad) = load_function(ws3dHandle, "wGuiSpriteBankLoad");
	*(void**) (&wGuiSpriteBankRemoveAll) = load_function(ws3dHandle, "wGuiSpriteBankRemoveAll");
	*(void**) (&wGuiSpriteBankSetTexture) = load_function(ws3dHandle, "wGuiSpriteBankSetTexture");
	*(void**) (&wGuiTabControlAddTab) = load_function(ws3dHandle, "wGuiTabControlAddTab");
	*(void**) (&wGuiTabControlCreate) = load_function(ws3dHandle, "wGuiTabControlCreate");
	*(void**) (&wGuiTabControlGetActiveTab) = load_function(ws3dHandle, "wGuiTabControlGetActiveTab");
	*(void**) (&wGuiTabControlGetTab) = load_function(ws3dHandle, "wGuiTabControlGetTab");
	*(void**) (&wGuiTabControlGetTabExtraWidth) = load_function(ws3dHandle, "wGuiTabControlGetTabExtraWidth");
	*(void**) (&wGuiTabControlGetTabFromPos) = load_function(ws3dHandle, "wGuiTabControlGetTabFromPos");
	*(void**) (&wGuiTabControlGetTabHeight) = load_function(ws3dHandle, "wGuiTabControlGetTabHeight");
	*(void**) (&wGuiTabControlGetTabMaxWidth) = load_function(ws3dHandle, "wGuiTabControlGetTabMaxWidth");
	*(void**) (&wGuiTabControlGetTabsCount) = load_function(ws3dHandle, "wGuiTabControlGetTabsCount");
	*(void**) (&wGuiTabControlGetVerticalAlignment) = load_function(ws3dHandle, "wGuiTabControlGetVerticalAlignment");
	*(void**) (&wGuiTabControlInsertTab) = load_function(ws3dHandle, "wGuiTabControlInsertTab");
	*(void**) (&wGuiTabControlRemoveAll) = load_function(ws3dHandle, "wGuiTabControlRemoveAll");
	*(void**) (&wGuiTabControlRemoveTab) = load_function(ws3dHandle, "wGuiTabControlRemoveTab");
	*(void**) (&wGuiTabControlSetActiveTab) = load_function(ws3dHandle, "wGuiTabControlSetActiveTab");
	*(void**) (&wGuiTabControlSetActiveTabByIndex) = load_function(ws3dHandle, "wGuiTabControlSetActiveTabByIndex");
	*(void**) (&wGuiTabControlSetTabExtraWidth) = load_function(ws3dHandle, "wGuiTabControlSetTabExtraWidth");
	*(void**) (&wGuiTabControlSetTabHeight) = load_function(ws3dHandle, "wGuiTabControlSetTabHeight");
	*(void**) (&wGuiTabControlSetTabMaxWidth) = load_function(ws3dHandle, "wGuiTabControlSetTabMaxWidth");
	*(void**) (&wGuiTabControlSetVerticalAlignment) = load_function(ws3dHandle, "wGuiTabControlSetVerticalAlignment");
	*(void**) (&wGuiTabCreate) = load_function(ws3dHandle, "wGuiTabCreate");
	*(void**) (&wGuiTabGetBackgroundColor) = load_function(ws3dHandle, "wGuiTabGetBackgroundColor");
	*(void**) (&wGuiTabGetNumber) = load_function(ws3dHandle, "wGuiTabGetNumber");
	*(void**) (&wGuiTabGetTextColor) = load_function(ws3dHandle, "wGuiTabGetTextColor");
	*(void**) (&wGuiTabIsDrawBackground) = load_function(ws3dHandle, "wGuiTabIsDrawBackground");
	*(void**) (&wGuiTabSetBackgroundColor) = load_function(ws3dHandle, "wGuiTabSetBackgroundColor");
	*(void**) (&wGuiTabSetDrawBackground) = load_function(ws3dHandle, "wGuiTabSetDrawBackground");
	*(void**) (&wGuiTabSetTextColor) = load_function(ws3dHandle, "wGuiTabSetTextColor");
	*(void**) (&wGuiTableAddColumn) = load_function(ws3dHandle, "wGuiTableAddColumn");
	*(void**) (&wGuiTableAddRow) = load_function(ws3dHandle, "wGuiTableAddRow");
	*(void**) (&wGuiTableClearRows) = load_function(ws3dHandle, "wGuiTableClearRows");
	*(void**) (&wGuiTableCreate) = load_function(ws3dHandle, "wGuiTableCreate");
	*(void**) (&wGuiTableGetActiveColumn) = load_function(ws3dHandle, "wGuiTableGetActiveColumn");
	*(void**) (&wGuiTableGetActiveColumnOrdering) = load_function(ws3dHandle, "wGuiTableGetActiveColumnOrdering");
	*(void**) (&wGuiTableGetActiveFont) = load_function(ws3dHandle, "wGuiTableGetActiveFont");
	*(void**) (&wGuiTableGetCellData) = load_function(ws3dHandle, "wGuiTableGetCellData");
	*(void**) (&wGuiTableGetCellText) = load_function(ws3dHandle, "wGuiTableGetCellText");
	*(void**) (&wGuiTableGetColumnsCount) = load_function(ws3dHandle, "wGuiTableGetColumnsCount");
	*(void**) (&wGuiTableGetDrawFlags) = load_function(ws3dHandle, "wGuiTableGetDrawFlags");
	*(void**) (&wGuiTableGetHorizontalScrollBar) = load_function(ws3dHandle, "wGuiTableGetHorizontalScrollBar");
	*(void**) (&wGuiTableGetItemHeight) = load_function(ws3dHandle, "wGuiTableGetItemHeight");
	*(void**) (&wGuiTableGetOverrideFont) = load_function(ws3dHandle, "wGuiTableGetOverrideFont");
	*(void**) (&wGuiTableGetRowsCount) = load_function(ws3dHandle, "wGuiTableGetRowsCount");
	*(void**) (&wGuiTableGetSelected) = load_function(ws3dHandle, "wGuiTableGetSelected");
	*(void**) (&wGuiTableGetVerticalScrollBar) = load_function(ws3dHandle, "wGuiTableGetVerticalScrollBar");
	*(void**) (&wGuiTableIsColumnsResizable) = load_function(ws3dHandle, "wGuiTableIsColumnsResizable");
	*(void**) (&wGuiTableIsDrawBackground) = load_function(ws3dHandle, "wGuiTableIsDrawBackground");
	*(void**) (&wGuiTableRemoveColumn) = load_function(ws3dHandle, "wGuiTableRemoveColumn");
	*(void**) (&wGuiTableRemoveRow) = load_function(ws3dHandle, "wGuiTableRemoveRow");
	*(void**) (&wGuiTableSetActiveColumn) = load_function(ws3dHandle, "wGuiTableSetActiveColumn");
	*(void**) (&wGuiTableSetCellColor) = load_function(ws3dHandle, "wGuiTableSetCellColor");
	*(void**) (&wGuiTableSetCellData) = load_function(ws3dHandle, "wGuiTableSetCellData");
	*(void**) (&wGuiTableSetCellText) = load_function(ws3dHandle, "wGuiTableSetCellText");
	*(void**) (&wGuiTableSetColumnWidth) = load_function(ws3dHandle, "wGuiTableSetColumnWidth");
	*(void**) (&wGuiTableSetColumnsResizable) = load_function(ws3dHandle, "wGuiTableSetColumnsResizable");
	*(void**) (&wGuiTableSetDrawBackground) = load_function(ws3dHandle, "wGuiTableSetDrawBackground");
	*(void**) (&wGuiTableSetDrawFlags) = load_function(ws3dHandle, "wGuiTableSetDrawFlags");
	*(void**) (&wGuiTableSetOrderRows) = load_function(ws3dHandle, "wGuiTableSetOrderRows");
	*(void**) (&wGuiTableSetOverrideFont) = load_function(ws3dHandle, "wGuiTableSetOverrideFont");
	*(void**) (&wGuiTableSetSelectedByIndex) = load_function(ws3dHandle, "wGuiTableSetSelectedByIndex");
	*(void**) (&wGuiTableSwapRows) = load_function(ws3dHandle, "wGuiTableSwapRows");
	*(void**) (&wGuiTextAreaAddLine) = load_function(ws3dHandle, "wGuiTextAreaAddLine");
	*(void**) (&wGuiTextAreaCreate) = load_function(ws3dHandle, "wGuiTextAreaCreate");
	*(void**) (&wGuiTextAreaRemoveAll) = load_function(ws3dHandle, "wGuiTextAreaRemoveAll");
	*(void**) (&wGuiTextAreaSetAutoScroll) = load_function(ws3dHandle, "wGuiTextAreaSetAutoScroll");
	*(void**) (&wGuiTextAreaSetBackTexture) = load_function(ws3dHandle, "wGuiTextAreaSetBackTexture");
	*(void**) (&wGuiTextAreaSetBackgroundColor) = load_function(ws3dHandle, "wGuiTextAreaSetBackgroundColor");
	*(void**) (&wGuiTextAreaSetBorderColor) = load_function(ws3dHandle, "wGuiTextAreaSetBorderColor");
	*(void**) (&wGuiTextAreaSetBorderSize) = load_function(ws3dHandle, "wGuiTextAreaSetBorderSize");
	*(void**) (&wGuiTextAreaSetFont) = load_function(ws3dHandle, "wGuiTextAreaSetFont");
	*(void**) (&wGuiTextAreaSetPadding) = load_function(ws3dHandle, "wGuiTextAreaSetPadding");
	*(void**) (&wGuiTextAreaSetWrapping) = load_function(ws3dHandle, "wGuiTextAreaSetWrapping");
	*(void**) (&wGuiToolBarAddButton) = load_function(ws3dHandle, "wGuiToolBarAddButton");
	*(void**) (&wGuiToolBarCreate) = load_function(ws3dHandle, "wGuiToolBarCreate");
	*(void**) (&wGuiTreeCreate) = load_function(ws3dHandle, "wGuiTreeCreate");
	*(void**) (&wGuiTreeGetImageList) = load_function(ws3dHandle, "wGuiTreeGetImageList");
	*(void**) (&wGuiTreeGetLastEventNode) = load_function(ws3dHandle, "wGuiTreeGetLastEventNode");
	*(void**) (&wGuiTreeGetRoot) = load_function(ws3dHandle, "wGuiTreeGetRoot");
	*(void**) (&wGuiTreeGetSelected) = load_function(ws3dHandle, "wGuiTreeGetSelected");
	*(void**) (&wGuiTreeIsImageLeftOfIcon) = load_function(ws3dHandle, "wGuiTreeIsImageLeftOfIcon");
	*(void**) (&wGuiTreeIsLinesVisible) = load_function(ws3dHandle, "wGuiTreeIsLinesVisible");
	*(void**) (&wGuiTreeNodeAddChildBack) = load_function(ws3dHandle, "wGuiTreeNodeAddChildBack");
	*(void**) (&wGuiTreeNodeAddChildFront) = load_function(ws3dHandle, "wGuiTreeNodeAddChildFront");
	*(void**) (&wGuiTreeNodeGetChildsCount) = load_function(ws3dHandle, "wGuiTreeNodeGetChildsCount");
	*(void**) (&wGuiTreeNodeGetData) = load_function(ws3dHandle, "wGuiTreeNodeGetData");
	*(void**) (&wGuiTreeNodeGetData2) = load_function(ws3dHandle, "wGuiTreeNodeGetData2");
	*(void**) (&wGuiTreeNodeGetFirstChild) = load_function(ws3dHandle, "wGuiTreeNodeGetFirstChild");
	*(void**) (&wGuiTreeNodeGetIcon) = load_function(ws3dHandle, "wGuiTreeNodeGetIcon");
	*(void**) (&wGuiTreeNodeGetImageIndex) = load_function(ws3dHandle, "wGuiTreeNodeGetImageIndex");
	*(void**) (&wGuiTreeNodeGetLastChild) = load_function(ws3dHandle, "wGuiTreeNodeGetLastChild");
	*(void**) (&wGuiTreeNodeGetLevel) = load_function(ws3dHandle, "wGuiTreeNodeGetLevel");
	*(void**) (&wGuiTreeNodeGetNextSibling) = load_function(ws3dHandle, "wGuiTreeNodeGetNextSibling");
	*(void**) (&wGuiTreeNodeGetNextVisible) = load_function(ws3dHandle, "wGuiTreeNodeGetNextVisible");
	*(void**) (&wGuiTreeNodeGetOwner) = load_function(ws3dHandle, "wGuiTreeNodeGetOwner");
	*(void**) (&wGuiTreeNodeGetParent) = load_function(ws3dHandle, "wGuiTreeNodeGetParent");
	*(void**) (&wGuiTreeNodeGetPrevSibling) = load_function(ws3dHandle, "wGuiTreeNodeGetPrevSibling");
	*(void**) (&wGuiTreeNodeGetSelectedImageIndex) = load_function(ws3dHandle, "wGuiTreeNodeGetSelectedImageIndex");
	*(void**) (&wGuiTreeNodeGetText) = load_function(ws3dHandle, "wGuiTreeNodeGetText");
	*(void**) (&wGuiTreeNodeHasChildren) = load_function(ws3dHandle, "wGuiTreeNodeHasChildren");
	*(void**) (&wGuiTreeNodeInsertChildAfter) = load_function(ws3dHandle, "wGuiTreeNodeInsertChildAfter");
	*(void**) (&wGuiTreeNodeInsertChildBefore) = load_function(ws3dHandle, "wGuiTreeNodeInsertChildBefore");
	*(void**) (&wGuiTreeNodeIsExpanded) = load_function(ws3dHandle, "wGuiTreeNodeIsExpanded");
	*(void**) (&wGuiTreeNodeIsRoot) = load_function(ws3dHandle, "wGuiTreeNodeIsRoot");
	*(void**) (&wGuiTreeNodeIsSelected) = load_function(ws3dHandle, "wGuiTreeNodeIsSelected");
	*(void**) (&wGuiTreeNodeIsVisible) = load_function(ws3dHandle, "wGuiTreeNodeIsVisible");
	*(void**) (&wGuiTreeNodeMoveChildDown) = load_function(ws3dHandle, "wGuiTreeNodeMoveChildDown");
	*(void**) (&wGuiTreeNodeMoveChildUp) = load_function(ws3dHandle, "wGuiTreeNodeMoveChildUp");
	*(void**) (&wGuiTreeNodeRemoveChild) = load_function(ws3dHandle, "wGuiTreeNodeRemoveChild");
	*(void**) (&wGuiTreeNodeRemoveChildren) = load_function(ws3dHandle, "wGuiTreeNodeRemoveChildren");
	*(void**) (&wGuiTreeNodeSetData) = load_function(ws3dHandle, "wGuiTreeNodeSetData");
	*(void**) (&wGuiTreeNodeSetData2) = load_function(ws3dHandle, "wGuiTreeNodeSetData2");
	*(void**) (&wGuiTreeNodeSetExpanded) = load_function(ws3dHandle, "wGuiTreeNodeSetExpanded");
	*(void**) (&wGuiTreeNodeSetIcon) = load_function(ws3dHandle, "wGuiTreeNodeSetIcon");
	*(void**) (&wGuiTreeNodeSetImageIndex) = load_function(ws3dHandle, "wGuiTreeNodeSetImageIndex");
	*(void**) (&wGuiTreeNodeSetSelected) = load_function(ws3dHandle, "wGuiTreeNodeSetSelected");
	*(void**) (&wGuiTreeNodeSetSelectedImageIndex) = load_function(ws3dHandle, "wGuiTreeNodeSetSelectedImageIndex");
	*(void**) (&wGuiTreeNodeSetText) = load_function(ws3dHandle, "wGuiTreeNodeSetText");
	*(void**) (&wGuiTreeSetIconFont) = load_function(ws3dHandle, "wGuiTreeSetIconFont");
	*(void**) (&wGuiTreeSetImageLeftOfIcon) = load_function(ws3dHandle, "wGuiTreeSetImageLeftOfIcon");
	*(void**) (&wGuiTreeSetImageList) = load_function(ws3dHandle, "wGuiTreeSetImageList");
	*(void**) (&wGuiTreeSetLinesVisible) = load_function(ws3dHandle, "wGuiTreeSetLinesVisible");
	*(void**) (&wGuiWindowCreate) = load_function(ws3dHandle, "wGuiWindowCreate");
	*(void**) (&wGuiWindowGetButtonClose) = load_function(ws3dHandle, "wGuiWindowGetButtonClose");
	*(void**) (&wGuiWindowGetButtonMaximize) = load_function(ws3dHandle, "wGuiWindowGetButtonMaximize");
	*(void**) (&wGuiWindowGetButtonMinimize) = load_function(ws3dHandle, "wGuiWindowGetButtonMinimize");
	*(void**) (&wGuiWindowIsDraggable) = load_function(ws3dHandle, "wGuiWindowIsDraggable");
	*(void**) (&wGuiWindowIsDrawBackground) = load_function(ws3dHandle, "wGuiWindowIsDrawBackground");
	*(void**) (&wGuiWindowIsDrawTitleBar) = load_function(ws3dHandle, "wGuiWindowIsDrawTitleBar");
	*(void**) (&wGuiWindowSetDraggable) = load_function(ws3dHandle, "wGuiWindowSetDraggable");
	*(void**) (&wGuiWindowSetDrawBackground) = load_function(ws3dHandle, "wGuiWindowSetDrawBackground");
	*(void**) (&wGuiWindowSetDrawTitleBar) = load_function(ws3dHandle, "wGuiWindowSetDrawTitleBar");
	*(void**) (&wImageConvertToTexture) = load_function(ws3dHandle, "wImageConvertToTexture");
	*(void**) (&wImageCreate) = load_function(ws3dHandle, "wImageCreate");
	*(void**) (&wImageDestroy) = load_function(ws3dHandle, "wImageDestroy");
	*(void**) (&wImageGetInformation) = load_function(ws3dHandle, "wImageGetInformation");
	*(void**) (&wImageGetPixelColor) = load_function(ws3dHandle, "wImageGetPixelColor");
	*(void**) (&wImageLoad) = load_function(ws3dHandle, "wImageLoad");
	*(void**) (&wImageLock) = load_function(ws3dHandle, "wImageLock");
	*(void**) (&wImageSave) = load_function(ws3dHandle, "wImageSave");
	*(void**) (&wImageSetPixelColor) = load_function(ws3dHandle, "wImageSetPixelColor");
	*(void**) (&wImageUnlock) = load_function(ws3dHandle, "wImageUnlock");
	*(void**) (&wInputActivateJoystick) = load_function(ws3dHandle, "wInputActivateJoystick");
	*(void**) (&wInputGetJoysitcksCount) = load_function(ws3dHandle, "wInputGetJoysitcksCount");
	*(void**) (&wInputGetJoystickInfo) = load_function(ws3dHandle, "wInputGetJoystickInfo");
	*(void**) (&wInputGetMouseDelta) = load_function(ws3dHandle, "wInputGetMouseDelta");
	*(void**) (&wInputGetMouseDeltaX) = load_function(ws3dHandle, "wInputGetMouseDeltaX");
	*(void**) (&wInputGetMouseDeltaY) = load_function(ws3dHandle, "wInputGetMouseDeltaY");
	*(void**) (&wInputGetMouseLogicalPosition) = load_function(ws3dHandle, "wInputGetMouseLogicalPosition");
	*(void**) (&wInputGetMousePosition) = load_function(ws3dHandle, "wInputGetMousePosition");
	*(void**) (&wInputGetMouseWheel) = load_function(ws3dHandle, "wInputGetMouseWheel");
	*(void**) (&wInputGetMouseX) = load_function(ws3dHandle, "wInputGetMouseX");
	*(void**) (&wInputGetMouseY) = load_function(ws3dHandle, "wInputGetMouseY");
	*(void**) (&wInputIsCursorVisible) = load_function(ws3dHandle, "wInputIsCursorVisible");
	*(void**) (&wInputIsJoystickEventAvailable) = load_function(ws3dHandle, "wInputIsJoystickEventAvailable");
	*(void**) (&wInputIsKeyEventAvailable) = load_function(ws3dHandle, "wInputIsKeyEventAvailable");
	*(void**) (&wInputIsKeyHit) = load_function(ws3dHandle, "wInputIsKeyHit");
	*(void**) (&wInputIsKeyPressed) = load_function(ws3dHandle, "wInputIsKeyPressed");
	*(void**) (&wInputIsKeyUp) = load_function(ws3dHandle, "wInputIsKeyUp");
	*(void**) (&wInputIsMouseEventAvailable) = load_function(ws3dHandle, "wInputIsMouseEventAvailable");
	*(void**) (&wInputIsMouseHit) = load_function(ws3dHandle, "wInputIsMouseHit");
	*(void**) (&wInputIsMousePressed) = load_function(ws3dHandle, "wInputIsMousePressed");
	*(void**) (&wInputIsMouseUp) = load_function(ws3dHandle, "wInputIsMouseUp");
	*(void**) (&wInputReadJoystickEvent) = load_function(ws3dHandle, "wInputReadJoystickEvent");
	*(void**) (&wInputReadKeyEvent) = load_function(ws3dHandle, "wInputReadKeyEvent");
	*(void**) (&wInputReadMouseEvent) = load_function(ws3dHandle, "wInputReadMouseEvent");
	*(void**) (&wInputSetCursorVisible) = load_function(ws3dHandle, "wInputSetCursorVisible");
	*(void**) (&wInputSetMouseLogicalPosition) = load_function(ws3dHandle, "wInputSetMouseLogicalPosition");
	*(void**) (&wInputSetMousePosition) = load_function(ws3dHandle, "wInputSetMousePosition");
	*(void**) (&wInputWaitKey) = load_function(ws3dHandle, "wInputWaitKey");
	*(void**) (&wLensFlareCreate) = load_function(ws3dHandle, "wLensFlareCreate");
	*(void**) (&wLensFlareGetStrength) = load_function(ws3dHandle, "wLensFlareGetStrength");
	*(void**) (&wLensFlareSetStrength) = load_function(ws3dHandle, "wLensFlareSetStrength");
	*(void**) (&wLightCreate) = load_function(ws3dHandle, "wLightCreate");
	*(void**) (&wLightGetAmbientColor) = load_function(ws3dHandle, "wLightGetAmbientColor");
	*(void**) (&wLightGetAttenuation) = load_function(ws3dHandle, "wLightGetAttenuation");
	*(void**) (&wLightGetDiffuseColor) = load_function(ws3dHandle, "wLightGetDiffuseColor");
	*(void**) (&wLightGetDirection) = load_function(ws3dHandle, "wLightGetDirection");
	*(void**) (&wLightGetFallOff) = load_function(ws3dHandle, "wLightGetFallOff");
	*(void**) (&wLightGetInnerCone) = load_function(ws3dHandle, "wLightGetInnerCone");
	*(void**) (&wLightGetOuterCone) = load_function(ws3dHandle, "wLightGetOuterCone");
	*(void**) (&wLightGetRadius) = load_function(ws3dHandle, "wLightGetRadius");
	*(void**) (&wLightGetSpecularColor) = load_function(ws3dHandle, "wLightGetSpecularColor");
	*(void**) (&wLightGetType) = load_function(ws3dHandle, "wLightGetType");
	*(void**) (&wLightIsCastShadows) = load_function(ws3dHandle, "wLightIsCastShadows");
	*(void**) (&wLightSetAmbientColor) = load_function(ws3dHandle, "wLightSetAmbientColor");
	*(void**) (&wLightSetAttenuation) = load_function(ws3dHandle, "wLightSetAttenuation");
	*(void**) (&wLightSetCastShadows) = load_function(ws3dHandle, "wLightSetCastShadows");
	*(void**) (&wLightSetDiffuseColor) = load_function(ws3dHandle, "wLightSetDiffuseColor");
	*(void**) (&wLightSetFallOff) = load_function(ws3dHandle, "wLightSetFallOff");
	*(void**) (&wLightSetInnerCone) = load_function(ws3dHandle, "wLightSetInnerCone");
	*(void**) (&wLightSetOuterCone) = load_function(ws3dHandle, "wLightSetOuterCone");
	*(void**) (&wLightSetRadius) = load_function(ws3dHandle, "wLightSetRadius");
	*(void**) (&wLightSetSpecularColor) = load_function(ws3dHandle, "wLightSetSpecularColor");
	*(void**) (&wLightSetType) = load_function(ws3dHandle, "wLightSetType");
	*(void**) (&wListBoxClearElementColor) = load_function(ws3dHandle, "wListBoxClearElementColor");
	*(void**) (&wLodManagerAddMesh) = load_function(ws3dHandle, "wLodManagerAddMesh");
	*(void**) (&wLodManagerCreate) = load_function(ws3dHandle, "wLodManagerCreate");
	*(void**) (&wLodManagerSetMaterialMap) = load_function(ws3dHandle, "wLodManagerSetMaterialMap");
	*(void**) (&wLogClear) = load_function(ws3dHandle, "wLogClear");
	*(void**) (&wLogSetFile) = load_function(ws3dHandle, "wLogSetFile");
	*(void**) (&wLogSetLevel) = load_function(ws3dHandle, "wLogSetLevel");
	*(void**) (&wLogWrite) = load_function(ws3dHandle, "wLogWrite");
	*(void**) (&wMaterialGetAmbientColor) = load_function(ws3dHandle, "wMaterialGetAmbientColor");
	*(void**) (&wMaterialGetAntiAliasingMode) = load_function(ws3dHandle, "wMaterialGetAntiAliasingMode");
	*(void**) (&wMaterialGetColorMask) = load_function(ws3dHandle, "wMaterialGetColorMask");
	*(void**) (&wMaterialGetDiffuseColor) = load_function(ws3dHandle, "wMaterialGetDiffuseColor");
	*(void**) (&wMaterialGetEmissiveColor) = load_function(ws3dHandle, "wMaterialGetEmissiveColor");
	*(void**) (&wMaterialGetFlag) = load_function(ws3dHandle, "wMaterialGetFlag");
	*(void**) (&wMaterialGetLineThickness) = load_function(ws3dHandle, "wMaterialGetLineThickness");
	*(void**) (&wMaterialGetShininess) = load_function(ws3dHandle, "wMaterialGetShininess");
	*(void**) (&wMaterialGetSpecularColor) = load_function(ws3dHandle, "wMaterialGetSpecularColor");
	*(void**) (&wMaterialGetTexture) = load_function(ws3dHandle, "wMaterialGetTexture");
	*(void**) (&wMaterialGetTextureLodBias) = load_function(ws3dHandle, "wMaterialGetTextureLodBias");
	*(void**) (&wMaterialGetTextureWrapUMode) = load_function(ws3dHandle, "wMaterialGetTextureWrapUMode");
	*(void**) (&wMaterialGetTextureWrapVMode) = load_function(ws3dHandle, "wMaterialGetTextureWrapVMode");
	*(void**) (&wMaterialGetTypeParameter) = load_function(ws3dHandle, "wMaterialGetTypeParameter");
	*(void**) (&wMaterialGetTypeParameter2) = load_function(ws3dHandle, "wMaterialGetTypeParameter2");
	*(void**) (&wMaterialGetVertexColoringMode) = load_function(ws3dHandle, "wMaterialGetVertexColoringMode");
	*(void**) (&wMaterialRotateTexture) = load_function(ws3dHandle, "wMaterialRotateTexture");
	*(void**) (&wMaterialScaleTexture) = load_function(ws3dHandle, "wMaterialScaleTexture");
	*(void**) (&wMaterialScaleTextureFromCenter) = load_function(ws3dHandle, "wMaterialScaleTextureFromCenter");
	*(void**) (&wMaterialSetAmbientColor) = load_function(ws3dHandle, "wMaterialSetAmbientColor");
	*(void**) (&wMaterialSetAntiAliasingMode) = load_function(ws3dHandle, "wMaterialSetAntiAliasingMode");
	*(void**) (&wMaterialSetBlendingMode) = load_function(ws3dHandle, "wMaterialSetBlendingMode");
	*(void**) (&wMaterialSetColorMask) = load_function(ws3dHandle, "wMaterialSetColorMask");
	*(void**) (&wMaterialSetDiffuseColor) = load_function(ws3dHandle, "wMaterialSetDiffuseColor");
	*(void**) (&wMaterialSetEmissiveColor) = load_function(ws3dHandle, "wMaterialSetEmissiveColor");
	*(void**) (&wMaterialSetFlag) = load_function(ws3dHandle, "wMaterialSetFlag");
	*(void**) (&wMaterialSetLineThickness) = load_function(ws3dHandle, "wMaterialSetLineThickness");
	*(void**) (&wMaterialSetShininess) = load_function(ws3dHandle, "wMaterialSetShininess");
	*(void**) (&wMaterialSetSpecularColor) = load_function(ws3dHandle, "wMaterialSetSpecularColor");
	*(void**) (&wMaterialSetTexture) = load_function(ws3dHandle, "wMaterialSetTexture");
	*(void**) (&wMaterialSetTextureLodBias) = load_function(ws3dHandle, "wMaterialSetTextureLodBias");
	*(void**) (&wMaterialSetTextureWrapUMode) = load_function(ws3dHandle, "wMaterialSetTextureWrapUMode");
	*(void**) (&wMaterialSetTextureWrapVMode) = load_function(ws3dHandle, "wMaterialSetTextureWrapVMode");
	*(void**) (&wMaterialSetType) = load_function(ws3dHandle, "wMaterialSetType");
	*(void**) (&wMaterialSetTypeParameter) = load_function(ws3dHandle, "wMaterialSetTypeParameter");
	*(void**) (&wMaterialSetTypeParameter2) = load_function(ws3dHandle, "wMaterialSetTypeParameter2");
	*(void**) (&wMaterialSetVertexColoringMode) = load_function(ws3dHandle, "wMaterialSetVertexColoringMode");
	*(void**) (&wMaterialTranslateTexture) = load_function(ws3dHandle, "wMaterialTranslateTexture");
	*(void**) (&wMaterialTranslateTextureTransposed) = load_function(ws3dHandle, "wMaterialTranslateTextureTransposed");
	*(void**) (&wMathCeil) = load_function(ws3dHandle, "wMathCeil");
	*(void**) (&wMathDegToRad) = load_function(ws3dHandle, "wMathDegToRad");
	*(void**) (&wMathFloatEquals) = load_function(ws3dHandle, "wMathFloatEquals");
	*(void**) (&wMathFloatIsZero) = load_function(ws3dHandle, "wMathFloatIsZero");
	*(void**) (&wMathFloatMax2) = load_function(ws3dHandle, "wMathFloatMax2");
	*(void**) (&wMathFloatMax3) = load_function(ws3dHandle, "wMathFloatMax3");
	*(void**) (&wMathFloatMin2) = load_function(ws3dHandle, "wMathFloatMin2");
	*(void**) (&wMathFloatMin3) = load_function(ws3dHandle, "wMathFloatMin3");
	*(void**) (&wMathFloor) = load_function(ws3dHandle, "wMathFloor");
	*(void**) (&wMathIntEquals) = load_function(ws3dHandle, "wMathIntEquals");
	*(void**) (&wMathIntIsZero) = load_function(ws3dHandle, "wMathIntIsZero");
	*(void**) (&wMathIntMax2) = load_function(ws3dHandle, "wMathIntMax2");
	*(void**) (&wMathIntMax3) = load_function(ws3dHandle, "wMathIntMax3");
	*(void**) (&wMathIntMin2) = load_function(ws3dHandle, "wMathIntMin2");
	*(void**) (&wMathIntMin3) = load_function(ws3dHandle, "wMathIntMin3");
	*(void**) (&wMathRadToDeg) = load_function(ws3dHandle, "wMathRadToDeg");
	*(void**) (&wMathRandomRange) = load_function(ws3dHandle, "wMathRandomRange");
	*(void**) (&wMathRound) = load_function(ws3dHandle, "wMathRound");
	*(void**) (&wMathUIntEquals) = load_function(ws3dHandle, "wMathUIntEquals");
	*(void**) (&wMathUIntIsZero) = load_function(ws3dHandle, "wMathUIntIsZero");
	*(void**) (&wMathVector3fAdd) = load_function(ws3dHandle, "wMathVector3fAdd");
	*(void**) (&wMathVector3fCrossProduct) = load_function(ws3dHandle, "wMathVector3fCrossProduct");
	*(void**) (&wMathVector3fDotProduct) = load_function(ws3dHandle, "wMathVector3fDotProduct");
	*(void**) (&wMathVector3fGetDistanceFrom) = load_function(ws3dHandle, "wMathVector3fGetDistanceFrom");
	*(void**) (&wMathVector3fGetHorizontalAngle) = load_function(ws3dHandle, "wMathVector3fGetHorizontalAngle");
	*(void**) (&wMathVector3fGetLength) = load_function(ws3dHandle, "wMathVector3fGetLength");
	*(void**) (&wMathVector3fInterpolate) = load_function(ws3dHandle, "wMathVector3fInterpolate");
	*(void**) (&wMathVector3fInvert) = load_function(ws3dHandle, "wMathVector3fInvert");
	*(void**) (&wMathVector3fNormalize) = load_function(ws3dHandle, "wMathVector3fNormalize");
	*(void**) (&wMathVector3fSubstract) = load_function(ws3dHandle, "wMathVector3fSubstract");
	*(void**) (&wMayaCameraCreate) = load_function(ws3dHandle, "wMayaCameraCreate");
	*(void**) (&wMeshAddMeshBuffer) = load_function(ws3dHandle, "wMeshAddMeshBuffer");
	*(void**) (&wMeshAddToBatching) = load_function(ws3dHandle, "wMeshAddToBatching");
	*(void**) (&wMeshBufferAddToBatching) = load_function(ws3dHandle, "wMeshBufferAddToBatching");
	*(void**) (&wMeshBufferCreate) = load_function(ws3dHandle, "wMeshBufferCreate");
	*(void**) (&wMeshBufferDestroy) = load_function(ws3dHandle, "wMeshBufferDestroy");
	*(void**) (&wMeshBufferGetMaterial) = load_function(ws3dHandle, "wMeshBufferGetMaterial");
	*(void**) (&wMeshClearBatching) = load_function(ws3dHandle, "wMeshClearBatching");
	*(void**) (&wMeshCreate) = load_function(ws3dHandle, "wMeshCreate");
	*(void**) (&wMeshCreateArrow) = load_function(ws3dHandle, "wMeshCreateArrow");
	*(void**) (&wMeshCreateBatching) = load_function(ws3dHandle, "wMeshCreateBatching");
	*(void**) (&wMeshCreateCube) = load_function(ws3dHandle, "wMeshCreateCube");
	*(void**) (&wMeshCreateHillPlane) = load_function(ws3dHandle, "wMeshCreateHillPlane");
	*(void**) (&wMeshCreateSphere) = load_function(ws3dHandle, "wMeshCreateSphere");
	*(void**) (&wMeshCreateStaticWithTangents) = load_function(ws3dHandle, "wMeshCreateStaticWithTangents");
	*(void**) (&wMeshDestroy) = load_function(ws3dHandle, "wMeshDestroy");
	*(void**) (&wMeshDestroyBatching) = load_function(ws3dHandle, "wMeshDestroyBatching");
	*(void**) (&wMeshDuplicate) = load_function(ws3dHandle, "wMeshDuplicate");
	*(void**) (&wMeshEnableHardwareAcceleration) = load_function(ws3dHandle, "wMeshEnableHardwareAcceleration");
	*(void**) (&wMeshFinalizeBatching) = load_function(ws3dHandle, "wMeshFinalizeBatching");
	*(void**) (&wMeshFit) = load_function(ws3dHandle, "wMeshFit");
	*(void**) (&wMeshFlipSurface) = load_function(ws3dHandle, "wMeshFlipSurface");
	*(void**) (&wMeshGetBoundingBox) = load_function(ws3dHandle, "wMeshGetBoundingBox");
	*(void**) (&wMeshGetBuffer) = load_function(ws3dHandle, "wMeshGetBuffer");
	*(void**) (&wMeshGetBuffersCount) = load_function(ws3dHandle, "wMeshGetBuffersCount");
	*(void**) (&wMeshGetFramesCount) = load_function(ws3dHandle, "wMeshGetFramesCount");
	*(void**) (&wMeshGetIndices) = load_function(ws3dHandle, "wMeshGetIndices");
	*(void**) (&wMeshGetIndicesCount) = load_function(ws3dHandle, "wMeshGetIndicesCount");
	*(void**) (&wMeshGetName) = load_function(ws3dHandle, "wMeshGetName");
	*(void**) (&wMeshGetType) = load_function(ws3dHandle, "wMeshGetType");
	*(void**) (&wMeshGetVertices) = load_function(ws3dHandle, "wMeshGetVertices");
	*(void**) (&wMeshGetVerticesCount) = load_function(ws3dHandle, "wMeshGetVerticesCount");
	*(void**) (&wMeshGetVerticesMemory) = load_function(ws3dHandle, "wMeshGetVerticesMemory");
	*(void**) (&wMeshIsEmpty) = load_function(ws3dHandle, "wMeshIsEmpty");
	*(void**) (&wMeshLoad) = load_function(ws3dHandle, "wMeshLoad");
	*(void**) (&wMeshMakePlanarTextureMapping) = load_function(ws3dHandle, "wMeshMakePlanarTextureMapping");
	*(void**) (&wMeshMakePlanarTextureMappingAdvanced) = load_function(ws3dHandle, "wMeshMakePlanarTextureMappingAdvanced");
	*(void**) (&wMeshRecalculateNormals) = load_function(ws3dHandle, "wMeshRecalculateNormals");
	*(void**) (&wMeshRecalculateTangents) = load_function(ws3dHandle, "wMeshRecalculateTangents");
	*(void**) (&wMeshSave) = load_function(ws3dHandle, "wMeshSave");
	*(void**) (&wMeshSetIndices) = load_function(ws3dHandle, "wMeshSetIndices");
	*(void**) (&wMeshSetName) = load_function(ws3dHandle, "wMeshSetName");
	*(void**) (&wMeshSetRotation) = load_function(ws3dHandle, "wMeshSetRotation");
	*(void**) (&wMeshSetScale) = load_function(ws3dHandle, "wMeshSetScale");
	*(void**) (&wMeshSetVertices) = load_function(ws3dHandle, "wMeshSetVertices");
	*(void**) (&wMeshSetVerticesAlpha) = load_function(ws3dHandle, "wMeshSetVerticesAlpha");
	*(void**) (&wMeshSetVerticesColors) = load_function(ws3dHandle, "wMeshSetVerticesColors");
	*(void**) (&wMeshSetVerticesCoords) = load_function(ws3dHandle, "wMeshSetVerticesCoords");
	*(void**) (&wMeshSetVerticesSingleColor) = load_function(ws3dHandle, "wMeshSetVerticesSingleColor");
	*(void**) (&wMeshUpdateBatching) = load_function(ws3dHandle, "wMeshUpdateBatching");
	*(void**) (&wMeshUpdateTangentsAndBinormals) = load_function(ws3dHandle, "wMeshUpdateTangentsAndBinormals");
	*(void**) (&wNetClientCreate) = load_function(ws3dHandle, "wNetClientCreate");
	*(void**) (&wNetClientDisconnect) = load_function(ws3dHandle, "wNetClientDisconnect");
	*(void**) (&wNetClientIsConnected) = load_function(ws3dHandle, "wNetClientIsConnected");
	*(void**) (&wNetClientSendMessage) = load_function(ws3dHandle, "wNetClientSendMessage");
	*(void**) (&wNetClientSendPacket) = load_function(ws3dHandle, "wNetClientSendPacket");
	*(void**) (&wNetClientStop) = load_function(ws3dHandle, "wNetClientStop");
	*(void**) (&wNetClientUpdate) = load_function(ws3dHandle, "wNetClientUpdate");
	*(void**) (&wNetManagerDestroyAllPackets) = load_function(ws3dHandle, "wNetManagerDestroyAllPackets");
	*(void**) (&wNetManagerGetMessageId) = load_function(ws3dHandle, "wNetManagerGetMessageId");
	*(void**) (&wNetManagerGetPacketsCount) = load_function(ws3dHandle, "wNetManagerGetPacketsCount");
	*(void**) (&wNetManagerSetMessageId) = load_function(ws3dHandle, "wNetManagerSetMessageId");
	*(void**) (&wNetManagerSetVerbose) = load_function(ws3dHandle, "wNetManagerSetVerbose");
	*(void**) (&wNetPacketCreate) = load_function(ws3dHandle, "wNetPacketCreate");
	*(void**) (&wNetPacketGetClientIp) = load_function(ws3dHandle, "wNetPacketGetClientIp");
	*(void**) (&wNetPacketGetClientPort) = load_function(ws3dHandle, "wNetPacketGetClientPort");
	*(void**) (&wNetPacketGetClientPtr) = load_function(ws3dHandle, "wNetPacketGetClientPtr");
	*(void**) (&wNetPacketGetId) = load_function(ws3dHandle, "wNetPacketGetId");
	*(void**) (&wNetPacketReadFloat) = load_function(ws3dHandle, "wNetPacketReadFloat");
	*(void**) (&wNetPacketReadInt) = load_function(ws3dHandle, "wNetPacketReadInt");
	*(void**) (&wNetPacketReadMessage) = load_function(ws3dHandle, "wNetPacketReadMessage");
	*(void**) (&wNetPacketReadString) = load_function(ws3dHandle, "wNetPacketReadString");
	*(void**) (&wNetPacketReadUint) = load_function(ws3dHandle, "wNetPacketReadUint");
	*(void**) (&wNetPacketWriteFloat) = load_function(ws3dHandle, "wNetPacketWriteFloat");
	*(void**) (&wNetPacketWriteInt) = load_function(ws3dHandle, "wNetPacketWriteInt");
	*(void**) (&wNetPacketWriteString) = load_function(ws3dHandle, "wNetPacketWriteString");
	*(void**) (&wNetPacketWriteUInt) = load_function(ws3dHandle, "wNetPacketWriteUInt");
	*(void**) (&wNetServerAcceptNewConnections) = load_function(ws3dHandle, "wNetServerAcceptNewConnections");
	*(void**) (&wNetServerBroadcastMessage) = load_function(ws3dHandle, "wNetServerBroadcastMessage");
	*(void**) (&wNetServerClear) = load_function(ws3dHandle, "wNetServerClear");
	*(void**) (&wNetServerClearBannedList) = load_function(ws3dHandle, "wNetServerClearBannedList");
	*(void**) (&wNetServerCreate) = load_function(ws3dHandle, "wNetServerCreate");
	*(void**) (&wNetServerGetClientsCount) = load_function(ws3dHandle, "wNetServerGetClientsCount");
	*(void**) (&wNetServerKickClient) = load_function(ws3dHandle, "wNetServerKickClient");
	*(void**) (&wNetServerSendPacket) = load_function(ws3dHandle, "wNetServerSendPacket");
	*(void**) (&wNetServerStop) = load_function(ws3dHandle, "wNetServerStop");
	*(void**) (&wNetServerUnKickClient) = load_function(ws3dHandle, "wNetServerUnKickClient");
	*(void**) (&wNetServerUpdate) = load_function(ws3dHandle, "wNetServerUpdate");
	*(void**) (&wNodeAddCollision) = load_function(ws3dHandle, "wNodeAddCollision");
	*(void**) (&wNodeAddShadowVolume) = load_function(ws3dHandle, "wNodeAddShadowVolume");
	*(void**) (&wNodeAddShadowVolumeFromMeshBuffer) = load_function(ws3dHandle, "wNodeAddShadowVolumeFromMeshBuffer");
	*(void**) (&wNodeAnimateJoints) = load_function(ws3dHandle, "wNodeAnimateJoints");
	*(void**) (&wNodeCreateCone) = load_function(ws3dHandle, "wNodeCreateCone");
	*(void**) (&wNodeCreateCube) = load_function(ws3dHandle, "wNodeCreateCube");
	*(void**) (&wNodeCreateCylinder) = load_function(ws3dHandle, "wNodeCreateCylinder");
	*(void**) (&wNodeCreateEmpty) = load_function(ws3dHandle, "wNodeCreateEmpty");
	*(void**) (&wNodeCreateFromBatchingMesh) = load_function(ws3dHandle, "wNodeCreateFromBatchingMesh");
	*(void**) (&wNodeCreateFromBatchingMeshAsOctree) = load_function(ws3dHandle, "wNodeCreateFromBatchingMeshAsOctree");
	*(void**) (&wNodeCreateFromMesh) = load_function(ws3dHandle, "wNodeCreateFromMesh");
	*(void**) (&wNodeCreateFromMeshAsOctree) = load_function(ws3dHandle, "wNodeCreateFromMeshAsOctree");
	*(void**) (&wNodeCreateFromStaticMesh) = load_function(ws3dHandle, "wNodeCreateFromStaticMesh");
	*(void**) (&wNodeCreatePlane) = load_function(ws3dHandle, "wNodeCreatePlane");
	*(void**) (&wNodeCreateSphere) = load_function(ws3dHandle, "wNodeCreateSphere");
	*(void**) (&wNodeDestroy) = load_function(ws3dHandle, "wNodeDestroy");
	*(void**) (&wNodeDestroyAllAnimators) = load_function(ws3dHandle, "wNodeDestroyAllAnimators");
	*(void**) (&wNodeDraw) = load_function(ws3dHandle, "wNodeDraw");
	*(void**) (&wNodeDrawBoundingBox) = load_function(ws3dHandle, "wNodeDrawBoundingBox");
	*(void**) (&wNodeDuplicate) = load_function(ws3dHandle, "wNodeDuplicate");
	*(void**) (&wNodeGetAbsolutePosition) = load_function(ws3dHandle, "wNodeGetAbsolutePosition");
	*(void**) (&wNodeGetAbsoluteRotation) = load_function(ws3dHandle, "wNodeGetAbsoluteRotation");
	*(void**) (&wNodeGetAnimationFrame) = load_function(ws3dHandle, "wNodeGetAnimationFrame");
	*(void**) (&wNodeGetAnimatorByIndex) = load_function(ws3dHandle, "wNodeGetAnimatorByIndex");
	*(void**) (&wNodeGetAnimatorsCount) = load_function(ws3dHandle, "wNodeGetAnimatorsCount");
	*(void**) (&wNodeGetBoundingBox) = load_function(ws3dHandle, "wNodeGetBoundingBox");
	*(void**) (&wNodeGetChildsCount) = load_function(ws3dHandle, "wNodeGetChildsCount");
	*(void**) (&wNodeGetFirstAnimator) = load_function(ws3dHandle, "wNodeGetFirstAnimator");
	*(void**) (&wNodeGetFirstChild) = load_function(ws3dHandle, "wNodeGetFirstChild");
	*(void**) (&wNodeGetId) = load_function(ws3dHandle, "wNodeGetId");
	*(void**) (&wNodeGetJointById) = load_function(ws3dHandle, "wNodeGetJointById");
	*(void**) (&wNodeGetJointByName) = load_function(ws3dHandle, "wNodeGetJointByName");
	*(void**) (&wNodeGetJointSkinningSpace) = load_function(ws3dHandle, "wNodeGetJointSkinningSpace");
	*(void**) (&wNodeGetJointsCount) = load_function(ws3dHandle, "wNodeGetJointsCount");
	*(void**) (&wNodeGetLastAnimator) = load_function(ws3dHandle, "wNodeGetLastAnimator");
	*(void**) (&wNodeGetMaterial) = load_function(ws3dHandle, "wNodeGetMaterial");
	*(void**) (&wNodeGetMaterialsCount) = load_function(ws3dHandle, "wNodeGetMaterialsCount");
	*(void**) (&wNodeGetMesh) = load_function(ws3dHandle, "wNodeGetMesh");
	*(void**) (&wNodeGetName) = load_function(ws3dHandle, "wNodeGetName");
	*(void**) (&wNodeGetNextChild) = load_function(ws3dHandle, "wNodeGetNextChild");
	*(void**) (&wNodeGetParent) = load_function(ws3dHandle, "wNodeGetParent");
	*(void**) (&wNodeGetPosition) = load_function(ws3dHandle, "wNodeGetPosition");
	*(void**) (&wNodeGetRotation) = load_function(ws3dHandle, "wNodeGetRotation");
	*(void**) (&wNodeGetScale) = load_function(ws3dHandle, "wNodeGetScale");
	*(void**) (&wNodeGetTransformedBoundingBox) = load_function(ws3dHandle, "wNodeGetTransformedBoundingBox");
	*(void**) (&wNodeGetType) = load_function(ws3dHandle, "wNodeGetType");
	*(void**) (&wNodeGetUserData) = load_function(ws3dHandle, "wNodeGetUserData");
	*(void**) (&wNodeIsInView) = load_function(ws3dHandle, "wNodeIsInView");
	*(void**) (&wNodeIsLastChild) = load_function(ws3dHandle, "wNodeIsLastChild");
	*(void**) (&wNodeIsPointInside) = load_function(ws3dHandle, "wNodeIsPointInside");
	*(void**) (&wNodeIsReadOnlyMaterials) = load_function(ws3dHandle, "wNodeIsReadOnlyMaterials");
	*(void**) (&wNodeIsVisible) = load_function(ws3dHandle, "wNodeIsVisible");
	*(void**) (&wNodeMove) = load_function(ws3dHandle, "wNodeMove");
	*(void**) (&wNodeOnAnimate) = load_function(ws3dHandle, "wNodeOnAnimate");
	*(void**) (&wNodePlayMD2Animation) = load_function(ws3dHandle, "wNodePlayMD2Animation");
	*(void**) (&wNodeRemoveCollision) = load_function(ws3dHandle, "wNodeRemoveCollision");
	*(void**) (&wNodeRotateToNode) = load_function(ws3dHandle, "wNodeRotateToNode");
	*(void**) (&wNodeSetAbsoluteRotation) = load_function(ws3dHandle, "wNodeSetAbsoluteRotation");
	*(void**) (&wNodeSetAnimationFrame) = load_function(ws3dHandle, "wNodeSetAnimationFrame");
	*(void**) (&wNodeSetAnimationLoopMode) = load_function(ws3dHandle, "wNodeSetAnimationLoopMode");
	*(void**) (&wNodeSetAnimationRange) = load_function(ws3dHandle, "wNodeSetAnimationRange");
	*(void**) (&wNodeSetAnimationSpeed) = load_function(ws3dHandle, "wNodeSetAnimationSpeed");
	*(void**) (&wNodeSetCullingState) = load_function(ws3dHandle, "wNodeSetCullingState");
	*(void**) (&wNodeSetDebugDataVisible) = load_function(ws3dHandle, "wNodeSetDebugDataVisible");
	*(void**) (&wNodeSetDebugMode) = load_function(ws3dHandle, "wNodeSetDebugMode");
	*(void**) (&wNodeSetDecalsEnabled) = load_function(ws3dHandle, "wNodeSetDecalsEnabled");
	*(void**) (&wNodeSetId) = load_function(ws3dHandle, "wNodeSetId");
	*(void**) (&wNodeSetJointMode) = load_function(ws3dHandle, "wNodeSetJointMode");
	*(void**) (&wNodeSetJointSkinningSpace) = load_function(ws3dHandle, "wNodeSetJointSkinningSpace");
	*(void**) (&wNodeSetMesh) = load_function(ws3dHandle, "wNodeSetMesh");
	*(void**) (&wNodeSetName) = load_function(ws3dHandle, "wNodeSetName");
	*(void**) (&wNodeSetParent) = load_function(ws3dHandle, "wNodeSetParent");
	*(void**) (&wNodeSetPosition) = load_function(ws3dHandle, "wNodeSetPosition");
	*(void**) (&wNodeSetReadOnlyMaterials) = load_function(ws3dHandle, "wNodeSetReadOnlyMaterials");
	*(void**) (&wNodeSetRenderFromIdentity) = load_function(ws3dHandle, "wNodeSetRenderFromIdentity");
	*(void**) (&wNodeSetRotation) = load_function(ws3dHandle, "wNodeSetRotation");
	*(void**) (&wNodeSetRotationPositionChange) = load_function(ws3dHandle, "wNodeSetRotationPositionChange");
	*(void**) (&wNodeSetScale) = load_function(ws3dHandle, "wNodeSetScale");
	*(void**) (&wNodeSetTransitionTime) = load_function(ws3dHandle, "wNodeSetTransitionTime");
	*(void**) (&wNodeSetUserData) = load_function(ws3dHandle, "wNodeSetUserData");
	*(void**) (&wNodeSetVisibility) = load_function(ws3dHandle, "wNodeSetVisibility");
	*(void**) (&wNodeTurn) = load_function(ws3dHandle, "wNodeTurn");
	*(void**) (&wNodeUpdateAbsolutePosition) = load_function(ws3dHandle, "wNodeUpdateAbsolutePosition");
	*(void**) (&wNodeUpdateShadow) = load_function(ws3dHandle, "wNodeUpdateShadow");
	*(void**) (&wNodesAreIntersecting) = load_function(ws3dHandle, "wNodesAreIntersecting");
	*(void**) (&wNodesGetBetweenDistance) = load_function(ws3dHandle, "wNodesGetBetweenDistance");
	*(void**) (&wOcclusionQueryAddMesh) = load_function(ws3dHandle, "wOcclusionQueryAddMesh");
	*(void**) (&wOcclusionQueryAddNode) = load_function(ws3dHandle, "wOcclusionQueryAddNode");
	*(void**) (&wOcclusionQueryGetResult) = load_function(ws3dHandle, "wOcclusionQueryGetResult");
	*(void**) (&wOcclusionQueryRemoveAll) = load_function(ws3dHandle, "wOcclusionQueryRemoveAll");
	*(void**) (&wOcclusionQueryRemoveNode) = load_function(ws3dHandle, "wOcclusionQueryRemoveNode");
	*(void**) (&wOcclusionQueryRun) = load_function(ws3dHandle, "wOcclusionQueryRun");
	*(void**) (&wOcclusionQueryRunAll) = load_function(ws3dHandle, "wOcclusionQueryRunAll");
	*(void**) (&wOcclusionQueryUpdate) = load_function(ws3dHandle, "wOcclusionQueryUpdate");
	*(void**) (&wOcclusionQueryUpdateAll) = load_function(ws3dHandle, "wOcclusionQueryUpdateAll");
	*(void**) (&wParticleAffectorIsEnable) = load_function(ws3dHandle, "wParticleAffectorIsEnable");
	*(void**) (&wParticleAffectorSetEnable) = load_function(ws3dHandle, "wParticleAffectorSetEnable");
	*(void**) (&wParticleAttractionAffectorCreate) = load_function(ws3dHandle, "wParticleAttractionAffectorCreate");
	*(void**) (&wParticleAttractionAffectorGetParameters) = load_function(ws3dHandle, "wParticleAttractionAffectorGetParameters");
	*(void**) (&wParticleAttractionAffectorSetParameters) = load_function(ws3dHandle, "wParticleAttractionAffectorSetParameters");
	*(void**) (&wParticleBoxEmitterCreate) = load_function(ws3dHandle, "wParticleBoxEmitterCreate");
	*(void**) (&wParticleBoxEmitterGetBox) = load_function(ws3dHandle, "wParticleBoxEmitterGetBox");
	*(void**) (&wParticleBoxEmitterSetBox) = load_function(ws3dHandle, "wParticleBoxEmitterSetBox");
	*(void**) (&wParticleColorAffectorGetParameters) = load_function(ws3dHandle, "wParticleColorAffectorGetParameters");
	*(void**) (&wParticleColorAffectorSetParameters) = load_function(ws3dHandle, "wParticleColorAffectorSetParameters");
	*(void**) (&wParticleColorMorphAffectorCreate) = load_function(ws3dHandle, "wParticleColorMorphAffectorCreate");
	*(void**) (&wParticleCylinderEmitterCreate) = load_function(ws3dHandle, "wParticleCylinderEmitterCreate");
	*(void**) (&wParticleCylinderEmitterGetParameters) = load_function(ws3dHandle, "wParticleCylinderEmitterGetParameters");
	*(void**) (&wParticleCylinderEmitterSetParameters) = load_function(ws3dHandle, "wParticleCylinderEmitterSetParameters");
	*(void**) (&wParticleEmitterGetParameters) = load_function(ws3dHandle, "wParticleEmitterGetParameters");
	*(void**) (&wParticleEmitterSetParameters) = load_function(ws3dHandle, "wParticleEmitterSetParameters");
	*(void**) (&wParticleFadeOutAffectorCreate) = load_function(ws3dHandle, "wParticleFadeOutAffectorCreate");
	*(void**) (&wParticleFadeOutAffectorGetColor) = load_function(ws3dHandle, "wParticleFadeOutAffectorGetColor");
	*(void**) (&wParticleFadeOutAffectorGetTime) = load_function(ws3dHandle, "wParticleFadeOutAffectorGetTime");
	*(void**) (&wParticleFadeOutAffectorSetColor) = load_function(ws3dHandle, "wParticleFadeOutAffectorSetColor");
	*(void**) (&wParticleFadeOutAffectorSetTime) = load_function(ws3dHandle, "wParticleFadeOutAffectorSetTime");
	*(void**) (&wParticleGravityAffectorCreate) = load_function(ws3dHandle, "wParticleGravityAffectorCreate");
	*(void**) (&wParticleGravityAffectorGetGravity) = load_function(ws3dHandle, "wParticleGravityAffectorGetGravity");
	*(void**) (&wParticleGravityAffectorGetTimeLost) = load_function(ws3dHandle, "wParticleGravityAffectorGetTimeLost");
	*(void**) (&wParticleGravityAffectorSetGravity) = load_function(ws3dHandle, "wParticleGravityAffectorSetGravity");
	*(void**) (&wParticleGravityAffectorSetTimeLost) = load_function(ws3dHandle, "wParticleGravityAffectorSetTimeLost");
	*(void**) (&wParticleMeshEmitterCreate) = load_function(ws3dHandle, "wParticleMeshEmitterCreate");
	*(void**) (&wParticleMeshEmitterGetParameters) = load_function(ws3dHandle, "wParticleMeshEmitterGetParameters");
	*(void**) (&wParticleMeshEmitterSetParameters) = load_function(ws3dHandle, "wParticleMeshEmitterSetParameters");
	*(void**) (&wParticlePointEmitterCreate) = load_function(ws3dHandle, "wParticlePointEmitterCreate");
	*(void**) (&wParticlePushAffectorCreate) = load_function(ws3dHandle, "wParticlePushAffectorCreate");
	*(void**) (&wParticlePushAffectorGetParameters) = load_function(ws3dHandle, "wParticlePushAffectorGetParameters");
	*(void**) (&wParticlePushAffectorSetParameters) = load_function(ws3dHandle, "wParticlePushAffectorSetParameters");
	*(void**) (&wParticleRingEmitterCreate) = load_function(ws3dHandle, "wParticleRingEmitterCreate");
	*(void**) (&wParticleRingEmitterGetParameters) = load_function(ws3dHandle, "wParticleRingEmitterGetParameters");
	*(void**) (&wParticleRingEmitterSetParameters) = load_function(ws3dHandle, "wParticleRingEmitterSetParameters");
	*(void**) (&wParticleRotationAffectorCreate) = load_function(ws3dHandle, "wParticleRotationAffectorCreate");
	*(void**) (&wParticleRotationAffectorGetPivot) = load_function(ws3dHandle, "wParticleRotationAffectorGetPivot");
	*(void**) (&wParticleRotationAffectorGetSpeed) = load_function(ws3dHandle, "wParticleRotationAffectorGetSpeed");
	*(void**) (&wParticleRotationAffectorSetPivot) = load_function(ws3dHandle, "wParticleRotationAffectorSetPivot");
	*(void**) (&wParticleRotationAffectorSetSpeed) = load_function(ws3dHandle, "wParticleRotationAffectorSetSpeed");
	*(void**) (&wParticleScaleAffectorCreate) = load_function(ws3dHandle, "wParticleScaleAffectorCreate");
	*(void**) (&wParticleSphereEmitterCreate) = load_function(ws3dHandle, "wParticleSphereEmitterCreate");
	*(void**) (&wParticleSphereEmitterGetParameters) = load_function(ws3dHandle, "wParticleSphereEmitterGetParameters");
	*(void**) (&wParticleSphereEmitterSetParameters) = load_function(ws3dHandle, "wParticleSphereEmitterSetParameters");
	*(void**) (&wParticleSplineAffectorCreate) = load_function(ws3dHandle, "wParticleSplineAffectorCreate");
	*(void**) (&wParticleSplineAffectorGetParameters) = load_function(ws3dHandle, "wParticleSplineAffectorGetParameters");
	*(void**) (&wParticleSplineAffectorSetParameters) = load_function(ws3dHandle, "wParticleSplineAffectorSetParameters");
	*(void**) (&wParticleStopAffectorCreate) = load_function(ws3dHandle, "wParticleStopAffectorCreate");
	*(void**) (&wParticleStopAffectorGetTime) = load_function(ws3dHandle, "wParticleStopAffectorGetTime");
	*(void**) (&wParticleStopAffectorSetTime) = load_function(ws3dHandle, "wParticleStopAffectorSetTime");
	*(void**) (&wParticleSystemClear) = load_function(ws3dHandle, "wParticleSystemClear");
	*(void**) (&wParticleSystemCreate) = load_function(ws3dHandle, "wParticleSystemCreate");
	*(void**) (&wParticleSystemGetEmitter) = load_function(ws3dHandle, "wParticleSystemGetEmitter");
	*(void**) (&wParticleSystemRemoveAllAffectors) = load_function(ws3dHandle, "wParticleSystemRemoveAllAffectors");
	*(void**) (&wParticleSystemSetEmitter) = load_function(ws3dHandle, "wParticleSystemSetEmitter");
	*(void**) (&wParticleSystemSetGlobal) = load_function(ws3dHandle, "wParticleSystemSetGlobal");
	*(void**) (&wParticleSystemSetParticleSize) = load_function(ws3dHandle, "wParticleSystemSetParticleSize");
	*(void**) (&wPhysBodiesGetCollisionNormal) = load_function(ws3dHandle, "wPhysBodiesGetCollisionNormal");
	*(void**) (&wPhysBodiesGetCollisionPoint) = load_function(ws3dHandle, "wPhysBodiesGetCollisionPoint");
	*(void**) (&wPhysBodiesIsCollide) = load_function(ws3dHandle, "wPhysBodiesIsCollide");
	*(void**) (&wPhysBodyAddForce) = load_function(ws3dHandle, "wPhysBodyAddForce");
	*(void**) (&wPhysBodyAddImpulse) = load_function(ws3dHandle, "wPhysBodyAddImpulse");
	*(void**) (&wPhysBodyAddTorque) = load_function(ws3dHandle, "wPhysBodyAddTorque");
	*(void**) (&wPhysBodyCreateCapsule) = load_function(ws3dHandle, "wPhysBodyCreateCapsule");
	*(void**) (&wPhysBodyCreateCompound) = load_function(ws3dHandle, "wPhysBodyCreateCompound");
	*(void**) (&wPhysBodyCreateCone) = load_function(ws3dHandle, "wPhysBodyCreateCone");
	*(void**) (&wPhysBodyCreateCube) = load_function(ws3dHandle, "wPhysBodyCreateCube");
	*(void**) (&wPhysBodyCreateCylinder) = load_function(ws3dHandle, "wPhysBodyCreateCylinder");
	*(void**) (&wPhysBodyCreateHeightField) = load_function(ws3dHandle, "wPhysBodyCreateHeightField");
	*(void**) (&wPhysBodyCreateHull) = load_function(ws3dHandle, "wPhysBodyCreateHull");
	*(void**) (&wPhysBodyCreateNull) = load_function(ws3dHandle, "wPhysBodyCreateNull");
	*(void**) (&wPhysBodyCreateSphere) = load_function(ws3dHandle, "wPhysBodyCreateSphere");
	*(void**) (&wPhysBodyCreateTerrain) = load_function(ws3dHandle, "wPhysBodyCreateTerrain");
	*(void**) (&wPhysBodyCreateTree) = load_function(ws3dHandle, "wPhysBodyCreateTree");
	*(void**) (&wPhysBodyCreateTreeBsp) = load_function(ws3dHandle, "wPhysBodyCreateTreeBsp");
	*(void**) (&wPhysBodyCreateWaterSurface) = load_function(ws3dHandle, "wPhysBodyCreateWaterSurface");
	*(void**) (&wPhysBodyDraw) = load_function(ws3dHandle, "wPhysBodyDraw");
	*(void**) (&wPhysBodyGetAngularDamping) = load_function(ws3dHandle, "wPhysBodyGetAngularDamping");
	*(void**) (&wPhysBodyGetAngularVelocity) = load_function(ws3dHandle, "wPhysBodyGetAngularVelocity");
	*(void**) (&wPhysBodyGetCenterOfMass) = load_function(ws3dHandle, "wPhysBodyGetCenterOfMass");
	*(void**) (&wPhysBodyGetGravity) = load_function(ws3dHandle, "wPhysBodyGetGravity");
	*(void**) (&wPhysBodyGetLinearDamping) = load_function(ws3dHandle, "wPhysBodyGetLinearDamping");
	*(void**) (&wPhysBodyGetLinearVelocity) = load_function(ws3dHandle, "wPhysBodyGetLinearVelocity");
	*(void**) (&wPhysBodyGetMass) = load_function(ws3dHandle, "wPhysBodyGetMass");
	*(void**) (&wPhysBodyGetMaterial) = load_function(ws3dHandle, "wPhysBodyGetMaterial");
	*(void**) (&wPhysBodyGetMomentOfInertia) = load_function(ws3dHandle, "wPhysBodyGetMomentOfInertia");
	*(void**) (&wPhysBodyGetName) = load_function(ws3dHandle, "wPhysBodyGetName");
	*(void**) (&wPhysBodyIsAutoSleep) = load_function(ws3dHandle, "wPhysBodyIsAutoSleep");
	*(void**) (&wPhysBodyIsFreeze) = load_function(ws3dHandle, "wPhysBodyIsFreeze");
	*(void**) (&wPhysBodySetAngularDamping) = load_function(ws3dHandle, "wPhysBodySetAngularDamping");
	*(void**) (&wPhysBodySetAngularVelocity) = load_function(ws3dHandle, "wPhysBodySetAngularVelocity");
	*(void**) (&wPhysBodySetAutoSleep) = load_function(ws3dHandle, "wPhysBodySetAutoSleep");
	*(void**) (&wPhysBodySetCenterOfMass) = load_function(ws3dHandle, "wPhysBodySetCenterOfMass");
	*(void**) (&wPhysBodySetFreeze) = load_function(ws3dHandle, "wPhysBodySetFreeze");
	*(void**) (&wPhysBodySetGravity) = load_function(ws3dHandle, "wPhysBodySetGravity");
	*(void**) (&wPhysBodySetLinearDamping) = load_function(ws3dHandle, "wPhysBodySetLinearDamping");
	*(void**) (&wPhysBodySetLinearVelocity) = load_function(ws3dHandle, "wPhysBodySetLinearVelocity");
	*(void**) (&wPhysBodySetMass) = load_function(ws3dHandle, "wPhysBodySetMass");
	*(void**) (&wPhysBodySetMaterial) = load_function(ws3dHandle, "wPhysBodySetMaterial");
	*(void**) (&wPhysBodySetMomentOfInertia) = load_function(ws3dHandle, "wPhysBodySetMomentOfInertia");
	*(void**) (&wPhysBodySetName) = load_function(ws3dHandle, "wPhysBodySetName");
	*(void**) (&wPhysDestroyAllBodies) = load_function(ws3dHandle, "wPhysDestroyAllBodies");
	*(void**) (&wPhysDestroyAllJoints) = load_function(ws3dHandle, "wPhysDestroyAllJoints");
	*(void**) (&wPhysGetBodiesCount) = load_function(ws3dHandle, "wPhysGetBodiesCount");
	*(void**) (&wPhysGetBodyById) = load_function(ws3dHandle, "wPhysGetBodyById");
	*(void**) (&wPhysGetBodyByIndex) = load_function(ws3dHandle, "wPhysGetBodyByIndex");
	*(void**) (&wPhysGetBodyByName) = load_function(ws3dHandle, "wPhysGetBodyByName");
	*(void**) (&wPhysGetBodyFromRay) = load_function(ws3dHandle, "wPhysGetBodyFromRay");
	*(void**) (&wPhysGetBodyFromScreenCoords) = load_function(ws3dHandle, "wPhysGetBodyFromScreenCoords");
	*(void**) (&wPhysGetBodyPicked) = load_function(ws3dHandle, "wPhysGetBodyPicked");
	*(void**) (&wPhysGetJointById) = load_function(ws3dHandle, "wPhysGetJointById");
	*(void**) (&wPhysGetJointByIndex) = load_function(ws3dHandle, "wPhysGetJointByIndex");
	*(void**) (&wPhysGetJointByName) = load_function(ws3dHandle, "wPhysGetJointByName");
	*(void**) (&wPhysGetJointsCount) = load_function(ws3dHandle, "wPhysGetJointsCount");
	*(void**) (&wPhysJointCreateBall) = load_function(ws3dHandle, "wPhysJointCreateBall");
	*(void**) (&wPhysJointCreateCorkScrew) = load_function(ws3dHandle, "wPhysJointCreateCorkScrew");
	*(void**) (&wPhysJointCreateHinge) = load_function(ws3dHandle, "wPhysJointCreateHinge");
	*(void**) (&wPhysJointCreateSlider) = load_function(ws3dHandle, "wPhysJointCreateSlider");
	*(void**) (&wPhysJointCreateUpVector) = load_function(ws3dHandle, "wPhysJointCreateUpVector");
	*(void**) (&wPhysJointGetName) = load_function(ws3dHandle, "wPhysJointGetName");
	*(void**) (&wPhysJointIsCollision) = load_function(ws3dHandle, "wPhysJointIsCollision");
	*(void**) (&wPhysJointSetBallLimits) = load_function(ws3dHandle, "wPhysJointSetBallLimits");
	*(void**) (&wPhysJointSetCollisionState) = load_function(ws3dHandle, "wPhysJointSetCollisionState");
	*(void**) (&wPhysJointSetCorkScrewAngularLimits) = load_function(ws3dHandle, "wPhysJointSetCorkScrewAngularLimits");
	*(void**) (&wPhysJointSetCorkScrewLinearLimits) = load_function(ws3dHandle, "wPhysJointSetCorkScrewLinearLimits");
	*(void**) (&wPhysJointSetHingeLimits) = load_function(ws3dHandle, "wPhysJointSetHingeLimits");
	*(void**) (&wPhysJointSetName) = load_function(ws3dHandle, "wPhysJointSetName");
	*(void**) (&wPhysJointSetSliderLimits) = load_function(ws3dHandle, "wPhysJointSetSliderLimits");
	*(void**) (&wPhysMaterialCreate) = load_function(ws3dHandle, "wPhysMaterialCreate");
	*(void**) (&wPhysMaterialSetCollidable) = load_function(ws3dHandle, "wPhysMaterialSetCollidable");
	*(void**) (&wPhysMaterialSetContactSound) = load_function(ws3dHandle, "wPhysMaterialSetContactSound");
	*(void**) (&wPhysMaterialSetElasticity) = load_function(ws3dHandle, "wPhysMaterialSetElasticity");
	*(void**) (&wPhysMaterialSetFriction) = load_function(ws3dHandle, "wPhysMaterialSetFriction");
	*(void**) (&wPhysMaterialSetSoftness) = load_function(ws3dHandle, "wPhysMaterialSetSoftness");
	*(void**) (&wPhysPlayerControllerCreate) = load_function(ws3dHandle, "wPhysPlayerControllerCreate");
	*(void**) (&wPhysPlayerControllerSetVelocity) = load_function(ws3dHandle, "wPhysPlayerControllerSetVelocity");
	*(void**) (&wPhysSetFrictionModel) = load_function(ws3dHandle, "wPhysSetFrictionModel");
	*(void**) (&wPhysSetGravity) = load_function(ws3dHandle, "wPhysSetGravity");
	*(void**) (&wPhysSetSolverModel) = load_function(ws3dHandle, "wPhysSetSolverModel");
	*(void**) (&wPhysSetWorldSize) = load_function(ws3dHandle, "wPhysSetWorldSize");
	*(void**) (&wPhysStart) = load_function(ws3dHandle, "wPhysStart");
	*(void**) (&wPhysStop) = load_function(ws3dHandle, "wPhysStop");
	*(void**) (&wPhysUpdate) = load_function(ws3dHandle, "wPhysUpdate");
	*(void**) (&wPhysVehicleAddTire) = load_function(ws3dHandle, "wPhysVehicleAddTire");
	*(void**) (&wPhysVehicleCreate) = load_function(ws3dHandle, "wPhysVehicleCreate");
	*(void**) (&wPhysVehicleGetBody) = load_function(ws3dHandle, "wPhysVehicleGetBody");
	*(void**) (&wPhysVehicleGetMotorValue) = load_function(ws3dHandle, "wPhysVehicleGetMotorValue");
	*(void**) (&wPhysVehicleGetSpeed) = load_function(ws3dHandle, "wPhysVehicleGetSpeed");
	*(void**) (&wPhysVehicleGetSteering) = load_function(ws3dHandle, "wPhysVehicleGetSteering");
	*(void**) (&wPhysVehicleGetTireAngularVelocity) = load_function(ws3dHandle, "wPhysVehicleGetTireAngularVelocity");
	*(void**) (&wPhysVehicleGetTireBrakeForce) = load_function(ws3dHandle, "wPhysVehicleGetTireBrakeForce");
	*(void**) (&wPhysVehicleGetTireBrakeLateralFriction) = load_function(ws3dHandle, "wPhysVehicleGetTireBrakeLateralFriction");
	*(void**) (&wPhysVehicleGetTireBrakeLongitudinalFriction) = load_function(ws3dHandle, "wPhysVehicleGetTireBrakeLongitudinalFriction");
	*(void**) (&wPhysVehicleGetTireContactNormal) = load_function(ws3dHandle, "wPhysVehicleGetTireContactNormal");
	*(void**) (&wPhysVehicleGetTireContactPoint) = load_function(ws3dHandle, "wPhysVehicleGetTireContactPoint");
	*(void**) (&wPhysVehicleGetTireLateralFriction) = load_function(ws3dHandle, "wPhysVehicleGetTireLateralFriction");
	*(void**) (&wPhysVehicleGetTireLoad) = load_function(ws3dHandle, "wPhysVehicleGetTireLoad");
	*(void**) (&wPhysVehicleGetTireLocalPosition) = load_function(ws3dHandle, "wPhysVehicleGetTireLocalPosition");
	*(void**) (&wPhysVehicleGetTireLongitudinalFriction) = load_function(ws3dHandle, "wPhysVehicleGetTireLongitudinalFriction");
	*(void**) (&wPhysVehicleGetTireMass) = load_function(ws3dHandle, "wPhysVehicleGetTireMass");
	*(void**) (&wPhysVehicleGetTireMaxSteerAngle) = load_function(ws3dHandle, "wPhysVehicleGetTireMaxSteerAngle");
	*(void**) (&wPhysVehicleGetTireMotorForce) = load_function(ws3dHandle, "wPhysVehicleGetTireMotorForce");
	*(void**) (&wPhysVehicleGetTireRadius) = load_function(ws3dHandle, "wPhysVehicleGetTireRadius");
	*(void**) (&wPhysVehicleGetTireSpeed) = load_function(ws3dHandle, "wPhysVehicleGetTireSpeed");
	*(void**) (&wPhysVehicleGetTireSpinForce) = load_function(ws3dHandle, "wPhysVehicleGetTireSpinForce");
	*(void**) (&wPhysVehicleGetTireSpinTorqueFactor) = load_function(ws3dHandle, "wPhysVehicleGetTireSpinTorqueFactor");
	*(void**) (&wPhysVehicleGetTireSpringConst) = load_function(ws3dHandle, "wPhysVehicleGetTireSpringConst");
	*(void**) (&wPhysVehicleGetTireSpringDamper) = load_function(ws3dHandle, "wPhysVehicleGetTireSpringDamper");
	*(void**) (&wPhysVehicleGetTireSuspensionLenght) = load_function(ws3dHandle, "wPhysVehicleGetTireSuspensionLenght");
	*(void**) (&wPhysVehicleGetTireTorquePosition) = load_function(ws3dHandle, "wPhysVehicleGetTireTorquePosition");
	*(void**) (&wPhysVehicleGetTireTurnForceHelper) = load_function(ws3dHandle, "wPhysVehicleGetTireTurnForceHelper");
	*(void**) (&wPhysVehicleGetTireType) = load_function(ws3dHandle, "wPhysVehicleGetTireType");
	*(void**) (&wPhysVehicleGetTireUpDownPosition) = load_function(ws3dHandle, "wPhysVehicleGetTireUpDownPosition");
	*(void**) (&wPhysVehicleGetTireUserData) = load_function(ws3dHandle, "wPhysVehicleGetTireUserData");
	*(void**) (&wPhysVehicleGetTireWidth) = load_function(ws3dHandle, "wPhysVehicleGetTireWidth");
	*(void**) (&wPhysVehicleGetTiresCount) = load_function(ws3dHandle, "wPhysVehicleGetTiresCount");
	*(void**) (&wPhysVehicleIsAllTiresCollided) = load_function(ws3dHandle, "wPhysVehicleIsAllTiresCollided");
	*(void**) (&wPhysVehicleIsBrake) = load_function(ws3dHandle, "wPhysVehicleIsBrake");
	*(void**) (&wPhysVehicleIsTireBrake) = load_function(ws3dHandle, "wPhysVehicleIsTireBrake");
	*(void**) (&wPhysVehicleSetBrake) = load_function(ws3dHandle, "wPhysVehicleSetBrake");
	*(void**) (&wPhysVehicleSetMotorValue) = load_function(ws3dHandle, "wPhysVehicleSetMotorValue");
	*(void**) (&wPhysVehicleSetSteering) = load_function(ws3dHandle, "wPhysVehicleSetSteering");
	*(void**) (&wPhysVehicleSetTireBrakeForce) = load_function(ws3dHandle, "wPhysVehicleSetTireBrakeForce");
	*(void**) (&wPhysVehicleSetTireBrakeLateralFriction) = load_function(ws3dHandle, "wPhysVehicleSetTireBrakeLateralFriction");
	*(void**) (&wPhysVehicleSetTireBrakeLongitudinalFriction) = load_function(ws3dHandle, "wPhysVehicleSetTireBrakeLongitudinalFriction");
	*(void**) (&wPhysVehicleSetTireLateralFriction) = load_function(ws3dHandle, "wPhysVehicleSetTireLateralFriction");
	*(void**) (&wPhysVehicleSetTireLongitudinalFriction) = load_function(ws3dHandle, "wPhysVehicleSetTireLongitudinalFriction");
	*(void**) (&wPhysVehicleSetTireMass) = load_function(ws3dHandle, "wPhysVehicleSetTireMass");
	*(void**) (&wPhysVehicleSetTireMaxSteerAngle) = load_function(ws3dHandle, "wPhysVehicleSetTireMaxSteerAngle");
	*(void**) (&wPhysVehicleSetTireMotorForce) = load_function(ws3dHandle, "wPhysVehicleSetTireMotorForce");
	*(void**) (&wPhysVehicleSetTireRadius) = load_function(ws3dHandle, "wPhysVehicleSetTireRadius");
	*(void**) (&wPhysVehicleSetTireSpinForce) = load_function(ws3dHandle, "wPhysVehicleSetTireSpinForce");
	*(void**) (&wPhysVehicleSetTireSpinTorqueFactor) = load_function(ws3dHandle, "wPhysVehicleSetTireSpinTorqueFactor");
	*(void**) (&wPhysVehicleSetTireSpringConst) = load_function(ws3dHandle, "wPhysVehicleSetTireSpringConst");
	*(void**) (&wPhysVehicleSetTireSpringDamper) = load_function(ws3dHandle, "wPhysVehicleSetTireSpringDamper");
	*(void**) (&wPhysVehicleSetTireSuspensionLenght) = load_function(ws3dHandle, "wPhysVehicleSetTireSuspensionLenght");
	*(void**) (&wPhysVehicleSetTireTorquePosition) = load_function(ws3dHandle, "wPhysVehicleSetTireTorquePosition");
	*(void**) (&wPhysVehicleSetTireTurnForceHelper) = load_function(ws3dHandle, "wPhysVehicleSetTireTurnForceHelper");
	*(void**) (&wPhysVehicleSetTireType) = load_function(ws3dHandle, "wPhysVehicleSetTireType");
	*(void**) (&wPhysVehicleSetTireUserData) = load_function(ws3dHandle, "wPhysVehicleSetTireUserData");
	*(void**) (&wPhysVehicleSetTireWidth) = load_function(ws3dHandle, "wPhysVehicleSetTireWidth");
	*(void**) (&wPostEffectCreate) = load_function(ws3dHandle, "wPostEffectCreate");
	*(void**) (&wPostEffectDestroy) = load_function(ws3dHandle, "wPostEffectDestroy");
	*(void**) (&wPostEffectSetParameters) = load_function(ws3dHandle, "wPostEffectSetParameters");
	*(void**) (&wPostEffectsDestroyAll) = load_function(ws3dHandle, "wPostEffectsDestroyAll");
	*(void**) (&wRealCloudsCreate) = load_function(ws3dHandle, "wRealCloudsCreate");
	*(void**) (&wRealCloudsGetCloudHeight) = load_function(ws3dHandle, "wRealCloudsGetCloudHeight");
	*(void**) (&wRealCloudsGetCloudRadius) = load_function(ws3dHandle, "wRealCloudsGetCloudRadius");
	*(void**) (&wRealCloudsGetColors) = load_function(ws3dHandle, "wRealCloudsGetColors");
	*(void**) (&wRealCloudsGetTextureScale) = load_function(ws3dHandle, "wRealCloudsGetTextureScale");
	*(void**) (&wRealCloudsGetTextureTranslation) = load_function(ws3dHandle, "wRealCloudsGetTextureTranslation");
	*(void**) (&wRealCloudsSetCloudHeight) = load_function(ws3dHandle, "wRealCloudsSetCloudHeight");
	*(void**) (&wRealCloudsSetCloudRadius) = load_function(ws3dHandle, "wRealCloudsSetCloudRadius");
	*(void**) (&wRealCloudsSetColors) = load_function(ws3dHandle, "wRealCloudsSetColors");
	*(void**) (&wRealCloudsSetTextureScale) = load_function(ws3dHandle, "wRealCloudsSetTextureScale");
	*(void**) (&wRealCloudsSetTextureTranslation) = load_function(ws3dHandle, "wRealCloudsSetTextureTranslation");
	*(void**) (&wRealWaterSetColor) = load_function(ws3dHandle, "wRealWaterSetColor");
	*(void**) (&wRealWaterSetColorBlendFactor) = load_function(ws3dHandle, "wRealWaterSetColorBlendFactor");
	*(void**) (&wRealWaterSetWaveHeight) = load_function(ws3dHandle, "wRealWaterSetWaveHeight");
	*(void**) (&wRealWaterSetWindDirection) = load_function(ws3dHandle, "wRealWaterSetWindDirection");
	*(void**) (&wRealWaterSetWindForce) = load_function(ws3dHandle, "wRealWaterSetWindForce");
	*(void**) (&wRealWaterSurfaceCreate) = load_function(ws3dHandle, "wRealWaterSurfaceCreate");
	*(void**) (&wRtsCameraCreate) = load_function(ws3dHandle, "wRtsCameraCreate");
	*(void**) (&wSceneBegin) = load_function(ws3dHandle, "wSceneBegin");
	*(void**) (&wSceneBeginAdvanced) = load_function(ws3dHandle, "wSceneBeginAdvanced");
	*(void**) (&wSceneDestroyAllMeshes) = load_function(ws3dHandle, "wSceneDestroyAllMeshes");
	*(void**) (&wSceneDestroyAllNodes) = load_function(ws3dHandle, "wSceneDestroyAllNodes");
	*(void**) (&wSceneDestroyAllTextures) = load_function(ws3dHandle, "wSceneDestroyAllTextures");
	*(void**) (&wSceneDestroyAllUnusedMeshes) = load_function(ws3dHandle, "wSceneDestroyAllUnusedMeshes");
	*(void**) (&wSceneDrawAll) = load_function(ws3dHandle, "wSceneDrawAll");
	*(void**) (&wSceneDrawToTexture) = load_function(ws3dHandle, "wSceneDrawToTexture");
	*(void**) (&wSceneEnd) = load_function(ws3dHandle, "wSceneEnd");
	*(void**) (&wSceneGetActiveCamera) = load_function(ws3dHandle, "wSceneGetActiveCamera");
	*(void**) (&wSceneGetAmbientLight) = load_function(ws3dHandle, "wSceneGetAmbientLight");
	*(void**) (&wSceneGetFog) = load_function(ws3dHandle, "wSceneGetFog");
	*(void**) (&wSceneGetMeshByIndex) = load_function(ws3dHandle, "wSceneGetMeshByIndex");
	*(void**) (&wSceneGetMeshByName) = load_function(ws3dHandle, "wSceneGetMeshByName");
	*(void**) (&wSceneGetMeshesCount) = load_function(ws3dHandle, "wSceneGetMeshesCount");
	*(void**) (&wSceneGetNodeById) = load_function(ws3dHandle, "wSceneGetNodeById");
	*(void**) (&wSceneGetNodeByName) = load_function(ws3dHandle, "wSceneGetNodeByName");
	*(void**) (&wSceneGetNodesCount) = load_function(ws3dHandle, "wSceneGetNodesCount");
	*(void**) (&wSceneGetPrimitivesDrawn) = load_function(ws3dHandle, "wSceneGetPrimitivesDrawn");
	*(void**) (&wSceneGetRootNode) = load_function(ws3dHandle, "wSceneGetRootNode");
	*(void**) (&wSceneGetShadowColor) = load_function(ws3dHandle, "wSceneGetShadowColor");
	*(void**) (&wSceneGetTextureByName) = load_function(ws3dHandle, "wSceneGetTextureByName");
	*(void**) (&wSceneIsMeshLoaded) = load_function(ws3dHandle, "wSceneIsMeshLoaded");
	*(void**) (&wSceneLoad) = load_function(ws3dHandle, "wSceneLoad");
	*(void**) (&wSceneSave) = load_function(ws3dHandle, "wSceneSave");
	*(void**) (&wSceneSetAmbientLight) = load_function(ws3dHandle, "wSceneSetAmbientLight");
	*(void**) (&wSceneSetFog) = load_function(ws3dHandle, "wSceneSetFog");
	*(void**) (&wSceneSetRenderTarget) = load_function(ws3dHandle, "wSceneSetRenderTarget");
	*(void**) (&wSceneSetShadowColor) = load_function(ws3dHandle, "wSceneSetShadowColor");
	*(void**) (&wShaderAddHighLevelMaterial) = load_function(ws3dHandle, "wShaderAddHighLevelMaterial");
	*(void**) (&wShaderAddHighLevelMaterialEx) = load_function(ws3dHandle, "wShaderAddHighLevelMaterialEx");
	*(void**) (&wShaderAddHighLevelMaterialFromFiles) = load_function(ws3dHandle, "wShaderAddHighLevelMaterialFromFiles");
	*(void**) (&wShaderAddHighLevelMaterialFromFilesEx) = load_function(ws3dHandle, "wShaderAddHighLevelMaterialFromFilesEx");
	*(void**) (&wShaderAddMaterial) = load_function(ws3dHandle, "wShaderAddMaterial");
	*(void**) (&wShaderAddMaterialFromFiles) = load_function(ws3dHandle, "wShaderAddMaterialFromFiles");
	*(void**) (&wShaderCreateAddressedPixelConstant) = load_function(ws3dHandle, "wShaderCreateAddressedPixelConstant");
	*(void**) (&wShaderCreateAddressedVertexConstant) = load_function(ws3dHandle, "wShaderCreateAddressedVertexConstant");
	*(void**) (&wShaderCreateNamedPixelConstant) = load_function(ws3dHandle, "wShaderCreateNamedPixelConstant");
	*(void**) (&wShaderCreateNamedVertexConstant) = load_function(ws3dHandle, "wShaderCreateNamedVertexConstant");
	*(void**) (&wSkyBoxCreate) = load_function(ws3dHandle, "wSkyBoxCreate");
	*(void**) (&wSkyDomeCreate) = load_function(ws3dHandle, "wSkyDomeCreate");
	*(void**) (&wSkyDomeSetColor) = load_function(ws3dHandle, "wSkyDomeSetColor");
	*(void**) (&wSkyDomeSetColorBand) = load_function(ws3dHandle, "wSkyDomeSetColorBand");
	*(void**) (&wSkyDomeSetColorPoint) = load_function(ws3dHandle, "wSkyDomeSetColorPoint");
	*(void**) (&wSoundAddEffect) = load_function(ws3dHandle, "wSoundAddEffect");
	*(void**) (&wSoundAddFilter) = load_function(ws3dHandle, "wSoundAddFilter");
	*(void**) (&wSoundBufferDestroy) = load_function(ws3dHandle, "wSoundBufferDestroy");
	*(void**) (&wSoundBufferLoad) = load_function(ws3dHandle, "wSoundBufferLoad");
	*(void**) (&wSoundBufferLoadFromMemory) = load_function(ws3dHandle, "wSoundBufferLoadFromMemory");
	*(void**) (&wSoundCalculateGain) = load_function(ws3dHandle, "wSoundCalculateGain");
	*(void**) (&wSoundCreateEffect) = load_function(ws3dHandle, "wSoundCreateEffect");
	*(void**) (&wSoundCreateFilter) = load_function(ws3dHandle, "wSoundCreateFilter");
	*(void**) (&wSoundCreateFromBuffer) = load_function(ws3dHandle, "wSoundCreateFromBuffer");
	*(void**) (&wSoundGetCompressedAudioSize) = load_function(ws3dHandle, "wSoundGetCompressedAudioSize");
	*(void**) (&wSoundGetCurrentAudioPosition) = load_function(ws3dHandle, "wSoundGetCurrentAudioPosition");
	*(void**) (&wSoundGetCurrentAudioTime) = load_function(ws3dHandle, "wSoundGetCurrentAudioTime");
	*(void**) (&wSoundGetCurrentCompressedAudioPosition) = load_function(ws3dHandle, "wSoundGetCurrentCompressedAudioPosition");
	*(void**) (&wSoundGetDirection) = load_function(ws3dHandle, "wSoundGetDirection");
	*(void**) (&wSoundGetDopplerStrength) = load_function(ws3dHandle, "wSoundGetDopplerStrength");
	*(void**) (&wSoundGetDopplerVelocity) = load_function(ws3dHandle, "wSoundGetDopplerVelocity");
	*(void**) (&wSoundGetEffectType) = load_function(ws3dHandle, "wSoundGetEffectType");
	*(void**) (&wSoundGetFilterHighFrequencyVolume) = load_function(ws3dHandle, "wSoundGetFilterHighFrequencyVolume");
	*(void**) (&wSoundGetFilterLowFrequencyVolume) = load_function(ws3dHandle, "wSoundGetFilterLowFrequencyVolume");
	*(void**) (&wSoundGetFilterType) = load_function(ws3dHandle, "wSoundGetFilterType");
	*(void**) (&wSoundGetFilterVolume) = load_function(ws3dHandle, "wSoundGetFilterVolume");
	*(void**) (&wSoundGetInnerConeAngle) = load_function(ws3dHandle, "wSoundGetInnerConeAngle");
	*(void**) (&wSoundGetMaxDistance) = load_function(ws3dHandle, "wSoundGetMaxDistance");
	*(void**) (&wSoundGetMaxEffectsSupported) = load_function(ws3dHandle, "wSoundGetMaxEffectsSupported");
	*(void**) (&wSoundGetMaxVolume) = load_function(ws3dHandle, "wSoundGetMaxVolume");
	*(void**) (&wSoundGetMinDistance) = load_function(ws3dHandle, "wSoundGetMinDistance");
	*(void**) (&wSoundGetMinVolume) = load_function(ws3dHandle, "wSoundGetMinVolume");
	*(void**) (&wSoundGetNumEffectSlotsAvailable) = load_function(ws3dHandle, "wSoundGetNumEffectSlotsAvailable");
	*(void**) (&wSoundGetOuterConeAngle) = load_function(ws3dHandle, "wSoundGetOuterConeAngle");
	*(void**) (&wSoundGetOuterConeVolume) = load_function(ws3dHandle, "wSoundGetOuterConeVolume");
	*(void**) (&wSoundGetPitch) = load_function(ws3dHandle, "wSoundGetPitch");
	*(void**) (&wSoundGetRollOffFactor) = load_function(ws3dHandle, "wSoundGetRollOffFactor");
	*(void**) (&wSoundGetStrength) = load_function(ws3dHandle, "wSoundGetStrength");
	*(void**) (&wSoundGetTotalAudioSize) = load_function(ws3dHandle, "wSoundGetTotalAudioSize");
	*(void**) (&wSoundGetTotalAudioTime) = load_function(ws3dHandle, "wSoundGetTotalAudioTime");
	*(void**) (&wSoundGetVelocity) = load_function(ws3dHandle, "wSoundGetVelocity");
	*(void**) (&wSoundGetVolume) = load_function(ws3dHandle, "wSoundGetVolume");
	*(void**) (&wSoundIsEffectSupported) = load_function(ws3dHandle, "wSoundIsEffectSupported");
	*(void**) (&wSoundIsEffectValid) = load_function(ws3dHandle, "wSoundIsEffectValid");
	*(void**) (&wSoundIsFilterSupported) = load_function(ws3dHandle, "wSoundIsFilterSupported");
	*(void**) (&wSoundIsFilterValid) = load_function(ws3dHandle, "wSoundIsFilterValid");
	*(void**) (&wSoundIsLooping) = load_function(ws3dHandle, "wSoundIsLooping");
	*(void**) (&wSoundIsPaused) = load_function(ws3dHandle, "wSoundIsPaused");
	*(void**) (&wSoundIsPlaying) = load_function(ws3dHandle, "wSoundIsPlaying");
	*(void**) (&wSoundIsRelative) = load_function(ws3dHandle, "wSoundIsRelative");
	*(void**) (&wSoundIsStopped) = load_function(ws3dHandle, "wSoundIsStopped");
	*(void**) (&wSoundIsValid) = load_function(ws3dHandle, "wSoundIsValid");
	*(void**) (&wSoundLoad) = load_function(ws3dHandle, "wSoundLoad");
	*(void**) (&wSoundLoadFromMemory) = load_function(ws3dHandle, "wSoundLoadFromMemory");
	*(void**) (&wSoundLoadFromRaw) = load_function(ws3dHandle, "wSoundLoadFromRaw");
	*(void**) (&wSoundPause) = load_function(ws3dHandle, "wSoundPause");
	*(void**) (&wSoundPlay) = load_function(ws3dHandle, "wSoundPlay");
	*(void**) (&wSoundRemoveEffect) = load_function(ws3dHandle, "wSoundRemoveEffect");
	*(void**) (&wSoundRemoveFilter) = load_function(ws3dHandle, "wSoundRemoveFilter");
	*(void**) (&wSoundSeek) = load_function(ws3dHandle, "wSoundSeek");
	*(void**) (&wSoundSetDirection) = load_function(ws3dHandle, "wSoundSetDirection");
	*(void**) (&wSoundSetDopplerStrength) = load_function(ws3dHandle, "wSoundSetDopplerStrength");
	*(void**) (&wSoundSetDopplerVelocity) = load_function(ws3dHandle, "wSoundSetDopplerVelocity");
	*(void**) (&wSoundSetEffectAutowahParameters) = load_function(ws3dHandle, "wSoundSetEffectAutowahParameters");
	*(void**) (&wSoundSetEffectChorusParameters) = load_function(ws3dHandle, "wSoundSetEffectChorusParameters");
	*(void**) (&wSoundSetEffectCompressorParameters) = load_function(ws3dHandle, "wSoundSetEffectCompressorParameters");
	*(void**) (&wSoundSetEffectDistortionParameters) = load_function(ws3dHandle, "wSoundSetEffectDistortionParameters");
	*(void**) (&wSoundSetEffectEaxReverbParameters) = load_function(ws3dHandle, "wSoundSetEffectEaxReverbParameters");
	*(void**) (&wSoundSetEffectEchoParameters) = load_function(ws3dHandle, "wSoundSetEffectEchoParameters");
	*(void**) (&wSoundSetEffectEqualizerParameters) = load_function(ws3dHandle, "wSoundSetEffectEqualizerParameters");
	*(void**) (&wSoundSetEffectFlangerParameters) = load_function(ws3dHandle, "wSoundSetEffectFlangerParameters");
	*(void**) (&wSoundSetEffectFrequencyShiftParameters) = load_function(ws3dHandle, "wSoundSetEffectFrequencyShiftParameters");
	*(void**) (&wSoundSetEffectPitchShifterParameters) = load_function(ws3dHandle, "wSoundSetEffectPitchShifterParameters");
	*(void**) (&wSoundSetEffectReverbParameters) = load_function(ws3dHandle, "wSoundSetEffectReverbParameters");
	*(void**) (&wSoundSetEffectRingModulatorParameters) = load_function(ws3dHandle, "wSoundSetEffectRingModulatorParameters");
	*(void**) (&wSoundSetEffectType) = load_function(ws3dHandle, "wSoundSetEffectType");
	*(void**) (&wSoundSetEffectVocalMorpherParameters) = load_function(ws3dHandle, "wSoundSetEffectVocalMorpherParameters");
	*(void**) (&wSoundSetFilterHighFrequencyVolume) = load_function(ws3dHandle, "wSoundSetFilterHighFrequencyVolume");
	*(void**) (&wSoundSetFilterLowFrequencyVolume) = load_function(ws3dHandle, "wSoundSetFilterLowFrequencyVolume");
	*(void**) (&wSoundSetFilterType) = load_function(ws3dHandle, "wSoundSetFilterType");
	*(void**) (&wSoundSetFilterVolume) = load_function(ws3dHandle, "wSoundSetFilterVolume");
	*(void**) (&wSoundSetInnerConeAngle) = load_function(ws3dHandle, "wSoundSetInnerConeAngle");
	*(void**) (&wSoundSetLoopMode) = load_function(ws3dHandle, "wSoundSetLoopMode");
	*(void**) (&wSoundSetMaxDistance) = load_function(ws3dHandle, "wSoundSetMaxDistance");
	*(void**) (&wSoundSetMaxVolume) = load_function(ws3dHandle, "wSoundSetMaxVolume");
	*(void**) (&wSoundSetMinDistance) = load_function(ws3dHandle, "wSoundSetMinDistance");
	*(void**) (&wSoundSetMinVolume) = load_function(ws3dHandle, "wSoundSetMinVolume");
	*(void**) (&wSoundSetOuterConeAngle) = load_function(ws3dHandle, "wSoundSetOuterConeAngle");
	*(void**) (&wSoundSetOuterConeVolume) = load_function(ws3dHandle, "wSoundSetOuterConeVolume");
	*(void**) (&wSoundSetPitch) = load_function(ws3dHandle, "wSoundSetPitch");
	*(void**) (&wSoundSetRelative) = load_function(ws3dHandle, "wSoundSetRelative");
	*(void**) (&wSoundSetRollOffFactor) = load_function(ws3dHandle, "wSoundSetRollOffFactor");
	*(void**) (&wSoundSetStrength) = load_function(ws3dHandle, "wSoundSetStrength");
	*(void**) (&wSoundSetVelocity) = load_function(ws3dHandle, "wSoundSetVelocity");
	*(void**) (&wSoundSetVolume) = load_function(ws3dHandle, "wSoundSetVolume");
	*(void**) (&wSoundStop) = load_function(ws3dHandle, "wSoundStop");
	*(void**) (&wSoundUpdate) = load_function(ws3dHandle, "wSoundUpdate");
	*(void**) (&wSphericalTerrainCreate) = load_function(ws3dHandle, "wSphericalTerrainCreate");
	*(void**) (&wSphericalTerrainGetSurfaceAngle) = load_function(ws3dHandle, "wSphericalTerrainGetSurfaceAngle");
	*(void**) (&wSphericalTerrainGetSurfaceLogicalPosition) = load_function(ws3dHandle, "wSphericalTerrainGetSurfaceLogicalPosition");
	*(void**) (&wSphericalTerrainGetSurfacePosition) = load_function(ws3dHandle, "wSphericalTerrainGetSurfacePosition");
	*(void**) (&wSphericalTerrainLoadVertexColor) = load_function(ws3dHandle, "wSphericalTerrainLoadVertexColor");
	*(void**) (&wSphericalTerrainSetTextures) = load_function(ws3dHandle, "wSphericalTerrainSetTextures");
	*(void**) (&wSystemCreateScreenShot) = load_function(ws3dHandle, "wSystemCreateScreenShot");
	*(void**) (&wSystemGetAvailableMemory) = load_function(ws3dHandle, "wSystemGetAvailableMemory");
	*(void**) (&wSystemGetClipboardText) = load_function(ws3dHandle, "wSystemGetClipboardText");
	*(void**) (&wSystemGetMaxTextureSize) = load_function(ws3dHandle, "wSystemGetMaxTextureSize");
	*(void**) (&wSystemGetProcessorSpeed) = load_function(ws3dHandle, "wSystemGetProcessorSpeed");
	*(void**) (&wSystemGetTotalMemory) = load_function(ws3dHandle, "wSystemGetTotalMemory");
	*(void**) (&wSystemGetVersion) = load_function(ws3dHandle, "wSystemGetVersion");
	*(void**) (&wSystemIsDriverSupported) = load_function(ws3dHandle, "wSystemIsDriverSupported");
	*(void**) (&wSystemIsTextureCreationFlag) = load_function(ws3dHandle, "wSystemIsTextureCreationFlag");
	*(void**) (&wSystemIsTextureFormatSupported) = load_function(ws3dHandle, "wSystemIsTextureFormatSupported");
	*(void**) (&wSystemSaveScreenShot) = load_function(ws3dHandle, "wSystemSaveScreenShot");
	*(void**) (&wSystemSetClipboardText) = load_function(ws3dHandle, "wSystemSetClipboardText");
	*(void**) (&wSystemSetTextureCreationFlag) = load_function(ws3dHandle, "wSystemSetTextureCreationFlag");
	*(void**) (&wTerrainCreate) = load_function(ws3dHandle, "wTerrainCreate");
	*(void**) (&wTerrainGetHeight) = load_function(ws3dHandle, "wTerrainGetHeight");
	*(void**) (&wTerrainScaleDetailTexture) = load_function(ws3dHandle, "wTerrainScaleDetailTexture");
	*(void**) (&wTextureConvertToImage) = load_function(ws3dHandle, "wTextureConvertToImage");
	*(void**) (&wTextureCopy) = load_function(ws3dHandle, "wTextureCopy");
	*(void**) (&wTextureCreate) = load_function(ws3dHandle, "wTextureCreate");
	*(void**) (&wTextureCreateRenderTarget) = load_function(ws3dHandle, "wTextureCreateRenderTarget");
	*(void**) (&wTextureDestroy) = load_function(ws3dHandle, "wTextureDestroy");
	*(void**) (&wTextureDraw) = load_function(ws3dHandle, "wTextureDraw");
	*(void**) (&wTextureDrawAdvanced) = load_function(ws3dHandle, "wTextureDrawAdvanced");
	*(void**) (&wTextureDrawElement) = load_function(ws3dHandle, "wTextureDrawElement");
	*(void**) (&wTextureDrawElementAdvanced) = load_function(ws3dHandle, "wTextureDrawElementAdvanced");
	*(void**) (&wTextureDrawElementStretch) = load_function(ws3dHandle, "wTextureDrawElementStretch");
	*(void**) (&wTextureDrawEx) = load_function(ws3dHandle, "wTextureDrawEx");
	*(void**) (&wTextureDrawMouseCursor) = load_function(ws3dHandle, "wTextureDrawMouseCursor");
	*(void**) (&wTextureFlip) = load_function(ws3dHandle, "wTextureFlip");
	*(void**) (&wTextureGetFullName) = load_function(ws3dHandle, "wTextureGetFullName");
	*(void**) (&wTextureGetInformation) = load_function(ws3dHandle, "wTextureGetInformation");
	*(void**) (&wTextureGetInternalName) = load_function(ws3dHandle, "wTextureGetInternalName");
	*(void**) (&wTextureLoad) = load_function(ws3dHandle, "wTextureLoad");
	*(void**) (&wTextureLock) = load_function(ws3dHandle, "wTextureLock");
	*(void**) (&wTextureMakeNormalMap) = load_function(ws3dHandle, "wTextureMakeNormalMap");
	*(void**) (&wTextureSave) = load_function(ws3dHandle, "wTextureSave");
	*(void**) (&wTextureSetAlpha) = load_function(ws3dHandle, "wTextureSetAlpha");
	*(void**) (&wTextureSetBlur) = load_function(ws3dHandle, "wTextureSetBlur");
	*(void**) (&wTextureSetBrightness) = load_function(ws3dHandle, "wTextureSetBrightness");
	*(void**) (&wTextureSetColorKey) = load_function(ws3dHandle, "wTextureSetColorKey");
	*(void**) (&wTextureSetContrast) = load_function(ws3dHandle, "wTextureSetContrast");
	*(void**) (&wTextureSetGray) = load_function(ws3dHandle, "wTextureSetGray");
	*(void**) (&wTextureSetInverse) = load_function(ws3dHandle, "wTextureSetInverse");
	*(void**) (&wTextureUnlock) = load_function(ws3dHandle, "wTextureUnlock");
	*(void**) (&wTexturesSetBlendMode) = load_function(ws3dHandle, "wTexturesSetBlendMode");
	*(void**) (&wTiledTerrainAddTile) = load_function(ws3dHandle, "wTiledTerrainAddTile");
	*(void**) (&wTiledTerrainCreate) = load_function(ws3dHandle, "wTiledTerrainCreate");
	*(void**) (&wTiledTerrainSetTileColor) = load_function(ws3dHandle, "wTiledTerrainSetTileColor");
	*(void**) (&wTiledTerrainSetTileStructure) = load_function(ws3dHandle, "wTiledTerrainSetTileStructure");
	*(void**) (&wTimerGetDelta) = load_function(ws3dHandle, "wTimerGetDelta");
	*(void**) (&wTimerGetRealTime) = load_function(ws3dHandle, "wTimerGetRealTime");
	*(void**) (&wTimerGetRealTimeAndDate) = load_function(ws3dHandle, "wTimerGetRealTimeAndDate");
	*(void**) (&wTimerGetTime) = load_function(ws3dHandle, "wTimerGetTime");
	*(void**) (&wTimerIsStopped) = load_function(ws3dHandle, "wTimerIsStopped");
	*(void**) (&wTimerSetSpeed) = load_function(ws3dHandle, "wTimerSetSpeed");
	*(void**) (&wTimerSetTime) = load_function(ws3dHandle, "wTimerSetTime");
	*(void**) (&wTimerStart) = load_function(ws3dHandle, "wTimerStart");
	*(void**) (&wTimerStop) = load_function(ws3dHandle, "wTimerStop");
	*(void**) (&wTimerTick) = load_function(ws3dHandle, "wTimerTick");
	*(void**) (&wTpsCameraCreate) = load_function(ws3dHandle, "wTpsCameraCreate");
	*(void**) (&wTpsCameraDestroy) = load_function(ws3dHandle, "wTpsCameraDestroy");
	*(void**) (&wTpsCameraGetCamera) = load_function(ws3dHandle, "wTpsCameraGetCamera");
	*(void**) (&wTpsCameraRotateHorizontal) = load_function(ws3dHandle, "wTpsCameraRotateHorizontal");
	*(void**) (&wTpsCameraRotateVertical) = load_function(ws3dHandle, "wTpsCameraRotateVertical");
	*(void**) (&wTpsCameraSetCurrentDistance) = load_function(ws3dHandle, "wTpsCameraSetCurrentDistance");
	*(void**) (&wTpsCameraSetDefaultDistanceDirection) = load_function(ws3dHandle, "wTpsCameraSetDefaultDistanceDirection");
	*(void**) (&wTpsCameraSetHorizontalRotation) = load_function(ws3dHandle, "wTpsCameraSetHorizontalRotation");
	*(void**) (&wTpsCameraSetHorizontalSpeed) = load_function(ws3dHandle, "wTpsCameraSetHorizontalSpeed");
	*(void**) (&wTpsCameraSetMaximalDistance) = load_function(ws3dHandle, "wTpsCameraSetMaximalDistance");
	*(void**) (&wTpsCameraSetMinimalDistance) = load_function(ws3dHandle, "wTpsCameraSetMinimalDistance");
	*(void**) (&wTpsCameraSetRelativeTarget) = load_function(ws3dHandle, "wTpsCameraSetRelativeTarget");
	*(void**) (&wTpsCameraSetTarget) = load_function(ws3dHandle, "wTpsCameraSetTarget");
	*(void**) (&wTpsCameraSetVerticalRotation) = load_function(ws3dHandle, "wTpsCameraSetVerticalRotation");
	*(void**) (&wTpsCameraSetVerticalSpeed) = load_function(ws3dHandle, "wTpsCameraSetVerticalSpeed");
	*(void**) (&wTpsCameraSetZoomStepSize) = load_function(ws3dHandle, "wTpsCameraSetZoomStepSize");
	*(void**) (&wTpsCameraUpdate) = load_function(ws3dHandle, "wTpsCameraUpdate");
	*(void**) (&wTpsCameraZoomIn) = load_function(ws3dHandle, "wTpsCameraZoomIn");
	*(void**) (&wTpsCameraZoomOut) = load_function(ws3dHandle, "wTpsCameraZoomOut");
	*(void**) (&wTreeCreate) = load_function(ws3dHandle, "wTreeCreate");
	*(void**) (&wTreeGeneratorCreate) = load_function(ws3dHandle, "wTreeGeneratorCreate");
	*(void**) (&wTreeGeneratorDestroy) = load_function(ws3dHandle, "wTreeGeneratorDestroy");
	*(void**) (&wTreeGetBillboardVertexColor) = load_function(ws3dHandle, "wTreeGetBillboardVertexColor");
	*(void**) (&wTreeGetLeafNode) = load_function(ws3dHandle, "wTreeGetLeafNode");
	*(void**) (&wTreeGetMeshBuffer) = load_function(ws3dHandle, "wTreeGetMeshBuffer");
	*(void**) (&wTreeIsLeafEnabled) = load_function(ws3dHandle, "wTreeIsLeafEnabled");
	*(void**) (&wTreeSetBillboardVertexColor) = load_function(ws3dHandle, "wTreeSetBillboardVertexColor");
	*(void**) (&wTreeSetDistances) = load_function(ws3dHandle, "wTreeSetDistances");
	*(void**) (&wTreeSetLeafEnabled) = load_function(ws3dHandle, "wTreeSetLeafEnabled");
	*(void**) (&wUtilColor4fToStr) = load_function(ws3dHandle, "wUtilColor4fToStr");
	*(void**) (&wUtilColor4fToUInt) = load_function(ws3dHandle, "wUtilColor4fToUInt");
	*(void**) (&wUtilColor4sToStr) = load_function(ws3dHandle, "wUtilColor4sToStr");
	*(void**) (&wUtilColor4sToUInt) = load_function(ws3dHandle, "wUtilColor4sToUInt");
	*(void**) (&wUtilFloatToStr) = load_function(ws3dHandle, "wUtilFloatToStr");
	*(void**) (&wUtilIntToStr) = load_function(ws3dHandle, "wUtilIntToStr");
	*(void**) (&wUtilStrAddNullChar) = load_function(ws3dHandle, "wUtilStrAddNullChar");
	*(void**) (&wUtilStrToFloat) = load_function(ws3dHandle, "wUtilStrToFloat");
	*(void**) (&wUtilStrToInt) = load_function(ws3dHandle, "wUtilStrToInt");
	*(void**) (&wUtilStrToUInt) = load_function(ws3dHandle, "wUtilStrToUInt");
	*(void**) (&wUtilStrToWideStr) = load_function(ws3dHandle, "wUtilStrToWideStr");
	*(void**) (&wUtilSwapFloat) = load_function(ws3dHandle, "wUtilSwapFloat");
	*(void**) (&wUtilSwapInt) = load_function(ws3dHandle, "wUtilSwapInt");
	*(void**) (&wUtilSwapUInt) = load_function(ws3dHandle, "wUtilSwapUInt");
	*(void**) (&wUtilUIntToColor4f) = load_function(ws3dHandle, "wUtilUIntToColor4f");
	*(void**) (&wUtilUIntToColor4s) = load_function(ws3dHandle, "wUtilUIntToColor4s");
	*(void**) (&wUtilUIntToStr) = load_function(ws3dHandle, "wUtilUIntToStr");
	*(void**) (&wUtilVector2fToStr) = load_function(ws3dHandle, "wUtilVector2fToStr");
	*(void**) (&wUtilVector3fToStr) = load_function(ws3dHandle, "wUtilVector3fToStr");
	*(void**) (&wUtilWideStrAddNullChar) = load_function(ws3dHandle, "wUtilWideStrAddNullChar");
	*(void**) (&wUtilWideStrToStr) = load_function(ws3dHandle, "wUtilWideStrToStr");
	*(void**) (&wVideoCreateTargetImage) = load_function(ws3dHandle, "wVideoCreateTargetImage");
	*(void**) (&wVideoDestroy) = load_function(ws3dHandle, "wVideoDestroy");
	*(void**) (&wVideoGetFramePosition) = load_function(ws3dHandle, "wVideoGetFramePosition");
	*(void**) (&wVideoGetFrameSize) = load_function(ws3dHandle, "wVideoGetFrameSize");
	*(void**) (&wVideoGetQuality) = load_function(ws3dHandle, "wVideoGetQuality");
	*(void**) (&wVideoGetSoundNode) = load_function(ws3dHandle, "wVideoGetSoundNode");
	*(void**) (&wVideoGetTargetTexture) = load_function(ws3dHandle, "wVideoGetTargetTexture");
	*(void**) (&wVideoGetTimePosition) = load_function(ws3dHandle, "wVideoGetTimePosition");
	*(void**) (&wVideoIsAtEnd) = load_function(ws3dHandle, "wVideoIsAtEnd");
	*(void**) (&wVideoIsEmpty) = load_function(ws3dHandle, "wVideoIsEmpty");
	*(void**) (&wVideoIsLooping) = load_function(ws3dHandle, "wVideoIsLooping");
	*(void**) (&wVideoIsPaused) = load_function(ws3dHandle, "wVideoIsPaused");
	*(void**) (&wVideoIsPlaying) = load_function(ws3dHandle, "wVideoIsPlaying");
	*(void**) (&wVideoLoad) = load_function(ws3dHandle, "wVideoLoad");
	*(void**) (&wVideoPause) = load_function(ws3dHandle, "wVideoPause");
	*(void**) (&wVideoPlay) = load_function(ws3dHandle, "wVideoPlay");
	*(void**) (&wVideoRewind) = load_function(ws3dHandle, "wVideoRewind");
	*(void**) (&wVideoSetLoopMode) = load_function(ws3dHandle, "wVideoSetLoopMode");
	*(void**) (&wVideoUpdate) = load_function(ws3dHandle, "wVideoUpdate");
	*(void**) (&wWaterSurfaceCreate) = load_function(ws3dHandle, "wWaterSurfaceCreate");
	*(void**) (&wWindGeneratorCreate) = load_function(ws3dHandle, "wWindGeneratorCreate");
	*(void**) (&wWindGeneratorDestroy) = load_function(ws3dHandle, "wWindGeneratorDestroy");
	*(void**) (&wWindGeneratorGetRegularity) = load_function(ws3dHandle, "wWindGeneratorGetRegularity");
	*(void**) (&wWindGeneratorGetStrength) = load_function(ws3dHandle, "wWindGeneratorGetStrength");
	*(void**) (&wWindGeneratorGetWind) = load_function(ws3dHandle, "wWindGeneratorGetWind");
	*(void**) (&wWindGeneratorSetRegularity) = load_function(ws3dHandle, "wWindGeneratorSetRegularity");
	*(void**) (&wWindGeneratorSetStrength) = load_function(ws3dHandle, "wWindGeneratorSetStrength");
	*(void**) (&wWindowGetSize) = load_function(ws3dHandle, "wWindowGetSize");
	*(void**) (&wWindowIsActive) = load_function(ws3dHandle, "wWindowIsActive");
	*(void**) (&wWindowIsFocused) = load_function(ws3dHandle, "wWindowIsFocused");
	*(void**) (&wWindowIsFullscreen) = load_function(ws3dHandle, "wWindowIsFullscreen");
	*(void**) (&wWindowIsMinimized) = load_function(ws3dHandle, "wWindowIsMinimized");
	*(void**) (&wWindowIsResizable) = load_function(ws3dHandle, "wWindowIsResizable");
	*(void**) (&wWindowMaximize) = load_function(ws3dHandle, "wWindowMaximize");
	*(void**) (&wWindowMinimize) = load_function(ws3dHandle, "wWindowMinimize");
	*(void**) (&wWindowMove) = load_function(ws3dHandle, "wWindowMove");
	*(void**) (&wWindowPlaceToCenter) = load_function(ws3dHandle, "wWindowPlaceToCenter");
	*(void**) (&wWindowResize) = load_function(ws3dHandle, "wWindowResize");
	*(void**) (&wWindowRestore) = load_function(ws3dHandle, "wWindowRestore");
	*(void**) (&wWindowSetCaption) = load_function(ws3dHandle, "wWindowSetCaption");
	*(void**) (&wWindowSetDepth) = load_function(ws3dHandle, "wWindowSetDepth");
	*(void**) (&wWindowSetFullscreen) = load_function(ws3dHandle, "wWindowSetFullscreen");
	*(void**) (&wWindowSetResizable) = load_function(ws3dHandle, "wWindowSetResizable");
	*(void**) (&wXEffectsAddNodeToDepthPass) = load_function(ws3dHandle, "wXEffectsAddNodeToDepthPass");
	*(void**) (&wXEffectsAddPostProcessingFromFile) = load_function(ws3dHandle, "wXEffectsAddPostProcessingFromFile");
	*(void**) (&wXEffectsAddShadowLight) = load_function(ws3dHandle, "wXEffectsAddShadowLight");
	*(void**) (&wXEffectsAddShadowToNode) = load_function(ws3dHandle, "wXEffectsAddShadowToNode");
	*(void**) (&wXEffectsEnableDepthPass) = load_function(ws3dHandle, "wXEffectsEnableDepthPass");
	*(void**) (&wXEffectsExcludeNodeFromLightingCalculations) = load_function(ws3dHandle, "wXEffectsExcludeNodeFromLightingCalculations");
	*(void**) (&wXEffectsGetDepthMapTexture) = load_function(ws3dHandle, "wXEffectsGetDepthMapTexture");
	*(void**) (&wXEffectsGetShadowLightColor) = load_function(ws3dHandle, "wXEffectsGetShadowLightColor");
	*(void**) (&wXEffectsGetShadowLightFarValue) = load_function(ws3dHandle, "wXEffectsGetShadowLightFarValue");
	*(void**) (&wXEffectsGetShadowLightMapResolution) = load_function(ws3dHandle, "wXEffectsGetShadowLightMapResolution");
	*(void**) (&wXEffectsGetShadowLightPosition) = load_function(ws3dHandle, "wXEffectsGetShadowLightPosition");
	*(void**) (&wXEffectsGetShadowLightTarget) = load_function(ws3dHandle, "wXEffectsGetShadowLightTarget");
	*(void**) (&wXEffectsGetShadowLightsCount) = load_function(ws3dHandle, "wXEffectsGetShadowLightsCount");
	*(void**) (&wXEffectsGetShadowMapTexture) = load_function(ws3dHandle, "wXEffectsGetShadowMapTexture");
	*(void**) (&wXEffectsRemoveShadowFromNode) = load_function(ws3dHandle, "wXEffectsRemoveShadowFromNode");
	*(void**) (&wXEffectsSetAmbientColor) = load_function(ws3dHandle, "wXEffectsSetAmbientColor");
	*(void**) (&wXEffectsSetClearColor) = load_function(ws3dHandle, "wXEffectsSetClearColor");
	*(void**) (&wXEffectsSetPostProcessingUserTexture) = load_function(ws3dHandle, "wXEffectsSetPostProcessingUserTexture");
	*(void**) (&wXEffectsSetScreenRenderTargetResolution) = load_function(ws3dHandle, "wXEffectsSetScreenRenderTargetResolution");
	*(void**) (&wXEffectsSetShadowLightColor) = load_function(ws3dHandle, "wXEffectsSetShadowLightColor");
	*(void**) (&wXEffectsSetShadowLightMapResolution) = load_function(ws3dHandle, "wXEffectsSetShadowLightMapResolution");
	*(void**) (&wXEffectsSetShadowLightPosition) = load_function(ws3dHandle, "wXEffectsSetShadowLightPosition");
	*(void**) (&wXEffectsSetShadowLightTarget) = load_function(ws3dHandle, "wXEffectsSetShadowLightTarget");
	*(void**) (&wXEffectsStart) = load_function(ws3dHandle, "wXEffectsStart");
	*(void**) (&wXMLReaderCreateUTF8) = load_function(ws3dHandle, "wXMLReaderCreateUTF8");
	*(void**) (&wXmlGetAttributeNameByIdx) = load_function(ws3dHandle, "wXmlGetAttributeNameByIdx");
	*(void**) (&wXmlGetAttributeValueByIdx) = load_function(ws3dHandle, "wXmlGetAttributeValueByIdx");
	*(void**) (&wXmlGetAttributeValueByName) = load_function(ws3dHandle, "wXmlGetAttributeValueByName");
	*(void**) (&wXmlGetAttributeValueFloatByIdx) = load_function(ws3dHandle, "wXmlGetAttributeValueFloatByIdx");
	*(void**) (&wXmlGetAttributeValueFloatByName) = load_function(ws3dHandle, "wXmlGetAttributeValueFloatByName");
	*(void**) (&wXmlGetAttributeValueIntByIdx) = load_function(ws3dHandle, "wXmlGetAttributeValueIntByIdx");
	*(void**) (&wXmlGetAttributeValueIntByName) = load_function(ws3dHandle, "wXmlGetAttributeValueIntByName");
	*(void**) (&wXmlGetAttributeValueSafeByName) = load_function(ws3dHandle, "wXmlGetAttributeValueSafeByName");
	*(void**) (&wXmlGetAttributesCount) = load_function(ws3dHandle, "wXmlGetAttributesCount");
	*(void**) (&wXmlGetNodeData) = load_function(ws3dHandle, "wXmlGetNodeData");
	*(void**) (&wXmlGetNodeName) = load_function(ws3dHandle, "wXmlGetNodeName");
	*(void**) (&wXmlGetNodeType) = load_function(ws3dHandle, "wXmlGetNodeType");
	*(void**) (&wXmlGetParserFormat) = load_function(ws3dHandle, "wXmlGetParserFormat");
	*(void**) (&wXmlGetSourceFormat) = load_function(ws3dHandle, "wXmlGetSourceFormat");
	*(void**) (&wXmlIsEmptyElement) = load_function(ws3dHandle, "wXmlIsEmptyElement");
	*(void**) (&wXmlRead) = load_function(ws3dHandle, "wXmlRead");
	*(void**) (&wXmlReaderCreate) = load_function(ws3dHandle, "wXmlReaderCreate");
	*(void**) (&wXmlReaderDestroy) = load_function(ws3dHandle, "wXmlReaderDestroy");
	*(void**) (&wXmlWriteClosingTag) = load_function(ws3dHandle, "wXmlWriteClosingTag");
	*(void**) (&wXmlWriteComment) = load_function(ws3dHandle, "wXmlWriteComment");
	*(void**) (&wXmlWriteElement) = load_function(ws3dHandle, "wXmlWriteElement");
	*(void**) (&wXmlWriteHeader) = load_function(ws3dHandle, "wXmlWriteHeader");
	*(void**) (&wXmlWriteLineBreak) = load_function(ws3dHandle, "wXmlWriteLineBreak");
	*(void**) (&wXmlWriteText) = load_function(ws3dHandle, "wXmlWriteText");
	*(void**) (&wXmlWriterCreate) = load_function(ws3dHandle, "wXmlWriterCreate");
	*(void**) (&wXmlWriterDestroy) = load_function(ws3dHandle, "wXmlWriterDestroy");
	*(void**) (&wZoneManagerAddTerrain) = load_function(ws3dHandle, "wZoneManagerAddTerrain");
	*(void**) (&wZoneManagerCreate) = load_function(ws3dHandle, "wZoneManagerCreate");
	*(void**) (&wZoneManagerSetBoundingBox) = load_function(ws3dHandle, "wZoneManagerSetBoundingBox");
	*(void**) (&wZoneManagerSetProperties) = load_function(ws3dHandle, "wZoneManagerSetProperties");
}

void wLibraryUnload()
{
	unload_library(ws3dHandle);
	ws3dHandle = NULL;
}