#include "weapon_item.h"
#include "weapons.h"
#include "bullets.h"
#include "res_manager.h"
#include "file_system.h"
#include "sound.h"

using namespace FileSystem;

vector<TWeaponItemInMem> g_WeaponItems;
int TWeaponItemNode::count = 0;


// создание ноды
TWeaponItemNode::TWeaponItemNode (TWeaponItemInMem* index) : TBillboard(index)
{
    addVar("type", "weaponItem");                  // имя типа объекта
    if (index)
    {
        addVar("curAmmo",   (int)(index->curAmmo));
        addVar("maxAmmo",   (int)(index->maxAmmo));
        addVar("infiniteAmmo", index->infiniteAmmo);
        addVar("weaponNumber", index->weaponNumber + 1); // нумерация внутри скриптов с 1
        addVar("pickUpSound",  index->pickUpSound);  // имя звука подбора

        /* имя типа патронов по id */
        if (index->ammoTypeId != -1 &&
            index->ammoTypeId < g_AmmoTypes.size())
        {
            addVar("ammoType", g_AmmoTypes.at(index->ammoTypeId));
        }
    }
    ++count;
}


void InitWeaponItems()
{
    if (g_WeaponItems.capacity() != WEAPONS_MAX_COUNT)
        g_WeaponItems.reserve(WEAPONS_MAX_COUNT);

    if (g_WeaponItems.size() != 0)
        g_WeaponItems.resize(0);
/*
    for (auto& item : g_WeaponItems)
    {
        item.clear();
    }
*/
}

bool LoadWeaponItemsPack()
{
    bool    isLoaded = false;
    //TWeaponItemInMem* weaponItem = nullptr;
    //TWeaponInMem* weapon = nullptr;

    char*    buffer = nullptr;
    uint64_t bufferSize;
    string   fileName;

    string   name;
    int      pickUpSoundId;

    double   damage, accuracy, distance;
    uint32_t curAmmo, maxAmmo;
    bool     infiniteAmmo;
    int      ammoTypeId;
    int      bulletId, fireType;
    double   solidX, solidY;

    uint8_t  animSpeed;
    uint32_t framesCount;

    // читаем количество текстур
    File::readFull("weapons.cfg", &buffer, &bufferSize);
    if (bufferSize <= 0 ||
        buffer == nullptr ||
        !Res_OpenIniFromBuffer(buffer, bufferSize))
    {
        PrintWithColor("can not open weapons.cfg!!!\n");
        goto end;
    }
    g_WeaponsCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_WeaponsCount == 0)
        goto end;

    InitWeapons();
    InitWeaponItems();

    for (int i = 0; i < g_WeaponsCount; ++i)
    {
        // читаем инфу по текстуре
        fileName = "weapon_" + to_string(i) + ".cfg\0";
        File::readFull(fileName, &buffer, &bufferSize);
        if (bufferSize <= 0)
        {
            PrintWithColor((char*)("can not get size of weapon_" + to_string(i) + ".cfg").c_str());
            continue;
        }

        Res_OpenIniFromBuffer(buffer, bufferSize);

        name        = Res_IniReadString ("Options", "name", (char*)("Weapon #" + to_string(i) + "\0").c_str());
        damage      = Res_IniReadDouble ("Options", "damage",   0);
        curAmmo     = Res_IniReadInteger("Options", "cur_ammo", 0);
        maxAmmo     = Res_IniReadInteger("Options", "max_ammo", 0);
        infiniteAmmo = Res_IniReadBool  ("Options", "infinite_ammo", false);
        distance    = Res_IniReadDouble ("Options", "distance", 100);
        accuracy    = Res_IniReadDouble ("Options", "accuracy", 100);
        fireType    = Res_IniReadInteger("Options", "fire_type", 0);
        bulletId    = Res_IniReadInteger("Options", "bullet_id", -1);
        ammoTypeId  = Res_IniReadInteger("Options", "ammo_type_id", -1);
        pickUpSoundId= Res_IniReadInteger("Options", "pickup_sound_id", -1);
        solidX      = Res_IniReadDouble ("Options", "solid_x", 100.0);
        solidY      = Res_IniReadDouble ("Options", "solid_y", 100.0);

        animSpeed   = (uint8_t)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = Res_IniReadInteger("Options", "frames_count", 0);

        Res_CloseIni();

        // запоминаем настройки оружия
        if (i >= g_Weapons.size())
            g_Weapons.resize(g_Weapons.size() + 1);
        TWeaponInMem& weapon = g_Weapons[i];
        weapon.name     = name;
        weapon.posInRep = i;
        weapon.damage   = damage;
        weapon.accuracy = accuracy;
        weapon.fireType = fireType;
        weapon.distance = distance;
        weapon.curAmmo  = curAmmo;
        weapon.maxAmmo  = maxAmmo;
        weapon.infiniteAmmo = infiniteAmmo;
        weapon.bulletId   = bulletId;
        weapon.ammoTypeId = ammoTypeId;

        if (i >= g_WeaponItems.size())
            g_WeaponItems.resize(g_WeaponItems.size() + 1);
        TWeaponItemInMem& weaponItem = g_WeaponItems[i];
        weaponItem.name      = name;
        weaponItem.ammoTypeId= ammoTypeId;
        weaponItem.curAmmo   = curAmmo;
        weaponItem.maxAmmo   = maxAmmo;
        weaponItem.infiniteAmmo = infiniteAmmo;
        weaponItem.posInRep  = i;
        weaponItem.weaponNumber = i;   // номер оружия по порядку
        weaponItem.solidX    = solidX / 100.0;
        weaponItem.solidY    = solidY / 100.0;
        weaponItem.animSpeed = animSpeed;
        weaponItem.script.loadFromFile("weapon_item.js");
        /* ищем имя звука по id */
        if (pickUpSoundId != -1 && pickUpSoundId < g_SoundBuffers.size())
            for (auto snd : g_SoundBuffers)
                if (pickUpSoundId == snd.second->id)
                {
                    weaponItem.pickUpSound = snd.first;
                    break;
                }

        // если не нужна для загрузки
        if (framesCount == 0 || !g_WeaponItemsNeedForLoad[i])
            continue;

        weaponItem.loadFrames();
    }
    isLoaded = true;

end:
    free(buffer);
    Res_CloseIni();

    return isLoaded;
}
