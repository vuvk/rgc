#include <cmath>

#include "global.h"
#include "constants.h"
#include "js_util.h"
#include "util.h"
#include "base_object.h"
#include "player.h"


TPlayer* g_Player = nullptr;
TCamera* g_Camera = nullptr;
ScrResourceInMem g_PlayerResource;
ScrResourceInMem g_CameraResource;


TPlayer::TPlayer(float x, float y, float z) : TObject3D(&g_PlayerResource)
{
    //TObject3D::TObject3D();

    //this->id = 1;

    addVar("health", 100.0);
    addVar("type",   "player");            // имя типа объекта
    addVar("name",   "player");

    //this->name = "player";

    uint32_t tesselation = 8;
    float radius = 0.3 * BLOCK_SIZE;
    float length = 0.6 * BLOCK_SIZE;

    this->node = wNodeCreateCylinder(tesselation, radius, length, wCOLOR4s_RED);
    wNodeSetPosition(this->node, {x, y, z});

    wMaterial* mat = wNodeGetMaterial(this->node, 0);
    MaterialSetOldSchool(mat);

    createCollider();

    //wNodeSetDebugMode(this->node, /*wDebugMode.wDM_MESH_WIRE_OVERLAY | */wDebugMode.wDM_BBOX);
    //wNodeSetDebugDataVisible(this->node, true);

    //g_ObjectsIdTable[id] = this;
}

TPlayer::~TPlayer()
{
    if (this->node && this->physAnim)
        wAnimatorDestroy(this->node, this->physAnim);
}

void TPlayer::createCollider()
{
    float radius = 0.3 * BLOCK_SIZE;
    float length = 0.6 * BLOCK_SIZE;

    wMesh* mesh = wNodeGetMesh(this->node);
    this->physBody = wCollisionCreateFromMesh(mesh, this->node, 0);
    //this->physBody = wCollisionCreateFromBox(this->node);
    //wNodeAddCollision(this->node, this->physBody);

    if (g_WorldCollider != nullptr)
    {
        this->physAnim = wAnimatorCollisionResponseCreate(g_WorldCollider, this->node, 0.05f);

        ///Get & Set collision animator parameters
        wAnimatorCollisionResponse params;
        wAnimatorCollisionResponseGetParameters(this->physAnim, &params);
        params.gravity = {0.0, -BLOCK_SIZE, 0.0};
        params.ellipsoidRadius = {radius, radius, radius};
        params.ellipsoidTranslation = {0.0, -length / 2.0, 0.0};
        wAnimatorCollisionResponseSetParameters(this->physAnim, params);
    }
}

void TPlayer::destroyCollider()
{
    wNodeDestroyAllAnimators(this->node);
}

/*
 * TCamera
 * */
TCamera::TCamera (float x, float y, float z) : TObject3D(&g_CameraResource)
{
    //TObject3D::TObject3D();

    addVar("name", "camera");            // имя объекта
    addVar("type", "camera");            // имя типа объекта

    //this->name = "camera";

    //this->id = 2;
    this->node = wCameraCreate({x, y, z}, wVECTOR3f_ZERO);

    wCameraSetClipDistance(this->node, ((MAP_WIDTH * MAP_HEIGHT) >> 1), 0.001f);
    this->updateAngles();

    //g_ObjectsIdTable[id] = this;
}

void TCamera::updateAngles()
{
    if (this->pitch < 0.0f) this->pitch += 360.0;
    if (this->yaw   < 0.0f) this->yaw   += 360.0;
    if (this->roll  < 0.0f) this->roll  += 360.0;

    if (this->pitch >= 360.0f) this->pitch -= 360.0;
    if (this->yaw   >= 360.0f) this->yaw   -= 360.0;
    if (this->roll  >= 360.0f) this->roll  -= 360.0;

    float pitchRad = this->pitch * DEG_TO_RAD_COEFF;
    float yawRad   = this->yaw   * DEG_TO_RAD_COEFF;

    wVector3f pos = wNodeGetPosition(this->node);

    wVector3f target = {pos.x - sin(yawRad),
                        pos.y + tan(pitchRad),
                        pos.z - cos(yawRad)};
    wCameraSetTarget(this->node, target);
}

void TCamera::setPitch(float deg)
{
    this->pitch = deg;
    updateAngles();
}

void TCamera::setYaw(float deg)
{
    this->yaw = deg;
    updateAngles();
}
/*
void TCamera::setRoll(float deg)
{
    this->roll = deg;
    updateAngles();
}
*/



/** JerryScript */
/** INTERPRETER */

/** TARGET */
JS_FUNCTION(js_cameraGetTarget)
{
    if (g_Camera != nullptr && g_Camera->node != nullptr)
    {
        wVector3f tgt = wCameraGetTarget(g_Camera->node);
        double x = tgt.x;
        double y = tgt.y;
        double z = tgt.z;

        jerry_value_t array = jerry_create_typedarray (JERRY_TYPEDARRAY_FLOAT64, 3);

        jerry_length_t byteLength;
        jerry_length_t byteOffset;
        jerry_value_t arrayBuffer = jerry_get_typedarray_buffer(array, &byteOffset, &byteLength);

        jerry_arraybuffer_write(arrayBuffer,  0, (const uint8_t*)&x, sizeof(double));
        jerry_arraybuffer_write(arrayBuffer,  8, (const uint8_t*)&y, sizeof(double));
        jerry_arraybuffer_write(arrayBuffer, 16, (const uint8_t*)&z, sizeof(double));

        jerry_release_value(arrayBuffer);

        return array;
    }

    return jerry_create_undefined();
}

JS_FUNCTION(js_cameraGetTargetX)
{
    if (g_Camera != nullptr && g_Camera->node != nullptr)
    {
        wVector3f tgt = wCameraGetTarget(g_Camera->node);
        return jerry_create_number(tgt.x);
    }

    return jerry_create_number_nan();
}

JS_FUNCTION(js_cameraGetTargetY)
{
    if (g_Camera != nullptr && g_Camera->node != nullptr)
    {
        wVector3f tgt = wCameraGetTarget(g_Camera->node);
        return jerry_create_number(tgt.y);
    }

    return jerry_create_number_nan();
}

JS_FUNCTION(js_cameraGetTargetZ)
{
    if (g_Camera != nullptr && g_Camera->node != nullptr)
    {
        wVector3f tgt = wCameraGetTarget(g_Camera->node);
        return jerry_create_number(tgt.z);
    }

    return jerry_create_number_nan();
}

JS_FUNCTION(js_cameraUpdate)
{
    if (g_Camera != nullptr)
    {
        g_Camera->updateAngles();
    }

    return jerry_create_undefined();
}

/** ANGLES */

JS_FUNCTION(js_cameraSetPitch)
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    if (g_Camera != nullptr)
    {
        g_Camera->setPitch(jerry_get_number_value(arg0));
    }

    return jerry_create_undefined();
}

JS_FUNCTION(js_cameraSetYaw)
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    if (g_Camera != nullptr)
    {
        g_Camera->setYaw(jerry_get_number_value(arg0));
    }

    return jerry_create_undefined();
}

JS_FUNCTION(js_cameraGetPitch)
{
    if (g_Camera != nullptr)
    {
        return jerry_create_number(g_Camera->pitch);
    }

    return jerry_create_number_nan();
}

JS_FUNCTION(js_cameraGetYaw)
{
    if (g_Camera != nullptr)
    {
        return jerry_create_number(g_Camera->yaw);
    }

    return jerry_create_number_nan();
}

/** PLAYER */
/** POSITION */

void RegisterCameraJSFunctions()
{
    JS_RegisterCFunction(js_cameraGetTarget,  "cameraGetTarget" );
    JS_RegisterCFunction(js_cameraGetTargetX, "cameraGetTargetX");
    JS_RegisterCFunction(js_cameraGetTargetY, "cameraGetTargetY");
    JS_RegisterCFunction(js_cameraGetTargetZ, "cameraGetTargetZ");
    JS_RegisterCFunction(js_cameraUpdate,     "cameraUpdate"    );

    JS_RegisterCFunction(js_cameraSetPitch,   "cameraSetPitch"  );
    JS_RegisterCFunction(js_cameraSetYaw,     "cameraSetYaw"    );
    JS_RegisterCFunction(js_cameraGetPitch,   "cameraGetPitch"  );
    JS_RegisterCFunction(js_cameraGetYaw,     "cameraGetYaw"    );
}

void RegisterPlayerJSFunctions()
{
    //
}
