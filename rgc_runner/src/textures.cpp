#include <cstring>
#include <string>
#include <iostream>

#include "constants.h"
#include "global.h"
#include "textures.h"
#include "file_system.h"
#include "util.h"
#include "res_manager.h"
#include "SampleFunctions.h"

using namespace FileSystem;

vector<TTextureInMem> g_Textures;
vector<TTextureInMem> g_TexturesShadowed;
vector<TTextureNode*> g_TextureNodes;


/*
 * TTEXTURENODE
 * */
TTextureNode::TTextureNode(TTextureInMem* index, wNode* object)
{
    this->index     = index;
    this->animSpeed = index->animSpeed;
    this->node      = object;
}

TTextureNode::~TTextureNode()
{
    if (this->node != nullptr)
    {
        wMesh* mesh = wNodeGetMesh(this->node);
        if (mesh != nullptr)
            wMeshDestroy(mesh);
        wNodeDestroy(this->node);
    }
}

void TTextureNode::update()
{
    if (this->animSpeed == 0.0f)
        return;

    if (this->node == nullptr)
        return;

    if (this->_animSpeed < 1.0f)
    {
        this->_animSpeed += g_DeltaTime * this->animSpeed;
    }
    else
    {
        this->_animSpeed = 0.0f;

        // переходим на следующий кадр
        ++(this->curFrame);
        if (this->curFrame >= this->index->frames.size())
            this->curFrame = 0;

        // если там пустая текстура, то ищем на следующем кадре
        wTexture* txr = this->index->frames[this->curFrame];
        while (txr == nullptr)
        {
            ++(this->curFrame);
            if (this->curFrame >= this->index->frames.size())
                this->curFrame = 0;

            txr = this->index->frames[this->curFrame];
        }

        wMaterial* mat = wNodeGetMaterial(this->node, 0);
        if (mat)
            wMaterialSetTexture(mat, 0, txr);
    }
}

/*
 * GLOBAL
 * */
void InitTextures()
{
    if (g_Textures.capacity()         != TEXTURES_MAX_COUNT) g_Textures.reserve        (TEXTURES_MAX_COUNT);
    if (g_TexturesShadowed.capacity() != TEXTURES_MAX_COUNT) g_TexturesShadowed.reserve(TEXTURES_MAX_COUNT);

    if (g_Textures.size()         != 0) g_Textures.resize        (0);
    if (g_TexturesShadowed.size() != 0) g_TexturesShadowed.resize(0);
}

bool LoadTexturesPack()
{
    bool isLoaded = false;

    TTextureInMem* texture = nullptr;
    TTextureInMem* textureShadowed = nullptr;

    char*    buffer = nullptr;
    uint64_t bufferSize = 0;
    string   fileName;

    int      i;
    uint8_t  animSpeed;
    uint32_t framesCount;

    // читаем количество текстур
    File::readFull("textures.cfg", &buffer, &bufferSize);
    if ((buffer == nullptr) ||
        (bufferSize <= 0) ||
        !Res_OpenIniFromBuffer(buffer, bufferSize))
    {
        PrintWithColor("can not open textures.cfg!!!\n");
        goto end;
    }
    g_TexturesCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_TexturesCount == 0)
        goto end;

    InitTextures();

    for (i = 0; i < g_TexturesCount; ++i)
    {
        // читаем инфу по текстуре
        fileName = "texture_" + to_string(i) + ".cfg\0";
        File::readFull(fileName, &buffer, &bufferSize);
        if (bufferSize <= 0)
        {
            PrintWithColor((char*)("can not get size of " + fileName).c_str());
            continue;
        }

        g_Textures.push_back({});
        g_TexturesShadowed.push_back({});
        texture = &(g_Textures[i]);
        textureShadowed = &(g_TexturesShadowed[i]);

        Res_OpenIniFromBuffer(buffer, bufferSize);
        animSpeed   = (uint8_t)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = Res_IniReadInteger("Options", "frames_count",    0);
        Res_CloseIni();

        texture->posInRep  = i;
        texture->animSpeed = animSpeed;
        textureShadowed->posInRep  = i;
        textureShadowed->animSpeed = animSpeed;

        if (framesCount == 0 || !g_TexturesNeedForLoad[i])
            continue;

        // читаем кадры, если есть
        texture->loadFrames();
        textureShadowed->loadFrames(true);
    }

    isLoaded = true;

end:
    free(buffer);
    Res_CloseIni();

    return isLoaded;
}

void TextureNodesUpdate()
{
    if (g_TextureNodes.size() == 0)
        return;

    for (auto object : g_TextureNodes)
    {
        if (object)
            object->update();
    }
}
