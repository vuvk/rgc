#include "global.h"
#include "util.h"
#include "maps.h"
#include "textures.h"
#include "sprites.h"
#include "doors.h"
#include "keys.h"
#include "weapons.h"
#include "weapon_item.h"
#include "bullets.h"
#include "ammo.h"
#include "audio_system.h"
#include "sound.h"
#include "WorldSim3D.h"

using namespace FileSystem;


int g_TexturesCount = 0;
int g_SpritesCount  = 0;
int g_DoorsCount    = 0;
int g_KeysCount     = 0;
int g_WeaponsCount  = 0;
int g_BulletsCount  = 0;
int g_AmmoCount     = 0;
int g_MapsCount     = 0;
int64_t g_ScriptsCount = 0;      /** количество загруженных скриптов */

bool g_PreviewMode = false;

wVector2u g_Resolution = wDEFAULT_SCREENSIZE;
wDriverTypes g_Renderer = wDRT_OPENGL;   /** рендерер, с которым будет запущен раннер */
bool   g_FullScreenMode = false;  /** режим полного экрана */
bool   g_LimitFps = true;         /** установить лимит кадров в MAX_FPS */
bool   g_VSync    = false;        /** вертикальная синхронизация */
bool   g_ShowFog  = false;        /** отображать ли туман */
double g_DeltaTime = 0;           /** время между кадрами реальное */
double g_DeltaTimeScript = 0;     /** время между кадрами для скриптов */
double g_TimeForRunScript = 0;    /** задержка перед запуском скриптов */
double g_TimeForGC = 0.0;         /** задержка перед запуском сборки мусора */


wTexture* g_InvisibleTexture = nullptr;
wMesh*    g_InvisibleMesh = nullptr;
//wNode*    invisibleNode = nullptr;

wVector3f g_IntersectNormal = wVECTOR3f_ZERO;  /** нормаль с плоскостью пересечения лучом */
wVector3f g_IntersectPoint  = wVECTOR3f_ZERO;  /** точка пересечения с лучом */

wSelector* g_WorldCollider = nullptr;  /** коллайдер статичного мира - ВСЕ твёрдые объекты (включая стены) */
wNode* g_SolidObjects = nullptr;       /** пустая нода, к которой припарентены все твердые внутриигровые объекты (только те, которыми можно управлять) */

/* флаги над теми номерами текстур, которые нужно грузить */
vector<bool> g_TexturesNeedForLoad;
vector<bool> g_SpritesNeedForLoad;
vector<bool> g_DoorsNeedForLoad;
vector<bool> g_KeysNeedForLoad;
vector<bool> g_WeaponItemsNeedForLoad;
vector<bool> g_AmmoNeedForLoad;

/** список типов патронов */
vector<std::string> g_AmmoTypes;

/* мьютекс для обновления идентификатора объекта, обрабатываемого в данный момент */
//Mutex g_MutexNodes;

/** участок для компиляции JS-кода */
uint32_t* g_ByteCode = nullptr;

/** путь запуска */
std::string g_RunPath;

void InitRunner()
{
    if (g_LimitFps)
        wEngineSetFPS(MAX_FPS);

    //wLogSetLevel(wLL_DEBUG);
    ///Show logo WS3D
    //wEngineShowLogo(true);

    wSystemSetTextureCreationFlag(wTCF_CREATE_MIP_MAPS,       false);
    wSystemSetTextureCreationFlag(wTCF_ALLOW_NON_POWER_2,     false);
    wSystemSetTextureCreationFlag(wTCF_OPTIMIZED_FOR_QUALITY, false);
    //wSystemSetTextureCreationFlag(wTCF_OPTIMIZED_FOR_SPEED, true);
//    wSystemSetTextureCreationFlag(wTCF_NO_ALPHA_CHANNEL,      true);

    if (g_Renderer == wDriverTypes::wDRT_SOFTWARE)
    {
        wSystemSetTextureCreationFlag(wTextureCreationFlag::wTCF_ALWAYS_16_BIT, true);
        wSystemSetTextureCreationFlag(wTextureCreationFlag::wTCF_NO_ALPHA_CHANNEL, true);
    }

    // создаем мьютекс
    //g_MutexNodes = new Mutex();

    /* создаем хранилища */
    g_TexturesNeedForLoad.resize(TEXTURES_MAX_COUNT);
    g_SpritesNeedForLoad.resize (SPRITES_MAX_COUNT);
    g_DoorsNeedForLoad.resize   (DOORS_MAX_COUNT);
    g_KeysNeedForLoad.resize    (KEYS_MAX_COUNT);
    g_WeaponItemsNeedForLoad.resize(WEAPONS_MAX_COUNT);
    g_AmmoNeedForLoad.resize    (AMMO_MAX_COUNT);

    g_ByteCode = (uint32_t*)calloc(BYTECODE_MAX_LENGTH, sizeof(uint32_t));

    /* инициализируем PhysFS */
    FileManager::Init(g_RunPath);
    g_RunPath = ExtractPath(g_RunPath);

    /* ресурсы */
    FileManager::Mount("./");
    FileManager::Mount("assets/");
    FileManager::Mount("resources/");
    FileManager::Mount("resources/textures/");
    FileManager::Mount("resources/sprites/");
    FileManager::Mount("resources/maps/");
    FileManager::Mount("resources/door_keys/");
    FileManager::Mount("resources/weapons_ammo/");
    FileManager::Mount("resources/skyboxes/");
    FileManager::Mount("resources/sounds/");

    /* скрипты */
    FileManager::Mount("assets/scripts/");
    FileManager::Mount("assets/scripts/doors/");
    FileManager::Mount("assets/scripts/keys/");
    FileManager::Mount("assets/scripts/level/");
    FileManager::Mount("assets/scripts/player/");
    FileManager::Mount("assets/scripts/sprites/");
    FileManager::Mount("assets/scripts/weapons/");
    FileManager::Mount("assets/scripts/weapons/bullets/");
    FileManager::Mount("assets/scripts/weapons/weapon_items/");
    FileManager::Mount("assets/scripts/weapons/weapon_inhand/");
    FileManager::Mount("assets/scripts/weapons/ammo/");

    /* пак */
    FileManager::Mount("resources.pk3");

    /* инициализируем звук */
    AudioSystem::Init();

    /* менеджер ресурсов, если рантайм-загрузка функций */
    #ifdef __RES_MANAGER_RUNTIME_LOAD
    ResManagerLoadLibrary();
    #endif // __RES_MANAGER_RUNTIME_LOAD
}

void StopRunner()
{
    free(g_ByteCode);
    g_ByteCode = nullptr;

    ClearLevel();

    AudioSystem::Deinit();
    FileManager::Deinit();

    g_TexturesNeedForLoad.clear();
    g_SpritesNeedForLoad.clear();
    g_DoorsNeedForLoad.clear();
    g_KeysNeedForLoad.clear();
    g_WeaponItemsNeedForLoad.clear();
    g_AmmoNeedForLoad.clear();

    g_Textures.clear();
    g_TexturesShadowed.clear();
    g_Sprites.clear();
    g_Doors.clear();
    g_Keys.clear();
    g_Bullets.clear();
    g_WeaponItems.clear();
    g_Weapons.clear();
    g_Ammo.clear();
    g_AmmoTypes.clear();
    g_Images2D.clear();

    wEngineStop(true);

    #ifdef __WORLDSIM3D_RUNTIME_LOAD
    wLibraryUnload();
    #endif // __WORLDSIM3D_RUNTIME_LOAD

    /* менеджер ресурсов, если рантайм-загрузка функций */
    #ifdef __RES_MANAGER_RUNTIME_LOAD
    ResManagerUnloadLibrary();
    #endif // __RES_MANAGER_RUNTIME_LOAD
}

void MaterialSetOldSchool(wMaterial* material)
{
    wMaterialSetAntiAliasingMode(material, wAntiAliasingMode::wAAM_OFF);
    wMaterialSetFlag(material, wMaterialFlags::wMF_LIGHTING,           false);
    wMaterialSetFlag(material, wMaterialFlags::wMF_ANISOTROPIC_FILTER, false);
    wMaterialSetFlag(material, wMaterialFlags::wMF_ANTI_ALIASING,      false);
    wMaterialSetFlag(material, wMaterialFlags::wMF_BILINEAR_FILTER,    false);
    wMaterialSetFlag(material, wMaterialFlags::wMF_TRILINEAR_FILTER,   false);
    wMaterialSetFlag(material, wMaterialFlags::wMF_TEXTURE_WRAP,       false);
    //wMaterialSetFlag(material, wMF_BACK_FACE_CULLING,                 false);
    wMaterialSetFlag(material, wMaterialFlags::wMF_FOG_ENABLE,         g_ShowFog);
}

void PrepareTexturesNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    fill(g_TexturesNeedForLoad.begin(), g_TexturesNeedForLoad.end(), false);

    g_ShowFog = map->showFog;

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map->ceil[i][j];
            if ((element >  0) &&
                (element <= TEXTURES_MAX_COUNT))
                g_TexturesNeedForLoad[--element] = true;

            element = map->floor[i][j];
            if ((element >  0) &&
                (element <= TEXTURES_MAX_COUNT))
                g_TexturesNeedForLoad[--element] = true;

            element = map->level[i][j];
            if ((element >  0) &&
                (element <= TEXTURES_MAX_COUNT))
                g_TexturesNeedForLoad[--element] = true;
        }
    }
}


void PrepareSpritesNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    fill(g_SpritesNeedForLoad.begin(), g_SpritesNeedForLoad.end(), false);

    g_ShowFog = map->showFog;

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map->level[i][j] - TEXTURES_MAX_COUNT;
            if ((element >  0) &&
                (element <= SPRITES_MAX_COUNT))
                g_SpritesNeedForLoad[--element] = true;
        }
    }
}

void PrepareDoorsNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    fill(g_DoorsNeedForLoad.begin(), g_DoorsNeedForLoad.end(), false);

    g_ShowFog = map->showFog;

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map->level[i][j] - TEXTURES_MAX_COUNT - SPRITES_MAX_COUNT;
            if ((element >  0) &&
                (element <= DOORS_MAX_COUNT))
                g_DoorsNeedForLoad[--element] = true;
        }
    }
}

void PrepareKeysNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    fill(g_KeysNeedForLoad.begin(), g_KeysNeedForLoad.end(), false);

    g_ShowFog = map->showFog;

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map->level[i][j] - TEXTURES_MAX_COUNT - SPRITES_MAX_COUNT - DOORS_MAX_COUNT;
            if ((element > 0) &&
                (element <= KEYS_MAX_COUNT))
                g_KeysNeedForLoad[--element] = true;
        }
    }
}

void PrepareWeaponsNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    fill(g_WeaponItemsNeedForLoad.begin(), g_WeaponItemsNeedForLoad.end(), false);

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map->level[i][j] - TEXTURES_MAX_COUNT -
                                         SPRITES_MAX_COUNT  -
                                         DOORS_MAX_COUNT    -
                                         KEYS_MAX_COUNT;
            if ((element > 0) &&
                (element <= WEAPONS_MAX_COUNT))
                g_WeaponItemsNeedForLoad[--element] = true;
        }
    }
}

void PrepareAmmoNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap& map = mapPack.maps[numOfMap];
    fill(g_AmmoNeedForLoad.begin(), g_AmmoNeedForLoad.end(), false);

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map.level[i][j] - TEXTURES_MAX_COUNT -
                                        SPRITES_MAX_COUNT  -
                                        DOORS_MAX_COUNT    -
                                        KEYS_MAX_COUNT     -
                                        WEAPONS_MAX_COUNT;
            if ((element > 0) &&
                (element <= AMMO_MAX_COUNT))
                g_AmmoNeedForLoad[--element] = true;
        }
    }
}

wMesh* CreateInvisibleBlock(string name, float width, float height, float offsetX, float offsetY)
{
    // создаем невидимую текстуру, если она не создана ранее
    if (g_InvisibleTexture == nullptr)
        g_InvisibleTexture = CreateColorTexture({0, 0, 0, 255}, "Invisible Color");

    /*static wMesh* mesh;*/
    wMesh* mesh = nullptr;

    // создаем невидимый блок, если он не создан ранее
    //if (g_InvisibleMesh == nullptr)
    {
        /*
        float lt =  0.0    * BLOCK_SIZE;
        float rt =  width  * BLOCK_SIZE;
        float dn =  0.0    * BLOCK_SIZE;
        float up =  height * BLOCK_SIZE;
        float bk =  0.0    * BLOCK_SIZE;
        float fw = -width  * BLOCK_SIZE;
        */
        float lt =  offsetX            * BLOCK_SIZE;
        float rt =  (offsetX + width)  * BLOCK_SIZE;
        float dn =  offsetY            * BLOCK_SIZE;
        float up =  (offsetY + height) * BLOCK_SIZE;
        float bk = -offsetX            * BLOCK_SIZE;
        float fw = -(offsetX + width)  * BLOCK_SIZE;

        wVert verts[] =
        {
            // forward
            { { rt, dn, fw }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, { 1.0, 1.0 } },
            { { lt, dn, fw }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, { 0.0, 1.0 } },
            { { lt, up, fw }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, { 0.0, 0.0 } },
            { { rt, up, fw }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, { 1.0, 0.0 } },

            // backward
            { { lt, dn, bk }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { 1.0, 1.0 } },
            { { rt, dn, bk }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { 0.0, 1.0 } },
            { { rt, up, bk }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { 0.0, 0.0 } },
            { { lt, up, bk }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { 1.0, 0.0 } },

            // right
            { { rt, dn, bk }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, { 1.0, 1.0 } },
            { { rt, dn, fw }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, { 0.0, 1.0 } },
            { { rt, up, fw }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, { 0.0, 0.0 } },
            { { rt, up, bk }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, { 1.0, 0.0 } },

            // left
            { { lt, dn, fw }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 1.0, 1.0 } },
            { { lt, dn, bk }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 0.0, 1.0 } },
            { { lt, up, bk }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 0.0, 0.0 } },
            { { lt, up, fw }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 1.0, 0.0 } }
        };
        uint16_t indices[] = { 0,  1,  2,  0,  2,  3,
                               4,  5,  6,  4,  6,  7,
                               8,  9, 10,  8, 10, 11,
                              12, 13, 14, 12, 14, 15 };

        //g_InvisibleMesh = wMeshCreate((char*)name.c_str());
        mesh = wMeshCreate((char*)name.c_str());
        wMeshBuffer* meshBuffer = wMeshBufferCreate(16, verts, 24, indices);

        wMaterial* material = wMeshBufferGetMaterial(meshBuffer);
        wMaterialSetTexture(material, 0, g_InvisibleTexture);

        MaterialSetOldSchool(material);
        wMaterialSetFlag(material, wMaterialFlags::wMF_FOG_ENABLE, false);
        wMaterialSetType(material, wMaterialTypes::wMT_TRANSPARENT_ALPHA_CHANNEL);

        //wMeshAddMeshBuffer(g_InvisibleMesh, meshBuffer);
        //wMeshEnableHardwareAcceleration(g_InvisibleMesh, 0);
        wMeshAddMeshBuffer(mesh, meshBuffer);
        wMeshEnableHardwareAcceleration(mesh, 0);
    }

    //return g_InvisibleMesh;
    return mesh;
}
