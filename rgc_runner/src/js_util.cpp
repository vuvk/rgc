#include <cstdlib>

#include "sound.h"
#include "file_system.h"
#include "js_util.h"
#include "input.h"
#include "maps.h"
#include "base_object.h"
#include "weapons.h"
#include "player.h"


using namespace FileSystem;

/* Методы структуры скрипта */

TScriptJS::~TScriptJS()
{
    clear();
}

void TScriptJS::setId()
{
    this->id = g_ScriptsCount;
    ++g_ScriptsCount;
}

void TScriptJS::compile()
{
    if (text.size() == 0)
        return;

    memset(g_ByteCode, 0, BYTECODE_MAX_LENGTH * sizeof(uint32_t));

    jerry_value_t generate_result;
    generate_result = jerry_generate_snapshot (nullptr,
                                               0,
                                               (jerry_char_t*)(text.c_str()),
                                               text.size(),
                                               jerry_generate_snapshot_opts_t::JERRY_SNAPSHOT_SAVE_STRICT,
                                               g_ByteCode,
                                               BYTECODE_MAX_LENGTH);
    snapshot_size = 0;
    if (!jerry_value_is_error(generate_result))
    {
        snapshot_size = (size_t)jerry_get_number_value(generate_result);
        if (bytecode != nullptr)
            free(bytecode);
        bytecode = (uint32_t*)calloc(snapshot_size, 1);
        memcpy(bytecode, g_ByteCode, snapshot_size);
    }
    else
    {
        snapshot_size = 0;
        free(bytecode);
        bytecode = nullptr;
        string str = "Error when compiling code '" + fileName + "'";
        PrintWithColor((char*)(str.c_str()));
        int lineNum = JS_PrintError(generate_result);
        if (lineNum >= 0)
            printErrorLine(lineNum);
        getchar();
        return;
    }

    jerry_release_value (generate_result);

    //text = "";
}

void TScriptJS::loadFromFile(const std::string fileName)
{
    if (fileName == "" || !File::exists(fileName))
    {
        PrintWithColor((char*)(("File '" + fileName + "' is not exists!").c_str()), wCFC_RED, false);
        return;
    }

    clear();
    setId();
    this->fileName = fileName;

    char* buffer = nullptr;
    uint64_t bufferSize;
    File::readFull(fileName, &buffer, &bufferSize);
    if (buffer && bufferSize > 0)
    {
        // добавим в конец 0
        buffer = (char*)realloc(buffer, bufferSize + 1);
        buffer[bufferSize] = '\0';
        text = buffer;
    }
    free(buffer);
}

void TScriptJS::reload()
{
    loadFromFile(fileName);
}

void TScriptJS::clear()
{
    if (bytecode != nullptr)
    {
        free(bytecode);
        bytecode = nullptr;
    }
    fileName = "";
    text     = "";
    snapshot_size = 0;
}

void TScriptJS::run()
{
    if (text.size() == 0 && snapshot_size == 0)
        return;

    if (snapshot_size == 0)
    {
        jerry_value_t ret_val = jerry_eval ((jerry_char_t*)(text.c_str()), text.size(), 0);
        if (jerry_value_is_error(ret_val))
        {
            PrintWithColor((char*)("Error when executing script '" + this->fileName + "'").c_str());
            int lineNum = JS_PrintError(ret_val);
            if (lineNum >= 0)
                printErrorLine(lineNum);
            getchar();
        }

        jerry_release_value (ret_val);
    }
    else
    {
        if (bytecode != nullptr)
        {
            jerry_value_t ret_val = jerry_exec_snapshot (bytecode,
                                                         snapshot_size,
                                                         0,
                                                         JERRY_SNAPSHOT_EXEC_ALLOW_STATIC);
            if (jerry_value_is_error(ret_val))
            {
                PrintWithColor((char*)("Error when executing script '" + fileName + "'").c_str());
                int lineNum = JS_PrintError(ret_val);
                if (lineNum >= 0)
                    printErrorLine(lineNum);
                getchar();
            }

            jerry_release_value (ret_val);
        }
    }
}

void TScriptJS::printErrorLine(int lineNum)
{
    std::string prevLineStr, curLineStr, nextLineStr;
    if (lineNum < 0)
    {
        return;
    }
    else
    {
        int curLine = 0;
        string out = "";
        for (char c : this->text)
        {
            if (c != '\n')
                out += c;
            else
            {
                ++curLine;
                if (curLine == lineNum - 1)
                    prevLineStr = out;

                if (curLine == lineNum)
                    curLineStr = out;

                if (curLine == lineNum + 1)
                {
                    printf("%4d\t%s\n", lineNum - 1, prevLineStr.c_str());

                    printf("%4d\t", lineNum);
                    PrintWithColor((char*)curLineStr.c_str(), wCFC_LIGHTRED);

                    printf("%4d\t%s\n", lineNum + 1, nextLineStr.c_str());
                }

                out = "";
            }
        }
        std::cout << std::endl;
        // последняя строка
        //printf("%4d ", curLine);
        //PrintWithColor((char*)out.c_str(), wCFC_LIGHTRED);
    }
}


/**
 * Step 5. Description of JerryScript value descriptors
 */
void JS_PrintValue (jerry_value_t value)
{
    if (jerry_value_is_undefined (value) == 1)
    {
        printf ("undefined");
    }
    else if (jerry_value_is_null (value) == 1)
    {
        printf ("null");
    }
    else if (jerry_value_is_boolean (value) == 1)
    {
        if (jerry_get_boolean_value (value) == 1)
        {
            printf ("true");
        }
        else
        {
            printf ("false");
        }
    }
    /* Float value */
    else if (jerry_value_is_number (value) == 1)
    {
        printf ("number");
    }
    /* String value */
    else if (jerry_value_is_string (value) == 1)
    {
        /* Determining required buffer size */
        jerry_size_t req_sz = jerry_get_string_size (value);
        jerry_char_t* str_buf_p = (jerry_char_t*)calloc(sizeof(jerry_char_t), req_sz + 1);

        jerry_string_to_char_buffer (value, str_buf_p, req_sz);
        str_buf_p[req_sz] = '\0';

        printf ("%s", str_buf_p);

        free(str_buf_p);
    }
    /* Object reference */
    else if (jerry_value_is_object (value) == 1)
    {
        printf ("[JS object]");
    }

    printf ("\n");
}

/**
 *  Get info from throwable object and print it
 */
int JS_PrintError(jerry_value_t value)
{
    jerry_value_t throwable = jerry_get_value_from_error(value, 0);
    int line_num = -1;

    if (jerry_value_is_object(throwable))
    {
        jerry_value_t name    = jerry_create_string((jerry_char_t*)"name");
        jerry_value_t message = jerry_create_string((jerry_char_t*)"message");
        jerry_value_t stack   = jerry_create_string((jerry_char_t*)"stack");

        jerry_value_t exists;

        // print name
        exists = jerry_has_property (throwable, name);
        if ((jerry_value_is_boolean(exists)) && (jerry_get_boolean_value(exists)))
        {
            jerry_value_t txt = jerry_get_property(throwable, name);
            JS_PrintValue(txt);
            jerry_release_value (txt);
        }

        // print error message
        exists = jerry_has_property (throwable, message);
        if ((jerry_value_is_boolean(exists)) && (jerry_get_boolean_value(exists)))
        {
            jerry_value_t txt = jerry_get_property(throwable, message);
            JS_PrintValue(txt);
            if (jerry_value_is_string(txt))
            {
                std::string error_message = JS_GetStringValue(txt);
                int line_pos = error_message.find("[line:");
                if (line_pos != std::string::npos)
                {
                    error_message.erase(0, line_pos + 6 /* 6 == length of '[line:' */);
                    int dot_pos = error_message.find(",");
                    if (dot_pos != std::string::npos)
                    {
                        error_message.erase(dot_pos, error_message.size() - dot_pos);
                        line_num = std::stoi(error_message);
                    }
                }
            }
            jerry_release_value (txt);
        }

        // print backtrace
        exists = jerry_has_property (throwable, stack);
        if ((jerry_value_is_boolean(exists)) && (jerry_get_boolean_value(exists)))
        {
            if (!jerry_value_is_error (stack) && jerry_value_is_array (stack))
            {
                PrintWithColor("Exception backtrace:\n");

                uint32_t length = jerry_get_array_length (stack);

                /* This length should be enough. */
                if (length > 32)
                {
                    length = 32;
                }

                for (uint32_t i = 0; i < length; ++i)
                {
                    jerry_value_t item_val = jerry_get_property_by_index (stack, i);

                    if (!jerry_value_is_error (item_val) && jerry_value_is_string (item_val))
                    {
                        jerry_size_t str_size = jerry_get_string_size (item_val);
                        char error_txt[str_size];
                        memset(error_txt, 0, str_size);

                        if (str_size >= 256)
                        {
                            sprintf(error_txt, "%3u: [Backtrace string too long]\n", i);
                        }
                        else
                        {
                            jerry_size_t string_end = jerry_string_to_char_buffer (item_val, (jerry_char_t*)error_txt, str_size);
                            assert (string_end == str_size);
                        }
                        PrintWithColor(error_txt, wCFC_LIGHTRED);
                    }

                    jerry_release_value (item_val);
                }
            }
        }

        jerry_release_value (name);
        jerry_release_value (message);
        jerry_release_value (stack);
        jerry_release_value (exists);
    }

    jerry_release_value (throwable);
    return line_num;
}

void JS_RegisterCFunction(jerry_external_handler_t native_function, const char* new_name)
{
    jerryx_handler_register_global((jerry_char_t*)new_name, native_function);
}

void JS_RegisterCFunctionForObject(jerry_value_t object, string objectName, jerry_external_handler_t native_function, string new_name)
{
    /* Create a JS function object and wrap into a jerry value */
    jerry_value_t func_obj = jerry_create_external_function (native_function);

    /* Set the native function as a property of the empty JS object */
    jerry_value_t prop_name = jerry_create_string ((const jerry_char_t*)(new_name.c_str()));
    jerry_set_property (object, prop_name, func_obj);
    jerry_release_value (prop_name);
    jerry_release_value (func_obj);

    /* Wrap the JS object (not empty anymore) into a jerry api value */
    jerry_value_t global_object = jerry_get_global_object ();

    /* Add the JS object to the global context */
    prop_name = jerry_create_string ((const jerry_char_t*)(objectName.c_str()));
    jerry_set_property (global_object, prop_name, object);
    jerry_release_value (prop_name);
}

/*
 * INTERPRETATION
 * */

JS_FUNCTION(js_degToRad)
{
    jerry_value_t arg = args_p[0];

    if (args_cnt > 0 && jerry_value_is_number(arg))
    {
        double deg = jerry_get_number_value(arg);
        return jerry_create_number(deg * DEG_TO_RAD_COEFF);
    }

    return jerry_create_number_nan();
}
JS_FUNCTION(js_radToDeg)
{
    jerry_value_t arg = args_p[0];

    if (args_cnt > 0 && jerry_value_is_number(arg))
    {
        double rad = jerry_get_number_value(arg);
        return jerry_create_number(rad*RAD_TO_DEG_COEFF);
    }

    return jerry_create_number_nan();
}
JS_FUNCTION(js_distanceBetweenPoints)
{
    jerry_value_t ret_val;

    switch(args_cnt)
    {
        case 4 :
        {
            double x0 = jerry_get_number_value(args_p[0]);
            double y0 = jerry_get_number_value(args_p[1]);
            double x1 = jerry_get_number_value(args_p[2]);
            double y1 = jerry_get_number_value(args_p[3]);
            ret_val = jerry_create_number(sqrt(sqr(x1 - x0) + sqr(y1 - y0)));
            break;
        }

        case 6 :
        {
            double x0 = jerry_get_number_value(args_p[0]);
            double y0 = jerry_get_number_value(args_p[1]);
            double z0 = jerry_get_number_value(args_p[2]);
            double x1 = jerry_get_number_value(args_p[3]);
            double y1 = jerry_get_number_value(args_p[4]);
            double z1 = jerry_get_number_value(args_p[5]);
            ret_val = jerry_create_number(sqrt(sqr(x1 - x0) + sqr(y1 - y0) + sqr(z1 - z0)));
            break;
        }

        default:
            ret_val = jerry_create_number_nan();
            break;
    }

    if (jerry_value_is_error(ret_val))
        JS_PrintError(ret_val);
    return ret_val;
}


/** SYSTEM */
JS_FUNCTION(js_deltaTime)
{
    return jerry_create_number(g_DeltaTimeScript);
}
JS_FUNCTION(js_windowGetWidth)
{
    wVector2u wndSize;
    wWindowGetSize(&wndSize);

    return jerry_create_number(wndSize.x);
}
JS_FUNCTION(js_windowGetHeight)
{
    wVector2u wndSize;
    wWindowGetSize(&wndSize);

    return jerry_create_number(wndSize.y);
}



void JS_InitVM()
{
    jerry_init (JERRY_INIT_MEM_STATS);

    JS_RegisterCFunction(js_degToRad, "degToRad");
    JS_RegisterCFunction(js_radToDeg, "radToDeg");
    JS_RegisterCFunction(js_distanceBetweenPoints, "distanceBetweenPoints");

    /** system */
    JS_RegisterCFunction(jerryx_handler_print,  "print"          );
    JS_RegisterCFunction(jerryx_handler_gc,     "gc"             );
    JS_RegisterCFunction(jerryx_handler_assert, "assert"         );
    JS_RegisterCFunction(js_deltaTime,          "deltaTime"      );
    JS_RegisterCFunction(js_windowGetWidth,     "windowGetWidth" );
    JS_RegisterCFunction(js_windowGetHeight,    "windowGetHeight");

    /** sound */
    RegisterSoundJSFunctions();

    /** map */
    RegisterMapJSFunctions();

    /** base object */
    RegisterObjectJSFunctions();

    /** camera */
    RegisterCameraJSFunctions();

    /** player */
    RegisterPlayerJSFunctions();

    /** weapons */
    RegisterWeaponsJSFunctions();

    /** keyboard */
    RegisterKeyboardJSFunctions();

    /** mouse */
    RegisterMouseJSFunctions();

    g_ScriptsCount = 0;
}

void JS_StopVM()
{
    jerry_cleanup();
}

void JS_GC()
{
    jerry_gc(JERRY_GC_SEVERITY_HIGH);
}

string JS_GetStringValue(jerry_value_t value)
{
    string result;
    char*  buffer = nullptr;

    jerry_size_t size = jerry_get_string_size(value);
    buffer = (char*)calloc(size + 1, 1);
    jerry_string_to_char_buffer(value, (jerry_char_t*)buffer, size);

    result = buffer;
    free(buffer);

    return result;
}

jerry_value_t JS_PushDoubleArray(vector<double> values)
{
    jerry_value_t array = jerry_create_typedarray (JERRY_TYPEDARRAY_FLOAT64, values.size());

    jerry_length_t byteLength;
    jerry_length_t byteOffset;
    jerry_value_t arrayBuffer = jerry_get_typedarray_buffer (array, &byteOffset, &byteLength);

    int offset = 0;
    for (auto value : values)
    {
        jerry_arraybuffer_write(arrayBuffer, offset, (uint8_t*)&value, sizeof(double));
        offset += sizeof(double);
    }

    jerry_release_value(arrayBuffer);

    return array;
}

/* Реализация вывода символа. В Jerryscript не оказалось, но нужна в jerryscript-ext */
void jerryx_port_handler_print_char (char c)
{
    //printf("%c", c);
    cout << c;
}
