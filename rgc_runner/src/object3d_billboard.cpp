#include "object3d_billboard.h"

TBillboard::TBillboard(ScrResourceInMem *index, bool centered) : TObject3D(index)
{
    wMaterial* material = nullptr;
    wMesh* mesh = nullptr;

    double solidX = index->solidX;
    double solidY = index->solidY;
    double scaleX = index->scaleX;
    double scaleY = index->scaleY;

    if (!centered)
        mesh = CreateInvisibleBlock("solid box " + to_string(TObject3D::objects.size()), solidX, solidY, -solidX/2);
    else
        mesh = CreateInvisibleBlock("solid box " + to_string(TObject3D::objects.size()), solidX, solidY, -solidX/2, -solidY/2);

    this->solidBox = wNodeCreateFromMesh(mesh);
    this->node = solidBox;
    //wNodeSetDebugMode(solidBox, wDM_BBOX);
    //wNodeSetDebugDataVisible(solidBox, true);

    this->billboard = wBillboardCreate(wVECTOR3f_ZERO, {BLOCK_SIZE, BLOCK_SIZE});
    if (this->billboard)
    {
        wNodeSetParent(billboard, solidBox);

        if (index->frames.size() > 0)
        {
            material = wNodeGetMaterial(billboard, 0);
            wMaterialSetTexture(material, 0, index->frames[0]);
            MaterialSetOldSchool(material);
            wMaterialSetType(material, wMT_TRANSPARENT_ALPHA_CHANNEL);
        }

        // помещаем биллборд в центр
        if (!centered)
            wNodeMove(billboard, {0.0, 0.5, 0.0});

        // фиксация по вертикали
        wBillboardSetEnabledAxis(this->billboard, {false, true, false});
    }

    wVector3f scale = {scaleX, scaleY, scaleX};
    this->setScale(scale);
}


void TBillboard::setFrame(int frame)
{
    TObject::setFrame(frame);

    if (billboard &&
        frame >= 0 &&
        frame < this->index->frames.size())
    {
        wTexture* txr = this->index->frames[this->curFrame];
        if (txr)
        {
            wMaterial* mat = wNodeGetMaterial(this->billboard, 0);
            wMaterialSetTexture(mat, 0, txr);
        }
    }
}

void TBillboard::update()
{
    if (!this->isVisible || this->billboard == nullptr)
        return;

    TObject3D::update();
}

void TBillboard::setScale(wVector3f& scale)
{
    if (node)
        wNodeSetScale(node, scale);
    if (billboard)
        wBillboardSetSize(billboard, {scale.x * BLOCK_SIZE, scale.y * BLOCK_SIZE});
}
