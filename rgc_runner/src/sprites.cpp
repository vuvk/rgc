#include "sprites.h"
#include "res_manager.h"
#include "file_system.h"
#include "sound.h"

using namespace FileSystem;

vector<TSpriteInMem> g_Sprites;
int TSpriteNode::count = 0;


/*
 * TSpriteNode
 * */
TSpriteNode::TSpriteNode(TSpriteInMem* index) : TBillboard(index)
{
    wBillboardAxisParam axisParams;
    wMaterial* material = nullptr;
    wMesh* mesh = nullptr;

    addVar("type",        "sprite"  );           // имя типа
    addVar("destroyable", index->destroyable);   // можно ли разрушить
    addVar("deathSound",  index->deathSound);    // имя звука разрушения
    addVar("health",      index->endurance);     // текущее значение жизней
    addVar("playOnce",    index->playOnce);
    addVar("deleteOnLastFrame", index->deleteOnLastFrame);

    mesh = wNodeGetMesh(this->node);
    if (index->collision)
    {
        this->enableCollision();
    }

    if (this->billboard)
    {
        // устанавливаем параметры поворота спрайта
        switch (index->axisParams)
        {
            // смотреть на игрока
            default:
            case 0 :
                axisParams.isEnablePitch = false;
                axisParams.isEnableYaw   = true;
                axisParams.isEnableRoll  = false;
                break;

            // фиксация по горизонтали (XoY)
            case 1 :
                axisParams.isEnablePitch = true;
                axisParams.isEnableYaw   = true;
                axisParams.isEnableRoll  = false;
                if (billboard != nullptr)
                {
                    material = wNodeGetMaterial(billboard, 0);
                    wMaterialSetFlag(material, wMF_BACK_FACE_CULLING, false);
                }
                break;

            // фиксация по вертикали (ZoY)
            case 2 :
                axisParams.isEnablePitch = false;
                axisParams.isEnableYaw   = true;
                axisParams.isEnableRoll  = true;
                if (billboard != nullptr)
                {
                    material = wNodeGetMaterial(billboard, 0);
                    wMaterialSetFlag(material, wMF_BACK_FACE_CULLING, false);
                }
                break;
        }

        wBillboardSetEnabledAxis(billboard, axisParams);
    }

    ++count;
}



void InitSprites()
{
    if (g_Sprites.capacity() != SPRITES_MAX_COUNT)
        g_Sprites.reserve(SPRITES_MAX_COUNT);

    if (g_Sprites.size() != 0)
        g_Sprites.resize(0);
/*
    for (auto& sprite : g_Sprites)
    {
        sprite.clear();
    }
*/
}

bool LoadSpritesPack()
{
    bool    isLoaded = false;
    TSpriteInMem* sprite = nullptr;

    char*    buffer = nullptr;
    uint64_t bufferSize;
    string   fileName;

    string   name;
    int      i;
    uint8_t  animSpeed;
    uint32_t framesCount;
    uint8_t  axisParams;
    bool     collision;
    bool     destroyable;
    int      deathSoundId;
    double   endurance;
    bool     playOnce;
    bool     deleteOnLastFrame;
    float    scaleX, scaleY;
    float    solidX, solidY;

    // читаем количество текстур
    File::readFull("sprites.cfg", &buffer, &bufferSize);
    if (buffer == nullptr ||
        bufferSize <= 0 ||
        !Res_OpenIniFromBuffer(buffer, bufferSize))
    {
        PrintWithColor("can not open sprites.cfg!!!\n");
        goto end;
    }
    g_SpritesCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_SpritesCount == 0)
        goto end;

    InitSprites();

    for (i = 0; i < g_SpritesCount; ++i)
    {
        // читаем инфу по текстуре
        fileName = "sprite_" + to_string(i) + ".cfg\0";
        File::readFull(fileName, &buffer, &bufferSize);
        if (bufferSize <= 0)
        {
            PrintWithColor((char*)("can not get size of " + fileName).c_str());
            continue;
        }

        if (i >= g_Sprites.size())
            g_Sprites.resize(g_Sprites.size() + 1);
        sprite = &(g_Sprites[i]);

        Res_OpenIniFromBuffer(buffer, bufferSize);
        name        = Res_IniReadString("Options", "name", ("Sprite #" + to_string(i) + "\0").c_str());
        animSpeed   = (uint8_t)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = (uint8_t)Res_IniReadInteger("Options", "frames_count",    0);
        axisParams  = (uint8_t)Res_IniReadInteger("Options", "axis",            0);
        collision   = Res_IniReadBool   ("Options", "collision",   false);
        destroyable = Res_IniReadBool   ("Options", "destroyable", false);
        deathSoundId= Res_IniReadInteger("Options", "death_sound_id", -1);
        endurance   = Res_IniReadDouble ("Options", "endurance",   0.0);
        playOnce    = Res_IniReadBool   ("Options", "play_once",   false);
        deleteOnLastFrame = Res_IniReadBool("Options", "delete_on_last_frame", false);
        scaleX      = Res_IniReadDouble("Options", "scale_x", 100.0); // в процентах
        scaleY      = Res_IniReadDouble("Options", "scale_y", 100.0);
        solidX      = Res_IniReadDouble("Options", "solid_x", 100.0);
        solidY      = Res_IniReadDouble("Options", "solid_y", 100.0);
        Res_CloseIni();

        sprite->name        = name;
        sprite->posInRep    = i;
        sprite->animSpeed   = animSpeed;
        sprite->axisParams  = axisParams;
        sprite->collision   = collision;
        sprite->destroyable = destroyable;
        /* ищем имя звука по id */
        if (deathSoundId != -1 && deathSoundId < g_SoundBuffers.size())
        {
            for (auto key : g_SoundBuffers)
            {
                if (key.second->id == deathSoundId)
                {
                    sprite->deathSound = key.first;
                    break;
                }
            }
        }
        sprite->endurance   = endurance;
        sprite->playOnce    = playOnce;
        sprite->deleteOnLastFrame = deleteOnLastFrame;
        sprite->scaleX      = scaleX / 100.0;
        sprite->scaleY      = scaleY / 100.0;
        sprite->solidX      = solidX / 100.0;
        sprite->solidY      = solidY / 100.0;
        sprite->script.loadFromFile("sprite.js");

        if (framesCount == 0 || !g_SpritesNeedForLoad[i])
            continue;

        // читаем кадры, если есть
        sprite->loadFrames();
    }
    isLoaded = true;

end:
    free(buffer);
    Res_CloseIni();

    return isLoaded;
}
