#include <sstream>
#include <string>
#include <cstring>

#define _NO_SOUND_

#include "WorldSim3D.h"
#include "SampleFunctions.h"

#include "global.h"
#include "js_util.h"
#include "file_system.h"
#include "audio_system.h"
#include "audio_listener.h"
#include "audio_buffer.h"
#include "audio_source.h"
#include "sound.h"
/*#include "base_object.h"
#include "object2d.h"
#include "object3d.h"
*/
#include "player.h"
#include "maps.h"
#include "textures.h"
#include "sprites.h"
#include "doors.h"
#include "keys.h"
#include "bullets.h"
#include "weapons.h"
#include "weapon_item.h"
#include "ammo.h"

using namespace std;
/*
AudioBuffer *snd  = nullptr,
            *snd2 = nullptr,
            *snd3 = nullptr,
            *snd4 = nullptr;
AudioSource *src  = nullptr,
            *src2 = nullptr,
            *src3 = nullptr,
            *src4 = nullptr;
*/
TScriptJS levelUpdateCommand = {};

void StartInterpreter()
{
    JS_InitVM();

    string className;
    string objectName;

    TScriptJS tmp;
    // load global values
    tmp.loadFromFile("global.js");
    tmp.run();

    // load key codes
    tmp.loadFromFile("keycodes.js");
    tmp.run();

    // load vectors
    tmp.loadFromFile("vectors.js");
    tmp.run();
    //tmp.clear();

    // level's scripts
    g_ObjectId = 0;
    tmp.loadFromFile("level.js");

    className  = "LevelClass";
    objectName = "Level";
    // заворачиваем код в вид:
    // function Object0(){ текст_скрипта }
    // var object0 = new Object0();
    tmp.text = "function " + className + "(){" + tmp.text + "} ";
    tmp.text += "var " + objectName + " = new " + className + "();";
    //compile();
    tmp.run();

    // запоминаем команды для инициализации и обновления объекта
    tmp.text = objectName + ".onInit();";
    //compile();
    tmp.run();

    levelUpdateCommand.fileName = objectName + " update command";
    levelUpdateCommand.text = objectName + ".onUpdate();";
    levelUpdateCommand.compile();

    // player's scripts
    if (g_PlayerResource.name == "")
    {
        g_PlayerResource.name = "player";
        g_PlayerResource.script.loadFromFile("player.js");
    }
    else
        g_PlayerResource.reloadScript();

    // camera's scripts
    if (g_CameraResource.name == "")
    {
        g_CameraResource.name = "camera";
        g_CameraResource.script.loadFromFile("camera.js");
    }
    else
        g_CameraResource.reloadScript();

    // object scripts
    /* перегружаем скрипты, если класс создан */
    for (auto& resInMem : ScrResourceInMem::scrResourcesInMem)
    {
        resInMem->reloadScript();
    }
    /* заново инициализируем объекты */
    for (auto& object : TObject::objects)
    {
        object->init();
    }
}

void LoadLevel(int num, bool firstRun = false)
{
    if (firstRun)
        LoadMapPack();

    InitImages2D();
    PrepareTexturesNeedForLoad(num);
    PrepareSpritesNeedForLoad(num);
    PrepareKeysNeedForLoad(num);
    PrepareDoorsNeedForLoad(num);
    PrepareWeaponsNeedForLoad(num);
    PrepareAmmoNeedForLoad(num);

    LoadSounds();
    LoadTexturesPack();
    LoadSpritesPack();
    LoadKeysPack();
    LoadDoorsPack();
    LoadBulletsPack();
    LoadAmmoTypes();
    LoadWeaponItemsPack();
    LoadWeaponsPack();
    LoadAmmoPack();

    GenerateLevel(num);

    /* создаем игрока */
    g_Player = new TPlayer(3.0 * BLOCK_SIZE, 0.1 * BLOCK_SIZE, -3.0 * BLOCK_SIZE);
    g_Camera = new TCamera(3.0, 0.0, -3.0);

/*
    if (snd) delete(snd);
    if (src) delete(src);
    snd = new AudioBuffer("sound_9.dat", false);
    src = new AudioSource(snd);
    src->setPosition ( 4.5, 0.5, - 4.5);
    src->play(true);

	
    if (snd2) delete(snd2);
    if (src2) delete(src2);
    snd2 = new AudioBuffer("sound_1.dat", true);
    src2 = new AudioSource(snd2);
    src2->setPosition(14.5, 0.5, - 4.5);
    src2->play(true);

    if (snd3) delete(snd3);
    if (src3) delete(src3);
    snd3 = new AudioBuffer("sound_0.dat", true);
    src3 = new AudioSource(snd3);
    src3->setPosition(14.5, 0.5, -14.5);
    src3->play(true);

    if (snd4) delete(snd4);
    if (src4) delete(src4);
    snd4 = new AudioBuffer("sound_10.dat", false);
    src4 = new AudioSource(snd4);
    src4->setPosition( 4.5, 0.5, -14.5);
    src4->play(true);
*/

//    alDistanceModel(AL_INVERSE_DISTANCE_CLAMPED);
//    alListenerf(AL_GAIN, 1.0);
}

wVector3f prevPos = {};

int main(int argc, char** args)
{
#ifdef UNIT_TESTS
    unitTestObjectUserVars();
#endif // UNIT_TESTS

    int prevFPS, fps;
    wVector3f camPos;
    wstring fpsString;
    wstring wndCaption = L"Raycasting Game Constructor Runner";

    // перебираем параметры
    for (int i = 0; i < argc; ++i)
    {
        // установка рендера
        if (strcmp("-ogl",   args[i]) == 0) { g_Renderer = wDriverTypes::wDRT_OPENGL;    }
        if (strcmp("-d3d",   args[i]) == 0) { g_Renderer = wDriverTypes::wDRT_DIRECT3D9; }
        //if (strcmp("-soft",  args[i]) == 0) { g_Renderer = wDriverTypes::wDRT_SOFTWARE;       g_LimitFps = false; }
        if (strcmp("-soft2", args[i]) == 0) { g_Renderer = wDriverTypes::wDRT_BURNINGS_VIDEO; g_LimitFps = false; }
        if (strcmp("-vsync", args[i]) == 0) { g_VSync    = true; }
        if (strcmp("-fullscreen",   args[i]) == 0) { g_FullScreenMode = true; }
        if (strcmp("-no_limit_fps", args[i]) == 0) { g_LimitFps = false; }

        if (strcmp("-w", args[i]) == 0 && (i + 1 < argc))
        {
            int w = atoi(args[i + 1]);
            g_Resolution.x = w;
        }

        if (strcmp("-h", args[i]) == 0 && (i + 1 < argc))
        {
            int h = std::atoi(args[i + 1]);
            g_Resolution.y = h;
        }
    }

    // Start engine
#ifdef __WORLDSIM3D_RUNTIME_LOAD
    wLibraryLoad();
#endif  // __WS3D_DYNAMIC_LOAD
    bool init = wEngineStart(g_Renderer, g_Resolution, 32, g_FullScreenMode, false, true, g_VSync) != 0;
    if (!init)
    {
        PrintWithColor("wEngineStart() failed!", wCFC_RED, true);
        return -1;
    }

    wTexture* crosshair = wTextureLoad("assets/crosshair.png");
    wFont* font = wFontLoad("assets/4.png");

    /* запомним путь приложения, откуда оно было запущено */
    g_RunPath = args[0];

    InitRunner();
    JS_InitVM();

    LoadLevel(0, true);

    StartInterpreter();

    g_DeltaTime = 0;
    g_DeltaTimeScript = 0;
    g_TimeForRunScript = 0;
    g_TimeForGC = 0.0;

    while (wEngineRunning())
    {
        //wSceneBeginAdvanced(wCOLOR4s_BLACK, 0, 1);
        wSceneBegin(wCOLOR4s_BLACK);

        g_DeltaTime = wTimerGetDelta();
        g_TimeForRunScript += g_DeltaTime;
        g_TimeForGC += g_DeltaTime;

        g_DeltaTimeScript = 0;
        if (g_TimeForRunScript >= SCRIPTS_DELAY)
        {
            g_DeltaTimeScript = g_TimeForRunScript;
            g_TimeForRunScript = 0;
        }

        /* запускаем скрипт уровня */
        g_ObjectId = 0;
        levelUpdateCommand.run();

        /* обновляем состояние уровня */
        TextureNodesUpdate();
        for (auto object : TObject3D::objects)
        {
            if (object != nullptr)
                object->update();
        }

        wSceneDrawAll();

        /* лазеры */
        TLaser::show();

        camPos = wNodeGetPosition(g_Camera->node);
        wstring strBuffer = L"Camera Pos : " + to_wstring(camPos.x) + L" " + to_wstring(-camPos.z) + L"\0";
        if (font)
        {
            wFontDraw(font, strBuffer.c_str(), {10, 10}, {100, 20}, wCOLOR4s_WHITE);
            wFontDraw(font, fpsString.c_str(), {10, 25}, {100, 20}, wCOLOR4s_WHITE);
        }
        if (crosshair)
            wTextureDrawEx(crosshair, {(g_Resolution.x >> 1) - 16, (g_Resolution.y >> 1) - 16}, {0.5, 0.5}, true);

        wEngineSet2dMaterial(true);
        /*for (auto object : TObject2D::objects)
        {
            object->update();
        }*/
        WeaponUpdate();
        wEngineSet2dMaterial(false);

        wSceneEnd();
//        src->setPosition(5.0, 0.0, -5.0);
//        src2->setPosition(15.0, 0.0, -5.0);

        wVector3f target, right, up;
        wCameraGetOrientation(g_Camera->node, &up, &target, &right);
        AudioListener::SetPosition((float*)&camPos);
        AudioListener::SetOrientation(target.x, target.y, target.z, -up.x, -up.y, -up.z);

        SoundNodesUpdate();

		AudioSystem::Update();

        // инициализируем объекты из таблицы ининициализации
        InitObjects();
        // ищем и удаляем объекты, помеченные на удаление
        DeleteObjects();

        fps = wEngineGetFPS();
        if (fps != prevFPS)
        {
            prevFPS = fps;
            wstring string = wndCaption + L" FPS : " + to_wstring(fps);
            fpsString = L"FPS : " + to_wstring(prevFPS) + L"\0";
            wWindowSetCaption(string.c_str());
        }

        wEngineCloseByEsc();

        if (wInputIsKeyHit(wKC_F10))
            wSystemSaveScreenShot("screenshot.jpg");

        // перезапуск виртуальной машины
        if (wInputIsKeyUp(wKC_KEY_T))
        {
            JS_StopVM();
            StartInterpreter();
        }

        // рестарт уровня
        if (wInputIsKeyUp(wKC_KEY_R))
        {
            LoadLevel(0);
            JS_StopVM();
            StartInterpreter();
        }

        // собираем мусор
        if (g_TimeForGC >= 5.0)
        {
            g_TimeForGC = 0.0;
            JS_GC();
        }

        prevPos = camPos;
    }

    StopRunner();
    JS_StopVM();

    return 0;
}
