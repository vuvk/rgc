#ifdef __cplusplus
#include <cstdint>
#else
#include <stdbool.h>
#include <stdint.h>
#endif

#include <cstdio>
#include "runtime_loader.h"

#if defined(_WIN32) || defined(WIN32) ||  defined(_WIN64) || defined(WIN64)
#define RES_LIBRARY_NAME "res_manager.dll"
#else
	#define RES_LIBRARY_NAME "./res_manager.so"
#endif

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

int32_t (*Res_OpenZipForRead)(const char* zipName);
int32_t (*Res_GetCompressedFileSize)(const char* fileName);
int32_t (*Res_GetUncompressedFileSize)(const char* fileName);
int32_t (*Res_ReadFileFromZipToBuffer)(const char* fileName, void* buffer, int32_t bufferSize);
bool    (*Res_CloseZipAfterRead)();
bool    (*Res_OpenZipForWrite)(const char* zipName, bool isAppend);
int32_t (*Res_WriteFileFromBufferToZip)(const char* fileName, const void* buffer, uint32_t bufferSize, int32_t compressionLevel);
bool    (*Res_CloseZipAfterWrite)();

bool    (*Res_PackAllFilesFromDirToZip)(const char* path, int32_t compressionLevel);
bool    (*Res_UnpackAllFilesFromZipToDir)(const char* zipName, const char* path, bool rewrite);
bool    (*Res_ReadFileToBuffer)(void** buffer, int32_t* bufferSize, const char* fileName);
bool    (*Res_WriteBufferToFile)(void* buffer, int32_t bufferSize, const char* fileName, bool rewrite);
void    (*Res_DeleteAllFilesInDir)(const char* dirName, bool deleteDir);

bool    (*Res_OpenIniFromFile)(const char* fileName);
bool    (*Res_OpenIniFromBuffer)(const void* buffer, uint32_t bufferSize);
char*   (*Res_IniReadString)(const char* section, const char* key, const char* defaultValue);
int32_t (*Res_IniReadInteger)(const char* section, const char* key, int32_t defaultValue);
int64_t (*Res_IniReadInteger64)(const char* section, const char* key, int64_t defaultValue);
bool    (*Res_IniReadBool)(const char* section, const char* key, bool defaultValue);
double  (*Res_IniReadDouble)(const char* section, const char* key, double defaultValue);
void    (*Res_IniWriteString)(const char* section, const char* key, const char* value);
void    (*Res_IniWriteInteger)(const char* section, const char* key, int32_t value);
void    (*Res_IniWriteInteger64)(const char* section, const char* key, int64_t value);
void    (*Res_IniWriteBool)(const char* section, const char* key, bool value);
void    (*Res_IniWriteDouble)(const char* section, const char* key, double value);
bool    (*Res_SaveIniToBuffer)(void** buffer, int32_t* bufferSize);
bool    (*Res_SaveIniToFile)(const char* fileName);
void    (*Res_CloseIni)();


static void* resHandle = NULL;

void ResManagerLoadLibrary()
{
    resHandle = load_library(RES_LIBRARY_NAME);
    if (!resHandle)
    {
        printf("Error load '%s'!\n", RES_LIBRARY_NAME);
        return;
    }

    *(void**) (&Res_OpenZipForRead) = load_function(resHandle, "Res_OpenZipForRead");
    *(void**) (&Res_GetCompressedFileSize) = load_function(resHandle, "Res_GetCompressedFileSize");
    *(void**) (&Res_GetUncompressedFileSize) = load_function(resHandle, "Res_GetUncompressedFileSize");
    *(void**) (&Res_ReadFileFromZipToBuffer) = load_function(resHandle, "Res_ReadFileFromZipToBuffer");
    *(void**) (&Res_CloseZipAfterRead) = load_function(resHandle, "Res_CloseZipAfterRead");
    *(void**) (&Res_OpenZipForWrite) = load_function(resHandle, "Res_OpenZipForWrite");
    *(void**) (&Res_WriteFileFromBufferToZip) = load_function(resHandle, "Res_WriteFileFromBufferToZip");
    *(void**) (&Res_CloseZipAfterWrite) = load_function(resHandle, "Res_CloseZipAfterWrite");

    *(void**) (&Res_PackAllFilesFromDirToZip) = load_function(resHandle, "Res_PackAllFilesFromDirToZip");
    *(void**) (&Res_UnpackAllFilesFromZipToDir) = load_function(resHandle, "Res_UnpackAllFilesFromZipToDir");
    *(void**) (&Res_ReadFileToBuffer) = load_function(resHandle, "Res_ReadFileToBuffer");
    *(void**) (&Res_WriteBufferToFile) = load_function(resHandle, "Res_WriteBufferToFile");
    *(void**) (&Res_DeleteAllFilesInDir) = load_function(resHandle, "Res_DeleteAllFilesInDir");

    *(void**) (&Res_OpenIniFromFile) = load_function(resHandle, "Res_OpenIniFromFile");
    *(void**) (&Res_OpenIniFromBuffer) = load_function(resHandle, "Res_OpenIniFromBuffer");
    *(void**) (&Res_IniReadString) = load_function(resHandle, "Res_IniReadString");
    *(void**) (&Res_IniReadInteger) = load_function(resHandle, "Res_IniReadInteger");
    *(void**) (&Res_IniReadInteger64) = load_function(resHandle, "Res_IniReadInteger64");
    *(void**) (&Res_IniReadBool) = load_function(resHandle, "Res_IniReadBool");
    *(void**) (&Res_IniReadDouble) = load_function(resHandle, "Res_IniReadDouble");
    *(void**) (&Res_IniWriteString) = load_function(resHandle, "Res_IniWriteString");
    *(void**) (&Res_IniWriteInteger) = load_function(resHandle, "Res_IniWriteInteger");
    *(void**) (&Res_IniWriteInteger64) = load_function(resHandle, "Res_IniWriteInteger64");
    *(void**) (&Res_IniWriteBool) = load_function(resHandle, "Res_IniWriteBool");
    *(void**) (&Res_IniWriteDouble) = load_function(resHandle, "Res_IniWriteDouble");
    *(void**) (&Res_SaveIniToBuffer) = load_function(resHandle, "Res_SaveIniToBuffer");
    *(void**) (&Res_SaveIniToFile) = load_function(resHandle, "Res_SaveIniToFile");
    *(void**) (&Res_CloseIni) = load_function(resHandle, "Res_CloseIni");
}

void ResManagerUnloadLibrary()
{
    unload_library(resHandle);
    resHandle = NULL;

    Res_OpenZipForRead = NULL;
    Res_GetCompressedFileSize = NULL;
    Res_GetUncompressedFileSize = NULL;
    Res_ReadFileFromZipToBuffer = NULL;
    Res_CloseZipAfterRead = NULL;
    Res_OpenZipForWrite = NULL;
    Res_WriteFileFromBufferToZip = NULL;
    Res_CloseZipAfterWrite = NULL;

    Res_PackAllFilesFromDirToZip = NULL;
    Res_UnpackAllFilesFromZipToDir = NULL;
    Res_ReadFileToBuffer = NULL;
    Res_WriteBufferToFile = NULL;
    Res_DeleteAllFilesInDir = NULL;

    Res_OpenIniFromFile = NULL;
    Res_OpenIniFromBuffer = NULL;
    Res_IniReadString = NULL;
    Res_IniReadInteger = NULL;
    Res_IniReadInteger64 = NULL;
    Res_IniReadBool = NULL;
    Res_IniReadDouble = NULL;
    Res_IniWriteString = NULL;
    Res_IniWriteInteger = NULL;
    Res_IniWriteInteger64 = NULL;
    Res_IniWriteBool = NULL;
    Res_IniWriteDouble = NULL;
    Res_SaveIniToBuffer = NULL;
    Res_SaveIniToFile = NULL;
    Res_CloseIni = NULL;
}

#ifdef __cplusplus
}
#endif // __cplusplus