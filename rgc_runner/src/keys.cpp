#include "keys.h"
#include "res_manager.h"
#include "file_system.h"
#include "sound.h"

using namespace FileSystem;

vector<TKeyInMem> g_Keys;
int TKeyNode::count = 0;

/*
 * TKeyNode
 * */
TKeyNode::TKeyNode (TKeyInMem* index) : TBillboard(index)
{
    addVar("type", "key");                      // имя типа объекта
    if (index)
    {
        addVar("pickUpSound", index->pickUpSound);  // имя звука подбора
    }
    ++count;
}

// IMPLEMENTATION

void InitKeys()
{
    if (g_Keys.capacity() != KEYS_MAX_COUNT)
        g_Keys.reserve(KEYS_MAX_COUNT);

    if (g_Keys.size() != 0)
        g_Keys.resize(0);
/*
    for (auto& key : g_Keys)
    {
        key.clear();
    }
*/
}

bool LoadKeysPack()
{
    bool    isLoaded = false;
    TKeyInMem* key = nullptr;

    char*    buffer = nullptr;
    uint64_t bufferSize;
    string   fileName;

    int     i;
    int     pickUpSoundId;

    string   name;
    double   solidX, solidY;
    uint8_t  animSpeed;
    uint32_t framesCount;

    // читаем количество текстур
    File::readFull("keys.cfg", &buffer, &bufferSize);
    if ((buffer == nullptr) ||
        (bufferSize == 0) ||
        !Res_OpenIniFromBuffer(buffer, bufferSize))
    {
        PrintWithColor("can not open keys.cfg!!!\n");
        goto end;
    }
    g_KeysCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_KeysCount == 0)
        goto end;

    InitKeys();

    for (i = 0; i < g_KeysCount; ++i)
    {
        // читаем инфу по текстуре
        fileName = "key_" + to_string(i) + ".cfg\0";
        File::readFull(fileName, &buffer, &bufferSize);
        if (bufferSize <= 0)
        {
            PrintWithColor((char*)("can not get size of key_" + to_string(i) + ".cfg").c_str());
            continue;
        }

        if (i >= g_Keys.size())
            g_Keys.resize(g_Keys.size() + 1);
        key = &(g_Keys[i]);

        Res_OpenIniFromBuffer(buffer, bufferSize);

        name        = Res_IniReadString("Options", "name", ("Key #" + to_string(i) + "\0").c_str());
        pickUpSoundId = Res_IniReadInteger("Options", "pickup_sound_id", -1);
        solidX      = Res_IniReadDouble("Options", "solid_x", 100.0);
        solidY      = Res_IniReadDouble("Options", "solid_y", 100.0);
        animSpeed   = (uint8_t)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = (uint8_t)Res_IniReadInteger("Options", "frames_count",    0);
        Res_CloseIni();

        key->name      = name.c_str();
        key->posInRep  = i;
        key->solidX    = solidX / 100.0;
        key->solidY    = solidY / 100.0;
        key->animSpeed = animSpeed;
        key->script.loadFromFile("key.js");
        /* ищем имя звука по id */
        if (pickUpSoundId != -1 && pickUpSoundId < g_SoundBuffers.size())
            for (auto snd : g_SoundBuffers)
                if (snd.second->id == pickUpSoundId)
                {
                    key->pickUpSound = snd.first;
                    break;
                }

        if (framesCount == 0 || !g_KeysNeedForLoad[i])
            continue;

        // читаем кадры, если есть
        key->loadFrames();
    }
    isLoaded = true;

end:
    free(buffer);
    Res_CloseIni();

    return isLoaded;
}
