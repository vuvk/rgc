#include "doors.h"
#include "keys.h"
#include "res_manager.h"
#include "file_system.h"
#include "sound.h"

using namespace FileSystem;

vector<TDoorInMem> g_Doors;
int TDoorNode::count = 0;

/*
 * TDoorNode
 * */
TDoorNode::TDoorNode(TDoorInMem* index, bool isHorizontal, bool isVertical) : TObject3D(index)
{
    addVar("type",        "door"  );             // имя типа объекта
    addVar("openSpeed",   index->openSpeed);     // скорость открывания
    addVar("stayOpened",  index->stayOpened);    // открывается один раз?
    addVar("needKey",     index->needKey);       // нужен ключ для открывания?
    addVar("message",     index->needKeyMsg);    // сообщение о необходимости ключа
    addVar("keyName",     index->keyName);       // какой ключ нужен для открывания
    addVar("startSound",  index->startSound);    // какой звук для начала движения
    addVar("moveSound",   index->moveSound);     // какой звук для открывания
    addVar("stopSound",   index->stopSound);     // какой звук для останова

    wMesh* mesh = CreateDoorMesh("door mesh " + to_string(count),
                                 index->frames[0],
                                 index->rib,
                                 index->width,
                                 isHorizontal, isVertical);
    if (mesh != nullptr)
    {
        this->node = wNodeCreateFromMesh(mesh);
        this->physBody = wCollisionCreateFromMesh(mesh, node, 0);
        this->enableCollision();
    }

    ++count;
}


// IMPLEMENTATION

void InitDoors()
{
    if (g_Doors.capacity() != DOORS_MAX_COUNT)
        g_Doors.reserve(DOORS_MAX_COUNT);

    if (g_Doors.size() != 0)
        g_Doors.resize(0);
/*
    for (auto& door : g_Doors)
    {
        door.clear();
    }
*/
}


bool LoadDoorsPack()
{
    bool    isLoaded = false;

    char*    buffer = nullptr;
    uint64_t bufferSize;
    string   fileName;

    string  doorName;
    int     i;

    string   name;
    double   openSpeed;
    bool     stayOpened;
    bool     needKey;
    string   needKeyMsg;
    int16_t  keyId,
             startSoundId,
             moveSoundId,
             stopSoundId;
    double   width;
    uint8_t  animSpeed;
    uint32_t framesCount;

    // читаем количество текстур
    File::readFull("doors.cfg", &buffer, &bufferSize);
    if ((buffer == nullptr) ||
        (bufferSize == 0) ||
        !Res_OpenIniFromBuffer(buffer, bufferSize))
    {
        PrintWithColor("can not open doors.cfg!!!\n");
        goto end;
    }
    g_DoorsCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_DoorsCount == 0)
        goto end;

    InitDoors();

    for (i = 0; i < g_DoorsCount; ++i)
    {
        // читаем инфу по текстуре
        fileName = "door_" + to_string(i) + ".cfg\0";
        File::readFull(fileName, &buffer, &bufferSize);
        if (bufferSize <= 0)
        {
            PrintWithColor((char*)("can not get size of door_" + to_string(i) + ".cfg").c_str());
            continue;
        }

        if (i >= g_Doors.size())
            g_Doors.resize(g_Doors.size() + 1);
        TDoorInMem& door = g_Doors[i];

        Res_OpenIniFromBuffer(buffer, bufferSize);

        name        = Res_IniReadString("Options", "name", ("Door #" + to_string(i) + "\0").c_str());
        openSpeed   = Res_IniReadDouble("Options", "open_speed", 1.0);
        stayOpened  = Res_IniReadBool  ("Options", "stay_opened",  false);
        needKey     = Res_IniReadBool  ("Options", "need_key",     false);
        needKeyMsg  = Res_IniReadString("Options", "message", "I need a key.\0");
        keyId       = Res_IniReadInteger("Options", "key_id",         -1);
        startSoundId= Res_IniReadInteger("Options", "start_sound_id", -1);
        moveSoundId = Res_IniReadInteger("Options", "move_sound_id",  -1);
        stopSoundId = Res_IniReadInteger("Options", "stop_sound_id",  -1);
        width       = Res_IniReadDouble("Options", "width", 1.0) / 100.0;
        animSpeed   = (uint8_t)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = (uint8_t)Res_IniReadInteger("Options", "frames_count",    0);
        Res_CloseIni();

        door.name        = (char*)name.c_str();
        door.posInRep    = i;
        door.openSpeed   = openSpeed;
        door.stayOpened  = stayOpened;
        door.needKey     = needKey;
        door.needKeyMsg  = (char*)needKeyMsg.c_str();
        door.width       = width;
        door.animSpeed   = animSpeed;
        door.script.loadFromFile("door.js");
        /* ищем имя звука по id */
        if (startSoundId != -1 && startSoundId < g_SoundBuffers.size())
            for (auto key : g_SoundBuffers)
                if (key.second->id == startSoundId)
                {
                    door.startSound = key.first;
                    break;
                }
        if (moveSoundId != -1 && moveSoundId < g_SoundBuffers.size())
            for (auto key : g_SoundBuffers)
                if (key.second->id == moveSoundId)
                {
                    door.moveSound = key.first;
                    break;
                }
        if (stopSoundId != -1 && stopSoundId < g_SoundBuffers.size())
            for (auto key : g_SoundBuffers)
                if (key.second->id == stopSoundId)
                {
                    door.stopSound = key.first;
                    break;
                }

        // подтягиваем имя необходимого ключа
        if (keyId >= 0 && keyId < g_KeysCount && keyId < g_Keys.size())
            door.keyName = g_Keys[keyId].name.c_str();

        if (framesCount == 0 || !g_DoorsNeedForLoad[i])
            continue;

        fileName = "door_" + to_string(i) + "_rib.dat\0";
        doorName = "Door_" + to_string(i) + "_Rib";
        door.rib = LoadTexture(fileName, doorName);

        // читаем кадры, если есть
        door.loadFrames();
    }
    isLoaded = true;

end:
    free(buffer);
    Res_CloseIni();

    return isLoaded;
}


/* СОЗДАНИЕ МЕША ДВЕРИ */
wMesh* CreateDoorMesh(const string name, wTexture* texture, wTexture* ribTexture, double width, bool isHorizontal, bool isVertical)
{
    if (!isHorizontal && !isVertical)
        return nullptr;

    wVert    verts[8];
    uint16_t indices[] = { 0, 1, 2, 0, 2, 3,
                           4, 5, 6, 4, 6, 7 };

    wVert    ribVerts[16];
    uint16_t ribIndices[] = {  0,  1,  2,  0,  2,  3,
                               4,  5,  6,  4,  6,  7,
                               8,  9, 10,  8, 10, 11,
                              12, 13, 14, 12, 14, 15 };

    float lt =  0.0 * BLOCK_SIZE;
    float rt =  1.0 * BLOCK_SIZE;
    float dn =  0.0 * BLOCK_SIZE;
    float up =  1.0 * BLOCK_SIZE;
    float bk =  0.0 * BLOCK_SIZE;
    float fw = -1.0 * BLOCK_SIZE;

    double hW = width / 2.0 * BLOCK_SIZE;
    if (isHorizontal)
    {
        int i = 0;
        // ребра
        // right
        ribVerts[i++] = { { rt, dn, hW }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, { 1.0, 1.0 } };
        ribVerts[i++] = { { rt, dn,-hW }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, { 0.0, 1.0 } };
        ribVerts[i++] = { { rt, up,-hW }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, { 0.0, 0.0 } };
        ribVerts[i++] = { { rt, up, hW }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, { 1.0, 0.0 } };
        // left
        ribVerts[i++] = { { lt, dn,-hW }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 1.0, 1.0 } };
        ribVerts[i++] = { { lt, dn, hW }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 0.0, 1.0 } };
        ribVerts[i++] = { { lt, up, hW }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 0.0, 0.0 } };
        ribVerts[i++] = { { lt, up,-hW }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 1.0, 0.0 } };
        // top
        ribVerts[i++] = { { lt, up,-hW }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 0.0, 0.0 } };
        ribVerts[i++] = { { lt, up, hW }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 1.0, 0.0 } };
        ribVerts[i++] = { { rt, up, hW }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 1.0, 1.0 } };
        ribVerts[i++] = { { rt, up,-hW }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 0.0, 1.0 } };
        // bottom
        ribVerts[i++] = { { lt, dn,-hW }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 0.0, 1.0 } };
        ribVerts[i++] = { { rt, dn,-hW }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 0.0, 0.0 } };
        ribVerts[i++] = { { rt, dn, hW }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 1.0, 0.0 } };
        ribVerts[i++] = { { lt, dn, hW }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 1.0, 1.0 } };

        // сама дверь
        i = 0;
        // forward
        verts[i++] = { { rt, dn,-hW }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, { 0.0, 1.0 } };
        verts[i++] = { { lt, dn,-hW }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, { 1.0, 1.0 } };
        verts[i++] = { { lt, up,-hW }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, { 1.0, 0.0 } };
        verts[i++] = { { rt, up,-hW }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, { 0.0, 0.0 } };
        // backward
        verts[i++] = { { lt, dn, hW }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { 1.0, 1.0 } };
        verts[i++] = { { rt, dn, hW }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { 0.0, 1.0 } };
        verts[i++] = { { rt, up, hW }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { 0.0, 0.0 } };
        verts[i++] = { { lt, up, hW }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { 1.0, 0.0 } };
    }
    else
    // может быть она вертикальная?
    {
        int i = 0;
        // ребра
        // forward
        ribVerts[i++] = { { hW, dn, fw }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, { 1.0, 1.0 } };
        ribVerts[i++] = { {-hW, dn, fw }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, { 0.0, 1.0 } };
        ribVerts[i++] = { {-hW, up, fw }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, { 0.0, 0.0 } };
        ribVerts[i++] = { { hW, up, fw }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, { 1.0, 0.0 } };
        // backward
        ribVerts[i++] = { {-hW, dn, bk }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { 1.0, 1.0 } };
        ribVerts[i++] = { { hW, dn, bk }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { 0.0, 1.0 } };
        ribVerts[i++] = { { hW, up, bk }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { 0.0, 0.0 } };
        ribVerts[i++] = { {-hW, up, bk }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { 1.0, 0.0 } };
        // top
        ribVerts[i++] = { {-hW, up, fw }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 1.0, 0.0 } };
        ribVerts[i++] = { {-hW, up, bk }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 1.0, 1.0 } };
        ribVerts[i++] = { { hW, up, bk }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 0.0, 1.0 } };
        ribVerts[i++] = { { hW, up, fw }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 0.0, 0.0 } };
        // bottom
        ribVerts[i++] = { {-hW, dn, fw }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 0.0, 0.0 } };
        ribVerts[i++] = { { hW, dn, fw }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 1.0, 0.0 } };
        ribVerts[i++] = { { hW, dn, bk }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 1.0, 1.0 } };
        ribVerts[i++] = { {-hW, dn, bk }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 0.0, 1.0 } };

        // сама дверь
        i = 0;
        // right
        verts[i++] = { { hW, dn, bk }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, { 1.0, 1.0 } };
        verts[i++] = { { hW, dn, fw }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, { 0.0, 1.0 } };
        verts[i++] = { { hW, up, fw }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, { 0.0, 0.0 } };
        verts[i++] = { { hW, up, bk }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, { 1.0, 0.0 } };
        // left
        verts[i++] = { {-hW, dn, fw }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 0.0, 1.0 } };
        verts[i++] = { {-hW, dn, bk }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 1.0, 1.0 } };
        verts[i++] = { {-hW, up, bk }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 1.0, 0.0 } };
        verts[i++] = { {-hW, up, fw }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, { 0.0, 0.0 } };
    }

    wMesh* mesh = wMeshCreate((char*)name.c_str());
    wMeshBuffer* meshBuffer;
    wMaterial* material;

    // 1 мешбуффер - дверь
    meshBuffer = wMeshBufferCreate(8, verts, 12, indices);
    material = wMeshBufferGetMaterial(meshBuffer);
    MaterialSetOldSchool(material);
    //wMaterialSetFlag(material, wMaterialFlags.wMF_BACK_FACE_CULLING, false);
    wMaterialSetType(material, wMT_TRANSPARENT_ALPHA_CHANNEL);
    wMaterialSetTexture(material, 0, texture);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    // 2 мешбуффер - ограничивающая коробка
    meshBuffer = wMeshBufferCreate(16, ribVerts, 24, ribIndices);
    material = wMeshBufferGetMaterial(meshBuffer);
    MaterialSetOldSchool(material);
    wMaterialSetTexture(material, 0, ribTexture);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    wMeshEnableHardwareAcceleration(mesh, 0);

    return mesh;
}
