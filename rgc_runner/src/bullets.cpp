#include "bullets.h"
#include "file_system.h"
#include "res_manager.h"

using namespace FileSystem;

vector<TBulletInMem> g_Bullets;

TBulletNode::TBulletNode(TBulletInMem *index) : TBillboard(index, true)
{
    addVar("type",          "bullet"  );                // имя типа
    addVar("speed",         index->speed);              // скорость движения пули
    addVar("distance",      index->distance);           // макс. дистанция выстрела
    addVar("lifeTime",      index->lifeTime);           // время жизни снаряда
    addVar("lifeFrameLast", index->lifeFrameLast - 1);  // последний кадр анимации жизни (нумерация с 0)

    wBillboardSetEnabledAxis(this->billboard, {true, true, true});
}

void InitBullets()
{
    if (g_Bullets.capacity() != BULLETS_MAX_COUNT)
        g_Bullets.reserve(BULLETS_MAX_COUNT);

    if (g_Bullets.size() != 0)
        g_Bullets.resize(0);
/*
    for (auto& bullet : g_Bullets)
    {
        bullet.clear();
    }
*/
}

bool LoadBulletsPack()
{
    bool    isLoaded = false;
    //TBulletInMem* bullet = nullptr;

    char*    buffer = nullptr;
    uint64_t bufferSize;
    string   fileName;
    double   solidX,
             solidY;

    // читаем количество текстур
    File::readFull("bullets.cfg", &buffer, &bufferSize);
    if (bufferSize <= 0 ||
        buffer == nullptr ||
        !Res_OpenIniFromBuffer(buffer, bufferSize))
    {
        PrintWithColor("can not open bullets.cfg!!!\n");
        goto end;
    }
    g_BulletsCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_BulletsCount == 0)
        goto end;

    InitBullets();

    for (int i = 0; i < g_BulletsCount; ++i)
    {
        // читаем инфу по текстуре
        fileName = "bullet_" + to_string(i) + ".cfg";
        File::readFull(fileName, &buffer, &bufferSize);
        if (bufferSize <= 0)
        {
            PrintWithColor((char*)("can not get size of " + fileName).c_str());
            continue;
        }

        if (i >= g_Bullets.size())
            g_Bullets.resize(g_Bullets.size() + 1);
        TBulletInMem& bullet = g_Bullets.at(i);

        Res_OpenIniFromBuffer(buffer, bufferSize);

        bullet.name      = Res_IniReadString("Options", "name", (char*)("Bullet #" + to_string(i) + "\0").c_str());
        bullet.posInRep  = i;
        bullet.animSpeed = (uint8_t)Res_IniReadInteger("Options", "animation_speed", 0);
        //framesCount = Res_IniReadInteger("Options", "frames_count",    0);
        bullet.speed     = Res_IniReadDouble("Options", "speed",  1);
        bullet.distance  = Res_IniReadDouble("Options", "distance",  0);
        bullet.lifeTime  = Res_IniReadDouble("Options", "life_time", 0);
        bullet.lifeFrameLast = Res_IniReadInteger("Options", "life_frame_last", 0);
        solidX = Res_IniReadDouble("Options", "solid_x", 100.0);
        solidY = Res_IniReadDouble("Options", "solid_y", 100.0);
        bullet.solidX = solidX / 100.0;
        bullet.solidY = solidY / 100.0;

        bullet.script.loadFromFile("bullet.js");

        Res_CloseIni();
    }
    isLoaded = true;

end:
    Res_CloseIni();
    free(buffer);

    return isLoaded;
}
