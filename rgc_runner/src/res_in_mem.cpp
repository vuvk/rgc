#include "res_in_mem.h"
#include "file_system.h"

using namespace FileSystem;

vector<ScrResourceInMem*> ScrResourceInMem::scrResourcesInMem;


ResourceInMem::ResourceInMem()
{
    clear();
}
ResourceInMem::~ResourceInMem()
{
    clear();
}

bool ResourceInMem::isLoaded()
{
    if (frames.size() > 0)
    {
        for (wTexture* txr : frames)
        {
            if (txr != nullptr)
            {
                return true;
            }
        }
    }

    return false;
}

void ResourceInMem::clear()
{
    animSpeed = 0;
    solidX = solidY =
    scaleX = scaleY = 1.0;
    clearFrames();
}

void ResourceInMem::loadFrames()
{
    char*    buffer = nullptr;
    uint64_t bufferSize;
    string   fileName;
    string   textureName;
    string   objectName;
    uint32_t framesCount;

    clearFrames();

    switch (type)
    {
        case rtTexture:
            objectName = "texture";
            break;

        case rtSprite:
            objectName = "sprite";
            break;

        case rtDoor:
            objectName = "door";
            break;

        case rtKey:
            objectName = "key";
            break;

        case rtWeaponItem:
            objectName = "weapon";
            break;

        case rtWeaponInHand:
            objectName = "weapon_inhand";
            break;

        case rtBullet:
            objectName = "bullet";
            break;

        case rtAmmo:
            objectName = "ammo";
            break;

        default:
            return;
            break;
    }

    // читаем инфу по текстуре
    fileName = objectName + "_" + to_string(posInRep) + ".cfg\0";
    File::readFull(fileName, &buffer, &bufferSize);
    if (bufferSize <= 0)
    {
        PrintWithColor((char*)("can not get size of " + fileName).c_str());
        goto end;
    }

    Res_OpenIniFromBuffer(buffer, bufferSize);
    framesCount = Res_IniReadInteger("Options", "frames_count", 0);

    if (framesCount == 0)
        goto end;

    // читаем кадры, если есть
    for (int i = 0; i < framesCount; ++i)
    {
        fileName    = objectName + "_" + to_string(posInRep) + "_frame_" + to_string(i) + ".dat\0";
        textureName = objectName + "_" + to_string(posInRep) + "_Frame_" + to_string(i);
        frames.push_back(LoadTexture(fileName, textureName));
    }

end:
    Res_CloseIni();
    free(buffer);
}

void ResourceInMem::clearFrames()
{
    if (frames.size() == 0)
        return;

    for (wTexture* txr : frames)
    {
        if (txr)
            wTextureDestroy(txr);
    }
    frames.clear();
}



ScrResourceInMem::ScrResourceInMem() : ResourceInMem()
{
    scrResourcesInMem.push_back(this);
}
ScrResourceInMem::~ScrResourceInMem()
{
    clearScript();

    for (vector<ScrResourceInMem*>::iterator it = scrResourcesInMem.begin();
         it != scrResourcesInMem.end();
         ++it)
    {
        if (*it == this)
        {
            *it = scrResourcesInMem.back();
            scrResourcesInMem.pop_back();
            break;
        }
    }
}

/* создать скрипт класса */
void ScrResourceInMem::init(string className, bool numerate)
{
    TScriptJS classScript;  // скрипт, содержащий обернутый класс
    classScript.setId();

    // заворачиваем код в вид:
    // function Object0(){ текст_скрипта }
    if (className.size()  == 0 || className  == "")
        className  = "Object";
    if (numerate)
        className += to_string(classScript.id);

    classScript.fileName = className;
    classScript.text = "function " + className + "(){" + script.text + "\n}\n";
    classScript.run();

    this->className = className;

    isClassCreated = true;
}

void ScrResourceInMem::reloadScript()
{
    script.reload();
    className = "";
    isClassCreated = false;
}

void ScrResourceInMem::clearScript()
{
    script.clear();
    className = "";
    isClassCreated = false;
}

void ScrResourceInMem::clear()
{
    ResourceInMem::clear();
    clearScript();
}
