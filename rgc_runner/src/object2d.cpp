#include "object2d.h"


vector<TImage2D> g_Images2D;
vector<TObject2D*> TObject2D::objects;

/*
 * TObject2D
 * */
TObject2D::TObject2D(ScrResourceInMem* index) : TObject(index)
{
    if (!curTexture && index && index->frames.size() > 0)
        curTexture = index->frames.front();
/*
    for (vector<TObject2D*>::iterator it = TObject2D::objects.begin();
         it != TObject2D::objects.end();
         ++it)
    {
        if (*it == nullptr)
        {
            *it = this;
            return;
        }
    }
*/
    TObject2D::objects.push_back(this);
}

TObject2D::~TObject2D()
{
    /* удаляем ссылку на объект из списка всех 2д-объектов */
    if (TObject2D::objects.size() > 0)
    {
        for (vector<TObject2D*>::iterator it = TObject2D::objects.begin();
             it != TObject2D::objects.end();
             ++it)
        {
            if (*it == this)
            {
                TObject2D::objects.erase(it);
                break;
            }
        }
    }
}

void TObject2D::setFrame(int frame)
{
    if (index == nullptr || index->frames.size() == 0)
        return;

    if (frame < 0 || frame >= index->frames.size())
        return;

    curFrame = frame;
    curTexture = index->frames[curFrame];
}

inline int TObject2D::getFrame()
{
    return curFrame;
}

int TObject2D::getFramesCount()
{
    if (index == nullptr)
        return 0;

    return index->frames.size();
}

void TObject2D::getSize(vector<double>& vec)
{
    double x = 0, y = 0;

    if (curTexture != nullptr)
    {
        wVector2u size;
        uint32_t pitch;
        wColorFormat format;
        wTextureGetInformation(curTexture, &size, &pitch, &format);

        x = size.x;
        y = size.y;
    }

    vec = { x, y };
}

void TObject2D::draw()
{
    if (!isVisible || curTexture == nullptr)
        return;

    wVector2i pos = { position.x, position.y };

    if (rotation != 0.0)
    {
        wVector2u size;
        uint32_t pitch;
        wColorFormat format;
        wVector2i pivot = pos;

        wTextureGetInformation(curTexture, &size, &pitch, &format);
        pivot.x += int(size.x * scale.x) >> 1;
        pivot.y += int(size.y * scale.y) >> 1;

        wTextureDrawAdvanced(curTexture, pos, pivot, rotation, scale, true, wCOLOR4s_WHITE, wAAM_OFF, false, false, false);
    }
    else
        wTextureDrawEx(curTexture, pos, scale, true);
}


/*
 * IMPLEMENTATION
 * */
void InitImages2D()
{
    if (g_Images2D.capacity() == 0)
        g_Images2D.reserve(IMAGES_MAX_COUNT);
    g_Images2D.clear();
}
