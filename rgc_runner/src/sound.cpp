#include "sound.h"
#include "js_util.h"
#include "file_system.h"
#include "res_manager.h"

using namespace FileSystem;

std::map<std::string, SoundBuffer*> g_SoundBuffers;
//std::vector<SoundInMem> g_Sounds;
std::vector<SoundNode*> g_SoundNodes;


SoundNode::SoundNode(AudioBuffer* buffer) : AudioSource(buffer)
{
    this->id = (unsigned int)this;
    this->setRelative(true);
    g_SoundNodes.push_back(this);
}

SoundNode::~SoundNode()
{
    for (std::vector<SoundNode*>::iterator it = g_SoundNodes.begin();
         it != g_SoundNodes.end();
         ++it)
    {
        if (*it == this)
        {
            g_SoundNodes.erase(it);
            break;
        }
    }
}

void SoundNode::play(bool isLoop)
{
    if (isLoop)
        m_playOnce = false;
    AudioSource::play(isLoop);
}

void SoundNode::playOnce()
{
    m_playOnce = true;
    play(false);
}

/*
void SoundNode::update()
{
    if (isStopped() && m_playOnce)
        delete this;
}
*/

bool SoundNode::isPlayOnce()
{
    return this->m_playOnce;
}

unsigned int SoundNode::getId()
{
    return this->id;
}



void InitSounds()
{
    AudioSystem::DeleteAllSources();
    AudioSystem::DeleteAllBuffers();

    g_SoundBuffers.clear();
    g_SoundNodes.clear();
}

bool LoadSounds()
{
    bool isLoaded = false;

    SoundBuffer* snd = nullptr;

    char*    buffer = nullptr;
    uint64_t bufferSize = 0;
    string   fileName;

    int      i;
    int      sndCount;

    bool     precached;
    string   sndName;

    // читаем количество текстур
    File::readFull("sounds.cfg", &buffer, &bufferSize);
    if ((buffer == nullptr) ||
        (bufferSize <= 0) ||
        !Res_OpenIniFromBuffer(buffer, bufferSize))
    {
        PrintWithColor("can not open sounds.cfg!!!\n");
        goto end;
    }
    sndCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (sndCount == 0)
        goto end;

    InitSounds();

    for (i = 0; i < sndCount; ++i)
    {
        // читаем инфу по текстуре
        fileName = "sound_" + to_string(i) + ".cfg\0";
        File::readFull(fileName, &buffer, &bufferSize);
        if (bufferSize <= 0)
        {
            PrintWithColor((char*)("can not get size of " + fileName).c_str());
            continue;
        }

        Res_OpenIniFromBuffer(buffer, bufferSize);
        sndName   = Res_IniReadString("Options", "name", ("Sound #" + to_string(i)).c_str());
        precached = Res_IniReadBool  ("Options", "precached", false);
        Res_CloseIni();

        fileName = "sound_" + to_string(i) + ".dat\0";
        snd = new SoundBuffer(fileName, !precached);
        snd->id = i;
        std::cout << "SoundBuffer \"" << fileName << "\" with name \"" << sndName << "\" created!\n";

        /* добавляем звуко-буффер в словарь */
        auto key = g_SoundBuffers.find(sndName);
        if (key != g_SoundBuffers.end())
        {
            delete key->second;
            g_SoundBuffers.erase(key);
        }

        g_SoundBuffers.insert(make_pair(sndName, snd));
    }

    isLoaded = true;

end:
    free(buffer);
    Res_CloseIni();

    return isLoaded;
}

void SoundNodesUpdate()
{
    SoundNode* node;

    /* ищем источники, которые можо удалить */
    for (std::vector<SoundNode*>::iterator it = g_SoundNodes.begin();
         it != g_SoundNodes.end();
         )
    {
        if ((*it) &&
            (*it)->isStopped() &&
            (*it)->isPlayOnce())
        {
            node = *it;
            it = g_SoundNodes.erase(it);
            delete node;
        }        
        else
        {
            ++it;
        }
    }
}


/** JerryScript */
/** INTERPRETER */

/* для упрощения создания ноды звука - пытается найти буффер по имени и создает ноду */
SoundNode* SoundCreate(const std::string name)
{
    /* ищем буффер по имени */
    auto key = g_SoundBuffers.find(name);
    if (key == g_SoundBuffers.end())
        return nullptr;

    /* существует буффер? */
    AudioBuffer* buffer = key->second;
    if (!buffer)
        return nullptr;

    /* пытаемся создать ноду */
    return new SoundNode(buffer);
}

JS_FUNCTION(js_soundCreate)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = 0,
                  arg2 = 0,
                  arg3 = 0;
    float x = 0,
          y = 0,
          z = 0;
    if (args_cnt > 3)
    {
        arg1 = args_p[1];
        arg2 = args_p[2];
        arg3 = args_p[3];

        if (jerry_value_is_number(arg1))
            x = jerry_get_number_value(arg1);

        if (jerry_value_is_number(arg2))
            y = jerry_get_number_value(arg2);

        if (jerry_value_is_number(arg3))
            z = jerry_get_number_value(arg3);
    }

    if (!jerry_value_is_string(arg0))
        return jerry_create_number(0);
    std::string name = JS_GetStringValue(arg0);
    SoundNode* node = SoundCreate(name);
    if (!node)
        return jerry_create_number(0);

    /* передана позиция? */
    if (args_cnt > 3)
    {
        node->setRelative(false);
        node->setPosition(x, y, z);
    }

    return jerry_create_number(node->getId());
}

JS_FUNCTION(js_soundPlay)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    jerry_value_t arg1;
    arg1 = (args_cnt > 1) ?
            args_p[1]     :
            0;

    bool isLoop;
    isLoop = (jerry_value_is_boolean(arg1)) ?
              jerry_get_boolean_value(arg1)  :
              false;

    SoundNode* node = nullptr;
    /* передали имя для создания звука? */
    if (jerry_value_is_string(arg0))
    {
        std::string name = JS_GetStringValue(arg0);
        node = SoundCreate(name);
    }
    else /* или передали id? */
        if (jerry_value_is_number(arg0))
        {
            /* проверим нода ли это вообще */
            AudioSource* src = (AudioSource*)((int)jerry_get_number_value(arg0));
            node = dynamic_cast<SoundNode*>(src);
        }

    if (node)
    {
        node->play(isLoop);
        return jerry_create_number(node->getId());
    }

    return jerry_create_number(0);
}

JS_FUNCTION(js_soundPlayOnce)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = 0,
                  arg2 = 0,
                  arg3 = 0;
    float x = 0,
          y = 0,
          z = 0;
    if (args_cnt > 3)
    {
        arg1 = args_p[1];
        arg2 = args_p[2];
        arg3 = args_p[3];

        if (jerry_value_is_number(arg1))
            x = jerry_get_number_value(arg1);

        if (jerry_value_is_number(arg2))
            y = jerry_get_number_value(arg2);

        if (jerry_value_is_number(arg3))
            z = jerry_get_number_value(arg3);
    }

    SoundNode* node = nullptr;
    /* передали имя для создания звука? */
    if (jerry_value_is_string(arg0))
    {
        std::string name = JS_GetStringValue(arg0);
        node = SoundCreate(name);
    }
    else /* или передали id? */
        if (jerry_value_is_number(arg0))
        {
            /* проверим нода ли это вообще */
            AudioSource* src = (AudioSource*)((int)jerry_get_number_value(arg0));
            node = dynamic_cast<SoundNode*>(src);
        }

    if (node)
    {
        if (args_cnt > 3)
        {
            node->setRelative(false);
            node->setPosition(x, y, z);
        }
        node->playOnce();
        return jerry_create_number(node->getId());
    }

    return jerry_create_number(0);
}

JS_FUNCTION(js_soundPause)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (jerry_value_is_number(arg0))
    {
        /* проверим нода ли это вообще */
        AudioSource* src = (AudioSource*)((int)jerry_get_number_value(arg0));
        SoundNode* node = dynamic_cast<SoundNode*>(src);
        if (node != nullptr)
        {
            node->pause();
            return jerry_create_number(node->getId());
        }
    }

    return jerry_create_number(0);
}

JS_FUNCTION(js_soundResume)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (jerry_value_is_number(arg0))
    {
        /* проверим нода ли это вообще */
        AudioSource* src = (AudioSource*)((int)jerry_get_number_value(arg0));
        SoundNode* node = dynamic_cast<SoundNode*>(src);
        if (node != nullptr)
        {
            node->resume();
            return jerry_create_number(node->getId());
        }
    }

    return jerry_create_number(0);
}

JS_FUNCTION(js_soundStop)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (jerry_value_is_number(arg0))
    {
        /* проверим нода ли это вообще */
        AudioSource* src = (AudioSource*)((int)jerry_get_number_value(arg0));
        SoundNode* node = dynamic_cast<SoundNode*>(src);
        if (node != nullptr)
        {
            node->stop();
            return jerry_create_number(node->getId());
        }
    }

    return jerry_create_number(0);
}

JS_FUNCTION(js_soundIsPlaying)
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (jerry_value_is_number(arg0))
    {
        /* проверим нода ли это вообще */
        AudioSource* src = (AudioSource*)((int)jerry_get_number_value(arg0));
        SoundNode* node = dynamic_cast<SoundNode*>(src);
        if (node != nullptr)
        {
            return jerry_create_boolean(node->isPlaying());
        }
    }

    return jerry_create_number(0);
}

JS_FUNCTION(js_soundSetPosition)
{
    if (args_cnt < 3)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0],
                  arg1 = args_p[1],
                  arg2 = args_p[2],
                  arg3 = args_p[3];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    float x = 0,
          y = 0,
          z = 0;

    if (jerry_value_is_number(arg1))
        x = jerry_get_number_value(arg1);

    if (jerry_value_is_number(arg2))
        y = jerry_get_number_value(arg2);

    if (jerry_value_is_number(arg3))
        z = jerry_get_number_value(arg3);

    /* проверим нода ли это вообще */
    AudioSource* src = (AudioSource*)((int)jerry_get_number_value(arg0));
    SoundNode* node = dynamic_cast<SoundNode*>(src);
    if (node != nullptr)
    {
        node->setRelative(false);
        node->setPosition(x, y, z);
        return jerry_create_number(node->getId());
    }

    return jerry_create_number(0);
}

JS_FUNCTION(js_soundSetGain)
{
    if (args_cnt < 2)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    float gain = 0.0;
    if (jerry_value_is_number(arg1))
        gain = jerry_get_number_value(arg1);

    if (jerry_value_is_number(arg0))
    {
        /* проверим нода ли это вообще */
        AudioSource* src = (AudioSource*)((int)jerry_get_number_value(arg0));
        SoundNode* node = dynamic_cast<SoundNode*>(src);
        if (node != nullptr)
        {
            node->setGain(gain);
            return jerry_create_number(node->getId());
        }
    }

    return jerry_create_number(0);
}

void RegisterSoundJSFunctions()
{
    JS_RegisterCFunction(js_soundCreate,      "soundCreate");
    JS_RegisterCFunction(js_soundPlay,        "soundPlay");
    JS_RegisterCFunction(js_soundPlayOnce,    "soundPlayOnce");
    JS_RegisterCFunction(js_soundPause,       "soundPause");
    JS_RegisterCFunction(js_soundResume,      "soundResume");
    JS_RegisterCFunction(js_soundStop,        "soundStop");
    JS_RegisterCFunction(js_soundIsPlaying,   "soundIsPlaying");
    JS_RegisterCFunction(js_soundSetPosition, "soundSetPosition");
    JS_RegisterCFunction(js_soundSetGain,     "soundSetGain");
}
