#include "weapons.h"
#include "bullets.h"
#include "res_manager.h"
#include "file_system.h"
#include "sound.h"

using namespace FileSystem;

vector<TWeaponInMem> g_Weapons;
vector<TWeapon*> TWeapon::objects;
int g_ActiveWeapon = -1;


TWeapon::TWeapon(TWeaponInMem* index) : TObject2D(index)
{
    addVar("type", "weaponInHand");

    if (index)
    {
        addVar("align",     index->weaponAlign);
        addVar("fireFrame", index->fireFrame);
        addVar("fireSound", index->fireSound);
        addVar("damage",    index->damage  );
        addVar("distance",  index->distance);
        addVar("accuracy",  index->accuracy);
        addVar("curAmmo",   (int)(index->curAmmo));
        addVar("maxAmmo",   (int)(index->maxAmmo));
        addVar("infiniteAmmo", index->infiniteAmmo);
        addVar("fireType",  index->fireType);
        // если тип - видимая пуля, то запомнить имя
        if (index->fireType == 1 &&
            index->bulletId >= 0 &&
            index->bulletId < g_BulletsCount)
        {
            addVar("bulletName", g_Bullets[index->bulletId].name);
            // и подгрузить снаряды для пушки
            if (!g_Bullets[index->bulletId].isLoaded())
                g_Bullets[index->bulletId].loadFrames();
        }

        /* имя типа патронов по id */
        if (index->ammoTypeId != -1 &&
            index->ammoTypeId < g_AmmoTypes.size())
        {
            addVar("ammoType", g_AmmoTypes.at(index->ammoTypeId));
        }
    }

    TWeapon::objects.push_back(this);
}

TWeapon::~TWeapon()
{
    for (vector<TWeapon*>::iterator it = TWeapon::objects.begin();
         it != TWeapon::objects.end();
         ++it)
    {
        if (*it == this)
        {
            *it = nullptr;
            break;
        }
    }
}


void InitWeapons()
{
    if (g_Weapons.capacity() != WEAPONS_MAX_COUNT)
        g_Weapons.reserve(WEAPONS_MAX_COUNT);

    if (g_Weapons.size() != 0)
        g_Weapons.resize(0);
/*
    for (auto& weapon : g_Weapons)
    {
        weapon.clear();
    }
*/
    g_ActiveWeapon = -1;
}

bool LoadWeaponsPack()
{
    bool    isLoaded = false;
    TWeaponInMem* weapon = nullptr;

    char*    buffer = nullptr;
    uint64_t bufferSize;
    string   fileName;

    //string  weaponName;
    int fireFrame;
    int fireSoundId;

    uint8_t  animSpeed;
    uint32_t framesCount;
    uint8_t  weaponAlign;

    for (int i = 0; i < g_WeaponsCount; ++i)
    {
        // читаем инфу по текстуре
        fileName = "weapon_inhand_" + to_string(i) + ".cfg\0";
        File::readFull(fileName, &buffer, &bufferSize);
        if (bufferSize <= 0)
        {
            //PrintWithColor("can not get size of weapon_inhand_" ~ to!string(i) ~ ".cfg", wConsoleFontColor.wCFC_RED, false);
            continue;
        }

        weapon = &(g_Weapons[i]);

        Res_OpenIniFromBuffer(buffer, bufferSize);

        fireFrame   = Res_IniReadInteger("Options", "fire_frame", 0);
        fireSoundId = Res_IniReadInteger("Options", "fire_sound_id", -1);
        weaponAlign = (uint8_t)Res_IniReadInteger("Options", "align", 0);
        //fireType    = cast(TFireType)Res_IniReadInteger("Options", "fire_type",   0);
        animSpeed   = (uint8_t)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = Res_IniReadInteger("Options", "frames_count",   0);
        Res_CloseIni();

        weapon->fireFrame = fireFrame;
        /* ищем имя звука по id */
        if (fireSoundId != -1 && fireSoundId < g_SoundBuffers.size())
        {
            for (auto key : g_SoundBuffers)
            {
                if (key.second->id == fireSoundId)
                {
                    weapon->fireSound = key.first;
                    break;
                }
            }
        }
        weapon->weaponAlign = weaponAlign;
        weapon->animSpeed = animSpeed;
        weapon->script.loadFromFile("weapon_inhand.js");
        if (framesCount == 0)
            continue;

        // читаем кадры, если есть
        weapon->loadFrames();
    }
    isLoaded = true;

end:
    free(buffer);
    Res_CloseIni();

    return isLoaded;
}

void WeaponUpdate()
{
    if (g_ActiveWeapon < 0 || g_ActiveWeapon >= WEAPONS_MAX_COUNT)
        return;

    TWeapon* weapon = TWeapon::objects[g_ActiveWeapon];
    if (!weapon ||
        !weapon->index ||
        !weapon->isAvailable ||
        !weapon->index->isLoaded())
    {
        return;
    }

    weapon->update();
}




/**
 *  JerryScript
 **/


/** работа с объектами */
static jerry_value_t js_weaponSetAvailable
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_boolean(arg1))
        return jerry_create_undefined();

    uint32_t pos = jerry_get_number_value(arg0);
    if (pos > WEAPONS_MAX_COUNT)
        return jerry_create_undefined();

    bool isAvailable = (jerry_get_boolean_value(arg1) != 0);
    TWeapon::objects[pos - 1]->isAvailable = isAvailable; // -1, потому что снаружи нумерация 1..10

    return jerry_create_undefined();
}
static jerry_value_t js_weaponIsAvailable
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_boolean(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_boolean(0);

    uint32_t pos = jerry_get_number_value(arg0);
    if (pos > WEAPONS_MAX_COUNT)
        return jerry_create_boolean(0);

    return jerry_create_boolean(TWeapon::objects[pos - 1]->isAvailable);        // -1, потому что снаружи нумерация 1..10
}
static jerry_value_t js_weaponSetActive
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    uint32_t pos = jerry_get_number_value(arg0);
    if (pos > WEAPONS_MAX_COUNT)
        return jerry_create_undefined();

    --pos;       // -1, потому что снаружи нумерация 1..10
    if (pos >= 0 &&
        pos < TWeapon::objects.size() &&
        TWeapon::objects[pos]->isAvailable)
    {
        g_ActiveWeapon = pos;
    }

    return jerry_create_undefined();
}

void RegisterWeaponsJSFunctions()
{
    JS_RegisterCFunction(&js_weaponSetAvailable, "weaponSetAvailable");
    JS_RegisterCFunction(&js_weaponIsAvailable,  "weaponIsAvailable" );
    JS_RegisterCFunction(&js_weaponSetActive,    "weaponSetActive"   );
}
