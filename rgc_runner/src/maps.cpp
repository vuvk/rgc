#include <iostream>

#include "WorldSim3D.h"
#include "SampleFunctions.h"

#include "res_manager.h"
#include "global.h"
#include "maps.h"
#include "js_util.h"
#include "sound.h"
#include "file_system.h"
#include "textures.h"
#include "sprites.h"
#include "doors.h"
#include "keys.h"
#include "weapons.h"
#include "weapon_item.h"
#include "bullets.h"
#include "ammo.h"
#include "player.h"

using namespace FileSystem;

TMapPack mapPack;
int g_NumOfCurMap = -1;

// простая плоскость для пола и потолка
static wNode* floorNode = nullptr;
static wNode* ceilNode  = nullptr;
// нода Скайбокса
static wNode*    skyNode          = nullptr;
static wTexture* skyTextureFront  = nullptr;
static wTexture* skyTextureBack   = nullptr;
static wTexture* skyTextureLeft   = nullptr;
static wTexture* skyTextureRight  = nullptr;
static wTexture* skyTextureTop    = nullptr;
static wTexture* skyTextureBottom = nullptr;


/** инициализация пака карт*/
void InitMapPack()
{
    if (g_MapsCount >= 0)
        mapPack.maps.resize(g_MapsCount);
    else
        mapPack.maps.resize(0);
}

/** загрузка пака карт*/
bool LoadMapPack()
{
    TMapElement t;
    TMap*    map;
    uint8_t  x, y;  // координаты
    int      m;     // номер карты
    int      c;
    uint16_t count;
    char*    buffer = nullptr;
    uint64_t bufferSize;
    bool     isLoaded = false;
    int      mapSize;
    string   fileName;
    char*    p;

    PrintWithColor("Starting load maps...\n", wCFC_YELLOW, false);

    mapSize = MAP_WIDTH * MAP_HEIGHT;

    // читаем количество карт
    File::readFull("maps.cfg", &buffer, &bufferSize);
    if ((buffer == nullptr) ||
        (bufferSize == 0) ||
        !Res_OpenIniFromBuffer(buffer, bufferSize))
    {
        PrintWithColor("can not open maps.cfg!!!\n");
        goto end;
    }
    g_MapsCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_MapsCount == 0)
        goto end;

    InitMapPack();

    for (m = 0; m < g_MapsCount; ++m)
    {
        // читаем параметры карты
        fileName = "map_" + to_string(m) + ".cfg\0";
        File::readFull(fileName, &buffer, &bufferSize);
        Res_OpenIniFromBuffer(buffer, bufferSize);

        map = &(mapPack.maps[m]);
        map->name       = Res_IniReadString  ("Options", "name", ("Level #" + to_string(m) + "\0").c_str());
        map->floorColor = Res_IniReadInteger ("Options", "floor_color", 0);
        map->ceilColor  = Res_IniReadInteger ("Options", "ceil_color",  0);
        map->fogColor   = Res_IniReadInteger ("Options", "fog_color",   0);
        map->fogIntensity = Res_IniReadDouble("Options", "fog_intensity", 0);
        map->showFloor  = Res_IniReadBool    ("Options", "show_floor", false);
        map->showCeil   = Res_IniReadBool    ("Options", "show_ceil",  false);
        map->showFog    = Res_IniReadBool    ("Options", "show_fog",   false);
        map->showSky    = Res_IniReadBool    ("Options", "show_sky",   false);
        map->skyNumber  = Res_IniReadInteger ("Options", "sky_number", -1);

        Res_CloseIni();

        ////////////////////////////////////
        // читаем карту
        ////////////////////////////////////
        fileName = "map_" + to_string(m) + ".dat\0";
        File::readFull(fileName, &buffer, &bufferSize);

        /////////////////////////////////////////////////////
        // читаем зарисованные ячейки пола (если есть)
        /////////////////////////////////////////////////////
        p = buffer;
        count = *((uint16_t*)p);
        p += sizeof(count);
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                t = *((TMapElement*)p);
                p += sizeof(TMapElement);
                x = *p;
                p += sizeof(x);
                y = *p;
                p += sizeof(y);

                mapPack.maps[m].floor[x][y] = t;
            }
        }

        /////////////////////////////////////////////////////
        // читаем зарисованные ячейки уровня (если есть)
        /////////////////////////////////////////////////////
        count = *((uint16_t*)p);
        p += sizeof(count);
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                t = *((TMapElement*)p);
                p += sizeof(TMapElement);
                x = *p;
                p += sizeof(x);
                y = *p;
                p += sizeof(y);

                mapPack.maps[m].level[x][y] = t;
            }
        }

        /////////////////////////////////////////////////////
        // читаем зарисованные ячейки потолка (если есть)
        /////////////////////////////////////////////////////
        count = *((uint16_t*)p);
        p += sizeof(count);
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                t = *((TMapElement*)p);
                p += sizeof(TMapElement);
                x = *p;
                p += sizeof(x);
                y = *p;
                p += sizeof(y);

                mapPack.maps[m].ceil[x][y] = t;
            }
        }
    }

    isLoaded = true;

end:
    free(buffer);
    Res_CloseIni();
    PrintWithColor("Finished load maps.\n", wCFC_YELLOW, false);
    cout << "Maps Count - " << g_MapsCount << endl;

    return isLoaded;
}


/** очистить текущий уровень*/
void ClearLevel()
{
    wMesh* mesh;

    if (g_WorldCollider != nullptr)
    {
        wCollisionGroupRemoveAll(g_WorldCollider);
        g_WorldCollider = nullptr;
    }

    if (g_TextureNodes.size() > 0)
    {        
        for (TTextureNode* texture : g_TextureNodes)
        {
            if (texture != nullptr)
            {
                delete texture;
            }
        }
        g_TextureNodes.clear();
    }

    while (TObject::objects.size() > 0)
    {
        delete TObject::objects.front();
    }

    TSpriteNode::count = 0;
    TDoorNode::count = 0;
    TKeyNode::count = 0;
    TWeaponItemNode::count = 0;
    TAmmoNode::count = 0;

    TObject::objects.resize(0);
    TObject2D::objects.resize(0);
    TObject3D::objects.resize(0);
    TWeapon::objects.resize(0);
/*
    if (TObject::objects.capacity   < OBJECTS_MAX_COUNT)  TObject::objects.reserve  (OBJECTS_MAX_COUNT);
    if (TObject2D::objects.capacity < OBJECTS_MAX_COUNT)  TObject2D::objects.reserve(OBJECTS_MAX_COUNT);
    if (TObject3D::objects.capacity < OBJECTS_MAX_COUNT)  TObject3D::objects.reserve(OBJECTS_MAX_COUNT);
    if (TWeapon::objects.capacity   < WEAPONS_MAX_COUNT)  TWeapon::objects.reserve  (WEAPONS_MAX_COUNT);
*/
    if (g_SolidObjects != nullptr)
    {
        wNodeDestroy(g_SolidObjects);
        g_SolidObjects = nullptr;
    }

    if (g_InvisibleTexture != nullptr)
    {
        wTextureDestroy(g_InvisibleTexture);
        g_InvisibleTexture = nullptr;
    }

    if (g_InvisibleMesh != nullptr)
    {
        wMeshDestroy(g_InvisibleMesh);
        g_InvisibleMesh = nullptr;
    }

    if (floorNode != nullptr)
    {
        mesh = wNodeGetMesh(floorNode);
        if (mesh != nullptr)
            wMeshDestroy(mesh);
        wNodeDestroy(floorNode);
        floorNode = nullptr;
    }

    if (ceilNode != nullptr)
    {
        mesh = wNodeGetMesh(ceilNode);
        if (mesh != nullptr)
            wMeshDestroy(mesh);
        wNodeDestroy(ceilNode);
        ceilNode = nullptr;
    }

    if (skyNode != nullptr)
    {
        mesh = wNodeGetMesh(skyNode);
        if (mesh != nullptr)
            wMeshDestroy(mesh);
        wNodeDestroy(skyNode);
        skyNode = nullptr;

        if (skyTextureFront != nullptr)
        {
            wTextureDestroy(skyTextureFront);
            skyTextureFront = nullptr;
        }
        if (skyTextureBack != nullptr)
        {
            wTextureDestroy(skyTextureBack);
            skyTextureBack = nullptr;
        }
        if (skyTextureLeft != nullptr)
        {
            wTextureDestroy(skyTextureLeft);
            skyTextureLeft = nullptr;
        }
        if (skyTextureRight != nullptr)
        {
            wTextureDestroy(skyTextureRight);
            skyTextureRight = nullptr;
        }
        if (skyTextureTop != nullptr)
        {
            wTextureDestroy(skyTextureTop);
            skyTextureTop = nullptr;
        }
        if (skyTextureBottom != nullptr)
        {
            wTextureDestroy(skyTextureBottom);
            skyTextureBottom = nullptr;
        }
    }

    g_Player = nullptr;
    g_Camera = nullptr;

    // на всякий случай
    wSceneDestroyAllNodes();
    wSceneDestroyAllMeshes();

    g_NumOfCurMap = -1;
}


/*
 * ГЕНЕРАЦИЯ УРОВНЯ
 * */

/** создание стены направленной во front */
wMesh* CreateFrontWall(const string name, wTexture* texture, int x, int z, int len)
{
    float fromX = x         * BLOCK_SIZE;
    float toX   = (x + len) * BLOCK_SIZE;

    float dn = 0.0 * BLOCK_SIZE;
    float up = 1.0 * BLOCK_SIZE;

    float fd = (z - 1) * BLOCK_SIZE;

    wVert verts[] =
    {
        { { toX,   dn, fd }, wVECTOR3f_FORWARD, wCOLOR4s_WHITE, { len, 1.0 } },
        { { fromX, dn, fd }, wVECTOR3f_FORWARD, wCOLOR4s_WHITE, { 0.0, 1.0 } },
        { { fromX, up, fd }, wVECTOR3f_FORWARD, wCOLOR4s_WHITE, { 0.0, 0.0 } },
        { { toX,   up, fd }, wVECTOR3f_FORWARD, wCOLOR4s_WHITE, { len, 0.0 } }
    };
    const uint16_t indices[] = { 0, 1, 2, 0, 2, 3 };

    wMesh* mesh = wMeshCreate((char*)name.c_str());
    wMeshBuffer* meshBuffer;
    wMaterial* material;

    meshBuffer = wMeshBufferCreate(4, verts, 6, (uint16_t*)indices);
    material = wMeshBufferGetMaterial(meshBuffer);
    MaterialSetOldSchool(material);
    wMaterialSetTexture(material, 0, texture);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    return mesh;
}

/** создание стены направленной в back */
wMesh* CreateBackWall(const string name, wTexture* texture, int x, int z, int len)
{
    float fromX = x         * BLOCK_SIZE;
    float toX   = (x + len) * BLOCK_SIZE;

    float dn = 0.0 * BLOCK_SIZE;
    float up = 1.0 * BLOCK_SIZE;

    float bk = z * BLOCK_SIZE;

    wVert verts[] =
    {
        { { fromX, dn, bk }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { len, 1.0 } },
        { { toX,   dn, bk }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { 0.0, 1.0 } },
        { { toX,   up, bk }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { 0.0, 0.0 } },
        { { fromX, up, bk }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, { len, 0.0 } }
    };
    const uint16_t indices[] = { 0, 1, 2, 0, 2, 3 };

    wMesh* mesh = wMeshCreate((char*)name.c_str());
    wMeshBuffer* meshBuffer;
    wMaterial* material;

    meshBuffer = wMeshBufferCreate(4, verts, 6, (uint16_t*)indices);
    material = wMeshBufferGetMaterial(meshBuffer);
    MaterialSetOldSchool(material);
    wMaterialSetTexture(material, 0, texture);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    return mesh;
}

/** создание стены направленной в right */
wMesh* CreateRightWall(const string name, wTexture* texture, int x, int z, int len)
{
    float fromZ = z         * BLOCK_SIZE;
    float toZ   = (z - len) * BLOCK_SIZE;

    float dn = 0.0 * BLOCK_SIZE;
    float up = 1.0 * BLOCK_SIZE;

    float rt = (x + 1) * BLOCK_SIZE;

    wVert verts[] =
    {
        { { rt, dn, fromZ }, wVECTOR3f_RIGHT, wCOLOR4s_WHITE, { len, 1.0 } },
        { { rt, dn, toZ   }, wVECTOR3f_RIGHT, wCOLOR4s_WHITE, { 0.0, 1.0 } },
        { { rt, up, toZ   }, wVECTOR3f_RIGHT, wCOLOR4s_WHITE, { 0.0, 0.0 } },
        { { rt, up, fromZ }, wVECTOR3f_RIGHT, wCOLOR4s_WHITE, { len, 0.0 } }
    };
    const uint16_t indices[] = { 0, 1, 2, 0, 2, 3 };

    wMesh* mesh = wMeshCreate((char*)name.c_str());
    wMeshBuffer* meshBuffer;
    wMaterial* material;

    meshBuffer = wMeshBufferCreate(4, verts, 6, (uint16_t*)indices);
    material = wMeshBufferGetMaterial(meshBuffer);
    MaterialSetOldSchool(material);
    wMaterialSetTexture(material, 0, texture);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    return mesh;
}

/** создание стены направленной в left */
wMesh* CreateLeftWall(const string name, wTexture* texture, int x, int z, int len)
{
    float fromZ = z         * BLOCK_SIZE;
    float toZ   = (z - len) * BLOCK_SIZE;

    float dn = 0.0 * BLOCK_SIZE;
    float up = 1.0 * BLOCK_SIZE;

    float lt = x * BLOCK_SIZE;

    wVert verts[] =
    {
        { { lt, dn, toZ   }, wVECTOR3f_LEFT, wCOLOR4s_WHITE, { len, 1.0 } },
        { { lt, dn, fromZ }, wVECTOR3f_LEFT, wCOLOR4s_WHITE, { 0.0, 1.0 } },
        { { lt, up, fromZ }, wVECTOR3f_LEFT, wCOLOR4s_WHITE, { 0.0, 0.0 } },
        { { lt, up, toZ   }, wVECTOR3f_LEFT, wCOLOR4s_WHITE, { len, 0.0 } }
    };
    const uint16_t indices[] = { 0, 1, 2, 0, 2, 3 };

    wMesh* mesh = wMeshCreate((char*)name.c_str());
    wMeshBuffer* meshBuffer;
    wMaterial* material;

    meshBuffer = wMeshBufferCreate(4, verts, 6, (uint16_t*)indices);
    material = wMeshBufferGetMaterial(meshBuffer);
    MaterialSetOldSchool(material);
    wMaterialSetTexture(material, 0, texture);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    return mesh;
}

/** создание меша пола */
wMesh* CreateFloor(const string name, wTexture* texture, int x, int z, int lenX, int lenZ)
{
    float fromX = x          * BLOCK_SIZE;
    float toX   = (x + lenX) * BLOCK_SIZE;
    float fromZ = z          * BLOCK_SIZE;
    float toZ   = (z - lenZ) * BLOCK_SIZE;

    float dn = 0.0 * BLOCK_SIZE;
    //float up = 1.0 * BLOCK_SIZE;

    wVert verts[] =
    {
        { { fromX, dn, fromZ }, wVECTOR3f_UP, wCOLOR4s_WHITE, {  0.0,  0.0 } },
        { { toX,   dn, fromZ }, wVECTOR3f_UP, wCOLOR4s_WHITE, { lenX,  0.0 } },
        { { toX,   dn, toZ   }, wVECTOR3f_UP, wCOLOR4s_WHITE, { lenX, lenZ } },
        { { fromX, dn, toZ   }, wVECTOR3f_UP, wCOLOR4s_WHITE, {  0.0, lenZ } }
    };
    const uint16_t indices[] = { 0, 1, 2, 0, 2, 3 };

    wMesh* mesh = wMeshCreate((char*)name.c_str());
    wMeshBuffer* meshBuffer;
    wMaterial* material;

    meshBuffer = wMeshBufferCreate(4, verts, 6, (uint16_t*)indices);
    material = wMeshBufferGetMaterial(meshBuffer);
    wMaterialSetTexture(material, 0, texture);
    MaterialSetOldSchool(material);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    return mesh;
}

/** создание меша потолка */
wMesh* CreateCeil(const string name, wTexture* texture, int x, int z, int lenX, int lenZ)
{
    float fromX = x          * BLOCK_SIZE;
    float toX   = (x + lenX) * BLOCK_SIZE;
    float fromZ = z          * BLOCK_SIZE;
    float toZ   = (z - lenZ) * BLOCK_SIZE;

    //float dn = 0.0 * BLOCK_SIZE;
    float up = 1.0 * BLOCK_SIZE;

    wVert verts[] =
    {
        { { fromX, up, toZ   }, wVECTOR3f_UP, wCOLOR4s_WHITE, {  0.0, lenZ } },
        { { toX,   up, toZ   }, wVECTOR3f_UP, wCOLOR4s_WHITE, { lenX, lenZ } },
        { { toX,   up, fromZ }, wVECTOR3f_UP, wCOLOR4s_WHITE, { lenX,  0.0 } },
        { { fromX, up, fromZ }, wVECTOR3f_UP, wCOLOR4s_WHITE, { 0.0,   0.0 } }
    };
    const uint16_t indices[] = { 0, 1, 2, 0, 2, 3 };

    wMesh* mesh = wMeshCreate((char*)name.c_str());
    wMeshBuffer* meshBuffer;
    wMaterial* material;

    meshBuffer = wMeshBufferCreate(4, verts, 6, (uint16_t*)indices);
    material = wMeshBufferGetMaterial(meshBuffer);
    wMaterialSetTexture(material, 0, texture);
    MaterialSetOldSchool(material);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    return mesh;
}

/** создание одноцветных плоскостей пола и потолка */
void CreateFloorAndCeilPlane(uint32_t floorColor, uint32_t ceilColor)
{
    float lt =  0.0 * BLOCK_SIZE;
    float rt =  MAP_WIDTH * BLOCK_SIZE;
    float dn = -0.1 * BLOCK_SIZE;
    float up =  1.1 * BLOCK_SIZE;
    float bk =  0.0 * BLOCK_SIZE;
    float fw = -MAP_HEIGHT * BLOCK_SIZE;

    // набор вершин пола
    wVert floorVerts[] =
    {
        { { lt, dn, bk }, wVECTOR3f_UP, wCOLOR4s_WHITE, { 0.0,       0.0        } },
        { { rt, dn, bk }, wVECTOR3f_UP, wCOLOR4s_WHITE, { MAP_WIDTH, 0.0        } },
        { { rt, dn, fw }, wVECTOR3f_UP, wCOLOR4s_WHITE, { MAP_WIDTH, MAP_HEIGHT } },
        { { lt, dn, fw }, wVECTOR3f_UP, wCOLOR4s_WHITE, { 0.0,       MAP_HEIGHT } }
    };
    const uint16_t floorIndices[] = { 0, 1, 2, 0, 2, 3 };

    // набор вершин потолка
    wVert ceilVerts[] =
    {
        { { lt, up, fw }, wVECTOR3f_UP, wCOLOR4s_WHITE, { 0.0,       MAP_HEIGHT } },
        { { rt, up, fw }, wVECTOR3f_UP, wCOLOR4s_WHITE, { MAP_WIDTH, MAP_HEIGHT } },
        { { rt, up, bk }, wVECTOR3f_UP, wCOLOR4s_WHITE, { MAP_WIDTH, 0.0        } },
        { { lt, up, bk }, wVECTOR3f_UP, wCOLOR4s_WHITE, { 0.0,       0.0        } }
    };
    const uint16_t ceilIndices[] = { 0, 1, 2, 0, 2, 3 };

    wTexture* texture;
    wMaterial* material;
    wMesh* mesh;
    wMeshBuffer* meshBuffer;

    //
    // создаем плоскость пола
    mesh = wMeshCreate("floor plane");
    texture = CreateColorTexture(wUtilUIntToColor4s(floorColor), "Floor Color");

    meshBuffer = wMeshBufferCreate(4, floorVerts, 6, (uint16_t*)floorIndices);
    material = wMeshBufferGetMaterial(meshBuffer);
    wMaterialSetTexture(material, 0, texture);
    MaterialSetOldSchool(material);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    floorNode = wNodeCreateFromMesh(mesh);

    //
    // создаем плоскость потолка
    mesh = wMeshCreate("ceil plane");
    texture = CreateColorTexture(wUtilUIntToColor4s(ceilColor), "Ceil Color");

    meshBuffer = wMeshBufferCreate(4, ceilVerts, 6, (uint16_t*)ceilIndices);
    material = wMeshBufferGetMaterial(meshBuffer);
    wMaterialSetTexture(material, 0, texture);
    MaterialSetOldSchool(material);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    ceilNode = wNodeCreateFromMesh(mesh);
}

/** сгенерировать уровень №__ */
void GenerateLevel(int numOfMap)
{
    /* ТЕЛО ФУНКЦИИ */

    int i, j/*, k*/;
    TMap* map = nullptr;
    wNode* node = nullptr;
    wMesh* mesh = nullptr;
    wMesh* batch = nullptr;
    wNode* billboard = nullptr;
    wNode* solidBox = nullptr;
    wTexture*  texture = nullptr;
    wMaterial* material = nullptr;
    TMapElement element = 0;
    int count = 0;

    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    ClearLevel();

    g_NumOfCurMap = numOfMap;

    map = &(mapPack.maps[numOfMap]);

    // создаем список мешей, который будет сформирован по одинаковым текстурам
    vector<wMesh*> meshes;

    // создаем коллизионную группу, если её нет
    if (g_WorldCollider == nullptr) g_WorldCollider = wCollisionGroupCreate();
    if (g_SolidObjects  == nullptr) g_SolidObjects  = wNodeCreateEmpty();

    // обрабатываем спрайты
    int fromPos, toPos;
    fromPos = TEXTURES_MAX_COUNT;
    toPos   = TEXTURES_MAX_COUNT + SPRITES_MAX_COUNT;
    for (i = 0; i < MAP_WIDTH; ++i)
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map->level[i][j];
            // если ничего нет...
            if (element == 0)
                continue;

            // -1 потому что нумерация в массиве с 0, а элементы не могут быть равны 0
            --element;

            if (element >= fromPos && element < toPos)
            {
                int sprNum = element - fromPos;

                TSpriteNode* spriteNode = new TSpriteNode(&(g_Sprites[sprNum]));
                wVector3f position = {(i + 0.5) * BLOCK_SIZE, 0.0, (-j - 0.5) * BLOCK_SIZE};
                spriteNode->setPosition(position);
            }
        }

    // обрабатываем ключи
    fromPos = TEXTURES_MAX_COUNT + SPRITES_MAX_COUNT + DOORS_MAX_COUNT;
    toPos   = fromPos + KEYS_MAX_COUNT;
    for (i = 0; i < MAP_WIDTH; ++i)
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map->level[i][j];
            // если ничего нет...
            if (element == 0)
                continue;

            // -1 потому что нумерация в массиве с 0, а элементы не могут быть равны 0
            --element;

            if (element >= fromPos && element < toPos)
            {
                int keyNum = element - fromPos;

                wVector3f position = { (i + 0.5) * BLOCK_SIZE, 0.0, (-j - 0.5) * BLOCK_SIZE };

                TKeyNode* keyNode = new TKeyNode(&(g_Keys[keyNum]));
                keyNode->setPosition(position);
            }
        }

    // обрабатываем двери
    fromPos = TEXTURES_MAX_COUNT + SPRITES_MAX_COUNT;
    toPos   = TEXTURES_MAX_COUNT + SPRITES_MAX_COUNT + DOORS_MAX_COUNT;
    for (i = 0; i < MAP_WIDTH; ++i)
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map->level[i][j];
            // если ничего нет...
            if (element == 0)
                continue;

            // -1 потому что нумерация в массиве с 0, а элементы не могут быть равны 0
            --element;

            if (element >= fromPos && element < toPos)
            {
                int el0, el1;
                bool isDoorCreated = false;
                bool isVertical   = false,
                     isHorizontal = false;

                /* горизонтальная дверь? */
                el0 = el1 = 0;
                if (i > 0)             el0 = map->level[i - 1][j];
                if (i < MAP_WIDTH - 1) el1 = map->level[i + 1][j];
                if ((el0 > 0 && el0 < TEXTURES_MAX_COUNT) &&
                    (el1 > 0 && el1 < TEXTURES_MAX_COUNT))
                {
                    isDoorCreated = true;
                    isHorizontal  = true;
                }
                else
                /* может быть она вертикальная? */
                {
                    el0 = el1 = 0;
                    if (j > 0)              el0 = map->level[i][j - 1];
                    if (j < MAP_HEIGHT - 1) el1 = map->level[i][j + 1];
                    if ((el0 > 0 && el0 < TEXTURES_MAX_COUNT) &&
                        (el1 > 0 && el1 < TEXTURES_MAX_COUNT))
                    {
                        isDoorCreated = true;
                        isVertical    = true;
                    }
                }

                // дверь не может быть создана
                if (!isDoorCreated || (!isVertical && !isHorizontal))
                    continue;

                int doorNum = element - fromPos;

                TDoorNode* doorNode = new TDoorNode(&(g_Doors[doorNum]), isHorizontal, isVertical);
                wVector3f position;
                if (isHorizontal)
                    position = { i        * BLOCK_SIZE, 0.0 * BLOCK_SIZE, (-j - 0.5) * BLOCK_SIZE};
                else
                    position = {(i + 0.5) * BLOCK_SIZE, 0.0 * BLOCK_SIZE, (-j)       * BLOCK_SIZE};
                doorNode->setPosition(position);
            }
        }

    // обрабатываем оружие
    fromPos = TEXTURES_MAX_COUNT +
              SPRITES_MAX_COUNT  +
              DOORS_MAX_COUNT    +
              KEYS_MAX_COUNT;
    toPos   = fromPos + WEAPONS_MAX_COUNT;
    for (i = 0; i < MAP_WIDTH; ++i)
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map->level[i][j];
            // если ничего нет...
            if (element == 0)
                continue;

            // -1 потому что нумерация в массиве с 0, а элементы не могут быть равны 0
            --element;

            if (element >= fromPos && element < toPos)
            {
                int weaponNum = element - fromPos;

                wVector3f position = { (i + 0.5) * BLOCK_SIZE, 0.0, (-j - 0.5) * BLOCK_SIZE };

                TWeaponItemNode* weaponNode = new TWeaponItemNode(&(g_WeaponItems[weaponNum]));
                weaponNode->setPosition(position);
            }
        }

    // обрабатываем патроны
    fromPos = TEXTURES_MAX_COUNT +
              SPRITES_MAX_COUNT  +
              DOORS_MAX_COUNT    +
              KEYS_MAX_COUNT     +
              WEAPONS_MAX_COUNT;
    toPos   = fromPos + AMMO_MAX_COUNT;
    for (i = 0; i < MAP_WIDTH; ++i)
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map->level[i][j];
            // если ничего нет...
            if (element == 0)
                continue;

            // -1 потому что нумерация в массиве с 0, а элементы не могут быть равны 0
            --element;

            if (element >= fromPos && element < toPos)
            {
                int ammoNum = element - fromPos;

                wVector3f position = { (i + 0.5) * BLOCK_SIZE, 0.0, (-j - 0.5) * BLOCK_SIZE };

                TAmmoNode* ammoNode = new TAmmoNode(&(g_Ammo[ammoNum]));
                ammoNode->setPosition(position);
            }
        }

    ///////////////////////////////////////////////////
    // обработка горизонтальных стен
    ///////////////////////////////////////////////////
    TMapElement pEl;    // пред. элемент
    TMapElement el;     // текущий элемент
    TMapElement tEl;    // элемент для теста
    int lenX;       // длина сегмента в ширину
    int lenY;       // длина сегмента в высоту

    int pos;        // позиция с которой началось объединение
    int x, y;       // временные для прохода арстекающегося алгоритма

    for (j = 0; j < MAP_HEIGHT; ++j)
    {
        // нижняя грань (в 3Д - передняя)
        if (j < MAP_HEIGHT - 1)
        {
            for (i = 0; i < MAP_WIDTH - 1; ++i)
            {
                el  = map->level[i][j];
                tEl = map->level[i][j + 1];

                // если текущий блок стена, а снизу не стена, то наращиваем длину сегмента
                if ((el > 0 && el < TEXTURES_MAX_COUNT) &&
                    (tEl == 0 || tEl >= TEXTURES_MAX_COUNT))
                {
                    lenX = 1;
                    pos = i;

                    pEl = el;
                    ++i;
                    bool proceed = true;
                    while (proceed)
                    {
                        el  = map->level[i][j];
                        tEl = map->level[i][j + 1];

                        // если следующий элемент не равен предыдущему или перекрыт,
                        // то закончить проход и откатиться назад
                        if (el != pEl || (tEl > 0 && tEl < TEXTURES_MAX_COUNT))
                        {
                            --i;
                            proceed = false;
                            continue;
                        }

                        ++lenX;
                        if (i == MAP_WIDTH - 1)
                        {
                            proceed = false;
                            continue;
                        }

                        ++i;
                    }

                    string nodeName = "wall node " + to_string(count);
                    meshes.push_back(CreateFrontWall(nodeName, g_Textures[--pEl].frames[0], pos, -j, lenX));
                    ++count;
                }
            }
        }

        // верхняя грань (в 3Д - задняя)
        if (j > 0)
        {
            for (i = 0; i < MAP_WIDTH - 1; ++i)
            {
                el  = map->level[i][j];
                tEl = map->level[i][j - 1];

                // если текущий блок стена, а снизу не стена, то наращиваем длину сегмента
                if ((el > 0 && el < TEXTURES_MAX_COUNT) &&
                    (tEl == 0 || tEl >= TEXTURES_MAX_COUNT))
                {
                    lenX = 1;
                    pos = i;

                    pEl = el;
                    ++i;
                    bool proceed = true;
                    while (proceed)
                    {
                        el  = map->level[i][j];
                        tEl = map->level[i][j - 1];

                        // если следующий элемент не равен предыдущему или перекрыт,
                        // то закончить проход и откатиться назад
                        if (el != pEl || (tEl > 0 && tEl < TEXTURES_MAX_COUNT))
                        {
                            --i;
                            proceed = false;
                            continue;
                        }

                        ++lenX;
                        if (i == MAP_WIDTH - 1)
                        {
                            proceed = false;
                            continue;
                        }

                        ++i;
                    }

                    string nodeName = "wall node " + to_string(count);
                    meshes.push_back(CreateBackWall(nodeName, g_Textures[--pEl].frames[0], pos, -j, lenX));
                    ++count;
                }
            }
        }
    }

    ///////////////////////////////////////////////////
    // обработка вертикальных стен
    ///////////////////////////////////////////////////
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        // правая грань
        if (i < MAP_WIDTH - 1)
        {
            for (j = 0; j < MAP_HEIGHT - 1; ++j)
            {
                el  = map->level[i    ][j];
                tEl = map->level[i + 1][j];

                // если текущий блок стена, а снизу не стена, то наращиваем длину сегмента
                if ((el > 0 && el < TEXTURES_MAX_COUNT) &&
                    (tEl == 0 || tEl >= TEXTURES_MAX_COUNT))
                {
                    lenY = 1;
                    pos = j;

                    pEl = el;
                    ++j;
                    bool proceed = true;
                    while (proceed)
                    {
                        el  = map->level[i    ][j];
                        tEl = map->level[i + 1][j];

                        // если следующий элемент не равен предыдущему или перекрыт,
                        // то закончить проход и откатиться назад
                        if (el != pEl || (tEl > 0 && tEl < TEXTURES_MAX_COUNT))
                        {
                            --j;
                            proceed = false;
                            continue;
                        }

                        ++lenY;
                        if (j == MAP_HEIGHT - 1)
                        {
                            proceed = false;
                            continue;
                        }

                        ++j;
                    }

                    string nodeName = "wall node " + to_string(count);
                    meshes.push_back(CreateRightWall(nodeName, g_TexturesShadowed[--pEl].frames[0], i, -pos, lenY));
                    ++count;
                }
            }
        }

        // левая грань
        if (i > 0)
        {
            for (j = 0; j < MAP_HEIGHT - 1; ++j)
            {
                el  = map->level[i    ][j];
                tEl = map->level[i - 1][j];

                // если текущий блок стена, а снизу не стена, то наращиваем длину сегмента
                if ((el > 0 && el < TEXTURES_MAX_COUNT) &&
                    (tEl == 0 || tEl >= TEXTURES_MAX_COUNT))
                {
                    lenY = 1;
                    pos = j;

                    pEl = el;
                    ++j;
                    bool proceed = true;
                    while (proceed)
                    {
                        el  = map->level[i    ][j];
                        tEl = map->level[i - 1][j];

                        // если следующий элемент не равен предыдущему или перекрыт,
                        // то закончить проход и откатиться назад
                        if (el != pEl || (tEl > 0 && tEl < TEXTURES_MAX_COUNT))
                        {
                            --j;
                            proceed = false;
                            continue;
                        }

                        ++lenY;
                        if (j == MAP_HEIGHT - 1)
                        {
                            proceed = false;
                            continue;
                        }

                        ++j;
                    }

                    string nodeName = "wall node " + to_string(count);
                    meshes.push_back(CreateLeftWall(nodeName, g_TexturesShadowed[--pEl].frames[0], i, -pos, lenY));
                    ++count;
                }
            }
        }
    }



    ///////////////////////////////////////////////////
    // обработка пола и потолка
    ///////////////////////////////////////////////////

    TMapElement mergeInfo[MAP_WIDTH][MAP_HEIGHT] = {};   // информация о том, можно ли объединять текущую ячейку и что там
                                                         // если 0, значит нельзя
    TMapElement lEl;    // элемент на уровне

    // если не показывать небо, то создать одноцветные плоскости потолка и пола
    if (!map->showSky && map->skyNumber >= 0)
        CreateFloorAndCeilPlane(map->floorColor, map->ceilColor);
    else
    {
        string fileName;
        string skyboxName;

        /// FRONT
        fileName = "skybox_front_" + to_string(map->skyNumber) + ".dat";
        skyboxName = "Skybox_front_" + to_string(map->skyNumber);
        skyTextureFront = LoadTexture(fileName, skyboxName);

        /// BACK
        fileName = "skybox_back_" + to_string(map->skyNumber) + ".dat";
        skyboxName = "Skybox_back_" + to_string(map->skyNumber);
        skyTextureBack = LoadTexture(fileName, skyboxName);

        /// LEFT
        fileName = "skybox_left_" + to_string(map->skyNumber) + ".dat";
        skyboxName = "Skybox_left_" + to_string(map->skyNumber);
        skyTextureLeft = LoadTexture(fileName, skyboxName);

        /// RIGHT
        fileName = "skybox_right_" + to_string(map->skyNumber) + ".dat";
        skyboxName = "Skybox_right_" + to_string(map->skyNumber);
        skyTextureRight = LoadTexture(fileName, skyboxName);

        /// TOP
        fileName = "skybox_top_" + to_string(map->skyNumber) + ".dat";
        skyboxName = "Skybox_top_" + to_string(map->skyNumber);
        skyTextureTop = LoadTexture(fileName, skyboxName);

        /// BOTTOM
        fileName = "skybox_bottom_" + to_string(map->skyNumber) + ".dat";
        skyboxName = "Skybox_bottom_" + to_string(map->skyNumber);
        skyTextureBottom = LoadTexture(fileName, skyboxName);

        /// CREATE SKY CUBE
        skyNode = wSkyBoxCreate(skyTextureTop,   skyTextureBottom,
                                skyTextureLeft,  skyTextureRight,
                                skyTextureFront, skyTextureBack);
    }

    ///////////////////////////////////////////////////
    // ПОЛ
    ///////////////////////////////////////////////////
    // если не показывать текстурный пол
    if (!map->showFloor) goto no_floor;

    // обновляем информацию по объединению
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            el  = map->floor[i][j];
            lEl = map->level[i][j];
            // можно объединять, если есть элемент пола и на этой позиции нет стены
            if ((el > 0) && (lEl == 0 || lEl >= TEXTURES_MAX_COUNT))
                mergeInfo[i][j] = el;
            else
                mergeInfo[i][j] = 0;
        }
    }

    // теперь начнем проход для объединения ячеек по типу
    for (j = 0; j < MAP_HEIGHT; ++j)
    {
        for (i = 0; i < MAP_WIDTH; ++i)
        {
            el = mergeInfo[i][j];
            // если элемент можно объединять
            if (el != 0)
            {
                pos  = i;               // запомнить его позицию
                lenX = lenY = 1;        // длина и высота как минимум в один квадрат
                pEl  = el;              // запомним какой элемент по типу
                mergeInfo[i][j] = 0;    // помечаем, что текущий элемент будет объединен

                // проверяем следующий за текущим элемент справа
                if (pos < MAP_WIDTH - 1)
                {
                    tEl = mergeInfo[pos + 1][j];
                    // если он такой же
                    if (tEl == pEl)
                    {
                        // то бежим по строке, пока не закончится уровень или не наткнемся на что-то другое
                        bool proceed = true;
                        for (x = pos + 1; (x < MAP_WIDTH) && proceed; ++x)
                        {
                            el = mergeInfo[x][j];
                            if (el != pEl)          // что-то другое
                            {
                                proceed = false;    // выйти
                                continue;
                            }

                            // следующий элемент такой же
                            mergeInfo[x][j] = 0;    // помечаем
                            ++lenX;                 // наращиваем ширину
                        }
                    }
                }

                // проверяем следующий за текущим элемент снизу
                if (j < MAP_HEIGHT - 1)
                {
                    tEl = mergeInfo[pos][j + 1];
                    // если он такой же
                    if (tEl == pEl)
                    {
                        // здесь мы пробежимся по высоте
                        bool proceed = true;
                        for (y = j + 1; (y < MAP_HEIGHT) && proceed; ++y)
                        {
                            for (x = pos; (x < pos + lenX) && proceed; ++x)
                            {
                                el = mergeInfo[x][y];
                                if (el != pEl)          // что-то другое
                                {
                                    proceed = false;    // выйти
                                    continue;
                                }
                            }

                            if (!proceed)
                                continue;

                            // если дошел сюда, значит вся линия совпадает с верхней
                            // наращиваем высоту и помечаем инфу
                            for (x = pos; x < pos + lenX; ++x)
                                mergeInfo[x][y] = 0;

                            ++lenY;
                        }
                    }
                }

                string nodeName = "floor node " + to_string(count);
                meshes.push_back(CreateFloor(nodeName, g_Textures[--pEl].frames[0], pos, -j, lenX, lenY));

                // следующие lenX элементов всё равно 0, так что перепрыгиваем
                i = lenX - 1;
                ++count;
            }
        }
    }
no_floor:


    ///////////////////////////////////////////////////
    // ПОТОЛОК
    ///////////////////////////////////////////////////
    // если не рисовать текстурный пол
    if (!map->showCeil) goto no_ceil;

    // обновляем информацию по объединению
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            el  = map->ceil [i][j];
            lEl = map->level[i][j];
            // можно объединять, если есть элемент пола и на этой позиции нет стены
            if ((el > 0) && (lEl == 0 || lEl >= TEXTURES_MAX_COUNT))
                mergeInfo[i][j] = el;
            else
                mergeInfo[i][j] = 0;
        }
    }

    // теперь начнем проход для объединения ячеек по типу
    for (j = 0; j < MAP_HEIGHT; ++j)
    {
        for (i = 0; i < MAP_WIDTH; ++i)
        {
            el = mergeInfo[i][j];
            // если элемент можно объединять
            if (el != 0)
            {
                pos  = i;               // запомнить его позицию
                lenX = lenY = 1;        // длина и высота как минимум в один квадрат
                pEl  = el;              // запомним какой элемент по типу
                mergeInfo[i][j] = 0;    // помечаем, что текущий элемент будет объединен

                // проверяем следующий за текущим элемент справа
                if (pos < MAP_WIDTH - 1)
                {
                    tEl = mergeInfo[pos + 1][j];
                    // если он такой же
                    if (tEl == pEl)
                    {
                        // то бежим по строке, пока не закончится уровень или не наткнемся на что-то другое
                        bool proceed = true;
                        for (x = pos + 1; (x < MAP_WIDTH) && proceed; ++x)
                        {
                            el = mergeInfo[x][j];
                            if (el != pEl)          // что-то другое
                            {
                                proceed = false;    // выйти
                                continue;
                            }

                            // следующий элемент такой же
                            mergeInfo[x][j] = 0;    // помечаем
                            ++lenX;                 // наращиваем ширину
                        }
                    }
                }

                // проверяем следующий за текущим элемент снизу
                if (j < MAP_HEIGHT - 1)
                {
                    tEl = mergeInfo[pos][j + 1];
                    // если он такой же
                    if (tEl == pEl)
                    {
                        // здесь мы пробежимся по высоте
                        bool proceed = true;
                        for (y = j + 1; (y < MAP_HEIGHT) && proceed; ++y)
                        {
                            for (x = pos; (x < pos + lenX) && proceed; ++x)
                            {
                                el = mergeInfo[x][y];
                                if (el != pEl)          // что-то другое
                                {
                                    proceed = false;    // выйти
                                    continue;
                                }
                            }

                            if (!proceed)
                                continue;

                            // если дошел сюда, значит вся линия совпадает с верхней
                            // наращиваем высоту и помечаем инфу
                            for (x = pos; x < pos + lenX; ++x)
                                mergeInfo[x][y] = 0;

                            ++lenY;
                        }
                    }
                }

                string nodeName = "ceil node " + to_string(count);
                meshes.push_back(CreateCeil(nodeName, g_Textures[--pEl].frames[0], pos, -j, lenX, lenY));

                // следующие lenX элементов всё равно 0, так что перепрыгиваем
                i = lenX - 1;
                ++count;
            }
        }
    }
no_ceil:

    ///////////////////////////////////////////////////
    // БАТЧИНГ
    ///////////////////////////////////////////////////


    // теперь у нас есть список мешей. Можно пройтись по нему и создать несколько батчинг мешей
    wTexture* tTexture;  // тестируемая текстура
    // проходимся по каждой текстуре
    cout << "meshes length = " << meshes.size() << endl;

    if (meshes.size() > 0)
    {
        for (i = 0; i < g_Textures.size(); ++i)
        {
            if (g_Textures[i].frames.size() > 0)
            {
                tTexture = g_Textures[i].frames[0];
                batch = wMeshCreateBatching();
                // ищем меши с такой же текстурой
                for (auto mesh : meshes)
                {
                    if (mesh == nullptr)
                        continue;

                    material = wMeshBufferGetMaterial(wMeshGetBuffer(mesh, 0, 0));
                    texture = wMaterialGetTexture(material, 0);
                    if (texture == tTexture)
                    {
                        wMeshAddToBatching(batch, mesh, wVECTOR3f_ZERO, wVECTOR3f_ZERO, wVECTOR3f_ONE);
                    }
                }

                wMeshFinalizeBatching(batch);
                node = wNodeCreateFromBatchingMesh(batch);

                TTextureNode* textureNode = new TTextureNode(&(g_Textures[i]), node);
                g_TextureNodes.push_back(textureNode);

                // добавляем коллизии в коллизионную группу
                wSelector* col = wCollisionCreateFromBatchingMesh(batch, node);
                if (col != nullptr)
                    wCollisionGroupAddCollision(g_WorldCollider, col);

                //wNodeSetDebugMode(node, wDebugMode.wDM_MESH_WIRE_OVERLAY);
                //wNodeSetDebugDataVisible(node, true);
            }

            // теперь то же самое для теневых текстур
            if (g_TexturesShadowed[i].frames.size() > 0)
            {
                tTexture = g_TexturesShadowed[i].frames[0];
                batch = wMeshCreateBatching();
                // ищем меши с такой же текстурой
                for (auto mesh : meshes)
                {
                    if (mesh == nullptr)
                        continue;

                    material = wMeshBufferGetMaterial(wMeshGetBuffer(mesh, 0, 0));
                    texture = wMaterialGetTexture(material, 0);
                    if (texture == tTexture)
                        wMeshAddToBatching(batch, mesh, wVECTOR3f_ZERO, wVECTOR3f_ZERO, wVECTOR3f_ONE);
                }

                wMeshFinalizeBatching(batch);
                node = wNodeCreateFromBatchingMesh(batch);

                TTextureNode* textureNode = new TTextureNode(&(g_TexturesShadowed[i]), node);
                g_TextureNodes.push_back(textureNode);

                // добавляем коллизии в коллизионную группу
                wSelector* col = wCollisionCreateFromBatchingMesh(batch, node);
                if (col != nullptr)
                    wCollisionGroupAddCollision(g_WorldCollider, col);

                //wNodeSetDebugMode(node, wDebugMode.wDM_MESH_WIRE_OVERLAY);
                //wNodeSetDebugDataVisible(node, true);
            }
        }
    }

    /* TODO : нужно ли очищать указатели на оригинальные меши? */
    meshes.clear();

    wSceneDestroyAllUnusedMeshes();

    /* создаем стволы в руках */
    for (int i = 0; i < g_WeaponsCount; ++i)
        new TWeapon(&(g_Weapons[i]));

    // Сюда???
    if (map->showFog)
    {
        float dest = sqrt(sqr(MAP_WIDTH) + sqr(MAP_HEIGHT));
        wSceneSetFog(wUtilUIntToColor4s(map->fogColor), wFT_LINEAR, 0, dest - map->fogIntensity * dest, map->fogIntensity, true, false);
    }
}




/**
 * INTERPRETER
 */
JS_FUNCTION(js_mapIsPlaceFree)
{
    /* карта не загружена */
    if (g_NumOfCurMap == -1 || args_cnt < 2)
    {
        return jerry_create_boolean(0);
    }

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
    {
        return jerry_create_boolean(0);
    }

    int x = jerry_get_number_value(arg0);
    int y = jerry_get_number_value(arg1);

    /* если координаты запрошены неверные */
    if ((x < 0 || x >= MAP_WIDTH ) ||
        (y < 0 || y >= MAP_HEIGHT))
    {
        return jerry_create_boolean(0);
    }

    /* если место занято стеной */
    TMapElement el = mapPack.maps[g_NumOfCurMap].level[x][y];
    if (el > 0 && el < TEXTURES_MAX_COUNT)
    {
        return jerry_create_boolean(0);
    }

    /* свободно */
    return jerry_create_boolean(1);
}


void RegisterMapJSFunctions()
{
    JS_RegisterCFunction(&js_mapIsPlaceFree, "mapIsPlaceFree");
}
