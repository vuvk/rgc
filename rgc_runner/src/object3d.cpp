#include "WorldSim3D.h"
#include "object3d.h"

vector<TObject3D*> TObject3D::objects;


TObject3D::TObject3D(ScrResourceInMem* index) : TObject(index)
{
    TObject3D::objects.push_back(this);
}

TObject3D::~TObject3D()
{
    /* отключаем все коллизии */
    disableCollision();

    /* если содержит спрайт, то удалить его и проверить не установлен ли он как нода */
    if (billboard)
    {
        if (node == billboard)
            node = nullptr;
        wNodeDestroy(billboard);
        billboard = nullptr;
    }

    /* если содержит коробку, то удалить её и проверить не установлена ли она как нода */
    if (solidBox)
    {
        if (node == solidBox)
            node = nullptr;

        wMesh* mesh = wNodeGetMesh(this->solidBox);
        if (mesh != nullptr)
            wMeshDestroy(mesh);

        wNodeDestroy(solidBox);
        solidBox = nullptr;
    }

    if (this->node)
    {
        wNodeDestroy(this->node);
        node = nullptr;
    }

    /* удаляем ссылку на объект из списка всех 3д-объектов */
    if (TObject3D::objects.size() > 0)
    {
        for (vector<TObject3D*>::iterator it = TObject3D::objects.begin();
             it != TObject3D::objects.end();
             ++it)
        {
            if (*it == this)
            {
                TObject3D::objects.erase(it);
                break;
            }
        }
    }
}

/* установить видимость объекта */
void TObject3D::setVisibility(bool isVisible)
{
    TObject::setVisibility(isVisible);
    if (this->node)
        wNodeSetVisibility(this->node, isVisible);
}

void TObject3D::setFrame(int frame)
{
    TObject::setFrame(frame);

    if (node)
    {
        wTexture* txr = this->index->frames[this->curFrame];
        if (txr)
        {
            wMaterial* mat = wNodeGetMaterial(this->node, 0);
            wMaterialSetTexture(mat, 0, txr);
        }
    }
}

/* управление коллизионной коробкой объекта */
void TObject3D::enableCollision()
{
    if (isCollisionAdded)
        return;

    if (!physBody && solidBox)
    {
        wMesh* mesh = wNodeGetMesh(solidBox);
        if (mesh)
        {
            physBody = wCollisionCreateFromMesh(mesh, solidBox, 0);
        }
    }

    if (g_WorldCollider && physBody && node)
    {
        wCollisionGroupAddCollision(g_WorldCollider, physBody);
        wNodeSetParent(node, g_SolidObjects);
        wNodeSetUserData(node, &(this->id));

        isCollisionAdded = true;
    }
}

void TObject3D::disableCollision()
{
    if (!isCollisionAdded)
        return;

    if (g_WorldCollider && physBody && node)
    {
        wCollisionGroupRemoveCollision(g_WorldCollider, physBody);
        wNodeSetParent(node, wSceneGetRootNode());
        wNodeSetUserData(node, 0);

        isCollisionAdded = false;
    }
}

inline bool TObject3D::isEnabledCollision()
{
    return isCollisionAdded;
}


/* manipulations with object */
/* position */
void TObject3D::setPosition(wVector3f& pos)
{
    if (node) wNodeSetPosition(node, pos);
}
void TObject3D::setPosition(wVector2f& pos)
{
    if (node) wNodeSetPosition(node, {pos.x, pos.y, 0});
}
void TObject3D::setPositionX(float& x)
{
    if (node)
    {
        wVector3f pos = wNodeGetPosition(node);
        wNodeSetPosition(node, {x, pos.y, pos.z});
    }
}
void TObject3D::setPositionY(float& y)
{
    if (node)
    {
        wVector3f pos = wNodeGetPosition(node);
        wNodeSetPosition(node, {pos.x, y, pos.z});
    }
}
void TObject3D::setPositionZ(float& z)
{
    if (node)
    {
        wVector3f pos = wNodeGetPosition(node);
        wNodeSetPosition(node, {pos.x, pos.y, z});
    }
}
void TObject3D::getPosition(vector<double> &pos)
{
    if (node)
    {
        wVector3f vec = wNodeGetPosition(node);
        pos = { vec.x, vec.y, vec.z };
    }
    else
        pos = {numeric_limits<float>::max(), numeric_limits<float>::max(), numeric_limits<float>::max()};
}
float TObject3D::getPositionX()
{
    if (node)
        return wNodeGetPosition(node).x;
    return numeric_limits<float>::max();
}
float TObject3D::getPositionY()
{
    if (node)
        return wNodeGetPosition(node).y;
    return numeric_limits<float>::max();
}
float TObject3D::getPositionZ()
{
    if (node)
        return wNodeGetPosition(node).z;
    return numeric_limits<float>::max();
}
void TObject3D::move(wVector3f& moveVector)
{
    if (node) wNodeMove(node, moveVector);
}

/* rotation */
void TObject3D::setRotation(wVector3f& rot)
{
    if (node) wNodeSetRotation(node, rot);
}
void TObject3D::setRotationX(float& x)
{
    if (node)
    {
        wVector3f rot = wNodeGetRotation(node);
        wNodeSetRotation(node, {x, rot.y, rot.z});
    }
}
void TObject3D::setRotationY(float& y)
{
    if (node)
    {
        wVector3f rot = wNodeGetRotation(node);
        wNodeSetRotation(node, {rot.x, y, rot.z});
    }
}
void TObject3D::setRotationZ(float& z)
{
    if (node)
    {
        wVector3f rot = wNodeGetRotation(node);
        wNodeSetRotation(node, {rot.x, rot.y, z});
    }
}
void TObject3D::getRotation(vector<double> &rot)
{
    if (node)
    {
        wVector3f vec = wNodeGetRotation(node);
        rot = { vec.x, vec.y, vec.z };
    }
    else
        rot = {numeric_limits<float>::max(), numeric_limits<float>::max(), numeric_limits<float>::max()};
}
float TObject3D::getRotationX()
{
    if (node)
        return wNodeGetRotation(node).x;
    return numeric_limits<float>::max();
}
float TObject3D::getRotationY()
{
    if (node)
        return wNodeGetRotation(node).y;
    return numeric_limits<float>::max();
}
float TObject3D::getRotationZ()
{
    if (node)
        return wNodeGetRotation(node).z;
    return numeric_limits<float>::max();
}
void TObject3D::rotate(wVector3f& rot)
{
    if (node) wNodeTurn(node, rot);
}

/* size */
void TObject3D::getSize(vector<double> &size)
{
    if (node)
    {
        wVector3f min;
        wVector3f max;
        wNodeGetBoundingBox(node, &min, &max);

        size = { abs(max.x - min.x),
                 abs(max.y - min.y),
                 abs(max.z - min.z) };
    }
    else
        size = {numeric_limits<float>::max(), numeric_limits<float>::max(), numeric_limits<float>::max()};
}
float TObject3D::getSizeX()
{
    if (node)
    {
        wVector3f min, max;
        wNodeGetBoundingBox(node, &min, &max);

        return abs(max.x - min.x);
    }
    return numeric_limits<float>::max();
}
float TObject3D::getSizeY()
{
    if (node)
    {
        wVector3f min, max;
        wNodeGetBoundingBox(node, &min, &max);

        return abs(max.y - min.y);
    }
    return numeric_limits<float>::max();
}
float TObject3D::getSizeZ()
{
    if (node)
    {
        wVector3f min, max;
        wNodeGetBoundingBox(node, &min, &max);

        return abs(max.z - min.z);
    }
    return numeric_limits<float>::max();
}

/* scale */
void TObject3D::setScale(wVector3f& scale)
{
    if (node)
    {
        wNodeSetScale(node, scale);
        uint32_t iterator = 0;
        wNode* child = wNodeGetFirstChild(node, &iterator);
        while (child)
        {
            wNodeSetScale(child, scale);
            child = wNodeGetNextChild(node, &iterator);
        }
    }
}
void TObject3D::setScale(wVector2f& scale)
{
    wVector3f scale3f = {scale.x, scale.y, 1};
    setScale(scale3f);
}
void TObject3D::setScaleX(float& x)
{
    if (node)
    {
        wVector3f scale = wNodeGetScale(node);
        scale.x = x;
        setScale(scale);
    }
}
void TObject3D::setScaleY(float& y)
{
    if (node)
    {
        wVector3f scale = wNodeGetScale(node);
        scale.y = y;
        setScale(scale);
    }
}
void TObject3D::setScaleZ(float& z)
{
    if (node)
    {
        wVector3f scale = wNodeGetScale(node);
        scale.z = z;
        setScale(scale);
    }
}
void TObject3D::getScale(vector<double> &scale)
{
    if (node)
    {
        wVector3f vec = wNodeGetScale(node);
        scale = { vec.x, vec.y, vec.z };
    }
    else
        scale = {numeric_limits<float>::max(), numeric_limits<float>::max(), numeric_limits<float>::max()};
}
float TObject3D::getScaleX()
{
    if (node)
        return wNodeGetScale(node).x;
    return numeric_limits<float>::max();
}
float TObject3D::getScaleY()
{
    if (node)
        return wNodeGetScale(node).y;
    return numeric_limits<float>::max();
}
float TObject3D::getScaleZ()
{
    if (node)
        return wNodeGetScale(node).z;
    return numeric_limits<float>::max();
}
