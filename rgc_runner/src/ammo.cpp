
#include "ammo.h"
#include "res_manager.h"
#include "file_system.h"
#include "sound.h"

using namespace FileSystem;

vector<TAmmoInMem> g_Ammo;
int TAmmoNode::count = 0;

/*
 * TKeyNode
 * */
TAmmoNode::TAmmoNode (TAmmoInMem* index) : TBillboard(index)
{
    addVar("type", "ammo");                      // имя типа объекта
    if (index)
    {
        addVar("volume",      index->volume);
        addVar("pickUpSound", index->pickUpSound);  // имя звука подбора

        /* имя типа патронов по id */
        if (index->ammoTypeId != -1 &&
            index->ammoTypeId < g_AmmoTypes.size())
        {
            addVar("ammoType", g_AmmoTypes.at(index->ammoTypeId));
        }
    }
    ++count;
}

// IMPLEMENTATION

void InitAmmo()
{
    if (g_Ammo.capacity() != AMMO_MAX_COUNT)
        g_Ammo.reserve(AMMO_MAX_COUNT);

    if (g_Ammo.size() != 0)
        g_Ammo.resize(0);
/*
    for (auto& ammo : g_Ammo)
    {
        ammo.clear();
    }
*/
}

void LoadAmmoTypes()
{
    int count;
    char*    buffer = nullptr;
    uint64_t bufferSize;

    g_AmmoTypes.clear();

    // если внешнего файла нет или не получилось загрузить,
    // то загрузить из дефолтных значений
    File::readFull("ammo_types.cfg", &buffer, &bufferSize);
    if ((buffer == nullptr) ||
        (bufferSize == 0) ||
        !Res_OpenIniFromBuffer(buffer, bufferSize))
    {
        for (std::string &type : g_DefAmmoTypes)
            g_AmmoTypes.push_back(type);
        goto end;
    }
    else
    {
        count = Res_IniReadInteger("Options", "count", 0);
        for (int i = 0; i < count; ++i)
        {
            std::string type = "ammo_type_" + std::to_string(i);
            g_AmmoTypes.push_back(Res_IniReadString("Types", type.c_str(), ""));
        }
    }

end:
    free(buffer);
    Res_CloseIni();
}

bool LoadAmmoPack()
{
    bool    isLoaded = false;

    char*    buffer = nullptr;
    uint64_t bufferSize;
    string   fileName;

    int     i;
    int     volume;
    int     ammoTypeId;
    int     pickUpSoundId;

    string   name;
    double   solidX, solidY;
    uint8_t  animSpeed;
    uint32_t framesCount;

    // читаем количество текстур
    File::readFull("ammo.cfg", &buffer, &bufferSize);
    if ((buffer == nullptr) ||
        (bufferSize == 0) ||
        !Res_OpenIniFromBuffer(buffer, bufferSize))
    {
        PrintWithColor("can not open ammo.cfg!!!\n");
        goto end;
    }
    g_AmmoCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_AmmoCount == 0)
        goto end;

    InitAmmo();

    for (i = 0; i < g_AmmoCount; ++i)
    {
        // читаем инфу по текстуре
        fileName = "ammo_" + to_string(i) + ".cfg\0";
        File::readFull(fileName, &buffer, &bufferSize);
        if (bufferSize <= 0)
        {
            PrintWithColor((char*)("can not get size of ammo_" + to_string(i) + ".cfg").c_str());
            continue;
        }

        if (i >= g_Ammo.size())
            g_Ammo.resize(g_Ammo.size() + 1);
        TAmmoInMem& ammo = g_Ammo.at(i);

        Res_OpenIniFromBuffer(buffer, bufferSize);

        name        = Res_IniReadString("Options", "name", ("Ammo #" + to_string(i) + "\0").c_str());
        volume      = Res_IniReadInteger("Options", "volume", 0);
        ammoTypeId  = Res_IniReadInteger("Options", "ammo_type_id", -1);
        pickUpSoundId = Res_IniReadInteger("Options", "pickup_sound_id", -1);
        solidX      = Res_IniReadDouble("Options", "solid_x", 100.0);
        solidY      = Res_IniReadDouble("Options", "solid_y", 100.0);
        animSpeed   = (uint8_t)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = (uint8_t)Res_IniReadInteger("Options", "frames_count",    0);
        Res_CloseIni();

        ammo.name      = name.c_str();
        ammo.posInRep  = i;
        ammo.volume    = volume;
        ammo.ammoTypeId = ammoTypeId;
        ammo.solidX    = solidX / 100.0;
        ammo.solidY    = solidY / 100.0;
        ammo.animSpeed = animSpeed;
        ammo.script.loadFromFile("ammo.js");
        /* ищем имя звука по id */
        if (pickUpSoundId != -1 && pickUpSoundId < g_SoundBuffers.size())
            for (auto snd : g_SoundBuffers)
                if (snd.second->id == pickUpSoundId)
                {
                    ammo.pickUpSound = snd.first;
                    break;
                }

        if (framesCount == 0 || !g_AmmoNeedForLoad[i])
            continue;

        // читаем кадры, если есть
        ammo.loadFrames();
    }
    isLoaded = true;

end:
    free(buffer);
    Res_CloseIni();

    return isLoaded;
}

