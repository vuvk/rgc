library res_manager;

{$mode objfpc}{$H+}

uses
  Classes,
  SysUtils,
  IniFiles,
  zip,
  unzip
  { you can add units after this };

const
  RES_TRUE  = 1;
  RES_FALSE = 0;      
  RES_ERROR = -1;
  RES_NOT_FOUND  = -2;
  RES_NOT_OPENED = -3;   
  RES_NOT_READABLE = -4;
  RES_NOT_WRITABLE = -5;


var
  zipper   : Pointer = nil;
  unzipper : Pointer = nil;
  iniFile  : TIniFile = nil;
  iniStream : TMemoryStream = nil;



function Res_OpenZipForRead(const zipName : PChar) : Int32; cdecl;
begin
  Result := RES_TRUE;

  if (unzipper <> nil) then
    begin
      unzClose(unzipper);
      unzipper := nil;
    end;

  if (not FileExists(zipName)) then    
    begin
      //WriteLn('"', zipName, '" not found!');
      Result := RES_NOT_FOUND;
      exit;
    end;

  unzipper := unzOpen(zipName);
  if (unzipper = nil) then
    begin
      //WriteLn('could not open "', zipName, '"!');
      Result := RES_NOT_OPENED;
      exit;
    end;
end;
                      
function Res_GetCompressedFileSize(const fileName : PChar) : Int32; cdecl;
var
  file_info : unz_file_info;
begin
  Result := RES_ERROR;

  if (unzipper = nil) then exit;

  if (unzLocateFile(unzipper, fileName, 2) <> UNZ_OK) then
    begin
      //WriteLn('"', fileName, '" not found!');
      Result := RES_NOT_FOUND;
      exit;
    end;

  unzGetCurrentFileInfo(unzipper, @file_info, nil, 0, nil, 0, nil, 0);

  Result := file_info.compressed_size;
end;

function Res_GetUncompressedFileSize(const fileName : PChar) : Int32; cdecl;
var
  file_info : unz_file_info;
begin
  Result := RES_ERROR;

  if (unzipper = nil) then exit;

  if (unzLocateFile(unzipper, fileName, 2) <> UNZ_OK) then
    begin
      //WriteLn('"', fileName, '" not found!');
      Result := RES_NOT_FOUND;
      exit;
    end;

  unzGetCurrentFileInfo(unzipper, @file_info, nil, 0, nil, 0, nil, 0);

  Result := file_info.uncompressed_size;
end;

function Res_ReadFileFromZipToBuffer(const fileName : PChar; buffer : Pointer; bufferSize : Int32) : Int32; cdecl;
var
  len : Integer;
begin
  Result := RES_ERROR;

  if ((unzipper = nil) or (buffer = nil)) then exit;

  if (unzLocateFile(unzipper, fileName, 2) <> UNZ_OK) then
    begin
      //WriteLn('"', fileName, '" not found!');
      Result := RES_NOT_FOUND;
      exit;
    end;

  if (unzOpenCurrentFile(unzipper) <> UNZ_OK) then
    begin
      //WriteLn('can not open "', fileName, '"');
      Result := RES_NOT_OPENED;
      exit;
    end;

  len := unzReadCurrentFile(unzipper, buffer, bufferSize);
  //Writeln('readed size - ', len);
  //if (len <> bufferSize) then
  //  WriteLn('can not read "', fileName, '"');

  unzCloseCurrentFile(unzipper);

  if (len < 0) then
    Result := RES_NOT_READABLE
  else
    Result := len;
end;

function Res_CloseZipAfterRead() : Boolean; cdecl;
var
  res : Int32 = UNZ_OK;
begin
  if (unzipper <> nil) then
    begin
      res := unzClose(unzipper);
      unzipper := nil;
    end;                  

  Result := (res = UNZ_OK);
end;

function Res_OpenZipForWrite(const zipName : PChar; isAppend : Boolean = False) : Boolean; cdecl;
begin
  Result := False;

  if (zipper <> nil) then
    begin
      zipClose(zipper, nil);
      zipper := nil;
    end;

  if ((not isAppend) and (FileExists(zipName))) then
    DeleteFile(zipName);

  zipper := zipOpen(zipName, Integer(isAppend));

  Result := (zipper <> nil);
end;

function Res_WriteFileFromBufferToZip(fileName : PChar; const buffer : Pointer; bufferSize : UInt32; compressionLevel : Int32) : Int32; cdecl;
var
  res : Int32;
begin
  Result := RES_ERROR;

  if (zipper = nil) then exit;

  zipOpenNewFileInZip(zipper, fileName, nil, nil, 0, nil, 0, nil, Z_DEFLATED, compressionLevel);
  res := zipWriteInFileInZip(zipper, buffer, bufferSize);
  zipCloseFileInZip(zipper);

  if (res <> ZIP_OK) then
    //WriteLn('error when writing "', fileName, '"');
    Result := RES_NOT_WRITABLE
  else
    Result := RES_TRUE;
end;

function Res_CloseZipAfterWrite() : Boolean; cdecl;
var
  res : Int32 = ZIP_OK;
begin
  if (zipper <> nil) then
    begin
      res := zipClose(zipper, nil);
      zipper := nil;
    end;

  Result := (res = ZIP_OK);
end;

function Res_PackAllFilesFromDirToZip(path : PChar;{ zipName : PChar;} compressionLevel : Int32) : Boolean; cdecl;
var
  i : Integer;
  res : Boolean = False;         // результат работы функции
  buffer : Pointer = nil;        // буффер для записи в архив
  bufferSize : Integer;
  wroteSize : Integer = 0;       // записано байт
  fullPath : AnsiString;         // полный путь до папки, откуда брать файлы
  fHandle : THandle;
  searchRec : TSearchRec;
Label
  end_func;
begin   
  WriteLn('directory is exists?...');
  if (not DirectoryExists(path)) then
    goto end_func;

  {WriteLn('file is exists?...');
  if (FileExists(zipName)) then
    DeleteFile(zipName);

  WriteLn('opening for write zip...');
  if (zipper <> nil) then
    zipClose(zipper, nil);
  zipper := zipOpen(zipName, 0);
  if (zipper = nil) then
    goto end_func; }

  if (zipper = nil) then
    goto end_func;

  // преобразуем переданный путь в UNIX-стайл
  fullPath := AnsiString(path);
  if ((fullPath[Length(fullPath)] <> '\') and
      (fullPath[Length(fullPath)] <> '/')) then
    fullPath += '/';
  for i := 1 to Length(fullPath) do
    if (fullPath[i] = '\') then
      fullPath[i] := '/';

  // Искать все файлы в заданной директории
  // Если хотя бы один файл найден, то продолжить поиск
  if (FindFirst(fullPath + '*.*', faAnyFile - faDirectory, searchRec) = 0) then
    repeat             
      WriteLn('opening for read - ', fullPath + searchRec.Name);
      fHandle := FileOpen(fullPath + searchRec.Name, fmOpenRead);
      if (fHandle = 0) then
        goto end_func;

      bufferSize := searchRec.Size;
      buffer := GetMem(bufferSize);

      wroteSize := FileRead(fHandle, PByte(buffer)^, bufferSize);
      WriteLn('read bytes - ', wroteSize, '; buffer size - ', bufferSize);
      if (wroteSize <> bufferSize) then
        goto end_func;   
      FileClose(fHandle);   
      fHandle := 0;

      WriteLn('opening file in zip...');
      zipOpenNewFileInZip(zipper, PChar(searchRec.Name), nil, nil, 0, nil, 0, nil, Z_DEFLATED, compressionLevel); 
      WriteLn('writing file in zip...');
      if (zipWriteInFileInZip(zipper, buffer, bufferSize) <> ZIP_OK) then
        goto end_func;
      zipCloseFileInZip(zipper);
      WriteLn('OK.');

      FreeMemAndNil(buffer);
    until (FindNext(searchRec) <> 0);

  res := True;

end_func: 
  // Должен освободить ресурсы, используемые этими успешными, поисками
  FindClose(searchRec);
  FreeMemAndNil(buffer);
  FileClose(fHandle);
  {if (zipper <> nil) then
    begin
      zipClose(zipper, nil);
      zipper := nil;
    end;    }
  Result := res;
end;

function Res_UnpackAllFilesFromZipToDir(zipName : PChar; path : PChar; rewrite : Boolean) : Boolean; cdecl;
var
  i : Integer;
  res : Boolean = False;         // результат работы функции
  file_info : unz_file_info;     // инфо о файле в архиве
  buffer : Pointer = nil;        // буффер для чтения из архива
  readedSize : Integer = 0;      // прочитано из архива байт
  wroteSize : Integer = 0;       // записано в файл байт
  fullPath : AnsiString;         // полный путь до папки, куда скидывать файлы
  fileName : PChar = nil;        // считанное имя файла из архива
  fullFileName : AnsiString;     // полное имя с путем файла, куда будет произведена запись
  fHandle : THandle = 0;  
Label
  end_func;
begin         
  WriteLn('directory is exists?...');
  if (not DirectoryExists(path)) then
    goto end_func;

  WriteLn('file is exists?...');
  if (not FileExists(zipName)) then
    goto end_func;

  WriteLn('opening for read zip...');
  //if (Res_OpenZipForRead(zipName) <> RES_TRUE) then
  //  goto end_func;
  if (unzipper <> nil) then
    unzClose(unzipper);
  unzipper := unzOpen(zipName);
  if (unzipper = nil) then
    goto end_func;
                         
  WriteLn('find first file in zip...');
  if (unzGoToFirstFile(unzipper) <> UNZ_OK) then
    goto end_func;


  // преобразуем переданный путь в UNIX-стайл
  fullPath := AnsiString(path);
  if ((fullPath[Length(fullPath)] <> '\') and
      (fullPath[Length(fullPath)] <> '/')) then
    fullPath += '/';
  for i := 1 to Length(fullPath) do
    if (fullPath[i] = '\') then
      fullPath[i] := '/';

  fileName := GetMem(256);

  WriteLn('start unpacking...');
  repeat
    unzGetCurrentFileInfo(unzipper, @file_info, fileName, 256, nil, 0, nil, 0);

    fullFileName := fullPath + fileName;
    WriteLn('unpack - ', fullFileName);
    if (FileExists(fullFileName)) then
      begin
        if (not rewrite) then continue;

        DeleteFile(fullFileName);
      end;

    WriteLn('prepare buffer...');

    buffer := GetMem(file_info.uncompressed_size);
    if (buffer = nil) then
      goto end_func;
    
    WriteLn('openfile in archive - ', fileName);
    if (unzOpenCurrentFile(unzipper) <> UNZ_OK) then
      goto end_func;

    WriteLn('start reading to buffer...');

    readedSize := unzReadCurrentFile(unzipper, buffer, file_info.uncompressed_size); 
    WriteLn('file size - ', file_info.uncompressed_size, '; readed - ', readedSize);
    if (readedSize <> file_info.uncompressed_size) then
      goto end_func;

    WriteLn('open file for write...');

    fHandle := FileCreate(fullFileName);
    if (fHandle = 0) then
      goto end_func;

    WriteLn('writing to file...');

    FileSeek(fHandle, 0, fsFromBeginning);
    wroteSize := FileWrite(fHandle, PByte(buffer)^, readedSize);    
    WriteLn('file size - ', file_info.uncompressed_size, '; wrote - ', wroteSize);
    if (wroteSize <> readedSize) then
      goto end_func;
    FileClose(fHandle);
    fHandle := 0;                     

    WriteLn('OK.');

    unzCloseCurrentFile(unzipper);
    FreeMemAndNil(buffer);
  until (unzGoToNextFile(unzipper) = UNZ_END_OF_LIST_OF_FILE);

  res := True;

end_func:
  FreeMemAndNil(buffer);  
  FreeMemAndNil(fileName);
  FileClose(fHandle);
  if (unzipper <> nil) then
    begin
      unzClose(unzipper);
      unzipper := nil;
    end;
  Result := res;
end;
         
function Res_ReadFileToBuffer(var buffer : Pointer; var bufferSize : Integer; fileName : PChar) : Boolean; cdecl;
var
  res : Integer = 0;
  stream : TMemoryStream = nil;
begin
  Result := False;

  if (not FileExists(fileName)) then exit;

  stream := TMemoryStream.Create;
  if (stream = nil) then exit;

  stream.LoadFromFile(fileName);
  bufferSize := stream.Size;

  if (buffer <> nil) then
    Freemem(buffer);
  buffer := GetMem(bufferSize);
  if (buffer = nil) then
    begin
      stream.Destroy;
      exit;
    end;

  stream.Position := 0;
  res := stream.Read(PByte(buffer)^, bufferSize);
  stream.Destroy;

  Result := (res = bufferSize);
end;

function Res_WriteBufferToFile(buffer : Pointer; bufferSize : Integer; fileName : PChar; rewrite : Boolean) : Boolean; cdecl;
var
  fHandle : THandle = 0;
  res : Integer = 0;
begin
  Result := False;

  if (buffer = nil) then exit;
  if (FileExists(fileName)) then
    begin
      if (not rewrite) then
        exit
      else
        DeleteFile(fileName);
    end;

  fHandle := FileCreate(fileName);
  if (fHandle = 0) then exit;
  res := FileWrite(fHandle, PByte(buffer)^, bufferSize);
  FileClose(fHandle);

  Result := (res = bufferSize);
end;

procedure Res_DeleteAllFilesInDir(dirName : PChar; deleteDir: Boolean); cdecl;
var
  searchRec : TSearchRec;
  path : AnsiString;
  i : Integer;
begin
  // преобразуем переданный путь в UNIX-стайл
  path := AnsiString(dirName);
  if ((path[Length(path)] <> '\') and
      (path[Length(path)] <> '/')) then
    path += '/';
  for i := 1 to Length(path) do
    if (path[i] = '\') then
      path[i] := '/';

  Writeln('DELETE ALL IN ', path);

  // Искать все файлы в заданной директории
  // Если хотя бы один файл найден, то продолжить поиск
  if (FindFirst(path + '*.*', faAnyFile - faDirectory, searchRec) = 0) then
    repeat            
      Writeln('DELETE ALL IN ', path + searchRec.Name);
      DeleteFile(path + searchRec.Name);
    until (FindNext(searchRec) <> 0);

  // Должен освободить ресурсы, используемые этими успешными, поисками
  FindClose(searchRec);

  if (deleteDir) then
    begin
      Writeln('DELETE FOLDER ', dirName);
      RemoveDir(dirName);
    end;
end;


{
    INI FILES
}
                           
function Res_OpenIniFromFile(const fileName : PChar) : Boolean; cdecl;
begin
  Result := False;

  if (iniFile <> nil) then
    begin
      iniFile.Destroy;
      iniFile := nil;
    end;  
  if (iniStream <> nil) then
    begin
      iniStream.Destroy;
      iniStream := nil;
    end;

  if (not FileExists(fileName)) then exit;

  iniStream := TMemoryStream.Create;
  iniStream.LoadFromFile(fileName);
  iniStream.Position := 0;

  iniFile := TIniFile.Create(iniStream, [ifoStripComments, ifoStripInvalid, ifoCaseSensitive]);

  iniStream.Position := 0;

  Result := ((iniFile <> nil) and (iniStream <> nil));
end;

function Res_OpenIniFromBuffer(buffer : Pointer; bufferSize : UInt32) : Boolean; cdecl;
begin
  if (iniFile <> nil) then
    begin
      iniFile.Destroy;
      iniFile := nil;
    end;
  if (iniStream <> nil) then
    begin
      iniStream.Destroy;
      iniStream := nil;
    end;

  iniStream := TMemoryStream.Create;
  iniStream.Write(PByte(buffer)^, bufferSize);
  iniStream.Position := 0;

  iniFile := TIniFile.Create(iniStream, [ifoStripComments, ifoStripInvalid, ifoCaseSensitive]);

  Result := ((iniFile <> nil) and (iniStream <> nil));
end;

{ READING INI }
function Res_IniReadString(const section, key, defaultValue : PChar) : PChar; cdecl;
begin
  Result := defaultValue;

  if (iniFile = nil) then
    exit;

  Result := PChar(iniFile.ReadString(section, key, defaultValue));
end;

function Res_IniReadInteger(const section, key : PChar; defaultValue : Int32) : Int32; cdecl;
begin
  Result := defaultValue;

  if (iniFile = nil) then
    exit;

  Result := iniFile.ReadInteger(section, key, defaultValue);
end;  

function Res_IniReadInteger64(const section, key : PChar; defaultValue : Int64) : Int64; cdecl;
begin
  Result := defaultValue;

  if (iniFile = nil) then
    exit;

  Result := iniFile.ReadInt64(section, key, defaultValue);
end;                                              

function Res_IniReadBool(const section, key : PChar; defaultValue : Boolean) : Boolean; cdecl;
begin
  Result := defaultValue;

  if (iniFile = nil) then
    exit;

  Result := iniFile.ReadBool(section, key, defaultValue);
end;

function Res_IniReadDouble(const section, key : PChar; defaultValue : Double) : Double; cdecl;
begin
  Result := defaultValue;

  if (iniFile = nil) then
    exit;

  Result := iniFile.ReadFloat(section, key, defaultValue);
end;

          
{ WRITING INI }
procedure Res_IniWriteString(const section, key, value : PChar); cdecl;
begin
  if (iniFile = nil) then
    exit;

  iniFile.WriteString(section, key, value);
end;

procedure Res_IniWriteInteger(const section, key : PChar; value : Int32); cdecl;
begin
  if (iniFile = nil) then
    exit;

  iniFile.WriteInteger(section, key, value);
end;

procedure Res_IniWriteInteger64(const section, key : PChar; value : Int64); cdecl;
begin
  if (iniFile = nil) then
    exit;

  iniFile.WriteInt64(section, key, value);
end;

procedure Res_IniWriteBool(const section, key : PChar; value : Boolean); cdecl;
begin
  if (iniFile = nil) then
    exit;

  iniFile.WriteBool(section, key, value);
end;

procedure Res_IniWriteDouble(const section, key : PChar; value : Double); cdecl;
begin
  if (iniFile = nil) then
    exit;

  iniFile.WriteFloat(section, key, value);
end;     

function Res_SaveIniToBuffer(var buffer : Pointer; var bufferSize : Int32) : Boolean; cdecl;
begin
  Result := False;

  if ((iniFile = nil) or (iniStream = nil)) then
    exit;

  if (buffer <> nil) then
    Freemem(buffer);
                               
  bufferSize := iniStream.Size;
  buffer := GetMem(bufferSize);
  FillChar(PByte(buffer)^, bufferSize, 0);

  iniStream.Position := 0;
  iniStream.Read(PByte(buffer)^, bufferSize);

  Result := True;
end;     

function Res_SaveIniToFile(const fileName : PChar) : Boolean; cdecl;
begin
  Result := False;

  if ((iniFile = nil) or (iniStream = nil)) then exit;

  iniStream.Position := 0;
  iniStream.SaveToFile(fileName);
  iniStream.Position := 0;

  Result := (FileExists(fileName));
end;

procedure Res_CloseIni(); cdecl;
begin
  if (iniStream <> nil) then
    begin
      iniStream.Destroy;
      iniStream := nil;
    end;
         
  if (iniFile <> nil) then
    begin
      iniFile.Destroy;
      iniFile := nil;
    end;
end;


exports
  Res_OpenZipForRead,
  Res_GetCompressedFileSize,
  Res_GetUncompressedFileSize,
  Res_ReadFileFromZipToBuffer,
  Res_CloseZipAfterRead,
  Res_OpenZipForWrite,
  Res_WriteFileFromBufferToZip,
  Res_CloseZipAfterWrite,
  Res_PackAllFilesFromDirToZip,
  Res_UnpackAllFilesFromZipToDir,

  Res_ReadFileToBuffer,
  Res_WriteBufferToFile,
  Res_DeleteAllFilesInDir,

  Res_OpenIniFromFile,
  Res_OpenIniFromBuffer,
  Res_IniReadString,
  Res_IniReadInteger,
  Res_IniReadInteger64,
  Res_IniReadBool,
  Res_IniReadDouble,
  Res_IniWriteString,
  Res_IniWriteInteger,
  Res_IniWriteInteger64,
  Res_IniWriteBool,
  Res_IniWriteDouble,
  Res_SaveIniToBuffer,
  Res_SaveIniToFile,
  Res_CloseIni
  ;
end.

