# RGC - Raycasting Game Constructor 
This is my first try to create oldschool fps game creator. 
But it is dead. Now project is not maintained.

Well, you can use these attempts in your projects. You can find here:

  - embedding a script interpreter (JS, Pawn, PascalScript);
  
  - a library for working with zip (Pascal with a C invocation declaration);
  
  - runtime loading of dynamic libraries depending on the running OS.
  
  - And other...

In this project I use my audio system - https://github.com/vuvk/audio_system 


Watch videos:

https://www.youtube.com/watch?v=oPgV-5MiM3Y

https://youtu.be/oiF8F2oQip4

https://youtu.be/jbhOwZsWyjA
