a = 1.0;
b = 666.0;
function test() 
{
	for (var i = 1; i <= 1000000; i++) 
		a *= i;
	return a / b;
}
 
for (var j = 0; j < 10; j++)
{
	a = 1.0;
	c = test();
}

print('a = ' + a + ' c = ' + c);

delete a;
delete b;
delete c;

Duktape.gc();
