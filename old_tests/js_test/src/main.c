#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <time.h>
#include <windows.h>

#include "duktape.h"
#include "duk_util.h"

char* HelloDll();


duk_ret_t native_wait_enter(duk_context* ctx)
{
    printf("\n\nPress ENTER to continue...\n");
    getchar();
    return 0;  /* no return value (= undefined) */
}

duk_ret_t native_get_pos_x(duk_context* ctx)
{
    duk_push_number(ctx, 1.0f);
    return 1;
}

duk_ret_t native_get_pos_y(duk_context* ctx)
{
    duk_push_number(ctx, 2.0f);
    return 1;
}

void RunScript()
{
    double a;
    double b;
    double c;
    for (int j = 0; j < 100; ++j)
    {
        a = 1.0;
        b = 666.0;
        for (int i = 1; i <= 1000000; ++i)
            a *= i;
        c = a / b;
    }

    printf("a = %f, c = %f\n", a, c);
}

float a;
float b = 666.0f;
float c;
int i;

float test()
{
    for (i = 1; i <= 1000000; ++i)
        a *= i;
    return a / b;
}

void RunScript2()
{
    int j;
    for (j = 0; j < 100; ++j)
    {
        a = 1.0f;
        c = test();
    }

    printf("a = %f, c = %f\n", a, c);
}

int main(int argc, char** args)
{
    Duk_InitVM();

    Duk_RegisterCFunction(native_wait_enter, 0, "wait_enter");
    Duk_RegisterCFunction(native_get_pos_x, 1, "player_get_pos_x");
    Duk_RegisterCFunction(native_get_pos_y, 1, "player_get_pos_y");

    /*FILE* file = fopen("big.js", "r");
    fseek(file, 0, SEEK_END);
    int len = ftell(file);
    fseek(file, 0, SEEK_SET);
    char script[len];
    memset(script, 0, len);
    fread(script, 1, len, file);
    fclose(file);
    file = NULL;
    printf(script);

    file = fopen("small.js", "r");
    fseek(file, 0, SEEK_END);
    len = ftell(file);
    fseek(file, 0, SEEK_SET);
    char script2[len];
    memset(script2, 0, len);
    fread(script2, 1, len, file);
    fclose(file);
    file = NULL;
    printf(script2);

    time_t before, after;

    printf("\n\nrun first...");
    before = time(NULL);
    Duk_RunScript(script );
    after = time(NULL);
    int test_1 = after - before;
    printf("done.\n");

    printf("run second...");
    before = time(NULL);
    Duk_RunScript(script2);
    after = time(NULL);
    int test_2 = after - before;
    printf("done.\n");

    printf("run native...");
    before = time(NULL);
    RunScript();
    after = time(NULL);
    int test_3 = after - before;
    printf("done.\n");

    printf("run native fast...");
    before = time(NULL);
    RunScript2();
    after = time(NULL);
    int test_4 = after - before;
    printf("done.\n");

    printf("time for first script is %d\n",  test_1);
    printf("time for second script is %d\n", test_2);
    printf("time for native is %d\n", test_3);
    printf("time for fast native is %d\n", test_4);*/

    Duk_StopVM();

    printf(HelloDll());

    return 0;
}
