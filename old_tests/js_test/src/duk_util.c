#undef __STRICT_ANSI__
#include <math.h>

#include "duk_util.h"


#define DUK_CHECK_NARGS_EQUAL(n)    if (duk_get_top(ctx) == n) return DUK_RET_TYPE_ERROR;
#define DUK_CHECK_NARGS_GREATER(n)  if (duk_get_top(ctx) >  n) return DUK_RET_TYPE_ERROR;
#define DUK_CHECK_NARGS_SMALLER(n)  if (duk_get_top(ctx) <  n) return DUK_RET_TYPE_ERROR;
#define DUK_REGISTER_C_FUNCTION(native_function, nargs, new_name) duk_push_c_function(ctx, native_function, nargs); duk_put_global_string(ctx, new_name);


static duk_context* ctx = NULL;
static const double DEG_TO_RAD_COEFF = M_PI / 180.0;
static const double RAD_TO_DEG_COEFF = 180.0 / M_PI;


static duk_ret_t native_gc(duk_context* ctx)
{
    duk_gc(ctx, DUK_GC_COMPACT);
    duk_push_boolean(ctx, 1);
    return 1;
}

/*
static duk_ret_t native_pi(duk_context* ctx)
{
    duk_push_number(ctx, M_PI);
    return 1;
}

static duk_ret_t native_pi_2(duk_context* ctx)
{
    duk_push_number(ctx, M_PI_2);
    return 1;
}

static duk_ret_t native_pi_4(duk_context* ctx)
{
    duk_push_number(ctx, M_PI_4);
    return 1;
}
*/

static duk_ret_t native_deg_to_rad(duk_context* ctx)
{
    DUK_CHECK_NARGS_EQUAL(0);
    DUK_CHECK_NARGS_GREATER(1);

    double deg = duk_to_number(ctx, 0);
    duk_push_number(ctx, deg*DEG_TO_RAD_COEFF);
    return 1;
}

static duk_ret_t native_rad_to_deg(duk_context* ctx)
{
    DUK_CHECK_NARGS_EQUAL(0);
    DUK_CHECK_NARGS_GREATER(1);

    double rad = duk_to_number(ctx, 0);
    duk_push_number(ctx, rad*RAD_TO_DEG_COEFF);
    return 1;
}

static duk_ret_t native_print(duk_context *ctx)
{
	duk_push_string(ctx, " ");
	duk_insert(ctx, 0);
	duk_join(ctx, duk_get_top(ctx) - 1);
	printf("%s\n", duk_safe_to_string(ctx, -1));
	return 0;
}

void Duk_InitVM()
{
    ctx = duk_create_heap_default();

    /*
    DUK_REGISTER_C_FUNCTION(native_pi, 0, "pi");
    DUK_REGISTER_C_FUNCTION(native_pi_2, 0, "pi_2");
    DUK_REGISTER_C_FUNCTION(native_pi_4, 0, "pi_4");
    */
    DUK_REGISTER_C_FUNCTION(native_gc, 0, "GC");

    DUK_REGISTER_C_FUNCTION(native_deg_to_rad, 1, "deg_to_rad");
    DUK_REGISTER_C_FUNCTION(native_rad_to_deg, 1, "rad_to_deg");

    DUK_REGISTER_C_FUNCTION(native_print, DUK_VARARGS, "print");
}

void Duk_StopVM()
{
    duk_destroy_heap(ctx);
    ctx = NULL;
}

void Duk_GC()
{
    duk_gc(ctx, DUK_GC_COMPACT);
}

void Duk_RegisterCFunction(duk_c_function func, duk_idx_t nargs, const char* new_name)
{
    DUK_REGISTER_C_FUNCTION(func, nargs, new_name);
}

void Duk_RunScript(const char* script)
{
    if (duk_peval_string(ctx, script) != 0)
		printf("Error: %s\n", duk_safe_to_string(ctx, -1));
}
