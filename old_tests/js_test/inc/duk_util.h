#pragma once
#ifndef __DUK_UTIL_H_INCLUDED
#define __DUK_UTIL_H_INCLUDED

#include "duktape.h"

void Duk_InitVM();
void Duk_StopVM();
void Duk_GC();
void Duk_RegisterCFunction(duk_c_function func, duk_idx_t nargs, const char* new_name);
void Duk_RunScript(const char* script);

#endif // __DUK_UTIL_H_INCLUDED
