#pragma once
#ifndef __INTERPRETER_H
#define __INTERPRETER_H

#include <stdint.h>

#define AMX_JIT
#define MAIN_PROGRAM "main.amx"

/* ������� ��������� ������� */
extern int32_t g_on_init_program_idx;
extern int32_t g_on_scene_begin_idx;
extern int32_t g_on_scene_draw_idx;
extern int32_t g_on_scene_end_idx;
extern int32_t g_on_keyboard_idx;

int InterpreterInitVM();
int InterpreterInitNatives();
int InterpreterCloseVM();

int InterpreterRunPublic(int32_t idx);


#endif // __INTERPRETER_H
