#include <string.h>

#include "interpreter.h"
#include "util.h"
#include "res_manager.h"
#include "amx/amx.h"
#include "amx/amxaux.h"
#include "WorldSim3D.h"
#include "player.h"

static AMX   amx = {};
static char* data = NULL;       // ������� ������ ��� ����������� ������
static char* program = NULL;    // ���������� ���������
static unsigned int programSize = 0;


int32_t g_on_init_program_idx = -1;
int32_t g_on_scene_begin_idx  = -1;
int32_t g_on_scene_draw_idx   = -1;
int32_t g_on_scene_end_idx    = -1;
int32_t g_on_keyboard_idx     = -1;


/** KEYBOARD */
static cell AMX_NATIVE_CALL n_keyboard_check(AMX* amx, const cell* params)
{
    bool result = wInputIsKeyHit((wKeyCode)params[1]);
    if (result)
        return 1;

    return 0;
}
static cell AMX_NATIVE_CALL n_keyboard_check_pressed(AMX* amx, const cell* params)
{
    bool result = wInputIsKeyPressed((wKeyCode)params[1]);
    if (result)
        return 1;

    return 0;

    //return (cell)wInputIsKeyPressed((wKeyCode)params[1]);
}
static cell AMX_NATIVE_CALL n_keyboard_check_released(AMX* amx, const cell* params)
{

    bool result = wInputIsKeyUp((wKeyCode)params[1]);
    if (result)
        return 1;

    return 0;

    //return (cell)wInputIsKeyUp((wKeyCode)params[1]);
}

const AMX_NATIVE_INFO input_Natives[] =
{
    { "keyboard_check",           n_keyboard_check         },
    { "keyboard_check_pressed",   n_keyboard_check_pressed },
    { "keyboard_check_released",  n_keyboard_check_released},
    {NULL,NULL}
    // terminator
};

/** MATH */
static cell AMX_NATIVE_CALL n_cos(AMX* amx, const cell* params)
{
    float num = cos(amx_ctof(params[1]));
    return amx_ftoc(num);
}
static cell AMX_NATIVE_CALL n_sin(AMX* amx, const cell* params)
{
    float num = sin(amx_ctof(params[1]));
    return amx_ftoc(num);
}

const AMX_NATIVE_INFO math_Natives[] =
{
    { "cos", n_cos },
    { "sin", n_sin },
    {NULL,NULL}
    // terminator
};


/** OBJECTS */
/** PLAYER*/
static cell AMX_NATIVE_CALL n_player_set_position(AMX* amx, const cell* params)
{
    wVector3f pos;
    pos.x = amx_ctof(params[1]);
    pos.y = amx_ctof(params[2]);
    pos.z = amx_ctof(params[3]);

    wNodeSetPosition(playerNode, pos);

    return 1;
}
static cell AMX_NATIVE_CALL n_player_get_position(AMX* amx, const cell* params)
{
    wVector3f pos = wNodeGetPosition(playerNode);

    cell* cp = amx_Address(amx, params[1]);

    cp[0] = amx_ftoc(pos.x);
    cp[1] = amx_ftoc(pos.y);
    cp[2] = amx_ftoc(pos.z);

    return 0;
}
cell AMX_NATIVE_CALL n_intersect(AMX *amx, const cell *params)
{
    /* get the (hidden) address for the return value */
    cell* cr3 = amx_Address(amx, params[1]);

    cr3[0] = 0;
    cr3[1] = 1;
    cr3[2] = 2;
    cr3[3] = 3;

    return 0; /* return value is irrelevant */
}

const AMX_NATIVE_INFO objects_Natives[] =
{
    { "intersect", n_intersect  },
    { "player_set_position", n_player_set_position },
    { "player_get_position", n_player_get_position },
    {NULL,NULL}
    // terminator
};

int InterpreterInitVM()
{
    AMX_HEADER hdr;
    int result;

    /* open the file, read and check the header */
    if (!FileExists(MAIN_PROGRAM))
        return AMX_ERR_NOTFOUND;

    if (program != NULL)
    {
        free(program);
        program = NULL;
    }

    Res_ReadFileToBuffer(&program, &programSize, MAIN_PROGRAM);

    memcpy(&hdr, program, sizeof(hdr));

    amx_Align16(&hdr.magic);
    amx_Align32((uint32_t*)&hdr.size);
    amx_Align32((uint32_t*)&hdr.stp);
    if (hdr.magic != AMX_MAGIC)
    {
        free(program);
        program = NULL;
        return AMX_ERR_FORMAT;
    }

    /* allocate the memblock if it is NULL */
    if ((data = malloc(hdr.stp)) == NULL)
    {
        free(program);
        program = NULL;
        return AMX_ERR_MEMORY;
    }

    /* after amx_Init(), amx->base points to the memory block */
    memcpy(data, program, (size_t)hdr.size);

    /* initialize the abstract machine */
    memset(&amx, 0, sizeof(AMX));
    result = amx_Init(&amx, data);

    /* free the memory block on error, if it was allocated here */
    if (result != AMX_ERR_NONE)
    {
        free(data);
        data = NULL;

        amx.base = NULL;                   /* avoid a double free */
    } /* if */

    free(program);
    program = NULL;

    return result;
}

static void InterpreterFindPublic(const char* funcName, int32_t* index)
{
    int err = amx_FindPublic(&amx, funcName, index);
    if (err != AMX_ERR_NONE)
    {
        printf("Error when finding public function %s - %d\n", funcName, err);
    }
}

int InterpreterInitNatives()
{
    if (data == NULL)
        return -1;

    int err;

    amx_ConsoleInit(&amx);

    err = amx_Register(&amx, input_Natives, -1);
    if (err)
    {
        printf("Input natives not registered! Error - %x \n", err);
        //return err;
    }

    err = amx_Register(&amx, math_Natives, -1);
    if (err)
    {
        printf("Math natives not registered! Error - %x \n", err);
        //return err;
    }

    err = amx_Register(&amx, objects_Natives, -1);
    if (err)
    {
        printf("Objects natives not registered! Error - %x \n", err);
        //return err;
    }


    /* ���������� ��������� ��������� ������� */
    InterpreterFindPublic("_on_init_program", &g_on_init_program_idx);
    InterpreterFindPublic("_on_scene_begin",  &g_on_scene_begin_idx);
    InterpreterFindPublic("_on_scene_draw",   &g_on_scene_draw_idx);
    InterpreterFindPublic("_on_scene_end",    &g_on_scene_end_idx);
    InterpreterFindPublic("_on_keyboard",     &g_on_keyboard_idx);

    return amx_Exec(&amx, NULL, -1);
}

int InterpreterCloseVM()
{
    aux_FreeProgram(&amx);
    data = NULL;
}

int InterpreterRunPublic(int32_t idx)
{
    cell retVal;
    /*
    cell index;
    int err;

    err = amx_FindPublic(&amx, funcName, &index);
    if (err != AMX_ERR_NONE)
    {
        printf("Error - %d\n", err);
    }
    */

    if (idx > 0)
        amx_Exec(&amx, NULL, idx);
    //amx_Release(&amx, &index);

    return retVal;
}
