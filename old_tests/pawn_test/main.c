#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "amx/amx.h"
#include "amx/amxaux.h"

static cell AMX_NATIVE_CALL n_print_int(AMX *amx, const cell *params)
{
    printf("integer - %d\n", (int)params[1]);
    return 0;
}

static cell AMX_NATIVE_CALL n_print_float(AMX *amx, const cell *params)
{
    printf("float - %f\n", amx_ctof(params[1]));
    return 0;
}

const AMX_NATIVE_INFO print_Natives[] =
{
    { "print_int",   n_print_int   },
    { "print_float", n_print_float },
    {NULL,NULL}
    /* terminator */
};

int main()
{
    AMX   amx;
    char* data = NULL;
    char* program1 = NULL;
    char* program2 = NULL;
    int   programSize;
    cell  ret = 0;
    int   err = 0;
    FILE* f = NULL;

    data = calloc(1024000, 1);


    f = fopen("scripts/test.amx", "rb");
    if (f == NULL)
    {
        printf("Program not loaded!\n");
        return 100;
    }

    fseek(f, 0, SEEK_END);
    programSize = ftell(f);
    fseek(f, 0, SEEK_SET);

    program1 = calloc(programSize, 1);
    fread(program1, programSize, 1, f);
    fclose(f);


    f = fopen("scripts/test2.amx", "rb");
    if (f == NULL)
    {
        printf("Program not loaded!\n");
        return 100;
    }

    fseek(f, 0, SEEK_END);
    programSize = ftell(f);
    fseek(f, 0, SEEK_SET);

    program2 = calloc(programSize, 1);
    fread(program2, programSize, 1, f);
    fclose(f);


/*
    err = aux_LoadProgram(&amx, "scripts/test.amx", data);
    if (err != AMX_ERR_NONE)
    {
        printf("Program not loaded!\n");
        return err;
    }

    amx_ConsoleInit(&amx);

    err = amx_Register(&amx, print_Natives, -1);
    if (err)
    {
        printf("Natives not registered! Error - %x \n", err);
        return err;
    }

    err = amx_Exec(&amx, &ret, AMX_EXEC_MAIN);
    if (err)
    {
        printf("Error when executing!\n");
        return err;
    }

    aux_FreeProgram(&amx);
*/


    memset(&amx, 0, sizeof(amx));
    amx.data = data;
    //amx.flags = AMX_FLAG_DSEG_INIT;
    err = amx_Init(&amx, program1);
    if (err != AMX_ERR_NONE)
    {
        printf("Program not loaded!\n");
        return err;
    }

    amx_ConsoleInit(&amx);
    err = amx_Register(&amx, print_Natives, -1);
    if (err)
    {
        printf("Natives not registered! Error - %x \n", err);
        return err;
    }

    err = amx_Exec(&amx, &ret, AMX_EXEC_MAIN);
    if (err)
    {
        printf("Error when executing!\n");
        return err;
    }




    memset(&amx, 0, sizeof(amx));
    amx.data = data;
    //amx.flags = AMX_FLAG_DSEG_INIT;
    err = amx_Init(&amx, program2);
    if (err != AMX_ERR_NONE)
    {
        printf("Program not loaded!\n");
        return err;
    }


    amx_ConsoleInit(&amx);
    err = amx_Register(&amx, print_Natives, -1);
    if (err)
    {
        printf("Natives not registered! Error - %x \n", err);
        return err;
    }

    err = amx_Exec(&amx, &ret, AMX_EXEC_MAIN);
    if (err)
    {
        printf("Error when executing!\n");
        return err;
    }

    //aux_FreeProgram(&amx);

    free(data);
    free(program1);
    free(program2);

    return 0;
}
