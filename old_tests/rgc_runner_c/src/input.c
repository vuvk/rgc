#include "input.h"

/*
wVector2i mousePos = {0, 0},
       oldMousePos = {0, 0};
*/

/** KEYBOARD */
duk_ret_t n_keyboard_is_event_available(duk_context* ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(0);

    duk_push_boolean(ctx, wInputIsKeyEventAvailable());

    return 1;
}
duk_ret_t n_keyboard_is_key_hit(duk_context *ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(1);

    int key = duk_to_int(ctx, 0);

    duk_push_boolean(ctx, wInputIsKeyHit((wKeyCode)key));

    return 1;
}
duk_ret_t n_keyboard_is_key_pressed(duk_context *ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(1);

    int key = duk_to_int(ctx, 0);

    duk_push_boolean(ctx, wInputIsKeyPressed((wKeyCode)key));

    return 1;
}
duk_ret_t n_keyboard_is_key_released(duk_context *ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(1);

    int key = duk_to_int(ctx, 0);

    duk_push_boolean(ctx, wInputIsKeyUp((wKeyCode)key));

    return 1;
}

static duk_ret_t n_keyboard_load_object(duk_context *ctx)
{
    const duk_function_list_entry n_keyboard_funcs[] =
    {
        { "isEventAvailable", n_keyboard_is_event_available, 0 },
        { "isKeyHit",      n_keyboard_is_key_hit,      1 },
        { "isKeyPressed",  n_keyboard_is_key_pressed,  1 },
        { "isKeyReleased", n_keyboard_is_key_released, 1 },
        { NULL, NULL, 0 }
    };

	duk_push_object(ctx);
	duk_put_function_list(ctx, -1, n_keyboard_funcs);
	return 1;
}

void RegisterKeyboardCFunctions()
{
    /*
    DUK_REGISTER_C_FUNCTION(n_keyboard_event_available, 0, "keyboard_event_available");
    DUK_REGISTER_C_FUNCTION(n_keyboard_check,          1, "keyboard_check"         );
    DUK_REGISTER_C_FUNCTION(n_keyboard_check_pressed,  1, "keyboard_check_pressed" );
    DUK_REGISTER_C_FUNCTION(n_keyboard_check_released, 1, "keyboard_check_released");
    */

	duk_push_c_function(ctx, n_keyboard_load_object, 0);
	duk_call(ctx, 0);
    duk_put_global_string(ctx, "Keyboard");
}


/** MOUSE */
duk_ret_t n_mouse_is_event_available(duk_context* ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    duk_push_boolean(ctx, wInputIsMouseEventAvailable());

    return 1;
}
duk_ret_t n_mouse_set_logical_position(duk_context* ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(2);

    wVector2f pos = {duk_to_number(ctx, 0), duk_to_number(ctx, 1)};
    wInputSetMouseLogicalPosition(&pos);
    /*mousePos.x = duk_to_number(ctx, 0);
    mousePos.y = duk_to_number(ctx, 1);
    oldMousePos = mousePos;
    wInputSetMousePosition(&mousePos);*/

    return 0;
}
duk_ret_t n_mouse_set_position(duk_context* ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(2);

    wVector2i pos = {duk_to_int(ctx, 0), duk_to_int(ctx, 1)};
    //wInputSetMouseAbsolutePosition(&pos);
/*
    mousePos.x = duk_to_int(ctx, 0);
    mousePos.y = duk_to_int(ctx, 1);
    oldMousePos = mousePos;
*/
    wInputSetMousePosition(&pos);

    return 0;
}
duk_ret_t n_mouse_get_x(duk_context* ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    wVector2i pos;
    wInputGetMousePosition(&pos);
    duk_push_int(ctx, pos.x);
/*
    wVector2f pos;
    wInputGetMousePosition(&pos);
    duk_push_number(ctx, pos.x);
*/
    return 1;
}
duk_ret_t n_mouse_get_y(duk_context* ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    wVector2i pos;
    wInputGetMousePosition(&pos);
    duk_push_int(ctx, pos.y);
/*
    wVector2f pos;
    wInputGetMousePosition(&pos);
    duk_push_number(ctx, pos.y);
*/
    return 1;
}
duk_ret_t n_mouse_get_delta_x(duk_context* ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    duk_push_number(ctx, wInputGetMouseDeltaX());
    //duk_push_int(ctx, mousePos.x - oldMousePos.x);

    return 1;
}
duk_ret_t n_mouse_get_delta_y(duk_context* ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    duk_push_number(ctx, wInputGetMouseDeltaY());
    //duk_push_int(ctx, mousePos.y - oldMousePos.y);

    return 1;
}

static duk_ret_t n_mouse_load_object(duk_context *ctx)
{
    const duk_function_list_entry n_mouse_funcs[] =
    {
        { "isEventAvailable",   n_mouse_is_event_available,   0 },
        { "setLogicalPosition", n_mouse_set_logical_position, 2 },
        { "setPosition", n_mouse_set_position, 2 },
        { "getX",        n_mouse_get_x,        0 },
        { "getY",        n_mouse_get_y,        0 },
        { "getDeltaX",   n_mouse_get_delta_x,  0 },
        { "getDeltaY",   n_mouse_get_delta_y,  0 },
        { NULL, NULL, 0 }
    };

	duk_push_object(ctx);
	duk_put_function_list(ctx, -1, n_mouse_funcs);
	return 1;
}

void RegisterMouseCFunctions()
{
    /*
    DUK_REGISTER_C_FUNCTION(n_mouse_event_available, 0, "mouse_event_available");
    DUK_REGISTER_C_FUNCTION(n_mouse_set_logical_position,  2, "mouse_set_logical_position");
    DUK_REGISTER_C_FUNCTION(n_mouse_set_absolute_position, 2, "mouse_set_absolute_position");
    DUK_REGISTER_C_FUNCTION(n_mouse_get_x,        0, "mouse_get_x"       );
    DUK_REGISTER_C_FUNCTION(n_mouse_get_y,        0, "mouse_get_y"       );
    DUK_REGISTER_C_FUNCTION(n_mouse_get_delta_x,  0, "mouse_get_delta_x" );
    DUK_REGISTER_C_FUNCTION(n_mouse_get_delta_y,  0, "mouse_get_delta_y" );
    */

	duk_push_c_function(ctx, n_mouse_load_object, 0);
	duk_call(ctx, 0);
    duk_put_global_string(ctx, "Mouse");
}
