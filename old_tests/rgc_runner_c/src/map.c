#include "util.h"
#include "res_manager.h"
#include "global.h"
#include "list.h"
#include "textures.h"
#include "sprites.h"
#include "map.h"
#include "WorldSim3D.h"
#include "SampleFunctions.h"


//TMapPack mapPack = {MAP_PACK_ID, NULL};
TMapPack mapPack = {NULL};

// ������� ��������� ��� ���� � �������
wNode* floorNode = NULL;
wNode* ceilNode  = NULL;
// ���� ���������
wNode* skyNode = NULL;
wTexture* skyTextureFront  = NULL;
wTexture* skyTextureBack   = NULL;
wTexture* skyTextureLeft   = NULL;
wTexture* skyTextureRight  = NULL;
wTexture* skyTextureTop    = NULL;
wTexture* skyTextureBottom = NULL;

// ������� ���������
wSelector* g_WorldCollider = NULL;


// ������ � ����� ����
void InitMapPack()
{
    if ((g_MapsCount == 0) && (mapPack.maps))
    {
        free(mapPack.maps);
        mapPack.maps = NULL;
        return;
    }

    if (mapPack.maps == NULL)
    {
        mapPack.maps = calloc(sizeof(TMap), g_MapsCount);
    }
    else
    {
        memset(mapPack.maps, 0, sizeof(TMap)*g_MapsCount);
    }
}

bool LoadMapPack()
{
/*
    FILE*   f = NULL;
    int32_t id  = 0;
    int16_t ver = 0;
    uint8_t x, y, m; //������, ����������, ����� �����
    int32_t c, count;
    TMapElement t;
    bool    isLoaded = false;

    if (!wFileIsExist(MAP_PACK_NAME))
    {
        PrintWithColor("Pack of maps is doesn''t exist!\n", wCFC_RED, false);
        goto end;
    }

    // �������� ������� ����
    f = fopen(MAP_PACK_NAME, "rb");
    if (f == NULL)
    {
        PrintWithColor("Error when opening pack of maps!\n", wCFC_RED, false);
        goto end;
    }
    fseek(f, 0, SEEK_SET);

    // ��������� ��������� ����
    fread(&id,  sizeof(int32_t), 1, f);
    fread(&ver, sizeof(int16_t), 1, f);
    if (id != MAP_PACK_ID || ver != MAP_PACK_VER)
    {
        PrintWithColor("Unknown format of map's pack!\n", wCFC_RED, false);
        goto end;
    }

    // ������ ������
    InitMapPack();
    //fread(mapPack.maps, sizeof(TMap)*g_MapsCount, 1, f);

    // ����� ������ ������ ����
    fseek(f, sizeof(char), 1);  // ���������� g_MapsCount

    for (m = 0; m < g_MapsCount; ++m)
    {
        // ������ ���������
        fread(&(mapPack.maps[m].floorColor), sizeof(int32_t), 1, f);
        fread(&(mapPack.maps[m].ceilColor),  sizeof(int32_t), 1, f);
        fread(&(mapPack.maps[m].fogColor),   sizeof(int32_t), 1, f);
        fread(&(mapPack.maps[m].fogIntensity), sizeof(float), 1, f);
        fread(&(mapPack.maps[m].showFloor),  sizeof(bool), 1, f);
        fread(&(mapPack.maps[m].showCeil),   sizeof(bool), 1, f);
        fread(&(mapPack.maps[m].g_ShowFog),    sizeof(bool), 1, f);
        fread(&(mapPack.maps[m].showSky),    sizeof(bool), 1, f);

        /////////////////////////////////////////////////////
        // ������ ������������ ������ ���� (���� ����)
        /////////////////////////////////////////////////////
        fread(&count, sizeof(count), 1, f);
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                fread(&t, sizeof(t), 1, f);
                fread(&x, sizeof(x), 1, f);
                fread(&y, sizeof(y), 1, f);
                mapPack.maps[m].floor[x][y] = t;
            }
        }

        /////////////////////////////////////////////////////
        // ������ ������������ ������ ������ (���� ����)
        /////////////////////////////////////////////////////
        fread(&count, sizeof(count), 1, f);
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                fread(&t, sizeof(t), 1, f);
                fread(&x, sizeof(x), 1, f);
                fread(&y, sizeof(y), 1, f);
                mapPack.maps[m].level[x][y] = t;
            }
        }

        /////////////////////////////////////////////////////
        // ������ ������������ ������ ������� (���� ����)
        /////////////////////////////////////////////////////
        fread(&count, sizeof(count), 1, f);
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                fread(&t, sizeof(t), 1, f);
                fread(&x, sizeof(x), 1, f);
                fread(&y, sizeof(y), 1, f);
                mapPack.maps[m].ceil[x][y] = t;
            }
        }
    }



    isLoaded = true;

end:
    if (f != NULL)
        fclose(f);
    f = NULL;
    return isLoaded;
*/
    TMapElement t;
    uint8_t  x, y, m; //������, ����������, ����� �����
    int32_t  /*i, */c;
    uint16_t count;
    void*    buffer = NULL;
    int      bufferSize = 0;
    int      bufferMaxSize = 0;
    bool     isLoaded = false;
    int      mapSize = 0;
    char     strBuffer[256];
    char*    p = NULL;
    /*TCellInfo cellInfo;*/

    PrintWithColor("Starting load maps...\n", wCFC_YELLOW, false);

    //InitMapPack();

    /*
    switch (Res_OpenZipForRead(MAPS_PACK_NAME))
    {
        case RES_NOT_FOUND  :
            PrintWithColor("Pack of maps is doesn''t exist!\n", wCFC_RED, false);
            goto end;
            break;
        case RES_NOT_OPENED :
            PrintWithColor("Error when opening pack of maps!\n", wCFC_RED, false);
            goto end;
            break;
    }
    */

    mapSize = MAP_WIDTH * MAP_HEIGHT;
    bufferMaxSize = mapSize * (sizeof(count) + sizeof(x) + sizeof(y) + sizeof(t));

    // ������ ���������� ����
    bufferSize = Res_GetUncompressedFileSize("maps.cfg");
    if (bufferSize <= 0)
    {
        PrintWithColor("can not get size of maps.cfg!", wCFC_RED, false);
        goto end;
    }
    buffer = malloc(bufferSize);
    if (Res_ReadFileFromZipToBuffer("maps.cfg", buffer, bufferSize) <= 0) goto end;
    if (!Res_OpenIniFromBuffer(buffer, bufferSize))
    {
        PrintWithColor("can not open maps.cfg!!!", wCFC_RED, false);
        goto end;
    }
    g_MapsCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    free(buffer);
    if (g_MapsCount == 0) goto end;

    InitMapPack();

    buffer = calloc(1, bufferMaxSize);

    for (m = 0; m < g_MapsCount; ++m)
    {
        // ������ ��������� �����
        sprintf(strBuffer, "map_%d.cfg", m);
        bufferSize = Res_GetUncompressedFileSize(strBuffer);
        if (bufferSize <= 0)
        {
            printf("cannot get size of map_%d.cfg!\n", m);
            continue;
        }

        Res_ReadFileFromZipToBuffer(strBuffer, buffer, bufferSize);
        Res_OpenIniFromBuffer(buffer, bufferSize);
        mapPack.maps[m].floorColor = Res_IniReadInteger("Options", "floor_color", 0);
        mapPack.maps[m].ceilColor  = Res_IniReadInteger("Options", "ceil_color",  0);
        mapPack.maps[m].fogColor   = Res_IniReadInteger("Options", "fog_color",   0);
        mapPack.maps[m].fogIntensity = Res_IniReadDouble("Options", "fog_intensity", 0);
        mapPack.maps[m].showFloor  = Res_IniReadBool   ("Options", "show_floor", false);
        mapPack.maps[m].showCeil   = Res_IniReadBool   ("Options", "show_ceil",  false);
        mapPack.maps[m].showFog    = Res_IniReadBool   ("Options", "show_fog",   false);
        mapPack.maps[m].showSky    = Res_IniReadBool   ("Options", "show_sky",   false);
        mapPack.maps[m].skyNumber  = Res_IniReadInteger("Options", "sky_number", 0);
        mapPack.maps[m].skyNumber--;    // �.�. ������� ��������� ���������� � ����
        Res_CloseIni();

        ////////////////////////////////////
        // ������ �����
        ////////////////////////////////////
        sprintf(strBuffer, "map_%d.dat", m);
        bufferSize = Res_GetUncompressedFileSize(strBuffer);
        if (bufferSize <= 0)
        {
            printf("cannot get size of map_%d.dat!\n", m);
            continue;
        }
        Res_ReadFileFromZipToBuffer(strBuffer, buffer, bufferSize);

        /////////////////////////////////////////////////////
        // ������ ������������ ������ ���� (���� ����)
        /////////////////////////////////////////////////////
        p = buffer;
        count = *((uint16_t*)p);
        p += sizeof(count);
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                t = *((TMapElement*)p);
                p += sizeof(TMapElement);
                x = *((uint8_t*)p);
                p += sizeof(x);
                y = *((uint8_t*)p);
                p += sizeof(y);

                mapPack.maps[m].floor[x][y] = t;
            }
        }

        /////////////////////////////////////////////////////
        // ������ ������������ ������ ������ (���� ����)
        /////////////////////////////////////////////////////
        count = *((uint16_t*)p);
        p += sizeof(count);
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                t = *((TMapElement*)p);
                p += sizeof(TMapElement);
                x = *((uint8_t*)p);
                p += sizeof(x);
                y = *((uint8_t*)p);
                p += sizeof(y);

                mapPack.maps[m].level[x][y] = t;
            }
        }

        /////////////////////////////////////////////////////
        // ������ ������������ ������ ������� (���� ����)
        /////////////////////////////////////////////////////
        count = *((uint16_t*)p);
        p += sizeof(count);
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                t = *((TMapElement*)p);
                p += sizeof(TMapElement);
                x = *((uint8_t*)p);
                p += sizeof(x);
                y = *((uint8_t*)p);
                p += sizeof(y);

                mapPack.maps[m].ceil[x][y] = t;
            }
        }


        /*
        // ������ ���������
        fread(&(mapPack.maps[m].floorColor), sizeof(int32_t), 1, f);
        fread(&(mapPack.maps[m].ceilColor),  sizeof(int32_t), 1, f);
        fread(&(mapPack.maps[m].fogColor),   sizeof(int32_t), 1, f);
        fread(&(mapPack.maps[m].fogIntensity), sizeof(float), 1, f);
        fread(&(mapPack.maps[m].showFloor),  sizeof(bool), 1, f);
        fread(&(mapPack.maps[m].showCeil),   sizeof(bool), 1, f);
        fread(&(mapPack.maps[m].g_ShowFog),    sizeof(bool), 1, f);
        fread(&(mapPack.maps[m].showSky),    sizeof(bool), 1, f);

        /////////////////////////////////////////////////////
        // ������ ������������ ������ ���� (���� ����)
        /////////////////////////////////////////////////////
        fread(&count, sizeof(count), 1, f);
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                fread(&t, sizeof(t), 1, f);
                fread(&x, sizeof(x), 1, f);
                fread(&y, sizeof(y), 1, f);
                mapPack.maps[m].floor[x][y] = t;
            }
        }

        /////////////////////////////////////////////////////
        // ������ ������������ ������ ������ (���� ����)
        /////////////////////////////////////////////////////
        fread(&count, sizeof(count), 1, f);
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                fread(&t, sizeof(t), 1, f);
                fread(&x, sizeof(x), 1, f);
                fread(&y, sizeof(y), 1, f);
                mapPack.maps[m].level[x][y] = t;
            }
        }

        /////////////////////////////////////////////////////
        // ������ ������������ ������ ������� (���� ����)
        /////////////////////////////////////////////////////
        fread(&count, sizeof(count), 1, f);
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                fread(&t, sizeof(t), 1, f);
                fread(&x, sizeof(x), 1, f);
                fread(&y, sizeof(y), 1, f);
                mapPack.maps[m].ceil[x][y] = t;
            }
        }
        */
    }



    isLoaded = true;

end:
    if (buffer != NULL)
        free(buffer);
    buffer = NULL;
    Res_CloseIni();
    /*Res_CloseZipAfterRead();*/
    PrintWithColor("Finished load maps.\n", wCFC_YELLOW, false);
    return isLoaded;
}

void ClearLevel()
{
    register int i;
    wMesh* mesh = NULL;

    if (g_WorldCollider != NULL)
    {
        wCollisionGroupRemoveAll(g_WorldCollider);
    }

    if (spriteNodes != NULL)
    {
        SListElement* element = spriteNodes->first;
        for (i = 0; (i < spriteNodes->size) && element; ++i, element = element->next)
        {
            if (element->value != NULL)
            {
                SpriteNodeDestroy((TSpriteNode**) (&(element->value)));
            }
        }
        ListClear(spriteNodes);
    }

    if (textureNodes != NULL)
    {
        SListElement* element = textureNodes->first;
        for (i = 0; (i < textureNodes->size) && element; ++i, element = element->next)
        {
            if (element->value != NULL)
            {
                TextureNodeDestroy((TTextureNode**) (&(element->value)));
            }
        }
        ListClear(textureNodes);
    }
/*
    if (invisibleMesh != NULL)
    {
        wMeshDestroy(invisibleMesh);
        invisibleMesh = NULL;
    }

    if (invisibleNode != NULL)
    {
        wNodeDestroy(invisibleNode);
        invisibleNode = NULL;
    }
*/

    if (floorNode != NULL)
    {
        mesh = wNodeGetMesh(floorNode);
        if (mesh != NULL)
            wMeshDestroy(mesh);
        wNodeDestroy(floorNode);
        floorNode = NULL;
    }

    if (ceilNode != NULL)
    {
        mesh = wNodeGetMesh(ceilNode);
        if (mesh != NULL)
            wMeshDestroy(mesh);
        wNodeDestroy(ceilNode);
        ceilNode = NULL;
    }

    if (skyNode != NULL)
    {
        mesh = wNodeGetMesh(skyNode);
        if (mesh != NULL)
            wMeshDestroy(mesh);
        wNodeDestroy(skyNode);
        skyNode = NULL;

        if (skyTextureFront != NULL)
        {
            wTextureDestroy(skyTextureFront);
            skyTextureFront = NULL;
        }
        if (skyTextureBack != NULL)
        {
            wTextureDestroy(skyTextureBack);
            skyTextureBack = NULL;
        }
        if (skyTextureLeft != NULL)
        {
            wTextureDestroy(skyTextureLeft);
            skyTextureLeft = NULL;
        }
        if (skyTextureRight != NULL)
        {
            wTextureDestroy(skyTextureRight);
            skyTextureRight = NULL;
        }
        if (skyTextureTop != NULL)
        {
            wTextureDestroy(skyTextureTop);
            skyTextureTop = NULL;
        }
        if (skyTextureBottom != NULL)
        {
            wTextureDestroy(skyTextureBottom);
            skyTextureBottom = NULL;
        }
    }


    // �� ������ ������
    wSceneDestroyAllNodes();
    wSceneDestroyAllMeshes();
}

wMesh* CreateFrontWall(const char* name, wTexture* texture, int x, int z, int len)
{
    float fromX = x;
    float toX   = x + len;

    wVert verts[4] =
    {
        (wVector3f){ toX,   0.0f, z - 1 }, wVECTOR3f_FORWARD, wCOLOR4s_WHITE, (wVector2f){ len, 1.0 },
        (wVector3f){ fromX, 0.0f, z - 1 }, wVECTOR3f_FORWARD, wCOLOR4s_WHITE, (wVector2f){ 0.0, 1.0 },
        (wVector3f){ fromX, 1.0f, z - 1 }, wVECTOR3f_FORWARD, wCOLOR4s_WHITE, (wVector2f){ 0.0, 0.0 },
        (wVector3f){ toX,   1.0f, z - 1 }, wVECTOR3f_FORWARD, wCOLOR4s_WHITE, (wVector2f){ len, 0.0 }
    };
    uint16_t indices[] = { 0, 1, 2, 0, 2, 3 };

    wMesh* mesh = wMeshCreate((char*)name);
    wMeshBuffer* meshBuffer = NULL;
    wMaterial* material = NULL;

    meshBuffer = wMeshBufferCreate(4, verts, 6, indices);
    material = wMeshBufferGetMaterial(meshBuffer);
    MaterialSetOldSchool(material);
    wMaterialSetTexture(material, 0, texture);
    wMeshAddMeshBuffer(mesh, meshBuffer);
/*    wMeshEnableHardwareAcceleration(mesh, 0);

    /////////////////////////
    // CREATE TOTAL NODE
    /////////////////////////
    wNode* node = wNodeCreateFromMesh(mesh);
    //wNodeSetDebugMode(node, wDM_MESH_WIRE_OVERLAY);

    return node;
*/
    return mesh;
}

wMesh* CreateBackWall(const char* name, wTexture* texture, int x, int z, int len)
{
    float fromX = x;
    float toX   = x + len;

    wVert verts[4] =
    {
        (wVector3f){ fromX, 0.0f, z }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, (wVector2f){ len, 1.0 },
        (wVector3f){ toX,   0.0f, z }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, (wVector2f){ 0.0, 1.0 },
        (wVector3f){ toX,   1.0f, z }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, (wVector2f){ 0.0, 0.0 },
        (wVector3f){ fromX, 1.0f, z }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, (wVector2f){ len, 0.0 }
    };
    uint16_t indices[] = { 0, 1, 2, 0, 2, 3 };

    wMesh* mesh = wMeshCreate((char*)name);
    wMeshBuffer* meshBuffer = NULL;
    wMaterial* material = NULL;

    meshBuffer = wMeshBufferCreate(4, verts, 6, indices);
    material = wMeshBufferGetMaterial(meshBuffer);
    MaterialSetOldSchool(material);
    wMaterialSetTexture(material, 0, texture);
    wMeshAddMeshBuffer(mesh, meshBuffer);
/*    wMeshEnableHardwareAcceleration(mesh, 0);

    /////////////////////////
    // CREATE TOTAL NODE
    /////////////////////////
    wNode* node = wNodeCreateFromMesh(mesh);
    //wNodeSetDebugMode(node, wDM_MESH_WIRE_OVERLAY);

    return node;
*/
    return mesh;
}

wMesh* CreateRightWall(const char* name, wTexture* texture, int x, int z, int len)
{
    float fromZ = z;
    float toZ   = z - len;

    wVert verts[4] =
    {
        (wVector3f){ x + 1.0f, 0.0f, fromZ }, wVECTOR3f_RIGHT, wCOLOR4s_WHITE, (wVector2f){ len, 1.0 },
        (wVector3f){ x + 1.0f, 0.0f, toZ   }, wVECTOR3f_RIGHT, wCOLOR4s_WHITE, (wVector2f){ 0.0, 1.0 },
        (wVector3f){ x + 1.0f, 1.0f, toZ   }, wVECTOR3f_RIGHT, wCOLOR4s_WHITE, (wVector2f){ 0.0, 0.0 },
        (wVector3f){ x + 1.0f, 1.0f, fromZ }, wVECTOR3f_RIGHT, wCOLOR4s_WHITE, (wVector2f){ len, 0.0 }
    };
    uint16_t indices[] = { 0, 1, 2, 0, 2, 3 };

    wMesh* mesh = wMeshCreate((char*)name);
    wMeshBuffer* meshBuffer = NULL;
    wMaterial* material = NULL;

    meshBuffer = wMeshBufferCreate(4, verts, 6, indices);
    material = wMeshBufferGetMaterial(meshBuffer);
    MaterialSetOldSchool(material);
    wMaterialSetTexture(material, 0, texture);
    wMeshAddMeshBuffer(mesh, meshBuffer);
/*    wMeshEnableHardwareAcceleration(mesh, 0);

    /////////////////////////
    // CREATE TOTAL NODE
    /////////////////////////
    wNode* node = wNodeCreateFromMesh(mesh);
    //wNodeSetDebugMode(node, wDM_MESH_WIRE_OVERLAY);

    return node;
*/
    return mesh;
}

wMesh* CreateLeftWall(const char* name, wTexture* texture, int x, int z, int len)
{
    float fromZ = z;
    float toZ   = z - len;

    wVert verts[4] =
    {
        (wVector3f){ x, 0.0f, toZ   }, wVECTOR3f_LEFT, wCOLOR4s_WHITE, (wVector2f){ len, 1.0 },
        (wVector3f){ x, 0.0f, fromZ }, wVECTOR3f_LEFT, wCOLOR4s_WHITE, (wVector2f){ 0.0, 1.0 },
        (wVector3f){ x, 1.0f, fromZ }, wVECTOR3f_LEFT, wCOLOR4s_WHITE, (wVector2f){ 0.0, 0.0 },
        (wVector3f){ x, 1.0f, toZ   }, wVECTOR3f_LEFT, wCOLOR4s_WHITE, (wVector2f){ len, 0.0 }
    };
    uint16_t indices[] = { 0, 1, 2, 0, 2, 3 };

    wMesh* mesh = wMeshCreate((char*)name);
    wMeshBuffer* meshBuffer = NULL;
    wMaterial* material = NULL;

    meshBuffer = wMeshBufferCreate(4, verts, 6, indices);
    material = wMeshBufferGetMaterial(meshBuffer);
    MaterialSetOldSchool(material);
    wMaterialSetTexture(material, 0, texture);
    wMeshAddMeshBuffer(mesh, meshBuffer);
/*    wMeshEnableHardwareAcceleration(mesh, 0);

    /////////////////////////
    // CREATE TOTAL NODE
    /////////////////////////
    wNode* node = wNodeCreateFromMesh(mesh);
    //wNodeSetDebugMode(node, wDM_MESH_WIRE_OVERLAY);

    return node;
*/
    return mesh;
}

wMesh* CreateFloor(const char* name, wTexture* texture, int x, int z, int lenX, int lenZ)
{
    float fromX = x;
    float toX   = x + lenX;
    float fromZ = z;
    float toZ   = z - lenZ;

    wVert verts[4] =
    {
        (wVector3f){ fromX, 0.0f, fromZ }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){  0.0,  0.0 },
        (wVector3f){ toX,   0.0f, fromZ }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){ lenX,  0.0 },
        (wVector3f){ toX,   0.0f, toZ   }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){ lenX, lenZ },
        (wVector3f){ fromX, 0.0f, toZ   }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){  0.0, lenZ }
    };
    uint16_t indices[] = { 0, 1, 2, 0, 2, 3 };

    wMesh* mesh = wMeshCreate((char*)name);
    wMeshBuffer* meshBuffer = NULL;
    wMaterial* material = NULL;

    meshBuffer = wMeshBufferCreate(4, verts, 6, indices);
    material = wMeshBufferGetMaterial(meshBuffer);
    wMaterialSetTexture(material, 0, texture);
    MaterialSetOldSchool(material);
    wMeshAddMeshBuffer(mesh, meshBuffer);
/*    wMeshEnableHardwareAcceleration(mesh, 0);

    /////////////////////////
    // CREATE TOTAL NODE
    /////////////////////////
    wNode* node = wNodeCreateFromMesh(mesh);
    //wNodeSetDebugMode(node, wDM_MESH_WIRE_OVERLAY);

    return node;
*/
    return mesh;
}

wMesh* CreateCeil(const char* name, wTexture* texture, int x, int z, int lenX, int lenZ)
{
    float fromX = x;
    float toX   = x + lenX;
    float fromZ = z;
    float toZ   = z - lenZ;

    wVert verts[4] =
    {
        (wVector3f){ fromX, 1.0f, toZ   }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){  0.0, lenZ },
        (wVector3f){ toX,   1.0f, toZ   }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){ lenX, lenZ },
        (wVector3f){ toX,   1.0f, fromZ }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){ lenX,  0.0 },
        (wVector3f){ fromX, 1.0f, fromZ }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){ 0.0,  0.0 }
    };
    uint16_t indices[] = { 0, 1, 2, 0, 2, 3 };

    wMesh* mesh = wMeshCreate((char*)name);
    wMeshBuffer* meshBuffer = NULL;
    wMaterial* material = NULL;

    meshBuffer = wMeshBufferCreate(4, verts, 6, indices);
    material = wMeshBufferGetMaterial(meshBuffer);
    wMaterialSetTexture(material, 0, texture);
    MaterialSetOldSchool(material);
    wMeshAddMeshBuffer(mesh, meshBuffer);
/*    wMeshEnableHardwareAcceleration(mesh, 0);

    /////////////////////////
    // CREATE TOTAL NODE
    /////////////////////////
    wNode* node = wNodeCreateFromMesh(mesh);
    //wNodeSetDebugMode(node, wDM_MESH_WIRE_OVERLAY);

    return node;
*/
    return mesh;
}

wNode* CreateInvisibleBlock(const char* name, int x, int z)
{
    // ������� ��������� ��������, ���� ��� �� ������� �����
    if (g_InvisibleTexture == NULL)
        g_InvisibleTexture = CreateColorTexture(wCOLOR4s_ZERO);

    const wMesh* mesh = NULL;

    // ������� ��������� ����, ���� �� �� ������ �����
    if (mesh == NULL)
    {
        const wVert verts[] =
        {
            // forward
            (wVector3f){ 1.0f, 0.0f,-1.0f }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, (wVector2f){ 1.0, 1.0 },
            (wVector3f){ 0.0f, 0.0f,-1.0f }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, (wVector2f){ 0.0, 1.0 },
            (wVector3f){ 0.0f, 1.0f,-1.0f }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, (wVector2f){ 0.0, 0.0 },
            (wVector3f){ 1.0f, 1.0f,-1.0f }, wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, (wVector2f){ 1.0, 0.0 },

            // backward
            (wVector3f){ 0.0f, 0.0f, 0.0f }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, (wVector2f){ 1.0, 1.0 },
            (wVector3f){ 1.0f, 0.0f, 0.0f }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, (wVector2f){ 0.0, 1.0 },
            (wVector3f){ 1.0f, 1.0f, 0.0f }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, (wVector2f){ 0.0, 0.0 },
            (wVector3f){ 0.0f, 1.0f, 0.0f }, wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, (wVector2f){ 1.0, 0.0 },

            // right
            (wVector3f){ 1.0f, 0.0f, 0.0f }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, (wVector2f){ 1.0, 1.0 },
            (wVector3f){ 1.0f, 0.0f,-1.0f }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, (wVector2f){ 0.0, 1.0 },
            (wVector3f){ 1.0f, 1.0f,-1.0f }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, (wVector2f){ 0.0, 0.0 },
            (wVector3f){ 1.0f, 1.0f, 0.0f }, wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, (wVector2f){ 1.0, 0.0 },

            // left
            (wVector3f){ 0.0f, 0.0f,-1.0f }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, (wVector2f){ 1.0, 1.0 },
            (wVector3f){ 0.0f, 0.0f, 0.0f }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, (wVector2f){ 0.0, 1.0 },
            (wVector3f){ 0.0f, 1.0f, 0.0f }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, (wVector2f){ 0.0, 0.0 },
            (wVector3f){ 0.0f, 1.0f,-1.0f }, wVECTOR3f_LEFT,     wCOLOR4s_WHITE, (wVector2f){ 1.0, 0.0 }
        };
        uint16_t indices[] = {  0,  1,  2,  0,  2,  3,
                                4,  5,  6,  4,  6,  7,
                                8,  9, 10,  8, 10, 11,
                               12, 13, 14, 12, 14, 15 };

        mesh = wMeshCreate((char*)name);
        wMeshBuffer* meshBuffer = wMeshBufferCreate(16, verts, 24, indices);
        wMaterial* material = wMeshBufferGetMaterial(meshBuffer);
        MaterialSetOldSchool(material);

        wMaterialSetTexture(material, 0, g_InvisibleTexture);
        wMaterialSetFlag(material, wMF_FOG_ENABLE, false);
        wMaterialSetType(material, wMT_TRANSPARENT_ALPHA_CHANNEL);
        wMeshAddMeshBuffer(mesh, meshBuffer);
        wMeshEnableHardwareAcceleration(mesh, 0);
    }


    /////////////////////////
    // CREATE TOTAL NODE
    /////////////////////////
    wNode* node = wNodeCreateFromMesh(mesh);
    if (node != NULL)
        wNodeSetPosition(node, (wVector3f){x, 0, z});

    return node;
}

void CreateFloorAndCeilPlane(uint32_t floorColor, uint32_t ceilColor)
{
    // ����� ������ ����
    const wVert floorVerts[4] =
    {
        (wVector3f){ 0.0f,      -0.1f, 0.0f        }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){ 0.0,       0.0        },
        (wVector3f){ MAP_WIDTH, -0.1f, 0.0f        }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){ MAP_WIDTH, 0.0        },
        (wVector3f){ MAP_WIDTH, -0.1f, -MAP_HEIGHT }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){ MAP_WIDTH, MAP_HEIGHT },
        (wVector3f){ 0.0f,      -0.1f, -MAP_HEIGHT }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){ 0.0,       MAP_HEIGHT }
    };
    const uint16_t floorIndices[] = { 0, 1, 2, 0, 2, 3 };

    // ����� ������ �������
    const wVert ceilVerts[4] =
    {
        (wVector3f){ 0.0f,      1.1f, -MAP_HEIGHT }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){ 0.0,       MAP_HEIGHT },
        (wVector3f){ MAP_WIDTH, 1.1f, -MAP_HEIGHT }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){ MAP_WIDTH, MAP_HEIGHT },
        (wVector3f){ MAP_WIDTH, 1.1f, 0.0f        }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){ MAP_WIDTH, 0.0        },
        (wVector3f){ 0.0f,      1.1f, 0.0f        }, wVECTOR3f_UP, wCOLOR4s_WHITE, (wVector2f){ 0.0,       0.0        }
    };
    const uint16_t ceilIndices[] = { 0, 1, 2, 0, 2, 3 };

    wTexture* texture = NULL;
    wMaterial* material = NULL;
    wMesh* mesh = NULL;
    wMeshBuffer* meshBuffer = NULL;
    char strBuffer[256];

    //
    // ������� ��������� ����
    sprintf(strBuffer, "floor color");
    mesh = wMeshCreate(strBuffer);
    texture = CreateColorTexture(wUtilUIntToColor4s(floorColor));

    meshBuffer = wMeshBufferCreate(4, floorVerts, 6, floorIndices);
    material = wMeshBufferGetMaterial(meshBuffer);
    wMaterialSetTexture(material, 0, texture);
    MaterialSetOldSchool(material);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    floorNode = wNodeCreateFromMesh(mesh);

    //
    // ������� ��������� �������
    sprintf(strBuffer, "ceil color");
    mesh = wMeshCreate(strBuffer);
    texture = CreateColorTexture(wUtilUIntToColor4s(ceilColor));

    meshBuffer = wMeshBufferCreate(4, ceilVerts, 6, ceilIndices);
    material = wMeshBufferGetMaterial(meshBuffer);
    wMaterialSetTexture(material, 0, texture);
    MaterialSetOldSchool(material);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    ceilNode = wNodeCreateFromMesh(mesh);
}

void GenerateLevel(int numOfMap)
{
    int i, j/*, k*/;
    TMap* map = NULL;
    wNode* node = NULL;
    wMesh* mesh = NULL;
    wMesh* batch = NULL;
    wNode* billboard = NULL;
    wNode* solidBox = NULL;
    wTexture*  texture  = NULL;
    wMaterial* material = NULL;
    TMapElement element;
    char strBuffer[256];
    int count = 0;

    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    ClearLevel();

    map = &(mapPack.maps[numOfMap]);

    // ������� ������ �����, ������� ����� ����������� �� ���������� ���������
    SList* meshes = ListCreate();

    // ������ ������ ��������
    /*
    if (spriteNodes != NULL)
        ListClear(spriteNodes);
    else
        spriteNodes = ListCreate();
    */
    if (spriteNodes == NULL)
        spriteNodes = ListCreate();

    // ������������ �������
    for (i = 0; i < MAP_WIDTH; ++i)
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map->level[i][j];
            // ���� ������ ���...
            if (element == 0)
                continue;

            // -1 ������ ��� ��������� � ������� � 0, � �������� �� ����� ���� ����� 0
            --element;

            if (element >= TEXTURES_MAX_COUNT && element < TEXTURES_MAX_COUNT + SPRITES_MAX_COUNT)
            {
                sprintf(strBuffer, "sprite node %d", count);
                //const wBillboardAxisParam param = {false, true, false};
                int32_t sprNum = element - TEXTURES_MAX_COUNT;

                TSpriteInMem* sprite = &(sprites[sprNum]);

                billboard = wBillBoardCreate((wVector3f){i + 0.5f, 0.5f, -j - 0.5f}, (wVector2f){1, 1});
                //wBillboardSetEnabledAxis(billboard, param);
                if (sprite->framesCount > 0)
                {
                    material = wNodeGetMaterial(billboard, 0);
                    wMaterialSetTexture(material, 0, sprite->frames[0]);
                    MaterialSetOldSchool(material);
                    wMaterialSetType(material, wMT_TRANSPARENT_ALPHA_CHANNEL);
                }
                wNodeSetPosition(billboard, (wVector3f){i + 0.5f, 0.5f, -j - 0.5f});

                solidBox = NULL;
                if (sprite->collision)
                {
                    solidBox = CreateInvisibleBlock(strBuffer, i, -j);
                }

                TSpriteNode* spriteNode = SpriteNodeCreate(sprite, solidBox, billboard);
                ListAddElement(spriteNodes, spriteNode);

                ++count;
            }
        }

    ///////////////////////////////////////////////////
    // ��������� �������������� ����
    ///////////////////////////////////////////////////
    TMapElement pEl;    // ����. �������
    TMapElement el;     // ������� �������
    TMapElement tEl;    // ������� ��� �����
    int lenX;       // ����� �������� � ������
    int lenY;       // ����� �������� � ������

    int pos;        // ������� � ������� �������� �����������
    int x, y;       // ��������� ��� ������� �������������� ���������

    for (j = 0; j < MAP_HEIGHT; ++j)
    {
        // ������ ����� (� 3� - ��������)
        if (j < MAP_HEIGHT - 1)
        {
            for (i = 0; i < MAP_WIDTH - 1; ++i)
            {
                el  = map->level[i][j];
                tEl = map->level[i][j + 1];

                // ���� ������� ���� �����, � ����� �� �����, �� ���������� ����� ��������
                if ((el > 0 && el < TEXTURES_MAX_COUNT) &&
                    (tEl == 0 || tEl >= TEXTURES_MAX_COUNT))
                {
                    lenX = 1;
                    pos = i;

                    pEl = el;
                    ++i;
                    bool proceed = true;
                    while (proceed)
                    {
                        el  = map->level[i][j];
                        tEl = map->level[i][j + 1];

                        // ���� ��������� ������� �� ����� ����������� ��� ��������,
                        // �� ��������� ������ � ���������� �����
                        if (el != pEl || (tEl > 0 && tEl < TEXTURES_MAX_COUNT))
                        {
                            --i;
                            proceed = false;
                            continue;
                        }

                        ++lenX;
                        if (i == MAP_WIDTH - 1)
                        {
                            proceed = false;
                            continue;
                        }

                        ++i;
                    }

                    sprintf(strBuffer, "wall node %d", count);
                    ListAddElement(meshes, CreateFrontWall(strBuffer, textures[--pEl].frames[0], pos, -j, lenX));
                    ++count;
                }
            }
        }

        // ������� ����� (� 3� - ������)
        if (j > 0)
        {
            for (i = 0; i < MAP_WIDTH - 1; ++i)
            {
                el  = map->level[i][j];
                tEl = map->level[i][j - 1];

                // ���� ������� ���� �����, � ����� �� �����, �� ���������� ����� ��������
                if ((el > 0 && el < TEXTURES_MAX_COUNT) &&
                    (tEl == 0 || tEl >= TEXTURES_MAX_COUNT))
                {
                    lenX = 1;
                    pos = i;

                    pEl = el;
                    ++i;
                    bool proceed = true;
                    while (proceed)
                    {
                        el  = map->level[i][j];
                        tEl = map->level[i][j - 1];

                        // ���� ��������� ������� �� ����� ����������� ��� ��������,
                        // �� ��������� ������ � ���������� �����
                        if (el != pEl || (tEl > 0 && tEl < TEXTURES_MAX_COUNT))
                        {
                            --i;
                            proceed = false;
                            continue;
                        }

                        ++lenX;
                        if (i == MAP_WIDTH - 1)
                        {
                            proceed = false;
                            continue;
                        }

                        ++i;
                    }

                    sprintf(strBuffer, "wall node %d", count);
                    ListAddElement(meshes, CreateBackWall(strBuffer, textures[--pEl].frames[0], pos, -j, lenX));
                    ++count;
                }
            }
        }
    }

    ///////////////////////////////////////////////////
    // ��������� ������������ ����
    ///////////////////////////////////////////////////
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        // ������ �����
        if (i < MAP_WIDTH - 1)
        {
            for (j = 0; j < MAP_HEIGHT - 1; ++j)
            {
                el  = map->level[i    ][j];
                tEl = map->level[i + 1][j];

                // ���� ������� ���� �����, � ����� �� �����, �� ���������� ����� ��������
                if ((el > 0 && el < TEXTURES_MAX_COUNT) &&
                    (tEl == 0 || tEl >= TEXTURES_MAX_COUNT))
                {
                    lenY = 1;
                    pos = j;

                    pEl = el;
                    ++j;
                    bool proceed = true;
                    while (proceed)
                    {
                        el  = map->level[i    ][j];
                        tEl = map->level[i + 1][j];

                        // ���� ��������� ������� �� ����� ����������� ��� ��������,
                        // �� ��������� ������ � ���������� �����
                        if (el != pEl || (tEl > 0 && tEl < TEXTURES_MAX_COUNT))
                        {
                            --j;
                            proceed = false;
                            continue;
                        }

                        ++lenY;
                        if (j == MAP_HEIGHT - 1)
                        {
                            proceed = false;
                            continue;
                        }

                        ++j;
                    }

                    sprintf(strBuffer, "wall node %d", count);
                    ListAddElement(meshes, CreateRightWall(strBuffer, texturesShadowed[--pEl].frames[0], i, -pos, lenY));
                    ++count;
                }
            }
        }

        // ����� �����
        if (i > 0)
        {
            for (j = 0; j < MAP_HEIGHT - 1; ++j)
            {
                el  = map->level[i    ][j];
                tEl = map->level[i - 1][j];

                // ���� ������� ���� �����, � ����� �� �����, �� ���������� ����� ��������
                if ((el > 0 && el < TEXTURES_MAX_COUNT) &&
                    (tEl == 0 || tEl >= TEXTURES_MAX_COUNT))
                {
                    lenY = 1;
                    pos = j;

                    pEl = el;
                    ++j;
                    bool proceed = true;
                    while (proceed)
                    {
                        el  = map->level[i    ][j];
                        tEl = map->level[i - 1][j];

                        // ���� ��������� ������� �� ����� ����������� ��� ��������,
                        // �� ��������� ������ � ���������� �����
                        if (el != pEl || (tEl > 0 && tEl < TEXTURES_MAX_COUNT))
                        {
                            --j;
                            proceed = false;
                            continue;
                        }

                        ++lenY;
                        if (j == MAP_HEIGHT - 1)
                        {
                            proceed = false;
                            continue;
                        }

                        ++j;
                    }

                    sprintf(strBuffer, "wall node %d", count);
                    ListAddElement(meshes, CreateLeftWall(strBuffer, texturesShadowed[--pEl].frames[0], i, -pos, lenY));
                    ++count;
                }
            }
        }
    }



    ///////////////////////////////////////////////////
    // ��������� ���� � �������
    ///////////////////////////////////////////////////

    TMapElement mergeInfo[MAP_WIDTH][MAP_HEIGHT];   // ���������� � ���, ����� �� ���������� ������� ������ � ��� ���
                                                    // ���� 0, ������ ������
    TMapElement lEl;    // ������� �� ������

    // ���� �� ���������� ����, �� ������� ����������� ��������� ������� � ����
    if (!map->showSky)
        CreateFloorAndCeilPlane(map->floorColor, map->ceilColor);
    else
    {
        char skyboxName[256];

        /// FRONT
        sprintf(strBuffer, "skybox_front_%d.dat",      map->skyNumber);
        sprintf(skyboxName, "Texture_Skybox_front_%d", map->skyNumber);
        skyTextureFront = LoadTexture(strBuffer, skyboxName);

        /// BACK
        sprintf(strBuffer, "skybox_back_%d.dat",       map->skyNumber);
        sprintf(skyboxName, "Texture_Skybox_back_%d",  map->skyNumber);
        skyTextureBack = LoadTexture(strBuffer, skyboxName);

        /// LEFT
        sprintf(strBuffer, "skybox_left_%d.dat",      map->skyNumber);
        sprintf(skyboxName, "Texture_Skybox_left_%d", map->skyNumber);
        skyTextureLeft = LoadTexture(strBuffer, skyboxName);

        /// RIGHT
        sprintf(strBuffer, "skybox_right_%d.dat",      map->skyNumber);
        sprintf(skyboxName, "Texture_Skybox_right_%d", map->skyNumber);
        skyTextureRight = LoadTexture(strBuffer, skyboxName);

        /// TOP
        sprintf(strBuffer, "skybox_top_%d.dat",      map->skyNumber);
        sprintf(skyboxName, "Texture_Skybox_top_%d", map->skyNumber);
        skyTextureTop = LoadTexture(strBuffer, skyboxName);

        /// BOTTOM
        sprintf(strBuffer, "skybox_bottom_%d.dat",      map->skyNumber);
        sprintf(skyboxName, "Texture_Skybox_bottom_%d", map->skyNumber);
        skyTextureBottom = LoadTexture(strBuffer, skyboxName);

        /// CREATE SKY CUBE
        skyNode = wSkyBoxCreate(skyTextureTop,   skyTextureBottom,
                                skyTextureLeft,  skyTextureRight,
                                skyTextureFront, skyTextureBack);
    }

    ///////////////////////////////////////////////////
    // ���
    ///////////////////////////////////////////////////
    // ���� �� ���������� ���������� ���
    if (!map->showFloor) goto no_floor;

    // ��������� ���������� �� �����������
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            el  = map->floor[i][j];
            lEl = map->level[i][j];
            // ����� ����������, ���� ���� ������� ���� � �� ���� ������� ��� �����
            if ((el > 0) && (lEl == 0 || lEl >= TEXTURES_MAX_COUNT))
                mergeInfo[i][j] = el;
            else
                mergeInfo[i][j] = 0;
        }
    }

    // ������ ������ ������ ��� ����������� ����� �� ����
    for (j = 0; j < MAP_HEIGHT; ++j)
    {
        for (i = 0; i < MAP_WIDTH; ++i)
        {
            el = mergeInfo[i][j];
            // ���� ������� ����� ����������
            if (el != 0)
            {
                pos  = i;               // ��������� ��� �������
                lenX = lenY = 1;        // ����� � ������ ��� ������� � ���� �������
                pEl  = el;              // �������� ����� ������� �� ����
                mergeInfo[i][j] = 0;    // ��������, ��� ������� ������� ����� ���������

                // ��������� ��������� �� ������� ������� ������
                if (pos < MAP_WIDTH - 1)
                {
                    tEl = mergeInfo[pos + 1][j];
                    // ���� �� ����� ��
                    if (tEl == pEl)
                    {
                        // �� ����� �� ������, ���� �� ���������� ������� ��� �� ��������� �� ���-�� ������
                        bool proceed = true;
                        for (x = pos + 1; (x < MAP_WIDTH) && proceed; ++x)
                        {
                            el = mergeInfo[x][j];
                            if (el != pEl)          // ���-�� ������
                            {
                                proceed = false;    // �����
                                continue;
                            }

                            // ��������� ������� ����� ��
                            mergeInfo[x][j] = 0;    // ��������
                            ++lenX;                 // ���������� ������
                        }
                    }
                }

                // ��������� ��������� �� ������� ������� �����
                if (j < MAP_HEIGHT - 1)
                {
                    tEl = mergeInfo[pos][j + 1];
                    // ���� �� ����� ��
                    if (tEl == pEl)
                    {
                        // ����� �� ���������� �� ������
                        bool proceed = true;
                        for (y = j + 1; (y < MAP_HEIGHT) && proceed; ++y)
                        {
                            for (x = pos; (x < pos + lenX) && proceed; ++x)
                            {
                                el = mergeInfo[x][y];
                                if (el != pEl)          // ���-�� ������
                                {
                                    proceed = false;    // �����
                                    continue;
                                }
                            }

                            if (!proceed)
                                continue;

                            // ���� ����� ����, ������ ��� ����� ��������� � �������
                            // ���������� ������ � �������� ����
                            for (x = pos; x < pos + lenX; ++x)
                                mergeInfo[x][y] = 0;

                            ++lenY;
                        }
                    }
                }

                sprintf(strBuffer, "floor node %d", count);
                ListAddElement(meshes, CreateFloor(strBuffer, textures[--pEl].frames[0], pos, -j, lenX, lenY));

                // ��������� lenX ��������� �� ����� 0, ��� ��� �������������
                i = lenX - 1;
                ++count;
            }
        }
    }
no_floor:


    ///////////////////////////////////////////////////
    // �������
    ///////////////////////////////////////////////////
    // ���� �� �������� ���������� ���
    if (!map->showCeil) goto no_ceil;

    // ��������� ���������� �� �����������
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            el  = map->ceil [i][j];
            lEl = map->level[i][j];
            // ����� ����������, ���� ���� ������� ���� � �� ���� ������� ��� �����
            if ((el > 0) && (lEl == 0 || lEl >= TEXTURES_MAX_COUNT))
                mergeInfo[i][j] = el;
            else
                mergeInfo[i][j] = 0;
        }
    }

    // ������ ������ ������ ��� ����������� ����� �� ����
    for (j = 0; j < MAP_HEIGHT; ++j)
    {
        for (i = 0; i < MAP_WIDTH; ++i)
        {
            el = mergeInfo[i][j];
            // ���� ������� ����� ����������
            if (el != 0)
            {
                pos  = i;               // ��������� ��� �������
                lenX = lenY = 1;        // ����� � ������ ��� ������� � ���� �������
                pEl  = el;              // �������� ����� ������� �� ����
                mergeInfo[i][j] = 0;    // ��������, ��� ������� ������� ����� ���������

                // ��������� ��������� �� ������� ������� ������
                if (pos < MAP_WIDTH - 1)
                {
                    tEl = mergeInfo[pos + 1][j];
                    // ���� �� ����� ��
                    if (tEl == pEl)
                    {
                        // �� ����� �� ������, ���� �� ���������� ������� ��� �� ��������� �� ���-�� ������
                        bool proceed = true;
                        for (x = pos + 1; (x < MAP_WIDTH) && proceed; ++x)
                        {
                            el = mergeInfo[x][j];
                            if (el != pEl)          // ���-�� ������
                            {
                                proceed = false;    // �����
                                continue;
                            }

                            // ��������� ������� ����� ��
                            mergeInfo[x][j] = 0;    // ��������
                            ++lenX;                 // ���������� ������
                        }
                    }
                }

                // ��������� ��������� �� ������� ������� �����
                if (j < MAP_HEIGHT - 1)
                {
                    tEl = mergeInfo[pos][j + 1];
                    // ���� �� ����� ��
                    if (tEl == pEl)
                    {
                        // ����� �� ���������� �� ������
                        bool proceed = true;
                        for (y = j + 1; (y < MAP_HEIGHT) && proceed; ++y)
                        {
                            for (x = pos; (x < pos + lenX) && proceed; ++x)
                            {
                                el = mergeInfo[x][y];
                                if (el != pEl)          // ���-�� ������
                                {
                                    proceed = false;    // �����
                                    continue;
                                }
                            }

                            if (!proceed)
                                continue;

                            // ���� ����� ����, ������ ��� ����� ��������� � �������
                            // ���������� ������ � �������� ����
                            for (x = pos; x < pos + lenX; ++x)
                                mergeInfo[x][y] = 0;

                            ++lenY;
                        }
                    }
                }

                sprintf(strBuffer, "ceil node %d", count);
                ListAddElement(meshes, CreateCeil(strBuffer, textures[--pEl].frames[0], pos, -j, lenX, lenY));

                // ��������� lenX ��������� �� ����� 0, ��� ��� �������������
                i = lenX - 1;
                ++count;
            }
        }
    }
no_ceil:

    // ������� ������������ ������, ���� � ���
    if (g_WorldCollider == NULL)
        g_WorldCollider = wCollisionGroupCreate();

    ///////////////////////////////////////////////////
    // �������
    ///////////////////////////////////////////////////

    // ������ ������ ���������� ���
    /*
    if (textureNodes != NULL)
        ListClear(textureNodes);
    else
        textureNodes = ListCreate();
    */
    if (textureNodes == NULL)
        textureNodes = ListCreate();

    // ������ � ��� ���� ������ �����. ����� �������� �� ���� � ������� ��������� ������� �����
    wTexture* tTexture = NULL;  // ����������� ��������
    // ���������� �� ������ ��������
    if (meshes->size > 0)
    {
        for (i = 0; i < TEXTURES_MAX_COUNT; ++i)
        {
            if (textures[i].framesCount > 0)
            {
                tTexture = textures[i].frames[0];
                batch = wMeshCreateBatching();
                // ���� ���� � ����� �� ���������
                for (SListElement* element = meshes->first; element; element = element->next)
                {
                    mesh = element->value;
                    if (mesh == NULL)
                        continue;
                    material = wMeshBufferGetMaterial(wMeshGetBuffer(mesh, 0, 0));
                    texture = wMaterialGetTexture(material, 0);
                    if (texture == tTexture)
                        wMeshAddToBatching(batch, mesh, wVECTOR3f_ZERO, wVECTOR3f_ZERO, wVECTOR3f_ONE);
                }

                wMeshFinalizeBatching(batch);
                node = wNodeCreateFromBatchingMesh(batch);

                TTextureNode* textureNode = TextureNodeCreate(&(textures[i]), node);
                ListAddElement(textureNodes, textureNode);

                // ��������� �������� � ������������ ������
                wSelector* col = wCollisionCreateFromBatchingMesh(batch, node);
                wCollisionGroupAddCollision(g_WorldCollider, col);
            }

            // ������ �� �� ����� ��� ������� �������
            if (texturesShadowed[i].framesCount > 0)
            {
                tTexture = texturesShadowed[i].frames[0];
                batch = wMeshCreateBatching();
                // ���� ���� � ����� �� ���������
                for (SListElement* element = meshes->first; element; element = element->next)
                {
                    mesh = element->value;
                    if (mesh == NULL)
                        continue;
                    material = wMeshBufferGetMaterial(wMeshGetBuffer(mesh, 0, 0));
                    texture = wMaterialGetTexture(material, 0);
                    if (texture == tTexture)
                        wMeshAddToBatching(batch, mesh, wVECTOR3f_ZERO, wVECTOR3f_ZERO, wVECTOR3f_ONE);
                }

                wMeshFinalizeBatching(batch);
                node = wNodeCreateFromBatchingMesh(batch);

                TTextureNode* textureNode = TextureNodeCreate(&(texturesShadowed[i]), node);
                ListAddElement(textureNodes, textureNode);

                // ��������� �������� � ������������ ������
                wSelector* col = wCollisionCreateFromBatchingMesh(batch, node);
                wCollisionGroupAddCollision(g_WorldCollider, col);
            }
        }
    }

    // ��������� �������� � ������������ ������
    if (g_WorldCollider == NULL)
        g_WorldCollider = wCollisionGroupCreate();
    /*for (SListElement* element = textureNodes->first; element; element = element->next)
    {
        if (element == NULL)
            continue;
        node = ((TTextureNode*)(element->value))->node;
        if (node == NULL)
            continue;
        mesh = wNodeGetMesh(node);
        if (mesh == NULL)
            continue;
        wSelector* col = wCollisionCreateFromMesh(mesh, node, 0);
        //wCollisionGroupAddCollision(g_WorldCollider, col);
    }*/
/*
    // ��������� �������� � ������������ ������
    for (SListElement* element = meshes->first; element; element = element->next)
    {
        mesh = element->value;
        if (mesh == NULL)
            continue;
        node = wNodeCreateFromMesh(mesh);
        wSelector* col = wCollisionCreateFromMesh(mesh, node, 0);
        wCollisionGroupAddCollision(g_WorldCollider, col);
    }
*/
    for (SListElement* element = spriteNodes->first; element; element = element->next)
    {
        if (element == NULL)
            continue;
        /*
        node = ((TSpriteNode*)(element->value))->solidBox;
        if (node == NULL)
            continue;
        mesh = wNodeGetMesh(node);
        if (mesh == NULL)
            continue;
        wSelector* col = wCollisionCreateFromMesh(mesh, node, 0);
        wCollisionGroupAddCollision(g_WorldCollider, col);
        */
        wSelector* physBody = ((TSpriteNode*)(element->value))->physBody;
        if (physBody)
            wCollisionGroupAddCollision(g_WorldCollider, physBody);
    }

    ListDestroy(&meshes);

    wSceneDestroyAllUnusedMeshes();


    // ����???
    if (map->showFog)
    {
        float dest = sqrt(SQR(MAP_WIDTH) + SQR(MAP_HEIGHT));
        wSceneSetFog(wUtilUIntToColor4s(map->fogColor), wFT_COUNT, 0, dest - map->fogIntensity * dest, map->fogIntensity, true, false);
    }
}
