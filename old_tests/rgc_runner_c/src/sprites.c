
#include "res_manager.h"
#include "util.h"
#include "global.h"
#include "sprites.h"
#include "SampleFunctions.h"

//TSpriteInMem sprites[g_SpritesCount] = {};
TSpriteInMem* sprites = NULL;
SList*        spriteNodes = NULL;
/*
typedef struct
{
    uint8_t axisParams;     // ������� �� ����: 0 = ������� �� ������, 1 = �������� �� �����������, 2 = �������� �� ���������
    bool    collision;      // ����� �����������
    bool    destroyable;    // �����������?
    uint8_t endurance;      // ���������
    char    animSpeed;      // �������� ������ � �������
    bool    frm_flags[SPRITE_FRAMES_COUNT];
} TSpriteInFile;
*/

void InitSprites()
{
    TSpriteInMem* spr = NULL;
    wTexture*     txr = NULL;
    int i, j;

    if (sprites == NULL)
    {
        sprites = calloc(sizeof(TSpriteInMem), SPRITES_MAX_COUNT);
        return;
    }

    for (i = 0; i < SPRITES_MAX_COUNT; ++i)
    {
        spr = sprites + i;
        for (j = 0; j < spr->framesCount; ++j)
        {
            txr = spr->frames[j];
            if (txr != 0)
            {
                wTextureDestroy(txr);
                spr->frames[j] = NULL;
            }
        }
        spr->axisParams  = 0;
        spr->collision   = true;
        spr->destroyable = false;
        spr->endurance   = 0;
        spr->animSpeed   = 0;
        spr->framesCount = 0;
        free(spr->frames);
        spr->frames = NULL;
    }

    //wSceneDestroyAllTextures();
}

bool LoadSpritePack()
{
    /*
    // ��������� ����������� ����
    int32_t id  = 0;
    int16_t ver = 0;
    bool    flags[g_SpritesCount] = {0};

    bool    isLoaded = false;

    FILE*   f = NULL;
    char    strBuffer[256];

    wTexture* loadTexture = NULL;
    void*     loadTextureBits = NULL;

    TSpriteInMem* sprite = NULL;
    TSpriteInFile loadSpriteInfo;

    if (!wFileIsExist(SPRITES_PACK_NAME))
    {
        PrintWithColor("Pack of sprites is doesn''t exist!\n", wCFC_RED, false);
        goto end;
    }

    f = fopen(SPRITES_PACK_NAME, "rb");
    if (f == NULL)
    {
        PrintWithColor("Error when opening pack of sprites!\n", wCFC_RED, false);
        goto end;
    }
    fseek(f, 0, SEEK_SET);

    // ������ ID � �����
    fread(&id,   sizeof(id),    1, f);
    fread(&ver,  sizeof(ver),   1, f);
    fread(flags, sizeof(flags), 1, f);

    if (id != SPRITES_PACK_ID || ver != SPRITES_PACK_VER)
    {
        PrintWithColor("Unknown format of sprites pack!\n", wCFC_RED, false);
        goto end;
    }

    InitSprites();

    int i, j;
    int32_t w, h;
    int textureSize;
    for (i = 0; i < g_SpritesCount; ++i)
    {
        // ���� �� ����
        if (flags[i])
        {
            // ������ ���� �� �������
            fread(&loadSpriteInfo, sizeof(TSpriteInFile), 1, f);

            // ����� ��� ��������
            if (texturesNeedForLoad[i + TEXTURES_MAX_COUNT])
            {
                sprite = &(sprites[i]);

                sprite->axisParams  = loadSpriteInfo.axisParams;
                sprite->collision   = loadSpriteInfo.collision;
                sprite->destroyable = loadSpriteInfo.destroyable;
                sprite->endurance   = loadSpriteInfo.endurance;
                sprite->animSpeed   = loadSpriteInfo.animSpeed;

                // ������ �����, ���� ����
                for (j = 0; j < SPRITE_FRAMES_COUNT; ++j)
                {
                    if (loadSpriteInfo.frm_flags[j])
                    {
                        // ������ �������
                        fread(&w, sizeof(w), 1, f);
                        fread(&h, sizeof(h), 1, f);
                        textureSize = w*h*sizeof(int32_t);

                        sprintf(strBuffer, "Sprite_%d_Frame_%d", i, j);
                        sprite->frames[j] = LoadTextureFromPack(strBuffer, (wVector2i){w, h}, textureSize, f);
                    }
                }
            }
            // � ����� �������� ���������� ��� ��������
            else
            {
                // ������ �����, ���� ����
                for (j = 0; j < SPRITE_FRAMES_COUNT; ++j)
                {
                    if (loadSpriteInfo.frm_flags[j])
                    {
                        // ������ �������
                        fread(&w, sizeof(w), 1, f);
                        fread(&h, sizeof(h), 1, f);
                        textureSize = w*h*sizeof(int32_t);
                        fseek(f, textureSize, SEEK_CUR);
                    }
                }
            }
        }
    }
    isLoaded = true;

end:
    if (f != NULL)
        fclose(f);
    f = NULL;
    return isLoaded;
    */

    bool    isLoaded = false;
    char    strBuffer[256];
/*
    wTexture* loadTexture = NULL;
    void*     loadTextureBits = NULL;
*/
    TSpriteInMem* sprite;

    uint16_t w, h;
    void*    buffer = NULL;
    int32_t  bufferSize = 0;

    /*
    if (!wFileIsExist(SPRITES_PACK_NAME))
    {
        PrintWithColor("Pack of sprites is doesn''t exist!\n", wCFC_RED, false);
        goto end;
    }

    if (!Res_OpenZipForRead(SPRITES_PACK_NAME))
    {
        PrintWithColor("Error when opening pack of sprites for read!\n", wCFC_RED, false);
        goto end;
    }
    */

    // ������ ���������� �������
    bufferSize = Res_GetUncompressedFileSize("sprites.cfg");
    if (bufferSize <= 0)
    {
        PrintWithColor("can not get size of sprites.cfg\n", wCFC_RED, false);
        goto end;
    }
    buffer = malloc(bufferSize);
    if (!Res_ReadFileFromZipToBuffer("sprites.cfg", buffer, bufferSize))
    {
        PrintWithColor("can not read sprites.cfg\n", wCFC_RED, false);
        goto end;
    }
    if (!Res_OpenIniFromBuffer(buffer, bufferSize))
    {
        PrintWithColor("can not open sprites.cfg!!!\n", wCFC_RED, false);
        goto end;
    }
    g_SpritesCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_SpritesCount == 0)
        goto end;

    free(buffer);
    buffer = NULL;

    InitSprites();

//    int32_t color;
    char    spriteName[256];
    int     i, j;
    uint8_t  animSpeed;
    uint32_t framesCount;
    uint8_t  axisParams;
    bool     collision;
    bool     destroyable;
    uint8_t  endurance;
    int      textureSize;

    for (i = 0; i < g_SpritesCount; ++i)
    {
        // ���� �� ����� ��� ��������
        if (!g_SpritesNeedForLoad[i])
            continue;

        // ������ ���� �� ��������
        sprintf(strBuffer, "sprite_%d.cfg", i);
        bufferSize = Res_GetUncompressedFileSize(strBuffer);
        if (bufferSize <= 0)
        {
            sprintf(strBuffer, "can not get size of sprite_%d.cfg", i);
            PrintWithColor(strBuffer, wCFC_RED, false);
            continue;
        }

        sprite = &(sprites[i]);

        buffer = malloc(bufferSize);
        Res_ReadFileFromZipToBuffer(strBuffer, buffer, bufferSize);
        Res_OpenIniFromBuffer(buffer, bufferSize);
        animSpeed   = Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = Res_IniReadInteger("Options", "frames_count",    0);
        axisParams  = Res_IniReadInteger("Options", "axis",            0);
        collision   = Res_IniReadBool   ("Options", "collision",       false);
        destroyable = Res_IniReadBool   ("Options", "destroyable",     false);
        endurance   = Res_IniReadInteger("Options", "endurance",       0);
        Res_CloseIni();
        free(buffer);
        buffer = NULL;

        sprite->animSpeed   = animSpeed;
        sprite->framesCount = framesCount;
        sprite->axisParams  = axisParams;
        sprite->collision   = collision;
        sprite->destroyable = destroyable;
        sprite->endurance   = endurance;
        if (framesCount == 0)
            continue;

        sprite->frames = calloc(sizeof(wTexture*), framesCount);

        // ������ �����, ���� ����
        for (j = 0; j < framesCount; ++j)
        {
            /*
            sprintf(strBuffer, "sprite_%d_frame_%d.dat", i, j);
            bufferSize = Res_GetUncompressedFileSize(strBuffer);
            if (bufferSize <= 0)
            {
                sprintf(strBuffer, "can not get size of sprite_%d_frame_%d.dat", i, j);
                PrintWithColor(strBuffer, wCFC_RED, false);
                continue;
            }
            buffer = malloc(bufferSize);

            if (!Res_ReadFileFromZipToBuffer(strBuffer, buffer, bufferSize))
            {
                free(buffer);
                buffer = NULL;
                continue;
            }

            // ������ �������
            w = *(uint16_t*)buffer;
            h = *(uint16_t*)(buffer + 2);
            textureSize = w * h * sizeof(uint32_t);

            sprintf(strBuffer, "Sprite_%d_Frame_%d", i, j);
            sprite->frames[j] = LoadTextureFromMemory(strBuffer, (wVector2i){w, h}, textureSize, buffer + sizeof(w) + sizeof(h));

            free(buffer);
            buffer = NULL;
            */

            sprintf(strBuffer, "sprite_%d_frame_%d.dat", i, j);
            sprintf(spriteName, "Sprite_%d_Frame_%d", i, j);
            sprite->frames[j] = LoadTexture(strBuffer, spriteName);

        }
    }
    isLoaded = true;

end:
    if (buffer)
        free(buffer);
    return isLoaded;
}


TSpriteNode* SpriteNodeCreate(TSpriteInMem* index, wNode* solidBox, wNode* billboard)
{
    TSpriteInMem sprInfo = *index;
    wBillboardAxisParam axisParams;
    wMaterial* material = NULL;

    TSpriteNode* node = calloc(sizeof(TSpriteNode), 1);
    node->index       = index;
    node->destroyable = sprInfo.destroyable;
    node->health      = sprInfo.endurance;
    node->animSpeed   = sprInfo.animSpeed;
    node->solidBox    = solidBox;
    node->billboard   = billboard;
    if (solidBox)
    {
        wMesh* mesh = wNodeGetMesh(solidBox);
        node->physBody = wCollisionCreateFromMesh(mesh, node->solidBox, 0);
    }

    // ������������� ��������� �������� �������
    switch (sprInfo.axisParams)
    {
        // �������� �� ������
        case 0 :
            axisParams.isEnablePitch = false;
            axisParams.isEnableYaw   = true;
            axisParams.isEnableRoll  = false;
            break;

        // �������� �� ����������� (XoY)
        case 1 :
            axisParams.isEnablePitch = true;
            axisParams.isEnableYaw   = true;
            axisParams.isEnableRoll  = false;
            material = wNodeGetMaterial(billboard, 0);
            wMaterialSetFlag(material, wMF_BACK_FACE_CULLING, false);
            break;

        // �������� �� ��������� (ZoY)
        case 2 :
            axisParams.isEnablePitch = false;
            axisParams.isEnableYaw   = true;
            axisParams.isEnableRoll  = true;
            material = wNodeGetMaterial(billboard, 0);
            wMaterialSetFlag(material, wMF_BACK_FACE_CULLING, false);
            break;
    }
    wBillboardSetEnabledAxis(billboard, axisParams);

    return node;
}

void SpriteNodeUpdate(TSpriteNode* node)
{
    if (node->animSpeed == 0.0f)
        return;

    if (node->_animSpeed < 1.0f)
    {
        node->_animSpeed += wTimerGetDelta()*node->animSpeed;
    }
    else
    {
        node->_animSpeed = 0.0f;

        // ��������� �� ��������� ����
        ++(node->curFrame);
        if (node->curFrame >= node->index->framesCount)
            node->curFrame = 0;

        // ���� ��� ������ ��������, �� ���� �� ��������� �����
        wTexture* frames = node->index->frames;
        wTexture* txr = frames[node->curFrame];
        while (txr == NULL)
        {
            ++(node->curFrame);
            if (node->curFrame >= node->index->framesCount)
                node->curFrame = 0;

            txr = frames[node->curFrame];
        }

        wMaterial* mat = wNodeGetMaterial(node->billboard, 0);
        wMaterialSetTexture(mat, 0, txr);
    }
}

void SpriteNodesUpdate()
{
    if (spriteNodes == NULL || spriteNodes->size == 0)
        return;

    for (SListElement* element = spriteNodes->first; element; element = element->next)
        if (element->value)
            SpriteNodeUpdate(element->value);
}

void SpriteNodeDestroy(TSpriteNode** node)
{
    wNode* solidBox = (**node).solidBox;
    if (solidBox != NULL)
    {
        wMesh* mesh = wNodeGetMesh(solidBox);
        if (mesh != NULL)
            wMeshDestroy(mesh);
        wNodeDestroy(solidBox);
    }

    wNode* billboard = (**node).billboard;
    if (billboard != NULL)
    {
        wNodeDestroy(billboard);
    }

    free(*node);
    *node = NULL;
}
