#include "global.h"
#include "textures.h"
#include "util.h"
#include "res_manager.h"
#include "SampleFunctions.h"

/*
TTextureInMem textures[TEXTURES_MAX_COUNT] = {0};
TTextureInMem texturesShadowed[TEXTURES_MAX_COUNT] = {0};
*/
TTextureInMem* textures = NULL;
TTextureInMem* texturesShadowed = NULL;
SList* textureNodes = NULL;

/*
typedef struct
{
    char    animSpeed;      // �������� ������ � �������
    bool    frm_flags[SPRITE_FRAMES_COUNT];
} TTextureInFile;
*/

#if 0
wTexture* LoadTextureFromPack(const char* name, wVector2i size, size_t sizeForRead, FILE* file)
{
    if (file == NULL)
        return NULL;

    wTexture* loadTexture = NULL;
    void* loadTextureBits = NULL;

    // ��� ���������� ������� ����� ������������� ������ � ARGB1555
    if (g_Renderer == wDRT_SOFTWARE/* || g_Renderer == wDRT_BURNINGS_VIDEO*/)
    {
        int32_t  c32;
        int16_t* c16 = NULL;

        loadTexture = wTextureCreate(name, size, wCF_A1R5G5B5);
        c16 = wTextureLock(loadTexture);
        for (int i = 0; i < (sizeForRead / sizeof(int32_t)); ++i, ++c16)
        {
            fread(&c32, sizeof(int32_t), 1, file);
            // ���� ����� ����, �� ������ ������� ���������
            if ((c32 >> 24) == 0)
            {
                *c16 = 0;
            }
            else
            {
                *c16 = ARGB8888toARGB1555(c32);
            }
        }
        wTextureUnlock(loadTexture);
    }
    // ��� ���� ��������� �������� ������ ��������� �������� ��� ��� ����
    else
    {
        loadTexture = wTextureCreate(name, size, wCF_A8R8G8B8);
        loadTextureBits = wTextureLock(loadTexture);
        fread(loadTextureBits, sizeForRead, 1, file);
        wTextureUnlock(loadTexture);
    }

    return loadTexture;
}
#endif

void InitTextures()
{
    int i, j;
    wTexture*      wTxr;
    TTextureInMem* txr;

    if (textures == NULL && texturesShadowed == NULL)
    {
        textures = calloc(sizeof(TTextureInMem), TEXTURES_MAX_COUNT);
        texturesShadowed = calloc(sizeof(TTextureInMem), TEXTURES_MAX_COUNT);
        return;
    }

    for (i = 0; i < TEXTURES_MAX_COUNT; ++i)
    {
        txr = textures + i;
        for (j = 0; j < txr->framesCount; ++j)
        {
            wTxr = txr->frames[j];
            if (wTxr != NULL)
            {
                wTextureDestroy(wTxr);
                txr->frames[j] = NULL;
            }
        }
        txr->animSpeed = 0;
        txr->framesCount = 0;
        free(txr->frames);
        txr->frames = NULL;

        txr = texturesShadowed + i;
        for (j = 0; j < txr->framesCount; ++j)
        {
            wTxr = txr->frames[j];
            if (wTxr != NULL)
            {
                wTextureDestroy(wTxr);
                txr->frames[j] = NULL;
            }
        }
        txr->animSpeed = 0;
        txr->framesCount = 0;
        free(txr->frames);
        txr->frames = NULL;
    }

    //wSceneDestroyAllTextures();
}

bool LoadTexturePack()
{
    bool    isLoaded = false;
    char    strBuffer[256];

    wTexture* loadTexture = NULL;
    wTexture* loadTextureShadowed = NULL;
    void*     loadTextureBits = NULL;

    TTextureInMem* texture;
    TTextureInMem* textureShadowed;

    uint16_t w, h;
    void*    buffer = NULL;
    int32_t  bufferSize = 0;

    /*
    if (!wFileIsExist(TEXTURES_PACK_NAME))
    {
        PrintWithColor("Pack of wall's textures is doesn''t exist!\n", wCFC_RED, false);
        goto end;
    }

    if (!Res_OpenZipForRead(TEXTURES_PACK_NAME))
    {
        PrintWithColor("Error when opening pack of wall's textures for read!\n", wCFC_RED, false);
        goto end;
    }
    */

    // ������ ���������� �������
    bufferSize = Res_GetUncompressedFileSize("textures.cfg");
    if (bufferSize <= 0)
    {
        PrintWithColor("can not get size of textures.cfg\n", wCFC_RED, false);
        goto end;
    }
    buffer = malloc(bufferSize);
    if (!Res_ReadFileFromZipToBuffer("textures.cfg", buffer, bufferSize))
    {
        PrintWithColor("can not read textures.cfg\n", wCFC_RED, false);
        goto end;
    }
    if (!Res_OpenIniFromBuffer(buffer, bufferSize))
    {
        PrintWithColor("can not open textures.cfg!!!\n", wCFC_RED, false);
        goto end;
    }
    g_TexturesCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_TexturesCount == 0)
        goto end;

    free(buffer);
    buffer = NULL;

    InitTextures();

    int32_t color;
    char    textureName[256];
    int     i, j;
    uint8_t  animSpeed;
    uint32_t framesCount;
    int      textureSize;
    for (i = 0; i < g_TexturesCount; ++i)
    {
        // ���� �� ����� ��� ��������
        if (!g_TexturesNeedForLoad[i])
            continue;

        // ������ ���� �� ��������
        sprintf(strBuffer, "texture_%d.cfg", i);
        bufferSize = Res_GetUncompressedFileSize(strBuffer);
        if (bufferSize <= 0)
        {
            sprintf(strBuffer, "can not get size of texture_%d.cfg", i);
            PrintWithColor(strBuffer, wCFC_RED, false);
            continue;
        }

        texture = &(textures[i]);
        textureShadowed = &(texturesShadowed[i]);

        buffer = malloc(bufferSize);
        Res_ReadFileFromZipToBuffer(strBuffer, buffer, bufferSize);
        Res_OpenIniFromBuffer(buffer, bufferSize);
        animSpeed   = Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = Res_IniReadInteger("Options", "frames_count",  0);
        Res_CloseIni();
        free(buffer);
        buffer = NULL;

        texture->animSpeed   = animSpeed;
        texture->framesCount = framesCount;
        textureShadowed->animSpeed   = animSpeed;
        textureShadowed->framesCount = framesCount;
        if (framesCount == 0)
            continue;

        texture->frames         = calloc(sizeof(wTexture*), framesCount);
        textureShadowed->frames = calloc(sizeof(wTexture*), framesCount);

        // ������ �����, ���� ����
        for (j = 0; j < framesCount; ++j)
        {
/*
            sprintf(strBuffer, "texture_%d_frame_%d.dat", i, j);
            bufferSize = Res_GetUncompressedFileSize(strBuffer);
            if (bufferSize <= 0)
            {
                sprintf(strBuffer, "can not get size of texture_%d_frame_%d.dat", i, j);
                PrintWithColor(strBuffer, wCFC_RED, false);
                continue;
            }
            buffer = malloc(bufferSize);

            if (!Res_ReadFileFromZipToBuffer(strBuffer, buffer, bufferSize))
            {
                free(buffer);
                buffer = NULL;
                continue;
            }

            // ������ �������
            w = *(uint16_t*)buffer;
            h = *(uint16_t*)(buffer + 2);
            textureSize = w * h * sizeof(uint32_t);

            sprintf(strBuffer, "Texture_%d_Frame_%d", i, j);
            texture->frames[j] = LoadTextureFromMemory(strBuffer, (wVector2i){w, h}, textureSize, buffer + sizeof(w) + sizeof(h));
            loadTextureShadowed = wTextureCopy(texture->frames[j], "shadowed");
            loadTextureBits = wTextureLock(loadTextureShadowed);
            for (int b = 0; b < textureSize / sizeof(int32_t); ++b)
            {
                color = ((int32_t*)loadTextureBits)[b];
                color = (color >> 1) & 8355711;
                ((int32_t*)loadTextureBits)[b] = color;
            }
            wTextureUnlock(loadTextureShadowed);
            textureShadowed->frames[j] = loadTextureShadowed;

            free(buffer);
            buffer = NULL;
*/

            sprintf(strBuffer, "texture_%d_frame_%d.dat", i, j);
            sprintf(textureName, "Texture_%d_Frame_%d", i, j);
            texture->frames[j] = LoadTexture(strBuffer, textureName);


            /* ������ ����ͨ���� ������� */
            wVector2u size;
            uint32_t pitch;
            wColorFormat colorFormat;

            wTextureGetInformation(texture->frames[j], &size, &pitch, &colorFormat);
            textureSize = size.x * size.y * sizeof(int32_t);

            loadTextureShadowed = wTextureCopy(texture->frames[j], "shadowed");
            loadTextureBits = wTextureLock(loadTextureShadowed);
            for (int b = 0; b < textureSize / sizeof(int32_t); ++b)
            {
                color = ((int32_t*)loadTextureBits)[b];
                color = (color >> 1) & 8355711;
                ((int32_t*)loadTextureBits)[b] = color;
            }
            wTextureUnlock(loadTextureShadowed);
            textureShadowed->frames[j] = loadTextureShadowed;
        }
    }
    isLoaded = true;

end:
    if (buffer)
        free(buffer);
    return isLoaded;
}



TTextureNode* TextureNodeCreate(TTextureInMem* index, wNode* object)
{
    TTextureInMem texInfo = *index;

    TTextureNode* texNode = calloc(sizeof(TTextureNode), 1);
    texNode->index        = index;
    texNode->animSpeed    = texInfo.animSpeed;
    texNode->node         = object;
    return texNode;
}

void TextureNodeUpdate(TTextureNode* texNode)
{
    if (texNode->animSpeed == 0.0f)
        return;

    if (texNode->_animSpeed < 1.0f)
    {
        texNode->_animSpeed += wTimerGetDelta()*texNode->animSpeed;
    }
    else
    {
        texNode->_animSpeed = 0.0f;

        // ��������� �� ��������� ����
        ++(texNode->curFrame);
        if (texNode->curFrame >= texNode->index->framesCount)
            texNode->curFrame = 0;

        // ���� ��� ������ ��������, �� ���� �� ��������� �����
        wTexture* frames = texNode->index->frames;
        wTexture* txr = frames[texNode->curFrame];
        while (txr == NULL)
        {
            ++(texNode->curFrame);
            if (texNode->curFrame >= texNode->index->framesCount)
                texNode->curFrame = 0;

            txr = frames[texNode->curFrame];
        }

        wMaterial* mat = wNodeGetMaterial(texNode->node, 0);
        wMaterialSetTexture(mat, 0, txr);
    }
}

void TextureNodesUpdate()
{
    if (textureNodes == NULL || textureNodes->size == 0)
        return;

    for (SListElement* element = textureNodes->first; element; element = element->next)
        if (element->value)
            TextureNodeUpdate(element->value);
}

void TextureNodeDestroy(TTextureNode** texNode)
{
    wNode* node = (**texNode).node;
    if (node != NULL)
    {
        wMesh* mesh = wNodeGetMesh(node);
        if (mesh != NULL)
            wMeshDestroy(mesh);
        wNodeDestroy(node);
    }

    free(*texNode);
    *texNode = NULL;
}
