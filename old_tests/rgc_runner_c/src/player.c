#include <stdlib.h>

#include "player.h"
#include "global.h"
#include "constants.h"
#include "duk_util.h"


TPlayer* player = NULL;
TCamera* camera = NULL;


void PlayerCreate(float x, float y, float z)
{
    if (player != NULL)
        PlayerDestroy();

    player = calloc(sizeof(TPlayer), 1);
    player->health = 100.0f;
    player->node = wNodeCreateCylinder(8, 0.3, 0.8, wCOLOR4s_RED);
    player->pos = (wVector3f){x, y, z};
    wNodeSetPosition(player->node, player->pos);

    wMaterial* mat = wNodeGetMaterial(player->node, 0);
    MaterialSetOldSchool(mat);

    player->physBody = wCollisionCreateFromBox(player->node);
    wNodeAddCollision(player->node, player->physBody);

    player->physAnim = wAnimatorCollisionResponseCreate(g_WorldCollider, player->node, 0.05f);

    ///Get & Set collision animator parameters
    wAnimatorCollisionResponse params;
    wAnimatorCollisionResponseGetParameters(player->physAnim, &params);
    params.gravity = (wVector3f){0, 0.0f, 0};
    params.ellipsoidRadius = (wVector3f){0.3, 0.8, 0.3};
    //params.ellipsoidTranslation = (wVector3f){0, 25, 0};
    wAnimatorCollisionResponseSetParameters(player->physAnim, params);
}

void PlayerDestroy()
{
    if (player == NULL)
        return;

    if (player->physAnim) wAnimatorDestroy(player->node, player->physAnim);
    //if (player->physBody) wCollision
    if (player->node)     wNodeDestroy(player->node);
    free(player);
    player = NULL;
}


static void CameraUpdateAngles()
{
    if (camera == NULL)
        return;

    if (camera->pitch < 0.0f) camera->pitch += 360.0;
    if (camera->yaw   < 0.0f) camera->yaw   += 360.0;
    //if (camera->roll  < 0.0f) camera->roll  += 360.0;

    if (camera->pitch >= 360.0f) camera->pitch -= 360.0;
    if (camera->yaw   >= 360.0f) camera->yaw   -= 360.0;
    //if (camera->roll  >= 360.0f) camera->roll  -= 360.0;

    float pitchRad = camera->pitch * DEG_TO_RAD_COEFF;
    float yawRad   = camera->yaw   * DEG_TO_RAD_COEFF;

    wVector3f target = {camera->pos.x - sin(yawRad),
                        camera->pos.y + tan(pitchRad),
                        camera->pos.z - cos(yawRad)};
    wCameraSetTarget(camera->node, target);


    /*
    if (camera->roll != 0.0f)
    {
        float camera->rollRad  = camera->roll  * DEG_TO_RAD_COEFF;
        wVector3f upVector = {sin(camera->rollRad), -cos(camera->rollRad), 0.0f};
        //wVector3f target = {-sin(camera->pitchRad),
        //                     tan(camera->yawRad),
        //                    -cos(camera->pitchRad)};
        wCameraSetUpDirection(playerNode, upVector);
    }
    */
}

void CameraCreate(float x, float y, float z)
{
    if (camera != NULL)
        CameraDestroy();

    camera = calloc(sizeof(TCamera), 1);

    camera->pos = (wVector3f){x, y, z};
    camera->node = wCameraCreate(camera->pos, wVECTOR3f_ZERO);

    wCameraSetClipDistance(camera->node, ((MAP_WIDTH * MAP_HEIGHT) >> 1), 0.001f);
    CameraUpdateAngles();
}

void CameraDestroy()
{
    if (camera == NULL)
        return;

    if (camera->node) wNodeDestroy(camera->node);

    free(camera);
    camera = NULL;
}

/*
void CameraSetAngles(wVector3f angles)
{
    if (playerNode != NULL)
    {
        camera->pitch = angles.x;
        camera->yaw   = angles.y;
        camera->roll  = angles.z;

        CameraUpdateAngles();
    }
}
*/
void CameraSetPitch(float deg)
{
    if (camera != NULL)
    {
        camera->pitch = deg;

        CameraUpdateAngles();
    }
}

void CameraSetYaw(float deg)
{
    if (camera != NULL)
    {
        camera->yaw = deg;

        CameraUpdateAngles();
    }
}
/*
void CameraSetRoll(float deg)
{
    if (playerNode != NULL)
    {
        camera->roll = deg;

        CameraUpdateAngles();
    }
}
*/
/*
wVector3f CameraGetAngles()
{
    if (playerNode == NULL)
        return wVECTOR3f_ZERO;

    return (wVector3f){camera->pitch, camera->yaw, camera->roll};
}
*/

float CameraGetPitch()
{
    if (camera == NULL)
        return 0.0f;

    return camera->pitch;
}

float CameraGetYaw()
{
    if (camera == NULL)
        return 0.0f;

    return camera->yaw;
}
/*
float CameraGetRoll()
{
    if (playerNode == NULL)
        return 0.0f;

    return camera->roll;
}
*/


/** INTERPRETER */
/** POSITION */
duk_ret_t n_camera_set_position(duk_context *ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(3);

    if (camera != NULL)
    {
        camera->pos = (wVector3f){duk_to_number(ctx, 0), duk_to_number(ctx, 1), duk_to_number(ctx, 2)};

        wNodeSetPosition(camera->node, camera->pos);
    }

    return 0;
}
duk_ret_t n_camera_get_position_x(duk_context *ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    if (camera != NULL)
    {
        camera->pos = wNodeGetPosition(camera->node);
        duk_push_number(ctx, camera->pos.x);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_camera_get_position_y(duk_context *ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    if (camera != NULL)
    {
        camera->pos = wNodeGetPosition(camera->node);
        duk_push_number(ctx, camera->pos.y);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_camera_get_position_z(duk_context *ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    if (camera != NULL)
    {
        camera->pos = wNodeGetPosition(camera->node);
        duk_push_number(ctx, camera->pos.z);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}


/** TARGET */
/*duk_ret_t n_camera_set_target(duk_context *ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(3);

    if (playerNode != NULL)
    {
        wVector3f target;
        target.x = duk_to_number(ctx, 0);
        target.y = duk_to_number(ctx, 1);
        target.z = duk_to_number(ctx, 2);

        wCameraSetTarget(playerNode, target);
    }

    return 0;
}
duk_ret_t n_camera_get_target_x(duk_context *ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    if (playerNode != NULL)
    {
        wVector3f target = wCameraGetTarget(playerNode);
        duk_push_number(ctx, target.x);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_camera_get_target_y(duk_context *ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    if (playerNode != NULL)
    {
        wVector3f target = wCameraGetTarget(playerNode);
        duk_push_number(ctx, target.y);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_camera_get_target_z(duk_context *ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    if (playerNode != NULL)
    {
        wVector3f target = wCameraGetTarget(playerNode);
        duk_push_number(ctx, target.z);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}*/

/** ANGLES */
/*
duk_ret_t n_camera_set_angles(duk_context* ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(3);

    if (playerNode != NULL)
    {
        CameraSetAngles((wVector3f){duk_to_number(ctx, 0), duk_to_number(ctx, 1), duk_to_number(ctx, 2)});
    }

    return 0;
}
*/
duk_ret_t n_camera_set_pitch(duk_context* ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(1);

    if (camera != NULL)
    {
        camera->pitch = duk_to_number(ctx, 0);
        CameraSetPitch(camera->pitch);
    }

    return 0;
}
duk_ret_t n_camera_set_yaw(duk_context* ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(1);

    if (camera != NULL)
    {
        camera->yaw = duk_to_number(ctx, 0);
        CameraSetYaw(camera->yaw);
    }

    return 0;
}
/*
duk_ret_t n_camera_set_roll(duk_context* ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(1);

    if (playerNode != NULL)
    {
        camera->roll = duk_to_number(ctx, 0);
        CameraSetRoll(camera->roll);
    }

    return 0;
}
*/

//duk_ret_t n_camera_get_angles(duk_context* ctx)
duk_ret_t n_camera_get_pitch(duk_context* ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    if (camera != NULL)
    {
        duk_push_number(ctx, camera->pitch);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_camera_get_yaw(duk_context* ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    if (camera != NULL)
    {
        duk_push_number(ctx, camera->yaw);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
/*
duk_ret_t n_camera_get_roll(duk_context* ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    if (playerNode != NULL)
    {
        duk_push_number(ctx, camera->roll);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
*/

/** PLAYER */
/** POSITION */
duk_ret_t n_player_set_position(duk_context *ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(3);

    if (player != NULL)
    {
        player->pos = (wVector3f){duk_to_number(ctx, 0), duk_to_number(ctx, 1), duk_to_number(ctx, 2)};
        wNodeSetPosition(player->node, player->pos);
    }

    return 0;
}
duk_ret_t n_player_get_position_x(duk_context *ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    if (player != NULL)
    {
        //wVector3f pos = wNodeGetPosition(playerNode);
        //duk_push_number(ctx, pos.x);
        player->pos = wNodeGetPosition(player->node);
        duk_push_number(ctx, player->pos.x);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_player_get_position_y(duk_context *ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    if (player != NULL)
    {
        //wVector3f pos = wNodeGetPosition(playerNode);
        //duk_push_number(ctx, pos.x);
        player->pos = wNodeGetPosition(player->node);
        duk_push_number(ctx, player->pos.y);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_player_get_position_z(duk_context *ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    if (player != NULL)
    {
        //wVector3f pos = wNodeGetPosition(playerNode);
        //duk_push_number(ctx, pos.x);
        player->pos = wNodeGetPosition(player->node);
        duk_push_number(ctx, player->pos.z);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}


void RegisterCameraCFunctions()
{
    DUK_REGISTER_C_FUNCTION(n_camera_set_position,   3, "camera_set_position"  );
    DUK_REGISTER_C_FUNCTION(n_camera_get_position_x, 0, "camera_get_position_x");
    DUK_REGISTER_C_FUNCTION(n_camera_get_position_y, 0, "camera_get_position_y");
    DUK_REGISTER_C_FUNCTION(n_camera_get_position_z, 0, "camera_get_position_z");
/*
    DUK_REGISTER_C_FUNCTION(n_camera_set_target,     3, "camera_set_target"  );
    DUK_REGISTER_C_FUNCTION(n_camera_get_target_x,   0, "camera_get_target_x");
    DUK_REGISTER_C_FUNCTION(n_camera_get_target_y,   0, "camera_get_target_y");
    DUK_REGISTER_C_FUNCTION(n_camera_get_target_z,   0, "camera_get_target_z");
*/
//  DUK_REGISTER_C_FUNCTION(n_camera_set_angles, 3, "camera_set_angles"  );
    DUK_REGISTER_C_FUNCTION(n_camera_set_pitch,  1, "camera_set_pitch");
    DUK_REGISTER_C_FUNCTION(n_camera_set_yaw,    1, "camera_set_yaw");
//  DUK_REGISTER_C_FUNCTION(n_camera_set_roll,   1, "camera_set_roll");
    DUK_REGISTER_C_FUNCTION(n_camera_get_pitch,  0, "camera_get_pitch");
    DUK_REGISTER_C_FUNCTION(n_camera_get_yaw,    0, "camera_get_yaw");
//  DUK_REGISTER_C_FUNCTION(n_camera_get_roll,   0, "camera_get_roll");
}

void RegisterPlayerCFunctions()
{
    DUK_REGISTER_C_FUNCTION(n_player_set_position,   3, "player_set_position"  );
    DUK_REGISTER_C_FUNCTION(n_player_get_position_x, 0, "player_get_position_x");
    DUK_REGISTER_C_FUNCTION(n_player_get_position_y, 0, "player_get_position_y");
    DUK_REGISTER_C_FUNCTION(n_player_get_position_z, 0, "player_get_position_z");
}
