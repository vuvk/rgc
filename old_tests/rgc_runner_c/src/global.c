

#include "constants.h"
#include "global.h"
#include "map.h"
#include "vm/duk_util.h"


double DEG_TO_RAD_COEFF = M_PI / 180.0;
double RAD_TO_DEG_COEFF = 180.0 / M_PI;

uint16_t g_TexturesCount = 0;
uint16_t g_SpritesCount  = 0;
uint16_t g_MapsCount     = 0;

bool g_PreviewMode = false;

wDriverTypes g_Renderer = wDRT_OPENGL;
bool g_LimitFps = true;
bool g_ShowFog  = false;
wTexture* g_InvisibleTexture = NULL;
//wMesh*    invisibleMesh = NULL;
//wNode*    invisibleNode = NULL;

bool g_TexturesNeedForLoad[TEXTURES_MAX_COUNT] = {};
bool g_SpritesNeedForLoad [SPRITES_MAX_COUNT ] = {};

void InitRunner()
{
    if (g_LimitFps)
        wEngineSetFPS(MAX_FPS);

    //wLogSetLevel(wLL_DEBUG);
    ///Show logo WS3D
    //wEngineShowLogo(true);

    wSystemSetTextureCreationFlag(wTCF_CREATE_MIP_MAPS, false);
    //wSystemSetTextureCreationFlag(wTCF_OPTIMIZED_FOR_QUALITY, true);
    wSystemSetTextureCreationFlag(wTCF_ALLOW_NON_POWER_2, false);

    if (g_Renderer == wDRT_SOFTWARE)
    {
        wSystemSetTextureCreationFlag(wTCF_ALWAYS_16_BIT, true);
        wSystemSetTextureCreationFlag(wTCF_NO_ALPHA_CHANNEL, true);
    }

    // ��������� �������������
    /*int res = 0;
    res = InterpreterInitVM();
    switch (res)
    {
        char strBuffer[100];
        sprintf(strBuffer, "Error - %d\n", res);

        case AMX_ERR_NONE     : PrintWithColor("Interpreter loaded.\n",                    wCFC_GREEN, false); break;
        case AMX_ERR_NOTFOUND : PrintWithColor("Main program not founded!\n",              wCFC_RED,   false); break;
        case AMX_ERR_FORMAT   : PrintWithColor("Unknown format of program.\n",             wCFC_RED,   false); break;
        case AMX_ERR_MEMORY   : PrintWithColor("Can not allocate a memory for program.\n", wCFC_RED,   false); break;
        default               : PrintWithColor(strBuffer,                                  wCFC_RED,   false); break;
    }

    res = InterpreterInitNatives();
    if (res != AMX_ERR_NONE)
    {
        printf("Error - %d\n", res);
    }
    */

    Duk_InitVM();
}

void MaterialSetOldSchool(wMaterial* material)
{
    wMaterialSetFlag(material, wMF_LIGHTING, false);
    wMaterialSetFlag(material, wMF_ANISOTROPIC_FILTER, false);
    wMaterialSetFlag(material, wMF_ANTI_ALIASING,      false);
    wMaterialSetFlag(material, wMF_BILINEAR_FILTER,    false);
    wMaterialSetFlag(material, wMF_TRILINEAR_FILTER,   false);
    //wMaterialSetFlag(material, wMF_BACK_FACE_CULLING,  false);
    wMaterialSetFlag(material, wMF_FOG_ENABLE,         g_ShowFog);
}

void PrepareTexturesNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    memset(g_TexturesNeedForLoad, 0, sizeof(g_TexturesNeedForLoad));

    g_ShowFog = map->showFog;

    int i, j;
    int32_t element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map->ceil[i][j];
            if ((element >  0) &&
                (element < TEXTURES_MAX_COUNT))
                g_TexturesNeedForLoad[--element] = true;

            element = map->floor[i][j];
            if ((element >  0) &&
                (element < TEXTURES_MAX_COUNT))
                g_TexturesNeedForLoad[--element] = true;

            element = map->level[i][j];
            if ((element >  0) &&
                (element < TEXTURES_MAX_COUNT))
                g_TexturesNeedForLoad[--element] = true;
        }
    }
}

void PrepareSpritesNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    memset(g_SpritesNeedForLoad, 0, sizeof(g_SpritesNeedForLoad));

    g_ShowFog = map->showFog;

    int i, j;
    int32_t element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map->level[i][j] - TEXTURES_MAX_COUNT;
            if ((element >  0) &&
                (element < SPRITES_MAX_COUNT))
                g_SpritesNeedForLoad[--element] = true;
        }
    }
}
