
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <wchar.h>

#include "WorldSim3D.h"
#include "SampleFunctions.h"

#include "res_manager.h"
#include "global.h"
#include "util.h"
#include "constants.h"
#include "textures.h"
#include "sprites.h"
#include "map.h"
#include "input.h"
#include "player.h"
#include "vm/duk_util.h"


const wchar_t* wndCaption = L"Raycasting Game Constructor Runner";
wchar_t strBuffer[256]      = {0};
Int32 prevFPS                 =0;

int main(int argc, char** args)
{
    g_LimitFps = false;
    // ���������� ���������
    for (int i = 0; i < argc; ++i)
    {
        // ��������� �������
        if (strcmp("-ogl",   args[i]) == 0) { g_Renderer = wDRT_OPENGL;    }
        if (strcmp("-d3d",   args[i]) == 0) { g_Renderer = wDRT_DIRECT3D9; }
        if (strcmp("-soft",  args[i]) == 0) { g_Renderer = wDRT_SOFTWARE;       g_LimitFps = false; }
        if (strcmp("-soft2", args[i]) == 0) { g_Renderer = wDRT_BURNINGS_VIDEO; g_LimitFps = false; }

        if (strcmp("-no_limit_fps", args[i]) == 0) { g_LimitFps = false; }
    }

    ///Start engine
    bool init = wEngineStart(g_Renderer, wDEFAULT_SCREENSIZE, 32, false, true, true, false);
    if (!init)
    {
        PrintWithColor("wEngineStart() failed!",wCFC_RED,true);
        return -1;
    }

    InitRunner();

    // ��������� ���
    int res = Res_OpenZipForRead("resources.pk3");
    switch (res)
    {
        case RES_NOT_FOUND  :
            PrintWithColor("Pack of maps is doesn''t exist!\n", wCFC_RED, false);
            break;
        case RES_NOT_OPENED :
            PrintWithColor("Error when opening pack of maps!\n", wCFC_RED, false);
            break;

        case RES_TRUE :
            //printf("1");
            LoadMapPack();
            //printf("2");
            PrepareTexturesNeedForLoad(0);
            PrepareSpritesNeedForLoad(0);
            //printf("3");
            LoadTexturePack();
            //printf("4");
            LoadSpritePack();
            GenerateLevel(0);
            //printf("7");
            break;
    }
    Res_CloseZipAfterRead();


    PlayerCreate(3.0, 0.0, -3.0);
    CameraCreate(0.0, 0.0, 0.0);


    //wColor4s fogColor={255,240,200,200};
    //wSceneSetFog(fogColor, wFT_LINEAR, 0.0f, 3.0f, 0.5f, false, false);

    // ��������
    //float intensivity = 0.3;
    //float dest = sqrt(SQR(MAP_WIDTH) + SQR(MAP_HEIGHT));
    //wSceneSetFog(wCOLOR4s_BLACK, wFT_COUNT, 0, dest - intensivity * dest, intensivity, true, false);


    //CreateNode1(wVECTOR3f_ZERO,wVECTOR3f_ONE);

    //CreateNode2((wVector3f){0,40,0},(wVector3f){20,20,20});


    //wNode* mainLight = wLightCreate((wVector3f){5, 5, 0.5}, wCOLOR4f_WHITE, 10);
    //wLightSetType(mainLight, wLT_POINT);
    // �� ��������?
    //wLightSetAmbientColor(mainLight, (wColor4f){0, 1, 0, 0});
    //wLightSetCastShadows(mainLight, true);

    wFont* font = wFontLoad("4.png");
    wVector3f camPos;


    TScript tmp;
    /* load key codes */
    Duk_LoadScriptFromFile(&tmp, "scripts/keyboard.js");
    Duk_RunScript(tmp);

    /* load classes */
    Duk_LoadScriptFromFile(&tmp, "scripts/classes.js");
    Duk_RunScript(tmp);

    Duk_FreeScript(&tmp);

    /* player's scripts */
    TScript playerInitScript;
    Duk_LoadScriptFromFile(&playerInitScript, "scripts/player/player_init.js");
    Duk_RunScript(playerInitScript);
    Duk_FreeScript(&playerInitScript);

    TScript playerUpdateScript;
    Duk_LoadScriptFromFile(&playerUpdateScript, "scripts/player/player_update.js");

    while(wEngineRunning())
    {
        wSceneBegin((wColor4s){255, 50, 50, 50});

        SpriteNodesUpdate();
        TextureNodesUpdate();

        wSceneDrawAll();

        camPos = wNodeGetPosition(camera->node);
        swprintf(strBuffer, L"Camera Pos : %.2f %.2f", camPos.x, camPos.z);
        wFontDraw(font, strBuffer, (wVector2i){10, 10}, (wVector2i){100, 20}, wCOLOR4s_WHITE);

        wSceneEnd();

        Duk_RunScript(playerUpdateScript);

        if (wInputIsKeyEventAvailable())
        {

            if (wInputIsKeyUp(wKC_KEY_L))
            {/*
                InitRunner();
                LoadTexturePack();
                LoadSpritePack();
                LoadMapPack();*/
                //GenerateLevel(0);
                //CreateCamera();

                wMesh* batch[1000];
                wMesh* mesh [1000];
                for (int i = 0; i < 1000; ++i)
                {
                    batch[i] = wMeshCreateBatching();
                    wMeshFinalizeBatching(batch[i]);
                }
                for (int i = 0; i < 1000; ++i)
                {
                    wMeshClearBatching(batch[i]);
                    wMeshDestroyBatching(batch[i]);
                    wMeshDestroy(mesh[i]);
                }

            }

            if (wInputIsKeyUp(wKC_KEY_K))
            {
                wSceneDestroyAllMeshes();
            }
        }

        ///Close by ESC
        wEngineCloseByEsc();

        /// update FPS
        if (prevFPS != wEngineGetFPS())
        {
            prevFPS = wEngineGetFPS();
            swprintf(strBuffer, L"%s  FPS : %d", wndCaption, prevFPS);
            wWindowSetCaption(strBuffer);
        }
    }

    ///Stop engine
    wEngineStop();

    //InterpreterCloseVM();
    Duk_StopVM();

    return 0;
}
