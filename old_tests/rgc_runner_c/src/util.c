#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "global.h"
#include "res_manager.h"

bool FileExists(const char* fileName)
{
    FILE* f = fopen(fileName, "r");
    if (f != NULL)
    {
        fclose(f);
        f = NULL;
        return true;
    }

    return false;
}


uint32_t ARGB1555toARGB8888(uint16_t c)
{
    const uint32_t a = c&0x8000, r = c&0x7C00, g = c&0x03E0, b = c&0x1F;
    const uint32_t rgb = (r << 9) | (g << 6) | (b << 3);
    return (a*0x1FE00) | rgb | ((rgb >> 5) & 0x070707);
}

uint16_t ARGB8888toARGB1555(uint32_t c)
{
    return (uint16_t)((((c>>16)&0x8000) | ((c>>9)&0x7C00) | ((c>>6)&0x03E0) | ((c>>3)&0x1F)));
}


/*static*/ wTexture* LoadTextureFromMemory(const char* name, wVector2i size, size_t sizeForRead, void* buffer)
{
    if (buffer == NULL)
        return NULL;

    wTexture* loadTexture = NULL;
    void* loadTextureBits = NULL;

    // ��� ���������� ������� ����� ������������� ������ � ARGB1555
    if (g_Renderer == wDRT_SOFTWARE/* || g_Renderer == wDRT_BURNINGS_VIDEO*/)
    {
        int32_t  c32;
        int16_t* c16 = NULL;

        loadTexture = wTextureCreate(name, size, wCF_A1R5G5B5);
        c16 = wTextureLock(loadTexture);
        for (int i = 0; i < (sizeForRead / sizeof(int32_t)); ++i, ++c16)
        {
            c32 = ((int32_t*)buffer)[i];
            // ���� ����� ����, �� ������ ������� ���������
            if ((c32 >> 24) == 0)
            {
                *c16 = 0;
            }
            else
            {
                *c16 = ARGB8888toARGB1555(c32);
            }
        }
        wTextureUnlock(loadTexture);
    }
    // ��� ���� ��������� �������� ������ ��������� �������� ��� ��� ����
    else
    {
        loadTexture = wTextureCreate(name, size, wCF_A8R8G8B8);
        loadTextureBits = wTextureLock(loadTexture);
        memcpy(loadTextureBits, buffer, sizeForRead);
        wTextureUnlock(loadTexture);
    }

    return loadTexture;
}
/*
wTexture* LoadTextureFromZip(const char* fileName, const char* textureName)
{
    wTexture* txr = NULL;
    char*  buffer = NULL;
    int    bufferSize = 0;
    uint16_t w, h;
    uint32_t textureSize;

    bufferSize = Res_GetUncompressedFileSize(fileName);
    if (bufferSize == 0)
        goto end_func;

    buffer = calloc(bufferSize, 1);
    if (buffer == NULL)
        goto end_func;

    Res_ReadFileFromZipToBuffer(fileName, buffer, bufferSize);

    // ������ �������
    w = *(uint16_t*)buffer;
    h = *(uint16_t*)(buffer + 2);
    textureSize = w * h * sizeof(uint32_t);

    txr = LoadTextureFromMemory(textureName, (wVector2i){w, h}, textureSize, buffer + sizeof(w) + sizeof(h));


end_func:
    if (buffer)
    {
        free(buffer);
        buffer = NULL;
    }

    return txr;
}

wTexture* LoadTextureFromDat(const char* fileName, const char* textureName)
{
    wTexture* txr = NULL;
    FILE* f = NULL;
    char*  buffer = NULL;
    int    bufferSize = 0;
    uint16_t w, h;
    uint32_t textureSize;

    if (!FileExists(fileName))
        goto end_func;

    f = fopen(fileName, "rb");
    fseek(f, 0, SEEK_END);
    bufferSize = ftell(f);
    fseek(f, 0, SEEK_SET);

    buffer = calloc(bufferSize, 1);
    if (buffer == NULL)
        goto end_func;

    fread(buffer, bufferSize, 1, f);

    // ������ �������
    w = *(uint16_t*)buffer;
    h = *(uint16_t*)(buffer + 2);
    textureSize = w * h * sizeof(uint32_t);

    txr = LoadTextureFromMemory(textureName, (wVector2i){w, h}, textureSize, buffer + sizeof(w) + sizeof(h));


end_func:
    if (f)
    {
        fclose(f);
        f = NULL;
    }

    if (buffer)
    {
        free(buffer);
        buffer = NULL;
    }

    return txr;
}
*/


wTexture* LoadTexture(const char* fileName, const char* textureName)
{
    wTexture* txr = NULL;
    char*    buffer = NULL;
    uint32_t bufferSize = 0;
    uint16_t w, h;
    uint32_t textureSize;

    if (g_PreviewMode)
    {
        char strBuffer[256] = "resources/textures/";
        strcat(strBuffer, fileName);

        if (!Res_ReadFileToBuffer(&buffer, &bufferSize, strBuffer))
            goto end_func;
    }
    else
    {
        bufferSize = Res_GetUncompressedFileSize(fileName);
        if (bufferSize == 0)
            goto end_func;

        buffer = calloc(bufferSize, 1);
        if (buffer == NULL)
            goto end_func;

        if (!Res_ReadFileFromZipToBuffer(fileName, buffer, bufferSize))
            goto end_func;
    }

    // ������ �������
    w = *(uint16_t*)buffer;
    h = *(uint16_t*)(buffer + 2);
    textureSize = w * h * sizeof(uint32_t);

    txr = LoadTextureFromMemory(textureName, (wVector2i){w, h}, textureSize, buffer + sizeof(w) + sizeof(h));


end_func:
    if (buffer)
    {
        free(buffer);
        buffer = NULL;
    }

    return txr;
}
