#include <stdlib.h>
#include "list.h"


SList* ListCreate()
{
    return calloc(sizeof(SList), 1);
}

void ListAddElement(SList* list, void* value)
{
    if (list == NULL)
        return;

    SListElement* element = calloc(sizeof(SListElement), 1);
    element->value = value;

    if (list->count == 0)
    {
        list->first = element;
        list->last  = element;
    }
    else
    {
        list->last->next = element;     // �� ��������� ��������� ������ ���
        element->prev = list->last;     // � �������� ���������� - ������ ���������
        list->last = element;           // � ������ ��������� ���� ���
    }

    ++list->count;
}

int ListGetCount(SList* list)
{
    if (list == NULL)
        return -1;

    return list->count;
}

void ListClear(SList* list)
{
    if (list == NULL)
        return;

    for (SListElement* element = list->last; element; element = element->prev)
    {
        element->value = NULL;
        if (element->next != NULL) free(element->next);
    }

    if (list->first != NULL) free(list->first);
    if (list->last  != NULL) free(list->last);
    list->first = list->last = NULL;

    list->count = 0;
}

void ListDestroy(SList** list)
{
    if (list == NULL || *list == NULL)
        return;

    ListClear(*list);
    free(*list);
    *list = NULL;
}
