#pragma once
#ifndef __RES_MANAGER_H
#define __RES_MANAGER_H

#include <stdbool.h>
#include <stdint.h>

#define RES_TRUE          1
#define RES_FALSE         0
#define RES_ERROR        -1
#define RES_NOT_FOUND    -2
#define RES_NOT_OPENED   -3
#define RES_NOT_READABLE -4
#define RES_NOT_WRITABLE -5

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

int32_t Res_OpenZipForRead(const char* zipName);
int32_t Res_GetCompressedFileSize(const char* fileName);
int32_t Res_GetUncompressedFileSize(const char* fileName);
int32_t Res_ReadFileFromZipToBuffer(const char* fileName, void* buffer, int32_t bufferSize);
bool    Res_CloseZipAfterRead();
bool    Res_OpenZipForWrite(const char* zipName, bool isAppend);
int32_t Res_WriteFileFromBufferToZip(const char* fileName, const void* buffer, uint32_t bufferSize, int32_t compressionLevel);
bool    Res_CloseZipAfterWrite();

bool    Res_PackAllFilesFromDirToZip(const char* path, int32_t compressionLevel);
bool    Res_UnpackAllFilesFromZipToDir(const char* zipName, const char* path, bool rewrite);
bool    Res_ReadFileToBuffer(void** buffer, int32_t* bufferSize, const char* fileName);
bool    Res_WriteBufferToFile(void* buffer, int32_t bufferSize, const char* fileName, bool rewrite);
void    Res_DeleteAllFilesInDir(const char* dirName, bool deleteDir);


bool    Res_OpenIniFromFile(const char* fileName);
bool    Res_OpenIniFromBuffer(const void* buffer, uint32_t bufferSize);
char*   Res_IniReadString(const char* section, const char* key, const char* defaultValue);
int32_t Res_IniReadInteger(const char* section, const char* key, int32_t defaultValue);
int64_t Res_IniReadInteger64(const char* section, const char* key, int64_t defaultValue);
bool    Res_IniReadBool(const char* section, const char* key, bool defaultValue);
double  Res_IniReadDouble(const char* section, const char* key, double defaultValue);
void    Res_IniWriteString(const char* section, const char* key, const char* value);
void    Res_IniWriteInteger(const char* section, const char* key, int32_t value);
void    Res_IniWriteInteger64(const char* section, const char* key, int64_t value);
void    Res_IniWriteBool(const char* section, const char* key, bool value);
void    Res_IniWriteDouble(const char* section, const char* key, double value);
bool    Res_SaveIniToBuffer(void** buffer, int32_t* bufferSize);
bool    Res_SaveIniToFile(const char* fileName);
void    Res_CloseIni();

#ifdef __cplusplus
}
#endif // __cplusplus

#endif // __RES_MANAGER_H
