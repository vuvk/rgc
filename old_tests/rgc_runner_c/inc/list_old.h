#pragma once
#ifndef __LIST_H
#define __LIST_H


typedef struct tag_SListElement
{
    void* prev;
    void* next;
    void* value;
} SListElement;

typedef struct
{
    SListElement* first;
    SListElement* last;
    int count;
} SList;


SList* ListCreate();
void ListAddElement(SList* list, void* value);
int ListGetCount(SList* list);
void ListClear(SList* list);
void ListDestroy(SList** list);

#endif // __LIST_H
