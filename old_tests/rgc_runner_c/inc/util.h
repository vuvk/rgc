
#pragma once
#ifndef __UTIL_H
#define __UTIL_H

#include <stdint.h>
#include <stdbool.h>

#include "WorldSim3D.h"
#include "global.h"

#define SQR(a) ((a)*(a))

bool FileExists(const char* fileName);

uint32_t ARGB1555toARGB8888(uint16_t c);
uint16_t ARGB8888toARGB1555(uint32_t c);


/*
wTexture* LoadTextureFromZip(const char* fileName, const char* textureName);
wTexture* LoadTextureFromDat(const char* fileName, const char* textureName);
*/
wTexture* LoadTextureFromMemory(const char* name, wVector2i size, size_t sizeForRead, void* buffer);
wTexture* LoadTexture(const char* fileName, const char* textureName);

#endif // __UTIL_H
