#ifndef SAMPLEFUNCTIONS_H_INCLUDED
#define SAMPLEFUNCTIONS_H_INCLUDED

#include <WorldSim3D.h>

///Custom utils
bool CheckFilePath(char* path);

#ifdef __cplusplus
void PrintWithColor(char* text,
					wConsoleFontColor color=wCFC_RED,
					bool waitKey=false);
#else
void PrintWithColor(char* text,
					wConsoleFontColor color,
					bool waitKey);
#endif // __cplusplus

///Quake map utils
void ParseQuakeMap(wMesh* BSPMesh);
void SetQuakeShadersVisible(bool value);

///Второй вариант парсинга bsp-карты
void ParseQuakeMap2(wMesh* BSPMesh);

///For load resources
wMesh* wMeshLoadEx(char* path);

///For animated meshes
void wMeshesUpdateTangentsAndBinormal();

///For color texture
wTexture* CreateColorTexture(wColor4s tColor);

void OutlineNode(wNode* node,Float32 Width,wColor4s lineColor);

wNode* CreateBullet(Float32 speed,wTexture* texture);

bool MousePick(bool isPick);

void CreateGround(Float32 scale);

wSound* wSoundLoadEx(char* pathFile,const char* extFile);

#ifdef __cplusplus
typedef struct
{
    public:

    UInt32 size();

    UInt32* get(UInt32 idx);

    Int32 getIndex(UInt32* element);

    bool add(UInt32* element);

    bool insert(UInt32* element, UInt32 idx);

    bool remove(UInt32* element,bool isNodeDestroy=false);

    bool remove(UInt32 idx,bool isNodeDestroy=false);

    void clear(bool isNodeDestroy=false);

    private:
    UInt32** Array=0;
    UInt32   ArraySize=0;

}wArray;
#else
typedef UInt32 wArray;
wArray* wArrayCreate();
UInt32 wArrayGetSize(wArray* array);
void* wArrayGetElement(wArray* array, UInt32 idx);
Int32 wArrayGetIndex(wArray* array, void* element);
bool wArrayAddElement(wArray* array, void* element);
bool wArrayInsertElement(wArray* array, void* element, UInt32 idx);
bool wArrayRemoveElement(wArray* array, UInt32* element, bool isNodeDestroy);
bool wArrayRemoveElementByIndex(wArray* array, UInt32 idx, bool isNodeDestroy);
void wArrayClear(wArray* array, bool isNodeDestroy);
void wArrayDestroy(wArray** array, bool isNodeDestroy);
#endif // __cplusplus


 #endif // SAMPLEFUNCTIONS_H_INCLUDED
