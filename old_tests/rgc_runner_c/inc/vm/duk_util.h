#pragma once
#ifndef __DUK_UTIL_H_INCLUDED
#define __DUK_UTIL_H_INCLUDED

#include "duktape.h"

extern duk_context* ctx;

#define DUK_CHECK_NARGS_EQUAL(n)     if (duk_get_top(ctx) == n) return DUK_RET_TYPE_ERROR;
#define DUK_CHECK_NARGS_NOT_EQUAL(n) if (duk_get_top(ctx) != n) return DUK_RET_TYPE_ERROR;
#define DUK_CHECK_NARGS_GREATER(n)   if (duk_get_top(ctx) >  n) return DUK_RET_TYPE_ERROR;
#define DUK_CHECK_NARGS_SMALLER(n)   if (duk_get_top(ctx) <  n) return DUK_RET_TYPE_ERROR;
#define DUK_REGISTER_C_FUNCTION(native_function, nargs, new_name) duk_push_c_function(ctx, native_function, nargs); duk_put_global_string(ctx, new_name);

typedef struct
{
    char* fileName;
    char* text;
} TScript;

void Duk_InitVM();
void Duk_StopVM();
void Duk_GC();
void Duk_RegisterCFunction(duk_c_function func, duk_idx_t nargs, const char* new_name);
void Duk_LoadScriptFromFile(TScript* script, const char* fileName);
void Duk_FreeScript(TScript* script);
void Duk_RunScript(TScript script);

#endif // __DUK_UTIL_H_INCLUDED
