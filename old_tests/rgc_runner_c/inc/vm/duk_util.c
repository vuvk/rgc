
#include "constants.h"
#include "duk_util.h"
#include "WorldSim3D.h"
#include "input.h"
#include "player.h"



duk_context* ctx = NULL;

/*
static duk_ret_t n_gc(duk_context* ctx)
{
    duk_gc(ctx, DUK_GC_COMPACT);
    duk_push_boolean(ctx, 1);
    return 1;
}
*/

/*
static duk_ret_t n_pi(duk_context* ctx)
{
    duk_push_number(ctx, M_PI);
    return 1;
}

static duk_ret_t n_pi_2(duk_context* ctx)
{
    duk_push_number(ctx, M_PI_2);
    return 1;
}

static duk_ret_t n_pi_4(duk_context* ctx)
{
    duk_push_number(ctx, M_PI_4);
    return 1;
}
*/

static duk_ret_t n_deg_to_rad(duk_context* ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(1);

    double deg = duk_to_number(ctx, 0);
    duk_push_number(ctx, deg*DEG_TO_RAD_COEFF);
    return 1;
}

static duk_ret_t n_rad_to_deg(duk_context* ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(1);

    double rad = duk_to_number(ctx, 0);
    duk_push_number(ctx, rad*RAD_TO_DEG_COEFF);
    return 1;
}



/** SYSTEM */
static duk_ret_t n_print(duk_context *ctx)
{
	duk_push_string(ctx, " ");
	duk_insert(ctx, 0);
	duk_join(ctx, duk_get_top(ctx) - 1);
	printf("%s\n", duk_safe_to_string(ctx, -1));
	return 0;
}
static duk_ret_t n_deltatime(duk_context* ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    duk_push_number(ctx, wTimerGetDelta());
    return 1;
}
static duk_ret_t n_window_get_width(duk_context* ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    wVector2u wndSize;
    wWindowGetSize(&wndSize);
    duk_push_int(ctx, wndSize.x);
    return 1;
}
static duk_ret_t n_window_get_height(duk_context* ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    wVector2u wndSize;
    wWindowGetSize(&wndSize);
    duk_push_int(ctx, wndSize.y);
    return 1;
}




/** PLAYER */

/** CAMERA */



void Duk_InitVM()
{
    ctx = duk_create_heap_default();

    /*
    DUK_REGISTER_C_FUNCTION(n_pi, 0, "pi");
    DUK_REGISTER_C_FUNCTION(n_pi_2, 0, "pi_2");
    DUK_REGISTER_C_FUNCTION(n_pi_4, 0, "pi_4");
    */
    //DUK_REGISTER_C_FUNCTION(n_gc, 0, "GC");
    DUK_REGISTER_C_FUNCTION(n_deg_to_rad, 1, "deg_to_rad");
    DUK_REGISTER_C_FUNCTION(n_rad_to_deg, 1, "rad_to_deg");

    /** system */
    DUK_REGISTER_C_FUNCTION(n_print, DUK_VARARGS, "print");
    DUK_REGISTER_C_FUNCTION(n_deltatime, 0, "deltatime");
    DUK_REGISTER_C_FUNCTION(n_window_get_width,  0, "window_get_width" );
    DUK_REGISTER_C_FUNCTION(n_window_get_height, 0, "window_get_height");

    /** keyboard */
    RegisterKeyboardCFunctions();

    /** mouse */
    RegisterMouseCFunctions();

    /** player */
    RegisterPlayerCFunctions();

    /** camera */
    RegisterCameraCFunctions();
}

void Duk_StopVM()
{
    duk_destroy_heap(ctx);
    ctx = NULL;
}

void Duk_GC()
{
    duk_gc(ctx, DUK_GC_COMPACT);
}

void Duk_RegisterCFunction(duk_c_function func, duk_idx_t nargs, const char* new_name)
{
    DUK_REGISTER_C_FUNCTION(func, nargs, new_name);
}

void Duk_LoadScriptFromFile(TScript* script, const char* fileName)
{
    if (script == NULL)
        return;

    FILE* file = NULL;
    int len;

    len = strlen(fileName);
    free(script->fileName);
    script->fileName = calloc(len + 1, 1);
    memcpy(script->fileName, fileName, len);

    file = fopen(fileName, "r");
    if (file != NULL)
    {
        fseek(file, 0, SEEK_END);
        len = ftell(file);
        fseek(file, 0, SEEK_SET);
        script->text = calloc(len, 1);
        fread(script->text, 1, len, file);
        fclose(file);
        file = NULL;
    }
}

void Duk_FreeScript(TScript* script)
{
    if (script)
    {
        free(script->fileName);
        free(script->text);
        script->fileName = NULL;
        script->text = NULL;
    }
}

void Duk_RunScript(TScript script)
{
    if (duk_peval_string(ctx, script.text) != 0)
		printf("Error in script '%s': %s\n", script.fileName, duk_safe_to_string(ctx, -1));

    //?
    //duk_pop(ctx);
}
