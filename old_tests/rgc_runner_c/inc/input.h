#ifndef __INPUT_H
#define __INPUT_H

#include "WorldSim3D.h"
#include "duk_util.h"

/*
extern wVector2i mousePos, oldMousePos;
*/

/** KEYBOARD */
void RegisterKeyboardCFunctions();

/** MOUSE */
void RegisterMouseCFunctions();

#endif // __INPUT_H
