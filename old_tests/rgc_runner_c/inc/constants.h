
#pragma once
#ifndef __CONSTANTS_H
#define __CONSTANTS_H

#include <stdint.h>
#undef __STRICT_ANSI__
#include <math.h>
#include "WorldSim3D.h"

#define MAX_FPS         60

#define MAP_WIDTH       64
#define MAP_HEIGHT      64
//#define MAPS_COUNT      20
//#define MAP_PACK_NAME   "MAPS.PAK"
#define MAPS_PACK_NAME  "maps.zip"
//#define MAP_PACK_VER    1
//#define MAP_PACK_ID     0x5F4B504D   // 'M' 'P' 'K' '_'

// ��������� ������ TGA
#define TEXTURE_MAX_WIDTH  256
#define TEXTURE_MAX_HEIGHT 256
#define TEXTURE_MAX_SIZE   TEXTURE_MAX_WIDTH*TEXTURE_MAX_HEIGHT*sizeof(int32_t)

//#define TEXTURES_COUNT      60
#define TEXTURES_MAX_COUNT  1000
//#define TEXTURE_FRAMES_COUNT 5
//#define TEXTURES_PACK_NAME  "TEXTURES.PAK"
#define TEXTURES_PACK_NAME  "textures.zip"
//#define TEXTURES_PACK_VER   1
//#define TEXTURES_PACK_ID    0x5F4B5054   // 'T' 'P' 'K' '_'

//#define SPRITES_COUNT       30
#define SPRITES_MAX_COUNT   1000
//#define SPRITE_FRAMES_COUNT 5
//#define SPRITES_PACK_NAME   "SPRITES.PAK"
#define SPRITES_PACK_NAME   "sprites.zip"
//#define SPRITES_PACK_VER    1
//#define SPRITES_PACK_ID     0x5F4B5053   // 'S' 'P' 'K' '_'

// ������ �������� � ����?
#define OBJECTS_WIDTH    10
#define OBJECTS_HEIGHT   10



extern double DEG_TO_RAD_COEFF;
extern double RAD_TO_DEG_COEFF;

#endif // __CONSTANTS_H
