#pragma once
#ifndef __GLOBAL_H
#define __GLOBAL_H

#include "constants.h"
#include "WorldSim3D.h"

extern uint16_t g_TexturesCount;
extern uint16_t g_SpritesCount;
extern uint16_t g_MapsCount;

extern bool g_PreviewMode;

extern wDriverTypes g_Renderer;   /* ��������, � ������� ����� ������� ������ */
extern bool g_LimitFps;           /* ���������� ����� ������ � MAX_FPS */
extern bool g_ShowFog;            /* ���������� �� ����� */

extern wTexture* g_InvisibleTexture;
//extern wMesh*    invisibleMesh;
//extern wNode*    invisibleNode;

extern wSelector* g_WorldCollider;  /* ��������� ���������� ���� */

/* ����� ��� ���� �������� �������, ������� ����� ������� */
extern bool g_TexturesNeedForLoad[TEXTURES_MAX_COUNT];
extern bool g_SpritesNeedForLoad [SPRITES_MAX_COUNT];

// ������������� ����������� - ����������� ���������� ��������
void InitRunner();
// ��������� ��������� ��� �������������
void MaterialSetOldSchool(wMaterial* material);

// ����������� �� ����� � ��������� g_TexturesNeedForLoad
void PrepareTexturesNeedForLoad(int numOfMap);
void PrepareSpritesNeedForLoad(int numOfMap);
#endif // __GLOBAL_H
