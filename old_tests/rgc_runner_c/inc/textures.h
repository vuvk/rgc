
#pragma once
#ifndef __TEXTURES_H
#define __TEXTURES_H

#include "list.h"
#include "constants.h"
#include "global.h"
#include "global_types.h"

#include "WorldSim3D.h"

//typedef TTexture TWallTexture;

// ��������-������, ����������� �� ����
typedef struct
{
    uint8_t  animSpeed;      // ���-�� ������ � �������
    uint32_t framesCount;
    //wTexture* frames[TEXTURE_FRAMES_COUNT];
    wTexture** frames;
} TTextureInMem;

// ���� �� ������ �������, ����������� �� ����� �� ������� �� ����, � ������� ����� �������� ����
typedef struct
{
    TTextureInMem* index;   // ������ �������� (������� � ������� textures ��� texturesShadowed)
    int     curFrame;       // ������� ����
    float   animSpeed;      // �������� ��������
    float   _animSpeed;     // �������� �������� �������� � ������ �����
    wNode*  node;
} TTextureNode;

/*
extern TTextureInMem textures[TEXTURES_MAX_COUNT];
extern TTextureInMem texturesShadowed[TEXTURES_MAX_COUNT];
*/

extern TTextureInMem* textures;
extern TTextureInMem* texturesShadowed;
extern SList* textureNodes;

/*
wTexture* LoadTextureFromPack(const char* name, wVector2i size, size_t sizeForRead, FILE* file);
*/

void InitTextures();
bool LoadTexturePack();


// ���������� ����������� ������ (�������� ���������)
TTextureNode* TextureNodeCreate(TTextureInMem* index, wNode* object);
void TextureNodeUpdate(TTextureNode* texNode);
void TextureNodesUpdate();
void TextureNodeDestroy(TTextureNode** texNode);

#endif // __TEXTURES_H
