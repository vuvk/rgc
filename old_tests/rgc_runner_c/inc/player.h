#pragma once
#ifndef __PLAYER_H
#define __PLAYER_H


#include "WorldSim3D.h"
#include "duktape.h"

typedef struct
{
    float  health;
    wVector3f pos;

    wNode* node;
    wSelector* physBody;
    wAnimator* physAnim;
} TPlayer;

typedef struct
{
    wVector3f pos;

    float pitch;
    float yaw;
    float roll;

    wNode* node;
} TCamera;

extern TPlayer* player;
extern TCamera* camera;


/** INTERPRETER */
/*
duk_ret_t n_camera_set_position(duk_context *ctx);
duk_ret_t n_camera_get_position_x(duk_context *ctx);
duk_ret_t n_camera_get_position_y(duk_context *ctx);
duk_ret_t n_camera_get_position_z(duk_context *ctx);
*/
/*
duk_ret_t n_camera_set_target(duk_context *ctx);
duk_ret_t n_camera_get_target_x(duk_context *ctx);
duk_ret_t n_camera_get_target_y(duk_context *ctx);
duk_ret_t n_camera_get_target_z(duk_context *ctx);
*/
/*
duk_ret_t n_camera_set_angles(duk_context* ctx);
duk_ret_t n_camera_set_pitch(duk_context* ctx);
duk_ret_t n_camera_set_yaw(duk_context* ctx);
//duk_ret_t n_camera_set_roll(duk_context* ctx);
//duk_ret_t n_camera_get_angles(duk_context* ctx);
duk_ret_t n_camera_get_pitch(duk_context* ctx);
duk_ret_t n_camera_get_yaw(duk_context* ctx);
//duk_ret_t n_camera_get_roll(duk_context* ctx);
*/

void PlayerCreate(float x, float y, float z);
void PlayerDestroy();

void CameraCreate(float x, float y, float z);
void CameraDestroy();


void RegisterPlayerCFunctions();
void RegisterCameraCFunctions();


#endif // __PLAYER_H
