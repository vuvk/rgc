
#pragma once
#ifndef __MAP_H
#define __MAP_H

#include "constants.h"
#include "global_types.h"
#include "WorldSim3D.h"

typedef uint16_t TMapElement;
#pragma pack (push, 1)
// �����
typedef struct
{
    int32_t floorColor;     // ���� ����
    int32_t ceilColor;      // ���� �������
    int32_t fogColor;       // ���� ������
    float   fogIntensity;   // ������������� ������
    bool    showFloor;      // ���������� ���������� ���?
    bool    showCeil;       // ���������� ���������� �������?
    bool    showFog;        // ���������� �����?
    bool    showSky;        // ���������� ����?
    int16_t skyNumber;      // ����� ���������
    TMapElement ceil [MAP_WIDTH][MAP_HEIGHT];    // ������ � ��������� ������� �������
    TMapElement level[MAP_WIDTH][MAP_HEIGHT];    // ������ � ���������� ������ (�����, �������, ����� � �.�.)
    TMapElement floor[MAP_WIDTH][MAP_HEIGHT];    // ������ � ��������� ������� ����
} TMap;
  // ��� ����
typedef struct
{
    //int32_t id;     // MAP_PACK_ID
    TMap*   maps;
} TMapPack;
#pragma pack (pop)

extern TMapPack mapPack;

// ������ � ����� ����
void InitMapPack();
bool LoadMapPack();

void ClearLevel();
void GenerateLevel(int numOfMap);

#endif // __MAP_H
