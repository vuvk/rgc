#pragma once
#ifndef __SPRITES_H
#define __SPRITES_H

#include "list.h"
#include "constants.h"
#include "global_types.h"
#include "WorldSim3D.h"

// ������-������, ����������� �� ����
typedef struct
{
    uint8_t  axisParams;     // ������� �� ����: 0 = ������� �� ������, 1 = �������� �� �����������, 2 = �������� �� ���������
    bool     collision;      // ����� �����������
    bool     destroyable;    // �����������?
    uint8_t  endurance;      // ���������
    char     animSpeed;      // ���-�� ������ � �������
    //wTexture* frames[SPRITE_FRAMES_COUNT];
    uint32_t framesCount;
    wTexture** frames;
} TSpriteInMem;

// ���� �������, ����������� �� �����
typedef struct
{
    TSpriteInMem* index;    // ������ ������� (������� � ������� sprites)
    bool    destroyable;    // ����������?
    float   health;         // ������� �������� ������
    int     curFrame;       // ������� ����
    float   animSpeed;      // �������� ��������
    float   _animSpeed;     // �������� �������� �������� �� ����� �����
    wNode*  solidBox;       // �������������� �������, ���� ������ �������
    wSelector* physBody;    // ���������� ���� �������������� �������
    wNode*  billboard;      // ���� ���������
} TSpriteNode;


//extern TSpriteInMem sprites[SPRITES_COUNT];
extern TSpriteInMem* sprites;
extern SList* spriteNodes;

void InitSprites();
bool LoadSpritePack();

// �������� ����
TSpriteNode* SpriteNodeCreate(TSpriteInMem* index, wNode* solidBox, wNode* billboard);
// �������� �������� ���� �������
void SpriteNodeUpdate(TSpriteNode* node);
void SpriteNodesUpdate();
// ���������� ����
void SpriteNodeDestroy(TSpriteNode** node);

#endif // __SPRITES_H
