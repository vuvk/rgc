g_PlayerPos.x = player_get_position_x();
g_PlayerPos.y = player_get_position_y();
g_PlayerPos.z = player_get_position_z();
camera_set_position(g_PlayerPos.x, g_PlayerPos.y + g_PlayerHeight, g_PlayerPos.z);

var moveSpeed = g_PlayerSpeed * deltatime();
var rotSpeed  = 0.5/* * deltatime()*/;
var pitch = camera_get_pitch();
var yaw   = camera_get_yaw();

var dX, dY, dZ;


if (Mouse.isEventAvailable()) {
	var halfWidth  = window_get_width()  / 2;
	var halfHeight = window_get_height() / 2;
	
	dX = (Mouse.getX() - halfWidth ) * rotSpeed;
	dY = (Mouse.getY() - halfHeight) * rotSpeed;	
	
	if (dX >  10.0) dX =  10.0;
	if (dX < -10.0) dX = -10.0;
	if (dY >  10.0) dY =  10.0;
	if (dY < -10.0) dY = -10.0;	
	
	yaw   += dX;
	pitch -= dY;
	if ((pitch > 45.0 ) && (pitch < 180.0)) pitch = 45.0;
	if ((pitch < 315.0) && (pitch > 180.0)) pitch = 315.0;
	
	Mouse.setPosition(halfWidth, halfHeight);
}


if (Keyboard.isEventAvailable()) {
	if (Keyboard.isKeyHit(VK_RETURN)) print("HELLO!");
	
	if (Keyboard.isKeyPressed(VK_KEY_W)) { // forward
		var rad = deg_to_rad(yaw);
		dX = Math.sin(rad) * moveSpeed;
		dZ = Math.cos(rad) * moveSpeed;

		g_PlayerPos.x -= dX;
		g_PlayerPos.z -= dZ;
	}
	
	if (Keyboard.isKeyPressed(VK_KEY_S)) { // backward
		var rad = deg_to_rad(yaw);
		dX = Math.sin(rad) * moveSpeed;
		dZ = Math.cos(rad) * moveSpeed;
			
		g_PlayerPos.x += dX;
		g_PlayerPos.z += dZ;
	}
	
	if (Keyboard.isKeyPressed(VK_KEY_D)) { //right
		var rad = deg_to_rad(yaw - 90.0);
		dX = Math.sin(rad) * moveSpeed;
		dZ = Math.cos(rad) * moveSpeed;
			
		g_PlayerPos.x += dX;
		g_PlayerPos.z += dZ;
	}
	
	if (Keyboard.isKeyPressed(VK_KEY_A)) { //left
		var rad = deg_to_rad(yaw + 90.0);
		dX = Math.sin(rad) * moveSpeed;
		dZ = Math.cos(rad) * moveSpeed;
			
		g_PlayerPos.x += dX;
		g_PlayerPos.z += dZ;
	}
}

player_set_position(g_PlayerPos.x, g_PlayerPos.y, g_PlayerPos.z);
camera_set_pitch(pitch);
camera_set_yaw  (yaw);

delete moveSpeed, rotSpeed;
delete dX, dY, dZ;
delete yaw, pitch;
