
/* key codes */

const VK_UNKNOWN          = 0x0;
const VK_LBUTTON          = 0x01;  // Left mouse button
const VK_RBUTTON          = 0x02;  // Right mouse button
const VK_CANCEL           = 0x03;  // Control-break processing
const VK_MBUTTON          = 0x04;  // Middle mouse button (three-button mouse)
const VK_XBUTTON1         = 0x05;  // Windows 2000/XP: X1 mouse button
const VK_XBUTTON2         = 0x06;  // Windows 2000/XP: X2 mouse button
const VK_BACK             = 0x08;  // BACKSPACE key
const VK_TAB              = 0x09;  // TAB key
const VK_CLEAR            = 0x0C;  // CLEAR key
const VK_RETURN           = 0x0D;  // ENTER key
const VK_SHIFT            = 0x10;  // SHIFT key
const VK_CONTROL          = 0x11;  // CTRL key
const VK_MENU             = 0x12;  // ALT key
const VK_PAUSE            = 0x13;  // PAUSE key
const VK_CAPITAL          = 0x14;  // CAPS LOCK key
const VK_KANA             = 0x15;  // IME Kana mode
const VK_HANGUEL          = 0x15;  // IME Hanguel mode (maintained for compatibility use KEY_HANGUL)
const VK_HANGUL           = 0x15;  // IME Hangul mode
const VK_JUNJA            = 0x17;  // IME Junja mode
const VK_FINAL            = 0x18;  // IME final mode
const VK_HANJA            = 0x19;  // IME Hanja mode
const VK_KANJI            = 0x19;  // IME Kanji mode
const VK_ESCAPE           = 0x1B;  // ESC key
const VK_CONVERT          = 0x1C;  // IME convert
const VK_NONCONVERT       = 0x1D;  // IME nonconvert
const VK_ACCEPT           = 0x1E;  // IME accept
const VK_MODECHANGE       = 0x1F;  // IME mode change request
const VK_SPACE            = 0x20;  // SPACEBAR
const VK_PRIOR            = 0x21;  // PAGE UP key
const VK_NEXT             = 0x22;  // PAGE DOWN key
const VK_END              = 0x23;  // END key
const VK_HOME             = 0x24;  // HOME key
const VK_LEFT             = 0x25;  // LEFT ARROW key
const VK_UP               = 0x26;  // UP ARROW key
const VK_RIGHT            = 0x27;  // RIGHT ARROW key
const VK_DOWN             = 0x28;  // DOWN ARROW key
const VK_SELECT           = 0x29;  // SELECT key
const VK_PRINT            = 0x2A;  // PRINT key
const VK_EXECUT           = 0x2B;  // EXECUTE key
const VK_SNAPSHOT         = 0x2C;  // PRINT SCREEN key
const VK_INSERT           = 0x2D;  // INS key
const VK_DELETE           = 0x2E;  // DEL key
const VK_HELP             = 0x2F;  // HELP key
const VK_KEY_0            = 0x30;  // 0 key
const VK_KEY_1            = 0x31;  // 1 key
const VK_KEY_2            = 0x32;  // 2 key
const VK_KEY_3            = 0x33;  // 3 key
const VK_KEY_4            = 0x34;  // 4 key
const VK_KEY_5            = 0x35;  // 5 key
const VK_KEY_6            = 0x36;  // 6 key
const VK_KEY_7            = 0x37;  // 7 key
const VK_KEY_8            = 0x38;  // 8 key
const VK_KEY_9            = 0x39;  // 9 key
const VK_KEY_A            = 0x41;  // A key
const VK_KEY_B            = 0x42;  // B key
const VK_KEY_C            = 0x43;  // C key
const VK_KEY_D            = 0x44;  // D key
const VK_KEY_E            = 0x45;  // E key
const VK_KEY_F            = 0x46;  // F key
const VK_KEY_G            = 0x47;  // G key
const VK_KEY_H            = 0x48;  // H key
const VK_KEY_I            = 0x49;  // I key
const VK_KEY_J            = 0x4A;  // J key
const VK_KEY_K            = 0x4B;  // K key
const VK_KEY_L            = 0x4C;  // L key
const VK_KEY_M            = 0x4D;  // M key
const VK_KEY_N            = 0x4E;  // N key
const VK_KEY_O            = 0x4F;  // O key
const VK_KEY_P            = 0x50;  // P key
const VK_KEY_Q            = 0x51;  // Q key
const VK_KEY_R            = 0x52;  // R key
const VK_KEY_S            = 0x53;  // S key
const VK_KEY_T            = 0x54;  // T key
const VK_KEY_U            = 0x55;  // U key
const VK_KEY_V            = 0x56;  // V key
const VK_KEY_W            = 0x57;  // W key
const VK_KEY_X            = 0x58;  // X key
const VK_KEY_Y            = 0x59;  // Y key
const VK_KEY_Z            = 0x5A;  // Z key
const VK_LWIN             = 0x5B;  // Left Windows key (Microsoft Natural keyboard)
const VK_RWIN             = 0x5C;  // Right Windows key (Natural keyboard)
const VK_APPS             = 0x5D;  // Applications key (Natural keyboard)
const VK_SLEEP            = 0x5F;  // Computer Sleep key
const VK_NUMPAD0          = 0x60;  // Numeric keypad 0 key
const VK_NUMPAD1          = 0x61;  // Numeric keypad 1 key
const VK_NUMPAD2          = 0x62;  // Numeric keypad 2 key
const VK_NUMPAD3          = 0x63;  // Numeric keypad 3 key
const VK_NUMPAD4          = 0x64;  // Numeric keypad 4 key
const VK_NUMPAD5          = 0x65;  // Numeric keypad 5 key
const VK_NUMPAD6          = 0x66;  // Numeric keypad 6 key
const VK_NUMPAD7          = 0x67;  // Numeric keypad 7 key
const VK_NUMPAD8          = 0x68;  // Numeric keypad 8 key
const VK_NUMPAD9          = 0x69;  // Numeric keypad 9 key
const VK_MULTIPLY         = 0x6A;  // Multiply key
const VK_ADD              = 0x6B;  // Add key
const VK_SEPARATOR        = 0x6C;  // Separator key
const VK_SUBTRACT         = 0x6D;  // Subtract key
const VK_DECIMAL          = 0x6E;  // Decimal key
const VK_DIVIDE           = 0x6F;  // Divide key
const VK_F1               = 0x70;  // F1 key
const VK_F2               = 0x71;  // F2 key
const VK_F3               = 0x72;  // F3 key
const VK_F4               = 0x73;  // F4 key
const VK_F5               = 0x74;  // F5 key
const VK_F6               = 0x75;  // F6 key
const VK_F7               = 0x76;  // F7 key
const VK_F8               = 0x77;  // F8 key
const VK_F9               = 0x78;  // F9 key
const VK_F10              = 0x79;  // F10 key
const VK_F11              = 0x7A;  // F11 key
const VK_F12              = 0x7B;  // F12 key
const VK_F13              = 0x7C;  // F13 key
const VK_F14              = 0x7D;  // F14 key
const VK_F15              = 0x7E;  // F15 key
const VK_F16              = 0x7F;  // F16 key
const VK_F17              = 0x80;  // F17 key
const VK_F18              = 0x81;  // F18 key
const VK_F19              = 0x82;  // F19 key
const VK_F20              = 0x83;  // F20 key
const VK_F21              = 0x84;  // F21 key
const VK_F22              = 0x85;  // F22 key
const VK_F23              = 0x86;  // F23 key
const VK_F24              = 0x87;  // F24 key
const VK_NUMLOCK          = 0x90;  // NUM LOCK key
const VK_SCROLL           = 0x91;  // SCROLL LOCK key
const VK_LSHIFT           = 0xA0;  // Left SHIFT key
const VK_RSHIFT           = 0xA1;  // Right SHIFT key
const VK_LCONTROL         = 0xA2;  // Left CONTROL key
const VK_RCONTROL         = 0xA3;  // Right CONTROL key
const VK_LMENU            = 0xA4;  // Left MENU key
const VK_RMENU            = 0xA5;  // Right MENU key
const VK_BROWSER_BACK     = 0xA6;  // Browser Back key
const VK_BROWSER_FORWARD  = 0xA7;  // Browser Forward key
const VK_BROWSER_REFRESH  = 0xA8;  // Browser Refresh key
const VK_BROWSER_STOP     = 0xA9;  // Browser Stop key
const VK_BROWSER_SEARCH   = 0xAA;  // Browser Search key
const VK_BROWSER_FAVORITES =0xAB;  // Browser Favorites key
const VK_BROWSER_HOME     = 0xAC;  // Browser Start and Home key
const VK_VOLUME_MUTE      = 0xAD;  // Volume Mute key
const VK_VOLUME_DOWN      = 0xAE;  // Volume Down key
const VK_VOLUME_UP        = 0xAF;  // Volume Up key
const VK_MEDIA_NEXT_TRACK = 0xB0;  // Next Track key
const VK_MEDIA_PREV_TRACK = 0xB1;  // Previous Track key
const VK_MEDIA_STOP       = 0xB2;  // Stop Media key
const VK_MEDIA_PLAY_PAUSE = 0xB3;  // Play/Pause Media key
const VK_OEM_1            = 0xBA;  // for US    ";:"
const VK_PLUS             = 0xBB;  // Plus Key   "+"
const VK_COMMA            = 0xBC;  // Comma Key  ","
const VK_MINUS            = 0xBD;  // Minus Key  "-"
const VK_PERIOD           = 0xBE;  // Period Key "."
const VK_OEM_2            = 0xBF;  // for US    "/?"
const VK_OEM_3            = 0xC0;  // for US    "`~"
const VK_OEM_4            = 0xDB;  // for US    "[{"
const VK_OEM_5            = 0xDC;  // for US    "\|"
const VK_OEM_6            = 0xDD;  // for US    "]}"
const VK_OEM_7            = 0xDE;  // for US    "'""
const VK_OEM_8            = 0xDF;  // None
const VK_OEM_AX           = 0xE1;  // for Japan "AX"
const VK_OEM_102          = 0xE2;  // "<>" or "\|"
const VK_ATTN             = 0xF6;  // Attn key
const VK_CRSEL            = 0xF7;  // CrSel key
const VK_EXSEL            = 0xF8;  // ExSel key
const VK_EREOF            = 0xF9;  // Erase EOF key
const VK_PLAY             = 0xFA;  // Play key
const VK_ZOOM             = 0xFB;  // Zoom key
const VK_PA1              = 0xFD;  // PA1 key
const VK_OEM_CLEAR        = 0xFE;  // Clear key
const VK_NONE             = 0xFF;  // usually no key mapping; but some laptops use it for fn key
const VK_KEY_CODES_COUNT  = 0x100; // this is not a key; but the amount of keycodes there are.
