module textures;

import std.stdio;
import std.conv;

import list;
import constants;
import global;
import res_manager;
import util;

import WorldSim3D;
import SampleFunctions;

extern (C):

/** текстура-эталон, загруженная из пака */
struct TTextureInMem
{
    ubyte  animSpeed;      // кол-во кадров в секунду
    wTexture*[] frames;
}

/** ноды по именам текстур, создаваемые на карте со ссылкой на меши, у которых будет меняться кадр */
struct TTextureNode
{
    //private
    public
    {
        TTextureInMem* index;   // индекс текстуры (позиция в массиве textures или texturesShadowed)
        int     curFrame;       // текущий кадр
        float   animSpeed;      // скорость анимации
        float   _animSpeed;     // значение прироста анимации до смены кадра
        wNode*  node;
    }

    this(TTextureInMem* index, wNode* object)
    {
        float animSpeed  = 0.0f;
        float _animSpeed = 0.0f;

        this.index     = index;
        this.animSpeed = index.animSpeed;
        this.node      = object;
    }

    ~this()
    {
        if (this.node != null)
        {
            wMesh* mesh = wNodeGetMesh(this.node);
            if (mesh != null)
                wMeshDestroy(mesh);
            wNodeDestroy(this.node);
        }
    }

    void Update()
    {
        if (this.animSpeed == 0.0f)
            return;

        if (this.node == null)
            return;

        if (this._animSpeed < 1.0f)
        {
            this._animSpeed += g_DeltaTime * this.animSpeed;
        }
        else
        {
            this._animSpeed = 0.0f;

            // переходим на следующий кадр
            ++(this.curFrame);
            if (this.curFrame >= this.index.frames.length)
                this.curFrame = 0;

            // если там пустая текстура, то ищем на следующем кадре
            wTexture* txr = this.index.frames[this.curFrame];
            while (txr == null)
            {
                ++(this.curFrame);
                if (this.curFrame >= this.index.frames.length)
                    this.curFrame = 0;

                txr = this.index.frames[this.curFrame];
            }

            wMaterial* mat = wNodeGetMaterial(this.node, 0);
            wMaterialSetTexture(mat, 0, txr);
        }
    }
}


__gshared TTextureInMem[] g_Textures;
__gshared TTextureInMem[] g_TexturesShadowed;
__gshared SList* g_TextureNodes;


void InitTextures();
bool LoadTexturePack();
void TextureNodesUpdate();


// IMPLEMENTATION


void InitTextures()
{
    int i, j;
    wTexture*      wTxr;
    TTextureInMem* txr;

    // если текстурные хранилища не были созданы, то просто их создать
    if ((g_Textures == null && g_TexturesShadowed == null) ||
        (g_Textures.length == 0 && g_TexturesShadowed.length == 0))
    {
        g_Textures = new TTextureInMem[TEXTURES_MAX_COUNT];
        g_TexturesShadowed = new TTextureInMem[TEXTURES_MAX_COUNT];
        return;
    }

    // удаляем все текстуры, которые хранятся в хранилищах
    for (i = 0; i < TEXTURES_MAX_COUNT; ++i)
    {
        txr = &(g_Textures[i]);
        for (j = 0; j < txr.frames.length; ++j)
        {
            wTxr = txr.frames[j];
            if (wTxr != null)
            {
                wTextureDestroy(wTxr);
                txr.frames[j] = null;
            }
        }
        txr.animSpeed = 0;
        txr.frames.length = 0;

        txr = &(g_TexturesShadowed[i]);
        for (j = 0; j < txr.frames.length; ++j)
        {
            wTxr = txr.frames[j];
            if (wTxr != null)
            {
                wTextureDestroy(wTxr);
                txr.frames[j] = null;
            }
        }
        txr.animSpeed = 0;
        txr.frames.length = 0;
    }

    //wSceneDestroyAllTextures();
}

bool LoadTexturePack()
{
    bool isLoaded;

    wTexture* loadTexture;
    wTexture* loadTextureShadowed;
    void*     loadTextureBits;

    TTextureInMem* texture;
    TTextureInMem* textureShadowed;

    ushort w, h;
    byte[] buffer = new byte[0];
    int    bufferSize;
    string fileName;

    int    color;
    int    i, j;
    ubyte  animSpeed;
    uint   framesCount;
    int    textureSize;
    string textureName;

    // читаем количество текстур
    bufferSize = Res_GetUncompressedFileSize("textures.cfg\0");
    if (bufferSize <= 0)
    {
        PrintWithColor("can not get size of textures.cfg\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    buffer.length = bufferSize;
    if (!Res_ReadFileFromZipToBuffer("textures.cfg", &(buffer[0]), bufferSize))
    {
        PrintWithColor("can not read textures.cfg\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    if (!Res_OpenIniFromBuffer(&(buffer[0]), bufferSize))
    {
        PrintWithColor("can not open textures.cfg!!!\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    g_TexturesCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_TexturesCount == 0)
        goto end;

    InitTextures();

    for (i = 0; i < g_TexturesCount; ++i)
    {
        // если не нужна для загрузки
        if (g_TexturesNeedForLoad[i] == false)
            continue;

        // читаем инфу по текстуре
        fileName = "texture_" ~ to!string(i) ~ ".cfg\0";
        bufferSize = Res_GetUncompressedFileSize(fileName.ptr);
        if (bufferSize <= 0)
        {
            PrintWithColor("can not get size of texture_" ~ to!string(i) ~ ".cfg", wConsoleFontColor.wCFC_RED, false);
            continue;
        }

        texture = &(g_Textures[i]);
        textureShadowed = &(g_TexturesShadowed[i]);

        buffer.length = bufferSize;
        Res_ReadFileFromZipToBuffer(fileName.ptr, &(buffer[0]), bufferSize);
        Res_OpenIniFromBuffer(&(buffer[0]), bufferSize);
        animSpeed   = cast(ubyte)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = Res_IniReadInteger("Options", "frames_count",    0);
        Res_CloseIni();

        texture.animSpeed = animSpeed;
        textureShadowed.animSpeed = animSpeed;
        if (framesCount == 0)
            continue;

        texture.frames         = new wTexture*[framesCount];
        textureShadowed.frames = new wTexture*[framesCount];

        // читаем кадры, если есть
        for (j = 0; j < framesCount; ++j)
        {
            fileName = "texture_" ~ to!string(i) ~ "_frame_" ~ to!string(j) ~ ".dat\0";
            textureName = "Texture_" ~ to!string(i) ~ "_Frame_" ~ to!string(j);
            texture.frames[j] = LoadTexture(fileName, textureName);

            // затеняем теневую версию
            wVector2u size;
            uint pitch;
            wColorFormat colorFormat;

            wTextureGetInformation(texture.frames[j], &size, &pitch, &colorFormat);
            textureSize = size.x * size.y * int.sizeof;

            textureName ~= " shadowed";
            loadTextureShadowed = wTextureCopy(texture.frames[j], cast(char*)textureName.ptr);
            loadTextureBits = wTextureLock(loadTextureShadowed);
            for (int b = 0; b < textureSize / int.sizeof; ++b)
            {
                color = (cast(int*)loadTextureBits)[b];
                color = (color >> 1) & 8355711;
                (cast(int*)loadTextureBits)[b] = color;
            }
            wTextureUnlock(loadTextureShadowed);
            textureShadowed.frames[j] = loadTextureShadowed;
        }
    }

    isLoaded = true;

end:
    buffer.length = 0;

    return isLoaded;
}


void TextureNodesUpdate()
{
    if (g_TextureNodes == null || g_TextureNodes.size == 0)
        return;

    for (SListElement* element = g_TextureNodes.first; (element != null); element = element.next)
    {
        if (element.value != null)
        {
            (cast(TTextureNode*)element.value).Update();
        }
    }
}
