module sprites;

import std.stdio;
import std.conv;

import list;
import constants;
import global;
import res_manager;
import util;
import base_object;

import WorldSim3D;
import SampleFunctions;


extern (C):

/** спрайт-эталон, загруженный из пака */
struct TSpriteInMem
{
    string name;
    ubyte  axisParams;     // поворот по осям: 0 = смотрит на игрока, 1 = фиксация по горизонтали, 2 = фиксация по вертикали
    bool   collision;      // можно столкнуться
    bool   destroyable;    // разрушаемый?
    ubyte  endurance;      // живучесть
    ubyte  animSpeed;      // кол-во кадров в секунду
    float  scaleX, scaleY; // масштабирование
    float  solidX, solidY; // размеры ограничивающей коробки
    wTexture*[] frames;
}

/** нода спрайта, создаваемая на карте */
class TSpriteNode : TObject
{
    TSpriteInMem* index;    // индекс спрайта (позиция в массиве sprites)
    int     curFrame;       // текущий кадр
    float   animSpeed;      // скорость анимации
    float   _animSpeed;     // значение прироста анимации до смены кадра
    wNode*  solidBox;       // ограничивающая коробка, если спрайт твердый
    wSelector* physBody;    // физическое тело ограничивающей коробки
    wNode*  billboard;      // нода биллборда

    // создание ноды
    this(TSpriteInMem* index, wNode* solidBox, wNode* billboard)
    {
        wBillboardAxisParam axisParams;
        wMaterial* material;

        addVar("name",        index.name);          // имя ключа
        addVar("destroyable", index.destroyable);   // можно ли разрушить
        addVar("health",      index.endurance);     // текущее значение жизней
        addVar("isDestroyed", false);               // разрушено?

        this.index       = index;
        this.animSpeed   = index.animSpeed;
        this.solidBox    = solidBox;
        this.billboard   = billboard;

        double scaleX = index.scaleX / 100.0;
        double scaleY = index.scaleY / 100.0;

        wBillBoardSetSize(this.billboard, wVector2f(scaleX, scaleY));
        double dX;
        double dY = (1.0 - scaleY) / 2.0;
        //double dY = (index.scaleY / 100.0) / 2.0;
        wNodeMove(this.billboard, wVector3f(0.0, -dY, 0.0));

        if (this.solidBox)
        {
            // изменяем размеры коробки
            double solidX = index.solidX / 100.0;
            double solidY = index.solidY / 100.0;

            // смешаем коробку в зависимости от размера коробки с учетом масштаба
            dX = (1.0 - scaleX * solidX) / 2;
            wNodeMove(this.solidBox, wVector3f(dX, 0.0, -dX));

            // меняем размер коробки с учетом масштаба
            dX = scaleX * solidX;
            dY = scaleY * solidY;
            wNodeSetScale(this.solidBox, wVector3f(dX, dY, dX));

            wMesh* mesh = wNodeGetMesh(this.solidBox);
            this.physBody = wCollisionCreateFromMesh(mesh, this.solidBox, 0);
        }

        // устанавливаем параметры поворота спрайта
        switch (index.axisParams)
        {
            // смотреть на игрока
            default:
            case 0 :
                axisParams.isEnablePitch = false;
                axisParams.isEnableYaw   = true;
                axisParams.isEnableRoll  = false;
                break;

            // фиксация по горизонтали (XoY)
            case 1 :
                axisParams.isEnablePitch = true;
                axisParams.isEnableYaw   = true;
                axisParams.isEnableRoll  = false;
                material = wNodeGetMaterial(billboard, 0);
                wMaterialSetFlag(material, wMaterialFlags.wMF_BACK_FACE_CULLING, false);
                break;

            // фиксация по вертикали (ZoY)
            case 2 :
                axisParams.isEnablePitch = false;
                axisParams.isEnableYaw   = true;
                axisParams.isEnableRoll  = true;
                material = wNodeGetMaterial(billboard, 0);
                wMaterialSetFlag(material, wMaterialFlags.wMF_BACK_FACE_CULLING, false);
                break;
        }
        wBillboardSetEnabledAxis(billboard, axisParams);
    }

    // уничтожить ноду
    ~this()
    {
        if (this.solidBox != null)
        {
            wMesh* mesh = wNodeGetMesh(this.solidBox);
            if (mesh != null)
                wMeshDestroy(mesh);
            wNodeDestroy(this.solidBox);
        }

        if (this.billboard != null)
        {
            wNodeDestroy(this.billboard);
        }
    }

    // обновить анимацию ноды спрайта
    void Update()
    {
        if (!this.isVisible)
            return;

        if (this.animSpeed == 0.0f)
            return;

        if (this.billboard == null)
            return;

        if (this._animSpeed < 1.0f)
        {
            this._animSpeed += g_DeltaTime * this.animSpeed;
        }
        else
        {
            this._animSpeed = 0.0f;

            // переходим на следующий кадр
            ++(this.curFrame);
            if (this.curFrame >= this.index.frames.length)
                this.curFrame = 0;

            // если там пустая текстура, то ищем на следующем кадре
            wTexture* txr = this.index.frames[this.curFrame];
            while (txr == null)
            {
                ++(this.curFrame);
                if (this.curFrame >= this.index.frames.length)
                    this.curFrame = 0;

                txr = this.index.frames[this.curFrame];
            }

            wMaterial* mat = wNodeGetMaterial(this.billboard, 0);
            wMaterialSetTexture(mat, 0, txr);
        }
    }
}

__gshared TSpriteInMem[] g_Sprites;
__gshared TSpriteNode[] g_SpriteNodes;

void InitSprites();
bool LoadSpritePack();
void SpriteNodesUpdate();


// IMPLEMENTATION


void InitSprites()
{
    TSpriteInMem* spr;
    wTexture*     txr;
    int i, j;

    // если текстурные хранилища не были созданы, то просто их создать
    if (g_Sprites == null || g_Sprites.length == 0)
    {
        g_Sprites = new TSpriteInMem[SPRITES_MAX_COUNT];
        return;
    }

    // удаляем все текстуры, которые хранятся в хранилищах
    for (i = 0; i < SPRITES_MAX_COUNT; ++i)
    {
        spr = &(g_Sprites[i]);
        for (j = 0; j < spr.frames.length; ++j)
        {
            txr = spr.frames[j];
            if (txr != null)
            {
                wTextureDestroy(txr);
                spr.frames[j] = null;
            }
        }
        spr.axisParams  = 0;
        spr.collision   = true;
        spr.destroyable = false;
        spr.endurance   = 0;
        spr.animSpeed   = 0;
        spr.frames.length = 0;
        spr.scaleX = spr.scaleY = 100.0;
    }
}

bool LoadSpritePack()
{
    bool    isLoaded;
    TSpriteInMem* sprite;

    ushort  w, h;
    byte[]  buffer = new byte[0];
    int     bufferSize;
    string  fileName;

    string  name;
    string  spriteName;
    int     i, j;
    ubyte   animSpeed;
    uint    framesCount;
    ubyte   axisParams;
    bool    collision;
    bool    destroyable;
    ubyte   endurance;
    float   scaleX, scaleY;
    float   solidX, solidY;
    int     textureSize;

    // читаем количество текстур
    bufferSize = Res_GetUncompressedFileSize("sprites.cfg\0");
    if (bufferSize <= 0)
    {
        PrintWithColor("can not get size of sprites.cfg\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    buffer.length = bufferSize;
    if (!Res_ReadFileFromZipToBuffer("sprites.cfg", &(buffer[0]), bufferSize))
    {
        PrintWithColor("can not read sprites.cfg\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    if (!Res_OpenIniFromBuffer(&(buffer[0]), bufferSize))
    {
        PrintWithColor("can not open sprites.cfg!!!\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    g_SpritesCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_SpritesCount == 0)
        goto end;

    InitSprites();

    for (i = 0; i < g_SpritesCount; ++i)
    {
        // если не нужна для загрузки
        if (!g_SpritesNeedForLoad[i])
            continue;

        // читаем инфу по текстуре
        fileName = "sprite_" ~ to!string(i) ~ ".cfg\0";
        bufferSize = Res_GetUncompressedFileSize(fileName.ptr);
        if (bufferSize <= 0)
        {
            PrintWithColor("can not get size of sprite_" ~ to!string(i) ~ ".cfg", wConsoleFontColor.wCFC_RED, false);
            continue;
        }

        sprite = &(g_Sprites[i]);

        buffer.length = bufferSize;
        Res_ReadFileFromZipToBuffer(fileName.ptr, &(buffer[0]), bufferSize);
        Res_OpenIniFromBuffer(&(buffer[0]), bufferSize);
        name        = to!string(Res_IniReadString("Options", "name", ("Sprite #" ~ to!string(i) ~ "\0").ptr));
        animSpeed   = cast(ubyte)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = cast(ubyte)Res_IniReadInteger("Options", "frames_count",    0);
        axisParams  = cast(ubyte)Res_IniReadInteger("Options", "axis",            0);
        collision   = Res_IniReadBool   ("Options", "collision",       false);
        destroyable = Res_IniReadBool   ("Options", "destroyable",     false);
        endurance   = cast(ubyte)Res_IniReadInteger("Options", "endurance",       0);
        scaleX      = Res_IniReadDouble("Options", "scale_x", 100.0);
        scaleY      = Res_IniReadDouble("Options", "scale_y", 100.0);
        solidX      = Res_IniReadDouble("Options", "solid_x", 100.0);
        solidY      = Res_IniReadDouble("Options", "solid_y", 100.0);
        Res_CloseIni();

        sprite.name        = name;
        sprite.animSpeed   = animSpeed;
        sprite.axisParams  = axisParams;
        sprite.collision   = collision;
        sprite.destroyable = destroyable;
        sprite.endurance   = endurance;
        sprite.scaleX      = scaleX;
        sprite.scaleY      = scaleY;
        sprite.solidX      = solidX;
        sprite.solidY      = solidY;
        if (framesCount == 0)
            continue;

        sprite.frames = new wTexture*[framesCount];

        // читаем кадры, если есть
        for (j = 0; j < framesCount; ++j)
        {
            fileName   = "sprite_" ~ to!string(i) ~ "_frame_" ~ to!string(j) ~ ".dat\0";
            spriteName = "Sprite_" ~ to!string(i) ~ "_Frame_" ~ to!string(j);
            sprite.frames[j] = LoadTexture(fileName, spriteName);
        }
    }
    isLoaded = true;

end:
    buffer.length = 0;

    return isLoaded;
}

void SpriteNodesUpdate()
{
    if (g_SpriteNodes.length == 0)
        return;

    foreach (sprite; g_SpriteNodes)
    {
        if (sprite !is null)
        {
            //g_MutexNodes.lock_nothrow();
            //g_ObjectId = sprite.id;
            sprite.Update();
            //g_MutexNodes.unlock_nothrow();
        }
    }
}
