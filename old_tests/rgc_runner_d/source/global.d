module global;

import std.stdio;
import std.algorithm;
import core.sync.mutex;

import WorldSim3D;

import constants;
import duk_util;
import map;

__gshared int g_TexturesCount = 0;
__gshared int g_SpritesCount  = 0;
__gshared int g_DoorsCount    = 0;
__gshared int g_KeysCount     = 0;
__gshared int g_MapsCount     = 0;

bool g_PreviewMode = false;

__gshared wDriverTypes g_Renderer = wDriverTypes.wDRT_OPENGL;   /** рендерер, с которым будет запущен раннер */
__gshared bool  g_LimitFps = true;    /** установить лимит кадров в MAX_FPS */
__gshared bool  g_VSync    = false;   /** вертикальная синхронизация */
__gshared bool  g_ShowFog  = false;   /** отображать ли туман */
__gshared float g_DeltaTime;          /** время между кадрами */

__gshared wTexture* g_InvisibleTexture;
__gshared wMesh*    g_InvisibleMesh;
//extern wNode*    invisibleNode;

__gshared wSelector* g_WorldCollider;  /** коллайдер статичного мира */

/* флаги над теми номерами текстур, которые нужно грузить */
__gshared bool[TEXTURES_MAX_COUNT] g_TexturesNeedForLoad;
__gshared bool[SPRITES_MAX_COUNT ] g_SpritesNeedForLoad;
__gshared bool[DOORS_MAX_COUNT   ] g_DoorsNeedForLoad;
__gshared bool[KEYS_MAX_COUNT    ] g_KeysNeedForLoad;

/* мьютекс для обновления идентификатора объекта, обрабатываемого в данный момент */
__gshared Mutex g_MutexNodes;

// инициализация запускатора - выставление глобальных настроек
void InitRunner();
// настройки материала для олдскульности
void MaterialSetOldSchool(wMaterial* material);

// пробегается по карте и заполняет g_TexturesNeedForLoad
void PrepareTexturesNeedForLoad(int numOfMap);
void PrepareSpritesNeedForLoad(int numOfMap);
void PrepareDoorsNeedForLoad(int numOfMap);
void PrepareKeysNeedForLoad(int numOfMap);


// IMPLEMENTATION


void InitRunner()
{
    if (g_LimitFps)
        wEngineSetFPS(MAX_FPS);

    //wLogSetLevel(wLL_DEBUG);
    ///Show logo WS3D
    //wEngineShowLogo(true);

    wSystemSetTextureCreationFlag(wTextureCreationFlag.wTCF_CREATE_MIP_MAPS, false);
    //wSystemSetTextureCreationFlag(wTCF_OPTIMIZED_FOR_QUALITY, true);
    wSystemSetTextureCreationFlag(wTextureCreationFlag.wTCF_ALLOW_NON_POWER_2, false);

    if (g_Renderer == wDriverTypes.wDRT_SOFTWARE)
    {
        wSystemSetTextureCreationFlag(wTextureCreationFlag.wTCF_ALWAYS_16_BIT, true);
        wSystemSetTextureCreationFlag(wTextureCreationFlag.wTCF_NO_ALPHA_CHANNEL, true);
    }

    // запускаем интерпретатор
    //Duk_InitVM();

    // создаем мьютекс
    g_MutexNodes = new Mutex();
}

void MaterialSetOldSchool(wMaterial* material)
{
    wMaterialSetFlag(material, wMaterialFlags.wMF_LIGHTING,           false);
    wMaterialSetFlag(material, wMaterialFlags.wMF_ANISOTROPIC_FILTER, false);
    wMaterialSetFlag(material, wMaterialFlags.wMF_ANTI_ALIASING,      false);
    wMaterialSetFlag(material, wMaterialFlags.wMF_BILINEAR_FILTER,    false);
    wMaterialSetFlag(material, wMaterialFlags.wMF_TRILINEAR_FILTER,   false);
    //wMaterialSetFlag(material, wMF_BACK_FACE_CULLING,                 false);
    wMaterialSetFlag(material, wMaterialFlags.wMF_FOG_ENABLE,         g_ShowFog);
}

void PrepareTexturesNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    g_TexturesNeedForLoad = new bool[TEXTURES_MAX_COUNT];

    g_ShowFog = map.showFog;

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map.ceil[i][j];
            if ((element >  0) &&
                (element < TEXTURES_MAX_COUNT))
                g_TexturesNeedForLoad[--element] = true;

            element = map.floor[i][j];
            if ((element >  0) &&
                (element < TEXTURES_MAX_COUNT))
                g_TexturesNeedForLoad[--element] = true;

            element = map.level[i][j];
            if ((element >  0) &&
                (element < TEXTURES_MAX_COUNT))
                g_TexturesNeedForLoad[--element] = true;
        }
    }
}

void PrepareSpritesNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    g_SpritesNeedForLoad = new bool[SPRITES_MAX_COUNT];

    g_ShowFog = map.showFog;

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map.level[i][j] - TEXTURES_MAX_COUNT;
            if ((element >  0) &&
                (element < SPRITES_MAX_COUNT))
                g_SpritesNeedForLoad[--element] = true;
        }
    }
}

void PrepareDoorsNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    g_DoorsNeedForLoad = new bool[DOORS_MAX_COUNT];

    g_ShowFog = map.showFog;

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map.level[i][j] - TEXTURES_MAX_COUNT - SPRITES_MAX_COUNT;
            if ((element >  0) &&
                (element < DOORS_MAX_COUNT))
                g_DoorsNeedForLoad[--element] = true;
        }
    }
}

void PrepareKeysNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    g_KeysNeedForLoad = new bool[KEYS_MAX_COUNT];

    g_ShowFog = map.showFog;

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map.level[i][j] - TEXTURES_MAX_COUNT - SPRITES_MAX_COUNT - DOORS_MAX_COUNT;
            if ((element > 0) &&
                (element < KEYS_MAX_COUNT))
            {
                g_KeysNeedForLoad[--element] = true;
                writeln("need key for load!");
            }
        }
    }
}
