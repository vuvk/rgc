module duk_util;

import std.stdio;
import std.conv;
import std.string;
import std.math;
import std.file;

public {
    import duk_config;
    import duktape;
}
import WorldSim3D;
import SampleFunctions;

import global;
import input;
import constants;

import util;

import map;
import player;
import base_object;

extern (C):

// DECLARATION //

__gshared duk_context* ctx = null;

struct TScript
{
    string fileName;
    string text;

    ~this()
    {
        this.fileName.length = 0;
        this.text.length = 0;
    }
}

void Duk_InitVM ();
void Duk_StopVM ();
void Duk_GC ();
void Duk_RegisterCFunction (duk_c_function native_function, duk_idx_t nargs, string new_name);
void Duk_LoadScriptFromFile (TScript* script, const string fileName);
void Duk_FreeScript (TScript* script);
void Duk_RunScript (TScript script, bool isGlobal = false);

// IMPLEMENTATION //

/* было когда-то макросами */
duk_ret_t DUK_CHECK_NARGS_EQUAL(duk_idx_t n)     { if (duk_get_top(ctx) == n) return DUK_RET_TYPE_ERROR; return DUK_ERR_NONE; }
duk_ret_t DUK_CHECK_NARGS_NOT_EQUAL(duk_idx_t n) { if (duk_get_top(ctx) != n) return DUK_RET_TYPE_ERROR; return DUK_ERR_NONE; }
duk_ret_t DUK_CHECK_NARGS_GREATER(duk_idx_t n)   { if (duk_get_top(ctx) >  n) return DUK_RET_TYPE_ERROR; return DUK_ERR_NONE; }
duk_ret_t DUK_CHECK_NARGS_SMALLER(duk_idx_t n)   { if (duk_get_top(ctx) <  n) return DUK_RET_TYPE_ERROR; return DUK_ERR_NONE; }

static duk_ret_t n_degToRad(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_NOT_EQUAL(1) != DUK_ERR_NONE)
    //    return 0;

    double deg = duk_to_number(ctx, 0);
    duk_push_number(ctx, deg*DEG_TO_RAD_COEFF);
    return 1;
}
static duk_ret_t n_radToDeg(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_NOT_EQUAL(1) != DUK_ERR_NONE)
    //    return 0;

    double rad = duk_to_number(ctx, 0);
    duk_push_number(ctx, rad*RAD_TO_DEG_COEFF);
    return 1;
}
static duk_ret_t n_distanceBetweenPoints(duk_context* ctx)
{
    int argc = duk_get_top(ctx);

    switch(argc)
    {
        case 4 :
        {
            double x0 = duk_to_number(ctx, 0);
            double y0 = duk_to_number(ctx, 1);
            double x1 = duk_to_number(ctx, 2);
            double y1 = duk_to_number(ctx, 3);
            duk_push_number(ctx, sqrt(sqr(x1 - x0) + sqr(y1 - y0)));
            break;
        }

        case 6 :
        {
            double x0 = duk_to_number(ctx, 0);
            double y0 = duk_to_number(ctx, 1);
            double z0 = duk_to_number(ctx, 2);
            double x1 = duk_to_number(ctx, 3);
            double y1 = duk_to_number(ctx, 4);
            double z1 = duk_to_number(ctx, 5);
            duk_push_number(ctx, sqrt(sqr(x1 - x0) + sqr(y1 - y0) + sqr(z1 - z0)));
            break;
        }

        default:
            duk_push_number(ctx, double.max);
    }

    return 1;
}



/** SYSTEM */
static duk_ret_t n_print(duk_context *ctx)
{
    duk_push_string(ctx, " ");
    duk_insert(ctx, 0);
    duk_join(ctx, duk_get_top(ctx) - 1);
    printf("%s\n", duk_safe_to_string(ctx, -1));
    //writef("%s\n", duk_safe_to_string(ctx, -1).to!string);
    return 0;
}
static duk_ret_t n_deltaTime(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    duk_push_number(ctx, g_DeltaTime/*wTimerGetDelta()*/);
    return 1;
}
static duk_ret_t n_windowGetWidth(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    wVector2u wndSize;
    wWindowGetSize(&wndSize);
    duk_push_int(ctx, wndSize.x);
    return 1;
}
static duk_ret_t n_windowGetHeight(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    wVector2u wndSize;
    wWindowGetSize(&wndSize);
    duk_push_int(ctx, wndSize.y);
    return 1;
}




/** PLAYER */

/** CAMERA */



void Duk_RegisterCFunction(duk_c_function native_function, duk_idx_t nargs, string new_name)
{
    duk_push_c_function(ctx, native_function, nargs);
    duk_put_global_string(ctx, new_name.ptr);
}

void Duk_InitVM()
{
    ctx = duk_create_heap_default();

    Duk_RegisterCFunction(&n_degToRad, 1, "degToRad");
    Duk_RegisterCFunction(&n_radToDeg, 1, "radToDeg");
    Duk_RegisterCFunction(&n_distanceBetweenPoints, DUK_VARARGS, "distanceBetweenPoints");


    /** system */
    Duk_RegisterCFunction(&n_print,           DUK_VARARGS, "print");
    Duk_RegisterCFunction(&n_deltaTime,       0,           "deltaTime");
    Duk_RegisterCFunction(&n_windowGetWidth,  0,           "windowGetWidth" );
    Duk_RegisterCFunction(&n_windowGetHeight, 0,           "windowGetHeight");

    /** map */
    RegisterMapCFunctions();

    /** base object */
    RegisterObjectCFunctions();

    /** keyboard */
    RegisterKeyboardCFunctions();

    /** mouse */
    RegisterMouseCFunctions();

    /** camera */
    RegisterCameraCFunctions();

    /** player */
    RegisterPlayerCFunctions();
}

void Duk_StopVM()
{
    duk_destroy_heap(ctx);
    ctx = null;
}

void Duk_GC()
{
    duk_gc(ctx, DUK_GC_COMPACT);
}

void Duk_LoadScriptFromFile(TScript* script, const string fileName)
{
    if (script == null)
        return;

    Duk_FreeScript(script);
    
    if (!std.file.exists(fileName))
    {
        PrintWithColor("File '" ~ fileName ~ "' is not exists!", wConsoleFontColor.wCFC_RED, false);
        return;    
    }
        
    File file = File(fileName, "r");
    string str;
    if (file.isOpen())
    {
        script.fileName = fileName;
        while (!file.eof())
        {
            str = file.readln();
            foreach(c; str)
            {
                // удаляем табы
                if (c != '\t') 
                    script.text ~= c;
            }
        }
        script.text ~= "\0";
        file.close();

        //writeln(script.text);
    }
}

void Duk_FreeScript(TScript* script)
{
    if (script)
    {
        script.fileName = script.fileName.init;
        script.text = script.text.init;
    }
}

void Duk_RunScript(TScript script, bool isGlobal = false)
{
    if (duk_peval_string(ctx, script.text.ptr) != 0)
        writefln("Error in script '%s': %s\n", script.fileName, to!string(duk_safe_to_string(ctx, -1)));

    //?
    if (!isGlobal)
        duk_pop(ctx);
}
