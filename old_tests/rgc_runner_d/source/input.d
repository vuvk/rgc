module input;

import duk_config;
import duk_util;
import duktape;
import WorldSim3D;

extern (C):

/** KEYBOARD */
void RegisterKeyboardCFunctions();

/** MOUSE */
void RegisterMouseCFunctions();

// IMPLEMENTATION

/** KEYBOARD */
duk_ret_t n_keyboard_is_event_available(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_NOT_EQUAL(0) != DUK_ERR_NONE)
    //    return 0;

    duk_push_boolean(ctx, wInputIsKeyEventAvailable());

    return 1;
}
duk_ret_t n_keyboard_is_key_hit(duk_context *ctx)
{
    //if (DUK_CHECK_NARGS_NOT_EQUAL(1) != DUK_ERR_NONE)
    //    return 0;

    int key = duk_to_int(ctx, 0);

    duk_push_boolean(ctx, wInputIsKeyHit(cast(wKeyCode)key));

    return 1;
}
duk_ret_t n_keyboard_is_key_pressed(duk_context *ctx)
{
    //if (DUK_CHECK_NARGS_NOT_EQUAL(1) != DUK_ERR_NONE)
    //    return 0;

    int key = duk_to_int(ctx, 0);

    duk_push_boolean(ctx, wInputIsKeyPressed(cast(wKeyCode)key));

    return 1;
}
duk_ret_t n_keyboard_is_key_released(duk_context *ctx)
{
    //if (DUK_CHECK_NARGS_NOT_EQUAL(1) != DUK_ERR_NONE)
    //    return 0;

    int key = duk_to_int(ctx, 0);

    duk_push_boolean(ctx, wInputIsKeyUp(cast(wKeyCode)key));

    return 1;
}

static duk_ret_t n_keyboard_load_object(duk_context *ctx)
{
    immutable duk_function_list_entry[] n_keyboard_funcs =
    [
        { key: "isEventAvailable", value: &n_keyboard_is_event_available, nargs: 0 },
        { key: "isKeyHit",         value: &n_keyboard_is_key_hit,      	  nargs: 1 },
        { key: "isKeyPressed",     value: &n_keyboard_is_key_pressed,     nargs: 1 },
        { key: "isKeyReleased",    value: &n_keyboard_is_key_released,    nargs: 1 },
        { key: null,               value: null,                           nargs: 0 }
    ];

    duk_push_object(ctx);
    duk_put_function_list(ctx, -1, &(n_keyboard_funcs[0]));
    return 1;
}

void RegisterKeyboardCFunctions()
{
    duk_push_c_function(ctx, &n_keyboard_load_object, 0);
    duk_call(ctx, 0);
    duk_put_global_string(ctx, "Keyboard");
}


/** MOUSE */
duk_ret_t n_mouseSetCursorVisible(duk_context* ctx)
{
    wInputSetCursorVisible(duk_to_boolean(ctx, 0) != 0);

    return 0;
}
duk_ret_t n_mouseIsCursorVisible(duk_context* ctx)
{
    duk_push_boolean(ctx, (wInputIsCursorVisible()) ? 1 : 0);

    return 1;
}
duk_ret_t n_mouseIsEventAvailable(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    duk_push_boolean(ctx, wInputIsMouseEventAvailable());

    return 1;
}
duk_ret_t n_mouseSetLogicalPosition(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_NOT_EQUAL(2) != DUK_ERR_NONE)
    //    return 0;

    wVector2f pos = {duk_to_number(ctx, 0), duk_to_number(ctx, 1)};
    wInputSetMouseLogicalPosition(&pos);
    /*mousePos.x = duk_to_number(ctx, 0);
    mousePos.y = duk_to_number(ctx, 1);
    oldMousePos = mousePos;
    wInputSetMousePosition(&mousePos);*/

    return 0;
}
duk_ret_t n_mouseSetPosition(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_NOT_EQUAL(2) != DUK_ERR_NONE)
    //    return 0;

    wVector2i pos = {duk_to_int(ctx, 0), duk_to_int(ctx, 1)};
    //wInputSetMouseAbsolutePosition(&pos);
/*
    mousePos.x = duk_to_int(ctx, 0);
    mousePos.y = duk_to_int(ctx, 1);
    oldMousePos = mousePos;
*/
    wInputSetMousePosition(&pos);

    return 0;
}
duk_ret_t n_mouseGetX(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    wVector2i pos;
    wInputGetMousePosition(&pos);
    duk_push_int(ctx, pos.x);
/*
    wVector2f pos;
    wInputGetMousePosition(&pos);
    duk_push_number(ctx, pos.x);
*/
    return 1;
}
duk_ret_t n_mouseGetY(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    wVector2i pos;
    wInputGetMousePosition(&pos);
    duk_push_int(ctx, pos.y);
/*
    wVector2f pos;
    wInputGetMousePosition(&pos);
    duk_push_number(ctx, pos.y);
*/
    return 1;
}
duk_ret_t n_mouseGetDeltaX(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    duk_push_number(ctx, wInputGetMouseDeltaX());
    //duk_push_int(ctx, mousePos.x - oldMousePos.x);

    return 1;
}
duk_ret_t n_mouseGetDeltaY(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    duk_push_number(ctx, wInputGetMouseDeltaY());
    //duk_push_int(ctx, mousePos.y - oldMousePos.y);

    return 1;
}

static duk_ret_t n_mouse_load_object(duk_context *ctx)
{
    immutable duk_function_list_entry[] n_mouse_funcs =
    [
        { key: "setCursorVisible",   value: &n_mouseSetCursorVisible,   nargs: 1 },
        { key: "isCursorVisible",    value: &n_mouseIsCursorVisible,    nargs: 0 },
        { key: "isEventAvailable",   value: &n_mouseIsEventAvailable,   nargs: 0 },
        { key: "setLogicalPosition", value: &n_mouseSetLogicalPosition, nargs: 2 },
        { key: "setPosition", 		 value: &n_mouseSetPosition, 		nargs: 2 },
        { key: "getX",        		 value: &n_mouseGetX,               nargs: 0 },
        { key: "getY",        		 value: &n_mouseGetY,               nargs: 0 },
        { key: "getDeltaX",   		 value: &n_mouseGetDeltaX,          nargs: 0 },
        { key: "getDeltaY",   		 value: &n_mouseGetDeltaY,          nargs: 0 },
        { key: null,                 value: null,                       nargs: 0 }
    ];

    duk_push_object(ctx);
    duk_put_function_list(ctx, -1, &(n_mouse_funcs[0]));
    return 1;
}

void RegisterMouseCFunctions()
{
    duk_push_c_function(ctx, &n_mouse_load_object, 0);
    duk_call(ctx, 0);
    duk_put_global_string(ctx, "Mouse");
}

