module list;

struct SListElement
{
    SListElement* prev;
    SListElement* next;

    void* value;
}

struct SList
{
    SListElement* first;        /* head */
    SListElement* last;         /* tail */

    uint size;

    /** create this.and return pointer to list */
    /*this()
    {

    }*/

    /** delete all elements from list */
    void Clear()
    {
        if (this.size == 0 || this.first == null)
            return;

        /* проходимся от первого до последнего */
        SListElement* element = this.first;
        SListElement* nextElement = null;
        while (element != null)
        {
            nextElement = element.next;

            /* удаляем значение, если таковое имеется */
            element.value = null;

            /* удаляем элемент, если есть */
            //free(element);
            destroy(element);

            element = nextElement;
        }

        /* чистим указатели на первый и последний элементы */
        this.first = null;
        this.last = null;
        this.size = 0;
    }

    /** clear and destroy list */
    ~this()
    {
        this.Clear();
    }


    /** add value to exists list, return false if not */
    bool AddElement (void* value)
    {
        if (value == null)
            return false;

        //SListElement* newElement = malloc(sizeof(SListElement));
        SListElement* newElement = new SListElement;
        newElement.value = value;
        newElement.next = null;
        ++this.size;

        // if it is first element
        if (this.first == null)
        {
            newElement.prev = null;
            this.first = newElement;
            this.last = newElement;
        }
        else
        {
            newElement.prev = this.last;
            this.last.next = newElement;

            this.last = newElement;
        }

        return true;
    }

    /** delete element from list */
    bool DeleteElement(SListElement* element)
    {
        if (element == null)
            return false;

        // удаляем элемент и сцепляем соседей

        element.value = null;

        --this.size;

        // элемент где-то между головой и хвостом
        if (element.prev != null && element.next != null)
        {
            element.prev.next = element.next;
            element.next.prev = element.prev;

            goto element_delete;
        }

        // элемент головной
        if (element.prev == null && element.next != null)
        {
            this.first = element.next;
            element.next.prev = null;

            goto element_delete;
        }

        // элемент хвостовой
        if (element.prev != null && element.next == null)
        {
            this.last = element.prev;
            element.prev.next = null;

            goto element_delete;
        }

        // элемент последний
        if ((this.size == 0) || (element.prev == null && element.next == null))
        {
            // уже всё удалено. Гудбай
            this.first = null;
            this.last = null;
            this.size = 0;
        }

        element_delete:
            destroy(element);
            element = null;

        return true;
    }

    /** delete value from list */
    void DeleteElementByValue(void* value)
    {
        if (value == null || this.size == 0)
            return;

        SListElement* element = this.GetElementByValue(value);
        if (element != null)
            this.DeleteElement(element);
    }

    /** delete value from list by position in list */
    void DeleteElementByNumber(uint numOfElement)
    {
        if (numOfElement >= this.size || this.size == 0)
            return;

        SListElement* element = this.GetElementByNumber(numOfElement);
        if (element != null)
            this.DeleteElement(element);
    }


    /* GETTERS */
    /** return element by value */
    SListElement* GetElementByValue(const void* value)
    {
        if (value == null || this.size == 0)
            return null;

        SListElement* element = this.first;
        while ((element != null) && (element.value != value))
                element = element.next;

        return element;
    }

    /** return element by number in list */
    SListElement* GetElementByNumber(uint numOfElement)
    {
        if (numOfElement >= this.size || this.size == 0)
            return null;

        SListElement* element = null;
        /* сheck which way is faster - from the head or tail */
        if ((this.size - numOfElement) >= numOfElement)
        {
            /* go from head */
            element = this.first;
            for (uint i = 0; (element != null) && (i < numOfElement); ++i)
                element = element.next;
        }
        else
        {
            /* go from tail */
            element = this.last;
            for (uint i = this.size - 1; (element != null) && (i > numOfElement); --i)
                element = element.prev;
        }

        return element;
    }

    /** return value by number in list */
    void* GetValueByNumber(uint numOfElement)
    {
        if (numOfElement >= this.size || this.size == 0)
            return null;

        SListElement* element = this.GetElementByNumber(numOfElement);
        if (element == null)
            return null;

        return element.value;
    }

    /** return number of value in list (if exists), else return -1 */
    int GetNumberByValue(void* value)
    {
        if (value == null || this.size == 0)
            return -1;

        // пробегаемся по всем элементам, пока не наткнемся на значение или на конец...
        int num = 0;
        SListElement* element = this.first;
        while ((element != null) && (element.value != value))
        {
            element = element.next;
            ++num;
        }
        if (element != null && element.value == value)
            return num;

        return -1;
    }

    /** get count of elements in list */
    uint GetSize()
    {
        return this.size;
    }

    /* SETTERS */
    /** set value of element by position in dictionary */
    void SetValueByNumber(uint numOfElement, void* value)
    {
        if (value == null || numOfElement >= this.size || this.size == 0)
            return;

        SListElement* element = this.GetElementByNumber(numOfElement);
        if (element != null)
            element.value = value;
    }

    /** change value of element. Return false if value not changed */
    bool ChangeValue(const void* oldValue, void* newValue)
    {
        if (this.size == 0 || oldValue == null || newValue == null)
            return false;

        SListElement* element = this.GetElementByValue(oldValue);
        if (element != null)
        {
            element.value = newValue;
            return true;
        }

        return false;
    }
}
