module res_manager;

extern (C):

enum RES_TRUE = 1;
enum RES_FALSE = 0;
enum RES_ERROR = -1;
enum RES_NOT_FOUND = -2;
enum RES_NOT_OPENED = -3;
enum RES_NOT_READABLE = -4;
enum RES_NOT_WRITABLE = -5;

// __cplusplus

int Res_OpenZipForRead (const(char)* zipName);
int Res_GetCompressedFileSize (const(char)* fileName);
int Res_GetUncompressedFileSize (const(char)* fileName);
int Res_ReadFileFromZipToBuffer (const(char)* fileName, void* buffer, int bufferSize);
bool Res_CloseZipAfterRead ();
bool Res_OpenZipForWrite (const(char)* zipName, bool isAppend);
int Res_WriteFileFromBufferToZip (const(char)* fileName, const(void)* buffer, uint bufferSize, int compressionLevel);
bool Res_CloseZipAfterWrite ();

bool Res_PackAllFilesFromDirToZip (const(char)* path, int compressionLevel);
bool Res_UnpackAllFilesFromZipToDir (const(char)* zipName, const(char)* path, bool rewrite);
bool Res_ReadFileToBuffer (void** buffer, int* bufferSize, const(char)* fileName);
bool Res_WriteBufferToFile (void* buffer, int bufferSize, const(char)* fileName, bool rewrite);
void Res_DeleteAllFilesInDir (const(char)* dirName, bool deleteDir);

bool Res_OpenIniFromFile (const(char)* fileName);
bool Res_OpenIniFromBuffer (const(void)* buffer, uint bufferSize);
char* Res_IniReadString (const(char)* section, const(char)* key, const(char)* defaultValue);
int Res_IniReadInteger (const(char)* section, const(char)* key, int defaultValue);
long Res_IniReadInteger64 (const(char)* section, const(char)* key, long defaultValue);
bool Res_IniReadBool (const(char)* section, const(char)* key, bool defaultValue);
double Res_IniReadDouble (const(char)* section, const(char)* key, double defaultValue);
void Res_IniWriteString (const(char)* section, const(char)* key, const(char)* value);
void Res_IniWriteInteger (const(char)* section, const(char)* key, int value);
void Res_IniWriteInteger64 (const(char)* section, const(char)* key, long value);
void Res_IniWriteBool (const(char)* section, const(char)* key, bool value);
void Res_IniWriteDouble (const(char)* section, const(char)* key, double value);
bool Res_SaveIniToBuffer (void** buffer, int* bufferSize);
bool Res_SaveIniToFile (const(char)* fileName);
void Res_CloseIni ();

// __cplusplus

// __RES_MANAGER_H
