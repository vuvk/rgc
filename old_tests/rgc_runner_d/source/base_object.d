module base_object;

import std.stdio;
import std.conv;
import std.math;
import core.stdc.stdlib;

import duk_util;
import WorldSim3D;

import global;
import util;
import constants;
import sprites;
import doors_keys;

extern (C) :

enum TUserVarType
{
    uvtNone = 0,
    uvtNumber,
    uvtBool,
    uvtString,
    uvtVector
}

class TUserVar
{
    TUserVarType type;
    double   dVal;
    bool     bVal;
    string   sVal;
    double[] vVal;

    this(TUserVarType type, double dVal, bool bVal, string sVal, double[] vVal)
    {
        this.type = type;
        this.dVal = dVal;
        this.bVal = bVal;
        this.sVal = sVal;
        this.vVal = vVal;
    }

    ~this()
    {
        sVal.length = 0;
        vVal.length = 0;
    }
}

class TObject
{
    static TObject[] objects;     /* массив всех объектов в игре */

    int     id;                   /* идентификатор объекта */
    wVector3f  pivotOffset;       /* смещение опорной точки */
    wNode*     node;
    wSelector* physBody;
    bool       isVisible;        /* видимость объекта */
    TUserVar[string] userVars;   /* пользовательские значения */

    this()
    {
        isVisible = true;
        ++objects.length;
        objects[$ - 1] = this;
    }

    ~this()
    {
        clearVars();

        if (this.physBody)
        {
            if (g_WorldCollider != null)
                wCollisionGroupRemoveCollision(g_WorldCollider, this.physBody);
        }
            
        if (this.node) 
        {
            //wMesh* mesh = wNodeGetMesh(this.node);
            //if (mesh != null)
            //    wMeshDestroy(mesh);
            wNodeDestroy(this.node);
        }

        /* уменьшаем список доступных объектов */
        //writefln("objects length before = %d", objects.length);
        if (objects.length > 0)
        {
            bool founded;
            for (int i = 0; (i < objects.length) && (!founded); ++i)
            {
                if (objects[i] !is null && objects[i] == this)
                {
                    founded = true;
                    g_ObjectsIdTable[objects[i].id] = null;
                    for (int j = i; j < objects.length - 1; ++j)
                        objects[j] = objects[j + 1];
                    --objects.length;
                }
            }
        }
        //writefln("objects length = %d", objects.length);
    }

    /* установить видимость объекта */
    void setVisibility(bool isVisible)
    {
        this.isVisible = isVisible;
        if (this.node)
            wNodeSetVisibility(this.node, isVisible);
    }

    /* adders */
    void addVar(string key, double val)
    {
        this.userVars.remove(key);
        this.userVars[key] = new TUserVar(TUserVarType.uvtNumber, val, bool.init, string.init, []);

        this.userVars = this.userVars.rehash(); 
    }
    void addVar(string key, bool val)
    {
        this.userVars.remove(key);
        this.userVars[key] = new TUserVar(TUserVarType.uvtBool, double.init, val, string.init, []);

        this.userVars = this.userVars.rehash(); 
    }
    void addVar(string key, string val)
    {
        this.userVars.remove(key);
        this.userVars[key] = new TUserVar(TUserVarType.uvtString, double.init, bool.init, val, []);

        this.userVars = this.userVars.rehash(); 
    }
    void addVar(string key, double[] val)
    {
        this.userVars.remove(key);
        this.userVars[key] = new TUserVar(TUserVarType.uvtVector, double.init, bool.init, string.init, val);

        this.userVars = this.userVars.rehash();
    }

    /* setters */
    void setVar(string key, double val)
    {     
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null && uVar.type == TUserVarType.uvtNumber)
        {
            uVar.dVal = val;
            uVar.bVal = bool.init;
            uVar.sVal = string.init;
            uVar.vVal = [];
        }
    }
    void setVar(string key, bool val)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null && uVar.type == TUserVarType.uvtBool)
        {
            uVar.dVal = double.init;
            uVar.bVal = val;
            uVar.sVal = string.init;
            uVar.vVal = [];
        }    
    }
    void setVar(string key, string val)
    {
        TUserVar uVar = this.userVars.get(key, null);
        
        if (uVar !is null && uVar.type == TUserVarType.uvtString)
        {
            uVar.dVal = double.init;
            uVar.bVal = bool.init;
            uVar.sVal = val;
            uVar.vVal = [];
        }    
    }
    void setVar(string key, double[] val)
    {
        TUserVar uVar = this.userVars.get(key, null);

        if (uVar !is null && uVar.type == TUserVarType.uvtVector)
        {
            uVar.dVal = double.init;
            uVar.bVal = bool.init;
            uVar.sVal = string.init;
            uVar.vVal = val;
        }
    }

    /* getters */
    double getVarNumber(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null/* && uVar.type == TUserVarType.uvtNumber*/)
            return uVar.dVal;

        return double.init;
    }
    bool getVarBool(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null/* && uVar.type == TUserVarType.uvtBool*/)
            return uVar.bVal;

        return bool.init;
    }
    string getVarString(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null/* && uVar.type == TUserVarType.uvtString*/)
            return uVar.sVal;            

        return "undefined";
    }
    double[] getVarVector(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null/* && uVar.type == TUserVarType.uvtString*/)
            return uVar.vVal;

        return [double.max, double.max, double.max];
    }

    /* delete userVars value */
    void removeVar(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null)
        {
            this.userVars[key].destroy();
            this.userVars[key] = null;
            this.userVars.remove(key);
            this.userVars = this.userVars.rehash();   
        }
    }

    /* clear userVars */
    void clearVars()
    {
        this.userVars.clear();
    }

    /* check type */
    bool isVarNumber(string key)
    {      
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null && uVar.type == TUserVarType.uvtNumber)
            return true;
            
        return false;
    }
    bool isVarBool(string key)
    {        
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null && uVar.type == TUserVarType.uvtBool)
            return true;
            
        return false;
    }
    bool isVarString(string key)
    {        
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null && uVar.type == TUserVarType.uvtString)
            return true;
            
        return false;
    }
    bool isVarVector(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null && uVar.type == TUserVarType.uvtVector)
            return true;

        return false;
    }
}

/* указатель на обрабатываемый скриптом на данный момент объект */
__gshared uint g_ObjectId;
__gshared TObject[] g_ObjectsIdTable;   // таблица идентификаторов объектов
__gshared TObject[] g_ObjectsForDelete; // таблица объектов,которые надо удалить


/** заполнение таблицы идентификаторов */
void FillObjectsIdTable();
/** удаление объектов, помеченных на удаление */
void DeleteObjects();
void RegisterObjectCFunctions();


// IMPLEMENTATION


void FillObjectsIdTable()
{
    g_ObjectsIdTable = new TObject[ushort.max];

    int id;

    foreach (object; TObject.objects)
    {
        id = object.id;
        while (id == 0 || g_ObjectsIdTable[id] !is null)
        {
            id = rand % (ushort.max);
        }
        object.id = id;
        //writefln("id = %d", id);
        g_ObjectsIdTable[id] = object;
    }
}

void DeleteObjects()
{
    if (g_ObjectsForDelete.length == 0)
        return;

    TObject obj;
    for (int i = 0; i < g_ObjectsForDelete.length; ++i)
    {
        obj = g_ObjectsForDelete[i];
        if (obj is null)
            continue;

        // ищем чем объект является
        bool founded;

        // это спрайт?
        if (cast(TSpriteNode)obj && g_SpriteNodes.length > 0)
        {
            //writeln("try to clear sprite node");
            TSpriteNode sprite;
            for (int j = 0; j < g_SpriteNodes.length && !founded; ++j)
            {
                sprite = g_SpriteNodes[j];
                if (sprite !is null && sprite == obj)
                {
                    founded = true;
                    for (int k = j; k < g_SpriteNodes.length - 1; ++k)
                    {
                        g_SpriteNodes[k] = g_SpriteNodes[k + 1];
                    }
                    --g_SpriteNodes.length;

                    //writeln("try to delete sprite node");
                    sprite.destroy();
                    //writeln("deleted");
                }
            }
        }
        if (founded)
            continue;

        // это дверь?
        if (cast(TDoorNode)obj && g_DoorNodes.length > 0)
        {
            //writeln("try to clear door node");
            TDoorNode door;
            for (int j = 0; j < g_DoorNodes.length && !founded; ++j)
            {
                door = g_DoorNodes[j];
                if (door !is null && door == obj)
                {
                    founded = true;
                    for (int k = j; k < g_DoorNodes.length - 1; ++k)
                    {
                        g_DoorNodes[k] = g_DoorNodes[k + 1];
                    }
                    --g_DoorNodes.length;

                    //writeln("try to delete door node");
                    door.destroy();
                    //writeln("deleted");
                }
            }
        }
        if (founded)
            continue;

        // это ключ?
        if (cast(TKeyNode)obj && g_KeyNodes.length > 0)
        {
            //writeln("try to clear key node");
            TKeyNode key;
            for (int j = 0; j < g_KeyNodes.length && !founded; ++j)
            {
                key = g_KeyNodes[j];
                if (key !is null && key == obj)
                {
                    founded = true;
                    for (int k = j; k < g_KeyNodes.length - 1; ++k)
                    {
                        g_KeyNodes[k] = g_KeyNodes[k + 1];
                    }
                    --g_KeyNodes.length;

                    //writeln("try to delete key node");
                    key.destroy();
                    //writeln("deleted");
                }
            }
        }
        if (founded)
            continue;

        // что-то непонятное. Тоже удалить
        obj.destroy();
    }

    g_ObjectsForDelete.length = 0;
}


/** работа с объектами */
static duk_ret_t n_objectSetEnable(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        bool isVisible = (duk_to_boolean(ctx, 1) != 0) ? true : false;
        obj.setVisibility(isVisible);
    }

    return 0;
}
static duk_ret_t n_objectIsEnable(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
    {
        duk_push_boolean(ctx, 0);
        return 1;
    }

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        duk_push_boolean(ctx, (obj.isVisible) ? 1 : 0);
    }
    else
        duk_push_boolean(ctx, 0);

    return 1;
}
static duk_ret_t n_objectDestroy(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        obj.setVisibility(false);
        ++g_ObjectsForDelete.length;
        g_ObjectsForDelete[$ - 1] = obj;
    }

    return 0;
}

/* СЕТТЕРЫ */
/* POSITION */
static duk_ret_t n_objectSetPosition(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f position = { duk_to_number(ctx, 1), duk_to_number(ctx, 2), duk_to_number(ctx, 3) };
        wNodeSetPosition(obj.node, position);
    }    

    return 0;
}
static duk_ret_t n_objectSetPositionX(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f position = wNodeGetPosition(obj.node);
        position.x = duk_to_number(ctx, 1);
        wNodeSetPosition(obj.node, position);
    }

    return 0;
}
static duk_ret_t n_objectSetPositionY(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f position = wNodeGetPosition(obj.node);
        position.y = duk_to_number(ctx, 1);
        wNodeSetPosition(obj.node, position);
    }

    return 0;
}
static duk_ret_t n_objectSetPositionZ(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f position = wNodeGetPosition(obj.node);
        position.z = duk_to_number(ctx, 1);
        wNodeSetPosition(obj.node, position);
    }

    return 0;
}
static duk_ret_t n_objectMove(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        double x = duk_to_number(ctx, 1);
        double y = duk_to_number(ctx, 2);
        double z = duk_to_number(ctx, 3);
        wNodeMove(obj.node, wVector3f(x, y, z));
    }

    return 0;
}

/* ROTATION */
static duk_ret_t n_objectSetRotation(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f rot = { duk_to_number(ctx, 1), duk_to_number(ctx, 2), duk_to_number(ctx, 3) };
        wNodeSetRotation(obj.node, rot);
    }

    return 0;
}
static duk_ret_t n_objectSetRotationX(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f rot = wNodeGetRotation(obj.node);
        rot.x = duk_to_number(ctx, 1);
        wNodeSetRotation(obj.node, rot);
    }

    return 0;
}
static duk_ret_t n_objectSetRotationY(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f rot = wNodeGetRotation(obj.node);
        rot.y = duk_to_number(ctx, 1);
        wNodeSetRotation(obj.node, rot);
    }

    return 0;
}
static duk_ret_t n_objectSetRotationZ(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f rot = wNodeGetRotation(obj.node);
        rot.z = duk_to_number(ctx, 1);
        wNodeSetRotation(obj.node, rot);
    }

    return 0;
}
static duk_ret_t n_objectRotate(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        double x = duk_to_number(ctx, 1);
        double y = duk_to_number(ctx, 2);
        double z = duk_to_number(ctx, 3);
        //wNodeMove(obj.node, obj.pivotOffset);
        wNodeTurn(obj.node, wVector3f(x, y, z));
    }

    return 0;
}


/* ГЕТТЕРЫ */
static duk_ret_t n_objectGetId(duk_context* ctx)
{
    duk_push_uint(ctx, g_ObjectId);
    return 1;
}

/* POSITION */
static duk_ret_t n_objectGetPosition(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f position = wNodeGetPosition(obj.node);
        duk_idx_t arr_idx;
        arr_idx = duk_push_array(ctx);
        duk_push_number(ctx, position.x);
        duk_put_prop_index(ctx, arr_idx, 0);
        duk_push_number(ctx, position.y);
        duk_put_prop_index(ctx, arr_idx, 1);
        duk_push_number(ctx, position.z);
        duk_put_prop_index(ctx, arr_idx, 2);
    }

    return 1;
}
static duk_ret_t n_objectGetPositionX(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f position = wNodeGetPosition(obj.node);
        duk_push_number(ctx, position.x);
    }    

    return 1;
}
static duk_ret_t n_objectGetPositionY(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f position = wNodeGetPosition(obj.node);
        duk_push_number(ctx, position.y);
    }    

    return 1;
}
static duk_ret_t n_objectGetPositionZ(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f position = wNodeGetPosition(obj.node);
        duk_push_number(ctx, position.z);
    }    

    return 1;
}

/* ROTATION */
static duk_ret_t n_objectGetRotation(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f rot = wNodeGetRotation(obj.node);
        duk_idx_t arr_idx;
        arr_idx = duk_push_array(ctx);
        duk_push_number(ctx, rot.x);
        duk_put_prop_index(ctx, arr_idx, 0);
        duk_push_number(ctx, rot.y);
        duk_put_prop_index(ctx, arr_idx, 1);
        duk_push_number(ctx, rot.z);
        duk_put_prop_index(ctx, arr_idx, 2);
    }

    return 1;
}
static duk_ret_t n_objectGetRotationX(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f rot = wNodeGetRotation(obj.node);
        duk_push_number(ctx, rot.x);
    }

    return 1;
}
static duk_ret_t n_objectGetRotationY(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f rot = wNodeGetRotation(obj.node);
        duk_push_number(ctx, rot.y);
    }

    return 1;
}
static duk_ret_t n_objectGetRotationZ(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null && obj.node != null)
    {
        wVector3f rot = wNodeGetRotation(obj.node);
        duk_push_number(ctx, rot.z);
    }

    return 1;
}


/* USERDATA */

/* adders */
static duk_ret_t n_objectAddVarNumber(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key = to!string(duk_to_string(ctx, 1));
        double dVal = duk_to_number(ctx, 2);
        obj.addVar(key, dVal);
    }    

    return 0;
}
static duk_ret_t n_objectAddVarBool(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {

        string key = to!string(duk_to_string(ctx, 1));
        bool bVal = (duk_to_boolean(ctx, 2) != 0);
        obj.addVar(key, bVal);
    }    

    return 0;
}
static duk_ret_t n_objectAddVarString(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key = to!string(duk_to_string(ctx, 1));
        string sVal = to!string(duk_to_string(ctx, 2));
        obj.addVar(key, sVal);
    }    

    return 0;
}
static duk_ret_t n_objectAddVarVector(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key = to!string(duk_to_string(ctx, 1));
        double[] vVal = [duk_to_number(ctx, 2), duk_to_number(ctx, 3), duk_to_number(ctx, 4)];
        obj.addVar(key, vVal);
    }

    return 0;
}

/* setters */
static duk_ret_t n_objectSetVar(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key = to!string(duk_to_string(ctx, 1));

        TUserVar uVar = obj.userVars.get(key, null);
        if (uVar !is null)
        {
            switch(uVar.type)
            {
                default:
                case TUserVarType.uvtNone : 
                    break;

                case TUserVarType.uvtNumber :
                    obj.setVar(key, duk_to_number(ctx, 2));
                    break;   

                case TUserVarType.uvtBool :
                    obj.setVar(key, (duk_to_boolean(ctx, 2) != 0));
                    break;  

                case TUserVarType.uvtString :
                    obj.setVar(key, to!string(duk_to_string(ctx, 2)));
                    break;

                case TUserVarType.uvtVector :
                    obj.setVar(key, [duk_to_number(ctx, 2), duk_to_number(ctx, 3), duk_to_number(ctx, 4)]);
                    break;
            }
        }
    }    

    return 0;
}

/* getters */
static duk_ret_t n_objectGetVar(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key = to!string(duk_to_string(ctx, 1));

        TUserVar uVar = obj.userVars.get(key, null);
        if (uVar !is null)
        {
            switch(uVar.type)
            {
                default:
                case TUserVarType.uvtNone : 
                    duk_push_number(ctx, 0);
                    break;
                    
                case TUserVarType.uvtNumber :
                    double dVal = obj.getVarNumber(key);
                    duk_push_number(ctx, dVal);
                    break;   
                    
                case TUserVarType.uvtBool :
                    int bVal = (obj.getVarBool(key)) ? 1 : 0;
                    duk_push_boolean(ctx, bVal);
                    break;  
                    
                case TUserVarType.uvtString :
                    string s = obj.getVarString(key) ~ "\0";
                    duk_push_string(ctx, s.ptr);
                    break;

                case TUserVarType.uvtVector :
                    double[] vVal = obj.getVarVector(key);duk_idx_t arr_idx;
                    arr_idx = duk_push_array(ctx);
                    for (int i = 0; i < vVal.length; ++i)
                    {
                        duk_push_number(ctx, vVal[i]);
                        duk_put_prop_index(ctx, arr_idx, i);
                    }
                    //duk_pop(ctx);

                    break;
            }
        }
        else    // данных нет
        {
            duk_push_number(ctx, 0);
        }
    }    

    return 1;
}

/* remove */
static duk_ret_t n_objectRemoveVar(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {        
        string key = to!string(duk_to_string(ctx, 1));
        obj.removeVar(key);
    }    

    return 0;
}

/* clear data */
static duk_ret_t n_objectClearVars(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
       obj.clearVars();
    }    

    return 0;
}

/* check type */
static duk_ret_t n_objectIsVarNumber(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key = to!string(duk_to_string(ctx, 1));
        
        int bVal = (obj.isVarNumber(key)) ? 1 : 0;
        duk_push_boolean(ctx, bVal);
    }    

    return 1;
}
static duk_ret_t n_objectIsVarBool(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key = to!string(duk_to_string(ctx, 1));
        
        int bVal = (obj.isVarBool(key)) ? 1 : 0;
        duk_push_boolean(ctx, bVal);
    }    

    return 1;
}
static duk_ret_t n_objectIsVarString(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key = to!string(duk_to_string(ctx, 1));
        
        int bVal = (obj.isVarString(key)) ? 1 : 0;
        duk_push_boolean(ctx, bVal);
    }    

    return 1;
}
static duk_ret_t n_objectIsVarVector(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key = to!string(duk_to_string(ctx, 1));

        int bVal = (obj.isVarVector(key)) ? 1 : 0;
        duk_push_boolean(ctx, bVal);
    }

    return 1;
}


/* util */
static duk_ret_t n_objectIsInView(duk_context* ctx)
{
    int pos = duk_to_uint(ctx, 0);
    if (pos >= g_ObjectsIdTable.length)
        return 0;

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        duk_push_boolean(ctx, wNodeIsInView(obj.node) ? 1 : 0);
    }        
    
    return 1;
}
static duk_ret_t n_distanceBetweenObjects(duk_context* ctx)
{
    int pos0 = duk_to_uint(ctx, 0);
    int pos1 = duk_to_uint(ctx, 1);
    if (pos0 >= g_ObjectsIdTable.length || pos1 >= g_ObjectsIdTable.length)
        return 0;

    TObject obj0 = g_ObjectsIdTable[pos0];
    TObject obj1 = g_ObjectsIdTable[pos1];
    if ((obj0 !is null && obj1 !is null) &&
        (obj0.node != null && obj1.node != null))
    {
        duk_push_number(ctx, wNodesGetBetweenDistance(obj0.node, obj1.node));
    }
    else
    {
        duk_push_number(ctx, double.max);
    }

    return 1;
}

void RegisterObjectCFunctions()
{
    /* global */
    Duk_RegisterCFunction(&n_objectGetId,        0, "objectGetId"      );
    Duk_RegisterCFunction(&n_objectDestroy,      1, "objectDestroy"    );
    Duk_RegisterCFunction(&n_objectSetEnable,    2, "objectSetEnable"  );
    Duk_RegisterCFunction(&n_objectIsEnable,     1, "objectIsEnable"   );

    /* util */
    Duk_RegisterCFunction(&n_objectIsInView,          1, "objectIsInView"         );
    Duk_RegisterCFunction(&n_distanceBetweenObjects,  2, "distanceBetweenObjects" );

    /* position */
    Duk_RegisterCFunction(&n_objectSetPosition,  4, "objectSetPosition" );
    Duk_RegisterCFunction(&n_objectSetPositionX, 2, "objectSetPositionX");
    Duk_RegisterCFunction(&n_objectSetPositionY, 2, "objectSetPositionY");
    Duk_RegisterCFunction(&n_objectSetPositionZ, 2, "objectSetPositionZ");
    Duk_RegisterCFunction(&n_objectGetPosition,  1, "objectGetPosition" );
    Duk_RegisterCFunction(&n_objectGetPositionX, 1, "objectGetPositionX");
    Duk_RegisterCFunction(&n_objectGetPositionY, 1, "objectGetPositionY");
    Duk_RegisterCFunction(&n_objectGetPositionZ, 1, "objectGetPositionZ");
    Duk_RegisterCFunction(&n_objectMove,         4, "objectMove"        );

    /* rotation */
    Duk_RegisterCFunction(&n_objectSetRotation,  4, "objectSetRotation" );
    Duk_RegisterCFunction(&n_objectSetRotationX, 2, "objectSetRotationX");
    Duk_RegisterCFunction(&n_objectSetRotationY, 2, "objectSetRotationY");
    Duk_RegisterCFunction(&n_objectSetRotationX, 2, "objectSetRotationZ");
    Duk_RegisterCFunction(&n_objectGetRotationX, 1, "objectGetRotation" );
    Duk_RegisterCFunction(&n_objectGetRotationX, 1, "objectGetRotationX");
    Duk_RegisterCFunction(&n_objectGetRotationY, 1, "objectGetRotationY");
    Duk_RegisterCFunction(&n_objectGetRotationX, 1, "objectGetRotationZ");
    Duk_RegisterCFunction(&n_objectRotate,       4, "objectRotate"      );
    
    /* user data */ 
    Duk_RegisterCFunction(&n_objectAddVarNumber, 3, "objectAddVarNumber");
    Duk_RegisterCFunction(&n_objectAddVarBool,   3, "objectAddVarBool"  );
    Duk_RegisterCFunction(&n_objectAddVarString, 3, "objectAddVarString");
    Duk_RegisterCFunction(&n_objectAddVarVector, 5, "objectAddVarVector");
    Duk_RegisterCFunction(&n_objectSetVar,       DUK_VARARGS, "objectSetVar");
    Duk_RegisterCFunction(&n_objectGetVar,       2, "objectGetVar"      );
    Duk_RegisterCFunction(&n_objectRemoveVar,    2, "objectRemoveVar"   );
    Duk_RegisterCFunction(&n_objectClearVars,    1, "objectClearVars"   );
    Duk_RegisterCFunction(&n_objectIsVarNumber,  2, "objectIsVarNumber" );
    Duk_RegisterCFunction(&n_objectIsVarBool,    2, "objectIsVarBool"   );
    Duk_RegisterCFunction(&n_objectIsVarString,  2, "objectIsVarString" );
    Duk_RegisterCFunction(&n_objectIsVarVector,  2, "objectIsVarVector" );
}
