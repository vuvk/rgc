module util;

import std.stdio;
import std.file;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;

import WorldSim3D;
import constants;
import global;
import res_manager;



bool FileExists(string fileName);

double sqr(double num);

uint ARGB1555toARGB8888(ushort c);
ushort ARGB8888toARGB1555(uint c);

wTexture* LoadTextureFromMemory(string name, wVector2i size, int sizeForRead, void* buffer);
wTexture* LoadTexture(string fileName, string textureName);



// IMPLEMENTATION

bool FileExists(string fileName)
{
    File f = File(fileName, "r");
    if (f.isOpen())
    {
        f.close();
        return true;
    }

    return false;
}

@nogc nothrow pure double sqr(double num)
{
    return num*num;
}

uint ARGB1555toARGB8888(ushort c)
{
    const uint a = c&0x8000, r = c&0x7C00, g = c&0x03E0, b = c&0x1F;
    const uint rgb = (r << 9) | (g << 6) | (b << 3);
    return (a*0x1FE00) | rgb | ((rgb >> 5) & 0x070707);
}

ushort ARGB8888toARGB1555(uint c)
{
    return cast(ushort)((((c>>16)&0x8000) | ((c>>9)&0x7C00) | ((c>>6)&0x03E0) | ((c>>3)&0x1F)));
}

wTexture* LoadTextureFromMemory(string name, wVector2i size, int sizeForRead, void* buffer)
{
    if (buffer == null)
        return null;

    wTexture* loadTexture;
    void* loadTextureBits;

    // при софтверном рендере нужно преобразовать формат в ARGB1555
    if (g_Renderer == wDriverTypes.wDRT_SOFTWARE/* || g_Renderer == wDRT_BURNINGS_VIDEO*/)
    {
        int    c32;
        short* c16 = null;

        loadTexture = wTextureCreate(cast(char*)name.ptr, size, wColorFormat.wCF_A1R5G5B5);
        c16 = cast(short*)wTextureLock(loadTexture);
        for (int i = 0; i < (sizeForRead / int.sizeof); ++i, ++c16)
        {
            c32 = (cast(int*)buffer)[i];
            // если альфа ноль, то значит область прозрачна
            if ((c32 >> 24) == 0)
            {
                *c16 = 0;
            }
            else
            {
                *c16 = ARGB8888toARGB1555(c32);
            }
        }
        wTextureUnlock(loadTexture);
    }
    // при всех остальных рендерах просто загрузить текстуру как она есть
    else
    {
        loadTexture = wTextureCreate(cast(char*)name.ptr, size, wColorFormat.wCF_A8R8G8B8);
        loadTextureBits = wTextureLock(loadTexture);
        memcpy(loadTextureBits, buffer, sizeForRead);
        wTextureUnlock(loadTexture);
    }

    return loadTexture;
}

wTexture* LoadTexture(string fileName, string textureName)
{
    wTexture* txr;
    byte*  buffer;
    int    bufferSize;
    ushort w, h;
    uint   textureSize;

    fileName ~= "\0";
    textureName ~= "\0";

    if (g_PreviewMode)
    {
        string fullPath = "resources/textures/" ~ fileName;

        if (!Res_ReadFileToBuffer(cast(void**)&buffer, &bufferSize, fullPath.ptr))
            goto end_func;
    }
    else
    {
        bufferSize = Res_GetUncompressedFileSize(fileName.ptr);
        if (bufferSize == 0)
            goto end_func;

        buffer = cast(byte*)calloc(bufferSize, 1);
        if (buffer == null)
            goto end_func;

        if (!Res_ReadFileFromZipToBuffer(fileName.ptr, buffer, bufferSize))
            goto end_func;
    }

    // читаем размеры
    w = *(cast(ushort*)buffer);
    h = *(cast(ushort*)(buffer + 2));
    textureSize = w * h * uint.sizeof;

    txr = LoadTextureFromMemory(textureName, wVector2i(w, h), textureSize, buffer + w.sizeof + h.sizeof);


end_func:
    if (buffer)
    {
        free(buffer);
        buffer = null;
    }

    return txr;
}
