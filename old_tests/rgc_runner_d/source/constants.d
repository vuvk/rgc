module constants;

import std.math;

immutable int MAX_FPS = 60;   /** Максимальный FPS для установки ограничения */

immutable int MAP_WIDTH  = 64;  /** Максимальная ширина карты */
immutable int MAP_HEIGHT = 64;  /** Максимальная высота карты */

immutable int TEXTURE_MAX_WIDTH  = 256;  /** Максимальная ширина текстуры в пикселях */
immutable int TEXTURE_MAX_HEIGHT = 256;  /** Максимальная высота текстуры в пикселях */
immutable int TEXTURE_MAX_SIZE   = TEXTURE_MAX_WIDTH * TEXTURE_MAX_HEIGHT * int.sizeof;   /** Максимальный размер текстуры в байтах */

immutable int TEXTURES_MAX_COUNT = 1024;
immutable int SPRITES_MAX_COUNT  = 1024;
immutable int DOORS_MAX_COUNT    = 1024;
immutable int KEYS_MAX_COUNT     = 1024;

/*
immutable string MAPS_PACK_NAME     = "maps.zip";
immutable string TEXTURES_PACK_NAME = "textures.zip";
immutable string SPRITES_PACK_NAME  = "sprites.zip";
*/

// размер объектов в игре?
immutable int OBJECTS_WIDTH   = 10;
immutable int OBJECTS_HEIGHT  = 10;

immutable double DEG_TO_RAD_COEFF = PI / 180.0;
immutable double RAD_TO_DEG_COEFF = 180.0 / PI;
