module app;

import std.stdio;
import std.conv;
import core.memory;
import core.thread;

import WorldSim3D;
import SampleFunctions;

import duk_util;
import global;
import res_manager;
import textures;
import sprites;
import doors_keys;
import map;

import base_object;
import player;


int main(string[] args)
{
    int prevFPS;
    wstring wndCaption = "Raycasting Game Constructor Runner";

    //GC.disable();

    // перебираем параметры
    for (int i = 0; i < args.length; ++i)
    {
        // установка рендера
        if ("-ogl"   == args[i]) { g_Renderer = wDriverTypes.wDRT_OPENGL;    }
        if ("-d3d"   == args[i]) { g_Renderer = wDriverTypes.wDRT_DIRECT3D9; }
        if ("-soft"  == args[i]) { g_Renderer = wDriverTypes.wDRT_SOFTWARE;       g_LimitFps = false; }
        if ("-soft2" == args[i]) { g_Renderer = wDriverTypes.wDRT_BURNINGS_VIDEO; g_LimitFps = false; }
        if ("-vsync" == args[i]) { g_VSync    = true; }

        if ("-no_limit_fps" == args[i]) { g_LimitFps = false; }
    }
    g_LimitFps = false;

    ///Start engine
    bool init = wEngineStart(g_Renderer, wDEFAULT_SCREENSIZE, 32, false, false, true, g_VSync);
    if (!init)
    {
        PrintWithColor("wEngineStart() failed!", wConsoleFontColor.wCFC_RED, true);
        return -1;
    }

    InitRunner();

    wFont* font = wFontLoad(cast(char*)"assets/4.png".ptr);
    wVector3f camPos;

    TScript playerInitScript;
    TScript playerUpdateScript;
    TScript cameraInitScript;
    TScript cameraUpdateScript;

    void StartInterpreter()
    {
        Duk_InitVM();

        TScript tmp;
        /* load global values */
        Duk_LoadScriptFromFile(&tmp, "assets/scripts/global.js");
        Duk_RunScript(tmp, true);
        Duk_FreeScript(&tmp);

        /* load key codes */
        Duk_LoadScriptFromFile(&tmp, "assets/scripts/keycodes.js");
        Duk_RunScript(tmp, true);
        Duk_FreeScript(&tmp);

        /* load vectors */
        Duk_LoadScriptFromFile(&tmp, "assets/scripts/vectors.js");
        Duk_RunScript(tmp, true);
        Duk_FreeScript(&tmp);

        /* player's scripts */
        g_ObjectId = g_Player.id;
        Duk_LoadScriptFromFile(&playerInitScript, "assets/scripts/player/player_init.js");
        Duk_RunScript(playerInitScript, true);
        Duk_FreeScript(&playerInitScript);
        Duk_LoadScriptFromFile(&playerUpdateScript, "assets/scripts/player/player_update.js");

        /* camera's scripts */
        g_ObjectId = g_Camera.id;
        Duk_LoadScriptFromFile(&cameraInitScript, "assets/scripts/player/camera_init.js");
        Duk_RunScript(cameraInitScript, true);
        Duk_FreeScript(&cameraInitScript);
        Duk_LoadScriptFromFile(&cameraUpdateScript, "assets/scripts/player/camera_update.js");

        /* door's scripts */
        TDoorInMem* door;
        for (int i = 0; i < g_DoorsCount; ++i)
        {
            door = &(g_Doors[i]);
            if (door == null)
                continue;

            if (door.initScript   != null) door.initScript.destroy();
            if (door.updateScript != null) door.updateScript.destroy();

            door.initScript = new TScript;
            Duk_LoadScriptFromFile(door.initScript, "assets/scripts/doors/door_init.js");

            door.updateScript = new TScript;
            Duk_LoadScriptFromFile(door.updateScript, "assets/scripts/doors/door_update.js");
        }
        foreach(doorNode; g_DoorNodes)
        {
            if (doorNode !is null)
            {
                g_ObjectId = doorNode.id;
                doorNode.Init();
            }
        }

        /* keys scripts*/
        TKeyInMem* key;
        for (int i = 0; i < g_KeysCount; ++i)
        {
            key = &(g_Keys[i]);
            if (key == null)
                continue;

            if (key.initScript   != null) key.initScript.destroy();
            if (key.updateScript != null) key.updateScript.destroy();

            key.initScript = new TScript;
            Duk_LoadScriptFromFile(key.initScript, "assets/scripts/keys/key_init.js");

            key.updateScript = new TScript;
            Duk_LoadScriptFromFile(key.updateScript, "assets/scripts/keys/key_update.js");
        }
        foreach(keyNode; g_KeyNodes)
        {
            if (keyNode !is null)
            {
                g_ObjectId = keyNode.id;
                keyNode.Init();
            }
        }
    }

    void LoadLevel(int num, bool firstRun = false)
    {
        //wSceneDestroyAllNodes();
        //wSceneDestroyAllMeshes();
        //wSceneDestroyAllTextures();

        // открываем пак
        int res = Res_OpenZipForRead("resources.pk3");
        switch (res)
        {
            case RES_NOT_FOUND  :
                PrintWithColor("Pack of maps is doesn''t exist!\n", wConsoleFontColor.wCFC_RED, false);
                break;
            case RES_NOT_OPENED :
                PrintWithColor("Error when opening pack of maps!\n", wConsoleFontColor.wCFC_RED, false);
                break;

            case RES_TRUE :
                //printf("LoadMapPack");
                if (firstRun)
                    LoadMapPack();
                //printf("Prepare");
                PrepareTexturesNeedForLoad(num);
                PrepareSpritesNeedForLoad(num);
                PrepareKeysNeedForLoad(num);
                PrepareDoorsNeedForLoad(num);
                //printf("LoadTexturePack");
                LoadTexturePack();
                //printf("LoadSpritePack");
                LoadSpritePack();
                //printf("LoadDoorPack");
                LoadKeyPack();
                LoadDoorPack();
                //printf("GenerateLevel");
                GenerateLevel(num);
                //printf("done");
                break;

            default : break;
        }
        Res_CloseZipAfterRead();

        /* создаем игрока */
        g_Player = new TPlayer(3.0, 0.1, -3.0);
        g_Camera = new TCamera(0.0, 0.0, 0.0);

        /* и только теперь мы можем собрать таблицу идентификаторов объектов */
        FillObjectsIdTable();

        StartInterpreter();
    }

    LoadLevel(0, true);
/*
    thread_init();
    Thread thrdSpriteNodesUpdate  = new Thread({SpriteNodesUpdate();});
    Thread thrdTextureNodesUpdate = new Thread({TextureNodesUpdate();});
    Thread thrdDoorNodesUpdate    = new Thread({DoorNodesUpdate();});
    Thread thrdKeyNodesUpdate     = new Thread({KeyNodesUpdate();});
*/
    while (wEngineRunning())
    {
        wSceneBegin(wColor4s(0, 0, 0, 0));
        g_DeltaTime = wTimerGetDelta();

        //GC.disable();

        /* запускаем скрипт для камеры */
        g_ObjectId = g_Camera.id;
        Duk_RunScript(cameraUpdateScript);
        /* запускаем скрипт для игрока */
        g_ObjectId = g_Player.id;
        Duk_RunScript(playerUpdateScript);

        // старое - в один поток
        SpriteNodesUpdate();
        TextureNodesUpdate();
        DoorNodesUpdate();
        KeyNodesUpdate();

/*
        //duk_thread_state st;
        //duk_suspend(ctx, &st);

        thrdSpriteNodesUpdate.start();
        thrdTextureNodesUpdate.start();
        thrdDoorNodesUpdate.start();
        thrdKeyNodesUpdate.start();

        thrdSpriteNodesUpdate.join();
        thrdTextureNodesUpdate.join();
        thrdDoorNodesUpdate.join();
        thrdKeyNodesUpdate.join();

        //thread_joinAll();
        //duk_resume(ctx, &st);
*/

        // ищем и удаляем объекты, помеченные на удаление
        DeleteObjects();

        wSceneDrawAll();

        camPos = wNodeGetPosition(g_Camera.node);
        wstring strBuffer = "Camera Pos : " ~ to!wstring(camPos.x) ~ " " ~ to!wstring(camPos.z) ~ "\0";
        wFontDraw(font, strBuffer.ptr, wVector2i(10, 10), wVector2i(100, 20), wCOLOR4s_WHITE);

        wSceneEnd();

        if (wInputIsKeyEventAvailable())
        {
            // перезапуск виртуальной машины
            if (wInputIsKeyUp(wKeyCode.wKC_KEY_T))
            {
                Duk_StopVM();
                StartInterpreter();
            }

            // рестарт уровня
            if (wInputIsKeyUp(wKeyCode.wKC_KEY_R))
            {
                g_Player.destroy();
                g_Camera.destroy();     //writeln("1");
                //ClearLevel();
                //writeln("1");
                Duk_StopVM();
                Duk_InitVM();
                //writeln("1");

                LoadLevel(0);
                //writeln("loaded!");
            }

            if (wInputIsKeyHit(wKeyCode.wKC_F10))
                wSystemSaveScreenShot("screenshot.jpg".ptr);

            if (wInputIsKeyUp(wKeyCode.wKC_KEY_L))
            {
                wMesh*[1000] batch;
                wMesh*[1000] mesh;
                for (int i = 0; i < 1000; ++i)
                {
                    batch[i] = wMeshCreateBatching();
                    wMeshFinalizeBatching(batch[i]);
                }
                for (int i = 0; i < 1000; ++i)
                {
                    wMeshClearBatching(batch[i]);
                    wMeshDestroyBatching(batch[i]);
                    wMeshDestroy(mesh[i]);
                }
            }
        }

        ///Close by ESC
        wEngineCloseByEsc();

        /// update FPS
        if (prevFPS != wEngineGetFPS())
        {
            prevFPS = wEngineGetFPS();

            strBuffer = wndCaption ~ "   FPS : " ~ to!wstring(prevFPS) ~ "\0";
            wWindowSetCaption(strBuffer.ptr);
        }

        //GC.enable();
    }

    //InterpreterCloseVM();
    Duk_StopVM();

    ///Stop engine
    wEngineStop();

    return 0;
}
