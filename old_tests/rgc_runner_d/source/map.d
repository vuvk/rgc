module map;

import std.stdio;
import std.conv;
import std.math;

import list;
import constants;
import global;
import util;

import duk_util;
import res_manager;

import WorldSim3D;
import SampleFunctions;
import base_object;
import textures;
import sprites;
import doors_keys;

alias TMapElement = ushort;

extern (C):

/** карта */
struct TMap
{
    string  name;           // имя карты
    int     floorColor;     // цвет пола
    int     ceilColor;      // цвет потолка
    int     fogColor;       // цвет тумана
    float   fogIntensity;   // интенсивность тумана
    bool    showFloor;      // показывать текстурный пол?
    bool    showCeil;       // показывать текстурный потолок?
    bool    showFog;        // показывать туман?
    bool    showSky;        // показывать небо?
    short   skyNumber;      // номер скайбокса
    TMapElement[MAP_WIDTH][MAP_HEIGHT] ceil;     // массив с индексами текстур потолка
    TMapElement[MAP_WIDTH][MAP_HEIGHT] level;    // массив с содержимым уровня (стены, спрайты, двери и т.д.)
    TMapElement[MAP_WIDTH][MAP_HEIGHT] floor;    // массив с индексами текстур пола
}

/** пак карт */
struct TMapPack
{
    TMap[]  maps;
}

__gshared TMapPack mapPack;
__gshared int g_NumOfCurMap = -1;

private
{
    // простая плоскость для пола и потолка
    wNode* floorNode;
    wNode* ceilNode;
    // нода Скайбокса
    wNode* skyNode;
    wTexture* skyTextureFront;
    wTexture* skyTextureBack;
    wTexture* skyTextureLeft;
    wTexture* skyTextureRight;
    wTexture* skyTextureTop;
    wTexture* skyTextureBottom;
}

/* работа с паком карт */
/** инициализация пака карт*/
void InitMapPack();
/** загрузка пака карт*/
bool LoadMapPack();
/** очистить текущийц уровень*/
void ClearLevel();
/** сгенерировать уровень №__ */
void GenerateLevel(int numOfMap);

void RegisterMapCFunctions();

// IMPLEMENTATION

/** инициализация пака карт*/
void InitMapPack()
{
    mapPack.maps = new TMap[g_MapsCount];
}

/** загрузка пака карт*/
bool LoadMapPack()
{
    TMapElement t;
    ubyte    x, y, m; //объект, координаты, номер карты
    int      /*i, */c;
    ushort   count;
    ubyte[]  buffer = new ubyte[0];
    int      bufferSize;
    bool     isLoaded;
    int      mapSize;
    string   fileName;
    ubyte*   p;

    PrintWithColor("Starting load maps...\n", wConsoleFontColor.wCFC_YELLOW, false);
    
    mapSize = MAP_WIDTH * MAP_HEIGHT;

    // читаем количество карт
    bufferSize = Res_GetUncompressedFileSize("maps.cfg\0");
    if (bufferSize <= 0)
    {
        PrintWithColor("can not get size of maps.cfg!", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    buffer.length = bufferSize;
    if (Res_ReadFileFromZipToBuffer("maps.cfg", buffer.ptr, bufferSize) <= 0) goto end;
    if (!Res_OpenIniFromBuffer(buffer.ptr, bufferSize))
    {
        PrintWithColor("can not open maps.cfg!!!", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    g_MapsCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_MapsCount == 0) goto end;

    InitMapPack();

    for (m = 0; m < g_MapsCount; ++m)
    {
        // читаем параметры карты
        fileName = "map_" ~ to!string(m) ~ ".cfg\0";
        bufferSize = Res_GetUncompressedFileSize(fileName.ptr);
        if (bufferSize <= 0)
        {
            writef("cannot get size of map_%d.cfg!\n", m);
            continue;
        }
        buffer.length = bufferSize;

        Res_ReadFileFromZipToBuffer(fileName.ptr, buffer.ptr, bufferSize);
        Res_OpenIniFromBuffer(buffer.ptr, bufferSize);
        with (mapPack.maps[m])
        {
            name = to!string(Res_IniReadString("Options", "name", ("Level #" ~ to!string(m) ~ "\0").ptr));
            floorColor = Res_IniReadInteger("Options", "floor_color", 0);
            ceilColor  = Res_IniReadInteger("Options", "ceil_color",  0);
            fogColor   = Res_IniReadInteger("Options", "fog_color",   0);
            fogIntensity = Res_IniReadDouble("Options", "fog_intensity", 0);
            showFloor  = Res_IniReadBool   ("Options", "show_floor", false);
            showCeil   = Res_IniReadBool   ("Options", "show_ceil",  false);
            showFog    = Res_IniReadBool   ("Options", "show_fog",   false);
            showSky    = Res_IniReadBool   ("Options", "show_sky",   false);
            skyNumber  = cast(short)Res_IniReadInteger("Options", "sky_number", 0);
            skyNumber--;    // т.к. порядок нумерации скайбоксов с нуля
        }
        Res_CloseIni();

        ////////////////////////////////////
        // читаем карту
        ////////////////////////////////////
        fileName = "map_" ~ to!string(m) ~ ".dat\0";
        bufferSize = Res_GetUncompressedFileSize(fileName.ptr);
        if (bufferSize <= 0)
        {
            printf("cannot get size of map_%d.dat!\n", m);
            continue;
        }
        buffer.length = bufferSize;
        Res_ReadFileFromZipToBuffer(fileName.ptr, buffer.ptr, bufferSize);

        /////////////////////////////////////////////////////
        // читаем зарисованные ячейки пола (если есть)
        /////////////////////////////////////////////////////
        p = cast(ubyte*)buffer.ptr;
        count = *(cast(ushort*)p);
        p += count.sizeof;
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                t = *(cast(TMapElement*)p);
                p += TMapElement.sizeof;
                x = *p;
                p += x.sizeof;
                y = *p;
                p += y.sizeof;

                mapPack.maps[m].floor[x][y] = t;
            }
        }

        /////////////////////////////////////////////////////
        // читаем зарисованные ячейки уровня (если есть)
        /////////////////////////////////////////////////////
        count = *(cast(ushort*)p);
        p += count.sizeof;
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                t = *(cast(TMapElement*)p);
                p += TMapElement.sizeof;
                x = *p;
                p += x.sizeof;
                y = *p;
                p += y.sizeof;

                mapPack.maps[m].level[x][y] = t;
            }
        }

        /////////////////////////////////////////////////////
        // читаем зарисованные ячейки потолка (если есть)
        /////////////////////////////////////////////////////
        count = *(cast(ushort*)p);
        p += count.sizeof;
        if (count > 0)
        {
            for (c = 0; c < count; ++c)
            {
                t = *(cast(TMapElement*)p);
                p += TMapElement.sizeof;
                x = *p;
                p += x.sizeof;
                y = *p;
                p += y.sizeof;

                mapPack.maps[m].ceil[x][y] = t;
            }
        }
    }



    isLoaded = true;

end:
    buffer.length = 0;
    Res_CloseIni();
    PrintWithColor("Finished load maps.\n", wConsoleFontColor.wCFC_YELLOW, false);
    writefln("Maps Count - %d", g_MapsCount);

    return isLoaded;
}

/** очистить текущий уровень*/
void ClearLevel()
{
    int i;
    wMesh* mesh;

    //writeln("2");
    if (g_WorldCollider != null)
    {
        wCollisionGroupRemoveAll(g_WorldCollider);
        g_WorldCollider = null;
    }
    //writeln("delete textures nodes");

    if (g_TextureNodes != null && g_TextureNodes.size > 0)
    {
        SListElement* element = g_TextureNodes.first;
        for (i = 0; (i < g_TextureNodes.size) && (element != null); ++i, element = element.next)
        {
            if (element.value != null)
            {
                (*cast(TTextureNode*)(element.value)).destroy();
            }
        }
        g_TextureNodes.Clear();
    }
    //writeln("delete sprites nodes");

    if (g_SpriteNodes.length > 0)
    {
        foreach (sprite; g_SpriteNodes)
        {
            if (sprite !is null)
                sprite.destroy();
        }
        g_SpriteNodes.length = 0;
    }
    //writeln("delete doors nodes");
    
    if (g_DoorNodes.length > 0)
    {
        foreach (door; g_DoorNodes)
        {
            if (door !is null)
                door.destroy();
        }
        g_DoorNodes.length = 0;
    }
    //writeln("delete another nodes");

    if (g_KeyNodes.length > 0)
    {
        foreach (key; g_KeyNodes)
        {
            if (key !is null)
                key.destroy();
        }
        g_KeyNodes.length = 0;
    }

    if (g_InvisibleTexture != null)
    {
        wTextureDestroy(g_InvisibleTexture);
        g_InvisibleTexture = null;
    }

    if (g_InvisibleMesh != null)
    {
        wMeshDestroy(g_InvisibleMesh);
        g_InvisibleMesh = null;
    }

/*
    if (invisibleNode != null)
    {
        wNodeDestroy(invisibleNode);
        invisibleNode = null;
    }
*/

    if (floorNode != null)
    {
        mesh = wNodeGetMesh(floorNode);
        if (mesh != null)
            wMeshDestroy(mesh);
        wNodeDestroy(floorNode);
        floorNode = null;
    }

    if (ceilNode != null)
    {
        mesh = wNodeGetMesh(ceilNode);
        if (mesh != null)
            wMeshDestroy(mesh);
        wNodeDestroy(ceilNode);
        ceilNode = null;
    }

    if (skyNode != null)
    {
        mesh = wNodeGetMesh(skyNode);
        if (mesh != null)
            wMeshDestroy(mesh);
        wNodeDestroy(skyNode);
        skyNode = null;

        if (skyTextureFront != null)
        {
            wTextureDestroy(skyTextureFront);
            skyTextureFront = null;
        }
        if (skyTextureBack != null)
        {
            wTextureDestroy(skyTextureBack);
            skyTextureBack = null;
        }
        if (skyTextureLeft != null)
        {
            wTextureDestroy(skyTextureLeft);
            skyTextureLeft = null;
        }
        if (skyTextureRight != null)
        {
            wTextureDestroy(skyTextureRight);
            skyTextureRight = null;
        }
        if (skyTextureTop != null)
        {
            wTextureDestroy(skyTextureTop);
            skyTextureTop = null;
        }
        if (skyTextureBottom != null)
        {
            wTextureDestroy(skyTextureBottom);
            skyTextureBottom = null;
        }
    }


    // на всякий случай
    wSceneDestroyAllNodes();
    wSceneDestroyAllMeshes();
    
    g_NumOfCurMap = -1;
    //writeln("2");

    //writeln("delete objects");
    TObject.objects.length = 0;
    //writeln("done");
}

/** сгенерировать уровень №__ */
void GenerateLevel(int numOfMap)
{
    /* ВЛОЖЕННЫЕ ФУНКЦИИ */
    wMesh* CreateFrontWall(const string name, wTexture* texture, int x, int z, int len)
    {
        float fromX = x;
        float toX   = x + len;

        wVert[] verts =
        [
            { wVector3f( toX,   0.0f, z - 1 ), wVECTOR3f_FORWARD, wCOLOR4s_WHITE, wVector2f( len, 1.0 ) },
            { wVector3f( fromX, 0.0f, z - 1 ), wVECTOR3f_FORWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) },
            { wVector3f( fromX, 1.0f, z - 1 ), wVECTOR3f_FORWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) },
            { wVector3f( toX,   1.0f, z - 1 ), wVECTOR3f_FORWARD, wCOLOR4s_WHITE, wVector2f( len, 0.0 ) }
        ];
        ushort[] indices = [ 0, 1, 2, 0, 2, 3 ];

        wMesh* mesh = wMeshCreate(cast(char*)name.ptr);
        wMeshBuffer* meshBuffer;
        wMaterial* material;

        meshBuffer = wMeshBufferCreate(verts.length, verts.ptr, indices.length, indices.ptr);
        material = wMeshBufferGetMaterial(meshBuffer);
        MaterialSetOldSchool(material);
        wMaterialSetTexture(material, 0, texture);
        wMeshAddMeshBuffer(mesh, meshBuffer);

        return mesh;
    }

    wMesh* CreateBackWall(const string name, wTexture* texture, int x, int z, int len)
    {
        float fromX = x;
        float toX   = x + len;

        wVert[] verts =
        [
            { wVector3f( fromX, 0.0f, z ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( len, 1.0 ) },
            { wVector3f( toX,   0.0f, z ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) },
            { wVector3f( toX,   1.0f, z ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) },
            { wVector3f( fromX, 1.0f, z ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( len, 0.0 ) }
        ];
        ushort[] indices = [ 0, 1, 2, 0, 2, 3 ];

        wMesh* mesh = wMeshCreate(cast(char*)name.ptr);
        wMeshBuffer* meshBuffer;
        wMaterial* material;

        meshBuffer = wMeshBufferCreate(verts.length, verts.ptr, indices.length, indices.ptr);
        material = wMeshBufferGetMaterial(meshBuffer);
        MaterialSetOldSchool(material);
        wMaterialSetTexture(material, 0, texture);
        wMeshAddMeshBuffer(mesh, meshBuffer);

        return mesh;
    }

    wMesh* CreateRightWall(const string name, wTexture* texture, int x, int z, int len)
    {
        float fromZ = z;
        float toZ   = z - len;

        wVert[] verts =
        [
            { wVector3f( x + 1.0f, 0.0f, fromZ ), wVECTOR3f_RIGHT, wCOLOR4s_WHITE, wVector2f( len, 1.0 ) },
            { wVector3f( x + 1.0f, 0.0f, toZ   ), wVECTOR3f_RIGHT, wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) },
            { wVector3f( x + 1.0f, 1.0f, toZ   ), wVECTOR3f_RIGHT, wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) },
            { wVector3f( x + 1.0f, 1.0f, fromZ ), wVECTOR3f_RIGHT, wCOLOR4s_WHITE, wVector2f( len, 0.0 ) }
        ];
        ushort[] indices = [ 0, 1, 2, 0, 2, 3 ];

        wMesh* mesh = wMeshCreate(cast(char*)name.ptr);
        wMeshBuffer* meshBuffer;
        wMaterial* material;

        meshBuffer = wMeshBufferCreate(verts.length, verts.ptr, indices.length, indices.ptr);
        material = wMeshBufferGetMaterial(meshBuffer);
        MaterialSetOldSchool(material);
        wMaterialSetTexture(material, 0, texture);
        wMeshAddMeshBuffer(mesh, meshBuffer);

        return mesh;
    }

    wMesh* CreateLeftWall(const string name, wTexture* texture, int x, int z, int len)
    {
        float fromZ = z;
        float toZ   = z - len;

        wVert[] verts =
        [
            { wVector3f( x, 0.0f, toZ   ), wVECTOR3f_LEFT, wCOLOR4s_WHITE, wVector2f( len, 1.0 ) },
            { wVector3f( x, 0.0f, fromZ ), wVECTOR3f_LEFT, wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) },
            { wVector3f( x, 1.0f, fromZ ), wVECTOR3f_LEFT, wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) },
            { wVector3f( x, 1.0f, toZ   ), wVECTOR3f_LEFT, wCOLOR4s_WHITE, wVector2f( len, 0.0 ) }
        ];
        ushort[] indices = [ 0, 1, 2, 0, 2, 3 ];

        wMesh* mesh = wMeshCreate(cast(char*)name.ptr);
        wMeshBuffer* meshBuffer;
        wMaterial* material;

        meshBuffer = wMeshBufferCreate(verts.length, verts.ptr, indices.length, indices.ptr);
        material = wMeshBufferGetMaterial(meshBuffer);
        MaterialSetOldSchool(material);
        wMaterialSetTexture(material, 0, texture);
        wMeshAddMeshBuffer(mesh, meshBuffer);

        return mesh;
    }

    wMesh* CreateFloor(const string name, wTexture* texture, int x, int z, int lenX, int lenZ)
    {
        float fromX = x;
        float toX   = x + lenX;
        float fromZ = z;
        float toZ   = z - lenZ;

        wVert[] verts =
        [
            { wVector3f( fromX, 0.0f, fromZ ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f(  0.0,  0.0 ) },
            { wVector3f( toX,   0.0f, fromZ ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f( lenX,  0.0 ) },
            { wVector3f( toX,   0.0f, toZ   ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f( lenX, lenZ ) },
            { wVector3f( fromX, 0.0f, toZ   ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f(  0.0, lenZ ) }
        ];
        ushort[] indices = [ 0, 1, 2, 0, 2, 3 ];

        wMesh* mesh = wMeshCreate(cast(char*)name.ptr);
        wMeshBuffer* meshBuffer;
        wMaterial* material;

        meshBuffer = wMeshBufferCreate(verts.length, verts.ptr, indices.length, indices.ptr);
        material = wMeshBufferGetMaterial(meshBuffer);
        wMaterialSetTexture(material, 0, texture);
        MaterialSetOldSchool(material);
        wMeshAddMeshBuffer(mesh, meshBuffer);

        return mesh;
    }

    wMesh* CreateCeil(const string name, wTexture* texture, int x, int z, int lenX, int lenZ)
    {
        float fromX = x;
        float toX   = x + lenX;
        float fromZ = z;
        float toZ   = z - lenZ;

        wVert[] verts =
        [
            { wVector3f( fromX, 1.0f, toZ   ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f(  0.0, lenZ ) },
            { wVector3f( toX,   1.0f, toZ   ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f( lenX, lenZ ) },
            { wVector3f( toX,   1.0f, fromZ ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f( lenX,  0.0 ) },
            { wVector3f( fromX, 1.0f, fromZ ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f( 0.0,   0.0 ) }
        ];
        ushort[] indices = [ 0, 1, 2, 0, 2, 3 ];

        wMesh* mesh = wMeshCreate(cast(char*)name.ptr);
        wMeshBuffer* meshBuffer;
        wMaterial* material;

        meshBuffer = wMeshBufferCreate(verts.length, verts.ptr, indices.length, indices.ptr);
        material = wMeshBufferGetMaterial(meshBuffer);
        wMaterialSetTexture(material, 0, texture);
        MaterialSetOldSchool(material);
        wMeshAddMeshBuffer(mesh, meshBuffer);

        return mesh;
    }

    wMesh* CreateInvisibleBlock(const string name)
    {
        // создаем невидимую текстуру, если она не создана ранее
        if (g_InvisibleTexture == null)
            g_InvisibleTexture = CreateColorTexture(wCOLOR4s_ZERO, "Invisible Color");

        /*static wMesh* mesh;*/

        // создаем невидимый блок, если он не создан ранее
        if (g_InvisibleMesh == null)
        {
            wVert[] verts =
            [
                // forward
                { wVector3f( 1.0f, 0.0f,-1.0f ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) },
                { wVector3f( 0.0f, 0.0f,-1.0f ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) },
                { wVector3f( 0.0f, 1.0f,-1.0f ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) },
                { wVector3f( 1.0f, 1.0f,-1.0f ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) },

                // backward
                { wVector3f( 0.0f, 0.0f, 0.0f ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) },
                { wVector3f( 1.0f, 0.0f, 0.0f ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) },
                { wVector3f( 1.0f, 1.0f, 0.0f ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) },
                { wVector3f( 0.0f, 1.0f, 0.0f ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) },

                // right
                { wVector3f( 1.0f, 0.0f, 0.0f ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) },
                { wVector3f( 1.0f, 0.0f,-1.0f ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) },
                { wVector3f( 1.0f, 1.0f,-1.0f ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) },
                { wVector3f( 1.0f, 1.0f, 0.0f ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) },

                // left
                { wVector3f( 0.0f, 0.0f,-1.0f ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) },
                { wVector3f( 0.0f, 0.0f, 0.0f ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) },
                { wVector3f( 0.0f, 1.0f, 0.0f ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) },
                { wVector3f( 0.0f, 1.0f,-1.0f ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) }
            ];
            ushort[] indices = [ 0,  1,  2,  0,  2,  3,
                                 4,  5,  6,  4,  6,  7,
                                 8,  9, 10,  8, 10, 11,
                                12, 13, 14, 12, 14, 15 ];

            g_InvisibleMesh = wMeshCreate(cast(char*)name.ptr);
            wMeshBuffer* meshBuffer = wMeshBufferCreate(verts.length, verts.ptr, indices.length, indices.ptr);
            wMaterial* material = wMeshBufferGetMaterial(meshBuffer);
            MaterialSetOldSchool(material);

            wMaterialSetTexture(material, 0, g_InvisibleTexture);
            wMaterialSetFlag(material, wMaterialFlags.wMF_FOG_ENABLE, false);
            wMaterialSetType(material, wMaterialTypes.wMT_TRANSPARENT_ALPHA_CHANNEL);
            wMeshAddMeshBuffer(g_InvisibleMesh, meshBuffer);
            wMeshEnableHardwareAcceleration(g_InvisibleMesh, 0);
        }

        return g_InvisibleMesh;
    }

    void CreateFloorAndCeilPlane(uint floorColor, uint ceilColor)
    {
        // набор вершин пола
        wVert[] floorVerts =
        [
            { wVector3f( 0.0f,      -0.01f, 0.0f        ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f( 0.0,       0.0        ) },
            { wVector3f( MAP_WIDTH, -0.01f, 0.0f        ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f( MAP_WIDTH, 0.0        ) },
            { wVector3f( MAP_WIDTH, -0.01f, -MAP_HEIGHT ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f( MAP_WIDTH, MAP_HEIGHT ) },
            { wVector3f( 0.0f,      -0.01f, -MAP_HEIGHT ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f( 0.0,       MAP_HEIGHT ) }
        ];
        ushort[] floorIndices = [ 0, 1, 2, 0, 2, 3 ];

        // набор вершин потолка
        wVert[] ceilVerts =
        [
            { wVector3f( 0.0f,      1.01f, -MAP_HEIGHT ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f( 0.0,       MAP_HEIGHT ) },
            { wVector3f( MAP_WIDTH, 1.01f, -MAP_HEIGHT ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f( MAP_WIDTH, MAP_HEIGHT ) },
            { wVector3f( MAP_WIDTH, 1.01f, 0.0f        ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f( MAP_WIDTH, 0.0        ) },
            { wVector3f( 0.0f,      1.01f, 0.0f        ), wVECTOR3f_UP, wCOLOR4s_WHITE, wVector2f( 0.0,       0.0        ) }
        ];
        ushort[] ceilIndices = [ 0, 1, 2, 0, 2, 3 ];

        wTexture* texture;
        wMaterial* material;
        wMesh* mesh;
        wMeshBuffer* meshBuffer;

        //
        // создаем плоскость пола
        mesh = wMeshCreate(cast(char*)"floor plane".ptr);
        texture = CreateColorTexture(wUtilUIntToColor4s(floorColor), "Floor Color");

        meshBuffer = wMeshBufferCreate(floorVerts.length, floorVerts.ptr, floorIndices.length, floorIndices.ptr);
        material = wMeshBufferGetMaterial(meshBuffer);
        wMaterialSetTexture(material, 0, texture);
        MaterialSetOldSchool(material);
        wMeshAddMeshBuffer(mesh, meshBuffer);

        floorNode = wNodeCreateFromMesh(mesh);

        //
        // создаем плоскость потолка
        mesh = wMeshCreate(cast(char*)"ceil plane".ptr);
        texture = CreateColorTexture(wUtilUIntToColor4s(ceilColor), "Ceil Color");

        meshBuffer = wMeshBufferCreate(ceilVerts.length, ceilVerts.ptr, ceilIndices.length, ceilIndices.ptr);
        material = wMeshBufferGetMaterial(meshBuffer);
        wMaterialSetTexture(material, 0, texture);
        MaterialSetOldSchool(material);
        wMeshAddMeshBuffer(mesh, meshBuffer);

        ceilNode = wNodeCreateFromMesh(mesh);
    }

    /* ТЕЛО ФУНКЦИИ */

    int i, j/*, k*/;
    TMap* map;
    wNode* node;
    wMesh* mesh;
    wMesh* batch;
    wNode* billboard;
    wNode* solidBox;
    wTexture*  texture;
    wMaterial* material;
    TMapElement element;
    int count = 0;

    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    ClearLevel();
    
    g_NumOfCurMap = numOfMap;

    map = &(mapPack.maps[numOfMap]);

    // создаем список мешей, который будет сформирован по одинаковым текстурам
    SList* meshes = new SList();

    // создаем коллизионную группу, если её нет
    if (g_WorldCollider == null) g_WorldCollider = wCollisionGroupCreate();
    // создаем ноды
    if (g_TextureNodes == null)  g_TextureNodes = new SList();
    //if (g_SpriteNodes  == null)  g_SpriteNodes  = new SList();

    // обрабатываем спрайты
    int fromPos, toPos;
    fromPos = TEXTURES_MAX_COUNT;
    toPos   = TEXTURES_MAX_COUNT + SPRITES_MAX_COUNT;
    for (i = 0; i < MAP_WIDTH; ++i)
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map.level[i][j];
            // если ничего нет...
            if (element == 0)
                continue;

            // -1 потому что нумерация в массиве с 0, а элементы не могут быть равны 0
            --element;

            if (element >= fromPos && element < toPos)
            {
                //const wBillboardAxisParam param = {false, true, false};
                int sprNum = element - fromPos;

                TSpriteInMem* sprite = &(g_Sprites[sprNum]);

                billboard = wBillBoardCreate(wVector3f(i + 0.5f, 0.5f, -j - 0.5f), wVector2f(1, 1));
                //wBillboardSetEnabledAxis(billboard, param);
                if (sprite.frames.length > 0)
                {
                    material = wNodeGetMaterial(billboard, 0);
                    wMaterialSetTexture(material, 0, sprite.frames[0]);
                    MaterialSetOldSchool(material);
                    wMaterialSetType(material, wMaterialTypes.wMT_TRANSPARENT_ALPHA_CHANNEL);
                }
                wNodeSetPosition(billboard, wVector3f(i + 0.5f, 0.5f, -j - 0.5f));

                solidBox = null;
                if (sprite.collision)
                {
                    mesh = CreateInvisibleBlock("sprite node " ~ to!string(count));
                    solidBox = wNodeCreateFromMesh(mesh);
                    wNodeSetPosition(solidBox, wVector3f(i, 0.0, -j));
                }

                TSpriteNode spriteNode = new TSpriteNode(sprite, solidBox, billboard);
                //g_SpriteNodes.AddElement(spriteNode);
                ++g_SpriteNodes.length;
                g_SpriteNodes[$ - 1] = spriteNode;

                ++count;
            }
        }

    // обрабатываем ключи
    fromPos = TEXTURES_MAX_COUNT + SPRITES_MAX_COUNT + DOORS_MAX_COUNT;
    toPos   = fromPos + KEYS_MAX_COUNT;
    for (i = 0; i < MAP_WIDTH; ++i)
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map.level[i][j];
            // если ничего нет...
            if (element == 0)
                continue;

            // -1 потому что нумерация в массиве с 0, а элементы не могут быть равны 0
            --element;

            if (element >= fromPos && element < toPos)
            {
                //const wBillboardAxisParam param = {false, true, false};
                int keyNum = element - fromPos;

                TKeyInMem* key = &(g_Keys[keyNum]);

                billboard = wBillBoardCreate(wVector3f(i + 0.5f, 0.5f, -j - 0.5f), wVector2f(1, 1));
                //wBillboardSetEnabledAxis(billboard, param);
                if (key.frames.length > 0)
                {
                    material = wNodeGetMaterial(billboard, 0);
                    wMaterialSetTexture(material, 0, key.frames[0]);
                    MaterialSetOldSchool(material);
                    wMaterialSetType(material, wMaterialTypes.wMT_TRANSPARENT_ALPHA_CHANNEL);
                }
                wNodeSetPosition(billboard, wVector3f(i + 0.5f, 0.5f, -j - 0.5f));

                TKeyNode keyNode = new TKeyNode(key, billboard);
                ++g_KeyNodes.length;
                g_KeyNodes[$ - 1] = keyNode;

                ++count;
            }
        }

    // обрабатываем двери
    fromPos = TEXTURES_MAX_COUNT + SPRITES_MAX_COUNT;
    toPos   = TEXTURES_MAX_COUNT + SPRITES_MAX_COUNT + DOORS_MAX_COUNT;
    for (i = 0; i < MAP_WIDTH; ++i)
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map.level[i][j];
            // если ничего нет...
            if (element == 0)
                continue;

            // -1 потому что нумерация в массиве с 0, а элементы не могут быть равны 0
            --element;

            if (element >= fromPos && element < toPos)
            {
                int el0, el1;
                bool isDoorCreated;
                bool isVertical, isHorizontal;

                /* горизонтальная дверь? */
                el0 = el1 = 0;
                if (i > 0)             el0 = map.level[i - 1][j];
                if (i < MAP_WIDTH - 1) el1 = map.level[i + 1][j];
                if ((el0 > 0 && el0 < TEXTURES_MAX_COUNT) &&
                    (el1 > 0 && el1 < TEXTURES_MAX_COUNT))
                {
                    isDoorCreated = true;
                    isHorizontal  = true;
                }
                else
                /* может быть она вертикальная? */
                {
                    el0 = el1 = 0;
                    if (j > 0)              el0 = map.level[i][j - 1];
                    if (j < MAP_HEIGHT - 1) el1 = map.level[i][j + 1];
                    if ((el0 > 0 && el0 < TEXTURES_MAX_COUNT) &&
                        (el1 > 0 && el1 < TEXTURES_MAX_COUNT))
                    {
                        isDoorCreated = true;
                        isVertical    = true;
                    }
                }

                // дверь не может быть создана
                if (!isDoorCreated || (!isVertical && !isHorizontal))
                    continue;

                int doorNum = element - fromPos;
                TDoorInMem* door = &(g_Doors[doorNum]);
/*
                wVector3f offset;
                mesh = CreateInvisibleBlock("door invisible mesh " ~ to!string(count));
                mesh = wMeshDublicate(mesh);
                if (isHorizontal)
                    wMeshFit(mesh, wVector3f(1.0, 0.0, 0.5),  &offset);
                else
                    wMeshFit(mesh, wVector3f(0.5, 0.0, -1.0), &offset);
                wMeshBuffer* meshBuffer = wMeshGetBuffer(mesh, 0, 0);
*/
                mesh = CreateDoorMesh("door mesh " ~ to!string(count), door.frames[0], door.rib, door.width, isHorizontal, isVertical);
                if (mesh == null)
                {
                    //wMeshDestroy(meshBuffer);
                    continue;
                }

                //wMeshAddMeshBuffer(mesh, meshBuffer);

                node = wNodeCreateFromMesh(mesh);
                if (isHorizontal)
                    wNodeSetPosition(node, wVector3f(i, 0.0, -j - 0.5));
                else
                    wNodeSetPosition(node, wVector3f(i + 0.5, 0.0, -j));

                // добавляем в конец массива ноду двери
                TDoorNode doorNode = new TDoorNode(door, node);
                //doorNode.pivotOffset = offset;
                ++g_DoorNodes.length;
                g_DoorNodes[$ - 1] = doorNode;

                ++count;
            }
        }

    ///////////////////////////////////////////////////
    // обработка горизонтальных стен
    ///////////////////////////////////////////////////
    TMapElement pEl;    // пред. элемент
    TMapElement el;     // текущий элемент
    TMapElement tEl;    // элемент для теста
    int lenX;       // длина сегмента в ширину
    int lenY;       // длина сегмента в высоту

    int pos;        // позиция с которой началось объединение
    int x, y;       // временные для прохода арстекающегося алгоритма

    for (j = 0; j < MAP_HEIGHT; ++j)
    {
        // нижняя грань (в 3Д - передняя)
        if (j < MAP_HEIGHT - 1)
        {
            for (i = 0; i < MAP_WIDTH - 1; ++i)
            {
                el  = map.level[i][j];
                tEl = map.level[i][j + 1];

                // если текущий блок стена, а снизу не стена, то наращиваем длину сегмента
                if ((el > 0 && el < TEXTURES_MAX_COUNT) &&
                    (tEl == 0 || tEl >= TEXTURES_MAX_COUNT))
                {
                    lenX = 1;
                    pos = i;

                    pEl = el;
                    ++i;
                    bool proceed = true;
                    while (proceed)
                    {
                        el  = map.level[i][j];
                        tEl = map.level[i][j + 1];

                        // если следующий элемент не равен предыдущему или перекрыт,
                        // то закончить проход и откатиться назад
                        if (el != pEl || (tEl > 0 && tEl < TEXTURES_MAX_COUNT))
                        {
                            --i;
                            proceed = false;
                            continue;
                        }

                        ++lenX;
                        if (i == MAP_WIDTH - 1)
                        {
                            proceed = false;
                            continue;
                        }

                        ++i;
                    }

                    string nodeName = "wall node " ~ to!string(count);
                    if (!meshes.AddElement(CreateFrontWall(nodeName, g_Textures[--pEl].frames[0], pos, -j, lenX)))
                        PrintWithColor("Node '" ~ nodeName ~ "' not created!", wConsoleFontColor.wCFC_RED, false);
                    ++count;
                }
            }
        }

        // верхняя грань (в 3Д - задняя)
        if (j > 0)
        {
            for (i = 0; i < MAP_WIDTH - 1; ++i)
            {
                el  = map.level[i][j];
                tEl = map.level[i][j - 1];

                // если текущий блок стена, а снизу не стена, то наращиваем длину сегмента
                if ((el > 0 && el < TEXTURES_MAX_COUNT) &&
                    (tEl == 0 || tEl >= TEXTURES_MAX_COUNT))
                {
                    lenX = 1;
                    pos = i;

                    pEl = el;
                    ++i;
                    bool proceed = true;
                    while (proceed)
                    {
                        el  = map.level[i][j];
                        tEl = map.level[i][j - 1];

                        // если следующий элемент не равен предыдущему или перекрыт,
                        // то закончить проход и откатиться назад
                        if (el != pEl || (tEl > 0 && tEl < TEXTURES_MAX_COUNT))
                        {
                            --i;
                            proceed = false;
                            continue;
                        }

                        ++lenX;
                        if (i == MAP_WIDTH - 1)
                        {
                            proceed = false;
                            continue;
                        }

                        ++i;
                    }

                    string nodeName = "wall node " ~ to!string(count);
                    if (!meshes.AddElement(CreateBackWall(nodeName, g_Textures[--pEl].frames[0], pos, -j, lenX)))
                        PrintWithColor("Node '" ~ nodeName ~ "' not created!", wConsoleFontColor.wCFC_RED, false);
                    ++count;
                }
            }
        }
    }

    ///////////////////////////////////////////////////
    // обработка вертикальных стен
    ///////////////////////////////////////////////////
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        // правая грань
        if (i < MAP_WIDTH - 1)
        {
            for (j = 0; j < MAP_HEIGHT - 1; ++j)
            {
                el  = map.level[i    ][j];
                tEl = map.level[i + 1][j];

                // если текущий блок стена, а снизу не стена, то наращиваем длину сегмента
                if ((el > 0 && el < TEXTURES_MAX_COUNT) &&
                    (tEl == 0 || tEl >= TEXTURES_MAX_COUNT))
                {
                    lenY = 1;
                    pos = j;

                    pEl = el;
                    ++j;
                    bool proceed = true;
                    while (proceed)
                    {
                        el  = map.level[i    ][j];
                        tEl = map.level[i + 1][j];

                        // если следующий элемент не равен предыдущему или перекрыт,
                        // то закончить проход и откатиться назад
                        if (el != pEl || (tEl > 0 && tEl < TEXTURES_MAX_COUNT))
                        {
                            --j;
                            proceed = false;
                            continue;
                        }

                        ++lenY;
                        if (j == MAP_HEIGHT - 1)
                        {
                            proceed = false;
                            continue;
                        }

                        ++j;
                    }

                    string nodeName = "wall node " ~ to!string(count);
                    if (!meshes.AddElement(CreateRightWall(nodeName, g_TexturesShadowed[--pEl].frames[0], i, -pos, lenY)))
                        PrintWithColor("Node '" ~ nodeName ~ "' not created!", wConsoleFontColor.wCFC_RED, false);
                    ++count;
                }
            }
        }

        // левая грань
        if (i > 0)
        {
            for (j = 0; j < MAP_HEIGHT - 1; ++j)
            {
                el  = map.level[i    ][j];
                tEl = map.level[i - 1][j];

                // если текущий блок стена, а снизу не стена, то наращиваем длину сегмента
                if ((el > 0 && el < TEXTURES_MAX_COUNT) &&
                    (tEl == 0 || tEl >= TEXTURES_MAX_COUNT))
                {
                    lenY = 1;
                    pos = j;

                    pEl = el;
                    ++j;
                    bool proceed = true;
                    while (proceed)
                    {
                        el  = map.level[i    ][j];
                        tEl = map.level[i - 1][j];

                        // если следующий элемент не равен предыдущему или перекрыт,
                        // то закончить проход и откатиться назад
                        if (el != pEl || (tEl > 0 && tEl < TEXTURES_MAX_COUNT))
                        {
                            --j;
                            proceed = false;
                            continue;
                        }

                        ++lenY;
                        if (j == MAP_HEIGHT - 1)
                        {
                            proceed = false;
                            continue;
                        }

                        ++j;
                    }

                    string nodeName = "wall node " ~ to!string(count);
                    if (!meshes.AddElement(CreateLeftWall(nodeName, g_TexturesShadowed[--pEl].frames[0], i, -pos, lenY)))
                        PrintWithColor("Node '" ~ nodeName ~ "' not created!", wConsoleFontColor.wCFC_RED, false);
                    ++count;
                }
            }
        }
    }



    ///////////////////////////////////////////////////
    // обработка пола и потолка
    ///////////////////////////////////////////////////

    TMapElement[MAP_WIDTH][MAP_HEIGHT] mergeInfo;   // информация о том, можно ли объединять текущую ячейку и что там
                                                    // если 0, значит нельзя
    TMapElement lEl;    // элемент на уровне

    // если не показывать небо, то создать одноцветные плоскости потолка и пола
    if (!map.showSky)
        CreateFloorAndCeilPlane(map.floorColor, map.ceilColor);
    else
    {
        string fileName;
        string skyboxName;

        /// FRONT
        fileName = "skybox_front_" ~ to!string(map.skyNumber) ~ ".dat";
        skyboxName = "Skybox_front_" ~ to!string(map.skyNumber);
        skyTextureFront = LoadTexture(fileName, skyboxName);

        /// BACK
        fileName = "skybox_back_" ~ to!string(map.skyNumber) ~ ".dat";
        skyboxName = "Skybox_back_" ~ to!string(map.skyNumber);
        skyTextureBack = LoadTexture(fileName, skyboxName);

        /// LEFT
        fileName = "skybox_left_" ~ to!string(map.skyNumber) ~ ".dat";
        skyboxName = "Skybox_left_" ~ to!string(map.skyNumber);
        skyTextureLeft = LoadTexture(fileName, skyboxName);

        /// RIGHT
        fileName = "skybox_right_" ~ to!string(map.skyNumber) ~ ".dat";
        skyboxName = "Skybox_right_" ~ to!string(map.skyNumber);
        skyTextureRight = LoadTexture(fileName, skyboxName);

        /// TOP
        fileName = "skybox_top_" ~ to!string(map.skyNumber) ~ ".dat";
        skyboxName = "Skybox_top_" ~ to!string(map.skyNumber);
        skyTextureTop = LoadTexture(fileName, skyboxName);

        /// BOTTOM
        fileName = "skybox_bottom_" ~ to!string(map.skyNumber) ~ ".dat";
        skyboxName = "Skybox_bottom_" ~ to!string(map.skyNumber);
        skyTextureBottom = LoadTexture(fileName, skyboxName);

        /// CREATE SKY CUBE
        skyNode = wSkyBoxCreate(skyTextureTop,   skyTextureBottom,
                                skyTextureLeft,  skyTextureRight,
                                skyTextureFront, skyTextureBack);
    }

    ///////////////////////////////////////////////////
    // ПОЛ
    ///////////////////////////////////////////////////
    // если не показывать текстурный пол
    if (!map.showFloor) goto no_floor;

    // обновляем информацию по объединению
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            el  = map.floor[i][j];
            lEl = map.level[i][j];
            // можно объединять, если есть элемент пола и на этой позиции нет стены
            if ((el > 0) && (lEl == 0 || lEl >= TEXTURES_MAX_COUNT))
                mergeInfo[i][j] = el;
            else
                mergeInfo[i][j] = 0;
        }
    }

    // теперь начнем проход для объединения ячеек по типу
    for (j = 0; j < MAP_HEIGHT; ++j)
    {
        for (i = 0; i < MAP_WIDTH; ++i)
        {
            el = mergeInfo[i][j];
            // если элемент можно объединять
            if (el != 0)
            {
                pos  = i;               // запомнить его позицию
                lenX = lenY = 1;        // длина и высота как минимум в один квадрат
                pEl  = el;              // запомним какой элемент по типу
                mergeInfo[i][j] = 0;    // помечаем, что текущий элемент будет объединен

                // проверяем следующий за текущим элемент справа
                if (pos < MAP_WIDTH - 1)
                {
                    tEl = mergeInfo[pos + 1][j];
                    // если он такой же
                    if (tEl == pEl)
                    {
                        // то бежим по строке, пока не закончится уровень или не наткнемся на что-то другое
                        bool proceed = true;
                        for (x = pos + 1; (x < MAP_WIDTH) && proceed; ++x)
                        {
                            el = mergeInfo[x][j];
                            if (el != pEl)          // что-то другое
                            {
                                proceed = false;    // выйти
                                continue;
                            }

                            // следующий элемент такой же
                            mergeInfo[x][j] = 0;    // помечаем
                            ++lenX;                 // наращиваем ширину
                        }
                    }
                }

                // проверяем следующий за текущим элемент снизу
                if (j < MAP_HEIGHT - 1)
                {
                    tEl = mergeInfo[pos][j + 1];
                    // если он такой же
                    if (tEl == pEl)
                    {
                        // здесь мы пробежимся по высоте
                        bool proceed = true;
                        for (y = j + 1; (y < MAP_HEIGHT) && proceed; ++y)
                        {
                            for (x = pos; (x < pos + lenX) && proceed; ++x)
                            {
                                el = mergeInfo[x][y];
                                if (el != pEl)          // что-то другое
                                {
                                    proceed = false;    // выйти
                                    continue;
                                }
                            }

                            if (!proceed)
                                continue;

                            // если дошел сюда, значит вся линия совпадает с верхней
                            // наращиваем высоту и помечаем инфу
                            for (x = pos; x < pos + lenX; ++x)
                                mergeInfo[x][y] = 0;

                            ++lenY;
                        }
                    }
                }

                string nodeName = "floor node " ~ to!string(count);
                if (!meshes.AddElement(CreateFloor(nodeName, g_Textures[--pEl].frames[0], pos, -j, lenX, lenY)))
                    PrintWithColor("Node '" ~ nodeName ~ "' not created!", wConsoleFontColor.wCFC_RED, false);

                // следующие lenX элементов всё равно 0, так что перепрыгиваем
                i = lenX - 1;
                ++count;
            }
        }
    }
no_floor:


    ///////////////////////////////////////////////////
    // ПОТОЛОК
    ///////////////////////////////////////////////////
    // если не рисовать текстурный пол
    if (!map.showCeil) goto no_ceil;

    // обновляем информацию по объединению
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            el  = map.ceil [i][j];
            lEl = map.level[i][j];
            // можно объединять, если есть элемент пола и на этой позиции нет стены
            if ((el > 0) && (lEl == 0 || lEl >= TEXTURES_MAX_COUNT))
                mergeInfo[i][j] = el;
            else
                mergeInfo[i][j] = 0;
        }
    }

    // теперь начнем проход для объединения ячеек по типу
    for (j = 0; j < MAP_HEIGHT; ++j)
    {
        for (i = 0; i < MAP_WIDTH; ++i)
        {
            el = mergeInfo[i][j];
            // если элемент можно объединять
            if (el != 0)
            {
                pos  = i;               // запомнить его позицию
                lenX = lenY = 1;        // длина и высота как минимум в один квадрат
                pEl  = el;              // запомним какой элемент по типу
                mergeInfo[i][j] = 0;    // помечаем, что текущий элемент будет объединен

                // проверяем следующий за текущим элемент справа
                if (pos < MAP_WIDTH - 1)
                {
                    tEl = mergeInfo[pos + 1][j];
                    // если он такой же
                    if (tEl == pEl)
                    {
                        // то бежим по строке, пока не закончится уровень или не наткнемся на что-то другое
                        bool proceed = true;
                        for (x = pos + 1; (x < MAP_WIDTH) && proceed; ++x)
                        {
                            el = mergeInfo[x][j];
                            if (el != pEl)          // что-то другое
                            {
                                proceed = false;    // выйти
                                continue;
                            }

                            // следующий элемент такой же
                            mergeInfo[x][j] = 0;    // помечаем
                            ++lenX;                 // наращиваем ширину
                        }
                    }
                }

                // проверяем следующий за текущим элемент снизу
                if (j < MAP_HEIGHT - 1)
                {
                    tEl = mergeInfo[pos][j + 1];
                    // если он такой же
                    if (tEl == pEl)
                    {
                        // здесь мы пробежимся по высоте
                        bool proceed = true;
                        for (y = j + 1; (y < MAP_HEIGHT) && proceed; ++y)
                        {
                            for (x = pos; (x < pos + lenX) && proceed; ++x)
                            {
                                el = mergeInfo[x][y];
                                if (el != pEl)          // что-то другое
                                {
                                    proceed = false;    // выйти
                                    continue;
                                }
                            }

                            if (!proceed)
                                continue;

                            // если дошел сюда, значит вся линия совпадает с верхней
                            // наращиваем высоту и помечаем инфу
                            for (x = pos; x < pos + lenX; ++x)
                                mergeInfo[x][y] = 0;

                            ++lenY;
                        }
                    }
                }

                string nodeName = "ceil node " ~ to!string(count);
                if (!meshes.AddElement(CreateCeil(nodeName, g_Textures[--pEl].frames[0], pos, -j, lenX, lenY)))
                    PrintWithColor("Node '" ~ nodeName ~ "' not created!", wConsoleFontColor.wCFC_RED, false);

                // следующие lenX элементов всё равно 0, так что перепрыгиваем
                i = lenX - 1;
                ++count;
            }
        }
    }
no_ceil:

    ///////////////////////////////////////////////////
    // БАТЧИНГ
    ///////////////////////////////////////////////////


    // теперь у нас есть список мешей. Можно пройтись по нему и создать несколько батчинг мешей
    wTexture* tTexture;  // тестируемая текстура
    // проходимся по каждой текстуре
    writeln("meshes length = " ~ to!string(meshes.size));

    if (meshes.size > 0)
    {
        for (i = 0; i < TEXTURES_MAX_COUNT; ++i)
        {
            if (g_Textures[i].frames.length > 0)
            {
                tTexture = g_Textures[i].frames[0];
                batch = wMeshCreateBatching();
                // ищем меши с такой же текстурой
                for (SListElement* listElement = meshes.first; (listElement != null); listElement = listElement.next)
                {
                    mesh = cast(wMesh*)listElement.value;
                    if (mesh == null)
                    {
                        writeln("null mesh in meshes list!");
                        continue;
                    }

                    material = wMeshBufferGetMaterial(wMeshGetBuffer(mesh, 0, 0));
                    texture = wMaterialGetTexture(material, 0);
                    if (texture == tTexture)
                    {
                        wMeshAddToBatching(batch, mesh, wVECTOR3f_ZERO, wVECTOR3f_ZERO, wVECTOR3f_ONE);
                        //writeln("add to batch");
                    }
                }

                wMeshFinalizeBatching(batch);
                node = wNodeCreateFromBatchingMesh(batch);

                TTextureNode* textureNode = new TTextureNode(&(g_Textures[i]), node);
                g_TextureNodes.AddElement(textureNode);

                // добавляем коллизии в коллизионную группу
                wSelector* col = wCollisionCreateFromBatchingMesh(batch, node);
                if (col != null)
                    wCollisionGroupAddCollision(g_WorldCollider, col);

                //wNodeSetDebugMode(node, wDebugMode.wDM_MESH_WIRE_OVERLAY);
                //wNodeSetDebugDataVisible(node, true);
            }

            // теперь то же самое для теневых текстур
            if (g_TexturesShadowed[i].frames.length > 0)
            {
                tTexture = g_TexturesShadowed[i].frames[0];
                batch = wMeshCreateBatching();
                // ищем меши с такой же текстурой
                for (SListElement* listElement = meshes.first; (listElement != null); listElement = listElement.next)
                {
                    mesh = cast(wMesh*)listElement.value;
                    if (mesh == null)
                    {
                        //writeln("null mesh in meshes list!");
                        continue;
                    }

                    material = wMeshBufferGetMaterial(wMeshGetBuffer(mesh, 0, 0));
                    texture = wMaterialGetTexture(material, 0);
                    if (texture == tTexture)
                        wMeshAddToBatching(batch, mesh, wVECTOR3f_ZERO, wVECTOR3f_ZERO, wVECTOR3f_ONE);
                }

                wMeshFinalizeBatching(batch);
                node = wNodeCreateFromBatchingMesh(batch);

                TTextureNode* textureNode = new TTextureNode(&(g_TexturesShadowed[i]), node);
                g_TextureNodes.AddElement(textureNode);

                // добавляем коллизии в коллизионную группу
                wSelector* col = wCollisionCreateFromBatchingMesh(batch, node);
                if (col != null)
                    wCollisionGroupAddCollision(g_WorldCollider, col);

                //wNodeSetDebugMode(node, wDebugMode.wDM_MESH_WIRE_OVERLAY);
                //wNodeSetDebugDataVisible(node, true);
            }
        }
    }

    /* добавляем коллизии в коллизионную группу */
    foreach (sprite; g_SpriteNodes)
    {
        if (sprite is null)
            continue;

        if (sprite.physBody != null)
            wCollisionGroupAddCollision(g_WorldCollider, sprite.physBody);

        //wNodeSetDebugMode(sprite.node, wDebugMode.wDM_MESH_WIRE_OVERLAY);
        //wNodeSetDebugDataVisible(sprite.node, true);
    }

    /* добавляем коллизии в коллизионную группу */
    foreach (door; g_DoorNodes)
    {
        if (door is null)
            continue;

        if (door.physBody != null)
            wCollisionGroupAddCollision(g_WorldCollider, door.physBody);

        //wNodeSetDebugMode(door.node, wDebugMode.wDM_MESH_WIRE_OVERLAY);
        //wNodeSetDebugDataVisible(door.node, true);
    }
    
    meshes.destroy();

    wSceneDestroyAllUnusedMeshes();


    // Сюда???
    if (map.showFog)
    {
        float dest = sqrt(sqr(MAP_WIDTH) + sqr(MAP_HEIGHT));
        wSceneSetFog(wUtilUIntToColor4s(map.fogColor), wFogType.wFT_EXP2, 0, dest - map.fogIntensity * dest, map.fogIntensity, true, false);
    }
}



/* INTERPRETER */
static duk_ret_t n_mapIsPlaceFree(duk_context* ctx)
{
    /* карта не зашружена */
    if (g_NumOfCurMap == -1)
    {
        duk_push_boolean(ctx, 0);
        return 1;   
    }

    int x = duk_to_int(ctx, 0);
    int y = duk_to_int(ctx, 1);

    /* если координаты запрошены неверные */
    if ((x < 0 || x >= MAP_WIDTH ) || 
        (y < 0 || y >= MAP_HEIGHT))

    {
        duk_push_boolean(ctx, 0);
        return 1;   
    }

    /* если место занято стеной */
    TMapElement el = mapPack.maps[g_NumOfCurMap].level[x][y];
    if (el > 0 && el < TEXTURES_MAX_COUNT)

    {
        duk_push_boolean(ctx, 0);
        return 1;   
    }

    /* свободно */
    duk_push_boolean(ctx, 1);
    return 1;
}



void RegisterMapCFunctions()
{
    Duk_RegisterCFunction(&n_mapIsPlaceFree,  2, "mapIsPlaceFree" );
}
