module doors_keys;

import std.stdio;
import std.conv;

import list;
import constants;
import global;
import res_manager;
import util;
import map;
import duk_util;
import base_object;

import WorldSim3D;
import SampleFunctions;


extern (C):

/** дверь-эталон, загруженная из пака */
struct TDoorInMem
{
    string name;           // имя двери
    bool   destroyable;    // разрушаемый?
    ubyte  endurance;      // живучесть
    double openSpeed;      // скорость открывания
    bool   stayOpened;     // открывается один раз?
    bool   needKey;        // нужен ключ для открывания?
    string needKeyMsg;     // сообщение о необходимости ключа
    //short  keyId;          // ключ для открывания
    string keyName;        // имя ключа для открывания
    double width;          // ширина двери
    wTexture* rib;         // текстура ребра

    ubyte  animSpeed;      // кол-во кадров в секунду
    wTexture*[] frames;

    TScript* initScript;     // скрипты по умолчанию
    TScript* updateScript;
}

/** нода двери, создаваемая на карте */
class TDoorNode : TObject
{
    TDoorInMem* index;      // индекс двери (позиция в массиве g_Doors)
    int     curFrame;       // текущий кадр
    float   animSpeed;      // скорость анимации
    float   _animSpeed;     // значение прироста анимации до смены кадра
    //wNode*  node;           // дверь + ограничивающая коробка
    //wSelector* physBody;    // физическое тело ограничивающей коробки

    // создание ноды
    this(TDoorInMem* index, wNode* node)
    {
        wMaterial* material;

        this.index     = index;
        this.animSpeed = index.animSpeed;
        this.node      = node;

        addVar("name",        index.name);          // имя двери
        addVar("destroyable", index.destroyable);   // можно ли разрушить
        addVar("health",      index.endurance);     // текущее значение жизней
        addVar("openSpeed",   index.openSpeed);     // скорость открывания
        addVar("stayOpened",  index.stayOpened);    // открывается один раз?
        addVar("needKey",     index.needKey);       // нужен ключ для открывания?
        addVar("message",     index.needKeyMsg);    // сообщение о необходимости ключа
        addVar("isDestroyed", false);               // разрушено?
        addVar("keyName",     index.keyName);       // какой ключ нужен для открывания

        if (node == null)
            return;

        wMesh* mesh = wNodeGetMesh(node);
        if (mesh == null)
            return;
/*
        if (wMeshGetBuffersCount(mesh, 0) > 1)
        {
            wMesh* mesh = wNodeGetMesh(this.node);
            this.physBody = wCollisionCreateFromMesh(mesh, this.node, 0);
        }
*/
        this.physBody = wCollisionCreateFromMesh(mesh, this.node, 0);
    }

    ~this()
    {
        //writeln("I am " ~ this.index.name);
    }

    void Init()
    {
        TScript* script = index.initScript;
        if (script != null)
        {
            Duk_RunScript(*script);
        }
    }

    void Update()
    {
        if (!this.isVisible)
            return;

        if (this.node == null)
            return;

        // обновить анимацию ноды спрайта
        if (this.animSpeed != 0.0)
        {
            if (this._animSpeed < 1.0f)
            {
                this._animSpeed += g_DeltaTime * this.animSpeed;
            }
            else
            {
                this._animSpeed = 0.0f;

                // переходим на следующий кадр
                ++(this.curFrame);
                if (this.curFrame >= this.index.frames.length)
                    this.curFrame = 0;

                // если там пустая текстура, то ищем на следующем кадре
                wTexture* txr = this.index.frames[this.curFrame];
                while (txr == null)
                {
                    ++(this.curFrame);
                    if (this.curFrame >= this.index.frames.length)
                        this.curFrame = 0;

                    txr = this.index.frames[this.curFrame];
                }

                wMaterial* mat = wNodeGetMaterial(this.node, 0);
                wMaterialSetTexture(mat, 0, txr);
            }
        }

        TScript* script = this.index.updateScript;
        if (script != null)
        {
            g_MutexNodes.lock_nothrow();
            g_ObjectId = this.id;
            Duk_RunScript(*script);
            g_MutexNodes.unlock_nothrow();
        }
    }
}

/** ключ-эталон, загруженный из пака */
struct TKeyInMem
{
    string name;          // имя ключа
    ubyte  animSpeed;     // кол-во кадров в секунду
    wTexture*[] frames;

    TScript* initScript;     // скрипты по умолчанию
    TScript* updateScript;
}

/** нода ключа, создаваемая на карте */
class TKeyNode : TObject
{
    TKeyInMem* index;       // индекс ключа (позиция в массиве g_Keys)
    wNode*  billboard;      // нода биллборда
    int     curFrame;       // текущий кадр
    float   animSpeed;      // скорость анимации
    float   _animSpeed;     // значение прироста анимации до смены кадра

    // создание ноды
    this (TKeyInMem* index, wNode* billboard)
    {
        wBillboardAxisParam axisParams;
        wMaterial* material;

        addVar("name", index.name);               // имя ключа

        this.index     = index;
        this.animSpeed = index.animSpeed;
        this.billboard = billboard;
        this.node      = wNodeCreateEmpty();

        wVector3f pos = wNodeGetPosition(billboard);
        wNodeSetPosition(this.node, pos);

/*
        double scaleX = index.scaleX / 100.0;
        double scaleY = index.scaleY / 100.0;

        wBillBoardSetSize(this.billboard, wVector2f(scaleX, scaleY));
        double dX;
        double dY = (1.0 - scaleY) / 2.0;
        //double dY = (index.scaleY / 100.0) / 2.0;
        wNodeMove(this.billboard, wVector3f(0.0, -dY, 0.0));
*/

        axisParams.isEnablePitch = false;
        axisParams.isEnableYaw   = true;
        axisParams.isEnableRoll  = false;
        wBillboardSetEnabledAxis(this.billboard, axisParams);
    }

    ~this()
    {
        //writeln("I am " ~ this.index.name);
        if (this.billboard != null)
        {
            wNodeDestroy(this.billboard);
        }
    }

    override void setVisibility(bool isVisible)
    {
        if (this.billboard)
            wNodeSetVisibility(this.billboard, isVisible);

        super.setVisibility(isVisible);
    }

    void Init()
    {
        TScript* script = index.initScript;
        if (script != null)
        {
            Duk_RunScript(*script);
        }
    }

    void Update()
    {
        if (!this.isVisible)
            return;

        if (this.billboard == null)
            return;

        // обновить анимацию ноды спрайта
        if (this.animSpeed != 0.0)
        {
            if (this._animSpeed < 1.0f)
            {
                this._animSpeed += g_DeltaTime * this.animSpeed;
            }
            else
            {
                this._animSpeed = 0.0f;

                // переходим на следующий кадр
                ++(this.curFrame);
                if (this.curFrame >= this.index.frames.length)
                    this.curFrame = 0;

                // если там пустая текстура, то ищем на следующем кадре
                wTexture* txr = this.index.frames[this.curFrame];
                while (txr == null)
                {
                    ++(this.curFrame);
                    if (this.curFrame >= this.index.frames.length)
                        this.curFrame = 0;

                    txr = this.index.frames[this.curFrame];
                }

                wMaterial* mat = wNodeGetMaterial(this.billboard, 0);
                wMaterialSetTexture(mat, 0, txr);
            }
        }

        TScript* script = this.index.updateScript;
        if (script != null)
        {
            g_MutexNodes.lock_nothrow();
            g_ObjectId = this.id;
            Duk_RunScript(*script);
            g_MutexNodes.unlock_nothrow();
        }
    }
}

__gshared TDoorInMem[] g_Doors;
__gshared TDoorNode[]  g_DoorNodes;
__gshared TKeyInMem[]  g_Keys;
__gshared TKeyNode[]   g_KeyNodes;

void InitDoors();
void InitKeys();
bool LoadDoorPack();
void DoorNodesUpdate();
void KeyNodesUpdate();



// IMPLEMENTATION

void InitDoors()
{
    TDoorInMem*   door;
    wTexture*     txr;
    int i, j;

    // если текстурные хранилища не были созданы, то просто их создать
    if (g_Doors == null || g_Doors.length == 0)
    {
        g_Doors = new TDoorInMem[DOORS_MAX_COUNT];
        return;
    }

    // удаляем все текстуры, которые хранятся в хранилищах
    for (i = 0; i < DOORS_MAX_COUNT; ++i)
    {
        door = &(g_Doors[i]);
        for (j = 0; j < door.frames.length; ++j)
        {
            txr = door.frames[j];
            if (txr != null)
            {
                wTextureDestroy(txr);
                door.frames[j] = null;
            }
        }
        if (door.rib != null)
        {
            wTextureDestroy(door.rib);
            door.rib = null;
        }
        door.name.length = 0;
        door.destroyable = false;
        door.endurance   = 0;
        door.openSpeed   = 180;
        door.stayOpened  = false;
        door.needKey     = false;
        door.needKeyMsg  = "I need a key.";
        //door.keyId       = 0;
        door.keyName.length = 0;
        door.width       = 1.0;
        door.animSpeed   = 0;
        door.frames.length = 0;

        door.initScript.destroy();
        door.updateScript.destroy();
        door.initScript = null;
        door.updateScript = null;        
    }
}

void InitKeys()
{
    TKeyInMem*  key;
    wTexture*   txr;
    int i, j;

    // если текстурные хранилища не были созданы, то просто их создать
    if (g_Keys == null || g_Keys.length == 0)
    {
        g_Keys = new TKeyInMem[KEYS_MAX_COUNT];
        return;
    }

    // удаляем все текстуры, которые хранятся в хранилищах
    for (i = 0; i < KEYS_MAX_COUNT; ++i)
    {
        key = &(g_Keys[i]);
        for (j = 0; j < key.frames.length; ++j)
        {
            txr = key.frames[j];
            if (txr != null)
            {
                wTextureDestroy(txr);
                key.frames[j] = null;
            }
        }

        key.name.length = 0;

        key.initScript.destroy();
        key.updateScript.destroy();
        key.initScript = null;
        key.updateScript = null;
    }
}

bool LoadDoorPack()
{
    bool    isLoaded;
    TDoorInMem* door;

    ushort  w, h;
    byte[]  buffer = new byte[0];
    int     bufferSize;
    string  fileName;

    string  doorName;
    int     i, j;

    string  name;
    bool    destroyable;
    ubyte   endurance;
    int     openSpeed;
    bool    stayOpened;
    bool    needKey;
    string  needKeyMsg;
    short   keyId;
    double  width;
    ubyte   animSpeed;
    uint    framesCount;
    int     textureSize;

    // читаем количество текстур
    bufferSize = Res_GetUncompressedFileSize("doors.cfg\0");
    if (bufferSize <= 0)
    {
        PrintWithColor("can not get size of doors.cfg\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    buffer.length = bufferSize;
    if (!Res_ReadFileFromZipToBuffer("doors.cfg", &(buffer[0]), bufferSize))
    {
        PrintWithColor("can not read doors.cfg\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    if (!Res_OpenIniFromBuffer(&(buffer[0]), bufferSize))
    {
        PrintWithColor("can not open doors.cfg!!!\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    g_DoorsCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_DoorsCount == 0)
        goto end;

    InitDoors();

    for (i = 0; i < g_DoorsCount; ++i)
    {
        // если не нужна для загрузки
        if (!g_DoorsNeedForLoad[i])
            continue;

        // читаем инфу по текстуре
        fileName = "door_" ~ to!string(i) ~ ".cfg\0";
        bufferSize = Res_GetUncompressedFileSize(fileName.ptr);
        if (bufferSize <= 0)
        {
            PrintWithColor("can not get size of door_" ~ to!string(i) ~ ".cfg", wConsoleFontColor.wCFC_RED, false);
            continue;
        }

        door = &(g_Doors[i]);

        buffer.length = bufferSize;
        Res_ReadFileFromZipToBuffer(fileName.ptr, &(buffer[0]), bufferSize);
        Res_OpenIniFromBuffer(&(buffer[0]), bufferSize);

        name        = to!string(Res_IniReadString("Options", "name", ("Door #" ~ to!string(i) ~ "\0").ptr));
        destroyable = Res_IniReadBool  ("Options", "destroyable",  false);
        endurance   = cast(ubyte)Res_IniReadInteger("Options", "endurance", 0);

        openSpeed   = Res_IniReadInteger("Options", "open_speed", 180);
        stayOpened  = Res_IniReadBool  ("Options", "stay_opened",  false);
        needKey     = Res_IniReadBool  ("Options", "need_key",     false);
        needKeyMsg  = to!string(Res_IniReadString("Options", "message", "I need a key.\0".ptr));
        keyId       = cast(short)Res_IniReadInteger("Options", "key_id",       0);
        --keyId;    // т.к. нумерация с 0
        width       = (Res_IniReadDouble("Options", "width", 1.0) / 100.0);
        animSpeed   = cast(ubyte)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = cast(ubyte)Res_IniReadInteger("Options", "frames_count",    0);
        Res_CloseIni();

        door.name        = name;
        door.destroyable = destroyable;
        door.endurance   = endurance;

        door.openSpeed   = cast(double)openSpeed / 255.0;
        door.stayOpened  = stayOpened;
        door.needKey     = needKey;
        door.needKeyMsg  = needKeyMsg;
        //door.keyId       = keyId;
        door.width       = width;
        door.animSpeed   = animSpeed;

        // подтягиваем имя необходимого ключа
        if (keyId >= 0 && keyId < g_KeysCount && keyId < g_Keys.length)
            door.keyName = g_Keys[keyId].name.dup;

        if (framesCount == 0)
            continue;

        fileName = "door_" ~ to!string(i) ~ "_rib.dat\0";
        doorName = "Door_" ~ to!string(i) ~ "_Rib";
        door.rib = LoadTexture(fileName, doorName);

        // читаем кадры, если есть
        door.frames = new wTexture*[framesCount];
        for (j = 0; j < framesCount; ++j)
        {
            fileName   = "door_" ~ to!string(i) ~ "_frame_" ~ to!string(j) ~ ".dat\0";
            doorName = "Door_" ~ to!string(i) ~ "_Frame_" ~ to!string(j);
            door.frames[j] = LoadTexture(fileName, doorName);
        }
    }
    isLoaded = true;

end:
    buffer.length = 0;

    return isLoaded;
}


bool LoadKeyPack()
{
    bool    isLoaded;
    TKeyInMem* key;

    ushort  w, h;
    byte[]  buffer = new byte[0];
    int     bufferSize;
    string  fileName;

    string  keyName;
    int     i, j;

    string  name;
    ubyte   animSpeed;
    uint    framesCount;
    int     textureSize;

    // читаем количество текстур
    bufferSize = Res_GetUncompressedFileSize("keys.cfg\0");
    if (bufferSize <= 0)
    {
        PrintWithColor("can not get size of keys.cfg\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    buffer.length = bufferSize;
    if (!Res_ReadFileFromZipToBuffer("keys.cfg", &(buffer[0]), bufferSize))
    {
        PrintWithColor("can not read keys.cfg\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    if (!Res_OpenIniFromBuffer(&(buffer[0]), bufferSize))
    {
        PrintWithColor("can not open keys.cfg!!!\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    g_KeysCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_KeysCount == 0)
        goto end;

    InitKeys();

    for (i = 0; i < g_KeysCount; ++i)
    {
        // если не нужна для загрузки
        if (!g_KeysNeedForLoad[i])
            continue;

        // читаем инфу по текстуре
        fileName = "key_" ~ to!string(i) ~ ".cfg\0";
        bufferSize = Res_GetUncompressedFileSize(fileName.ptr);
        if (bufferSize <= 0)
        {
            PrintWithColor("can not get size of key_" ~ to!string(i) ~ ".cfg", wConsoleFontColor.wCFC_RED, false);
            continue;
        }

        key = &(g_Keys[i]);

        buffer.length = bufferSize;
        Res_ReadFileFromZipToBuffer(fileName.ptr, &(buffer[0]), bufferSize);
        Res_OpenIniFromBuffer(&(buffer[0]), bufferSize);

        name        = to!string(Res_IniReadString("Options", "name", ("Key #" ~ to!string(i) ~ "\0").ptr));
        animSpeed   = cast(ubyte)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = cast(ubyte)Res_IniReadInteger("Options", "frames_count",    0);
        Res_CloseIni();

        key.name      = name;
        key.animSpeed = animSpeed;
        if (framesCount == 0)
            continue;

        // читаем кадры, если есть
        key.frames = new wTexture*[framesCount];
        for (j = 0; j < framesCount; ++j)
        {
            fileName   = "key_" ~ to!string(i) ~ "_frame_" ~ to!string(j) ~ ".dat\0";
            keyName = "Key_" ~ to!string(i) ~ "_Frame_" ~ to!string(j);
            key.frames[j] = LoadTexture(fileName, keyName);
        }
    }
    isLoaded = true;

end:
    buffer.length = 0;

    return isLoaded;
}

void DoorNodesUpdate()
{
    if (g_DoorNodes.length == 0)
        return;

    TDoorNode* door;
    for (int i = 0; i < g_DoorNodes.length; ++i)
    {
        door = &(g_DoorNodes[i]);

        if (door != null && *door !is null)
        {
            //g_MutexNodes.lock_nothrow();
            //g_ObjectId = door.id;
            door.Update();
            //g_MutexNodes.unlock_nothrow();
        }
    }
}

void KeyNodesUpdate()
{
    if (g_KeyNodes.length == 0)
        return;

    TKeyNode* key;
    for (int i = 0; i < g_KeyNodes.length; ++i)
    {
        key = &(g_KeyNodes[i]);

        if (key != null && *key !is null)
        {
            //g_MutexNodes.lock_nothrow();
            //g_ObjectId = key.id;
            key.Update();
            //g_MutexNodes.unlock_nothrow();
        }
    }
}


/* СОЗДАНИЕ МЕША ДВЕРИ */
wMesh* CreateDoorMesh(const string name, wTexture* texture, wTexture* ribTexture, double width, bool isHorizontal, bool isVertical)
{
    if (!isHorizontal && !isVertical)
        return null;

    wVert[]  verts;
    ushort[] indices = [ 0, 1, 2, 0, 2, 3,
                         4, 5, 6, 4, 6, 7 ];

    wVert[]  ribVerts;
    ushort[] ribIndices = [  0,  1,  2,  0,  2,  3,
                             4,  5,  6,  4,  6,  7,
                             8,  9, 10,  8, 10, 11,
                            12, 13, 14, 12, 14, 15 ];

    double hW = width / 2.0;
    if (isHorizontal)
    {
        // ребра
        ribVerts = [
        // right
                    wVert( wVector3f( 1.0, 0.0, hW ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( 1.0, 0.0,-hW ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( 1.0, 1.0,-hW ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( 1.0, 1.0, hW ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
        // left
                    wVert( wVector3f( 0.0, 0.0,-hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( 0.0, 0.0, hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( 0.0, 1.0, hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( 0.0, 1.0,-hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
        // top
                    wVert( wVector3f( 0.0, 1.0,-hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( 0.0, 1.0, hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
                    wVert( wVector3f( 1.0, 1.0, hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( 1.0, 1.0,-hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
        // bottom
                    wVert( wVector3f( 0.0, 0.0,-hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( 1.0, 0.0,-hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( 1.0, 0.0, hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
                    wVert( wVector3f( 0.0, 0.0, hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) )
        ];

        // сама дверь
        verts = [
        // forward
                    wVert( wVector3f( 1.0, 0.0,-hW ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( 0.0, 0.0,-hW ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( 0.0, 1.0,-hW ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
                    wVert( wVector3f( 1.0, 1.0,-hW ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
        // backward
                    wVert( wVector3f( 0.0, 0.0, hW ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( 1.0, 0.0, hW ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( 1.0, 1.0, hW ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( 0.0, 1.0, hW ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) )
        ];
    }
    else
    // может быть она вертикальная?
    {
        // ребра
        ribVerts = [
        // forward
                    wVert( wVector3f( hW, 0.0,-1.0 ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f(-hW, 0.0,-1.0 ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f(-hW, 1.0,-1.0 ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( hW, 1.0,-1.0 ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
        // backward
                    wVert( wVector3f(-hW, 0.0, 0.0 ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( hW, 0.0, 0.0 ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( hW, 1.0, 0.0 ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f(-hW, 1.0, 0.0 ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
        // top
                    wVert( wVector3f(-hW, 1.0,-1.0 ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
                    wVert( wVector3f(-hW, 1.0, 0.0 ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( hW, 1.0, 0.0 ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( hW, 1.0,-1.0 ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
        // bottom
                    wVert( wVector3f(-hW, 0.0,-1.0 ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( hW, 0.0,-1.0 ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
                    wVert( wVector3f( hW, 0.0, 0.0 ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f(-hW, 0.0, 0.0 ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) )
        ];

        // сама дверь
        verts = [
        // right
                    wVert( wVector3f( hW, 0.0, 0.0 ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( hW, 0.0,-1.0 ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( hW, 1.0,-1.0 ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( hW, 1.0, 0.0 ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
        // left
                    wVert( wVector3f(-hW, 0.0,-1.0 ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f(-hW, 0.0, 0.0 ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f(-hW, 1.0, 0.0 ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
                    wVert( wVector3f(-hW, 1.0,-1.0 ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
        ];
    }

    wMesh* mesh = wMeshCreate(cast(char*)name.ptr);
    wMeshBuffer* meshBuffer;
    wMaterial* material;

    // 1 мешбуффер - дверь
    meshBuffer = wMeshBufferCreate(verts.length, verts.ptr, indices.length, indices.ptr);
    material = wMeshBufferGetMaterial(meshBuffer);
    MaterialSetOldSchool(material);
    //wMaterialSetFlag(material, wMaterialFlags.wMF_BACK_FACE_CULLING, false);
    wMaterialSetType(material, wMaterialTypes.wMT_TRANSPARENT_ALPHA_CHANNEL);
    wMaterialSetTexture(material, 0, texture);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    // 2 мешбуффер - ограничивающая коробка
    meshBuffer = wMeshBufferCreate(ribVerts.length, ribVerts.ptr, ribIndices.length, ribIndices.ptr);
    material = wMeshBufferGetMaterial(meshBuffer);
    MaterialSetOldSchool(material);
    //wMaterialSetType(material, wMaterialTypes.wMT_TRANSPARENT_ALPHA_CHANNEL);
    // создаем невидимую текстуру, если она не создана ранее
    //if (g_InvisibleTexture == null)
    //    g_InvisibleTexture = CreateColorTexture(wCOLOR4s_ZERO, "Invisible Color");
    wMaterialSetTexture(material, 0, ribTexture);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    wMeshEnableHardwareAcceleration(mesh, 0);

    return mesh;
}
