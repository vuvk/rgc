module player;
/*
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;
import core.stdc.math;
*/
import std.stdio;
import std.math;

import WorldSim3D;

import global;
import constants;
import duk_config;
import duktape;
import duk_util;
import util;
import base_object;

extern (C):

class TPlayer : TObject
{
/*
    float  health;
    wVector3f pos;
    wNode* node;
    wSelector* physBody;
*/
    wAnimator* physAnim;

    void*[string] userData;   /* пользовательские значения */

    this(float x, float y, float z)
    {
        this.id = 1;

        addVar("health", 100.0);
        
        //wMesh* mesh = wMeshLoad(cast(char*)"assets/capsuleZ.obj".ptr, false);
        //wMeshSetScale(mesh, 0.15, 0, 0, mesh);
        //this.node = wNodeCreateFromMesh(mesh);
        this.node = wNodeCreateCylinder(8, 0.3, 0.6, wCOLOR4s_RED);
        //this.pos = wVector3f(x, y, z);
        wNodeSetPosition(this.node, wVector3f(x, y, z));

        wMaterial* mat = wNodeGetMaterial(this.node, 0);
        MaterialSetOldSchool(mat);

        wMesh* mesh = wNodeGetMesh(this.node);
        this.physBody = wCollisionCreateFromMesh(mesh, this.node, 0);
        //this.physBody = wCollisionCreateFromBox(this.node);
        wNodeAddCollision(this.node, this.physBody);

        if (g_WorldCollider != null)
        {
            this.physAnim = wAnimatorCollisionResponseCreate(g_WorldCollider, this.node, 0.05f);

            ///Get & Set collision animator parameters
            wAnimatorCollisionResponse params;
            wAnimatorCollisionResponseGetParameters(this.physAnim, &params);
            params.gravity = wVector3f(0.0, -1.0, 0.0);
            params.ellipsoidRadius = wVector3f(0.3, 0.3, 0.3);
            params.ellipsoidTranslation = wVector3f(0.0, -0.3, 0.0);
            wAnimatorCollisionResponseSetParameters(this.physAnim, params);
        }

        //wNodeSetDebugMode(this.node, /*wDebugMode.wDM_MESH_WIRE_OVERLAY | */wDebugMode.wDM_BBOX);
        //wNodeSetDebugDataVisible(this.node, true);
    }

    ~this()
    {
        //foreach(key; userData.keys)
        //    userData[key] = null;

        if (this.physAnim) wAnimatorDestroy(this.node, this.physAnim);
        //if (this.node)     wNodeDestroy(this.node);
    }
}

class TCamera : TObject
{
   /*
    wVector3f pos;
    */

    float pitch;
    float yaw;
    float roll;
/*
    wNode* node;

    void*[string] userData;   // пользовательские значения 
*/

    this(float x, float y, float z)
    {
        this.id = 2;
        //this.pos = wVector3f(x, y, z);
        this.node = wCameraCreate(wVector3f(x, y, z), wVECTOR3f_ZERO);

        this.pitch = 0.0f;
        this.yaw   = 0.0f;
        this.roll  = 0.0f;

        wCameraSetClipDistance(this.node, ((MAP_WIDTH * MAP_HEIGHT) >> 1), 0.001f);
        this.UpdateAngles();
    }

    ~this()
    {
        /*
        foreach(key; userData.keys)
            userData[key] = null;

        if (this.node)
            wNodeDestroy(this.node);
        */
    }

    void UpdateAngles()
    {
        if (this.pitch < 0.0f) this.pitch += 360.0;
        if (this.yaw   < 0.0f) this.yaw   += 360.0;
        if (this.roll  < 0.0f) this.roll  += 360.0;

        if (this.pitch >= 360.0f) this.pitch -= 360.0;
        if (this.yaw   >= 360.0f) this.yaw   -= 360.0;
        if (this.roll  >= 360.0f) this.roll  -= 360.0;

        float pitchRad = this.pitch * DEG_TO_RAD_COEFF;
        float yawRad   = this.yaw   * DEG_TO_RAD_COEFF;

        wVector3f pos = wNodeGetPosition(this.node);

        wVector3f target = {pos.x - sin(yawRad),
                            pos.y + tan(pitchRad),
                            pos.z - cos(yawRad)};
        wCameraSetTarget(this.node, target);
    }

    void SetPitch(float deg)
    {
        this.pitch = deg;
        UpdateAngles();
    }

    void SetYaw(float deg)
    {
        this.yaw = deg;
        UpdateAngles();
    }
    /*
    void SetRoll(float deg)
    {
        this.roll = deg;
        UpdateAngles();
    }
    */
}

__gshared TPlayer g_Player;
__gshared TCamera g_Camera;


/** INTERPRETER */
/*
duk_ret_t n_cameraSetPosition(duk_context *ctx);
duk_ret_t n_cameraGetPositionX(duk_context *ctx);
duk_ret_t n_cameraGetPositionY(duk_context *ctx);
duk_ret_t n_cameraGetPositionZ(duk_context *ctx);
*/
/*
duk_ret_t n_cameraSetTarget(duk_context *ctx);
duk_ret_t n_cameraGetTargetX(duk_context *ctx);
duk_ret_t n_cameraGetTargetY(duk_context *ctx);
duk_ret_t n_cameraGetTargetZ(duk_context *ctx);
*/
/*
duk_ret_t n_cameraSetAngles(duk_context* ctx);
duk_ret_t n_cameraSetPitch(duk_context* ctx);
duk_ret_t n_cameraSetYaw(duk_context* ctx);
//duk_ret_t n_cameraSetRoll(duk_context* ctx);
//duk_ret_t n_cameraGetAngles(duk_context* ctx);
duk_ret_t n_cameraGetPitch(duk_context* ctx);
duk_ret_t n_cameraGetYaw(duk_context* ctx);
//duk_ret_t n_cameraGetRoll(duk_context* ctx);
*/

/*
void CameraCreate(float x, float y, float z);
void CameraDestroy();
*/

void RegisterPlayerCFunctions();
void RegisterCameraCFunctions();


// IMPLEMENTATION
/*
void PlayerCreate(float x, float y, float z)
{
    if (player != null)
        PlayerDestroy();

    player = cast(TPlayer*)calloc(TPlayer.sizeof, 1);

    player.health = 100.0f;
    player.node = wNodeCreateCylinder(8, 0.3, 0.8, wCOLOR4s_RED);
    player.pos = wVector3f(x, y, z);
    wNodeSetPosition(player.node, player.pos);

    wMaterial* mat = wNodeGetMaterial(player.node, 0);
    MaterialSetOldSchool(mat);

    player.physBody = wCollisionCreateFromBox(player.node);
    wNodeAddCollision(player.node, player.physBody);

    player.physAnim = wAnimatorCollisionResponseCreate(g_WorldCollider, player.node, 0.05f);

    ///Get & Set collision animator parameters
    wAnimatorCollisionResponse params;
    wAnimatorCollisionResponseGetParameters(player.physAnim, &params);
    params.gravity = wVECTOR3fZERO;
    params.ellipsoidRadius = wVector3f(0.3, 0.8, 0.3);
    //params.ellipsoidTranslation = (wVector3f){0, 25, 0};
    wAnimatorCollisionResponseSetParameters(player.physAnim, params);
}

void PlayerDestroy()
{
    if (player == null)
        return;

    if (player.physAnim) wAnimatorDestroy(player.node, player.physAnim);
    //if (player.physBody) wCollision
    if (player.node)     wNodeDestroy(player.node);
    free(player);
    player = null;
}
*/
/*
static void CameraUpdateAngles()
{
    if (camera == null)
        return;

    if (camera.pitch < 0.0f) camera.pitch += 360.0;
    if (camera.yaw   < 0.0f) camera.yaw   += 360.0;
    //if (camera.roll  < 0.0f) camera.roll  += 360.0;

    if (camera.pitch >= 360.0f) camera.pitch -= 360.0;
    if (camera.yaw   >= 360.0f) camera.yaw   -= 360.0;
    //if (camera.roll  >= 360.0f) camera.roll  -= 360.0;

    float pitchRad = camera.pitch * DEG_TO_RAD_COEFF;
    float yawRad   = camera.yaw   * DEG_TO_RAD_COEFF;

    wVector3f target = {camera.pos.x - sin(yawRad),
                        camera.pos.y + tan(pitchRad),
                        camera.pos.z - cos(yawRad)};
    wCameraSetTarget(camera.node, target);
}

void CameraCreate(float x, float y, float z)
{
    if (camera != null)
        CameraDestroy();

    camera = cast(TCamera*)calloc(TCamera.sizeof, 1);

    camera.pos = wVector3f(x, y, z);
    camera.node = wCameraCreate(camera.pos, wVECTOR3fZERO);

    wCameraSetClipDistance(camera.node, ((MAP_WIDTH * MAP_HEIGHT) >> 1), 0.001f);
    CameraUpdateAngles();
}

void CameraDestroy()
{
    if (camera == null)
        return;

    if (camera.node) wNodeDestroy(camera.node);

    free(camera);
    camera = null;
}
*/
/*
void CameraSetAngles(wVector3f angles)
{
    if (playerNode != null)
    {
        camera.pitch = angles.x;
        camera.yaw   = angles.y;
        camera.roll  = angles.z;

        CameraUpdateAngles();
    }
}
*/
/*
void CameraSetPitch(float deg)
{
    if (camera != null)
    {
        camera.pitch = deg;

        CameraUpdateAngles();
    }
}

void CameraSetYaw(float deg)
{
    if (camera != null)
    {
        camera.yaw = deg;

        CameraUpdateAngles();
    }
}
*/
/*
void CameraSetRoll(float deg)
{
    if (playerNode != null)
    {
        camera.roll = deg;

        CameraUpdateAngles();
    }
}
*/
/*
wVector3f CameraGetAngles()
{
    if (playerNode == null)
        return wVECTOR3fZERO;

    return (wVector3f){camera.pitch, camera.yaw, camera.roll};
}
*/
/*
float CameraGetPitch()
{
    if (camera == null)
        return 0.0f;

    return camera.pitch;
}

float CameraGetYaw()
{
    if (camera == null)
        return 0.0f;

    return camera.yaw;
}
*/
/*
float CameraGetRoll()
{
    if (playerNode == null)
        return 0.0f;

    return camera.roll;
}
*/


/** INTERPRETER */
/** POSITION */
/*
duk_ret_t n_cameraSetPosition(duk_context *ctx)
{
    //if (DUK_CHECK_NARGS_NOT_EQUAL(3) != DUK_ERR_NONE)
    //    return 0;

    if (g_Camera != null)
    {
        wVector3f pos;
        pos.x = duk_to_number(ctx, 0);
        pos.y = duk_to_number(ctx, 1);
        pos.z = duk_to_number(ctx, 2);
        g_Camera.pos = pos;
        //g_Camera.pos = wVector3f(duk_to_number(ctx, 0), duk_to_number(ctx, 1), duk_to_number(ctx, 2));

        wNodeSetPosition(g_Camera.node, g_Camera.pos);
    }

    return 0;
}
duk_ret_t n_cameraGetPositionX(duk_context *ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    if (g_Camera != null)
    {
        g_Camera.pos = wNodeGetPosition(g_Camera.node);
        duk_push_number(ctx, g_Camera.pos.x);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_cameraGetPositionY(duk_context *ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    if (g_Camera != null)
    {
        g_Camera.pos = wNodeGetPosition(g_Camera.node);
        duk_push_number(ctx, g_Camera.pos.y);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_cameraGetPositionZ(duk_context *ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    if (g_Camera != null)
    {
        g_Camera.pos = wNodeGetPosition(g_Camera.node);
        duk_push_number(ctx, g_Camera.pos.z);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
*/

/** TARGET */
/*duk_ret_t n_cameraSetTarget(duk_context *ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(3);

    if (playerNode != null)
    {
        wVector3f target;
        target.x = duk_to_number(ctx, 0);
        target.y = duk_to_number(ctx, 1);
        target.z = duk_to_number(ctx, 2);

        wCameraSetTarget(playerNode, target);
    }

    return 0;
}
*/
duk_ret_t n_cameraGetTarget(duk_context *ctx)
{
    //DUK_CHECK_NARGS_GREATER(0);

    if (g_Camera !is null && g_Camera.node != null)
    {
        wVector3f tgt = wCameraGetTarget(g_Camera.node);

        duk_idx_t arr_idx;
        arr_idx = duk_push_array(ctx);
        duk_push_number(ctx, tgt.x);
        duk_put_prop_index(ctx, arr_idx, 0);
        duk_push_number(ctx, tgt.y);
        duk_put_prop_index(ctx, arr_idx, 1);
        duk_push_number(ctx, tgt.z);
        duk_put_prop_index(ctx, arr_idx, 2);
    }
    else
    {
        duk_idx_t arr_idx;
        arr_idx = duk_push_array(ctx);
        duk_push_number(ctx, 0);
        duk_put_prop_index(ctx, arr_idx, 0);
        duk_push_number(ctx, 0);
        duk_put_prop_index(ctx, arr_idx, 1);
        duk_push_number(ctx, 0);
        duk_put_prop_index(ctx, arr_idx, 2);
    }

    return 1;
}
duk_ret_t n_cameraGetTargetX(duk_context *ctx)
{
    //DUK_CHECK_NARGS_GREATER(0);

    if (g_Camera !is null && g_Camera.node != null)
    {
        wVector3f tgt = wCameraGetTarget(g_Camera.node);
        duk_push_number(ctx, tgt.x);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_cameraGetTargetY(duk_context *ctx)
{
    //DUK_CHECK_NARGS_GREATER(0);

    if (g_Camera !is null && g_Camera.node != null)
    {
        wVector3f tgt = wCameraGetTarget(g_Camera.node);
        duk_push_number(ctx, tgt.y);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_cameraGetTargetZ(duk_context *ctx)
{
    //DUK_CHECK_NARGS_GREATER(0);

    if (g_Camera !is null && g_Camera.node != null)
    {
        wVector3f tgt = wCameraGetTarget(g_Camera.node);
        duk_push_number(ctx, tgt.z);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_cameraUpdate(duk_context *ctx)
{
    if (g_Camera !is null)
    {
        g_Camera.UpdateAngles();
    }

    return 0;
}

/** ANGLES */
/*
duk_ret_t n_cameraSetAngles(duk_context* ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(3);

    if (playerNode != null)
    {
        CameraSetAngles((wVector3f){duk_to_number(ctx, 0), duk_to_number(ctx, 1), duk_to_number(ctx, 2)});
    }

    return 0;
}
*/
duk_ret_t n_cameraSetPitch(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_NOT_EQUAL(1) != DUK_ERR_NONE)
    //    return 0;

    if (g_Camera !is null)
    {
        g_Camera.SetPitch(duk_to_number(ctx, 0));
        /*wVector3f rot = wNodeGetRotation(g_Camera.node);

        wNodeSetRotation(g_Camera.node, wVector3f(duk_to_number(ctx, 0), rot.y, rot.z));*/
    }

    return 0;
}
duk_ret_t n_cameraSetYaw(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_NOT_EQUAL(1) != DUK_ERR_NONE)
    //    return 0;

    if (g_Camera !is null)
    {
        g_Camera.SetYaw(duk_to_number(ctx, 0));
    }

    return 0;
}
/*
duk_ret_t n_cameraSetRoll(duk_context* ctx)
{
    DUK_CHECK_NARGS_NOT_EQUAL(1);

    if (playerNode != null)
    {
        camera.roll = duk_to_number(ctx, 0);
        CameraSetRoll(camera.roll);
    }

    return 0;
}
*/

//duk_ret_t n_cameraGetAngles(duk_context* ctx)
duk_ret_t n_cameraGetPitch(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    if (g_Camera !is null)
    {
        duk_push_number(ctx, g_Camera.pitch);
        //wVector3f rot = wNodeGetRotation(g_Camera.node);
        //duk_push_number(ctx, rot.x);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_cameraGetYaw(duk_context* ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    if (g_Camera !is null)
    {
        duk_push_number(ctx, g_Camera.yaw);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
/*
duk_ret_t n_cameraGetRoll(duk_context* ctx)
{
    DUK_CHECK_NARGS_GREATER(0);

    if (g_Camera != null)
    {
        duk_push_number(ctx, g_Camera.roll);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
*/

/** PLAYER */
/** POSITION */
/*
duk_ret_t n_playerSetPosition(duk_context *ctx)
{
    //if (DUK_CHECK_NARGS_NOT_EQUAL(3) != DUK_ERR_NONE)
    //    return 0;

    if (g_Player !is null)
    {
        wVector3f pos = wVector3f(duk_to_number(ctx, 0), duk_to_number(ctx, 1), duk_to_number(ctx, 2));
        wNodeSetPosition(g_Player.node, pos);
    }

    return 0;
}
duk_ret_t n_playerGetPositionX(duk_context *ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    if (g_Player !is null)
    {
        wVector3f pos = wNodeGetPosition(g_Player.node);
        //if (pos != g_Player.pos)
        //    g_Player.pos = pos;
        duk_push_number(ctx, pos.x);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_playerGetPositionY(duk_context *ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    if (g_Player !is null)
    {
        wVector3f pos = wNodeGetPosition(g_Player.node);
        //if (pos != g_Player.pos)
        //    g_Player.pos = pos;
        duk_push_number(ctx, pos.y);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
duk_ret_t n_playerGetPositionZ(duk_context *ctx)
{
    //if (DUK_CHECK_NARGS_GREATER(0) != DUK_ERR_NONE)
    //    return 0;

    if (g_Player !is null)
    {
        wVector3f pos = wNodeGetPosition(g_Player.node);
        //if (pos != g_Player.pos)
        //    g_Player.pos = pos;
        duk_push_number(ctx, pos.z);
    }
    else
        duk_push_number(ctx, 0.0);

    return 1;
}
*/

void RegisterCameraCFunctions()
{
   /*
    Duk_RegisterCFunction(&n_cameraSetPosition,   3, "cameraSetPosition"  );
    Duk_RegisterCFunction(&n_cameraGetPositionX, 0, "cameraGetPositionX");
    Duk_RegisterCFunction(&n_cameraGetPositionY, 0, "cameraGetPositionY");
    Duk_RegisterCFunction(&n_cameraGetPositionZ, 0, "cameraGetPositionZ");
    */
/*
    Duk_RegisterCFunction(&n_cameraSetTarget,     3, "cameraSetTarget"  );
*/
    Duk_RegisterCFunction(&n_cameraGetTarget,  0, "cameraGetTarget");
    Duk_RegisterCFunction(&n_cameraGetTargetX, 0, "cameraGetTargetX");
    Duk_RegisterCFunction(&n_cameraGetTargetY, 0, "cameraGetTargetY");
    Duk_RegisterCFunction(&n_cameraGetTargetZ, 0, "cameraGetTargetZ");
    Duk_RegisterCFunction(&n_cameraUpdate,     0, "cameraUpdate");

//  Duk_RegisterCFunction(&n_cameraSetAngles, 3, "cameraSetAngles"  );
    Duk_RegisterCFunction(&n_cameraSetPitch,  1, "cameraSetPitch");
    Duk_RegisterCFunction(&n_cameraSetYaw,    1, "cameraSetYaw");
//  Duk_RegisterCFunction(&n_cameraSetRoll,   1, "cameraSetRoll");
    Duk_RegisterCFunction(&n_cameraGetPitch,  0, "cameraGetPitch");
    Duk_RegisterCFunction(&n_cameraGetYaw,    0, "cameraGetYaw");
//  Duk_RegisterCFunction(&n_cameraGetRoll,   0, "cameraGetRoll");
}

void RegisterPlayerCFunctions()
{
    /*
    Duk_RegisterCFunction(&n_playerSetPosition,   3, "playerSetPosition"  );
    Duk_RegisterCFunction(&n_playerGetPositionX, 0, "playerGetPositionX");
    Duk_RegisterCFunction(&n_playerGetPositionY, 0, "playerGetPositionY");
    Duk_RegisterCFunction(&n_playerGetPositionZ, 0, "playerGetPositionZ");
    */
}
