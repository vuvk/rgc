#include <stdio.h>
#include <stdint.h>

/*
�� �������� ���� ��� ��� �������:
   1. �������� � ���� ��������� �������������� ������� �� ��
   2. ������������� ��������������
   3. ���������� ������ �� ��������
*/
__declspec(dllimport) char PS_AddFunction(void* ptr, const char* declaration);
__declspec(dllimport) void PS_Init();
__declspec(dllimport) char PS_ExecuteFile(const char* fileName);

/* ����� ���� ����� ������, ������� ��������� � ��������� */
void print_msg(char* msg)
{
    printf("message - \"%s\"\n", msg);
}

/* ����� ���� ����� �����, ������� ��������� � ��������� */
void print_num (int32_t num)
{
    printf("number - \"%d\"\n", num);
}

void main()
{
	/* ������������ ������� �� �� �� ���������� ����� */
    PS_AddFunction(print_msg, "procedure PrintMsg(const msg : PChar);"  );
    PS_AddFunction(print_num, "procedure PrintNum(const num : Integer);");
	/* �������������� ������������� */
    PS_Init();
	/* ��������� ������ �� ������� */
    PS_ExecuteFile("test.pas");

    getchar();
}
