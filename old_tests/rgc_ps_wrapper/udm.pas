unit udm;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  FileUtil,
  Dialogs,
  uPSComponent,
  uPSComponent_Default;

type
  TFunctionDeclaration = record
    ptr  : Pointer;
    decl : AnsiString;
  end;

  { TDM }

  TDM = class(TDataModule)
    PSCustomPlugin: TPSCustomPlugin;
    PSDllPlugin: TPSDllPlugin;
    PSImport_Classes: TPSImport_Classes;
    PSImport_DateUtils: TPSImport_DateUtils;
    PSScript: TPSScript;
    procedure PSScriptCompile(Sender: TPSScript);
  private

  public

  end;

var
  DM: TDM;
  functionDeclarations : array of TFunctionDeclaration; 
  bufferToPChar : AnsiString;

function StrToPChar(str : AnsiString) : PChar;
procedure WriteStr(str : AnsiString);         
procedure WriteStrLn(str : AnsiString);

implementation

{$R *.lfm}

function StrToPChar(str : AnsiString) : PChar;
begin
  bufferToPChar := str;
  Result := PChar(bufferToPChar);
end;

procedure WriteStr(str: AnsiString);
begin
  Write(str);
end;

procedure WriteStrLn(str: AnsiString);
begin
  WriteLn(str);
end;

{ TDM }

procedure TDM.PSScriptCompile(Sender: TPSScript);
var
  i : Integer;
begin
  for i := 0 to High(functionDeclarations) do
    with (functionDeclarations[i]) do
      DM.PSScript.AddFunctionEx(ptr, decl, TDelphiCallingConvention.cdCdecl);

  PSScript.AddFunction(@WriteStr,    'procedure Write(str : String);');
  PSScript.AddFunction(@WriteStrLn,  'procedure WriteLn(str : String);');
  PSScript.AddFunction(@ShowMessage, 'procedure ShowMessage(const aMsg: string);'); 
  PSScript.AddFunction(@StrToPChar,  'function StrToPChar(const aStr: AnsiString) : PChar;');
end;

end.

