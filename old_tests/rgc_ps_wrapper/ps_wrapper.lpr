library ps_wrapper;

{$mode objfpc}{$H+}

uses
  Interfaces,
  Classes,
  SysUtils,
  Forms,
  Dialogs,
  pascalscript,
  uPSComponent,
  udm
  { you can add units after this };

{var
  PSCustomPlugin: TPSCustomPlugin = nil;
  PSDllPlugin: TPSDllPlugin = nil;
  PSImport_Classes: TPSImport_Classes = nil;
  PSImport_DateUtils: TPSImport_DateUtils = nil;
  PSScript: TPSScript = nil;         }

function PS_AddFunction(ptr : Pointer; const declaration : PChar) : Boolean; cdecl;
var
  funcDecl : TFunctionDeclaration;
  pos : Integer;
begin
  Result := False;

  //if (DM.PSScript = nil) then exit;
  if ((ptr = nil) or (declaration = nil)) then exit;

  // добавляем в конец функцию
  funcDecl.ptr  := ptr;
  funcDecl.decl := declaration;

  pos := Length(functionDeclarations);
  SetLength(functionDeclarations, pos + 1);
  functionDeclarations[pos] := funcDecl;

  //WriteLn('address - ', Integer(funcDecl.ptr));
  //WriteLn('declaration - ', funcDecl.decl);

  Result := True;
end;

procedure PS_Init(); cdecl;
var
  i : Integer;
begin                 
  {PSCustomPlugin := TPSCustomPlugin.Create(nil);
  PSDllPlugin := TPSDllPlugin.Create(nil);
  PSImport_Classes := TPSImport_Classes.Create(nil);
  PSImport_DateUtils := TPSImport_DateUtils.Create(nil);

  PSScript := TPSScript.Create(nil);  }

  Application.Initialize;
  Application.CreateForm(TDM, DM);
  //Application.Run;
  //DM := TDM.Create(nil);
  {for i := 0 to High(functionDeclarations) do
    with (functionDeclarations[i]) do
      DM.PSScript.AddFunctionEx(ptr, decl, TDelphiCallingConvention.cdRegister);   }
end;

function PS_ExecuteFile(const fileName : PChar) : Boolean; cdecl;
var
  i : Integer;
  msg : AnsiString = '';
begin   
  Result := False;   

  if (DM.PSScript = nil) then exit;
  if (not FileExists(fileName)) then exit;

  with (DM.PSScript) do
    begin   
      Script.LoadFromFile(fileName);
      if (Compile()) then
        Execute()
      else
        //show any compile errors
        begin
          for i := 0 to CompilerMessageCount - 1 do
            msg += CompilerErrorToStr(i) + #10#13;
          ShowMessage(msg);
        end;
    end;
end;


exports
  PS_AddFunction,
  PS_Init,
  PS_ExecuteFile
  ;

end.

