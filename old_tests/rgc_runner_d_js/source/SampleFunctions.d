module SampleFunctions;

import std.stdio;

import WorldSim3D;

void PrintWithColor(string text, wConsoleFontColor color, bool waitKey);
wTexture* CreateColorTexture(wColor4s tColor, string textureName);



// IMPLEMENTATION

void PrintWithColor(string text, wConsoleFontColor color, bool waitKey)
{
    int def = wConsoleSaveDefaultColors();
    wConsoleSetFontColor(color);
    writeln(text);
    //printf("%s\n",text);
    wConsoleResetColors(def);
    if (waitKey)
    {
        // printf("\n");
        //printf("Press any key...\n");
        writeln("\nPress any key...");
        wInputWaitKey();
    }
}

wTexture* CreateColorTexture(wColor4s tColor, string textureName)
{
    wTexture* t = wTextureCreate(cast(char*)textureName.ptr, wVector2i(1, 1), wColorFormat.wCF_A8R8G8B8);
    uint* lock = wTextureLock(t);
    *lock = wUtilColor4sToUInt(tColor);
    wTextureUnlock(t);
    return t;
}

