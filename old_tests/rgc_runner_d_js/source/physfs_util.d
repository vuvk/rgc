module physfs_util;

public {
import derelict.physfs.physfs;
}
import WorldSim3D;
import SampleFunctions;

extern(C):

void InitPhysFS();
void StopPhysFS();
//bool FileExists(const string fileName);
//void FileRead(const string fileName, ref ubyte[] buffer);


// IMPLEMENTATION

void InitPhysFS()
{
    DerelictPHYSFS.load();

    PHYSFS_init(null);

    /* ресурсы */
    PHYSFS_addToSearchPath("assets/",                 0);
    PHYSFS_addToSearchPath("resources/maps/",         0);
    PHYSFS_addToSearchPath("resources/skyboxes/",     0);
    PHYSFS_addToSearchPath("resources/sprites/",      0);
    PHYSFS_addToSearchPath("resources/textures/",     0);
    PHYSFS_addToSearchPath("resources/weapons_ammo/", 0);
    PHYSFS_addToSearchPath("resources/doors_keys/",   0);

    /* скрипты */
    PHYSFS_addToSearchPath("assets/scripts/",         0);
    PHYSFS_addToSearchPath("assets/scripts/doors/",   0);
    PHYSFS_addToSearchPath("assets/scripts/keys/",    0);
    PHYSFS_addToSearchPath("assets/scripts/level/",   0);
    PHYSFS_addToSearchPath("assets/scripts/player/",  0);
    PHYSFS_addToSearchPath("assets/scripts/sprites/", 0);
    PHYSFS_addToSearchPath("assets/scripts/weapons/", 0);
    PHYSFS_addToSearchPath("assets/scripts/weapons/bullets/",       0);
    PHYSFS_addToSearchPath("assets/scripts/weapons/weapon_items/",  0);
    PHYSFS_addToSearchPath("assets/scripts/weapons/weapon_inhand/", 0);

    /* пак */
    PHYSFS_addToSearchPath("resources.pk3", 1);
}

void StopPhysFS()
{
    PHYSFS_deinit();
}

bool FileExists(const string fileName)
{
    return cast(bool)(PHYSFS_exists(fileName.ptr));
}

bool FileRead(const string fileName, ref ubyte[] buffer)
{
    buffer.length = 0;
    if (!FileExists(fileName))
        return false;

    PHYSFS_File* file = PHYSFS_openRead(fileName.ptr);
    long fileSize = PHYSFS_fileLength(file);
    buffer.length = cast(uint)fileSize;
    long readed = PHYSFS_read(file, buffer.ptr, 1, cast(uint)fileSize);
    PHYSFS_close(file);
    if (readed != fileSize)
    {
        PrintWithColor("Readed bytes count and size of file '" ~ fileName ~ "' are not equal\n", wConsoleFontColor.wCFC_RED, false);
        return false;
    }

    return true;
}

long FileSize(const string fileName)
{
    if (!FileExists(fileName))
        return 0;

    PHYSFS_File* file = PHYSFS_openRead(fileName.ptr);
    long fileSize = PHYSFS_fileLength(file);
    PHYSFS_close(file);

    return fileSize;
}
