module constants;

import std.math;

immutable int MAX_FPS = 60;   /** Максимальный FPS для установки ограничения */

immutable int MAP_WIDTH  = 64;  /** Максимальная ширина карты */
immutable int MAP_HEIGHT = 64;  /** Максимальная высота карты */

immutable int TEXTURE_MAX_WIDTH  = 256;  /** Максимальная ширина текстуры в пикселях */
immutable int TEXTURE_MAX_HEIGHT = 256;  /** Максимальная высота текстуры в пикселях */
immutable int TEXTURE_MAX_SIZE   = TEXTURE_MAX_WIDTH * TEXTURE_MAX_HEIGHT * int.sizeof;   /** Максимальный размер текстуры в байтах */

immutable int TEXTURES_MAX_COUNT = 1024;
immutable int SPRITES_MAX_COUNT  = 1024;
immutable int DOORS_MAX_COUNT    = 1024;
immutable int KEYS_MAX_COUNT     = 1024;
immutable int WEAPONS_MAX_COUNT  = 10;
immutable int BULLETS_MAX_COUNT  = 1024;
immutable int AMMO_MAX_COUNT     = 1024;

immutable BYTECODE_MAX_LENGTH = 131072;  /** максимальная длина массива под байткод = 0.5 Mb */

immutable double SCRIPTS_DELAY = 1.0 / 100.0; /** частота, с которой будут вызываться скрипты - 60 раз в секунду */


/** размер объектов в игре? */
immutable double BLOCK_SIZE = 1;

immutable double DEG_TO_RAD_COEFF = PI / 180.0;
immutable double RAD_TO_DEG_COEFF = 180.0 / PI;
