module app;

import std.stdio;
import std.conv;
import core.memory;
import core.thread;

import WorldSim3D;
import SampleFunctions;

import js_util;
import global;
import constants;
import res_manager;
import textures;
import sprites;
import doors_keys;
import weapons_ammo;
import map;

import base_object;
import player;


int main(string[] args)
{
    int prevFPS;
    wstring wndCaption = "Raycasting Game Constructor Runner";

    //GC.disable();

    // перебираем параметры
    for (int i = 0; i < args.length; ++i)
    {
        // установка рендера
        if ("-ogl"   == args[i]) { g_Renderer = wDriverTypes.wDRT_OPENGL;    }
        if ("-d3d"   == args[i]) { g_Renderer = wDriverTypes.wDRT_DIRECT3D9; }
        //if ("-soft"  == args[i]) { g_Renderer = wDriverTypes.wDRT_SOFTWARE;       g_LimitFps = false; }
        if ("-soft2" == args[i]) { g_Renderer = wDriverTypes.wDRT_BURNINGS_VIDEO; g_LimitFps = false; }
        if ("-vsync" == args[i]) { g_VSync    = true; }
        if ("-fullscreen" == args[i]) { g_FullScreenMode = true; }
        if ("-no_limit_fps" == args[i]) { g_LimitFps = false; }

        if ("-w" == args[i] && (i + 1 < args.length))
        {
            uint w = parse!uint(args[i + 1]);
            g_Resolution.x = w;
        }

        if ("-h" == args[i] && (i + 1 < args.length))
        {
            uint h = parse!uint(args[i + 1]);
            g_Resolution.y = h;
        }
    }

    ///Start engine
    bool init = wEngineStart(g_Renderer, g_Resolution, 32, g_FullScreenMode, false, true, g_VSync) != 0;
//    bool init = wEngineStartAdvanced(g_Renderer, g_Resolution, 32, g_FullScreenMode, false, true, g_VSync, wDeviceTypes.wDT_BEST, , 0, true, null);
    if (!init)
    {
        PrintWithColor("wEngineStart() failed!", wConsoleFontColor.wCFC_RED, true);
        return -1;
    }

    InitRunner();

    wNode* laserNode;
    wTexture* laserTexture = wTextureLoad(cast(char*)("assets/beam.png".ptr));
    wTexture* crosshair = wTextureLoad(cast(char*)"assets/crosshair.png".ptr);
    wFont* font = wFontLoad(cast(char*)"assets/4.png".ptr);
    wVector3f camPos;
/*
    TScriptJS levelInitScript;
    TScriptJS levelUpdateScript;
    TScriptJS playerInitScript;
    TScriptJS playerUpdateScript;
    TScriptJS cameraInitScript;
    TScriptJS cameraUpdateScript;
*/
    TScriptJS levelUpdateCommand;

    void StartInterpreter()
    {
        JS_InitVM();

        TScriptJS tmp;
        // load global values
        //tmp.loadFromFile("assets/scripts/global.js");
        tmp.loadFromFile("global.js");
        tmp.run();

        // load key codes
        //tmp.loadFromFile("assets/scripts/keycodes.js");
        tmp.loadFromFile("keycodes.js");
        tmp.run();

        // load vectors
        //tmp.loadFromFile("assets/scripts/vectors.js");
        tmp.loadFromFile("vectors.js");
        tmp.run();
        tmp.clear();

        // level's scripts
        g_ObjectId = 0;
        //tmp.loadFromFile("assets/scripts/level/level.js");
        tmp.loadFromFile("level.js");
        with (tmp)
        {
            string className  = "LevelClass";
            string objectName = "Level";
            // заворачиваем код в вид:
            // function Object0(){ текст_скрипта }
            // var object0 = new Object0();
            text = "function " ~ className ~ "(){" ~ text ~ "} ";
            text ~= "var " ~ objectName ~ " = new " ~ className ~ "();";
            //compile();
            run();

            // запоминаем команды для инициализации и обновления объекта
            text = objectName ~ ".onInit();";
            //compile();
            run();

            levelUpdateCommand.fileName = objectName ~ " update command";
            levelUpdateCommand.text = objectName ~ ".onUpdate();";
            //levelUpdateCommand.compile();
        }


        // player's scripts
        with (g_Player)
        {
            //g_ObjectId = id;
            script = new TScriptJS;
            //script.loadFromFile("assets/scripts/player/player.js");
            script.loadFromFile("player.js");
            string className  = "PlayerClass";
            string objectName = "Player";
            init(className, objectName);
        }

        // camera's scripts
        with (g_Camera)
        {
            //g_ObjectId = id;
            script = new TScriptJS;
            //script.loadFromFile("assets/scripts/player/camera.js");
            script.loadFromFile("camera.js");
            string className  = "CameraClass";
            string objectName = "Camera";
            init(className, objectName);
        }

        /* spriteNodes scripts */
        foreach(spriteNode; g_SpriteNodes)
        {
            if (spriteNode !is null)
            {
                with (spriteNode)
                {
                    index.script.loadFromFile(index.script.fileName);

                    script          = new TScriptJS();
                    script.fileName = index.script.fileName.dup;
                    script.text     = index.script.text.dup;
                    script.setId();
                    string className  = "Sprite" ~ to!string(g_ScriptsCount);
                    string objectName = "sprite" ~ to!string(g_ScriptsCount);
                    init(className, objectName);
                }
            }
        }

        // door's scripts
        foreach(doorNode; g_DoorNodes)
        {
            if (doorNode !is null)
            {
                with (doorNode)
                {
                    index.script.loadFromFile(index.script.fileName);

                    script          = new TScriptJS();
                    script.fileName = index.script.fileName.dup;
                    script.text     = index.script.text.dup;
                    script.setId();
                    string className  = "Door" ~ to!string(g_ScriptsCount);
                    string objectName = "door" ~ to!string(g_ScriptsCount);
                    init(className, objectName);
                }
            }
        }

        // keys scripts
        foreach(keyNode; g_KeyNodes)
        {
            if (keyNode !is null)
            {
                with (keyNode)
                {
                    index.script.loadFromFile(index.script.fileName);

                    script          = new TScriptJS();
                    script.fileName = index.script.fileName.dup;
                    script.text     = index.script.text.dup;
                    script.setId();
                    string className  = "Key" ~ to!string(g_ScriptsCount);
                    string objectName = "key" ~ to!string(g_ScriptsCount);
                    init(className, objectName);
                }
            }
        }

        // bullets scripts
        foreach (bullet; g_BulletNodes)
        {
            if (bullet !is null)
                with (bullet)
                {
                    index.script.loadFromFile(index.script.fileName);

                    script          = new TScriptJS();
                    script.fileName = index.script.fileName.dup;
                    script.text     = index.script.text.dup;
                    script.setId();
                    string className  = "Bullet" ~ to!string(g_ScriptsCount);
                    string objectName = "bullet" ~ to!string(g_ScriptsCount);
                    init(className, objectName);
                }
        }

        // weapon items scripts
        foreach (weaponItemNode; g_WeaponItemNodes)
        {
            if (weaponItemNode !is null)
            {
                with (weaponItemNode)
                {
                    index.script.loadFromFile(index.script.fileName);

                    script          = new TScriptJS();
                    script.fileName = index.script.fileName.dup;
                    script.text     = index.script.text.dup;
                    script.setId();
                    string className  = "WeaponItem" ~ to!string(g_ScriptsCount);
                    string objectName = "weaponItem" ~ to!string(g_ScriptsCount);
                    init(className, objectName);
                }
            }
        }

        foreach (weapon; g_Weapons)
        {
            if (weapon !is null)
                with (weapon)
                {
                    script = new TScriptJS();
                    //script.loadFromFile("assets/scripts/weapons/weapon_inhand/weapon_inhand.js");
                    script.loadFromFile("weapon_inhand.js");
                    string className  = "WeaponInHand" ~ to!string(g_ScriptsCount);
                    string objectName = "weaponInHand" ~ to!string(g_ScriptsCount);
                    init(className, objectName);
                }
        }
    }

    void LoadLevel(int num, bool firstRun = false)
    {
        GC.disable();
        //wSceneDestroyAllNodes();
        //wSceneDestroyAllMeshes();
        //wSceneDestroyAllTextures();

        // открываем пак
        int res = Res_OpenZipForRead("resources.pk3");
        switch (res)
        {
            case RES_NOT_FOUND  :
                PrintWithColor("Pack of maps is doesn''t exist!\n", wConsoleFontColor.wCFC_RED, false);
                break;
            case RES_NOT_OPENED :
                PrintWithColor("Error when opening pack of maps!\n", wConsoleFontColor.wCFC_RED, false);
                break;

            case RES_TRUE :
                //printf("LoadMapPack");
                if (firstRun)
                    LoadMapPack();
                //printf("Prepare");
                InitImages2D();
                PrepareTexturesNeedForLoad(num);
                PrepareSpritesNeedForLoad(num);
                PrepareKeysNeedForLoad(num);
                PrepareDoorsNeedForLoad(num);
                PrepareWeaponsNeedForLoad(num);
                //printf("LoadTexturePack");
                LoadTexturesPack();
                //printf("LoadSpritePack");
                LoadSpritesPack();
                //printf("LoadDoorPack");
                LoadKeysPack();
                LoadDoorsPack();
                LoadBulletsPack();
                LoadWeaponItemsPack();
                LoadWeaponsPack();
                //printf("GenerateLevel");
                GenerateLevel(num);
                //printf("done");
                break;

            default : break;
        }
        Res_CloseZipAfterRead();

        /* создаем игрока */
        g_Player = new TPlayer(3.0 * BLOCK_SIZE, 0.1 * BLOCK_SIZE, -3.0 * BLOCK_SIZE);
        g_Camera = new TCamera(3.0, 0.0, -3.0);

        /* и только теперь мы можем собрать таблицу идентификаторов объектов */
        FillObjectsIdTable();

        StartInterpreter();

        laserNode = wBillBoardCreate(wVECTOR3f_ZERO, wVector2f(0.1, 0.1));
        wMaterial* material = wNodeGetMaterial(laserNode, 0);
        wMaterialSetTexture(material, 0, laserTexture);
        MaterialSetOldSchool(material);
        wMaterialSetType(material, wMaterialTypes.wMT_TRANSPARENT_ALPHA_CHANNEL);

        GC.enable();
        GC.collect();
    }

    LoadLevel(0, true);
/*
    thread_init();
    Thread thrdSpriteNodesUpdate  = new Thread({SpriteNodesUpdate();});
    Thread thrdTextureNodesUpdate = new Thread({TextureNodesUpdate();});
    Thread thrdDoorNodesUpdate    = new Thread({DoorNodesUpdate();});
    Thread thrdKeyNodesUpdate     = new Thread({KeyNodesUpdate();});
*/

    double timeForGC = 0;   /** пора собрать мусор? */
    g_DeltaTime = 0;
    g_DeltaTimeScript = 0;
    g_TimeForRunScript = 0;

    /** свойства материала для 2Д объектов */
    wMaterial* material2d = wEngineGet2dMaterial ();
    wMaterialSetAntiAliasingMode(material2d, wAntiAliasingMode.wAAM_OFF);
    wMaterialSetFlag(material2d, wMaterialFlags.wMF_ANISOTROPIC_FILTER, false);
    wMaterialSetFlag(material2d, wMaterialFlags.wMF_ANTI_ALIASING,      false);
    wMaterialSetFlag(material2d, wMaterialFlags.wMF_BILINEAR_FILTER,    false);
    wMaterialSetFlag(material2d, wMaterialFlags.wMF_TRILINEAR_FILTER,   false);
    wMaterialSetFlag(material2d, wMaterialFlags.wMF_FOG_ENABLE,         false);

    wVector3f pos, start, dir, end, collideAt;

    while (wEngineRunning())
    {
        //wSceneBeginAdvanced(wCOLOR4s_BLACK, 0, 1);
        wSceneBegin(wCOLOR4s_BLACK);

        g_DeltaTime = wTimerGetDelta();
        timeForGC += g_DeltaTime;
        g_TimeForRunScript += g_DeltaTime;

        g_DeltaTimeScript = 0;
        if (g_TimeForRunScript >= SCRIPTS_DELAY)
        {
            g_DeltaTimeScript = g_TimeForRunScript;
            g_TimeForRunScript = 0;
        }

        /* запускаем скрипт уровня */
        g_ObjectId = 0;
        levelUpdateCommand.run();

        /* запускаем скрипт для игрока */
        g_Player.update();
        /* запускаем скрипт для камеры */
        g_Camera.update();

        /* обновляем состояние уровня */
        TextureNodesUpdate();
        SpriteNodesUpdate();
        DoorNodesUpdate();
        KeyNodesUpdate();
        BulletNodesUpdate();
        WeaponItemNodesUpdate();

/*
        //duk_thread_state st;
        //duk_suspend(ctx, &st);

        thrdSpriteNodesUpdate.start();
        thrdTextureNodesUpdate.start();
        thrdDoorNodesUpdate.start();
        thrdKeyNodesUpdate.start();

        thrdSpriteNodesUpdate.join();
        thrdTextureNodesUpdate.join();
        thrdDoorNodesUpdate.join();
        thrdKeyNodesUpdate.join();

        //thread_joinAll();
        //duk_resume(ctx, &st);
*/

        // инициализируем объекты из таблицы ининициализации
        InitObjects();
        // ищем и удаляем объекты, помеченные на удаление
        DeleteObjects();
        if (timeForGC >= 1.0)
        {
            JS_GC();
            GC.collect();
            timeForGC -= 1.0;
        }

        /*
        for (int i = 0; i < lasers.length; ++i)
            lasers[i].show();

        pos   = wNodeGetPosition(g_Camera.node);
        start = wCameraGetTarget(g_Camera.node);
        dir = wVector3f (start.x - pos.x, start.y - pos.y, start.z - pos.z);
        dir = wMathVector3fNormalize(dir);

        end = wVector3f (dir.x * 100.0, dir.y * 100.0, dir.z * 100.0);
        end.x += pos.x;
        end.y += pos.y;
        end.z += pos.z;

        // чтобы сместить начало выстрела
        start.x -= dir.x * 0.6;
        start.y -= dir.y * 0.6;
        start.z -= dir.z * 0.6;

        collideAt = wVECTOR3f_ZERO;

        wTriangle tris;
        wVector3f normal;
        wstring message;
        if (wCollisionGetPointFromRay(g_WorldCollider, &start, &end, &collideAt, &normal, &tris))
        {
            //w3dDrawTriangle(tris, wCOLOR4s_RED);

            wVector3f offset;
            wVector3f point;

            // для адекватного отображения лазера немного сдвинем точку "наружу"
            offset = wMathVector3fNormalize(normal);
            offset.x *= 0.035;
            offset.y *= 0.035;
            offset.z *= 0.035;
            point = wMathVector3fAdd(collideAt, offset);
            wNodeSetPosition(laserNode, point);

            // для стопроцентного определения объекта немного сдвинем точку "вовнутрь"
            offset = wMathVector3fNormalize(normal);
            offset.x *= -0.01;
            offset.y *= -0.01;
            offset.z *= -0.01;
            point = wMathVector3fAdd(collideAt, offset);

            if (g_SolidObjects != null)
            {
                wNode* target = wCollisionGetNodeChildFromPoint(g_SolidObjects, 0, false, &point);
                if (target != null)
                {
                    void* ptr = wNodeGetUserData(target);
                    if (ptr != null)
                    {
                        int id = *(cast(int*)ptr);
                        if (id > 0 && id < g_ObjectsIdTable.length)
                        {
                            TObject object = g_ObjectsIdTable[id];
                            //writefln("id of object is %d, name is %s", object.id, object.name);
                            message = "id of object is " ~ to!wstring(object.id) ~ " name is " ~ to!wstring(object.name) ~ "\0";
                        }
                    }
                    else
                    {
                        message = "OBJECT IS NOT FOUNDED!\0";
                    }
                }
            }
        }
        */

        wSceneDrawAll();

        camPos = wNodeGetPosition(g_Camera.node);
        wstring strBuffer = "Camera Pos : " ~ to!wstring(camPos.x) ~ " " ~ to!wstring(-camPos.z) ~ "\0";
        wFontDraw(font, strBuffer.ptr, wVector2i(10, 10), wVector2i(100, 20), wCOLOR4s_WHITE);
        //wFontDraw(font, message.ptr, wVector2i(10, 25), wVector2i(100, 40), wCOLOR4s_WHITE);
        wTextureDrawEx(crosshair, wVector2i((g_Resolution.x >> 1) - 16, (g_Resolution.y >> 1) - 16), wVector2f(0.5, 0.5), true);

        wEngineSet2dMaterial(true);
        WeaponUpdate();
        wEngineSet2dMaterial(false);

        wSceneEnd();

        if (wInputIsKeyEventAvailable())
        {
            // перезапуск виртуальной машины
            if (wInputIsKeyUp(wKeyCode.wKC_KEY_T))
            {
                JS_StopVM();
                StartInterpreter();
            }

            // рестарт уровня
            if (wInputIsKeyUp(wKeyCode.wKC_KEY_R))
            {
                g_Player.destroy();
                g_Camera.destroy();     //writeln("1");
                //ClearLevel();
                //writeln("1");
                JS_StopVM();
                //JS_InitVM();
                //writeln("1");

                LoadLevel(0);
                //writeln("loaded!");
            }

            if (wInputIsKeyHit(wKeyCode.wKC_F10))
                wSystemSaveScreenShot("screenshot.jpg".ptr);

            if (wInputIsKeyUp(wKeyCode.wKC_KEY_L))
            {
                wMesh*[1000] batch;
                wMesh*[1000] mesh;
                for (int i = 0; i < 1000; ++i)
                {
                    batch[i] = wMeshCreateBatching();
                    wMeshFinalizeBatching(batch[i]);
                }
                for (int i = 0; i < 1000; ++i)
                {
                    wMeshClearBatching(batch[i]);
                    wMeshDestroyBatching(batch[i]);
                    wMeshDestroy(mesh[i]);
                }
            }
        }

        ///Close by ESC
        wEngineCloseByEsc();

        /// update FPS
        if (prevFPS != wEngineGetFPS())
        {
            prevFPS = wEngineGetFPS();

            strBuffer = wndCaption ~ "   FPS : " ~ to!wstring(prevFPS) ~ "\0";
            wWindowSetCaption(strBuffer.ptr);
        }

        //GC.enable();
    }

    //InterpreterCloseVM();
    //Duk_StopVM();
    JS_StopVM();

    ///Stop engine
    wEngineStop();

    return 0;
}
