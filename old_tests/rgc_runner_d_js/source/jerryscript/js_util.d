module js_util;

import std.stdio;
import std.conv;
import std.string;
import std.math;
import std.file;
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;

public {
    import jerryscript_core;
    import jerryscript_port;
    import jerryscript_snapshot;
    import handler;
}
import WorldSim3D;
import SampleFunctions;

import global;
import input;
import constants;

import physfs_util;
import util;

import map;
import player;
import base_object;
import weapons_ammo;

extern (C):

// DECLARATION //

//__gshared duk_context* ctx = null;

struct TScriptJS
{
    uint    id;
    string  fileName;
    string  text;
    uint[]  bytecode;
    size_t  snapshot_size;

    ~this()
    {
        fileName.length = 0;
        text.length     = 0;
        bytecode.length = 0;
    }

    void setId()
    {
        this.id = g_ScriptsCount;
        ++g_ScriptsCount;
    }

    void compile()
    {
        if (text.length == 0)
            return;

        memset(g_ByteCode.ptr, 0, BYTECODE_MAX_LENGTH * uint.sizeof);

        jerry_value_t generate_result;
        generate_result = jerry_generate_snapshot (null,
                                                   0,
                                                   cast(ubyte*)this.text.ptr,
                                                   this.text.length,
                                                   jerry_generate_snapshot_opts_t.JERRY_SNAPSHOT_SAVE_STRICT,
                                                   g_ByteCode.ptr,
                                                   BYTECODE_MAX_LENGTH);
        snapshot_size = 0;
        if (!jerry_value_is_error(generate_result))
        {
            snapshot_size = cast(size_t)jerry_get_number_value (generate_result);
            bytecode = new uint[snapshot_size / uint.sizeof];
            memcpy(bytecode.ptr, g_ByteCode.ptr, snapshot_size);
        }
        else
        {
            snapshot_size = 0;
            bytecode.destroy();
            writefln("Error when compiling code '%s'", this.fileName);
            JS_PrintError(generate_result);
            readln();
            return;
        }

        jerry_release_value (generate_result);

        this.text.length = 0;
    }

    void loadFromFile(const string fileName)
    {
        clear();
        if (fileName.length == 0 || fileName == "")
            return;

        setId();

        if (!FileExists(fileName))
        {
            PrintWithColor("File '" ~ fileName ~ "' is not exists!", wConsoleFontColor.wCFC_RED, false);
            return;
        }

        ubyte[] buffer;
        FileRead(fileName, buffer);
        if (buffer.length > 0)
        {
            this.text = to!string(cast(char[])buffer);
        }
        this.fileName = fileName;
    }

    void clear()
    {
        fileName.length = 0;
        text.length     = 0;
        bytecode.length = 0;
        snapshot_size   = 0;
    }

    void run()
    {
        if (text.length == 0 && snapshot_size == 0)
            return;

        if (snapshot_size == 0)
        {
            jerry_value_t ret_val = jerry_eval (cast(ubyte*)text.ptr, text.length, 0);
            if (jerry_value_is_error(ret_val))
            {
                writefln("Error in script '%s'", fileName);
                JS_PrintError(ret_val);
                writefln("\nscript:\n%s\n", text);
                readln();
            }

            jerry_release_value (ret_val);
        }
        else
        {
            if (bytecode.length > 0)
            {
                jerry_value_t res = jerry_exec_snapshot (cast(const uint*)bytecode.ptr,
                                                         snapshot_size,
                                                         0,
                                                         jerry_exec_snapshot_opts_t.JERRY_SNAPSHOT_EXEC_ALLOW_STATIC);
                if (jerry_value_is_error(res))
                {
                    writefln("Error when executing script '%s'", this.fileName);
                    JS_PrintError(res);
                    readln();
                }

                jerry_release_value (res);
            }
        }
    }
}

void JS_InitVM ();
void JS_StopVM ();
void JS_GC ();
void JS_RegisterCFunction(jerry_external_handler_t native_function, string new_name);   /* зарегистрировать нативную функцию */
void JS_RegisterCFunctionForObject();   /* зарегистрировать метод объекта */
void JS_PrintValue (const jerry_value_t value); /* вывести jerry_value_t в зависимости от типа */
void JS_PrintError(jerry_value_t value);        /* напечатать ошибку */

// IMPLEMENTATION //

static jerry_value_t js_degToRad
            (const jerry_value_t  func_obj_val, /**< function object */
             const jerry_value_t  this_p,       /**< this arg */
             const jerry_value_t* args_p,       /**< function arguments */
             const jerry_length_t args_cnt)     /**< number of function arguments */
{
    jerry_value_t arg = args_p[0];

    if (args_cnt > 0 && jerry_value_is_number(arg))
    {
        double deg = jerry_get_number_value(arg);
        return jerry_create_number(deg*DEG_TO_RAD_COEFF);
    }

    return jerry_create_number_nan();
}
static jerry_value_t
js_radToDeg (const jerry_value_t  func_obj_val, /**< function object */
             const jerry_value_t  this_p,       /**< this arg */
             const jerry_value_t* args_p,       /**< function arguments */
             const jerry_length_t args_cnt)     /**< number of function arguments */
{
    jerry_value_t arg = args_p[0];

    if (args_cnt > 0 && jerry_value_is_number(arg))
    {
        double rad = jerry_get_number_value(arg);
        return jerry_create_number(rad*RAD_TO_DEG_COEFF);
    }

    return jerry_create_number_nan();
}
static jerry_value_t js_distanceBetweenPoints
                         (const jerry_value_t  func_obj_val, /**< function object */
                          const jerry_value_t  this_p,       /**< this arg */
                          const jerry_value_t* args_p,       /**< function arguments */
                          const jerry_length_t args_cnt)     /**< number of function arguments */
{
    jerry_value_t ret_val;

    switch(args_cnt)
    {
        case 4 :
        {
            double x0 = jerry_get_number_value(args_p[0]);
            double y0 = jerry_get_number_value(args_p[1]);
            double x1 = jerry_get_number_value(args_p[2]);
            double y1 = jerry_get_number_value(args_p[3]);
            ret_val = jerry_create_number(sqrt(sqr(x1 - x0) + sqr(y1 - y0)));
            break;
        }

        case 6 :
        {
            double x0 = jerry_get_number_value(args_p[0]);
            double y0 = jerry_get_number_value(args_p[1]);
            double z0 = jerry_get_number_value(args_p[2]);
            double x1 = jerry_get_number_value(args_p[3]);
            double y1 = jerry_get_number_value(args_p[4]);
            double z1 = jerry_get_number_value(args_p[5]);
            ret_val = jerry_create_number(sqrt(sqr(x1 - x0) + sqr(y1 - y0) + sqr(z1 - z0)));
            break;
        }

        default:
            ret_val = jerry_create_number_nan();
            break;
    }

    if (jerry_value_is_error(ret_val))
        JS_PrintError(ret_val);
    return ret_val;
}


/** SYSTEM */
static jerry_value_t js_deltaTime
             (const jerry_value_t  func_obj_val, /**< function object */
              const jerry_value_t  this_p,       /**< this arg */
              const jerry_value_t* args_p,       /**< function arguments */
              const jerry_length_t args_cnt)     /**< number of function arguments */
{
    return jerry_create_number(g_DeltaTimeScript);
}
static jerry_value_t js_windowGetWidth
                  (const jerry_value_t  func_obj_val, /**< function object */
                   const jerry_value_t  this_p,       /**< this arg */
                   const jerry_value_t* args_p,       /**< function arguments */
                   const jerry_length_t args_cnt)     /**< number of function arguments */
{
    wVector2u wndSize;
    wWindowGetSize(&wndSize);

    return jerry_create_number(wndSize.x);
}
static jerry_value_t js_windowGetHeight
                  (const jerry_value_t  func_obj_val, /**< function object */
                   const jerry_value_t  this_p,       /**< this arg */
                   const jerry_value_t* args_p,       /**< function arguments */
                   const jerry_length_t args_cnt)     /**< number of function arguments */
{
    wVector2u wndSize;
    wWindowGetSize(&wndSize);

    return jerry_create_number(wndSize.y);
}


void JS_RegisterCFunction(jerry_external_handler_t native_function, string new_name)
{
    jerryx_handler_register_global(cast(ubyte*)((new_name ~ '\0').ptr), native_function);
}

void JS_RegisterCFunctionForObject(jerry_value_t object, string objectName, jerry_external_handler_t native_function, string new_name)
{
    /* Create a JS function object and wrap into a jerry value */
    jerry_value_t func_obj = jerry_create_external_function (native_function);

    /* Set the native function as a property of the empty JS object */
    jerry_value_t prop_name = jerry_create_string (cast(const jerry_char_t*)((new_name ~ '\0').ptr));
    jerry_set_property (object, prop_name, func_obj);
    jerry_release_value (prop_name);
    jerry_release_value (func_obj);

    /* Wrap the JS object (not empty anymore) into a jerry api value */
    jerry_value_t global_object = jerry_get_global_object ();

    /* Add the JS object to the global context */
    prop_name = jerry_create_string (cast(const jerry_char_t*)((objectName ~ '\0').ptr));
    jerry_set_property (global_object, prop_name, object);
    jerry_release_value (prop_name);
}

/**
 * Step 5. Description of JerryScript value descriptors
 */
void JS_PrintValue (const jerry_value_t value)
{
    if (jerry_value_is_undefined (value) == 1)
    {
        printf ("undefined");
    }
    else if (jerry_value_is_null (value) == 1)
    {
        printf ("null");
    }
    else if (jerry_value_is_boolean (value) == 1)
    {
        if (jerry_get_boolean_value (value) == 1)
        {
            printf ("true");
        }
        else
        {
            printf ("false");
        }
    }
    /* Float value */
    else if (jerry_value_is_number (value) == 1)
    {
        printf ("number");
    }
    /* String value */
    else if (jerry_value_is_string (value) == 1)
    {
        /* Determining required buffer size */
        jerry_size_t req_sz = jerry_get_string_size (value);
        jerry_char_t* str_buf_p = cast(ubyte*)calloc(jerry_char_t.sizeof, req_sz + 1);

        jerry_string_to_char_buffer (value, str_buf_p, req_sz);
        str_buf_p[req_sz] = '\0';

        printf ("%s", str_buf_p);

        free(str_buf_p);
    }
    /* Object reference */
    else if (jerry_value_is_object (value) == 1)
    {
        printf ("[JS object]");
    }

    printf ("\n");
}

/**
 *  Get info from throwable object and print it
 */
void JS_PrintError(jerry_value_t value)
{
    /*
    jerry_error_t err_type = jerry_get_error_type(value);
    writefln("error type - %d", err_type);
    */
    jerry_value_t throwable = jerry_get_value_from_error (value, 0);
    //print_value(throwable);

    if (jerry_value_is_object(throwable))
    {
        jerry_value_t name    = jerry_create_string(cast(jerry_char_t*)"name\0".ptr);
        jerry_value_t message = jerry_create_string(cast(jerry_char_t*)"message\0".ptr);
        jerry_value_t stack   = jerry_create_string(cast(jerry_char_t*)"stack\0".ptr);

        jerry_value_t exists;

        // print name
        exists = jerry_has_property (throwable, name);
        if ((jerry_value_is_boolean(exists)) && (jerry_get_boolean_value(exists)))
        {
            jerry_value_t txt = jerry_get_property(throwable, name);
            JS_PrintValue(txt);
            jerry_release_value (txt);
        }

        // print error message
        exists = jerry_has_property (throwable, message);
        if ((jerry_value_is_boolean(exists)) && (jerry_get_boolean_value(exists)))
        {
            jerry_value_t txt = jerry_get_property(throwable, message);
            JS_PrintValue(txt);
            jerry_release_value (txt);
        }

        // print backtrace
        exists = jerry_has_property (throwable, stack);
        if ((jerry_value_is_boolean(exists)) && (jerry_get_boolean_value(exists)))
        {
            /*
            jerry_value_t txt = jerry_get_property(throwable, stack);
            print_value(txt);
            jerry_release_value (txt);
            */

            if (!jerry_value_is_error (stack) && jerry_value_is_array (stack))
            {
                writeln ("Exception backtrace:\n");

                uint length = jerry_get_array_length (stack);

                /* This length should be enough. */
                if (length > 32)
                {
                    length = 32;
                }

                for (uint i = 0; i < length; ++i)
                {
                    jerry_value_t item_val = jerry_get_property_by_index (stack, i);

                    if (!jerry_value_is_error (item_val) && jerry_value_is_string (item_val))
                    {
                        jerry_size_t str_size = jerry_get_string_size (item_val);
                        string error_txt;
                        error_txt.length = str_size;

                        if (str_size >= 256)
                        {
                            printf ("%3u: [Backtrace string too long]\n", i);
                        }
                        else
                        {
                            jerry_size_t string_end = jerry_string_to_char_buffer (item_val, cast(ubyte*)error_txt.ptr, str_size);
                            //assert (string_end == str_size);
                            //err_str_buf[string_end] = 0;

                            //printf ("%3u: %s\n", i, err_str_buf.ptr);
                            writeln(error_txt);
                        }
                        //print_value(item_val);
                    }

                    jerry_release_value (item_val);
                }
            }
        }

        jerry_release_value (name);
        jerry_release_value (message);
        jerry_release_value (stack);
        jerry_release_value (exists);
    }

    //jerry_release_value (err_type);
    jerry_release_value (throwable);
}

void JS_InitVM()
{
    jerry_init (jerry_init_flag_t.JERRY_INIT_MEM_STATS);

    JS_RegisterCFunction(&js_degToRad, "degToRad");
    JS_RegisterCFunction(&js_radToDeg, "radToDeg");
    JS_RegisterCFunction(&js_distanceBetweenPoints, "distanceBetweenPoints");

    /** system */
    JS_RegisterCFunction(&jerryx_handler_print,  "print"          );
    JS_RegisterCFunction(&jerryx_handler_gc,     "gc"             );
    JS_RegisterCFunction(&jerryx_handler_assert, "assert"         );
    JS_RegisterCFunction(&js_deltaTime,          "deltaTime"      );
    JS_RegisterCFunction(&js_windowGetWidth,     "windowGetWidth" );
    JS_RegisterCFunction(&js_windowGetHeight,    "windowGetHeight");

    /** map */
    RegisterMapJSFunctions();

    /** base object */
    RegisterObjectJSFunctions();

    /** keyboard */
    RegisterKeyboardJSFunctions();

    /** mouse */
    RegisterMouseJSFunctions();

    /** camera */
    RegisterCameraJSFunctions();

    /** player */
    RegisterPlayerJSFunctions();

    /** weapons */
    RegisterWeaponsAmmoJSFunctions();

    g_ScriptsCount = 0;
}

void JS_StopVM()
{
    jerry_cleanup ();
}

void JS_GC()
{
    jerry_gc();
}
