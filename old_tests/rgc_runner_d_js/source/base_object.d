module base_object;

import std.stdio;
import std.conv;
import std.math;
import core.stdc.stdlib;

import js_util;
import WorldSim3D;

public {
    import object_2d;
    import object_3d;
}
import global;
import util;
import constants;
import sprites;
import doors_keys;
import weapons_ammo;
import map;

extern (C) :

enum TUserVarType
{
    uvtNone = 0,
    uvtNumber,
    uvtBool,
    uvtString,
    uvtVector
}

class TUserVar
{
    TUserVarType type;
    double   dVal;
    bool     bVal;
    string   sVal;
    double[] vVal;

    this(TUserVarType type, double dVal, bool bVal, string sVal, double[] vVal)
    {
        this.type = type;
        this.dVal = dVal;
        this.bVal = bVal;
        this.sVal = sVal;
        this.vVal = vVal;
    }

    ~this()
    {
        sVal.length = 0;
        vVal.length = 0;
    }
}

class TObject
{
    static TObject[] objects;     /* массив всех объектов в игре */

    TScriptJS* script;           // полный скрипт
    TScriptJS* initCommand;      // команда object.onInit
    TScriptJS* updateCommand;    // команда object.onUpdate
    //TScriptJS* destroyCommand;   // команда object.onDestroy

    int    curFrame;           // текущий кадр
    //float  animSpeed;          // скорость анимации

    string     name;              /* имя                    */
    int        id;                /* идентификатор объекта  */
    bool       isVisible;         /* видимость объекта      */
    TUserVar[string] userVars;    /* пользовательские значения */

    this()
    {
        isVisible = true;
        ++objects.length;
        objects[$ - 1] = this;
    }

    ~this()
    {
        clearVars();

        /* уменьшаем список доступных объектов */
        //writefln("objects length before = %d", objects.length);
        if (objects.length > 0)
        {
            bool founded;
            for (int i = 0; (i < objects.length) && (!founded); ++i)
            {
                if (objects[i] !is null && objects[i] == this)
                {
                    founded = true;
                    g_ObjectsIdTable[objects[i].id] = null;
                    for (int j = i; j < objects.length - 1; ++j)
                        objects[j] = objects[j + 1];
                    --objects.length;
                }
            }
        }
        //writefln("objects length = %d", objects.length);

        /*
        if (script)         script.destroy();
        if (initCommand)    initCommand.destroy();
        if (updateCommand)  updateCommand.destroy();
        if (destroyCommand)
        {
            destroyCommand.run();
            destroyCommand.destroy();
        }
        */
        //if (destroyCommand) destroyCommand.run();
    }

    void init(string className = "", string objectName = "")
    {
        if (script != null)
        {
            g_MutexNodes.lock_nothrow();
            g_ObjectId = id;

            // заворачиваем код в вид:
            // function Object0(){ текст_скрипта }
            // var object0 = new Object0();
            if (className.length  == 0 || className  == "") className  = "Object" ~ to!string(script.id);
            if (objectName.length == 0 || objectName == "") objectName = "object" ~ to!string(script.id);

            script.text = "function " ~ className ~ "(){" ~ script.text ~ "} ";
            script.text ~= "var " ~ objectName ~ " = new " ~ className ~ "();";
            //script.compile();
            script.run();

            // запоминаем команды для инициализации и обновления объекта
            //if (initCommand)
            //    initCommand.destroy();
            initCommand = new TScriptJS();
            initCommand.fileName = objectName ~ " init command";
            initCommand.text = objectName ~ ".onInit();";
            //initCommand.compile();
            initCommand.run();

            //if (updateCommand)
            //    updateCommand.destroy();
            updateCommand = new TScriptJS();
            updateCommand.fileName = objectName ~ " update command";
            updateCommand.text = objectName ~ ".onUpdate();";
            updateCommand.compile();

            //if (destroyCommand)
            //    destroyCommand.destroy();
            /*destroyCommand = new TScriptJS();
            destroyCommand.fileName = objectName ~ " destroy command";
            destroyCommand.text = objectName ~ ".onDestroy();
                                  delete(" ~ objectName ~ ");";*/

            g_MutexNodes.unlock_nothrow();
        }
    }

    void update()
    {
        if (!isVisible)
            return;

        if (updateCommand != null)
        {
            //g_MutexNodes.lock_nothrow();
            g_ObjectId = this.id;
            updateCommand.run();
            //g_MutexNodes.unlock_nothrow();
        }
    }

    /* управление кадрами */
    abstract void setFrame(int frame);
    abstract int getFrame();
    abstract int getFramesCount();

    /* управление коллизионной коробкой объекта */
    void enableCollision() {}
    void disableCollision() {}
    bool isEnabledCollision() { return false; }

    /* установить видимость объекта */
    void setVisibility(bool isVisible)
    {
        this.isVisible = isVisible;
    }

    /* adders */
    void addVar(string key, double val)
    {
        this.userVars.remove(key);
        this.userVars[key] = new TUserVar(TUserVarType.uvtNumber, val, bool.init, string.init, []);

        this.userVars = this.userVars.rehash();
    }
    void addVar(string key, bool val)
    {
        this.userVars.remove(key);
        this.userVars[key] = new TUserVar(TUserVarType.uvtBool, double.init, val, string.init, []);

        this.userVars = this.userVars.rehash();
    }
    void addVar(string key, string val)
    {
        this.userVars.remove(key);
        this.userVars[key] = new TUserVar(TUserVarType.uvtString, double.init, bool.init, val, []);

        this.userVars = this.userVars.rehash();
    }
    void addVar(string key, double[] val)
    {
        this.userVars.remove(key);
        this.userVars[key] = new TUserVar(TUserVarType.uvtVector, double.init, bool.init, string.init, val);

        this.userVars = this.userVars.rehash();
    }

    /* setters */
    void setVar(string key, double val)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null && uVar.type == TUserVarType.uvtNumber)
        {
            uVar.dVal = val;
            uVar.bVal = bool.init;
            uVar.sVal = string.init;
            uVar.vVal = [];
        }
    }
    void setVar(string key, bool val)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null && uVar.type == TUserVarType.uvtBool)
        {
            uVar.dVal = double.init;
            uVar.bVal = val;
            uVar.sVal = string.init;
            uVar.vVal = [];
        }
    }
    void setVar(string key, string val)
    {
        TUserVar uVar = this.userVars.get(key, null);

        if (uVar !is null && uVar.type == TUserVarType.uvtString)
        {
            uVar.dVal = double.init;
            uVar.bVal = bool.init;
            uVar.sVal = val;
            uVar.vVal = [];
        }
    }
    void setVar(string key, double[] val)
    {
        TUserVar uVar = this.userVars.get(key, null);

        if (uVar !is null && uVar.type == TUserVarType.uvtVector)
        {
            uVar.dVal = double.init;
            uVar.bVal = bool.init;
            uVar.sVal = string.init;
            uVar.vVal = val;
        }
    }

    /* getters */
    double getVarNumber(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null/* && uVar.type == TUserVarType.uvtNumber*/)
            return uVar.dVal;

        return double.init;
    }
    bool getVarBool(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null/* && uVar.type == TUserVarType.uvtBool*/)
            return uVar.bVal;

        return bool.init;
    }
    string getVarString(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null/* && uVar.type == TUserVarType.uvtString*/)
            return uVar.sVal;

        return "undefined";
    }
    double[] getVarVector(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null/* && uVar.type == TUserVarType.uvtString*/)
            return uVar.vVal;

        return [double.max, double.max, double.max];
    }

    /* delete userVars value */
    void removeVar(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null)
        {
            this.userVars[key].destroy();
            this.userVars[key] = null;
            this.userVars.remove(key);
            this.userVars = this.userVars.rehash();
        }
    }

    /* clear userVars */
    void clearVars()
    {
        this.userVars.clear();
    }

    /* check type */
    bool isVarNumber(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null && uVar.type == TUserVarType.uvtNumber)
            return true;

        return false;
    }
    bool isVarBool(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null && uVar.type == TUserVarType.uvtBool)
            return true;

        return false;
    }
    bool isVarString(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null && uVar.type == TUserVarType.uvtString)
            return true;

        return false;
    }
    bool isVarVector(string key)
    {
        TUserVar uVar = this.userVars.get(key, null);
        if (uVar !is null && uVar.type == TUserVarType.uvtVector)
            return true;

        return false;
    }
}

/* указатель на обрабатываемый скриптом на данный момент объект */
__gshared uint g_ObjectId;
__gshared TObject[] g_ObjectsIdTable;   // таблица идентификаторов объектов
__gshared TObject[] g_ObjectsForInit;   // таблица объектов,которые надо инициализировать
__gshared TObject[] g_ObjectsForDelete; // таблица объектов,которые надо удалить


/** заполнение таблицы идентификаторов */
void FillObjectsIdTable();
/** Инициализация выделенных объектов */
void InitObjects();
/** удаление объектов, помеченных на удаление */
void DeleteObjects();
void RegisterObjectJSFunctions();


// IMPLEMENTATION


void FillObjectsIdTable()
{
    g_ObjectsIdTable = new TObject[ushort.max];

    int id;
    for (int i = 0; i < TObject.objects.length; ++i)
    {
        if (TObject.objects[i] is null)
            continue;

        id = TObject.objects[i].id;
        while (id == 0 || g_ObjectsIdTable[id] !is null)
        {
            id = rand % (ushort.max);
        }
        TObject.objects[i].id = id;
        g_ObjectsIdTable[id] = TObject.objects[i];
    }
}

void InitObjects()
{
    if (g_ObjectsForInit.length == 0)
        return;

    foreach (object; g_ObjectsForInit)
    {
        if (object !is null)
        {
            object.init();
        }
    }

    g_ObjectsForInit.length = 0;
}

void DeleteObjects()
{
    if (g_ObjectsForDelete.length == 0)
        return;

    TObject obj;
    for (int i = 0; i < g_ObjectsForDelete.length; ++i)
    {
        obj = g_ObjectsForDelete[i];
        if (obj is null)
            continue;

        // ищем чем объект является
        bool founded;

        // это спрайт?
        if (cast(TSpriteNode)obj && g_SpriteNodes.length > 0)
        {
            //writeln("try to clear sprite node");
            TSpriteNode sprite;
            for (int j = 0; j < g_SpriteNodes.length && !founded; ++j)
            {
                sprite = g_SpriteNodes[j];
                if (sprite !is null && sprite == obj)
                {
                    founded = true;
                    for (int k = j; k < g_SpriteNodes.length - 1; ++k)
                    {
                        g_SpriteNodes[k] = g_SpriteNodes[k + 1];
                    }
                    --g_SpriteNodes.length;

                    //writeln("try to delete sprite node");
                    sprite.destroy();
                    //writeln("deleted");
                }
            }
        }
        if (founded)
            continue;

        // это дверь?
        if (cast(TDoorNode)obj && g_DoorNodes.length > 0)
        {
            //writeln("try to clear door node");
            TDoorNode door;
            for (int j = 0; j < g_DoorNodes.length && !founded; ++j)
            {
                door = g_DoorNodes[j];
                if (door !is null && door == obj)
                {
                    founded = true;
                    for (int k = j; k < g_DoorNodes.length - 1; ++k)
                    {
                        g_DoorNodes[k] = g_DoorNodes[k + 1];
                    }
                    --g_DoorNodes.length;

                    //writeln("try to delete door node");
                    door.destroy();
                    //writeln("deleted");
                }
            }
        }
        if (founded)
            continue;

        // это ключ?
        if (cast(TKeyNode)obj && g_KeyNodes.length > 0)
        {
            //writeln("try to clear key node");
            TKeyNode key;
            for (int j = 0; j < g_KeyNodes.length && !founded; ++j)
            {
                key = g_KeyNodes[j];
                if (key !is null && key == obj)
                {
                    founded = true;
                    for (int k = j; k < g_KeyNodes.length - 1; ++k)
                    {
                        g_KeyNodes[k] = g_KeyNodes[k + 1];
                    }
                    --g_KeyNodes.length;

                    //writeln("try to delete key node");
                    key.destroy();
                    //writeln("deleted");
                }
            }
        }
        if (founded)
            continue;

        // это оружие?
        if (cast(TWeaponItemNode)obj && g_WeaponItemNodes.length > 0)
        {
            //writeln("try to clear key node");
            TWeaponItemNode weaponItem;
            for (int j = 0; j < g_WeaponItemNodes.length && !founded; ++j)
            {
                weaponItem = g_WeaponItemNodes[j];
                if (weaponItem !is null && weaponItem == obj)
                {
                    founded = true;
                    for (int k = j; k < g_WeaponItemNodes.length - 1; ++k)
                    {
                        g_WeaponItemNodes[k] = g_WeaponItemNodes[k + 1];
                    }
                    --g_WeaponItemNodes.length;

                    //writeln("try to delete key node");
                    weaponItem.destroy();
                    //writeln("deleted");
                }
            }
        }
        if (founded)
            continue;

        // это пуля?
        if (cast(TBulletNode)obj && g_BulletNodes.length > 0)
        {
            //writeln("try to clear key node");
            TBulletNode bulletNode;
            for (int j = 0; j < g_BulletNodes.length && !founded; ++j)
            {
                bulletNode = g_BulletNodes[j];
                if (bulletNode !is null && bulletNode == obj)
                {
                    founded = true;
                    for (int k = j; k < g_BulletNodes.length - 1; ++k)
                    {
                        g_BulletNodes[k] = g_BulletNodes[k + 1];
                    }
                    --g_BulletNodes.length;

                    //writeln("try to delete key node");
                    bulletNode.destroy();
                    //writeln("deleted");
                }
            }
        }
        if (founded)
            continue;

        // что-то непонятное. Тоже удалить
        obj.destroy();
    }

    g_ObjectsForDelete.length = 0;
}


/**
 *  JerryScript
 **/


/** работа с объектами */

static jerry_value_t js_objectCreate
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];
    if (!jerry_value_is_string(arg0))
        return jerry_create_number(0);

    wVector3f pos;
    string    name;
    char[]    buffer;
    TObject   object;

    /* если заданы координаты после имени объекта, то подтянуть их */
    if (args_cnt > 3)
    {
        jerry_value_t arg1 = args_p[1];
        jerry_value_t arg2 = args_p[2];
        jerry_value_t arg3 = args_p[3];

        if (jerry_value_is_number(arg1) &&
            jerry_value_is_number(arg2) &&
            jerry_value_is_number(arg3))
        {
            pos.x = jerry_get_number_value(arg1) * BLOCK_SIZE;
            pos.y = jerry_get_number_value(arg2) * BLOCK_SIZE;
            pos.z = jerry_get_number_value(arg3) * BLOCK_SIZE;
        }
    }

    jerry_size_t size = jerry_get_string_size(arg0);
    buffer.length = size;
    jerry_size_t readed = jerry_string_to_char_buffer(arg0, cast(ubyte*)buffer.ptr, size);
    name = to!string(buffer);
    buffer.length = 0;

    writeln("start find for '" ~ name ~ "'");

    /* ищем по имени объект в списке доступных загруженных */
    bool founded;

    /* пули */
    TBulletInMem* bullet;
    for (int i = 0; i < g_Bullets.length; ++i)
    {
        bullet = &(g_Bullets[i]);
        if (bullet == null)
            continue;

        if (name == bullet.name)
        {
            writeln("founded");
            founded = true;

            wNode* billboard;

            /* если текстуры не подгружены, то пришло время это сделать */
            if (bullet.frames.length == 0)
                LoadBulletFrames(bullet, i);

            if (bullet.frames.length > 0)
            {
                billboard = wBillBoardCreate(wVECTOR3f_ZERO, wVector2f(BLOCK_SIZE, BLOCK_SIZE));
                wMaterial* material = wNodeGetMaterial(billboard, 0);
                wMaterialSetTexture(material, 0, bullet.frames[0]);
                MaterialSetOldSchool(material);
                wMaterialSetType(material, wMaterialTypes.wMT_TRANSPARENT_ALPHA_CHANNEL);
            }
            /* текстуры не подгрузились - нечего тогда и создавать */
            else
            {
                return jerry_create_number(0);
            }

            wMesh* mesh = CreateInvisibleBlock("bullet node " ~ to!string(TObject.objects.length), 1, 1);
            wNode* solidBox = wNodeCreateFromMesh(mesh);
            wNodeSetPosition(solidBox, pos);
            wNodeSetParent(solidBox, g_SolidObjects);
            wNodeSetDebugMode(solidBox, wDebugMode.wDM_BBOX);
            wNodeSetDebugDataVisible(solidBox, true);

            TBulletNode bulletNode = new TBulletNode(bullet, solidBox, billboard);
            ++g_BulletNodes.length;
            g_BulletNodes[$ - 1] = bulletNode;

            // привязываем внутренний id к ноде на стороне движка
            if (billboard)
                wNodeSetUserData(billboard, &(bulletNode.id));

            object = bulletNode;
        }
    }



    /* если объект найден, то подыскиваем ему место в таблице идентификаторов и назначаем id */
    if (founded)
    {
        for (int i = 0; ((i < g_ObjectsIdTable.length) && (object.id == 0)); ++i)
        {
            if (g_ObjectsIdTable[i] is null)
            {
                g_ObjectsIdTable[i] = object;
                object.id = i;
            }
        }

        ++g_ObjectsForInit.length;
        g_ObjectsForInit[$ - 1] = object;

        return jerry_create_number(object.id);
    }

    return jerry_create_number(0);
}

static jerry_value_t js_objectSetEnable
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_boolean(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        bool isVisible = (jerry_get_boolean_value(arg1) != 0) ? true : false;
        obj.setVisibility(isVisible);
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectIsEnable
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_boolean(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_boolean(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_boolean(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        return jerry_create_boolean(obj.isVisible);
    }

    return jerry_create_boolean(0);
}
static jerry_value_t js_objectDestroy
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        obj.setVisibility(false);
        ++g_ObjectsForDelete.length;
        g_ObjectsForDelete[$ - 1] = obj;
    }

    return jerry_create_undefined();
}

/* СЕТТЕРЫ */
/* POSITION */
static jerry_value_t js_objectSetPosition
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];
    jerry_value_t arg3;
    if (args_cnt >= 4)
        arg3 = args_p[3];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1) ||
        !jerry_value_is_number(arg2) || (args_cnt >= 4 && !jerry_value_is_number(arg3)))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        if (cast(TObject3D)obj)
        {
            wNode* node = (cast(TObject3D)obj).node;
            if (node != null)
            {
                wVector3f position = { jerry_get_number_value(arg1), jerry_get_number_value(arg2), jerry_get_number_value(arg3) };
                wNodeSetPosition(node, position);
            }
        }
        else
            if (cast(TObject2D)obj)
            {
                (cast(TObject2D)obj).position = wVector2f(jerry_get_number_value(arg1), jerry_get_number_value(arg2));
            }
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectSetPositionX
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        if (cast(TObject3D)obj)
        {
            wNode* node = (cast(TObject3D)obj).node;
            if (node != null)
            {
                wVector3f position = wNodeGetPosition(node);
                position.x = jerry_get_number_value(arg1);
                wNodeSetPosition(node, position);
            }
        }
        else
            if (cast(TObject2D)obj)
            {
                (cast(TObject2D)obj).position.x = jerry_get_number_value(arg1);
            }
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectSetPositionY
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        if (cast(TObject3D)obj)
        {
            wNode* node = (cast(TObject3D)obj).node;
            if (node != null)
            {
                wVector3f position = wNodeGetPosition(node);
                position.y = jerry_get_number_value(arg1);
                wNodeSetPosition(node, position);
            }
        }
        else
            if (cast(TObject2D)obj)
            {
                (cast(TObject2D)obj).position.y = jerry_get_number_value(arg1);
            }
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectSetPositionZ
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        if (cast(TObject3D)obj)
        {
            wNode* node = (cast(TObject3D)obj).node;
            if (node != null)
            {
                wVector3f position = wNodeGetPosition(node);
                position.z = jerry_get_number_value(arg1);
                wNodeSetPosition(node, position);
            }
        }
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectMove
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];
    jerry_value_t arg3;
    if (args_cnt >= 4)
        arg3 = args_p[3];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1) ||
        !jerry_value_is_number(arg2) || (args_cnt >= 4 && !jerry_value_is_number(arg3)))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        if (cast(TObject3D)obj)
        {
            wNode* node = (cast(TObject3D)obj).node;
            if (node != null)
            {
                wVector3f position = { jerry_get_number_value(arg1), jerry_get_number_value(arg2), jerry_get_number_value(arg3) };
                wNodeMove(node, position);
            }
        }
        else
            if (cast(TObject2D)obj)
            {
                (cast(TObject2D)obj).position.x += jerry_get_number_value(arg1);
                (cast(TObject2D)obj).position.y += jerry_get_number_value(arg2);
            }
    }

    return jerry_create_undefined();
}


/* SCALE */
static jerry_value_t js_objectSetScale
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];
    jerry_value_t arg3;
    if (args_cnt >= 4)
        arg3 = args_p[3];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1) ||
        !jerry_value_is_number(arg2) || (args_cnt >= 4 && !jerry_value_is_number(arg3)))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        if (cast(TObject3D)obj)
        {
            wNode* node = (cast(TObject3D)obj).node;
            if (node != null)
            {
                wVector3f scale = { jerry_get_number_value(arg1), jerry_get_number_value(arg2), jerry_get_number_value(arg3) };
                wNodeSetScale(node, scale);
            }
        }
        else
            if (cast(TObject2D)obj)
            {
                wVector2f scale = wVector2f(jerry_get_number_value(arg1), jerry_get_number_value(arg2));
                (cast(TObject2D)obj).scale = scale;
            }
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectSetScaleX
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        if (cast(TObject3D)obj)
        {
            wNode* node = (cast(TObject3D)obj).node;
            if (node != null)
            {
                wVector3f scale = wNodeGetPosition(node);
                scale.x = jerry_get_number_value(arg1);
                wNodeSetScale(node, scale);
            }
        }
        else
            if (cast(TObject2D)obj)
            {
                (cast(TObject2D)obj).scale.x = jerry_get_number_value(arg1);
            }
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectSetScaleY
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        if (cast(TObject3D)obj)
        {
            wNode* node = (cast(TObject3D)obj).node;
            if (node != null)
            {
                wVector3f scale = wNodeGetScale(node);
                scale.y = jerry_get_number_value(arg1);
                wNodeSetScale(node, scale);
            }
        }
        else
            if (cast(TObject2D)obj)
            {
                (cast(TObject2D)obj).scale.y = jerry_get_number_value(arg1);
            }
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectSetScaleZ
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        if (cast(TObject3D)obj)
        {
            wNode* node = (cast(TObject3D)obj).node;
            if (node != null)
            {
                wVector3f scale = wNodeGetScale(node);
                scale.z = jerry_get_number_value(arg1);
                wNodeSetScale(node, scale);
            }
        }
    }

    return jerry_create_undefined();
}


/* ROTATION */
static jerry_value_t js_objectSetRotation
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2;
    jerry_value_t arg3;
    if (args_cnt >= 4)
    {
        arg2 = args_p[2];
        arg3 = args_p[3];
    }

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1) ||
        (args_cnt >= 4 &&(!jerry_value_is_number(arg2) || !jerry_value_is_number(arg3))))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f rot = { jerry_get_number_value(arg1), jerry_get_number_value(arg2), jerry_get_number_value(arg3) };
            wNodeSetRotation(node, rot);
        }
    }
    else
        if (cast(TObject2D)obj)
        {
            (cast(TObject2D)obj).rotation = jerry_get_number_value(arg1);
        }

    return jerry_create_undefined();
}
static jerry_value_t js_objectSetRotationX
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f rot = wNodeGetRotation(node);
            rot.x = jerry_get_number_value(arg1);
            wNodeSetRotation(node, rot);
        }
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectSetRotationY
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f rot = wNodeGetRotation(node);
            rot.y = jerry_get_number_value(arg1);
            wNodeSetRotation(node, rot);
        }
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectSetRotationZ
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f rot = wNodeGetRotation(node);
            rot.z = jerry_get_number_value(arg1);
            wNodeSetRotation(node, rot);
        }
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectRotate
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2;
    jerry_value_t arg3;
    if (args_cnt >= 4)
    {
        arg2 = args_p[2];
        arg3 = args_p[3];
    }

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1) ||
        (args_cnt >= 4 &&(!jerry_value_is_number(arg2) || !jerry_value_is_number(arg3))))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f rot = { jerry_get_number_value(arg1), jerry_get_number_value(arg2), jerry_get_number_value(arg3) };
            wNodeTurn(node, rot);
        }
    }
    else
        if (cast(TObject2D)obj)
        {
            (cast(TObject2D)obj).rotation += jerry_get_number_value(arg1);
        }

    return jerry_create_undefined();
}


/* ГЕТТЕРЫ */
static jerry_value_t js_objectGetId
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    return jerry_create_number(g_ObjectId);
}

/* POSITION */
static jerry_value_t js_objectGetPosition
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f position = wNodeGetPosition(node);
            double x = position.x;
            double y = position.y;
            double z = position.z;

            jerry_value_t array = jerry_create_typedarray (jerry_typedarray_type_t.JERRY_TYPEDARRAY_FLOAT64, 3);

            jerry_length_t byteLength;
            jerry_length_t byteOffset;
            jerry_value_t arrayBuffer = jerry_get_typedarray_buffer (array, &byteOffset, &byteLength);

            jerry_arraybuffer_write(arrayBuffer,  0, cast(ubyte*)&x, double.sizeof);
            jerry_arraybuffer_write(arrayBuffer,  8, cast(ubyte*)&y, double.sizeof);
            jerry_arraybuffer_write(arrayBuffer, 16, cast(ubyte*)&z, double.sizeof);

            jerry_release_value(arrayBuffer);

            return array;
        }
    }
    else
        if (cast(TObject2D)obj)
        {
            double x = (cast(TObject2D)obj).position.x;
            double y = (cast(TObject2D)obj).position.y;

            jerry_value_t array = jerry_create_typedarray (jerry_typedarray_type_t.JERRY_TYPEDARRAY_FLOAT64, 2);

            jerry_length_t byteLength;
            jerry_length_t byteOffset;
            jerry_value_t arrayBuffer = jerry_get_typedarray_buffer (array, &byteOffset, &byteLength);

            jerry_arraybuffer_write(arrayBuffer,  0, cast(ubyte*)&x, double.sizeof);
            jerry_arraybuffer_write(arrayBuffer,  8, cast(ubyte*)&y, double.sizeof);

            jerry_release_value(arrayBuffer);

            return array;
        }

    return jerry_create_undefined();
}
static jerry_value_t js_objectGetPositionX
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_number(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f position = wNodeGetPosition(node);
            return jerry_create_number(position.x);
        }
    }
    else
        if (cast(TObject2D)obj)
        {
            return jerry_create_number((cast(TObject2D)obj).position.x);
        }

    return jerry_create_number(0);
}
static jerry_value_t js_objectGetPositionY
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_number(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f position = wNodeGetPosition(node);
            return jerry_create_number(position.y);
        }
    }
    else
        if (cast(TObject2D)obj)
        {
            return jerry_create_number((cast(TObject2D)obj).position.y);
        }

    return jerry_create_number(0);
}
static jerry_value_t js_objectGetPositionZ
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_number(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f position = wNodeGetPosition(node);
            return jerry_create_number(position.z);
        }
    }

    return jerry_create_number(0);
}


/* SCALE */
static jerry_value_t js_objectGetScale
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f scale = wNodeGetScale(node);
            double x = scale.x;
            double y = scale.y;
            double z = scale.z;

            jerry_value_t array = jerry_create_typedarray (jerry_typedarray_type_t.JERRY_TYPEDARRAY_FLOAT64, 3);

            jerry_length_t byteLength;
            jerry_length_t byteOffset;
            jerry_value_t arrayBuffer = jerry_get_typedarray_buffer (array, &byteOffset, &byteLength);

            jerry_arraybuffer_write(arrayBuffer,  0, cast(ubyte*)&x, double.sizeof);
            jerry_arraybuffer_write(arrayBuffer,  8, cast(ubyte*)&y, double.sizeof);
            jerry_arraybuffer_write(arrayBuffer, 16, cast(ubyte*)&z, double.sizeof);

            jerry_release_value(arrayBuffer);

            return array;
        }
    }
    else
        if (cast(TObject2D)obj)
        {
            double x = (cast(TObject2D)obj).scale.x;
            double y = (cast(TObject2D)obj).scale.y;

            jerry_value_t array = jerry_create_typedarray (jerry_typedarray_type_t.JERRY_TYPEDARRAY_FLOAT64, 2);

            jerry_length_t byteLength;
            jerry_length_t byteOffset;
            jerry_value_t arrayBuffer = jerry_get_typedarray_buffer (array, &byteOffset, &byteLength);

            jerry_arraybuffer_write(arrayBuffer,  0, cast(ubyte*)&x, double.sizeof);
            jerry_arraybuffer_write(arrayBuffer,  8, cast(ubyte*)&y, double.sizeof);

            jerry_release_value(arrayBuffer);

            return array;
        }

    return jerry_create_undefined();
}
static jerry_value_t js_objectGetScaleX
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_number(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f scale = wNodeGetScale(node);
            return jerry_create_number(scale.x);
        }
    }
    else
        if (cast(TObject2D)obj)
        {
            return jerry_create_number((cast(TObject2D)obj).scale.x);
        }

    return jerry_create_number(0);
}
static jerry_value_t js_objectGetScaleY
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_number(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f scale = wNodeGetScale(node);
            return jerry_create_number(scale.y);
        }
    }
    else
        if (cast(TObject2D)obj)
        {
            return jerry_create_number((cast(TObject2D)obj).scale.y);
        }

    return jerry_create_number(0);
}
static jerry_value_t js_objectGetScaleZ
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_number(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f scale = wNodeGetScale(node);
            return jerry_create_number(scale.z);
        }
    }

    return jerry_create_number(0);
}


/* SIZE */
static jerry_value_t js_objectGetSize
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f min;
            wVector3f max;
            wNodeGetBoundingBox(node, &min, &max);

            double x = abs(max.x - min.x);
            double y = abs(max.y - min.y);
            double z = abs(max.z - min.z);

            jerry_value_t array = jerry_create_typedarray (jerry_typedarray_type_t.JERRY_TYPEDARRAY_FLOAT64, 3);

            jerry_length_t byteLength;
            jerry_length_t byteOffset;
            jerry_value_t arrayBuffer = jerry_get_typedarray_buffer (array, &byteOffset, &byteLength);

            jerry_arraybuffer_write(arrayBuffer,  0, cast(ubyte*)&x, double.sizeof);
            jerry_arraybuffer_write(arrayBuffer,  8, cast(ubyte*)&y, double.sizeof);
            jerry_arraybuffer_write(arrayBuffer, 16, cast(ubyte*)&z, double.sizeof);

            jerry_release_value(arrayBuffer);

            return array;
        }
    }
    else
        if (cast(TObject2D)obj)
        {
            double x, y;

            TObject2D object = cast(TObject2D)obj;
            if (object.curTexture != null)
            {
                wVector2u size;
                uint pitch;
                wColorFormat format;
                wTextureGetInformation(object.curTexture, &size, &pitch, &format);

                x = size.x;
                y = size.y;
            }

            jerry_value_t array = jerry_create_typedarray (jerry_typedarray_type_t.JERRY_TYPEDARRAY_FLOAT64, 2);

            jerry_length_t byteLength;
            jerry_length_t byteOffset;
            jerry_value_t arrayBuffer = jerry_get_typedarray_buffer (array, &byteOffset, &byteLength);

            jerry_arraybuffer_write(arrayBuffer,  0, cast(ubyte*)&x, double.sizeof);
            jerry_arraybuffer_write(arrayBuffer,  8, cast(ubyte*)&y, double.sizeof);

            jerry_release_value(arrayBuffer);

            return array;
        }

    return jerry_create_undefined();
}
static jerry_value_t js_objectGetSizeX
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_number(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f min;
            wVector3f max;
            wNodeGetBoundingBox(node, &min, &max);

            return jerry_create_number(abs(max.x - min.x));
        }
    }
    else
        if (cast(TObject2D)obj)
        {
            TObject2D object = cast(TObject2D)obj;
            if (object.curTexture != null)
            {
                wVector2u size;
                uint pitch;
                wColorFormat format;
                wTextureGetInformation(object.curTexture, &size, &pitch, &format);

                return jerry_create_number(size.x);
            }
        }

    return jerry_create_number(0);
}
static jerry_value_t js_objectGetSizeY
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_number(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f min;
            wVector3f max;
            wNodeGetBoundingBox(node, &min, &max);

            return jerry_create_number(abs(max.y - min.y));
        }
    }
    else
        if (cast(TObject2D)obj)
        {
            TObject2D object = cast(TObject2D)obj;
            if (object.curTexture != null)
            {
                wVector2u size;
                uint pitch;
                wColorFormat format;
                wTextureGetInformation(object.curTexture, &size, &pitch, &format);

                return jerry_create_number(size.y);
            }
        }

    return jerry_create_number(0);
}
static jerry_value_t js_objectGetSizeZ
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_number(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f min;
            wVector3f max;
            wNodeGetBoundingBox(node, &min, &max);

            return jerry_create_number(abs(max.z - min.z));
        }
    }

    return jerry_create_number(0);
}


/* ROTATION */
static jerry_value_t js_objectGetRotation
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f rot = wNodeGetRotation(node);
            double x = rot.x;
            double y = rot.y;
            double z = rot.z;

            jerry_value_t array = jerry_create_typedarray (jerry_typedarray_type_t.JERRY_TYPEDARRAY_FLOAT64, 3);

            jerry_length_t byteLength;
            jerry_length_t byteOffset;
            jerry_value_t arrayBuffer = jerry_get_typedarray_buffer (array, &byteOffset, &byteLength);

            jerry_arraybuffer_write(arrayBuffer,  0, cast(ubyte*)&x, double.sizeof);
            jerry_arraybuffer_write(arrayBuffer,  8, cast(ubyte*)&y, double.sizeof);
            jerry_arraybuffer_write(arrayBuffer, 16, cast(ubyte*)&z, double.sizeof);

            jerry_release_value(arrayBuffer);

            return array;
        }
    }
    else
        if (cast(TObject2D)obj)
        {
            return jerry_create_number((cast(TObject2D)obj).rotation);
        }

    return jerry_create_undefined();
}
static jerry_value_t js_objectGetRotationX
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number_nan();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number_nan();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_number_nan();

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f rot = wNodeGetRotation(node);
            jerry_create_number(rot.x);
        }
    }

    return jerry_create_number_nan();
}
static jerry_value_t js_objectGetRotationY
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number_nan();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number_nan();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_number_nan();

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f rot = wNodeGetRotation(node);
            jerry_create_number(rot.y);
        }
    }

    return jerry_create_number_nan();
}
static jerry_value_t js_objectGetRotationZ
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number_nan();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number_nan();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_number_nan();

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            wVector3f rot = wNodeGetRotation(node);
            jerry_create_number(rot.z);
        }
    }

    return jerry_create_number_nan();
}


/* USERDATA */

/* adders */
static jerry_value_t js_objectAddVarNumber
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1) || !jerry_value_is_number(arg2))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key;
        char[] buffer;
        double dVal = jerry_get_number_value(arg2);

        jerry_size_t size = jerry_get_string_size(arg1);
        buffer.length = size;
        jerry_size_t readed = jerry_string_to_char_buffer(arg1, cast(ubyte*)buffer.ptr, size);
        key = to!string(buffer);

        obj.addVar(key, dVal);
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectAddVarBool
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1) || !jerry_value_is_boolean(arg2))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key;
        char[] buffer;
        bool   bVal = (jerry_get_boolean_value(arg2) != 0);

        jerry_size_t size = jerry_get_string_size(arg1);
        buffer.length = size;
        jerry_size_t readed = jerry_string_to_char_buffer(arg1, cast(ubyte*)buffer.ptr, size);
        key = to!string(buffer);

        obj.addVar(key, bVal);
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectAddVarString
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1) || !jerry_value_is_string(arg2))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key;
        char[] buffer;
        string sVal;

        jerry_size_t size = jerry_get_string_size(arg1);
        buffer.length = size;
        jerry_size_t readed = jerry_string_to_char_buffer(arg1, cast(ubyte*)buffer.ptr, size);
        key = to!string(buffer);
        writeln("key before adding = '" ~ key ~ "'");

        size = jerry_get_string_size(arg1);
        buffer.length = size;
        readed = jerry_string_to_char_buffer(arg1, cast(ubyte*)buffer.ptr, size);
        sVal = to!string(buffer);
        writeln("sVal before adding = '" ~ sVal ~ "'");

        obj.addVar(key, sVal);
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectAddVarVector
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 4)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];
    jerry_value_t arg3 = args_p[3];
    jerry_value_t arg4;
    if (args_cnt >= 5)
        arg4 = args_p[4];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1) ||
        !jerry_value_is_number(arg2) || !jerry_value_is_number(arg3) ||
        (args_cnt >= 5 && !jerry_value_is_number(arg4)))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string  key;
        char[]  buffer;
        double[] vVal;
        if (args_cnt >= 5)
            vVal = [ jerry_get_number_value(arg2),
                     jerry_get_number_value(arg3),
                     jerry_get_number_value(arg4) ];
        else
            vVal = [ jerry_get_number_value(arg2),
                     jerry_get_number_value(arg3) ];

        jerry_size_t size = jerry_get_string_size(arg1);
        buffer.length = size;
        jerry_size_t readed = jerry_string_to_char_buffer(arg1, cast(ubyte*)buffer.ptr, size);
        key = to!string(buffer);

        obj.addVar(key, vVal);
    }

    return jerry_create_undefined();
}

/* setters */
static jerry_value_t js_objectSetVar
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 3)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key;
        char[] buffer;

        jerry_size_t size = jerry_get_string_size(arg1);
        buffer.length = size;
        jerry_size_t readed = jerry_string_to_char_buffer(arg1, cast(ubyte*)buffer.ptr, size);
        key = to!string(buffer);

        TUserVar uVar = obj.userVars.get(key, null);
        if (uVar !is null)
        {
            switch(uVar.type)
            {
                default:
                case TUserVarType.uvtNone :
                    break;

                case TUserVarType.uvtNumber :
                    if (jerry_value_is_number(arg2))
                        obj.setVar(key, jerry_get_number_value(arg2));
                    break;

                case TUserVarType.uvtBool :
                    if (jerry_value_is_boolean(arg2))
                        obj.setVar(key, (jerry_get_boolean_value(arg2) != 0));
                    break;

                case TUserVarType.uvtString :
                    if (jerry_value_is_string(arg2))
                    {
                        size = jerry_get_string_size(arg1);
                        buffer.length = size;
                        readed = jerry_string_to_char_buffer(arg1, cast(ubyte*)buffer.ptr, size);
                        string sVal = to!string(buffer);

                        obj.setVar(key, sVal);
                    }
                    break;

                case TUserVarType.uvtVector :
                    if (args_cnt >= 5)
                    {
                        jerry_value_t arg3 = args_p[3];
                        jerry_value_t arg4 = args_p[4];
                        if (jerry_value_is_number(arg3) && jerry_value_is_number(arg4))
                        {
                            double[] vVal = [ jerry_get_number_value(arg2),
                                              jerry_get_number_value(arg3),
                                              jerry_get_number_value(arg4) ];
                            obj.setVar(key, vVal);
                        }
                    }
                    break;
            }
        }
    }

    return jerry_create_undefined();
}

/* getters */
static jerry_value_t js_objectGetVar
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key;
        char[] buffer;

        jerry_size_t size = jerry_get_string_size(arg1);
        buffer.length = size;
        jerry_size_t readed = jerry_string_to_char_buffer(arg1, cast(ubyte*)buffer.ptr, size);
        key = to!string(buffer);

        TUserVar uVar = obj.userVars.get(key, null);
        if (uVar !is null)
        {
            switch(uVar.type)
            {
                default:
                case TUserVarType.uvtNone :
                    return jerry_create_undefined();

                case TUserVarType.uvtNumber :
                    return jerry_create_number(obj.getVarNumber(key));

                case TUserVarType.uvtBool :
                    return jerry_create_boolean(obj.getVarBool(key));

                case TUserVarType.uvtString :
                    string s = obj.getVarString(key);
                    return jerry_create_string_sz(cast(jerry_char_t*)s.ptr, s.length);

                case TUserVarType.uvtVector :
                    double[] vVal = obj.getVarVector(key);

                    jerry_value_t array = jerry_create_typedarray (jerry_typedarray_type_t.JERRY_TYPEDARRAY_FLOAT64, vVal.length);

                    jerry_length_t byteLength;
                    jerry_length_t byteOffset;
                    jerry_value_t arrayBuffer = jerry_get_typedarray_buffer (array, &byteOffset, &byteLength);

                    uint sizeOfElement = byteLength / vVal.length;
                    uint offset = 0;
                    for (int i = 0; i < vVal.length; ++i)
                    {
                        jerry_arraybuffer_write(arrayBuffer, offset, cast(ubyte*)&(vVal[i]), double.sizeof);
                        offset += sizeOfElement;
                    }

                    jerry_release_value(arrayBuffer);

                    return array;
            }
        }
    }

    return jerry_create_undefined();
}

/* remove */
static jerry_value_t js_objectRemoveVar
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key;
        char[] buffer;

        jerry_size_t size = jerry_get_string_size(arg1);
        buffer.length = size;
        jerry_size_t readed = jerry_string_to_char_buffer(arg1, cast(ubyte*)buffer.ptr, size);
        key = to!string(buffer);

        obj.removeVar(key);
    }

    return 0;
}

/* clear data */
static jerry_value_t js_objectClearVars
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        obj.clearVars();
    }

    return 0;
}

/* check type */
static jerry_value_t js_objectIsVarNumber
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_boolean(0);

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_boolean(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_boolean(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key;
        char[] buffer;

        jerry_size_t size = jerry_get_string_size(arg1);
        buffer.length = size;
        jerry_size_t readed = jerry_string_to_char_buffer(arg1, cast(ubyte*)buffer.ptr, size);
        key = to!string(buffer);

        return jerry_create_boolean(obj.isVarNumber(key));
    }

    return jerry_create_boolean(0);
}
static jerry_value_t js_objectIsVarBool
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_boolean(0);

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_boolean(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_boolean(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key;
        char[] buffer;

        jerry_size_t size = jerry_get_string_size(arg1);
        buffer.length = size;
        jerry_size_t readed = jerry_string_to_char_buffer(arg1, cast(ubyte*)buffer.ptr, size);
        key = to!string(buffer);

        return jerry_create_boolean(obj.isVarBool(key));
    }

    return jerry_create_boolean(0);
}
static jerry_value_t js_objectIsVarString
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_boolean(0);

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_boolean(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_boolean(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key;
        char[] buffer;

        jerry_size_t size = jerry_get_string_size(arg1);
        buffer.length = size;
        jerry_size_t readed = jerry_string_to_char_buffer(arg1, cast(ubyte*)buffer.ptr, size);
        key = to!string(buffer);

        return jerry_create_boolean(obj.isVarString(key));
    }

    return jerry_create_boolean(0);
}
static jerry_value_t js_objectIsVarVector
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_boolean(0);

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_string(arg1))
        return jerry_create_boolean(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_boolean(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        string key;
        char[] buffer;

        jerry_size_t size = jerry_get_string_size(arg1);
        buffer.length = size;
        jerry_size_t readed = jerry_string_to_char_buffer(arg1, cast(ubyte*)buffer.ptr, size);
        key = to!string(buffer);

        return jerry_create_boolean(obj.isVarVector(key));
    }

    return jerry_create_boolean(0);
}


/* UTIL */
static jerry_value_t js_objectIsInView
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_boolean(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_boolean(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_boolean(0);

    TObject obj = g_ObjectsIdTable[pos];
    if (cast(TObject3D)obj)
    {
        wNode* node = (cast(TObject3D)obj).node;
        if (node != null)
        {
            return jerry_create_boolean(wNodeIsInView(node));
        }
    }

    return jerry_create_boolean(0);
}
static jerry_value_t js_distanceBetweenObjects
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_number_nan();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_number_nan();

    uint pos0 = to!uint(jerry_get_number_value(arg0));
    uint pos1 = to!uint(jerry_get_number_value(arg0));
    if (pos0 >= g_ObjectsIdTable.length || pos1 >= g_ObjectsIdTable.length)
        return jerry_create_number_nan();

    TObject obj0 = g_ObjectsIdTable[pos0];
    TObject obj1 = g_ObjectsIdTable[pos1];
    if (cast(TObject3D)obj0 && cast(TObject3D)obj1)
    {
        wNode* node0 = (cast(TObject3D)obj0).node;
        wNode* node1 = (cast(TObject3D)obj1).node;
        if (node0 != null && node1 != null)
        {
            return jerry_create_number(wNodesGetBetweenDistance(node0, node1));
        }
    }

    return jerry_create_number_nan();
}


/* ANIMATION */
/* setters */
static jerry_value_t js_objectSetFrame
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_number(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        obj.setFrame(to!uint(jerry_get_number_value(arg1)));
    }

    return jerry_create_undefined();
}
/* getters */
static jerry_value_t js_objectGetFrame
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number_nan();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number_nan();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_number_nan();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        return jerry_create_number(obj.getFrame());
    }

    return jerry_create_number_nan();
}
static jerry_value_t js_objectGetFramesCount
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_number_nan();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_number_nan();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_number_nan();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        return jerry_create_number(obj.getFramesCount());
    }

    return jerry_create_number_nan();
}


/* COLLISION */
/* setters */
static jerry_value_t js_objectEnableCollision
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        obj.enableCollision();
    }

    return jerry_create_undefined();
}
static jerry_value_t js_objectDisableCollision
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos >= g_ObjectsIdTable.length)
        return jerry_create_undefined();

    TObject obj = g_ObjectsIdTable[pos];
    if (obj !is null)
    {
        obj.disableCollision();
    }

    return jerry_create_undefined();
}

/* getters */
static jerry_value_t js_objectGetByRay
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 6)
        return jerry_create_number(0);

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];
    jerry_value_t arg2 = args_p[2];
    jerry_value_t arg3 = args_p[3];
    jerry_value_t arg4 = args_p[4];
    jerry_value_t arg5 = args_p[5];

    if (!jerry_value_is_number(arg0) ||
        !jerry_value_is_number(arg1) ||
        !jerry_value_is_number(arg2) ||
        !jerry_value_is_number(arg3) ||
        !jerry_value_is_number(arg4) ||
        !jerry_value_is_number(arg5))
        return jerry_create_number(0);

    double x1 = jerry_get_number_value(arg0);
    double y1 = jerry_get_number_value(arg1);
    double z1 = jerry_get_number_value(arg2);
    double x2 = jerry_get_number_value(arg3);
    double y2 = jerry_get_number_value(arg4);
    double z2 = jerry_get_number_value(arg5);

    wVector3f start = { x1, y1, z1 };
    wVector3f end   = { x2, y2, z2 };

    ++lasers.length;
    lasers[$ - 1].start = wVector3f (start.x, start.y - 0.1, start.z);
    lasers[$ - 1].end = end;

    wTriangle tris;
    wVector3f normal, collideAt;
    if (wCollisionGetPointFromRay(g_WorldCollider, &start, &end, &collideAt, &normal, &tris))
    {
        wVector3f offset;
        wVector3f point;

        // для стопроцентного определения объекта немного сдвинем точку "вовнутрь"
        offset = wMathVector3fNormalize(normal);
        offset.x *= -0.01;
        offset.y *= -0.01;
        offset.z *= -0.01;
        point = wMathVector3fAdd(collideAt, offset);

        if (g_SolidObjects != null)
        {
            wNode* target = wCollisionGetNodeChildFromPoint(g_SolidObjects, 0, false, &point);
            if (target != null)
            {
                void* ptr = wNodeGetUserData(target);
                if (ptr != null)
                {
                    int id = *(cast(int*)ptr);
                    if (id > 0 && id < g_ObjectsIdTable.length)
                    {
                        return jerry_create_number(id);
                    }
                }
            }
        }
    }

    return jerry_create_number(0);
}


void RegisterObjectJSFunctions()
{
    /* global */
    JS_RegisterCFunction(&js_objectCreate,    "objectCreate"   );
    JS_RegisterCFunction(&js_objectGetId,     "objectGetId"    );
    JS_RegisterCFunction(&js_objectDestroy,   "objectDestroy"  );
    JS_RegisterCFunction(&js_objectSetEnable, "objectSetEnable");
    JS_RegisterCFunction(&js_objectIsEnable,  "objectIsEnable" );

    /* animation */
    JS_RegisterCFunction(&js_objectSetFrame,       "objectSetFrame"      );
    JS_RegisterCFunction(&js_objectGetFrame,       "objectGetFrame"      );
    JS_RegisterCFunction(&js_objectGetFramesCount, "objectGetFramesCount");

    /* collision */
    JS_RegisterCFunction(&js_objectEnableCollision,  "objectEnableCollision" );
    JS_RegisterCFunction(&js_objectDisableCollision, "objectDisableCollision");
    JS_RegisterCFunction(&js_objectGetByRay,         "objectGetByRay"        );

    /* util */
    JS_RegisterCFunction(&js_objectIsInView,         "objectIsInView"        );
    JS_RegisterCFunction(&js_distanceBetweenObjects, "distanceBetweenObjects");

    /* position */
    JS_RegisterCFunction(&js_objectSetPosition,  "objectSetPosition" );
    JS_RegisterCFunction(&js_objectSetPositionX, "objectSetPositionX");
    JS_RegisterCFunction(&js_objectSetPositionY, "objectSetPositionY");
    JS_RegisterCFunction(&js_objectSetPositionZ, "objectSetPositionZ");
    JS_RegisterCFunction(&js_objectGetPosition,  "objectGetPosition" );
    JS_RegisterCFunction(&js_objectGetPositionX, "objectGetPositionX");
    JS_RegisterCFunction(&js_objectGetPositionY, "objectGetPositionY");
    JS_RegisterCFunction(&js_objectGetPositionZ, "objectGetPositionZ");
    JS_RegisterCFunction(&js_objectMove,         "objectMove"        );

    /* scale */
    JS_RegisterCFunction(&js_objectSetScale,  "objectSetScale" );
    JS_RegisterCFunction(&js_objectSetScaleX, "objectSetScaleX");
    JS_RegisterCFunction(&js_objectSetScaleY, "objectSetScaleY");
    JS_RegisterCFunction(&js_objectSetScaleZ, "objectSetScaleZ");
    JS_RegisterCFunction(&js_objectGetScale,  "objectGetScale" );
    JS_RegisterCFunction(&js_objectGetScaleX, "objectGetScaleX");
    JS_RegisterCFunction(&js_objectGetScaleY, "objectGetScaleY");
    JS_RegisterCFunction(&js_objectGetScaleZ, "objectGetScaleZ");

    /* size */
    JS_RegisterCFunction(&js_objectGetSize,   "objectGetSize" );
    JS_RegisterCFunction(&js_objectGetSizeX,  "objectGetSizeX");
    JS_RegisterCFunction(&js_objectGetSizeY,  "objectGetSizeY");
    JS_RegisterCFunction(&js_objectGetSizeZ,  "objectGetSizeZ");

    /* rotation */
    JS_RegisterCFunction(&js_objectSetRotation,  "objectSetRotation" );
    JS_RegisterCFunction(&js_objectSetRotationX, "objectSetRotationX");
    JS_RegisterCFunction(&js_objectSetRotationY, "objectSetRotationY");
    JS_RegisterCFunction(&js_objectSetRotationX, "objectSetRotationZ");
    JS_RegisterCFunction(&js_objectGetRotationX, "objectGetRotation" );
    JS_RegisterCFunction(&js_objectGetRotationX, "objectGetRotationX");
    JS_RegisterCFunction(&js_objectGetRotationY, "objectGetRotationY");
    JS_RegisterCFunction(&js_objectGetRotationX, "objectGetRotationZ");
    JS_RegisterCFunction(&js_objectRotate,       "objectRotate"      );

    /* user data */
    JS_RegisterCFunction(&js_objectAddVarNumber, "objectAddVarNumber");
    JS_RegisterCFunction(&js_objectAddVarBool,   "objectAddVarBool"  );
    JS_RegisterCFunction(&js_objectAddVarString, "objectAddVarString");
    JS_RegisterCFunction(&js_objectAddVarVector, "objectAddVarVector");
    JS_RegisterCFunction(&js_objectSetVar,       "objectSetVar"      );
    JS_RegisterCFunction(&js_objectGetVar,       "objectGetVar"      );
    JS_RegisterCFunction(&js_objectRemoveVar,    "objectRemoveVar"   );
    JS_RegisterCFunction(&js_objectClearVars,    "objectClearVars"   );
    JS_RegisterCFunction(&js_objectIsVarNumber,  "objectIsVarNumber" );
    JS_RegisterCFunction(&js_objectIsVarBool,    "objectIsVarBool"   );
    JS_RegisterCFunction(&js_objectIsVarString,  "objectIsVarString" );
    JS_RegisterCFunction(&js_objectIsVarVector,  "objectIsVarVector" );
}
