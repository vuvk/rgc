module player;
/*
import core.stdc.stdio;
import core.stdc.stdlib;
import core.stdc.string;
import core.stdc.math;
*/
import std.stdio;
import std.math;

import WorldSim3D;

import global;
import constants;
import js_util;
import util;
import base_object;

extern (C):

class TPlayer : TObject3D
{
    wAnimator* physAnim;

    this (float x, float y, float z)
    {
        this.id = 1;

        addVar("health", 100.0);
        addVar("type",   "player"  );            // имя типа объекта

        uint tesselation = 8;
        float radius = 0.3 * BLOCK_SIZE;
        float length = 0.6 * BLOCK_SIZE;

        this.node = wNodeCreateCylinder(tesselation, radius, length, wCOLOR4s_RED);
        wNodeSetPosition(this.node, wVector3f(x, y, z));

        wMaterial* mat = wNodeGetMaterial(this.node, 0);
        MaterialSetOldSchool(mat);

        wMesh* mesh = wNodeGetMesh(this.node);
        this.physBody = wCollisionCreateFromMesh(mesh, this.node, 0);
        //this.physBody = wCollisionCreateFromBox(this.node);
        wNodeAddCollision(this.node, this.physBody);

        if (g_WorldCollider != null)
        {
            this.physAnim = wAnimatorCollisionResponseCreate(g_WorldCollider, this.node, 0.05f);

            ///Get & Set collision animator parameters
            wAnimatorCollisionResponse params;
            wAnimatorCollisionResponseGetParameters(this.physAnim, &params);
            params.gravity = wVector3f(0.0, -BLOCK_SIZE, 0.0);
            params.ellipsoidRadius = wVector3f(radius, radius, radius);
            params.ellipsoidTranslation = wVector3f(0.0, -length / 2.0, 0.0);
            wAnimatorCollisionResponseSetParameters(this.physAnim, params);
        }

        //wNodeSetDebugMode(this.node, /*wDebugMode.wDM_MESH_WIRE_OVERLAY | */wDebugMode.wDM_BBOX);
        //wNodeSetDebugDataVisible(this.node, true);
    }

    ~this()
    {
        if (this.physAnim)
            wAnimatorDestroy(this.node, this.physAnim);
    }

    override void setFrame(int frame) {}
    override int getFrame() { return 0; }
    override int getFramesCount() { return 0; }
}

class TCamera : TObject3D
{
    float pitch;
    float yaw;
    float roll;

    this (float x, float y, float z)
    {
        addVar("name", "camera"  );            // имя объекта
        addVar("type", "camera"  );            // имя типа объекта

        this.id = 2;
        this.node = wCameraCreate(wVector3f(x, y, z), wVECTOR3f_ZERO);

        this.pitch = 0.0f;
        this.yaw   = 0.0f;
        this.roll  = 0.0f;

        wCameraSetClipDistance(this.node, ((MAP_WIDTH * MAP_HEIGHT) >> 1), 0.001f);
        this.updateAngles();
    }

    override void setFrame(int frame) {}
    override int getFrame() { return 0; }
    override int getFramesCount() { return 0; }

    void updateAngles()
    {
        if (this.pitch < 0.0f) this.pitch += 360.0;
        if (this.yaw   < 0.0f) this.yaw   += 360.0;
        if (this.roll  < 0.0f) this.roll  += 360.0;

        if (this.pitch >= 360.0f) this.pitch -= 360.0;
        if (this.yaw   >= 360.0f) this.yaw   -= 360.0;
        if (this.roll  >= 360.0f) this.roll  -= 360.0;

        float pitchRad = this.pitch * DEG_TO_RAD_COEFF;
        float yawRad   = this.yaw   * DEG_TO_RAD_COEFF;

        wVector3f pos = wNodeGetPosition(this.node);

        wVector3f target = {pos.x - sin(yawRad),
                            pos.y + tan(pitchRad),
                            pos.z - cos(yawRad)};
        wCameraSetTarget(this.node, target);
    }

    void setPitch(float deg)
    {
        this.pitch = deg;
        updateAngles();
    }

    void setYaw(float deg)
    {
        this.yaw = deg;
        updateAngles();
    }
    /*
    void setRoll(float deg)
    {
        this.roll = deg;
        updateAngles();
    }
    */
}

__gshared TPlayer g_Player;
__gshared TCamera g_Camera;


/** INTERPRETER */
/*
duk_ret_t n_cameraSetPosition(duk_context *ctx);
duk_ret_t n_cameraGetPositionX(duk_context *ctx);
duk_ret_t n_cameraGetPositionY(duk_context *ctx);
duk_ret_t n_cameraGetPositionZ(duk_context *ctx);
*/
/*
duk_ret_t n_cameraSetTarget(duk_context *ctx);
duk_ret_t n_cameraGetTargetX(duk_context *ctx);
duk_ret_t n_cameraGetTargetY(duk_context *ctx);
duk_ret_t n_cameraGetTargetZ(duk_context *ctx);
*/
/*
duk_ret_t n_cameraSetAngles(duk_context* ctx);
duk_ret_t n_cameraSetPitch(duk_context* ctx);
duk_ret_t n_cameraSetYaw(duk_context* ctx);
//duk_ret_t n_cameraSetRoll(duk_context* ctx);
//duk_ret_t n_cameraGetAngles(duk_context* ctx);
duk_ret_t n_cameraGetPitch(duk_context* ctx);
duk_ret_t n_cameraGetYaw(duk_context* ctx);
//duk_ret_t n_cameraGetRoll(duk_context* ctx);
*/

/*
void CameraCreate(float x, float y, float z);
void CameraDestroy();
*/


void RegisterPlayerJSFunctions();
void RegisterCameraJSFunctions();


// IMPLEMENTATION

/** JerrySCript */
/** INTERPRETER */

/** TARGET */
static jerry_value_t js_cameraGetTarget
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (g_Camera !is null && g_Camera.node != null)
    {
        wVector3f tgt = wCameraGetTarget(g_Camera.node);
        double x = tgt.x;
        double y = tgt.y;
        double z = tgt.z;

        jerry_value_t array = jerry_create_typedarray (jerry_typedarray_type_t.JERRY_TYPEDARRAY_FLOAT64, 3);

        jerry_length_t byteLength;
        jerry_length_t byteOffset;
        jerry_value_t arrayBuffer = jerry_get_typedarray_buffer (array, &byteOffset, &byteLength);

        jerry_arraybuffer_write(arrayBuffer,  0, cast(ubyte*)&x, double.sizeof);
        jerry_arraybuffer_write(arrayBuffer,  8, cast(ubyte*)&y, double.sizeof);
        jerry_arraybuffer_write(arrayBuffer, 16, cast(ubyte*)&z, double.sizeof);

        jerry_release_value(arrayBuffer);

        return array;
    }

    return jerry_create_undefined();
}
static jerry_value_t js_cameraGetTargetX
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (g_Camera !is null && g_Camera.node != null)
    {
        wVector3f tgt = wCameraGetTarget(g_Camera.node);
        return jerry_create_number(tgt.x);
    }

    return jerry_create_number_nan();
}
static jerry_value_t js_cameraGetTargetY
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (g_Camera !is null && g_Camera.node != null)
    {
        wVector3f tgt = wCameraGetTarget(g_Camera.node);
        return jerry_create_number(tgt.y);
    }

    return jerry_create_number_nan();
}
static jerry_value_t js_cameraGetTargetZ
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (g_Camera !is null && g_Camera.node != null)
    {
        wVector3f tgt = wCameraGetTarget(g_Camera.node);
        return jerry_create_number(tgt.z);
    }

    return jerry_create_number_nan();
}
static jerry_value_t js_cameraUpdate
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (g_Camera !is null)
    {
        g_Camera.updateAngles();
    }

    return jerry_create_undefined();
}

/** ANGLES */
static jerry_value_t js_cameraSetPitch
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    if (g_Camera !is null)
    {
        g_Camera.setPitch(jerry_get_number_value(arg0));
    }

    return jerry_create_undefined();
}
static jerry_value_t js_cameraSetYaw
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    if (g_Camera !is null)
    {
        g_Camera.setYaw(jerry_get_number_value(arg0));
    }

    return jerry_create_undefined();
}

static jerry_value_t js_cameraGetPitch
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (g_Camera !is null)
    {
        return jerry_create_number(g_Camera.pitch);
    }

    return jerry_create_number_nan();
}
static jerry_value_t js_cameraGetYaw
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (g_Camera !is null)
    {
        return jerry_create_number(g_Camera.yaw);
    }

    return jerry_create_number_nan();
}

/** PLAYER */
/** POSITION */

void RegisterCameraJSFunctions()
{
    JS_RegisterCFunction(&js_cameraGetTarget,  "cameraGetTarget" );
    JS_RegisterCFunction(&js_cameraGetTargetX, "cameraGetTargetX");
    JS_RegisterCFunction(&js_cameraGetTargetY, "cameraGetTargetY");
    JS_RegisterCFunction(&js_cameraGetTargetZ, "cameraGetTargetZ");
    JS_RegisterCFunction(&js_cameraUpdate,     "cameraUpdate"    );

    JS_RegisterCFunction(&js_cameraSetPitch,   "cameraSetPitch"  );
    JS_RegisterCFunction(&js_cameraSetYaw,     "cameraSetYaw"    );
    JS_RegisterCFunction(&js_cameraGetPitch,   "cameraGetPitch"  );
    JS_RegisterCFunction(&js_cameraGetYaw,     "cameraGetYaw"    );
}

void RegisterPlayerJSFunctions()
{
    //
}
