module input;

import std.conv;

import js_util;
import WorldSim3D;

extern (C):

/** KEYBOARD */
void RegisterKeyboardJSFunctions();

/** MOUSE */
void RegisterMouseJSFunctions();

// IMPLEMENTATION


static jerry_value_t js_keyboardIsEventAvailable
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    return jerry_create_boolean(wInputIsKeyEventAvailable());
}
static jerry_value_t js_keyboardIsKeyHit
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    jerry_value_t arg0 = args_p[0];
    if (args_cnt == 0 || !jerry_value_is_number(arg0))
    {
        return jerry_create_boolean(0);
    }

    int key = to!int(jerry_get_number_value(arg0));

    return jerry_create_boolean(wInputIsKeyHit(cast(wKeyCode)key));
}
static jerry_value_t js_keyboardIsKeyPressed
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    jerry_value_t arg0 = args_p[0];
    if (args_cnt == 0 || !jerry_value_is_number(arg0))
    {
        return jerry_create_boolean(0);
    }

    int key = to!int(jerry_get_number_value(arg0));

    return jerry_create_boolean(wInputIsKeyPressed(cast(wKeyCode)key));
}
static jerry_value_t js_keyboardIsKeyReleased
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    jerry_value_t arg0 = args_p[0];
    if (args_cnt == 0 || !jerry_value_is_number(arg0))
    {
        return jerry_create_boolean(0);
    }

    int key = to!int(jerry_get_number_value(arg0));

    return jerry_create_boolean(wInputIsKeyUp(cast(wKeyCode)key));
}

void RegisterKeyboardJSFunctions()
{
    jerry_value_t object = jerry_create_object ();

    JS_RegisterCFunctionForObject(object, "Keyboard", &js_keyboardIsEventAvailable, "isEventAvailable");
    JS_RegisterCFunctionForObject(object, "Keyboard", &js_keyboardIsKeyHit,         "isKeyHit"        );
    JS_RegisterCFunctionForObject(object, "Keyboard", &js_keyboardIsKeyPressed,     "isKeyPressed"    );
    JS_RegisterCFunctionForObject(object, "Keyboard", &js_keyboardIsKeyReleased,    "isKeyReleased"   );

    jerry_release_value (object);
}


/** MOUSE */
static jerry_value_t js_mouseSetCursorVisible
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt > 0)
    {
        jerry_value_t arg0 = args_p[0];
        if (jerry_value_is_boolean(arg0))
        {
            wInputSetCursorVisible(jerry_get_boolean_value(arg0) != 0);
        }
    }

    return jerry_create_undefined();
}
static jerry_value_t js_mouseIsCursorVisible
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    return jerry_create_boolean(wInputIsCursorVisible());
}
static jerry_value_t js_mouseIsEventAvailable
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    return jerry_create_boolean(wInputIsMouseEventAvailable());
}
static jerry_value_t js_mouseIsButtonHit
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    jerry_value_t arg0 = args_p[0];
    if (args_cnt == 0 || !jerry_value_is_number(arg0))
    {
        return jerry_create_boolean(0);
    }

    int key = to!int(jerry_get_number_value(arg0));

    return jerry_create_boolean(wInputIsMouseHit(cast(wMouseButtons)key));
}
static jerry_value_t js_mouseIsButtonPressed
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    jerry_value_t arg0 = args_p[0];
    if (args_cnt == 0 || !jerry_value_is_number(arg0))
    {
        return jerry_create_boolean(0);
    }

    int key = to!int(jerry_get_number_value(arg0));

    return jerry_create_boolean(wInputIsMousePressed(cast(wMouseButtons)key));
}
static jerry_value_t js_mouseIsButtonReleased
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    jerry_value_t arg0 = args_p[0];
    if (args_cnt == 0 || !jerry_value_is_number(arg0))
    {
        return jerry_create_boolean(0);
    }

    int key = to!int(jerry_get_number_value(arg0));

    return jerry_create_boolean(wInputIsMouseUp(cast(wMouseButtons)key));
}
static jerry_value_t js_mouseSetLogicalPosition
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt > 1)
    {
        jerry_value_t arg0 = args_p[0];
        jerry_value_t arg1 = args_p[1];
        if (jerry_value_is_number(arg0) && jerry_value_is_number(arg1))
        {
            wVector2f pos = {jerry_get_number_value(arg0), jerry_get_number_value(arg1)};
            wInputSetMouseLogicalPosition(&pos);
        }
    }
    return jerry_create_undefined();
}
static jerry_value_t js_mouseSetPosition
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt > 1)
    {
        jerry_value_t arg0 = args_p[0];
        jerry_value_t arg1 = args_p[1];
        if (jerry_value_is_number(arg0) && jerry_value_is_number(arg1))
        {
            wVector2i pos = {to!int(jerry_get_number_value(arg0)), to!int(jerry_get_number_value(arg1))};
            wInputSetMousePosition(&pos);
        }
    }
    return jerry_create_undefined();
}
static jerry_value_t js_mouseGetX
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    wVector2i pos;
    wInputGetMousePosition(&pos);

    return jerry_create_number(pos.x);
}
static jerry_value_t js_mouseGetY
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    wVector2i pos;
    wInputGetMousePosition(&pos);

    return jerry_create_number(pos.y);
}
static jerry_value_t js_mouseGetDeltaX
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    return jerry_create_number(wInputGetMouseDeltaX());
}
static jerry_value_t js_mouseGetDeltaY
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    return jerry_create_number(wInputGetMouseDeltaY());
}

void RegisterMouseJSFunctions()
{
    jerry_value_t object = jerry_create_object ();

    JS_RegisterCFunctionForObject(object, "Mouse", &js_mouseSetCursorVisible,   "setCursorVisible"  );
    JS_RegisterCFunctionForObject(object, "Mouse", &js_mouseIsCursorVisible,    "isCursorVisible"   );
    JS_RegisterCFunctionForObject(object, "Mouse", &js_mouseIsEventAvailable,   "isEventAvailable"  );
    JS_RegisterCFunctionForObject(object, "Mouse", &js_mouseIsButtonHit,        "isButtonHit"       );
    JS_RegisterCFunctionForObject(object, "Mouse", &js_mouseIsButtonPressed,    "isButtonPressed"   );
    JS_RegisterCFunctionForObject(object, "Mouse", &js_mouseIsButtonReleased,   "isButtonReleased"  );
    JS_RegisterCFunctionForObject(object, "Mouse", &js_mouseSetLogicalPosition, "setLogicalPosition");
    JS_RegisterCFunctionForObject(object, "Mouse", &js_mouseSetPosition,        "setPosition"       );
    JS_RegisterCFunctionForObject(object, "Mouse", &js_mouseGetX,               "getX"              );
    JS_RegisterCFunctionForObject(object, "Mouse", &js_mouseGetY,               "getY"              );
    JS_RegisterCFunctionForObject(object, "Mouse", &js_mouseGetDeltaX,          "getDeltaX"         );
    JS_RegisterCFunctionForObject(object, "Mouse", &js_mouseGetDeltaY,          "getDeltaY"         );

    jerry_release_value (object);
}

