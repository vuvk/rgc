module doors_keys;

import std.stdio;
import std.conv;
import core.stdc.string;

import constants;
import global;
import res_manager;
import util;
import map;
import js_util;
import physfs_util;
import base_object;

import WorldSim3D;
import SampleFunctions;


extern (C):

/** дверь-эталон, загруженная из пака */
struct TDoorInMem
{
    string name;           // имя двери
    bool   destroyable;    // разрушаемый?
    ubyte  endurance;      // живучесть
    double openSpeed;      // скорость открывания
    bool   stayOpened;     // открывается один раз?
    bool   needKey;        // нужен ключ для открывания?
    string needKeyMsg;     // сообщение о необходимости ключа
    string keyName;        // имя ключа для открывания
    double width;          // ширина двери
    wTexture* rib;         // текстура ребра

    ubyte  animSpeed;      // кол-во кадров в секунду
    wTexture*[] frames;

    TScriptJS script;

    ~this()
    {
        wTexture* txr;
        for (int i = 0; i < frames.length; ++i)
        {
            txr = frames[i];
            if (txr != null)
            {
                wTextureDestroy(txr);
            }
        }
        frames.length = 0;
        if (rib != null)
            wTextureDestroy(rib);
    }
}

/** нода двери, создаваемая на карте */
class TDoorNode : TObject3D
{
    TDoorInMem* index;      // индекс двери (позиция в массиве g_Doors)
    float   animSpeed;      // скорость анимации
    float   _animSpeed;     // значение прироста анимации до смены кадра

    // создание ноды
    this (TDoorInMem* index, wNode* node)
    {
        wMaterial* material;

        this.index     = index;
        this.animSpeed = index.animSpeed;
        this.node      = node;

        this.script          = new TScriptJS();
        this.script.fileName = index.script.fileName.dup;
        this.script.text     = index.script.text.dup;
        this.script.setId();

        addVar("name",        index.name);          // имя
        addVar("type",        "door"  );            // имя типа объекта
        addVar("destroyable", index.destroyable);   // можно ли разрушить
        addVar("health",      index.endurance);     // текущее значение жизней
        addVar("openSpeed",   index.openSpeed);     // скорость открывания
        addVar("stayOpened",  index.stayOpened);    // открывается один раз?
        addVar("needKey",     index.needKey);       // нужен ключ для открывания?
        addVar("message",     index.needKeyMsg);    // сообщение о необходимости ключа
        addVar("isDestroyed", false);               // разрушено?
        addVar("keyName",     index.keyName);       // какой ключ нужен для открывания
        addVar("animationSpeed", index.animSpeed);

        if (node == null)
            return;

        wMesh* mesh = wNodeGetMesh(node);
        if (mesh == null)
            return;

        this.physBody = wCollisionCreateFromMesh(mesh, this.node, 0);
    }

    override void setFrame(int frame)
    {
        if (index == null || index.frames.length == 0)
            return;

        if (frame < 0 || frame >= index.frames.length)
            return;

        curFrame = frame;
    }

    override int getFrame()
    {
        return curFrame;
    }

    override int getFramesCount()
    {
        if (index == null)
            return 0;

        return index.frames.length;
    }

    override void update()
    {
        if (!this.isVisible)
            return;

        if (this.node == null)
            return;

        /** TODO : ПЕРЕДЕЛАТЬ НА СКРИПТ */
        // обновить анимацию ноды спрайта
        if (this.animSpeed != 0.0)
        {
            if (this._animSpeed < 1.0f)
            {
                this._animSpeed += g_DeltaTime * this.animSpeed;
            }
            else
            {
                this._animSpeed = 0.0f;

                // переходим на следующий кадр
                ++(this.curFrame);
                if (this.curFrame >= this.index.frames.length)
                    this.curFrame = 0;

                // если там пустая текстура, то ищем на следующем кадре
                wTexture* txr = this.index.frames[this.curFrame];
                while (txr == null)
                {
                    ++(this.curFrame);
                    if (this.curFrame >= this.index.frames.length)
                        this.curFrame = 0;

                    txr = this.index.frames[this.curFrame];
                }

                wMaterial* mat = wNodeGetMaterial(this.node, 0);
                wMaterialSetTexture(mat, 0, txr);
            }
        }

        super.update();
    }
}

/** ключ-эталон, загруженный из пака */
struct TKeyInMem
{
    string name;          // имя ключа
    ubyte  animSpeed;     // кол-во кадров в секунду
    wTexture*[] frames;

    TScriptJS script;

    ~this()
    {
        wTexture* txr;
        for (int i = 0; i < frames.length; ++i)
        {
            txr = frames[i];
            if (txr != null)
            {
                wTextureDestroy(txr);
            }
        }
        frames.length = 0;
    }
}

/** нода ключа, создаваемая на карте */
class TKeyNode : TObject3D
{
    TKeyInMem* index;       // индекс ключа (позиция в массиве g_Keys)
    wNode*  billboard;      // нода биллборда
    float   animSpeed;      // скорость анимации
    float   _animSpeed;     // значение прироста анимации до смены кадра

    // создание ноды
    this (TKeyInMem* index, wNode* billboard)
    {
        wMaterial* material;

        addVar("name", index.name);               // имя ключа
        addVar("type", "key"  );            // имя типа объекта
        addVar("animationSpeed", index.animSpeed);

        this.index     = index;
        this.animSpeed = index.animSpeed;
        this.billboard = billboard;
        this.node      = wNodeCreateEmpty();

        this.script          = new TScriptJS();
        this.script.fileName = index.script.fileName.dup;
        this.script.text     = index.script.text.dup;
        this.script.setId();

        wVector3f pos = wNodeGetPosition(billboard);
        wNodeSetPosition(this.node, pos);

        // фиксация по вертикали
        wBillboardSetEnabledAxis(this.billboard, wBillboardAxisParam(false, true, false));
    }

    ~this()
    {
        //writeln("I am " ~ this.index.name);
        if (this.billboard != null)
        {
            wNodeDestroy(this.billboard);
        }
    }

    override void setFrame(int frame)
    {
        if (index == null || index.frames.length == 0)
            return;

        if (frame < 0 || frame >= index.frames.length)
            return;

        curFrame = frame;
    }

    override int getFrame()
    {
        return curFrame;
    }

    override int getFramesCount()
    {
        if (index == null)
            return 0;

        return index.frames.length;
    }

    override void setVisibility(bool isVisible)
    {
        if (this.billboard)
            wNodeSetVisibility(this.billboard, isVisible);

        super.setVisibility(isVisible);
    }

    override void update()
    {
        if (!this.isVisible)
            return;

        if (this.billboard == null)
            return;

        /** ПЕРЕДЕЛАТЬ НА СКРИПТЫ! */
        // обновить анимацию ноды спрайта
        if (this.animSpeed != 0.0)
        {
            if (this._animSpeed < 1.0f)
            {
                this._animSpeed += g_DeltaTime * this.animSpeed;
            }
            else
            {
                this._animSpeed = 0.0f;

                // переходим на следующий кадр
                ++(this.curFrame);
                if (this.curFrame >= this.index.frames.length)
                    this.curFrame = 0;

                // если там пустая текстура, то ищем на следующем кадре
                wTexture* txr = this.index.frames[this.curFrame];
                while (txr == null)
                {
                    ++(this.curFrame);
                    if (this.curFrame >= this.index.frames.length)
                        this.curFrame = 0;

                    txr = this.index.frames[this.curFrame];
                }

                wMaterial* mat = wNodeGetMaterial(this.billboard, 0);
                wMaterialSetTexture(mat, 0, txr);
            }
        }

        super.update();
    }
}

__gshared TDoorInMem[] g_Doors;
__gshared TDoorNode[]  g_DoorNodes;
__gshared TKeyInMem[]  g_Keys;
__gshared TKeyNode[]   g_KeyNodes;

void InitDoors();
void InitKeys();
bool LoadDoorsPack();
bool LoadKeysPack();
void DoorNodesUpdate();
void KeyNodesUpdate();



// IMPLEMENTATION

void InitDoors()
{
    //g_Doors = new TDoorInMem[DOORS_MAX_COUNT];
    if (g_Doors.length == 0)
        g_Doors.length = DOORS_MAX_COUNT;
    memset(g_Doors.ptr, 0, g_Doors.length * TDoorInMem.sizeof);
}

void InitKeys()
{
    //g_Keys = new TKeyInMem[KEYS_MAX_COUNT];
    if (g_Keys.length == 0)
        g_Keys.length = KEYS_MAX_COUNT;
    memset(g_Keys.ptr, 0, g_Keys.length * TKeyInMem.sizeof);
}

bool LoadDoorsPack()
{
    bool    isLoaded;
    TDoorInMem* door;

    ushort  w, h;
    ubyte[] buffer;
    int     bufferSize;
    string  fileName;

    string  doorName;
    int     i, j;

    string  name;
    bool    destroyable;
    ubyte   endurance;
    int     openSpeed;
    bool    stayOpened;
    bool    needKey;
    string  needKeyMsg;
    short   keyId;
    double  width;
    ubyte   animSpeed;
    uint    framesCount;
    int     textureSize;

    // читаем количество текстур
    /*
    bufferSize = Res_GetUncompressedFileSize("doors.cfg\0");
    if (bufferSize <= 0)
    {
        PrintWithColor("can not get size of doors.cfg\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    buffer.length = bufferSize;
    if (!Res_ReadFileFromZipToBuffer("doors.cfg", &(buffer[0]), bufferSize))
    {
        PrintWithColor("can not read doors.cfg\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    */
    FileRead("doors.cfg", buffer);
    bufferSize = cast(int)FileSize("doors.cfg");
    if (!Res_OpenIniFromBuffer(&(buffer[0]), bufferSize))
    {
        PrintWithColor("can not open doors.cfg!!!\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    g_DoorsCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_DoorsCount == 0)
        goto end;

    InitDoors();

    for (i = 0; i < g_DoorsCount; ++i)
    {
        // если не нужна для загрузки
        if (!g_DoorsNeedForLoad[i])
            continue;

        // читаем инфу по текстуре
        fileName = "door_" ~ to!string(i) ~ ".cfg\0";
        //bufferSize = Res_GetUncompressedFileSize(fileName.ptr);
        bufferSize = cast(int)FileSize(fileName);
        if (bufferSize <= 0)
        {
            PrintWithColor("can not get size of door_" ~ to!string(i) ~ ".cfg", wConsoleFontColor.wCFC_RED, false);
            continue;
        }

        door = &(g_Doors[i]);

        /*
        buffer.length = bufferSize;
        Res_ReadFileFromZipToBuffer(fileName.ptr, &(buffer[0]), bufferSize);
        */
        FileRead(fileName, buffer);
        Res_OpenIniFromBuffer(&(buffer[0]), bufferSize);

        name        = to!string(Res_IniReadString("Options", "name", ("Door #" ~ to!string(i) ~ "\0").ptr));
        destroyable = Res_IniReadBool  ("Options", "destroyable",  false);
        endurance   = cast(ubyte)Res_IniReadInteger("Options", "endurance", 0);

        openSpeed   = Res_IniReadInteger("Options", "open_speed", 180);
        stayOpened  = Res_IniReadBool  ("Options", "stay_opened",  false);
        needKey     = Res_IniReadBool  ("Options", "need_key",     false);
        needKeyMsg  = to!string(Res_IniReadString("Options", "message", "I need a key.\0".ptr));
        keyId       = cast(short)Res_IniReadInteger("Options", "key_id",       0);
        --keyId;    // т.к. нумерация с 0
        width       = (Res_IniReadDouble("Options", "width", 1.0) / 100.0);
        animSpeed   = cast(ubyte)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = cast(ubyte)Res_IniReadInteger("Options", "frames_count",    0);
        Res_CloseIni();

        door.name        = name;
        door.destroyable = destroyable;
        door.endurance   = endurance;

        door.openSpeed   = cast(double)openSpeed / 255.0;
        door.stayOpened  = stayOpened;
        door.needKey     = needKey;
        door.needKeyMsg  = needKeyMsg;
        //door.keyId       = keyId;
        door.width       = width;
        door.animSpeed   = animSpeed;
        //door.script.loadFromFile("assets/scripts/doors/door.js");
        door.script.loadFromFile("door.js");

        // подтягиваем имя необходимого ключа
        if (keyId >= 0 && keyId < g_KeysCount && keyId < g_Keys.length)
            door.keyName = g_Keys[keyId].name.dup;

        if (framesCount == 0)
            continue;

        fileName = "door_" ~ to!string(i) ~ "_rib.dat\0";
        doorName = "Door_" ~ to!string(i) ~ "_Rib";
        door.rib = LoadTexture(fileName, doorName);

        // читаем кадры, если есть
        door.frames = new wTexture*[framesCount];
        for (j = 0; j < framesCount; ++j)
        {
            fileName   = "door_" ~ to!string(i) ~ "_frame_" ~ to!string(j) ~ ".dat\0";
            doorName = "Door_" ~ to!string(i) ~ "_Frame_" ~ to!string(j);
            door.frames[j] = LoadTexture(fileName, doorName);
        }
    }
    isLoaded = true;

end:
    buffer.length = 0;

    return isLoaded;
}


bool LoadKeysPack()
{
    bool    isLoaded;
    TKeyInMem* key;

    ushort  w, h;
    ubyte[] buffer;
    int     bufferSize;
    string  fileName;

    string  keyName;
    int     i, j;

    string  name;
    ubyte   animSpeed;
    uint    framesCount;
    int     textureSize;

    // читаем количество текстур
    /*
    bufferSize = Res_GetUncompressedFileSize("keys.cfg\0");
    if (bufferSize <= 0)
    {
        PrintWithColor("can not get size of keys.cfg\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    buffer.length = bufferSize;
    if (!Res_ReadFileFromZipToBuffer("keys.cfg", &(buffer[0]), bufferSize))
    {
        PrintWithColor("can not read keys.cfg\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    */
    FileRead("keys.cfg", buffer);
    bufferSize = cast(int)FileSize("keys.cfg");
    if (!Res_OpenIniFromBuffer(&(buffer[0]), bufferSize))
    {
        PrintWithColor("can not open keys.cfg!!!\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    g_KeysCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_KeysCount == 0)
        goto end;

    InitKeys();

    for (i = 0; i < g_KeysCount; ++i)
    {
        // если не нужна для загрузки
        if (!g_KeysNeedForLoad[i])
            continue;

        // читаем инфу по текстуре
        fileName = "key_" ~ to!string(i) ~ ".cfg\0";
        //bufferSize = Res_GetUncompressedFileSize(fileName.ptr);
        bufferSize = cast(int)FileSize(fileName);
        if (bufferSize <= 0)
        {
            PrintWithColor("can not get size of key_" ~ to!string(i) ~ ".cfg", wConsoleFontColor.wCFC_RED, false);
            continue;
        }

        key = &(g_Keys[i]);

        /*
        buffer.length = bufferSize;
        Res_ReadFileFromZipToBuffer(fileName.ptr, &(buffer[0]), bufferSize);
        */
        FileRead(fileName, buffer);
        Res_OpenIniFromBuffer(&(buffer[0]), bufferSize);

        name        = to!string(Res_IniReadString("Options", "name", ("Key #" ~ to!string(i) ~ "\0").ptr));
        animSpeed   = cast(ubyte)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = cast(ubyte)Res_IniReadInteger("Options", "frames_count",    0);
        Res_CloseIni();

        key.name      = name;
        key.animSpeed = animSpeed;
        //key.script.loadFromFile("assets/scripts/keys/key.js");
        key.script.loadFromFile("key.js");

        if (framesCount == 0)
            continue;

        // читаем кадры, если есть
        key.frames = new wTexture*[framesCount];
        for (j = 0; j < framesCount; ++j)
        {
            fileName   = "key_" ~ to!string(i) ~ "_frame_" ~ to!string(j) ~ ".dat\0";
            keyName = "Key_" ~ to!string(i) ~ "_Frame_" ~ to!string(j);
            key.frames[j] = LoadTexture(fileName, keyName);
        }
    }
    isLoaded = true;

end:
    buffer.length = 0;

    return isLoaded;
}

void DoorNodesUpdate()
{
    if (g_DoorNodes.length == 0)
        return;

    TDoorNode door;
    for (int i = 0; i < g_DoorNodes.length; ++i)
    {
        door = g_DoorNodes[i];

        if (door !is null)
        {
            door.update();
        }
    }
}

void KeyNodesUpdate()
{
    if (g_KeyNodes.length == 0)
        return;

    TKeyNode key;
    for (int i = 0; i < g_KeyNodes.length; ++i)
    {
        key = g_KeyNodes[i];

        if (key !is null)
        {
            key.update();
        }
    }
}


/* СОЗДАНИЕ МЕША ДВЕРИ */
wMesh* CreateDoorMesh(const string name, wTexture* texture, wTexture* ribTexture, double width, bool isHorizontal, bool isVertical)
{
    if (!isHorizontal && !isVertical)
        return null;

    wVert[]  verts;
    ushort[] indices = [ 0, 1, 2, 0, 2, 3,
                         4, 5, 6, 4, 6, 7 ];

    wVert[]  ribVerts;
    ushort[] ribIndices = [  0,  1,  2,  0,  2,  3,
                             4,  5,  6,  4,  6,  7,
                             8,  9, 10,  8, 10, 11,
                            12, 13, 14, 12, 14, 15 ];

    float lt =  0.0 * BLOCK_SIZE;
    float rt =  1.0 * BLOCK_SIZE;
    float dn =  0.0 * BLOCK_SIZE;
    float up =  1.0 * BLOCK_SIZE;
    float bk =  0.0 * BLOCK_SIZE;
    float fw = -1.0 * BLOCK_SIZE;

    double hW = width / 2.0 * BLOCK_SIZE;
    if (isHorizontal)
    {
        // ребра
        ribVerts = [
        // right
                    wVert( wVector3f( rt, dn, hW ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( rt, dn,-hW ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( rt, up,-hW ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( rt, up, hW ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
        // left
                    wVert( wVector3f( lt, dn,-hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( lt, dn, hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( lt, up, hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( lt, up,-hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
        // top
                    wVert( wVector3f( lt, up,-hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( lt, up, hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
                    wVert( wVector3f( rt, up, hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( rt, up,-hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
        // bottom
                    wVert( wVector3f( lt, dn,-hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( rt, dn,-hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( rt, dn, hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
                    wVert( wVector3f( lt, dn, hW ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) )
        ];

        // сама дверь
        verts = [
        // forward
                    wVert( wVector3f( rt, dn,-hW ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( lt, dn,-hW ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( lt, up,-hW ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
                    wVert( wVector3f( rt, up,-hW ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
        // backward
                    wVert( wVector3f( lt, dn, hW ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( rt, dn, hW ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( rt, up, hW ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( lt, up, hW ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) )
        ];
    }
    else
    // может быть она вертикальная?
    {
        // ребра
        ribVerts = [
        // forward
                    wVert( wVector3f( hW, dn, fw ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f(-hW, dn, fw ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f(-hW, up, fw ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( hW, up, fw ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
        // backward
                    wVert( wVector3f(-hW, dn, bk ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( hW, dn, bk ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( hW, up, bk ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f(-hW, up, bk ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
        // top
                    wVert( wVector3f(-hW, up, fw ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
                    wVert( wVector3f(-hW, up, bk ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( hW, up, bk ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( hW, up, fw ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
        // bottom
                    wVert( wVector3f(-hW, dn, fw ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( hW, dn, fw ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
                    wVert( wVector3f( hW, dn, bk ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f(-hW, dn, bk ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) )
        ];

        // сама дверь
        verts = [
        // right
                    wVert( wVector3f( hW, dn, bk ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f( hW, dn, fw ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f( hW, up, fw ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
                    wVert( wVector3f( hW, up, bk ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
        // left
                    wVert( wVector3f(-hW, dn, fw ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) ),
                    wVert( wVector3f(-hW, dn, bk ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) ),
                    wVert( wVector3f(-hW, up, bk ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) ),
                    wVert( wVector3f(-hW, up, fw ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) ),
        ];
    }

    wMesh* mesh = wMeshCreate(cast(char*)name.ptr);
    wMeshBuffer* meshBuffer;
    wMaterial* material;

    // 1 мешбуффер - дверь
    meshBuffer = wMeshBufferCreate(verts.length, verts.ptr, indices.length, indices.ptr);
    material = wMeshBufferGetMaterial(meshBuffer);
    MaterialSetOldSchool(material);
    //wMaterialSetFlag(material, wMaterialFlags.wMF_BACK_FACE_CULLING, false);
    wMaterialSetType(material, wMaterialTypes.wMT_TRANSPARENT_ALPHA_CHANNEL);
    wMaterialSetTexture(material, 0, texture);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    // 2 мешбуффер - ограничивающая коробка
    meshBuffer = wMeshBufferCreate(ribVerts.length, ribVerts.ptr, ribIndices.length, ribIndices.ptr);
    material = wMeshBufferGetMaterial(meshBuffer);
    MaterialSetOldSchool(material);
    //wMaterialSetType(material, wMaterialTypes.wMT_TRANSPARENT_ALPHA_CHANNEL);
    // создаем невидимую текстуру, если она не создана ранее
    //if (g_InvisibleTexture == null)
    //    g_InvisibleTexture = CreateColorTexture(wCOLOR4s_ZERO, "Invisible Color");
    wMaterialSetTexture(material, 0, ribTexture);
    wMeshAddMeshBuffer(mesh, meshBuffer);

    wMeshEnableHardwareAcceleration(mesh, 0);

    return mesh;
}
