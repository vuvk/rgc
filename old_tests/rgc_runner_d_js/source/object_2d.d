module object_2d;

import std.conv;

import WorldSim3D;
import base_object;

extern (C):

struct TImage2D
{
    wTexture*[] frames;           /* кадры анимации */

    ~this()
    {
        for (int i = 0; i < frames.length; ++i)
        {
            if (frames[i] != null)
                wTextureDestroy(frames[i]);
        }

        frames.length = 0;
    }
}

class TObject2D : TObject
{
//    wGuiObject* guiObject;
    wVector2f position;
    wVector2f scale;
    float     rotation;

    //int    curFrame;           // текущий кадр
    //float  animSpeed;          // скорость анимации
    //protected {
    //    float _animSpeed;         // значение прироста анимации до смены кадра
    //}
    TImage2D* index;        /* индекс набора кадров */
    wTexture* curTexture;   // текущая для отображения текстура

    this (string name = "")
    {
        this.name = name;
        position = wVECTOR2f_ZERO;
        scale    = wVector2f(1, 1);
        rotation = 0.0;

        //animSpeed = 0;
        //_animSpeed = 0;
    }

/*
    ~this()
    {
        foreach(frame; frames)
        {
            if (frame != null)
                wTextureDestroy(frame);
            frame = null;
        }
        frames.length = 0;

        if (guiObject != null)
            wGuiObjectDestroy(guiObject);
    }
*/

    override void setFrame(int frame)
    {
        if (index == null || index.frames.length == 0)
            return;

        if (frame < 0 || frame >= index.frames.length)
            return;

        curFrame = frame;
        curTexture = index.frames[curFrame];
    }

    override int getFrame()
    {
        return curFrame;
    }

    override int getFramesCount()
    {
        if (index == null)
            return 0;

        return index.frames.length;
    }

    void draw()
    {
        if (!isVisible)
        {
//            wGuiObjectSetVisible(guiObject, false);
            return;
        }

        if (curTexture == null)
            return;
/*
        wVector2u size;
        uint pitch;
        wColorFormat format;
        wTextureGetInformation(curTexture, &size, &pitch, &format);

        if (guiObject == null)
        {

            guiObject = wGuiImageCreate (curTexture, wVector2i(size.x, size.y), true);
            wGuiImageSetScaling(guiObject, true);
        }
        else
        {
            wGuiImageSet(guiObject, curTexture);
        }
*/
        wVector2i pos = { to!int(position.x), to!int(position.y) };

        if (rotation != 0.0)
        {
            wVector2u size;
            uint pitch;
            wColorFormat format;
            wVector2i pivot = pos;

            wTextureGetInformation(curTexture, &size, &pitch, &format);
            pivot.x += to!int((size.x * scale.x) / 2.0);
            pivot.y += to!int((size.y * scale.y) / 2.0);

            wTextureDrawAdvanced(curTexture, pos, pivot, rotation, scale, true, wCOLOR4s_WHITE, wAntiAliasingMode.wAAM_OFF, false, false, false);
        }
        else
            wTextureDrawEx(curTexture, pos, scale, true);

/*
        wGuiObjectSetRelativePosition(guiObject, pos);
        wGuiObjectSetRelativeSize(guiObject, wVector2i(cast(int)(size.x * scale.x), cast(int)(size.y * scale.y)));
        wGuiObjectDraw(guiObject);
*/
    }
}

__gshared TImage2D[] g_Images2D;

void InitImages2D();


// IMPLEMENTATION

void InitImages2D()
{
    g_Images2D.length = 0;
}
