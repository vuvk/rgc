module WorldSim3D;

import core.stdc.config;
import core.stdc.stddef;

extern (C):

// pureC

// __cplusplus

enum FLOAT_DEFAULTVALUE = 7285624.0f;

alias UInt64 = ulong;
alias UInt32 = uint;
alias UInt16 = ushort;
alias UInt8 = ubyte;

alias Int64 = long;
alias Int32 = int;
alias Int16 = short;
alias Int8 = byte;

/*
typedef unsigned long int UInt64;
typedef unsigned int UInt32;
typedef unsigned short UInt16;
typedef unsigned char UInt8;

typedef signed long int Int64;
typedef signed int Int32;
typedef signed short Int16;
typedef signed char Int8;
*/

alias Float64 = double;
alias Float32 = float;

alias wImage = uint;
alias wTexture = uint;
alias wFont = uint;

alias wGuiObject = uint;

alias wMesh = uint;
alias wMeshBuffer = uint;

alias wNode = uint;

alias wMaterial = uint;
alias wSelector = uint;
alias wEmitter = uint;
alias wAffector = uint;
alias wAnimator = uint;

alias wXmlReader = uint;
alias wXmlWriter = uint;

alias wFile = uint;

alias wSoundEffect = uint;
alias wSoundFilter = uint;
alias wSound = uint;
alias wVideo = uint;

alias wPostEffect = uint;

alias wPacket = uint;

//' Mouse events
enum wMouseEventType
{
    wMET_LMOUSE_PRESSED_DOWN = 0,
    wMET_RMOUSE_PRESSED_DOWN = 1,
    wMET_MMOUSE_PRESSED_DOWN = 2,
    wMET_LMOUSE_LEFT_UP = 3,
    wMET_RMOUSE_LEFT_UP = 4,
    wMET_MMOUSE_LEFT_UP = 5,
    wMET_MOUSE_MOVED = 6,
    wMET_MOUSE_WHEEL = 7,
    wMET_LMOUSE_DOUBLE_CLICK = 8,
    wMET_RMOUSE_DOUBLE_CLICK = 9,
    wMET_MMOUSE_DOUBLE_CLICK = 10,
    wMET_LMOUSE_TRIPLE_CLICK = 11,
    wMET_RMOUSE_TRIPLE_CLICK = 12,
    wMET_MMOUSE_TRIPLE_CLICK = 13,
    wMET_COUNT = 14
}

enum wMouseButtons
{
    wMB_LEFT = 0x01,
    wMB_RIGHT = 0x02,
    wMB_MIDDLE = 0x04,
    wMB_EXTRA1 = 0x08, //not used
    wMB_EXTRA2 = 0x10, //not used
    wMB_FORCE_32_BIT = 0x7fffffff //not for use!
}

struct wVector2i
{
    Int32 x;
    Int32 y;
}

immutable wVector2i wVECTOR2i_ZERO={0,0};
immutable wVector2i wVECTOR2i_ONE={1,1};

struct wMouseEvent
{
    wMouseEventType action;
    Float32 delta;
    wVector2i position;
    bool isShift;
    bool isControl;
}

enum wKeyCode
{
    wKC_UNKNOWN = 0x0,
    wKC_LBUTTON = 0x01, // Left mouse button
    wKC_RBUTTON = 0x02, // Right mouse button
    wKC_CANCEL = 0x03, // Control-break processing
    wKC_MBUTTON = 0x04, // Middle mouse button (three-button mouse)
    wKC_XBUTTON1 = 0x05, // Windows 2000/XP: X1 mouse button
    wKC_XBUTTON2 = 0x06, // Windows 2000/XP: X2 mouse button
    wKC_BACK = 0x08, // BACKSPACE key
    wKC_TAB = 0x09, // TAB key
    wKC_CLEAR = 0x0C, // CLEAR key
    wKC_RETURN = 0x0D, // ENTER key
    wKC_SHIFT = 0x10, // SHIFT key
    wKC_CONTROL = 0x11, // CTRL key
    wKC_MENU = 0x12, // ALT key
    wKC_PAUSE = 0x13, // PAUSE key
    wKC_CAPITAL = 0x14, // CAPS LOCK key
    wKC_KANA = 0x15, // IME Kana mode
    wKC_HANGUEL = 0x15, // IME Hanguel mode (maintained for compatibility use KEY_HANGUL)
    wKC_HANGUL = 0x15, // IME Hangul mode
    wKC_JUNJA = 0x17, // IME Junja mode
    wKC_FINAL = 0x18, // IME final mode
    wKC_HANJA = 0x19, // IME Hanja mode
    wKC_KANJI = 0x19, // IME Kanji mode
    wKC_ESCAPE = 0x1B, // ESC key
    wKC_CONVERT = 0x1C, // IME convert
    wKC_NONCONVERT = 0x1D, // IME nonconvert
    wKC_ACCEPT = 0x1E, // IME accept
    wKC_MODECHANGE = 0x1F, // IME mode change request
    wKC_SPACE = 0x20, // SPACEBAR
    wKC_PRIOR = 0x21, // PAGE UP key
    wKC_NEXT = 0x22, // PAGE DOWN key
    wKC_END = 0x23, // END key
    wKC_HOME = 0x24, // HOME key
    wKC_LEFT = 0x25, // LEFT ARROW key
    wKC_UP = 0x26, // UP ARROW key
    wKC_RIGHT = 0x27, // RIGHT ARROW key
    wKC_DOWN = 0x28, // DOWN ARROW key
    wKC_SELECT = 0x29, // SELECT key
    wKC_PRINT = 0x2A, // PRINT key
    wKC_EXECUT = 0x2B, // EXECUTE key
    wKC_SNAPSHOT = 0x2C, // PRINT SCREEN key
    wKC_INSERT = 0x2D, // INS key
    wKC_DELETE = 0x2E, // DEL key
    wKC_HELP = 0x2F, // HELP key
    wKC_KEY_0 = 0x30, // 0 key
    wKC_KEY_1 = 0x31, // 1 key
    wKC_KEY_2 = 0x32, // 2 key
    wKC_KEY_3 = 0x33, // 3 key
    wKC_KEY_4 = 0x34, // 4 key
    wKC_KEY_5 = 0x35, // 5 key
    wKC_KEY_6 = 0x36, // 6 key
    wKC_KEY_7 = 0x37, // 7 key
    wKC_KEY_8 = 0x38, // 8 key
    wKC_KEY_9 = 0x39, // 9 key
    wKC_KEY_A = 0x41, // A key
    wKC_KEY_B = 0x42, // B key
    wKC_KEY_C = 0x43, // C key
    wKC_KEY_D = 0x44, // D key
    wKC_KEY_E = 0x45, // E key
    wKC_KEY_F = 0x46, // F key
    wKC_KEY_G = 0x47, // G key
    wKC_KEY_H = 0x48, // H key
    wKC_KEY_I = 0x49, // I key
    wKC_KEY_J = 0x4A, // J key
    wKC_KEY_K = 0x4B, // K key
    wKC_KEY_L = 0x4C, // L key
    wKC_KEY_M = 0x4D, // M key
    wKC_KEY_N = 0x4E, // N key
    wKC_KEY_O = 0x4F, // O key
    wKC_KEY_P = 0x50, // P key
    wKC_KEY_Q = 0x51, // Q key
    wKC_KEY_R = 0x52, // R key
    wKC_KEY_S = 0x53, // S key
    wKC_KEY_T = 0x54, // T key
    wKC_KEY_U = 0x55, // U key
    wKC_KEY_V = 0x56, // V key
    wKC_KEY_W = 0x57, // W key
    wKC_KEY_X = 0x58, // X key
    wKC_KEY_Y = 0x59, // Y key
    wKC_KEY_Z = 0x5A, // Z key
    wKC_LWIN = 0x5B, // Left Windows key (Microsoft® Natural® keyboard)
    wKC_RWIN = 0x5C, // Right Windows key (Natural keyboard)
    wKC_APPS = 0x5D, // Applications key (Natural keyboard)
    wKC_SLEEP = 0x5F, // Computer Sleep key
    wKC_NUMPAD0 = 0x60, // Numeric keypad 0 key
    wKC_NUMPAD1 = 0x61, // Numeric keypad 1 key
    wKC_NUMPAD2 = 0x62, // Numeric keypad 2 key
    wKC_NUMPAD3 = 0x63, // Numeric keypad 3 key
    wKC_NUMPAD4 = 0x64, // Numeric keypad 4 key
    wKC_NUMPAD5 = 0x65, // Numeric keypad 5 key
    wKC_NUMPAD6 = 0x66, // Numeric keypad 6 key
    wKC_NUMPAD7 = 0x67, // Numeric keypad 7 key
    wKC_NUMPAD8 = 0x68, // Numeric keypad 8 key
    wKC_NUMPAD9 = 0x69, // Numeric keypad 9 key
    wKC_MULTIPLY = 0x6A, // Multiply key
    wKC_ADD = 0x6B, // Add key
    wKC_SEPARATOR = 0x6C, // Separator key
    wKC_SUBTRACT = 0x6D, // Subtract key
    wKC_DECIMAL = 0x6E, // Decimal key
    wKC_DIVIDE = 0x6F, // Divide key
    wKC_F1 = 0x70, // F1 key
    wKC_F2 = 0x71, // F2 key
    wKC_F3 = 0x72, // F3 key
    wKC_F4 = 0x73, // F4 key
    wKC_F5 = 0x74, // F5 key
    wKC_F6 = 0x75, // F6 key
    wKC_F7 = 0x76, // F7 key
    wKC_F8 = 0x77, // F8 key
    wKC_F9 = 0x78, // F9 key
    wKC_F10 = 0x79, // F10 key
    wKC_F11 = 0x7A, // F11 key
    wKC_F12 = 0x7B, // F12 key
    wKC_F13 = 0x7C, // F13 key
    wKC_F14 = 0x7D, // F14 key
    wKC_F15 = 0x7E, // F15 key
    wKC_F16 = 0x7F, // F16 key
    wKC_F17 = 0x80, // F17 key
    wKC_F18 = 0x81, // F18 key
    wKC_F19 = 0x82, // F19 key
    wKC_F20 = 0x83, // F20 key
    wKC_F21 = 0x84, // F21 key
    wKC_F22 = 0x85, // F22 key
    wKC_F23 = 0x86, // F23 key
    wKC_F24 = 0x87, // F24 key
    wKC_NUMLOCK = 0x90, // NUM LOCK key
    wKC_SCROLL = 0x91, // SCROLL LOCK key
    wKC_LSHIFT = 0xA0, // Left SHIFT key
    wKC_RSHIFT = 0xA1, // Right SHIFT key
    wKC_LCONTROL = 0xA2, // Left CONTROL key
    wKC_RCONTROL = 0xA3, // Right CONTROL key
    wKC_LMENU = 0xA4, // Left MENU key
    wKC_RMENU = 0xA5, // Right MENU key
    wKC_BROWSER_BACK = 0xA6, // Browser Back key
    wKC_BROWSER_FORWARD = 0xA7, // Browser Forward key
    wKC_BROWSER_REFRESH = 0xA8, // Browser Refresh key
    wKC_BROWSER_STOP = 0xA9, // Browser Stop key
    wKC_BROWSER_SEARCH = 0xAA, // Browser Search key
    wKC_BROWSER_FAVORITES = 0xAB, // Browser Favorites key
    wKC_BROWSER_HOME = 0xAC, // Browser Start and Home key
    wKC_VOLUME_MUTE = 0xAD, // Volume Mute key
    wKC_VOLUME_DOWN = 0xAE, // Volume Down key
    wKC_VOLUME_UP = 0xAF, // Volume Up key
    wKC_MEDIA_NEXT_TRACK = 0xB0, // Next Track key
    wKC_MEDIA_PREV_TRACK = 0xB1, // Previous Track key
    wKC_MEDIA_STOP = 0xB2, // Stop Media key
    wKC_MEDIA_PLAY_PAUSE = 0xB3, // Play/Pause Media key
    wKC_OEM_1 = 0xBA, // for US    ";:"
    wKC_PLUS = 0xBB, // Plus Key   "+"
    wKC_COMMA = 0xBC, // Comma Key  ","
    wKC_MINUS = 0xBD, // Minus Key  "-"
    wKC_PERIOD = 0xBE, // Period Key "."
    wKC_OEM_2 = 0xBF, // for US    "/?"
    wKC_OEM_3 = 0xC0, // for US    "`~"
    wKC_OEM_4 = 0xDB, // for US    "[{"
    wKC_OEM_5 = 0xDC, // for US    "\|"
    wKC_OEM_6 = 0xDD, // for US    "]}"
    wKC_OEM_7 = 0xDE, // for US    "'""
    wKC_OEM_8 = 0xDF, // None
    wKC_OEM_AX = 0xE1, // for Japan "AX"
    wKC_OEM_102 = 0xE2, // "<>" or "\|"
    wKC_ATTN = 0xF6, // Attn key
    wKC_CRSEL = 0xF7, // CrSel key
    wKC_EXSEL = 0xF8, // ExSel key
    wKC_EREOF = 0xF9, // Erase EOF key
    wKC_PLAY = 0xFA, // Play key
    wKC_ZOOM = 0xFB, // Zoom key
    wKC_PA1 = 0xFD, // PA1 key
    wKC_OEM_CLEAR = 0xFE, // Clear key
    wKC_NONE = 0xFF, // usually no key mapping, but some laptops use it for fn key

    wKC_KEY_CODES_COUNT = 0x100 // this is not a key, but the amount of keycodes there are.
}

enum wKeyDirection
{
    wKD_UP = 0,
    wKD_DOWN = 1
}

struct wKeyEvent
{
    wKeyCode key;
    wKeyDirection direction;
    //wchar_t keyChar; //Character corresponding to the key (0, if not a character)
    bool isShift;
    bool isControl;
}

enum wKeyAction
{
    wKA_MOVE_FORWARD = 0,
    wKA_MOVE_BACKWARD = 1,
    wKA_STRAFE_LEFT = 2,
    wKA_STRAFE_RIGHT = 3,
    wKA_JUMP_UP = 4,
    wKA_COUNT = 5,
    wKA_FORCE_32BIT = 0x7fffffff
}

struct wKeyMap
{
    wKeyAction Action;
    wKeyCode KeyCode;
}

immutable wKeyMap[8] wKeyMapDefault = [
    {wKeyAction.wKA_MOVE_FORWARD,  wKeyCode.wKC_KEY_W},
    {wKeyAction.wKA_MOVE_FORWARD,  wKeyCode.wKC_UP   },
    {wKeyAction.wKA_MOVE_BACKWARD, wKeyCode.wKC_KEY_S},
    {wKeyAction.wKA_MOVE_BACKWARD, wKeyCode.wKC_DOWN },
    {wKeyAction.wKA_STRAFE_LEFT,   wKeyCode.wKC_KEY_A},
    {wKeyAction.wKA_STRAFE_LEFT,   wKeyCode.wKC_LEFT },
    {wKeyAction.wKA_STRAFE_RIGHT,  wKeyCode.wKC_KEY_D},
    {wKeyAction.wKA_STRAFE_RIGHT,  wKeyCode.wKC_RIGHT}
];

enum wJoystickPovHat
{
    wJPH_PRESENT = 0, //A hat is definitely present.
    wJPH_ABSENT = 1, //A hat is definitely not present.
    wJPH_UNKNOWN = 2 //The presence or absence of a hat cannot be determined.
}

struct wJoystickInfo
{
    //Note: with a Linux device, the POV hat (if any) will use two axes.
    //These will be included in this count.
    UInt32 Axes; //The number of axes that the joystick has, i.e. X, Y, Z, R, U, V.

    UInt32 Buttons; //The number of buttons that the joystick has.

    //This is an internal WS3D index; it does not map directly to any particular hardware joystick.
    UInt8 joyId; //The ID of the joystick.

    //char joyName0[256];//
    const(char)* joyName;

    //A Windows device will identify the presence or absence or the POV hat.
    //A Linux device cannot, and will always return wJPH_UNKNOWN.
    //Mac OSX not supported!
    wJoystickPovHat PovHat; //An indication of whether the joystick has a POV hat.
}

enum
{
    NUMBER_OF_BUTTONS = 32,
    AXIS_X = 0,
    AXIS_Y = 1,
    AXIS_Z = 2,
    AXIS_R = 3,
    AXIS_U = 4,
    AXIS_V = 5,
    NUMBER_OF_AXES = 6
}

struct wJoystickEvent
{
    UInt8 joyId; //The ID of the joystick which generated this event.
    bool[NUMBER_OF_BUTTONS] ButtonStates; //A helper function to check if a button is pressed
    short[NUMBER_OF_AXES] Axis;
    UInt16 POV;
}

//' Event types
enum wEventType
{
    wET_GUI_EVENT = 0,
    wET_MOUSE_INPUT_EVENT = 1,
    wET_KEY_INPUT_EVENT = 2,
    wET_JOYSTICK_INPUT_EVENT = 3,
    wET_LOG_TEXT_EVENT = 4,
    wET_USER_EVENT = 5,
    wET_FORCE_32_BIT = 0x7fffffff
}

enum wGuiCallerType
{
    wGCT_ELEMENT_FOCUS_LOST = 0, //A gui element has lost its focus.GUIEvent.Caller is losing the focus to GUIEvent.Element.
    //If the event is absorbed then the focus will not be changed.
    wGCT_ELEMENT_FOCUSED = 1, //A gui element has got the focus.
    //If the event is absorbed then the focus will not be changed.
    wGCT_ELEMENT_HOVERED = 2, //The mouse cursor hovered over a gui element.
    //If an element has sub-elements you also get this message for the subelements
    wGCT_ELEMENT_LEFT = 3, //The mouse cursor left the hovered element.
    //If an element has sub-elements you also get this message for the subelements
    wGCT_ELEMENT_CLOSED = 4, //An element would like to close.
    //Windows and context menus use this event when they would like to close, this can be cancelled by absorbing the event.
    wGCT_BUTTON_CLICKED = 5, //A button was clicked.

    wGCT_SCROLL_BAR_CHANGED = 6, //A scrollbar has changed its position.

    wGCT_CHECKBOX_CHANGED = 7, //A checkbox has changed its check state.

    wGCT_LISTBOX_CHANGED = 8, //A new item in a listbox was selected.
    //NOTE: You also get this event currently when the same item was clicked again after more than 500 ms.
    wGCT_LISTBOX_SELECTED_AGAIN = 9, //An item in the listbox was selected, which was already selected.
    //NOTE: You get the event currently only if the item was clicked again within 500 ms or selected by "enter" or "space".
    wGCT_FILE_SELECTED = 10, //A file has been selected in the file dialog.
    wGCT_DIRECTORY_SELECTED = 11, //A directory has been selected in the file dialog.
    wGCT_FILE_CHOOSE_DIALOG_CANCELLED = 12, //A file open dialog has been closed without choosing a file.
    wGCT_MESSAGEBOX_YES = 13, //'Yes' was clicked on a messagebox
    wGCT_MESSAGEBOX_NO = 14, //'No' was clicked on a messagebox
    wGCT_MESSAGEBOX_OK = 15, //'OK' was clicked on a messagebox
    wGCT_MESSAGEBOX_CANCEL = 16, //'Cancel' was clicked on a messagebox
    wGCT_EDITBOX_ENTER = 17, //In an editbox 'ENTER' was pressed.
    wGCT_EDITBOX_CHANGED = 18, //The text in an editbox was changed. This does not include automatic changes in text-breaking.
    wGCT_EDITBOX_MARKING_CHANGED = 19, //The marked area in an editbox was changed.
    wGCT_TAB_CHANGED = 20, //The tab was changed in an tab control.
    wGCT_MENU_ITEM_SELECTED = 21, //A menu item was selected in a (context) menu.
    wGCT_COMBO_BOX_CHANGED = 22, //The selection in a combo box has been changed.
    wGCT_SPINBOX_CHANGED = 23, //The value of a spin box has changed.
    wGCT_TABLE_CHANGED = 24, //A table has changed.
    wGCT_TABLE_HEADER_CHANGED = 25,
    wGCT_TABLE_SELECTED_AGAIN = 26,
    wGCT_TREEVIEW_NODE_DESELECT = 27, //A tree view node lost selection. See IGUITreeView::getLastEventNode().
    wGCT_TREEVIEW_NODE_SELECT = 28, //A tree view node was selected. See IGUITreeView::getLastEventNode().
    wGCT_TREEVIEW_NODE_EXPAND = 29, //A tree view node was expanded. See IGUITreeView::getLastEventNode().
    wGCT_TREEVIEW_NODE_COLLAPSE = 30, //A tree view node was collapsed. See IGUITreeView::getLastEventNode().
    wGCT_RADIOBUTTONGROUP_CHANGED = 31, //new
    wGCT_RADIOCHECKBOXGROUP_CHANGED = 32, //new
    wGCT_COUNT = 33 //No real event. Just for convenience to get number of events.
}

struct wGuiEvent
{
    Int32 id;
    const(char)* name;
    wGuiCallerType event;
    wVector2i position;
}

enum wGuiMessageBoxFlags
{
    wGMBF_OK = 0x1, //Flag for the ok button.
    wGMBF_CANCEL = 0x2, //Flag for the cancel button.
    wGMBF_YES = 0x4, //Flag for the yes button.
    wGMBF_NO = 0x8, //Flag for the no button.
    wGMBF_FORCE_32BIT = 0x7fffffff //This value is not used. It only forces this enumeration to compile in 32 bit.
}

enum wFogType
{
    wFT_EXP = 0,
    wFT_LINEAR = 1,
    wFT_EXP2 = 2
}

struct wVector2f
{
    Float32 x;
    Float32 y;
}

immutable wVector2f wVECTOR2f_ZERO={0,0};
immutable wVector2f wVECTOR2f_ONE={1,1};

struct wVector2u
{
    UInt32 x;
    UInt32 y;
}

immutable wVector2u wVECTOR2u_ZERO={0,0};
immutable wVector2u wVECTOR2u_ONE={1,1};

immutable wVector2u wDEFAULT_SCREENSIZE = {800, 600};

struct wVector3f
{
    Float32 x;
    Float32 y;
    Float32 z;
}

immutable wVector3f wVECTOR3f_ZERO       = {0,0,0};
immutable wVector3f wVECTOR3f_ONE        = {1.0f,1.0f,1.0f};
immutable wVector3f wVECTOR3f_UP         = {0.0f,1.0f,0.0f};
immutable wVector3f wVECTOR3f_DOWN       = {0.0f,-1.0f,0.0f};
immutable wVector3f wVECTOR3f_FORWARD    = { 0.0f, 0.0f, -1.0f};
immutable wVector3f wVECTOR3f_BACKWARD   = { 0.0f, 0.0f, 1.0f};
immutable wVector3f wVECTOR3f_RIGHT      = { 1.0f, 0.0f,  0.0f};
immutable wVector3f wVECTOR3f_LEFT       = {-1.0f, 0.0f,  0.0f};

struct wVector3i
{
    Int32 x;
    Int32 y;
    Int32 z;
}

immutable wVector3i wVECTOR3i_ZERO       = {0,0,0};
immutable wVector3i wVECTOR3i_ONE        = {1,1,1};
immutable wVector3i wVECTOR3i_UP         = {0,1,0};
immutable wVector3i wVECTOR3i_DOWN       = {0,-1,0};
immutable wVector3i wVECTOR3i_FORWARD    = { 0,0,-1};
immutable wVector3i wVECTOR3i_BACKWARD   = {0,0,1};
immutable wVector3i wVECTOR3i_RIGHT      = { 1,0,0};
immutable wVector3i wVECTOR3i_LEFT       = {-1,0,0};

struct wVector3u
{
    UInt32 x;
    UInt32 y;
    UInt32 z;
}

immutable wVector3u wVECTOR3u_ZERO={0,0,0};
immutable wVector3u wVECTOR3u_ONE={1,1,1};

struct wColor4s
{
    UInt16 alpha;
    UInt16 red;
    UInt16 green;
    UInt16 blue;
}

immutable __gshared wColor4s wCOLOR4s_ZERO={0,0,0,0};

immutable __gshared wColor4s wCOLOR4s_WHITE={255,255,255,255};

immutable __gshared wColor4s wCOLOR4s_DARKGREY={255,64,64,64};
immutable __gshared wColor4s wCOLOR4s_GREY={255,128,128,128};
immutable __gshared wColor4s wCOLOR4s_SILVER={255,192,192,192};

immutable __gshared wColor4s wCOLOR4s_BLACK={255,0,0,0};

immutable __gshared wColor4s wCOLOR4s_RED={255,255,0,0};
immutable __gshared wColor4s wCOLOR4s_DARKRED={255,140,0,0};
immutable __gshared wColor4s wCOLOR4s_MAROON={255,128,0,0};

immutable __gshared wColor4s wCOLOR4s_GREEN={255,0,255,0};
immutable __gshared wColor4s wCOLOR4s_LIME={255,250,128,114};
immutable __gshared wColor4s wCOLOR4s_DARKGREEN={255,0,100,0};
immutable __gshared wColor4s wCOLOR4s_OLIVE={255,240,128,128};

immutable __gshared wColor4s wCOLOR4s_BLUE={255,0,0,255};
immutable __gshared wColor4s wCOLOR4s_DARKBLUE={255,0,0,139};
immutable __gshared wColor4s wCOLOR4s_NAVY={255,0,0,128};
immutable __gshared wColor4s wCOLOR4s_SKYBLUE={255,135,206,235};

immutable __gshared wColor4s wCOLOR4s_MAGENTA={255,255,0,255};
immutable __gshared wColor4s wCOLOR4s_PINK={255,255,192,203};
immutable __gshared wColor4s wCOLOR4s_DEEPPINK={255,255,20,147};
immutable __gshared wColor4s wCOLOR4s_INDIGO={255,75,0,130};

immutable __gshared wColor4s wCOLOR4s_YELLOW={255,255,255,0};
immutable __gshared wColor4s wCOLOR4s_GOLD={255,255,215,0};
immutable __gshared wColor4s wCOLOR4s_KHAKI={255,245,230,140};

immutable __gshared wColor4s wCOLOR4s_ORANGE={255,255,68,0};
immutable __gshared wColor4s wCOLOR4s_DARKORANGE={255,255,140,0};
immutable __gshared wColor4s wCOLOR4s_ORANGERED={255,255,69,0};

struct wColor4f
{
    Float32 alpha;
    Float32 red;
    Float32 green;
    Float32 blue;
}

immutable wColor4f wCOLOR4f_WHITE={1.0f,1.0f,1.0f,1.0f};
immutable wColor4f wCOLOR4f_BLACK={0.0f,0.0f,0.0f,0.0f};

struct wColor3s
{
    UInt16 red;
    UInt16 green;
    UInt16 blue;
}

immutable wColor3s wCOLOR3s_WHITE={255,255,255};
immutable wColor3s wCOLOR3s_BLACK={0,0,0};

struct wColor3f
{
    Float32 red;
    Float32 green;
    Float32 blue;
}

immutable wColor3f wCOLOR3f_WHITE={1.0f,1.0f,1.0f};
immutable wColor3f wCOLOR3f_BLACK={0,0,0};

struct wVert
{
    wVector3f vertPos;
    wVector3f vertNormal;
    wColor4s vertColor; // The 32bit ARGB color of the vertex
    wVector2f texCoords;
}

///STRUCTURES FOR PARTICLE EMITTERS///
struct wParticleEmitter
{
    wVector3f direction;
    UInt32 minParticlesPerSecond;
    UInt32 maxParticlesPerSecond;
    wColor4s minStartColor;
    wColor4s maxStartColor;
    UInt32 lifeTimeMin;
    UInt32 lifeTimeMax;
    Int32 maxAnglesDegrees;
    wVector2f minStartSize;
    wVector2f maxStartSize;
    // __cplusplus
}

struct wParticleCylinderEmitter
{
    wVector3f center;
    Float32 length;
    wVector3f normal;

    bool getOutlineOnly;
    // __cplusplus
    Float32 radius;
}

struct wParticleMeshEmitter
{
    wMesh* mesh;
    bool useNormalDirection;
    Float32 normalDirectionModifier;
    bool everyMeshVertex;
    // __cplusplus
}

struct wParticleRingEmitter
{
    wVector3f center;
    Float32 radius;
    Float32 ringThickness;
}

struct wParticleSphereEmitter
{
    wVector3f center;
    Float32 radius;
}

///STRUCTURES FOR PARTICLE AFFECTORS///
struct wParticleAttractionAffector
{
    wVector3f point;
    bool attract;
    bool affectX;
    bool affectY;
    bool affectZ;
    // __cplusplus
}

struct wParticleColorMorphAffector
{
    wColor4s* colorsList;
    UInt32 colorsCount;
    UInt32* timesList;
    UInt32 timesCount;

    bool smooth;
    // __cplusplus
}

struct wParticlePushAffector
{
    Float32 furthestDistance;
    Float32 nearestDistance;
    Float32 columnDistance;
    wVector3f center;
    wVector3f strength;
    bool distant;
}

struct wParticleSplineAffector
{
    wVector3f* points;
    UInt32 pointsCount;
    Float32 speed;
    Float32 tightness;
    Float32 attraction;
    bool deleteAtFinalPoint;
}

struct wTriangle
{
    wVector3f pointA;
    wVector3f pointB;
    wVector3f pointC;
}

immutable __gshared wTriangle wTRIANGLE3f_ZERO={wVECTOR3f_ZERO,wVECTOR3f_ZERO,wVECTOR3f_ZERO};

///STRUCTURE FOR NODE ANIMATORS///
///COLLISION RESPONSE ANIMATOR///
struct wAnimatorCollisionResponse
{
    ///read/write//
    wSelector* world;
    wNode* targetNode;

    wVector3f ellipsoidRadius;
    wVector3f gravity;
    bool animateTarget;
    wVector3f ellipsoidTranslation;
    // __cplusplus

    ///only for read//
    wVector3f collisionPoint;
    wVector3f collisionResultPosition;
    wTriangle collisionTriangle;
    wNode* collisionNode;
    bool isFalling;
    bool collisionOccured;
}

/* storage for information pertaining to a shader constant
 */
//typedef struct wConstant wConstant;
struct tag_wConstant
{
    tag_wConstant* next;
    const(char)* name;
    Int32 address;
    Int32 preset;
    const(Float32)* data;
    Int32 count;
}

alias wConstant = tag_wConstant;

enum wDriverTypes
{
    wDRT_NULL = 0, //' a NULL device with no display
    wDRT_SOFTWARE = 1, //' WorldSim3Ds default software renderer
    wDRT_BURNINGS_VIDEO = 2, //     ' An improved quality software renderer
    wDRT_OPENGL = 3, //' hardware accelerated OpenGL renderer
    wDRT_DIRECT3D9 = 4, //' hardware accelerated DirectX 9 renderer
    wDRT_CHOICE_CONSOLE = 6
}

enum wDeviceTypes
{
    wDT_BEST = 0, //'This selection allows Irrlicht to choose the best device from the ones available.
    wDT_WIN32 = 1, //'A device native to Microsoft Windows. This device uses the Win32 API and works in all versions of Windows.
    wDT_WINCE = 2, //'A device native to Windows CE devices.This device works on Windows Mobile, Pocket PC and Microsoft SmartPhone devices
    wDT_X11 = 3, //'A device native to Unix style operating systems. This device uses the X11 windowing system and works in Linux,
    //'Solaris, FreeBSD, OSX and other operating systems which support X11.
    wDT_OSX = 4, //'A device native to Mac OSX. This device uses Apple's Cocoa API and works in Mac OSX 10.2 and above.
    wDT_SDL = 5, //'A device which uses Simple DirectMedia Layer. The SDL device works under all platforms supported by SDL
    wDT_FRAMEBUFFER = 6, //'A device for raw framebuffer access.Best used with embedded devices and mobile systems.
    //'Does not need X11 or other graphical subsystems. May support hw-acceleration via OpenGL-ES for FBDirect
    wDT_CONSOLE = 7 //'A simple text only device supported by all platforms. This device allows applications to run from the command line
    //'without opening a window. It can render the output of the software drivers to the console as ASCII.
    //'It only supports mouse and keyboard in Windows operating systems.
}

//' Vertex shader program versions
enum wVertexShaderVersion
{
    wVSV_1_1 = 0,
    wVSV_2_0 = 1,
    wVSV_2_a = 2,
    wVSV_3_0 = 3
}

//' Pixel shader program versions
enum wPixelShaderVersion
{
    wPSV_1_1 = 0,
    wPSV_1_2 = 1,
    wPSV_1_3 = 2,
    wPSV_1_4 = 3,
    wPSV_2_0 = 4,
    wPSV_2_a = 5,
    wPSV_2_b = 6,
    wPSV_3_0 = 7
}

enum wGeometryShaderVersion
{
    wGSV_4_0 = 0,
    wGSV_COUNT = 1
}

//For Geometry shaders
enum wPrimitiveType
{
    wPT_POINTS = 0, //All vertices are non-connected points.
    wPT_LINE_STRIP = 1, //All vertices form a single connected line.
    wPT_LINE_LOOP = 2, //Just as LINE_STRIP, but the last and the first vertex is also connected.
    wPT_LINES = 3, //Every two vertices are connected creating n/2 lines.
    wPT_TRIANGLE_STRIP = 4, //After the first two vertices each vertex defines a new triangle. Always the two last and the new one form a new triangle.
    wPT_TRIANGLE_FAN = 5, //After the first two vertices each vertex defines a new triangle. All around the common first vertex.
    wPT_TRIANGLES = 6, //Explicitly set all vertices for each triangle.
    wPT_QUAD_STRIP = 7, //After the first two vertices each further tw vetices create a quad with the preceding two.
    wPT_QUADS = 8, //Every four vertices create a quad.
    wPT_POLYGON = 9, //Just as LINE_LOOP, but filled.
    wPT_POINT_SPRITES = 10, //}wPrimitiveType
    wPT_COUNT = 11 //Not for use!!!
}

enum wShaderConstants
{
    wSC_NO_PRESET = 0,
    wSC_INVERSE_WORLD = 1,
    wSC_WORLD_VIEW_PROJECTION = 2,
    wSC_CAMERA_POSITION = 3,
    wSC_TRANSPOSED_WORLD = 4
}

immutable __gshared Float32 wShaderConstZero  = 0;
immutable __gshared Float32 wShaderConstOne   = 1;
immutable __gshared Float32 wShaderConstTwo   = 2;
immutable __gshared Float32 wShaderConstThree = 3;

enum wVideoFeatureQuery
{
    //! Is driver able to render to a surface?
    wVDF_RENDER_TO_TARGET = 0,
    //! Is hardeware transform and lighting supported?
    wVDF_HARDWARE_TL = 1,
    //! Are multiple textures per material possible?
    wVDF_MULTITEXTURE = 2,
    //! Is driver able to render with a bilinear filter applied?
    wVDF_BILINEAR_FILTER = 3,
    //! Can the driver handle mip maps?
    wVDF_MIP_MAP = 4,
    //! Can the driver update mip maps automatically?
    wVDF_MIP_MAP_AUTO_UPDATE = 5,
    //! Are stencilbuffers switched on and does the device support stencil buffers?
    wVDF_STENCIL_BUFFER = 6,
    //! Is Vertex Shader 1.1 supported?
    wVDF_VERTEX_SHADER_1_1 = 7,
    //! Is Vertex Shader 2.0 supported?
    wVDF_VERTEX_SHADER_2_0 = 8,
    //! Is Vertex Shader 3.0 supported?
    wVDF_VERTEX_SHADER_3_0 = 9,
    //! Is Pixel Shader 1.1 supported?
    wVDF_PIXEL_SHADER_1_1 = 10,
    //! Is Pixel Shader 1.2 supported?
    wVDF_PIXEL_SHADER_1_2 = 11,
    //! Is Pixel Shader 1.3 supported?
    wVDF_PIXEL_SHADER_1_3 = 12,
    //! Is Pixel Shader 1.4 supported?
    wVDF_PIXEL_SHADER_1_4 = 13,
    //! Is Pixel Shader 2.0 supported?
    wVDF_PIXEL_SHADER_2_0 = 14,
    //! Is Pixel Shader 3.0 supported?
    wVDF_PIXEL_SHADER_3_0 = 15,
    //! Are ARB vertex programs v1.0 supported?
    wVDF_ARB_VERTEX_PROGRAM_1 = 16,
    //! Are ARB fragment programs v1.0 supported?
    wVDF_ARB_FRAGMENT_PROGRAM_1 = 17,
    //! Is GLSL supported?
    wVDF_ARB_GLSL = 18,
    //! Is HLSL supported?
    wVDF_HLSL = 19,
    //! Are non-square textures supported?
    wVDF_TEXTURE_NSQUARE = 20,
    //! Are non-power-of-two textures supported?
    wVDF_TEXTURE_NPOT = 21,
    //! Are framebuffer objects supported?
    wVDF_FRAMEBUFFER_OBJECT = 22,
    //! Are vertex buffer objects supported?
    wVDF_VERTEX_BUFFER_OBJECT = 23,
    //! Supports Alpha To Coverage
    wVDF_ALPHA_TO_COVERAGE = 24,
    //! Supports Color masks (disabling color planes in output)
    wVDF_COLOR_MASK = 25,
    //! Supports multiple render targets at once
    wVDF_MULTIPLE_RENDER_TARGETS = 26,
    //! Supports separate blend settings for multiple render targets
    wVDF_MRT_BLEND = 27,
    //! Supports separate color masks for multiple render targets
    wVDF_MRT_COLOR_MASK = 28,
    //! Supports separate blend functions for multiple render targets
    wVDF_MRT_BLEND_FUNC = 29,
    //! Supports geometry shaders
    wVDF_GEOMETRY_SHADER = 30,
    //! Supports occlusion queries
    wVDF_OCCLUSION_QUERY = 31,
    //! Supports polygon offset/depth bias for avoiding z-fighting
    wVDF_POLYGON_OFFSET = 32,
    //! Support for different blend functions. Without, only ADD is available
    wVDF_BLEND_OPERATIONS = 33,
    //! Support for separate blending for RGB and Alpha.
    wVDF_BLEND_SEPARATE = 34,
    //! Support for texture coord transformation via texture matrix
    wVDF_TEXTURE_MATRIX = 35,
    //! Support for DXTn compressed textures.
    wVDF_TEXTURE_COMPRESSED_DXT = 36,
    //! Support for PVRTC compressed textures.
    wVDF_TEXTURE_COMPRESSED_PVRTC = 37,
    //! Support for PVRTC2 compressed textures.
    wVDF_TEXTURE_COMPRESSED_PVRTC2 = 38,
    //! Support for ETC1 compressed textures.
    wVDF_TEXTURE_COMPRESSED_ETC1 = 39,
    //! Support for ETC2 compressed textures.
    wVDF_TEXTURE_COMPRESSED_ETC2 = 40,
    //! Support for cube map textures.
    wVDF_TEXTURE_CUBEMAP = 41,
    //! Only used for counting the elements of this enum
    wVDF_COUNT = 42
}

enum wLoggingLevel
{
    wLL_NONE = 0, //'Logs with ELL_NONE will never be filtered. And used as filter it will remove all logging except ELL_NONE messages.
    wLL_DEBUG = 1, //'Used for printing information helpful in debugging.
    wLL_ERROR = 2, //'Something did go wrong.
    wLL_INFORMATION = 3, //'Useful information to print. For example hardware infos or something started/stopped.
    wLL_WARNING = 4 //'Warnings that something isn't as expected and can cause oddities.
}

enum wMaterialFlags
{
    wMF_WIREFRAME = 0, //' Render as wireframe outline.
    wMF_POINTCLOUD = 1, //' Draw a point cloud instead of polygons.
    wMF_GOURAUD_SHADING = 2, //' Render smoothly across polygons.
    wMF_LIGHTING = 3, //' Material is effected by lighting.
    wMF_ZBUFFER = 4, //' Enable z-buffer.
    wMF_ZWRITE_ENABLE = 5, //' Can write as well as read z-buffer.
    wMF_BACK_FACE_CULLING = 6, //' Cull polygons facing away.
    wMF_FRONT_FACE_CULLING = 7, //' Cull polygons facing front.
    wMF_BILINEAR_FILTER = 8, //' Enable bilinear filtering.
    wMF_TRILINEAR_FILTER = 9, //' Enable trilinear filtering.
    wMF_ANISOTROPIC_FILTER = 10, //' Reduce blur in distant textures.
    wMF_FOG_ENABLE = 11, //' Enable fogging in the distance.
    wMF_NORMALIZE_NORMALS = 12, //' Use when scaling dynamically lighted models.
    wMF_TEXTURE_WRAP = 13, //' Gives access to all layers texture wrap settings. Overwrites separate layer settings.
    wMF_ANTI_ALIASING = 14, //' Anti-aliasing mode.
    wMF_COLOR_MASK = 15, //' ColorMask bits, for enabling the color planes.
    wMF_COLOR_MATERIAL = 16 //' ColorMaterial enum for vertex color interpretation.
}

enum wMaterialTypes
{
    wMT_SOLID = 0,
    wMT_SOLID_2_LAYER = 1,
    wMT_LIGHTMAP = 2,
    wMT_LIGHTMAP_ADD = 3,
    wMT_LIGHTMAP_M2 = 4,
    wMT_LIGHTMAP_M4 = 5,
    wMT_LIGHTMAP_LIGHTING = 6,
    wMT_LIGHTMAP_LIGHTING_M2 = 7,
    wMT_LIGHTMAP_LIGHTING_M4 = 8,
    wMT_DETAIL_MAP = 9,
    wMT_SPHERE_MAP = 10,
    wMT_REFLECTION_2_LAYER = 11,
    wMT_TRANSPARENT_ADD_COLOR = 12,
    wMT_TRANSPARENT_ALPHA_CHANNEL = 13,
    wMT_TRANSPARENT_ALPHA_CHANNEL_REF = 14,
    wMT_TRANSPARENT_VERTEX_ALPHA = 15,
    wMT_TRANSPARENT_REFLECTION_2_LAYER = 16,
    wMT_NORMAL_MAP_SOLID = 17,
    wMT_NORMAL_MAP_TRANSPARENT_ADD_COLOR = 18,
    wMT_NORMAL_MAP_TRANSPARENT_VERTEX_ALPHA = 19,
    wMT_PARALLAX_MAP_SOLID = 20,
    wMT_PARALLAX_MAP_TRANSPARENT_ADD_COLOR = 21,
    wMT_PARALLAX_MAP_TRANSPARENT_VERTEX_ALPHA = 22,
    wMT_ONETEXTURE_BLEND = 23,
    wMT_FOUR_DETAIL_MAP = 24,
    wMT_TRANSPARENT_ADD_ALPHA_CHANNEL_REF = 25,
    wMT_TRANSPARENT_ADD_ALPHA_CHANNEL = 26,
    wMT_FORCE_32BIT = 0x7fffffff
}

enum wColorMaterial
{
    wCM_NONE = 0, //' Dont use vertex color for lighting
    wCM_DIFFUSE = 1, //' Use vertex color for diffuse light, (default)
    wCM_AMBIENT = 2, //' Use vertex color for ambient light
    wCM_EMISSIVE = 3, //' Use vertex color for emissive light
    wCM_SPECULAR = 4, //' Use vertex color for specular light
    wCM_DIFFUSE_AND_AMBIENT = 5 //' Use vertex color for both diffuse and ambient light
}

enum wBlendFactor
{
    wBF_ZERO = 0,
    wBF_ONE = 1,
    wBF_DST_COLOR = 2,
    wBF_ONE_MINUS_DST_COLOR = 3,
    wBF_SRC_COLOR = 4,
    wBF_ONE_MINUS_SRC_COLOR = 5,
    wBF_SRC_ALPHA = 6,
    wBF_ONE_MINUS_SRC_ALPHA = 7,
    wBF_DST_ALPHA = 8,
    wBF_ONE_MINUS_DST_ALPHA = 9,
    wBF_SRC_ALPHA_SATURATE = 10
}

enum wBlendOperation
{
    wBO_SCREEN = 0,
    wBO_ADD = 1,
    wBO_SUBTRACT = 2,
    wBO_MULTIPLY = 3,
    wBO_DIVIDE = 4
}

enum wTextureCreationFlag
{
    wTCF_ALWAYS_16_BIT = 0x00000001, //' Forces the driver to create 16 bit textures always, independent of which format the file on disk has. When choosing this you may loose some color detail, but gain much speed and memory. 16 bit textures can be transferred twice as fast as 32 bit textures and only use half of the space in memory. When using this flag, it does not make sense to use the flags wTCF_ALWAYS_32_BIT, wTCF_OPTIMIZED_FOR_QUALITY, or wTCF_OPTIMIZED_FOR_SPEED at the same time.
    wTCF_ALWAYS_32_BIT = 0x00000002, //' Forces the driver to create 32 bit textures always, independent of which format the file on disk has. Please note that some drivers (like the software device) will ignore this, because they are only able to create and use 16 bit textures. When using this flag, it does not make sense to use the flags wTCF_ALWAYS_16_BIT, wTCF_OPTIMIZED_FOR_QUALITY, or wTCF_OPTIMIZED_FOR_SPEED at the same time.
    wTCF_OPTIMIZED_FOR_QUALITY = 0x00000004, //' Lets the driver decide in which format the textures are created and tries to make the textures look as good as possible. Usually it simply chooses the format in which the texture was stored on disk. When using this flag, it does not make sense to use the flags wTCF_ALWAYS_16_BIT, wTCF_ALWAYS_32_BIT, or wTCF_OPTIMIZED_FOR_SPEED at the same time.
    wTCF_OPTIMIZED_FOR_SPEED = 0x00000008, //' Lets the driver decide in which format the textures are created and tries to create them maximizing render speed. When using this flag, it does not make sense to use the flags wTCF_ALWAYS_16_BIT, wTCF_ALWAYS_32_BIT, or wTCF_OPTIMIZED_FOR_QUALITY, at the same time.
    wTCF_CREATE_MIP_MAPS = 0x00000010, //' Automatically creates mip map levels for the textures.
    wTCF_NO_ALPHA_CHANNEL = 0x00000020, //' Discard any alpha layer and use non-alpha color format.
    wTCF_ALLOW_NON_POWER_2 = 0x00000040 //' Allow non power of two dimention textures
}

enum wTextureClamp
{
    wTC_REPEAT = 0,
    wTC_CLAMP = 1,
    wTC_CLAMP_TO_EDGE = 2,
    wTC_CLAMP_TO_BORDER = 3,
    wTC_MIRROR = 4,
    wTC_MIRROR_CLAMP = 5,
    wTC_MIRROR_CLAMP_TO_EDGE = 6,
    wTC_MIRROR_CLAMP_TO_BORDER = 7
}

enum wColorPlane
{
    wCP_NONE = 0,
    wCP_ALPHA = 1,
    wCP_RED = 2,
    wCP_GREEN = 4,
    wCP_BLUE = 8,
    wCP_RGB = 14,
    wCP_ALL = 15
}

//'Antialiasing mode for matrials
enum wAntiAliasingMode
{
    wAAM_OFF = 0,
    wAAM_SIMPLE = 1,
    wAAM_QUALITY = 3,
    wAAM_LINE_SMOOTH = 4,
    wAAM_POINT_SMOOTH = 8,
    wAAM_FULL_BASIC = 15,
    wAAM_ALPHA_TO_COVERAGE = 16
}

enum wCullingState
{
    wCS_OFF = 0,
    wCS_BOX = 1,
    wCS_FRUSTUM_BOX = 2,
    wCS_FRUSTUM_SPHERE = 4
}

enum wSceneNodeType
{
    //' for irr-file reader info
    wSNT_CUBE = 1, //' "cube"
    wSNT_SPHERE = 2, //' "sphere"
    wSNT_TEXT = 3, //' "text"
    wSNT_WATER_SURFACE = 4, //' "waterSurface"
    wSNT_TERRAIN = 5, //' "terrain"
    wSNT_SKY_BOX = 6, //' "skyBox"
    wSNT_SKY_DOME = 7, //' "skyDome"
    wSNT_SHADOW_VOLUME = 8, //' "shadowVolume"
    wSNT_OCTREE = 9, //' "octree"   ,   "octTree"
    wSNT_MESH = 10, //' "mesh"
    wSNT_LIGHT = 11, //' "light"
    wSNT_EMPTY = 12, //' "empty"
    wSNT_DUMMY_TRANSFORMATION = 13, //' "dummyTreansormation"
    wSNT_CAMERA = 14, //' "camera"
    wSNT_BILLBOARD = 15, //' "billBoard"
    wSNT_ANIMATED_MESH = 16, //' "animatedMesh"
    wSNT_PARTICLE_SYSTEM = 17, //' "particleSystem"
    wSNT_VOLUME_LIGHT = 18, //' "volumeLight"
    //'for version <=1.4.x irr files
    wSNT_CAMERA_MAYA = 19, //' "cameraMaya"
    wSNT_CAMERA_FPS = 20, //' "cameraFPS"
    wSNT_Q3SHADER_SCENE_NODE = 21, //' "quacke3Shader"
    //'added
    wSNT_UNKNOWN = 22, //' "unknown"
    wSNT_ANY = 23
}

enum wXmlNodeType
{
    wXNT_NONE = 0, //(No xml node. This is usually the node if you did not read anything yet)
    wXNT_ELEMENT = 1,
    wXNT_ELEMENT_END = 2,
    wXNT_TEXT = 3,
    wXNT_COMMENT = 4,
    wXNT_CDATA = 5,
    wXNT_UNKNOWN = 6
}

enum wTextFormat
{
    //' ASCII, file without byte order mark, or not a text file
    wTF_ASCII = 0,
    //' UTF-8 format
    wTF_UTF8 = 1,
    //'UTF-16 format, big endian
    wTF_UTF16_BE = 2,
    //' UTF-16 format, little endian
    wTF_UTF16_LE = 3,
    //' UTF-32 format, big endian
    wTF_UTF32_BE = 4,
    //'UTF-32 format, little endian
    wTF_UTF32_LE = 5
}

enum wFilterType
{
    wFT_NONE = 0,
    wFT_4PCF = 1,
    wFT_8PCF = 2,
    wFT_12PCF = 3,
    wFT_16PCF = 4,
    wFT_COUNT = 5
}

enum wShadowMode
{
    wSM_RECEIVE = 0,
    wSM_CAST = 1,
    wSM_BOTH = 2,
    wSM_EXCLUDE = 3,
    wSM_COUNT = 4
}

enum wColorFormat
{
    wCF_A1R5G5B5 = 0,
    wCF_R5G6B5 = 1,
    wCF_R8G8B8 = 2,
    wCF_A8R8G8B8 = 3
}

enum wMd2AnimationType
{
    wMAT_STAND = 0,
    wMAT_RUN = 1,
    wMAT_ATTACK = 2,
    wMAT_PAIN_A = 3,
    wMAT_PAIN_B = 4,
    wMAT_PAIN_C = 5,
    wMAT_JUMP = 6,
    wMAT_FLIP = 7,
    wMAT_SALUTE = 8,
    wMAT_FALLBACK = 9,
    wMAT_WAVE = 10,
    wMAT_POINT = 11,
    wMAT_CROUCH_STAND = 12,
    wMAT_CROUCH_WALK = 13,
    wMAT_CROUCH_ATTACK = 14,
    wMAT_CROUCH_PAIN = 15,
    wMAT_CROUCH_DEATH = 16,
    wMAT_DEATH_FALLBACK = 17,
    wMAT_DEATH_FALLFORWARD = 18,
    wMAT_DEATH_FALLBACKSLOW = 19,
    wMAT_BOOM = 20
}

enum wJointMode
{
    wJM_NONE = 0,
    wJM_READ = 1,
    wJM_CONTROL = 2
}

enum wBoneSkinningSpace
{
    wBSS_LOCAL = 0,
    wBSS_GLOBAL = 1,
    wBSS_COUNT = 2
}

enum wMeshFileFormat
{
    wMFF_WS_MESH = 0,
    wMFF_COLLADA = 1,
    wMFF_STL = 2
}

enum wAnimatedMeshType
{
    wAMT_UNKNOWN = 0, ///Unknown animated mesh type.
    wAMT_MD2 = 1, //Quake 2 MD2 model file.
    wAMT_MD3 = 2, //Quake 3 MD3 model file.
    wAMT_OBJ = 3, //Maya .obj static model.
    wAMT_BSP = 4, //Quake 3 .bsp static Map.
    wAMT_3DS = 5, //3D Studio .3ds file
    wAMT_MY3D = 6, //My3D Mesh, the file format by Zhuck Dimitry.
    wAMT_LMTS = 7, //Pulsar LMTools .lmts file. This Irrlicht loader was written by Jonas Petersen.
    wAMT_CSM = 8, //Cartography Shop .csm file. This loader was created by Saurav Mohapatra.
    wAMT_OCT = 9, //.oct file for Paul Nette's FSRad or from Murphy McCauley's Blender .oct exporter.
    //The oct file format contains 3D geometry and lightmaps and can be loaded directly by Irrlicht
    wAMT_MDL_HALFLIFE = 10, //Halflife MDL model file.
    wAMT_SKINNED = 11 //generic skinned mesh
}

enum wPhysSolverModel
{
    wPSM_EXACT = 0, //точный обсчет
    wPSM_ADAPTIVE = 1, //уменьшение точности обсчета в пользу производительности
    wPSM_LINEAR = 2,
    wPSM_LINEAR2 = 4
}

enum wPhysFrictionModel
{
    wPFM_ZERO = 0,
    wPFM_ONE = 1
}

enum wPhysVehicleType
{
    wPVT_RAYCAST_WORLD = 0,
    wPVT_RAYCAST_CONVEX = 1
}

enum wPhysVehicleTireType
{
    wPVTT_STEER = 0, ///Поворачиваемое колесо
    wPVTT_ACCEL = 1, ///Ведущее колесо
    wPVTT_ACCEL_STEER = 2, ///Поворачиваемое и ведущее
    wPVTT_ONLYWEEL = 3 ///Просто колесо
}

enum wPhysRagDollBoneCollisionType
{
    wPRBCT_BOX = 0,
    wPRBCT_SPHERE = 1,
    wPRBCT_CAPSULE = 2,
    wPRBCT_HULL = 3
}

struct wPhysRagDollBoneParameters
{
    char* boneName;
    wPhysRagDollBoneCollisionType type;
    Float32 mass;
    Float32 coneAngle;
    Float32 minTwistAngle;
    Float32 maxTwistAngle;
    Float32 pitch;
    Float32 yaw;
    Float32 roll;
    Int32 collideWithNonImmidiateBodies;
}

enum wGuiAlignment
{
    wGA_UPPERLEFT = 0,
    wGA_LOWERRIGHT = 1,
    wGA_CENTER = 2,
    wGA_SCALE = 3
}

enum wGuiElementType
{
    wGET_BUTTON = 0,
    wGET_CHECK_BOX = 1,
    wGET_COMBO_BOX = 2,
    wGET_CONTEXT_MENU = 3,
    wGET_MENU = 4,
    wGET_EDIT_BOX = 5,
    wGET_FILE_OPEN_DIALOG = 6,
    wGET_COLOR_SELECT_DIALOG = 7,
    wGET_IN_OUT_FADER = 8,
    wGET_IMAGE = 9,
    wGET_LIST_BOX = 10,
    wGET_MESH_VIEWER = 11,
    wGET_MESSAGE_BOX = 12,
    wGET_MODAL_SCREEN = 13,
    wGET_SCROLL_BAR = 14,
    wGET_SPIN_BOX = 15,
    wGET_STATIC_TEXT = 16,
    wGET_TAB = 17,
    wGET_TAB_CONTROL = 18,
    wGET_TABLE = 19,
    wGET_TOOL_BAR = 20,
    wGET_TREE_VIEW = 21,
    wGET_WINDOW = 22,
    wGET_ELEMENT = 23,
    wGET_ROOT = 24,
    wGET_COUNT = 25,
    wGET_FORCE_32_BIT = 26
}

enum wGuiColumnOrdering
{
    //'Do not use ordering
    wGCO_NONE = 0,
    //'Send a wGET_TABLE_HEADER_CHANGED message when a column header is clicked.
    wGCO_CUSTOM = 1,
    //'Sort it ascending by it's ascii value like: a,b,c,...
    wGCO_ASCENDING = 2,
    //'Sort it descending by it's ascii value like: z,x,y,...
    wGCO_DESCENDING = 3,
    //'Sort it ascending on first click, descending on next, etc
    wGCO_FLIP_ASCENDING_DESCENDING = 4,
    //'Not used as mode, only to get maximum value for this enum
    wGCO_COUNT = 5
}

enum wGuiListboxColor
{
    wGLC_TEXT = 0,
    wGLC_TEXT_HIGHLIGHT = 1,
    wGLC_ICON = 2,
    wGLC_ICON_HIGHLIGHT = 3,
    wGLC_COUNT = 4
}

enum wGuiDefaultColor
{
    wGDC_3D_DARK_SHADOW = 0,
    wGDC_3D_SHADOW = 1,
    wGDC_3D_FACE = 2,
    wGDC_3D_HIGH_LIGHT = 3,
    wGDC_3D_LIGHT = 4,
    wGDC_ACTIVE_BORDER = 5,
    wGDC_ACTIVE_CAPTION = 6,
    wGDC_APP_WORKSPACE = 7,
    wGDC_BUTTON_TEXT = 8,
    wGDC_GRAY_TEXT = 9,
    wGDC_HIGH_LIGHT = 10,
    wGDC_HIGH_LIGHT_TEXT = 11,
    wGDC_INACTIVE_BORDER = 12,
    wGDC_INACTIVE_CAPTION = 13,
    wGDC_TOOLTIP = 14,
    wGDC_TOOLTIP_BACKGROUND = 15,
    wGDC_SCROLLBAR = 16,
    wGDC_WINDOW = 17,
    wGDC_WINDOW_SYMBOL = 18,
    wGDC_ICON = 19,
    wGDC_ICON_HIGH_LIGHT = 20,
    wGDC_COUNT = 21
}

enum wContextMenuClose
{
    wCMC_IGNORE = 0,
    wCMC_REMOVE = 1,
    wCMC_HIDE = 2
}

enum wGuiOrderingMode
{
    wGOM_NONE = 0,
    wGOM_ASCENDING = 1,
    wGOM_DESCENDING = 2,
    wGOM_COUNT = 3
}

enum wGuiTableDrawFlags
{
    wGTDF_ROWS = 1,
    wGTDF_COLUMNS = 2,
    wGTDF_ACTIVE_ROW = 4,
    wGTDF_COUNT = 5
}

enum wGuiSkinSpace
{
    wGSS_WINDOWS_CLASSIC = 0,
    wGSS_WINDOWS_METALLIC = 1,
    wGSS_BURNING_SKIN = 2,
    wGSS_UNKNOWN = 3,
    wGSS_COUNT = 4
}

enum wGuiDefaultSize
{
    wGDS_SCROLLBAR_SIZE = 0,
    wGDS_MENU_HEIGHT = 1,
    wGDS_WINDOW_BUTTON_WIDTH = 2,
    wGDS_CHECK_BOX_WIDTH = 3,
    wGDS_MESSAGE_BOX_WIDTH = 4,
    wGDS_MESSAGE_BOX_HEIGHT = 5,
    wGDS_BUTTON_WIDTH = 6,
    wGDS_BUTTON_HEIGHT = 7,
    wGDS_TEXT_DISTANCE_X = 8,
    wGDS_TEXT_DISTANCE_Y = 9,
    wGDS_TITLEBARTEXT_DISTANCE_X = 10,
    wGDS_TITLEBARTEXT_DISTANCE_Y = 11,
    wGDS_MESSAGE_BOX_GAP_SPACE = 12,
    wGDS_MESSAGE_BOX_MIN_TEXT_WIDTH = 13,
    wGDS_MESSAGE_BOX_MAX_TEXT_WIDTH = 14,
    wGDS_MESSAGE_BOX_MIN_TEXT_HEIGHT = 15,
    wGDS_MESSAGE_BOX_MAX_TEXT_HEIGHT = 16,
    wGDS_BUTTON_PRESSED_IMAGE_OFFSET_X = 17,
    wGDS_BUTTON_PRESSED_IMAGE_OFFSET_Y = 18,
    wGDS_BUTTON_PRESSED_TEXT_OFFSET_X = 19,
    wGDS_BUTTON_PRESSED_TEXT_OFFSET_Y = 20,
    wGDS_BUTTON_PRESSED_SPRITE_OFFSET_X = 21,
    wGDS_BUTTON_PRESSED_SPRITE_OFFSET_Y = 22,
    wGDS_COUNT = 23
}

enum wGuiDefaultText
{
    wGDT_MSG_BOX_OK = 0,
    wGDT_MSG_BOX_CANCEL = 1,
    wGDT_MSG_BOX_YES = 2,
    wGDT_MSG_BOX_NO = 3,
    wGDT_WINDOW_CLOSE = 4,
    wGDT_WINDOW_MAXIMIZE = 5,
    wGDT_WINDOW_MINIMIZE = 6,
    wGDT_WINDOW_RESTORE = 7,
    wGDT_COUNT = 8
}

enum wGuiDefaultFont
{
    wGDF_DEFAULT = 0,
    wGDF_BUTTON = 1,
    wGDF_WINDOW = 2,
    wGDF_MENU = 3,
    wGDF_TOOLTIP = 4,
    wGDF_COUNT = 5
}

enum wGuiButtonState
{
    //'The button is not pressed
    wGBS_BUTTON_UP = 0,
    //'The button is currently pressed down
    wGBS_BUTTON_DOWN = 1,
    //'The mouse cursor is over the button
    wGBS_BUTTON_MOUSE_OVER = 2,
    //'The mouse cursor is not over the button
    wGBS_BUTTON_MOUSE_OFF = 3,
    //'The button has the focus
    wGBS_BUTTON_FOCUSED = 4,
    //'The button doesn't have the focus
    wGBS_BUTTON_NOT_FOCUSED = 5,
    //'not used, counts the number of enumerated items
    wGBS_COUNT = 6
}

enum wGuiDefaultIcon
{
    wGDI_WINDOW_MAXIMIZE = 0,
    wGDI_WINDOW_RESTORE = 1,
    wGDI_WINDOW_CLOSE = 2,
    wGDI_WINDOW_MINIMIZE = 3,
    wGDI_WINDOW_RESIZE = 4,
    wGDI_CURSOR_UP = 5,
    wGDI_CURSOR_DOWN = 6,
    wGDI_CURSOR_LEFT = 7,
    wGDI_CURSOR_RIGHT = 8,
    wGDI_MENU_MORE = 9,
    wGDI_CHECK_BOX_CHECKED = 10,
    wGDI_DROP_DOWN = 11,
    wGDI_SMALL_CURSOR_UP = 12,
    wGDI_SMALL_CURSOR_DOWN = 13,
    wGDI_RADIO_BUTTON_CHECKED = 14,
    wGDI_MORE_LEFT = 15,
    wGDI_MORE_RIGHT = 16,
    wGDI_MORE_UP = 17,
    wGDI_MORE_DOWN = 18,
    wGDI_EXPAND = 19,
    wGDI_COLLAPSE = 20,
    wGDI_FILE = 21,
    wGDI_DIRECTORY = 22,
    wGDI_COUNT = 23
}

enum wLightType
{
    wLT_POINT = 0,
    wLT_SPOT = 1,
    wLT_DIRECTIONAL = 2
}

enum wDebugMode
{
    wDM_OFF = 0,
    wDM_BBOX = 1,
    wDM_NORMALS = 2,
    wDM_SKELETON = 4,
    wDM_MESH_WIRE_OVERLAY = 8,
    wDM_HALF_TRANSPARENCY = 16,
    wDM_BBOX_BUFFERS = 32,
    wDM_FULL = 0xffffffff
}

enum wTerrainPatchSize
{
    wTPS_9 = 9, //' patch size of 9, at most, use 4 levels of detail with this patch size.
    wTPS_17 = 17, // ' patch size of 17, at most, use 5 levels of detail with this patch size.
    wTPS_33 = 33, // ' patch size of 33, at most, use 6 levels of detail with this patch size.
    wTPS_65 = 65, //' patch size of 65, at most, use 7 levels of detail with this patch size.
    wTPS_129 = 129 //' patch size of 129, at most, use 8 levels of detail with this patch size.
}

enum wTiledTerrainEdge
{
    wTTE_TOP = 0,
    wTTE_BOTTOM = 1,
    wTTE_LEFT = 2,
    wTTE_RIGHT = 3
}

enum wPostEffectQuality
{
    wPEQ_CRUDE = 0,
    wPEQ_FAST = 1,
    wPEQ_DEFAULT = 2,
    wPEQ_GOOD = 3,
    wPEQ_BEST = 4
}

enum wPostEffectId
{
    wPEI_CUSTOM = 0, // () Do not use - used internally
    wPEI_DIRECT = 1, // () Does nothing to the input - useful for anti-aliasing
    wPEI_PUNCH = 2, // (dx,dy,cx,cy)Applies a punch effect to the input, centred at (cx,cy) with strength (dx,dy)
    wPEI_PIXELATE = 3, // (w,h) Pixellates the input into w x h sized chunks (units in the range 0-1) Note: this does NOT use full antialiasing - only the centre pixel of each block is sampled.
    wPEI_PIXELATEBANDS = 4, // (w,h,m) As PP_PIXELATE, but also darkens every other row (multiplies colour by m)
    wPEI_DARKEN = 5, // (mult) Multiplies rgb by mult and maintains black (0) = black (0)
    wPEI_LIGHTEN = 6, // (mult) Multiplies rgb by mult and maintains white (1) = white (1)
    wPEI_RANGE = 7, // (low,high) Changes contrast so that low -> 0, high -> 1
    wPEI_POSTERIZE = 8, // (levels) Reduces the colours by "rounding" them to the levels. i.e. levels = 2 means each channel is either 0.0 or 1.0. levels = 3 means 0.0, 0.5 or 1.0, etc.
    wPEI_INVERT = 9, // () Inverts the rgba channels
    wPEI_TINT = 10, // (r,g,b,m) Converts the pixels to monochrome using a simple weighting then applies a tint. maintains black = black, white = white. Finally merges with original (m=0 -> no change, m=1 -> full tint)
    wPEI_CURVES = 11, // (r,g,b) Applies a colour curve to the rgb channels, maintaining black = black and white = white. Values of 1.0 are no change, > 1.0 raises colour presence
    wPEI_GREYSCALE = 12, // (power) = PP_TINT( power, power, power, 1.0 )
    wPEI_SEPIA = 13, // () = PP_TINT( 2.0, 1.0, 0.7, 1.0 )
    wPEI_SATURATE = 14, // (amount) = PP_TINT( 1.0, 1.0, 1.0, 1.0-amount ) 1.0 = no change, > 1.0 = saturate, < 1.0 = desaturate. Negative values will invert the colours, but not the luminosiry, can make interesting effects
    wPEI_VIGNETTE = 15, // (power,start,end)Applies a black vignette around the input. Set power to 2 for a standard circle, or a higher value for a more rectangular shape. Lower values will make star-like patterns.
    wPEI_NOISE = 16, // (amount) Adds psudo-random monochromatic noise to each pixel. Each frame uses different random numbers. Random function is crude.
    wPEI_COLORNOISE = 17, // (amount) As above, but r,g,b channels are seperate
    wPEI_PURENOISE = 18, // (amount) As PP_NOISE, but ignores input. Renders as if on a grey background. Useful as a generator
    wPEI_HBLUR = 19, // (distance) Applies a simple horizontal linear blur filter using 5 samples
    wPEI_VBLUR = 20, // (distance) As PP_HBLUR but vertical
    wPEI_HSHARPEN = 21, // (d,mult) A horizontal sharpen; raises contrast around edges
    wPEI_VSHARPEN = 22, // (d,mult) As PP_HSHARPEN but vertical
    wPEI_BIBLUR = 23, // (dx,dy) A simultaneous horizontal &amp; vertical blur. A better effect, but cannot take advantage of parallel processing, so usually slower than HBLUR+VBLUR.
    wPEI_HBLURDOFFAR = 24, // (near,far,d) Applies a depth of field, blurier further away. Needs depth in alpha channel, like PP_DEPTH and PP_OCCLUSION
    wPEI_VBLURDOFFAR = 25, // (near,far,d) As PP_HBLURDOFFAR but vertical
    wPEI_HBLURDOFNEAR = 26, // (near,far,d) Applies a depth of field, blurier close-up. Needs depth in alpha channel, like PP_DEPTH and PP_OCCLUSION
    wPEI_VBLURDOFNEAR = 27, // (near,far,d) As PP_HBLURDOFNEAR but vertical
    wPEI_LINEARBLUR = 28, // (dx,dy) As PP_HBLUR, but applies along the line dx,dy
    wPEI_RADIALBLUR = 29, // (cx,cy,dx,dy)Applies a radial blur from (cx,cy) with a size of dx at (cx+1,cy) and dy at (cx,cy+1)
    wPEI_RADIALBEAM = 30, // (cx,cy,dx,dy)Applies a radial blur from (cx,cy) with a size of dx at (cx+1,cy) and dy at (cx,cy+1) with an additive effect to make beams
    wPEI_ROTATIONALBLUR = 31, // (cx,cy,dx,dy)Applies a rotational blur around (cx,cy)
    wPEI_OVERLAY = 32, // (mult) Output = Texture1 + Texture2 * mult
    wPEI_OVERLAYNEG = 33, // (mult) Output = Texture1 - (1 - Texture2) * mult !WARNING: Due to no EMT_TRANSPARENT_SUBTRACT_COLOR option, this uses a SLOW method. Will cause a performance hit if Texture2 is non-static.
    wPEI_MOTIONBLUR = 34, // (sharp) Retains a memory of past renders, low sharp = long trails. Recommended sharp ~= 0.1
    wPEI_HAZE = 35, // (dist,opac,speed,scale) Adds a heat haze, using the red channel of texture2 as heat, with 0 = cold, 1 = hot
    wPEI_HAZEDEPTH = 36, // (dist,opac,speed,scale) Adds a heat haze, using the red channel of texture2 as heat, with 0 = cold, 1 = hot and the green channel as the z-depth
    wPEI_DEPTH = 37, // () Renders depth (alpha) as greyscale, lighter = further away
    wPEI_OCCLUSION = 38, // (mult) Taken from the Irrlicht forums and heavily mutilated, uses alpha channel to judge depth

    // Composite effects

    wPEI_BLUR = 39, // (distance) = HBLUR(distance) + VBLUR(distance)
    wPEI_SHARPEN = 40, // (distance,mult) = HSHARPEN(distance,mult) + VSHARPEN(distance,mult)
    wPEI_BLURDOFFAR = 41, // (near,far,distance) = HBLURDOFFAR(near,far,distance) + VBLURDOFFAR(near,far,distance)
    wPEI_BLURDOFNEAR = 42, // (near,far,distance) = HBLURDOFNEAR(near,far,distance) + VBLURDOFNEAR(near,far,distance)
    wPEI_BLURDOF = 43, // (b1,f1,f2,b2,dist) = BLURDOFFAR(f2,b2,dist) + HBLURDOFNEAR(b1,f1,dist) + VBLURDOFNEAR(b1,f1,dist)
    wPEI_BLOOM = 44, // (cut,distance,light) = LIGHTEN(cut) + BLUR(distance) + OVERLAY(light)
    wPEI_GLOOM = 45, // (cut,distance,dark) = DARKEN(cut) + BLUR(distance) + OVERLAYNEG(dark) !WARNING: uses OVERLAYNEG which is slow!
    wPEI_NIGHTVISION = 46, // (max,distance,noise) = RANGE(-0.5,max) + BLUR(distance) + NOISE(noise) + TINT(0.2,2.0,0.5)
    wPEI_MONITOR = 47, // (vig,bulge,noise,sat,pixel,rowm) = TINT(1.0,0.9,0.8,1.0-sat) + NOISE(noise) + PIXELATEBANDS(pixel,pixel,rowm) + PUNCH(bulge,bulge,0.5,0.5) + VIGNETTE(4.0,0.0,1.0/vig)
    wPEI_WATERCOLOR = 48, // (bright,blur,levels,sharp,mult,noise) = NOISE( noise ) + CURVES( bright, bright, bright ) + BIBLUR( blur, blur ) + POSTERIZE( levels ) + SHARPEN( sharp, mult )
    wPEI_COUNT = 49 //Not for use!!!
}

struct tag_wBillboard
{
    wVector3f Position;
    wVector2f Size;
    Float32 Roll;
    wVector3f Axis;
    Int32 HasAxis;
    Int32 sColor; //not for use
    UInt32 alpha;
    UInt32 red;
    UInt32 green;
    UInt32 blue;

    UInt32 vertexIndex;
    tag_wBillboard* sprev;
    tag_wBillboard* snext;
}

alias wBillboard = tag_wBillboard;

enum wConsoleFontColor
{
    wCFC_BLACK = 0,
    wCFC_BLUE = 1,
    wCFC_GREEN = 2,
    wCFC_CYAN = 3,
    wCFC_RED = 4,
    wCFC_MAGENTA = 5,
    wCFC_BROWN = 6,
    wCFC_GREY = 7,
    wCFC_DARKGREY = 8,
    wCFC_LIGHTBLUE = 9,
    wCFC_LIGHTGREEN = 10,
    wCFC_LIGHTCYAN = 11,
    wCFC_LIGHTRED = 12,
    wCFC_LIGHTMAGENTA = 13,
    wCFC_YELLOW = 14,
    wCFC_WHITE = 15,
    wCFC_COUNT = 16
}

enum wConsoleBackColor
{
    wCBC_BLACK = 0,
    wCBC_BLUE = 1,
    wCBC_GREEN = 2,
    wCBC_CYAN = 3,
    wCBC_RED = 4,
    wCBC_MAGENTA = 5,
    wCBC_YELLOW = 6,
    wCBC_WHITE = 7,
    wCBC_COUNT = 8
}

enum wWeekDay
{
    wWD_SUNDAY = 0,
    wWD_MONDAY = 1,
    wWD_TUESDAY = 2,
    wWD_WEDNESDAY = 3,
    wWD_THURSDAY = 4,
    wWD_FRIDAY = 5,
    wWD_SATURDAY = 6
}

struct wRealTimeDate
{
    UInt32 Day;
    UInt32 Hour;
    bool IsDST;
    UInt32 Minute;
    UInt32 Month;
    UInt32 Second;
    wWeekDay Weekday;
    Int32 Year;
    UInt32 Yearday;
}

struct wBillboardAxisParam
{
    bool isEnablePitch; // вращение по X
    bool isEnableYaw; // вращение по Y
    bool isEnableRoll; //вращение по Z
}

enum wFileArchiveType
{
    wFAT_ZIP = 0, //A PKZIP archive.
    wFAT_GZIP = 1, //A gzip archive.
    wFAT_FOLDER = 2, //A virtual directory.
    wFAT_PAK = 3, //An ID Software PAK archive.
    wFAT_NPK = 4, //A Nebula Device archive.
    wFAT_TAR = 5, //A Tape ARchive.
    wFAT_WAD = 6, //A wad Archive, Quake2, Halflife.
    wFAT_UNKNOWN = 7 //The type of this archive is unknown.
}

///// OPEN AL  ENUMS //////
enum wSoundEffectType
{
    wSET_NULL = 0,
    wSET_EAX_REVERB = 1,
    wSET_REVERB = 2,
    wSET_CHORUS = 3,
    wSET_DISTORTION = 4,
    wSET_ECHO = 5,
    wSET_FLANGER = 6,
    wSET_FREQUENCY_SHIFTER = 7,
    wSET_VOCAL_MORPHER = 8,
    wSET_PITCH_SHIFTER = 9,
    wSET_RING_MODULATOR = 10,
    wSET_AUTOWAH = 11,
    wSET_COMPRESSOR = 12,
    wSET_EQUALIZER = 13,
    wSET_COUNT = 14
}

enum wSoundFilterType
{
    wSFT_NULL = 0,
    wSFT_LOWPASS = 1,
    wSFT_HIGHPASS = 2,
    wSFT_BANDPASS = 3,
    wSFT_COUNT = 4
}

enum wAudioFormats
{
    wAF_8BIT_MONO = 0,
    wAF_8BIT_STEREO = 1,
    wAF_16BIT_MONO = 2,
    wAF_16BIT_STEREO = 3
}

//! Contains parameters for the EAX Reverb Effect.  This effect tries to simulate how sound behaves in different environments.
struct tag_wEaxReverbParameters
{
    // __cplusplus

    //! Reverb Modal Density controls the coloration of the late reverb. Lowering the value adds
    //! more coloration to the late reverb.
    //! Range: 0.0 to 1.0
    Float32 Density;

    //! The Reverb Diffusion property controls the echo density in the reverberation decay. It's set by
    //! default to 1.0, which provides the highest density. Reducing diffusion gives the reverberation a
    //! more "grainy" character that is especially noticeable with percussive sound sources. If you set a
    //! diffusion value of 0.0, the later reverberation sounds like a succession of distinct echoes.
    //! Range: 0.0 to 1.0
    Float32 Diffusion;

    //! The Reverb Gain property is the master volume control for the reflected sound (both early
    //! reflections and reverberation) that the reverb effect adds to all sound sources. It sets the
    //! maximum amount of reflections and reverberation added to the final sound mix. The value of the
    //! Reverb Gain property ranges from 1.0 (0db) (the maximum amount) to 0.0 (-100db) (no reflected
    //! sound at all).
    //! Range: 0.0 to 1.0
    Float32 Gain;

    //! The Reverb Gain HF property further tweaks reflected sound by attenuating it at high frequencies.
    //! It controls a low-pass filter that applies globally to the reflected sound of all sound sources
    //! feeding the particular instance of the reverb effect. The value of the Reverb Gain HF property
    //! ranges from 1.0 (0db) (no filter) to 0.0 (-100db) (virtually no reflected sound). HF Reference sets
    //! the frequency at which the value of this property is measured.
    //! Range: 0.0 to 1.0
    Float32 GainHF;

    //! The Reverb Gain LF property further tweaks reflected sound by attenuating it at low frequencies.
    //! It controls a high-pass filter that applies globally to the reflected sound of all sound sources
    //! feeding the particular instance of the reverb effect. The value of the Reverb Gain LF property
    //! ranges from 1.0 (0db) (no filter) to 0.0 (-100db) (virtually no reflected sound). LF Reference sets
    //! the frequency at which the value of this property is measured.
    //! Range: 0.0 to 1.0
    Float32 GainLF;

    //! The Decay Time property sets the reverberation decay time. It ranges from 0.1 (typically a small
    //! room with very dead surfaces) to 20.0 (typically a large room with very live surfaces).
    //! Range: 0.1 to 20.0
    Float32 DecayTime;

    //! The Decay HF Ratio property adjusts the spectral quality of the Decay Time parameter. It is the
    //! ratio of high-frequency decay time relative to the time set by Decay Time. The Decay HF Ratio
    //! value 1.0 is neutral: the decay time is equal for all frequencies. As Decay HF Ratio increases
    //! above 1.0, the high-frequency decay time increases so it's longer than the decay time at mid
    //! frequencies. You hear a more brilliant reverberation with a longer decay at high frequencies. As
    //! the Decay HF Ratio value decreases below 1.0, the high-frequency decay time decreases so it's
    //! shorter than the decay time of the mid frequencies. You hear a more natural reverberation.
    //! Range: 0.1 to 20.0
    Float32 DecayHFRatio;

    //! The Decay LF Ratio property adjusts the spectral quality of the Decay Time parameter. It is the
    //! ratio of low-frequency decay time relative to the time set by Decay Time. The Decay LF Ratio
    //! value 1.0 is neutral: the decay time is equal for all frequencies. As Decay LF Ratio increases
    //! above 1.0, the low-frequency decay time increases so it's longer than the decay time at mid
    //! frequencies. You hear a more booming reverberation with a longer decay at low frequencies. As
    //! the Decay LF Ratio value decreases below 1.0, the low-frequency decay time decreases so it's
    //! shorter than the decay time of the mid frequencies. You hear a more tinny reverberation.
    //! Range: 0.1 to 20.0
    Float32 DecayLFRatio;

    //! The Reflections Gain property controls the overall amount of initial reflections relative to the Gain
    //! property. (The Gain property sets the overall amount of reflected sound: both initial reflections
    //! and later reverberation.) The value of Reflections Gain ranges from a maximum of 3.16 (+10 dB)
    //! to a minimum of 0.0 (-100 dB) (no initial reflections at all), and is corrected by the value of the
    //! Gain property. The Reflections Gain property does not affect the subsequent reverberation decay.
    //! Range: 0.0 to 3.16
    Float32 ReflectionsGain;

    //! The Reflections Delay property is the amount of delay between the arrival time of the direct path
    //! from the source to the first reflection from the source. It ranges from 0 to 300 milliseconds. You
    //! can reduce or increase Reflections Delay to simulate closer or more distant reflective surfaces—
    //! and therefore control the perceived size of the room.
    //! Range: 0.0 to 0.3
    Float32 ReflectionsDelay;

    //! The Reflections Pan property is a 3D vector that controls the spatial distribution of the cluster of
    //! early reflections. The direction of this vector controls the global direction of the reflections, while
    //! its magnitude controls how focused the reflections are towards this direction.
    //! It is important to note that the direction of the vector is interpreted in the coordinate system of the
    //! user, without taking into account the orientation of the virtual listener. For instance, assuming a
    //! four-point loudspeaker playback system, setting Reflections Pan to (0, 0, 0.7) means that the
    //! reflections are panned to the front speaker pair, whereas as setting of (0, 0, -0.7) pans the
    //! reflections towards the rear speakers. These vectors follow the a left-handed co-ordinate system,
    //! unlike OpenAL uses a right-handed co-ordinate system.
    //! If the magnitude of Reflections Pan is zero (the default setting), the early reflections come evenly
    //! from all directions. As the magnitude increases, the reflections become more focused in the
    //! direction pointed to by the vector. A magnitude of 1.0 would represent the extreme case, where
    //! all reflections come from a single direction.
    //cVector3 ReflectionsPan;
    Float32 ReflectionsPanX;
    Float32 ReflectionsPanY;
    Float32 ReflectionsPanZ;
    //! The Late Reverb Gain property controls the overall amount of later reverberation relative to the
    //! Gain property. (The Gain property sets the overall amount of both initial reflections and later
    //! reverberation.) The value of Late Reverb Gain ranges from a maximum of 10.0 (+20 dB) to a
    //! minimum of 0.0 (-100 dB) (no late reverberation at all).
    //! Range: 0.0 to 10.0
    Float32 LateReverbGain;

    //! The Late Reverb Delay property defines the begin time of the late reverberation relative to the
    //! time of the initial reflection (the first of the early reflections). It ranges from 0 to 100 milliseconds.
    //! Reducing or increasing Late Reverb Delay is useful for simulating a smaller or larger room.
    //! Range: 0.0 to 0.1
    Float32 LateReverbDelay;

    //! The Late Reverb Pan property is a 3D vector that controls the spatial distribution of the late
    //! reverb. The direction of this vector controls the global direction of the reverb, while its magnitude
    //! controls how focused the reverb are towards this direction. The details under Reflections Pan,
    //! above, also apply to Late Reverb Pan.
    //cVector3 LateReverbPan;
    Float32 LateReverbPanX;
    Float32 LateReverbPanY;
    Float32 LateReverbPanZ;

    //! Echo Time controls the rate at which the cyclic echo repeats itself along the
    //! reverberation decay. For example, the default setting for Echo Time is 250 ms. causing the echo
    //! to occur 4 times per second. Therefore, if you were to clap your hands in this type of
    //! environment, you will hear four repetitions of clap per second.
    //! Range: 0.075 to 0.25
    Float32 EchoTime;

    //! Echo Depth introduces a cyclic echo in the reverberation decay, which will be noticeable with
    //! transient or percussive sounds. A larger value of Echo Depth will make this effect more
    //! prominent.
    //! Together with Reverb Diffusion, Echo Depth will control how long the echo effect will persist along
    //! the reverberation decay. In a more diffuse environment, echoes will wash out more quickly after
    //! the direct sound. In an environment that is less diffuse, you will be able to hear a larger number
    //! of repetitions of the echo, which will wash out later in the reverberation decay. If Diffusion is set
    //! to 0.0 and Echo Depth is set to 1.0, the echo will persist distinctly until the end of the
    //! reverberation decay.
    //! Range: 0.0 to 1.0
    Float32 EchoDepth;

    //! Using these two properties below, you can create a pitch modulation in the reverberant sound. This will
    //! be most noticeable applied to sources that have tonal color or pitch. You can use this to make
    //! some trippy effects! Modulation Time controls the speed of the vibrato (rate of periodic changes in pitch).
    //! Range: 0.004 to 4.0
    Float32 ModulationTime;

    //! Modulation Depth controls the amount of pitch change. Low values of Diffusion will contribute to
    //! reinforcing the perceived effect by reducing the mixing of overlapping reflections in the
    //! reverberation decay.
    //! Range: 0.0 to 1.0
    Float32 ModulationDepth;

    //! The Air Absorption Gain HF property controls the distance-dependent attenuation at high
    //! frequencies caused by the propagation medium. It applies to reflected sound only. You can use
    //! Air Absorption Gain HF to simulate sound transmission through foggy air, dry air, smoky
    //! atmosphere, and so on. The default value is 0.994 (-0.05 dB) per meter, which roughly
    //! corresponds to typical condition of atmospheric humidity, temperature, and so on. Lowering the
    //! value simulates a more absorbent medium (more humidity in the air, for example); raising the
    //! value simulates a less absorbent medium (dry desert air, for example).
    //! Range: 0.892 to 1.0
    Float32 AirAbsorptionGainHF;

    //! The properties HF Reference and LF Reference determine respectively the frequencies at which
    //! the high-frequency effects and the low-frequency effects created by EAX Reverb properties are
    //! measured, for example Decay HF Ratio and Decay LF Ratio.
    //! Note that it is necessary to maintain a factor of at least 10 between these two reference
    //! frequencies so that low frequency and high frequency properties can be accurately controlled and
    //! will produce independent effects. In other words, the LF Reference value should be less than
    //! 1/10 of the HF Reference value.
    //! Range: 1000.0 to 20000.0
    Float32 HFReference;

    //! See HFReference.
    //! Range: 20.0 to 1000.0
    Float32 LFReference;

    //! The Room Rolloff Factor property is one of two methods available to attenuate the reflected
    //! sound (containing both reflections and reverberation) according to source-listener distance. It's
    //! defined the same way as OpenAL's Rolloff Factor, but operates on reverb sound instead of
    //! direct-path sound. Setting the Room Rolloff Factor value to 1.0 specifies that the reflected sound
    //! will decay by 6 dB every time the distance doubles. Any value other than 1.0 is equivalent to a
    //! scaling factor applied to the quantity specified by ((Source listener distance) - (Reference
    //! Distance)). Reference Distance is an OpenAL source parameter that specifies the inner border
    //! for distance rolloff effects: if the source comes closer to the listener than the reference distance,
    //! the direct-path sound isn't increased as the source comes closer to the listener, and neither is the
    //! reflected sound.
    //! The default value of Room Rolloff Factor is 0.0 because, by default, the Effects Extension reverb
    //! effect naturally manages the reflected sound level automatically for each sound source to
    //! simulate the natural rolloff of reflected sound vs. distance in typical rooms.
    //! Range: 0.0 to 10.0
    Float32 RoomRolloffFactor;

    //! When this flag is set, the high-frequency decay time automatically stays below a limit value that's
    //! derived from the setting of the property Air Absorption Gain HF. This limit applies regardless of
    //! the setting of the property Decay HF Ratio, and the limit doesn't affect the value of Decay HF
    //! Ratio. This limit, when on, maintains a natural sounding reverberation decay by allowing you to
    //! increase the value of Decay Time without the risk of getting an unnaturally long decay time at
    //! high frequencies. If this flag is set to false, high-frequency decay time isn't automatically
    //! limited.
    bool DecayHFLimit;
}

alias wEaxReverbParameters = tag_wEaxReverbParameters;

//! Similar to the above EAX Reverb Effect, but has less features, meaning it may be better supported on lower end hardware.
struct tag_wReverbParameters
{
    // __cplusplus

    //! Reverb Modal Density controls the coloration of the late reverb. Lowering the value adds more
    //! coloration to the late reverb.
    //! Range: 0.0 to 1.0
    Float32 Density;

    //! The Reverb Diffusion property controls the echo density in the reverberation decay. It's set by
    //! default to 1.0, which provides the highest density. Reducing diffusion gives the reverberation a
    //! more "grainy" character that is especially noticeable with percussive sound sources. If you set a
    //! diffusion value of 0.0, the later reverberation sounds like a succession of distinct echoes.
    //! Range: 0.0 to 1.0
    Float32 Diffusion;

    //! The Reverb Gain property is the master volume control for the reflected sound (both early
    //! reflections and reverberation) that the reverb effect adds to all sound sources. It sets the
    //! maximum amount of reflections and reverberation added to the final sound mix. The value of the
    //! Reverb Gain property ranges from 1.0 (0db) (the maximum amount) to 0.0 (-100db) (no reflected
    //! sound at all).
    //! Range: 0.0 to 1.0
    Float32 Gain;

    //! The Reverb Gain HF property further tweaks reflected sound by attenuating it at high frequencies.
    //! It controls a low-pass filter that applies globally to the reflected sound of all sound sources
    //! feeding the particular instance of the reverb effect. The value of the Reverb Gain HF property
    //! ranges from 1.0 (0db) (no filter) to 0.0 (-100db) (virtually no reflected sound).
    //! Range: 0.0 to 1.0
    Float32 GainHF;

    //! The Decay Time property sets the reverberation decay time. It ranges from 0.1 (typically a small
    //! room with very dead surfaces) to 20.0 (typically a large room with very live surfaces).
    //! Range: 0.1 to 20.0
    Float32 DecayTime;

    //! The Decay HF Ratio property sets the spectral quality of the Decay Time parameter. It is the
    //! ratio of high-frequency decay time relative to the time set by Decay Time. The Decay HF Ratio
    //! value 1.0 is neutral: the decay time is equal for all frequencies. As Decay HF Ratio increases
    //! above 1.0, the high-frequency decay time increases so it's longer than the decay time at low
    //! frequencies. You hear a more brilliant reverberation with a longer decay at high frequencies. As
    //! the Decay HF Ratio value decreases below 1.0, the high-frequency decay time decreases so it's
    //! shorter than the decay time of the low frequencies. You hear a more natural reverberation.
    //! Range: 0.1 to 2.0
    Float32 DecayHFRatio;

    //! The Reflections Gain property controls the overall amount of initial reflections relative to the Gain
    //! property. (The Gain property sets the overall amount of reflected sound: both initial reflections
    //! and later reverberation.) The value of Reflections Gain ranges from a maximum of 3.16 (+10 dB)
    //! to a minimum of 0.0 (-100 dB) (no initial reflections at all), and is corrected by the value of the
    //! Gain property. The Reflections Gain property does not affect the subsequent reverberation
    //! decay.
    //! Range: 0.0 to 3.16
    Float32 ReflectionsGain;

    //! The Reflections Delay property is the amount of delay between the arrival time of the direct path
    //! from the source to the first reflection from the source. It ranges from 0 to 300 milliseconds. You
    //! can reduce or increase Reflections Delay to simulate closer or more distant reflective surfaces—
    //! and therefore control the perceived size of the room.
    //! Range: 0.0 to 0.3
    Float32 ReflectionsDelay;

    //! The Late Reverb Gain property controls the overall amount of later reverberation relative to the
    //! Gain property. (The Gain property sets the overall amount of both initial reflections and later
    //! reverberation.) The value of Late Reverb Gain ranges from a maximum of 10.0 (+20 dB) to a
    //! minimum of 0.0 (-100 dB) (no late reverberation at all).
    //! Range: 0.0 to 10.0
    Float32 LateReverbGain;

    //! The Late Reverb Delay property defines the begin time of the late reverberation relative to the
    //! time of the initial reflection (the first of the early reflections). It ranges from 0 to 100 milliseconds.
    //! Reducing or increasing Late Reverb Delay is useful for simulating a smaller or larger room.
    //! Range: 0.0 to 0.1
    Float32 LateReverbDelay;

    //! The Air Absorption Gain HF property controls the distance-dependent attenuation at high
    //! frequencies caused by the propagation medium. It applies to reflected sound only. You can use
    //! Air Absorption Gain HF to simulate sound transmission through foggy air, dry air, smoky
    //! atmosphere, and so on. The default value is 0.994 (-0.05 dB) per meter, which roughly
    //! corresponds to typical condition of atmospheric humidity, temperature, and so on. Lowering the
    //! value simulates a more absorbent medium (more humidity in the air, for example); raising the
    //! value simulates a less absorbent medium (dry desert air, for example).
    //! Range: 0.892 to 1.0
    Float32 AirAbsorptionGainHF;

    //! The Room Rolloff Factor property is one of two methods available to attenuate the reflected
    //! sound (containing both reflections and reverberation) according to source-listener distance. It's
    //! defined the same way as OpenAL's Rolloff Factor, but operates on reverb sound instead of
    //! direct-path sound. Setting the Room Rolloff Factor value to 1.0 specifies that the reflected sound
    //! will decay by 6 dB every time the distance doubles. Any value other than 1.0 is equivalent to a
    //! scaling factor applied to the quantity specified by ((Source listener distance) - (Reference
    //! Distance)). Reference Distance is an OpenAL source parameter that specifies the inner border
    //! for distance rolloff effects: if the source comes closer to the listener than the reference distance,
    //! the direct-path sound isn't increased as the source comes closer to the listener, and neither is the
    //! reflected sound.
    //! The default value of Room Rolloff Factor is 0.0 because, by default, the Effects Extension reverb
    //! effect naturally manages the reflected sound level automatically for each sound source to
    //! simulate the natural rolloff of reflected sound vs. distance in typical rooms.
    //! Range: 0.0 to 10.0
    Float32 RoomRolloffFactor;

    //! When this flag is set, the high-frequency decay time automatically stays below a limit value that's
    //! derived from the setting of the property Air Absorption Gain HF. This limit applies regardless of
    //! the setting of the property Decay HF Ratio, and the limit doesn't affect the value of Decay HF
    //! Ratio. This limit, when on, maintains a natural sounding reverberation decay by allowing you to
    //! increase the value of Decay Time without the risk of getting an unnaturally long decay time at
    //! high frequencies. If this flag is set to false, high-frequency decay time isn't automatically
    //! limited.
    bool DecayHFLimit;
}

alias wReverbParameters = tag_wReverbParameters;

//! The chorus effect essentially replays the input audio accompanied by another slightly delayed version of the signal, creating a "doubling" effect.

enum ChorusWaveform
{
    ECW_SINUSOID = 0,
    ECW_TRIANGLE = 1,
    ECW_COUNT = 2
}

struct tag_wChorusParameters
{
    // __cplusplus

    //! This property sets the waveform shape of the LFO that controls the delay time of the delayed signals.
    ChorusWaveform Waveform;

    //! This property controls the phase difference between the left and right LFO's. At zero degrees the
    //! two LFOs are synchronized. Use this parameter to create the illusion of an expanded stereo field
    //! of the output signal.
    //! Range: -180 to 180
    Int32 Phase;

    //! This property sets the modulation rate of the LFO that controls the delay time of the delayed signals.
    //! Range: 0.0 to 10.0
    Float32 Rate;

    //! This property controls the amount by which the delay time is modulated by the LFO.
    //! Range: 0.0 to 1.0
    Float32 Depth;

    //! This property controls the amount of processed signal that is fed back to the input of the chorus
    //! effect. Negative values will reverse the phase of the feedback signal. At full magnitude the
    //! identical sample will repeat endlessly. At lower magnitudes the sample will repeat and fade out
    //! over time. Use this parameter to create a "cascading" chorus effect.
    //! Range: -1.0 to 1.0
    Float32 Feedback;

    //! This property controls the average amount of time the sample is delayed before it is played back,
    //! and with feedback, the amount of time between iterations of the sample. Larger values lower the
    //! pitch. Smaller values make the chorus sound like a flanger, but with different frequency
    //! characteristics.
    //! Range: 0.0 to 0.016
    Float32 Delay;
}

alias wChorusParameters = tag_wChorusParameters;

//! The distortion effect simulates turning up (overdriving) the gain stage on a guitar amplifier or adding a distortion pedal to an instrument's output.
struct tag_wDistortionParameters
{
    // __cplusplus

    //! This property controls the shape of the distortion. The higher the value for Edge, the "dirtier" and "fuzzier" the effect.
    //! Range: 0.0 to 1.0
    Float32 Edge;

    //! This property allows you to attenuate the distorted sound.
    //! Range: 0.01 to 1.0
    Float32 Gain;

    //! Input signal can have a low pass filter applied, to limit the amount of high frequency signal feeding into the distortion effect.
    //! Range: 80.0 to 24000.0
    Float32 LowpassCutoff;

    //! This property controls the frequency at which the post-distortion attenuation (Gain) is active.
    //! Range: 80.0 to 24000.0
    Float32 EqCenter;

    //! This property controls the bandwidth of the post-distortion attenuation.
    //! Range: 80.0 to 24000.0
    Float32 EqBandwidth;
}

alias wDistortionParameters = tag_wDistortionParameters;

//! The echo effect generates discrete, delayed instances of the input signal.
struct tag_wEchoParameters
{
    // __cplusplus

    //! This property controls the delay between the original sound and the first "tap", or echo instance.
    //! Range: 0.0 to 0.207
    Float32 Delay;

    //! This property controls the delay between the first "tap" and the second "tap".
    //! Range: 0.0 to 0.404
    Float32 LRDelay;

    //! This property controls the amount of high frequency damping applied to each echo. As the sound
    //! is subsequently fed back for further echoes, damping results in an echo which progressively gets
    //! softer in tone as well as intensity.
    //! Range: 0.0 to 0.99
    Float32 Damping;

    //! This property controls the amount of feedback the output signal fed back into the input. Use this
    //! parameter to create "cascading" echoes. At full magnitude, the identical sample will repeat
    //! endlessly. Below full magnitude, the sample will repeat and fade.
    //! Range: 0.0 to 1.0
    Float32 Feedback;

    //! This property controls how hard panned the individual echoes are. With a value of 1.0, the first
    //! "tap" will be panned hard left, and the second "tap" hard right. A value of -1.0 gives the opposite
    //! result. Settings nearer to 0.0 result in less emphasized panning.
    //! Range: -1.0 to 1.0
    Float32 Spread;
}

alias wEchoParameters = tag_wEchoParameters;

//! The flanger effect creates a "tearing" or "whooshing" sound (like a jet flying overhead).
enum FlangerWaveform
{
    EFW_SINUSOID = 0,
    EFW_TRIANGLE = 1,
    EFW_COUNT = 2
}

struct tag_wFlangerParameters
{
    // __cplusplus

    //! Selects the shape of the LFO waveform that controls the amount of the delay of the sampled signal.
    FlangerWaveform Waveform;

    //! This changes the phase difference between the left and right LFO's. At zero degrees the two LFOs are synchronized.
    //! Range: -180 to 180
    Int32 Phase;

    //! The number of times per second the LFO controlling the amount of delay repeats. Higher values increase the pitch modulation.
    //! Range: 0.0 to 10.0
    Float32 Rate;

    //! The ratio by which the delay time is modulated by the LFO. Use this parameter to increase the pitch modulation.
    //! Range: 0.0 to 1.0
    Float32 Depth;

    //! This is the amount of the output signal level fed back into the effect's input.
    //! A negative value will reverse the phase of the feedback signal. Use this parameter
    //! to create an "intense metallic" effect. At full magnitude, the identical sample will
    //! repeat endlessly. At less than full magnitude, the sample will repeat and fade out over time.
    //! Range: -1.0 to 1.0
    Float32 Feedback;

    //! The average amount of time the sample is delayed before it is played back; with feedback, the amount of time between iterations of the sample.
    //! Range: 0.0 to 0.004
    Float32 Delay;
}

alias wFlangerParameters = tag_wFlangerParameters;

//! The frequency shifter is a single-sideband modulator, which translates all the component frequencies of the input signal by an equal amount.
enum ShiftDirection
{
    ESD_DOWN = 0,
    ESD_UP = 1,
    ESD_OFF = 2,
    ESD_COUNT = 3
}

struct tag_wFrequencyShiftParameters
{
    // __cplusplus

    //! This is the carrier frequency. For carrier frequencies below the audible range, the singlesideband
    //! modulator may produce phaser effects, spatial effects or a slight pitch-shift. As the
    //! carrier frequency increases, the timbre of the sound is affected; a piano or guitar note becomes
    //! like a bell's chime, and a human voice sounds extraterrestrial!
    //! Range: 0.0 to 24000.0
    Float32 Frequency;

    //! These select which internal signals are added together to produce the output. Different
    //! combinations of values will produce slightly different tonal and spatial effects.
    ShiftDirection Left;

    //! These select which internal signals are added together to produce the output. Different
    //! combinations of values will produce slightly different tonal and spatial effects.
    ShiftDirection Right;
}

alias wFrequencyShiftParameters = tag_wFrequencyShiftParameters;

//! The vocal morpher consists of a pair of 4-band formant filters, used to impose vocal tract effects upon the input signal.
enum MorpherPhoneme
{
    EMP_A = 0,
    EMP_E = 1,
    EMP_I = 2,
    EMP_O = 3,
    EMP_U = 4,
    EMP_AA = 5,
    EMP_AE = 6,
    EMP_AH = 7,
    EMP_AO = 8,
    EMP_EH = 9,
    EMP_ER = 10,
    EMP_IH = 11,
    EMP_IY = 12,
    EMP_UH = 13,
    EMP_UW = 14,
    EMP_B = 15,
    EMP_D = 16,
    EMP_F = 17,
    EMP_G = 18,
    EMP_J = 19,
    EMP_K = 20,
    EMP_L = 21,
    EMP_M = 22,
    EMP_N = 23,
    EMP_P = 24,
    EMP_R = 25,
    EMP_S = 26,
    EMP_T = 27,
    EMP_V = 28,
    EMP_Z = 29,
    EMP_COUNT = 30
}

enum MorpherWaveform
{
    EMW_SINUSOID = 0,
    EMW_TRIANGLE = 1,
    EMW_SAW = 2,
    EMW_COUNT = 3
}

struct tag_wVocalMorpherParameters
{
    // __cplusplus

    //! If both parameters are set to the same phoneme, that determines the filtering effect that will be
    //! heard. If these two parameters are set to different phonemes, the filtering effect will morph
    //! between the two settings at a rate specified by Rate.
    MorpherPhoneme PhonemeA;

    //! If both parameters are set to the same phoneme, that determines the filtering effect that will be
    //! heard. If these two parameters are set to different phonemes, the filtering effect will morph
    //! between the two settings at a rate specified by Rate.
    MorpherPhoneme PhonemeB;

    //! This is used to adjust the pitch of phoneme filter A in 1-semitone increments.
    //! Range: -24 to 24
    Int32 PhonemeACoarseTune;

    //! This is used to adjust the pitch of phoneme filter B in 1-semitone increments.
    //! Range: -24 to 24
    Int32 PhonemeBCoarseTune;

    //! This controls the shape of the low-frequency oscillator used to morph between the two phoneme filters.
    MorpherWaveform Waveform;

    //! This controls the frequency of the low-frequency oscillator used to morph between the two phoneme filters.
    //! Range: 0.0 to 10.0
    Float32 Rate;
}

alias wVocalMorpherParameters = tag_wVocalMorpherParameters;

//! The pitch shifter applies time-invariant pitch shifting to the input signal, over a one octave range and controllable at a semi-tone and cent resolution.
struct tag_wPitchShifterParameters
{
    // __cplusplus

    //! This sets the number of semitones by which the pitch is shifted. There are 12 semitones per
    //! octave. Negative values create a downwards shift in pitch, positive values pitch the sound
    //! upwards.
    //! Range: -12 to 12
    Int32 CoarseTune;

    //! This sets the number of cents between Semitones a pitch is shifted. A Cent is 1/100th of a
    //! Semitone. Negative values create a downwards shift in pitch, positive values pitch the sound
    //! upwards.
    //! Range: -50 to 50
    Int32 FineTune;
}

alias wPitchShifterParameters = tag_wPitchShifterParameters;

//! The ring modulator multiplies an input signal by a carrier signal in the time domain, resulting in tremolo or inharmonic effects.
enum ModulatorWaveform
{
    MWF_SINUSOID = 0,
    MWF_SAW = 1,
    MWF_SQUARE = 2,
    MWF_COUNT = 3
}

struct tag_wRingModulatorParameters
{
    //ModulatorWaveform waveform = EMW_SINUSOID) :

    // __cplusplus

    //! This is the frequency of the carrier signal. If the carrier signal is slowly varying (less than 20 Hz),
    //! the result is a tremolo (slow amplitude variation) effect. If the carrier signal is in the audio range,
    //! audible upper and lower sidebands begin to appear, causing an inharmonic effect. The carrier
    //! signal itself is not heard in the output.
    //! Range: 0.0 to 8000.0
    Float32 Frequency;

    //! This controls the cutoff frequency at which the input signal is high-pass filtered before being ring
    //! modulated. If the cutoff frequency is 0, the entire signal will be ring modulated. If the cutoff
    //! frequency is high, very little of the signal (only those parts above the cutoff) will be ring
    //! modulated.
    //! Range: 0.0 to 24000.0
    Float32 HighPassCutoff;

    //! This controls which waveform is used as the carrier signal. Traditional ring modulator and
    //! tremolo effects generally use a sinusoidal carrier. Sawtooth and square waveforms are may
    //! cause unpleasant aliasing.
    ModulatorWaveform Waveform;
}

alias wRingModulatorParameters = tag_wRingModulatorParameters;

//! The Auto-wah effect emulates the sound of a wah-wah pedal used with an electric guitar, or a mute on a brass instrument.
struct tag_wAutowahParameters
{
    // __cplusplus

    //! This property controls the time the filtering effect takes to sweep from minimum to maximum center frequency when it is triggered by input signal.
    //! Range: 0.0001 to 1.0
    Float32 AttackTime;

    //! This property controls the time the filtering effect takes to sweep from maximum back to base center frequency, when the input signal ends.
    //! Range: 0.0001 to 1.0
    Float32 ReleaseTime;

    //! This property controls the resonant peak, sometimes known as emphasis or Q, of the auto-wah
    //! band-pass filter. Resonance occurs when the effect boosts the frequency content of the sound
    //! around the point at which the filter is working. A high value promotes a highly resonant, sharp
    //! sounding effect.
    //! Range: 2.0 to 1000.0
    Float32 Resonance;

    //! This property controls the input signal level at which the band-pass filter will be fully opened.
    //! Range: 0.00003 to 31621.0
    Float32 PeakGain;
}

alias wAutowahParameters = tag_wAutowahParameters;

//! The Automatic Gain Control effect performs the same task as a studio compressor, evening out the audio dynamic range of an input sound.
struct tag_wCompressorParameters
{
    // __cplusplus

    //! The Compressor can only be switched on and off – it cannot be adjusted.
    bool Active;
}

alias wCompressorParameters = tag_wCompressorParameters;

//! The OpenAL Effects Extension EQ is very flexible, providing tonal control over four different adjustable frequency ranges.
struct tag_wEqualizerParameters
{
    // __cplusplus

    //! This property controls amount of cut or boost on the low frequency range.
    //! Range: 0.126 to 7.943
    Float32 LowGain;

    //! This property controls the low frequency below which signal will be cut off.
    //! Range: 50.0 to 800.0
    Float32 LowCutoff;

    //! This property allows you to cut / boost signal on the "mid1" range.
    //! Range: 0.126 to 7.943
    Float32 Mid1Gain;

    //! This property sets the center frequency for the "mid1" range.
    //! Range: 200.0 to 3000.0
    Float32 Mid1Center;

    //! This property controls the width of the "mid1" range.
    //! Range: 0.01 to 1.0
    Float32 Mid1Width;

    //! This property allows you to cut / boost signal on the "mid2" range.
    //! Range: 0.126 to 7.943
    Float32 Mid2Gain;

    //! This property sets the center frequency for the "mid2" range.
    //! Range: 1000.0 to 8000.0
    Float32 Mid2Center;

    //! This property controls the width of the "mid2" range.
    //! Range: 0.01 to 1.0
    Float32 Mid2Width;

    //! This property allows you to cut / boost the signal at high frequencies.
    //! Range: 0.126 to 7.943
    Float32 HighGain;

    //! This property controls the high frequency above which signal will be cut off.
    //! Range: 4000.0 to 16000.0
    Float32 HighCutoff;
}

alias wEqualizerParameters = tag_wEqualizerParameters;

struct tag_wShader
{
    //wMaterialTypes material_type;
    //Нельзя менять!
    Int32 material_type;
    void* irrShaderCallBack;
    tag_wShader* next_shader;
}

alias wShader = tag_wShader;

// __cplusplus
///wConsole///
void wConsoleSetFontColor (wConsoleFontColor c);

void wConsoleSetBackColor (wConsoleBackColor c);

Int32 wConsoleSaveDefaultColors ();

void wConsoleResetColors (Int32 defValues);

///wTexture//
wTexture* wTextureLoad (char* cptrFile);

wTexture* wTextureCreateRenderTarget (wVector2i size);

wTexture* wTextureCreate (char* name, wVector2i size, wColorFormat format);

void wTextureDestroy (wTexture* texture);

UInt32* wTextureLock (wTexture* texture);

void wTextureUnlock (wTexture* texture);

void wTextureSave (wTexture* texture, const(char)* file);

wImage* wTextureConvertToImage (wTexture* texture);

void wTextureGetInformation (
    wTexture* texture,
    wVector2u* size,
    UInt32* pitch,
    wColorFormat* format);

void wTextureMakeNormalMap (wTexture* texture, Float32 amplitude);

Int32 wTexturesSetBlendMode (
    wTexture* texturedest,
    wTexture* texturesrc,
    wVector2i offset,
    wBlendOperation operation);

void wTextureSetColorKey (wTexture* texture, wColor4s key);

void wTextureSetGray (wTexture** texture);

void wTextureSetAlpha (wTexture** texture, UInt32 value);

void wTextureSetInverse (wTexture** texture);

void wTextureSetBrightness (wTexture** texture, UInt32 value);

wTexture* wTextureCopy (wTexture* texture, char* name);

void wTextureSetContrast (wTexture** texture, Float32 value);

wTexture* wTextureFlip (wTexture** texture, Int32 mode);
// __cplusplus

void wTextureSetBlur (wTexture** texture, Float32 radius);

const(char)* wTextureGetFullName (wTexture* texture);

const(char)* wTextureGetInternalName (wTexture* texture);

void wTextureDraw (
    wTexture* texture,
    wVector2i pos,
    bool useAlphaChannel,
    wColor4s color);
// __cplusplus

void wTextureDrawEx (
    wTexture* texture,
    wVector2i pos,
    wVector2f scale,
    bool useAlphaChannel);
// __cplusplus

void wTextureDrawMouseCursor (wTexture* texture);

void wTextureDrawElement (
    wTexture* texture,
    wVector2i pos,
    wVector2i fromPos,
    wVector2i toPos,
    bool useAlphaChannel,
    wColor4s color);
// __cplusplus

void wTextureDrawElementStretch (
    wTexture* texture,
    wVector2i destFromPos,
    wVector2i destToPos,
    wVector2i sourceFromPos,
    wVector2i sourceToPos,
    bool useAlphaChannel);
// __cplusplus


void wTextureDrawAdvanced(wTexture* texture,
                        wVector2i pos,
                        wVector2i rotPoint,
                        Float32 rotation,
                        wVector2f scale,
                        bool useAlphaChannel=true,
                        wColor4s color=wCOLOR4s_WHITE,
                        wAntiAliasingMode aliasMode=wAntiAliasingMode.wAAM_SIMPLE,
                        bool bilinearFilter=true,
                        bool trilinearFilter=true,
                        bool anisotropFilter=true);
// __cplusplus

void wTextureDrawElementAdvanced(wTexture* texture,
                               wVector2i pos,
                               wVector2i fromPos,
                               wVector2i toPos,
                               wVector2i rotPoint,
                               Float32 rotAngleDeg,
                               wVector2f scale,
                               bool useAlphaChannel,
                               wColor4s color=wCOLOR4s_WHITE,
                               wAntiAliasingMode aliasMode=wAntiAliasingMode.wAAM_SIMPLE,
                               bool bilinearFilter=true,
                               bool trilinearFilter=true,
                               bool anisotropFilter=true);
// __cplusplus

///w2d///

void w2dDrawRect (wVector2i minPos, wVector2i maxPos, wColor4s color);
// __cplusplus

void w2dDrawRectWithGradient (
    wVector2i minPos,
    wVector2i maxPos,
    wColor4s colorLeftUp,
    wColor4s colorRightUp,
    wColor4s colorLeftDown,
    wColor4s colorRightDown);

void w2dDrawRectOutline (wVector2i minPos, wVector2i maxPos, wColor4s color);

void w2dDrawLine (wVector2i fromPos, wVector2i toPos, wColor4s color);
// __cplusplus

void w2dDrawPixel (wVector2i pos, wColor4s color);
// __cplusplus

void w2dDrawPolygon (
    wVector2i pos,
    Float32 Radius,
    wColor4s color,
    Int32 vertexCount);
// __cplusplus

///w3d///

void w3dDrawLine (wVector3f start, wVector3f end, wColor4s color);
// __cplusplus

void w3dDrawBox (wVector3f minPoint, wVector3f maxPoint, wColor4s color);
// __cplusplus

void w3dDrawTriangle (wTriangle triangle, wColor4s color);
// __cplusplus

///wFont///
wFont* wFontLoad (char* fontPath);

wFont* wFontAddToFont (char* fontPath, wFont* destFont);

wFont* wFontGetDefault ();

void wFontDraw (
    wFont* font,
    const(wchar_t)* wcptrText,
    wVector2i fromPos,
    wVector2i toPos,
    wColor4s color);
// __cplusplus

void wFontDestroy (wFont* font);

wVector2u wFontGetTextSize (wFont* font, const(wchar_t)* text);

void wFontSetKerningSize (wFont* font, wVector2u kerning);

wVector2u wFontGetKerningSize (wFont* font);

Int32 wFontGetCharacterFromPos (
    wFont* font,
    const(wchar_t)* text,
    Int32 xPixel);

void wFontSetInvisibleCharacters (wFont* font, const(wchar_t)* s);

wFont* wFontLoadFromTTF (
    char* fontPath,
    UInt32 size,
    bool antialias,
    bool transparency);
// __cplusplus

void wFontDrawAsTTF (
    wFont* font,
    const(wchar_t)* wcptrText,
    wVector2i fromPos,
    wVector2i toPos,
    wColor4s color,
    bool hcenter,
    bool vcenter);
// __cplusplus

///wImage//////
wImage* wImageLoad (char* cptrFile);

bool wImageSave (wImage* img, const(char)* file);

wImage* wImageCreate (wVector2i size, wColorFormat format);

void wImageRemove (wImage* image);

UInt32* wImageLock (wImage* image);

void wImageUnlock (wImage* image);

wTexture* wImageConvertToTexture (wImage* img, const(char)* name);

wColor4s wImageGetPixelColor (wImage* img, wVector2u pos);

void wImageSetPixelColor (
    wImage* img,
    wVector2u pos,
    wColor4s color,
    bool blend);
// __cplusplus

void wImageGetInformation (
    wImage* image,
    wVector2u* size,
    UInt32* pitch,
    wColorFormat* format);

///wTimer///
Float32 wTimerGetDelta ();

UInt32 wTimerGetTime ();

wRealTimeDate wTimerGetRealTimeAndDate ();

//Returns current real time in milliseconds of the system
UInt32 wTimerGetRealTime ();

//set the current time in milliseconds//
void wTimerSetTime (UInt32 newTime);

//Returns if the virtual timer is currently stopped
bool wTimerIsStopped ();

//Sets the speed of the timer
void wTimerSetSpeed (Float32 speed);

//Starts the virtual time
void wTimerStart ();

//Stops the virtual timer
void wTimerStop ();

//Advances the virtual time
void wTimerTick ();

///wLog///

void wLogSetLevel (wLoggingLevel level);
// __cplusplus

void wLogSetFile (char* path);

void wLogClear (char* path);

//mode=0/1

void wLogWrite (
    const(wchar_t)* hint,
    const(wchar_t)* text,
    char* path,
    UInt32 mode);
// __cplusplus

///wSystem////
UInt32 wSystemGetProcessorSpeed ();

UInt32 wSystemGetTotalMemory ();

UInt32 wSystemGetAvailableMemory ();

wVector2i wSystemGetMaxTextureSize ();

bool wSystemIsTextureFormatSupported (wColorFormat format);

void wSystemSetTextureCreationFlag (wTextureCreationFlag flag, bool value);

bool wSystemIsTextureCreationFlag (wTextureCreationFlag flag);

void wSystemSetClipboardText (const(wchar_t)* text);

void wSystemClearClipboard ();

const(wchar_t)* wSystemGetClipboardText ();

wTexture* wSystemCreateScreenShot (wVector2u minPos, wVector2u maxPos);

bool wSystemSaveScreenShot (const(char)* file);

///Get the current operation system version as string.
const(char)* wSystemGetVersion ();

///Check if a driver type is supported by the engine.
///Even if true is returned the driver may not be available for
///a configuration requested when creating the device.
bool wSystemIsDriverSupported (wDriverTypes testDriver);

///wDisplay///
///Get the graphics card vendor name.
const(char)* wDisplayGetVendor ();

Int32 wDisplayModesGetCount ();

Int32 wDisplayModeGetDepth (Int32 modeNumber);

wVector2u wDisplayModeGetResolution (Int32 ModeNumber);

wVector2u wDisplayGetCurrentResolution ();

Int32 wDisplayGetCurrentDepth ();

///Set the current Gamma Value for the Display.
void wDisplaySetGammaRamp (wColor3f gamma, float brightness, float contrast);

void wDisplayGetGammaRamp (wColor3f* gamma, float* brightness, float* contrast);

bool wDisplaySetDepth (UInt32 depth);

///wMath///

immutable Float32 wMathPI = 3.14159265359f;

immutable Float64 wMathPI64=3.1415926535897932384626433832795028841971693993751;

///Возвращает нормализованный вектор
wVector3f wMathVector3fNormalize (wVector3f source);

///Возвращает длину вектора
Float32 wMathVector3fGetLength (wVector3f vector);

///Get the rotations that would make a (0,0,1) direction vector
///point in the same direction as this direction vector.
wVector3f wMathVector3fGetHorizontalAngle (wVector3f vector);

///Возвращает инвертированный вектор (все координаты меняют знак)
wVector3f wMathVector3fInvert (wVector3f vector);

///Суммирует два вектора
wVector3f wMathVector3fAdd (wVector3f vector1, wVector3f vector2);

///Вычитает из вектора 1 вектор 2
wVector3f wMathVector3fSubstract (wVector3f vector1, wVector3f vector2);

///Векторное произведение векторов
wVector3f wMathVector3fCrossProduct (wVector3f vector1, wVector3f vector2);
///Скалярное произведение векторов
Float32 wMathVector3fDotProduct (wVector3f vector1, wVector3f vector2);

///Определяет кратчайшее расстояние между векторами
Float32 wMathVector3fGetDistanceFrom (wVector3f vector1, wVector3f vector2);

///Возвращает интерполированный вектор
wVector3f wMathVector3fInterpolate (
    wVector3f vector1,
    wVector3f vector2,
    Float64 d);

///Возвращает случайное число из интервала (first, last)
Float64 wMathRandomRange (Float64 first, Float64 last);

///Из градусов- в радианы///
Float32 wMathDegToRad (Float32 degrees);

///Из радиан- в градусы
Float32 wMathRadToDeg (Float32 radians);

///Математически правильное округление///
Float32 wMathRound (Float32 value);

///Округление в большую сторону///
Int32 wMathCeil (Float32 value);

///Округление в меньшую сторону///
Int32 wMathFloor (Float32 value);

///returns if a equals b, taking possible rounding errors into account

bool wMathFloatEquals (Float32 value1, Float32 value2, Float32 tolerance);
// __cplusplus

///returns if a equals b, taking an explicit rounding tolerance into account

bool wMathIntEquals (Int32 value1, Int32 value2, Int32 tolerance);
// __cplusplus

///returns if a equals b, taking an explicit rounding tolerance into account

bool wMathUIntEquals (UInt32 value1, UInt32 value2, UInt32 tolerance);
// __cplusplus

///returns if a equals zero, taking rounding errors into account

bool wMathFloatIsZero (Float32 value, Float32 tolerance);
// __cplusplus

///returns if a equals zero, taking rounding errors into account

bool wMathIntIsZero (Int32 value, Int32 tolerance);
// __cplusplus

///returns if a equals zero, taking rounding errors into account

bool wMathUIntIsZero (UInt32 value, UInt32 tolerance);
// __cplusplus

///Возвращает больший Float32 из двух///
Float32 wMathFloatMax2 (Float32 value1, Float32 value2);

///Возвращает больший Float32 из трех///
Float32 wMathFloatMax3 (Float32 value1, Float32 value2, Float32 value3);

///Возвращает больший Int32 из двух///
Float32 wMathIntMax2 (Int32 value1, Int32 value2);

///Возвращает больший Int32 из трех///
Float32 wMathIntMax3 (Int32 value1, Int32 value2, Int32 value3);

///Возвращает меньший Float32 из двух///
Float32 wMathFloatMin2 (Float32 value1, Float32 value2);

///Возвращает меньший Float32 из трех///
Float32 wMathMinMax3 (Float32 value1, Float32 value2, Float32 value3);

///Возвращает меньший Int32 из двух///
Float32 wMathIntMin2 (Int32 value1, Int32 value2);

///Возвращает меньший Int32 из трех///
Float32 wMathIntMin3 (Int32 value1, Int32 value2, Int32 value3);

///wUtil///

/// Конвертирует трехмерный вектор с float-компонентами в строку с разделителем s

const(char)* wUtilVector3fToStr (wVector3f vector, char* s, bool addNullChar);
// __cplusplus

/// Конвертирует двумерный вектор с float-компонентами в строку с разделителем s

const(char)* wUtilVector2fToStr (wVector2f vector, char* s, bool addNullChar);
// __cplusplus

///Конвертирует цвет с UInt8-компонентами в строку с разделителем s

const(char)* wUtilColor4sToStr (wColor4s color, char* s, bool addNullChar);
// __cplusplus

///Конвертирует цвет с float-компонентами в строку с разделителем s

const(char)* wUtilColor4fToStr (wColor4f color, char* s, bool addNullChar);
// __cplusplus

UInt32 wUtilColor4sToUInt (wColor4s color);

UInt32 wUtilColor4fToUInt (wColor4f color);

wColor4s wUtilUIntToColor4s (UInt32 color);

wColor4f wUtilUIntToColor4f (UInt32 color);

///Convert a simple string of base 10 digits into a signed 32 bit integer.
Int32 wUtilStrToInt (char* str);

const(char)* wUtilIntToStr (Int32 value, bool addNullChar);
// __cplusplus

///Converts a sequence of digits into a whole positive floating point value.
///Only digits 0 to 9 are parsed.
///Parsing stops at any other character, including sign characters or a decimal point.
Float32 wUtilStrToFloat (char* str);

///Конвертирует Float32 в строку

const(char)* wUtilFloatToStr (Float32 value, bool addNullChar);
// __cplusplus

///Convert a simple string of base 10 digits into an unsigned 32 bit integer.
UInt32 wUtilStrToUInt (char* str);

///Конвертирует UInt32 в строку

const(char)* wUtilUIntToStr (UInt32 value, bool addNullChar);
// __cplusplus

///swaps the content of the passed parameters
void wUtilSwapInt (int* value1, int* value2);

///swaps the content of the passed parameters
void wUtilSwapUInt (UInt32* value1, UInt32* value2);

///swaps the content of the passed parameters
void wUtilSwapFloat (float* value1, float* value2);

///Конвертирует расширенную строку в С-строку
const(char)* wUtilWideStrToStr (const(wchar_t)* str);

///Конвертирует С-строку в расширенную строку
const(wchar_t)* wUtilStrToWideStr (const(char)* str);

///Добавляет символ конца строки
void wUtilStrAddNullChar (const(char*)* str);

///Добавляет символ конца строки к расширенной строке
void wUtilWideStrAddNullChar (const(wchar_t*)* str);

///wEngine///

bool wEngineStart (
    wDriverTypes iDevice,
    wVector2u size,
    UInt32 iBPP,
    bool boFullscreen,
    bool boShadows,
    bool boCaptureEvents,
    bool vsync);
// __cplusplus

void wEngineCloseByEsc ();

bool wEngineStartAdvanced (
    wDriverTypes drivertype,
    wVector2u size,
    UInt32 bits,
    bool fullscreen,
    bool shadows,
    bool dontignoreinput,
    bool vsyncEnabled,
    wDeviceTypes devicetype,
    bool doublebufferEnabled,
    UInt16 antialiasLevel,
    bool highprecisionfpu,
    void* winId);

// __cplusplus

void wEngineSetTransparentZWrite (bool value);

bool wEngineRunning ();

///Pause execution and let other processes to run for a specified amount of time.

void wEngineSleep (UInt32 Ms, bool pauseTimer);

///Cause the device to temporarily pause execution and let other processes run.
void wEngineYield ();

void wEngineSetViewPort (wVector2i fromPos, wVector2i toPos);

bool wEngineIsQueryFeature (wVideoFeatureQuery feature);

void wEngineDisableFeature (wVideoFeatureQuery feature, bool flag);

bool wEngineStop ();

void wEngineSetFPS (UInt32 limit);

//wMaterial* wEngineGetGlobalMaterial();

/*
Get the 2d override material for altering its values.
The 2d override materual allows to alter certain render states of the 2d methods.
Not all members of SMaterial are honored, especially not MaterialType and Textures.
Moreover, the zbuffer is always ignored, and lighting is always off.
All other flags can be changed, though some might have to effect in most cases.
Please note that you have to enable/disable this effect with enableInitMaterial2D().
This effect is costly, as it increases the number of state changes considerably.
Always reset the values when done.
*/
wMaterial* wEngineGet2dMaterial ();

void wEngineSet2dMaterial (bool value);

Int32 wEngineGetFPS ();

void wEngineShowLogo (bool value);

///wScene/////

bool wSceneBegin (wColor4s color);
// __cplusplus

bool wSceneBeginAdvanced (
    wColor4s backColor,
    bool clearBackBuffer,
    bool clearZBuffer);
// __cplusplus

void wSceneLoad (const(char)* filename);

void wSceneSave (const(char)* filename);

void wSceneDrawAll ();

bool wSceneEnd ();

void wSceneDrawToTexture (wTexture* renderTarget);

void wSceneSetRenderTarget (
    wTexture* renderTarget,
    wColor4s backColor,
    bool clearBackBuffer,
    bool clearZBuffer);
// __cplusplus

void wSceneSetAmbientLight (wColor4f color);

wColor4f wSceneGetAmbientLight ();

void wSceneSetShadowColor (wColor4s color);

wColor4s wSceneGetShadowColor ();

void wSceneSetFog (
    wColor4s color,
    wFogType fogtype,
    Float32 start,
    Float32 end,
    Float32 density,
    bool pixelFog,
    bool rangeFog);
// __cplusplus

void wSceneGetFog (
    wColor4s* color,
    wFogType* fogtype,
    float* start,
    float* end,
    float* density,
    bool* pixelFog,
    bool* rangeFog);

wNode* wSceneGetActiveCamera ();

///Поиск текстуры по АБСОЛЮТНОМУ пути
///Если требуется искать по относительному пути,
///используйте сначала wFileGetAbsolutePath
wTexture* wSceneGetTextureByName (char* name);

// When animating a mesh by "Morphing" or "Skeletal Animation" such as "*.md3", "*.x" and "*.b3d" using "Shaders" for rendering we can improve the final render if we "Cyclically Update" the "Tangents" and "Binormals"..
// We presume that our meshes are, among others, textured with a "NORMAL MAP" used by the "Shader" (cg, hlsl, or glsl etc) in calculating diffuse and specular.
// We also have one or more lights used by the shader.

// Update TANGENTS & BINORMALS at every frame for a skinned animation..

// We dont want to do this for static meshes like levels etc..
// We also dont want to do it for Rotating, Scaled and translated meshes..(we can however, as a bonus, scale, rotate and translate these)
// Only for animated skinned and morph based meshes..
// This is loose code that works. If anyone can improve it for the engine itself that would be great..
// You'll probably ID possible improvements immediately!

// At every N'th Frame we loop through all the vertices..
// 1. In the loop we Access the VERTEX of POINT A of the "INDEXED TRIANGLE"..
// 2. We interrogate the "OTHER TWO" VERTICES (which thankfully do change at each frame) for their Positions, Normals, and UV Coords to
//    Genertate a "BRAND NEW" (animated) TANGENT and BINORMAL. (We may want to calculate the the "Binormal" in the SHADER to save time)
// 3. We REWRITE the Tangent and Binormal for our SELECTED TRIANGLE POINT.
// 4. We DO THE SAME for POINTS B and C..
//

//  GENERATE "LIVING" TANGENTS & BINBORMALS
//  REMEMBER!
//  WE NEED "LOOP THROUGH ALL ITS BUFFERS"
//  WE NEED "LOOP THROUGH ALL THOSE BUFFER VERTICES"
// Possible types of (animated) meshes.
// Enumerator:
// 1  EAMT_MD2            Quake 2 MD2 model file..
// 2  EAMT_MD3            Quake 3 MD3 model file..
// 10 EAMT_MDL_HALFLIFE   Halflife MDL model file..
// Below is what an item type must be for it to qualify for Tangent Updates..
// 11 EAMT_SKINNED        generic skinned mesh "*.x" "*.b3d" etc.. (see Morphed too!)
//
// We want to change tangents for skinned meshes only so we must determine which ones are "Skinned"..
// This may change if we add and remove meshes during runtime..

void wMeshUpdateTangentsAndBinormals (wMesh* mesh);

void wSceneDestroyAllTextures ();

void wSceneDestroyAllNodes ();

///Можно для поиска меша использовать относительный путь
wMesh* wSceneGetMeshByName (char* name);

wMesh* wSceneGetMeshByIndex (uint index);

UInt32 wSceneGetMeshesCount ();

void wSceneDestroyAllMeshes ();

bool wSceneIsMeshLoaded (const(char)* filePath);

void wSceneDestroyAllUnusedMeshes ();

UInt32 wSceneGetPrimitivesDrawn ();

UInt32 wSceneGetNodesCount ();

wNode* wSceneGetNodeById (Int32 id);

wNode* wSceneGetNodeByName (char* name);

wNode* wSceneGetRootNode ();

///wWindow///
void wWindowSetCaption (const(wchar_t)* wcptrText);

void wWindowGetSize (wVector2u* size);

bool wWindowIsFullscreen ();

bool wWindowIsResizable ();

bool wWindowIsActive ();

bool wWindowIsFocused ();

bool wWindowIsMinimized ();

void wWindowMaximize ();

void wWindowMinimize ();

void wWindowRestore ();

void wWindowSetResizable (bool resizable);

void wWindowMove (wVector2u pos);

void wWindowPlaceToCenter ();

void wWindowResize (wVector2u newSize);

void wWindowSetFullscreen (bool value);

bool wWindowSetDepth (UInt32 depth);
///wPostEffect///

wPostEffect* wPostEffectCreate (
    wPostEffectId effectnum,
    wPostEffectQuality quality,
    Float32 value1,
    Float32 value2,
    Float32 value3,
    Float32 value4,
    Float32 value5,
    Float32 value6,
    Float32 value7,
    Float32 value8);
// __cplusplus

void wPostEffectDestroy (wPostEffect* ppEffect);

void wPostEffectSetParameters (
    wPostEffect* ppEffect,
    Float32 para1,
    Float32 para2,
    Float32 para3,
    Float32 para4,
    Float32 para5,
    Float32 para6,
    Float32 para7,
    Float32 para8);
// __cplusplus

void wPostEffectsDestroyAll ();

///wXEffects///

void wXEffectsStart (
    bool vsm,
    bool softShadows,
    bool bitDepth32,
    wColor4s color);
// __cplusplus

void wXEffectsEnableDepthPass (bool enable);

void wXEffectsAddPostProcessingFromFile (const(char)* name, Int32 effectType);
// __cplusplus

void wXEffectsSetPostProcessingUserTexture (wTexture* texture);

void wXEffectsAddShadowToNode (
    wNode* node,
    wFilterType filterType,
    wShadowMode shadowType);

void wXEffectsRemoveShadowFromNode (wNode* node);

void wXEffectsExcludeNodeFromLightingCalculations (wNode* node);

void wXEffectsAddNodeToDepthPass (wNode* node);

void wXEffectsSetAmbientColor (wColor4s color);

void wXEffectsSetClearColor (wColor4s color);

void wXEffectsAddShadowLight (
    UInt32 shadowDimen,
    wVector3f position,
    wVector3f target,
    wColor4f color,
    Float32 lightNearDist,
    Float32 lightFarDist,
    Float32 angleDeg);

UInt32 wXEffectsGetShadowLightsCount ();

wTexture* wXEffectsGetShadowMapTexture (UInt32 resolution, bool secondary);
// __cplusplus

wTexture* wXEffectsGetDepthMapTexture ();

void wXEffectsSetScreenRenderTargetResolution (wVector2u size);

void wXEffectsSetShadowLightPosition (UInt32 index, wVector3f position);

wVector3f wXEffectsGetShadowLightPosition (UInt32 index);

void wXEffectsSetShadowLightTarget (UInt32 index, wVector3f target);

wVector3f wXEffectsGetShadowLightTarget (UInt32 index);

void wXEffectsSetShadowLightColor (UInt32 index, wColor4f color);

wColor4f wXEffectsGetShadowLightColor (UInt32 index);

void wXEffectsSetShadowLightMapResolution (UInt32 index, UInt32 resolution);

UInt32 wXEffectsGetShadowLightMapResolution (UInt32 index);

Float32 wXEffectsGetShadowLightFarValue (UInt32 index);

///wAnimator///
wAnimator* wAnimatorFollowCameraCreate (wNode* node, wVector3f position);

wAnimator* wAnimatorCollisionResponseCreate (
    wSelector* selector,
    wNode* node,
    Float32 slidingValue);
// __cplusplus

void wAnimatorCollisionResponseSetParameters (
    wAnimator* anim,
    wAnimatorCollisionResponse params);

void wAnimatorCollisionResponseGetParameters (
    wAnimator* anim,
    wAnimatorCollisionResponse* params);

wAnimator* wAnimatorDeletingCreate (wNode* node, Int32 delete_after);

wAnimator* wAnimatorFlyingCircleCreate (
    wNode* node,
    wVector3f pos,
    Float32 radius,
    Float32 speed,
    wVector3f direction,
    Float32 startPos,
    Float32 radiusEllipsoid);

wAnimator* wAnimatorFlyingStraightCreate (
    wNode* node,
    wVector3f startPoint,
    wVector3f endPoint,
    UInt32 time,
    bool loop);

wAnimator* wAnimatorRotationCreate (wNode* node, wVector3f pos);

wAnimator* wAnimatorSplineCreate (
    wNode* node,
    Int32 iPoints,
    wVector3f* points,
    Int32 time,
    Float32 speed,
    Float32 tightness);

wAnimator* wAnimatorFadingCreate (
    wNode* node,
    Int32 delete_after,
    Float32 scale);

void wAnimatorDestroy (wNode* node, wAnimator* anim);

///wTpsCamera///
wNode* wTpsCameraCreate (const(char)* name);

void wTpsCameraDestroy (wNode* ctrl);

void wTpsCameraUpdate (wNode* ctrl);

void wTpsCameraSetTarget (wNode* ctrl, wNode* node);

void wTpsCameraRotateHorizontal (wNode* ctrl, Float32 rotVal);

void wTpsCameraRotateVertical (wNode* ctrl, Float32 rotVal);

void wTpsCameraSetHorizontalRotation (wNode* ctrl, Float32 rotVal);

void wTpsCameraSetVerticalRotation (wNode* ctrl, Float32 rotVal);

void wTpsCameraZoomIn (wNode* ctrl);

void wTpsCameraZoomOut (wNode* ctrl);

wNode* wTpsCameraGetCamera (wNode* ctrl);

void wTpsCameraSetCurrentDistance (wNode* ctrl, Float32 dist);

void wTpsCameraSetRelativeTarget (wNode* ctrl, wVector3f target);

void wTpsCameraSetDefaultDistanceDirection (wNode* ctrl, wVector3f dir);

void wTpsCameraSetMaximalDistance (wNode* ctrl, Float32 value);

void wTpsCameraSetMinimalDistance (wNode* ctrl, Float32 value);

void wTpsCameraSetZoomStepSize (wNode* ctrl, Float32 value);

void wTpsCameraSetHorizontalSpeed (wNode* ctrl, Float32 value);

void wTpsCameraSetVerticalSpeed (wNode* ctrl, Float32 value);
///wFpsCamera///

wNode* wFpsCameraCreate (
    Float32 rotateSpeed,
    Float32 moveSpeed,
    wKeyMap* keyMapArray,
    Int32 keyMapSize,
    bool noVerticalMovement,
    Float32 jumpSpeed);
// __cplusplus

Float32 wFpsCameraGetSpeed (wNode* camera);

void wFpsCameraSetSpeed (wNode* camera, Float32 newSpeed);

Float32 wFpsCameraGetRotationSpeed (wNode* camera);

void wFpsCameraSetRotationSpeed (wNode* camera, Float32 rotSpeed);

void wFpsCameraSetKeyMap (wNode* camera, wKeyMap* map, UInt32 count);

void wFpsCameraSetVerticalMovement (wNode* camera, bool value);

void wFpsCameraSetInvertMouse (wNode* camera, bool value);

void wFpsCameraSetMaxVerticalAngle (wNode* camera, float newValue);

Float32 wFpsCameraGetMaxVerticalAngle (wNode* camera);

///wCamera///
wNode* wCameraCreate (wVector3f pos, wVector3f target);

wNode* wMayaCameraCreate (
    Float32 rotateSpeed,
    Float32 zoomSpeed,
    Float32 moveSpeed);

void wCameraSetTarget (wNode* camera, wVector3f target);

wVector3f wCameraGetTarget (wNode* camera);

wVector3f wCameraGetUpDirection (wNode* camera);

void wCameraSetUpDirection (wNode* camera, wVector3f upDir);

void wCameraGetOrientation (
    wNode* camera,
    wVector3f* upDir,
    wVector3f* forwardDir,
    wVector3f* rightDir);

void wCameraSetClipDistance (
    wNode* camera,
    Float32 farDistance,
    Float32 nearDistance);
// __cplusplus

void wCameraSetActive (wNode* camera);

void wCameraSetFov (wNode* camera, Float32 fov);

Float32 wCameraGetFov (wNode* camera);

void wCameraSetOrthogonal (wNode* camera, wVector3f vec);

void wCameraRevolve (wNode* camera, wVector3f angleDeg, wVector3f offset);

void wCameraSetUpAtRightAngle (wNode* camera);

void wCameraSetAspectRatio (wNode* camera, Float32 aspectRatio);

void wCameraSetInputEnabled (wNode* camera, bool value);

bool wCameraIsInputEnabled (wNode* camera);

///wRtsCamera///
wNode* wRtsCameraCreate (
    wVector3f pos,
    wVector2f offsetX,
    wVector2f offsetZ,
    wVector2f offsetDistance,
    wVector2f offsetAngle,
    Float32 driftSpeed,
    Float32 scrollSpeed,
    Float32 mouseSpeed,
    Float32 orbit,
    UInt32 mouseButtonActive);

///wCollision///
wSelector* wCollisionGroupCreate ();

void wCollisionGroupAddCollision (wSelector* group, wSelector* selector);

void wCollisionGroupRemoveAll (wSelector* geoup);

void wCollisionGroupRemoveCollision (wSelector* group, wSelector* selector);

wSelector* wCollisionCreateFromMesh (wMesh* mesh, wNode* node, Int32 iframe);

wSelector* wCollisionCreateFromBatchingMesh (wMesh* mesh, wNode* node);

wSelector* wCollisionCreateFromMeshBuffer (
    wMeshBuffer* meshbuffer,
    wNode* node);

wSelector* wCollisionCreateFromOctreeMesh (
    wMesh* mesh,
    wNode* node,
    Int32 iframe);

wSelector* wCollisionCreateFromBox (wNode* node);

wSelector* wCollisionCreateFromTerrain (wNode* node, Int32 level_of_detail);

void wNodeRemoveCollision (wNode* node, wSelector* selector);

void wNodeAddCollision (wNode* node, wSelector* selector);

wNode* wCollisionGetNodeFromCamera (wNode* camera);

void wCameraSetCollisionWithScene (
    wNode* camera,
    wVector3f radius,
    wVector3f gravity,
    wVector3f offset,
    Float32 slidingValue);
// __cplusplus

wNode* wCollisionGetNodeFromRay (wVector3f* vectorStart, wVector3f* vectorEnd);

wNode* wCollisionGetNodeChildFromRay (
    wNode* node,
    Int32 id,
    bool recurse,
    wVector3f* vectorStart,
    wVector3f* vectorEnd);

wNode* wCollisionGetNodeAndPointFromRay (
    wVector3f* vectorStart,
    wVector3f* vectorEnd,
    wVector3f* colPoint,
    wVector3f* normal,
    Int32 id,
    wNode* rootNode);

wNode* wCollisionGetNodeFromScreen (
    wVector2i screenPos,
    Int32 idBitMask,
    bool bNoDebugObjects,
    wNode* root);
// __cplusplus

wVector2i wCollisionGetScreenCoordFrom3dPosition (wVector3f pos);

void wCollisionGetRayFromScreenCoord (
    wNode* camera,
    wVector2i screenCoord,
    wVector3f* vectorStart,
    wVector3f* vectorEnd);

wVector3f wCollisionGet3dPositionFromScreen (
    wNode* camera,
    wVector2i screenPos,
    wVector3f normal,
    Float32 distanceFromOrigin);

wVector2f wCollisionGet2dPositionFromScreen (
    wNode* camera,
    wVector2i screenPos);

bool wCollisionGetPointFromRay (
    wSelector* ts,
    wVector3f* vectorStart,
    wVector3f* vectorEnd,
    wVector3f* collisionPoint,
    wVector3f* vectorNormal,
    wTriangle* collisionTriangle);
// __cplusplus

wNode* wCollisionGetNodeChildFromPoint (
    wNode* node,
    Int32 id,
    bool recurse,
    wVector3f* vectorPoint);

void wCollisionGetResultPosition (
    wSelector* selector,
    wVector3f* ellipsoidPosition,
    wVector3f* ellipsoidRadius,
    wVector3f* velocity,
    wVector3f* gravity,
    Float32 slidingSpeed,
    wVector3f* outPosition,
    wVector3f* outHitPosition,
    int* outFalling);

///wFile///
void wFileAddZipArchive (char* cptrFile, bool boIgnoreCase, bool boIgnorePaths);

// __cplusplus
void wFileAddArchive (
    char* cptrFile,
    bool boIgnoreCase,
    bool boIgnorePaths,
    wFileArchiveType aType,
    const(char)* password);

void wFileSetWorkingDirectory (const(char)* cptrPath);

const(char)* wFileGetWorkingDirectory ();

void wFileAddPakArchive (char* cptrFile, bool boIgnoreCase, bool boIgnorePaths);

void wFileAddDirectory (char* cptrFile, bool boIgnoreCase, bool boIgnorePaths);

bool wFileIsExist (char* cptrFile);

const(char)* wFileGetAbsolutePath (char* cptrPath);

const(char)* wFileGetRelativePath (char* cptrPath, char* directory);

///Get the base part of a filename, i.e. the name without the directory part.
///If no directory is prefixed, the full name is returned.
const(char)* wFileGetBaseName (char* cptrPath, bool keepExtension);

const(char)* wFileGetDirectory (char* cptrPath);

///for read///
wFile* wFileOpenForRead (char* cptrFile);

Int32 wFileRead (wFile* file, void* buffer, UInt32 sizeToRead);

Int64 wFileGetSize (wFile* file);

///for write///
wFile* wFileCreateForWrite (char* cptrFile, bool append);
Int32 wFileWrite (wFile* file, const(void)* buffer, UInt32 sizeToWrite);

///for read/write///
const(char)* wFileGetName (wFile* file);

Int64 wFileGetPos (wFile* file);

bool wFileSeek (wFile* file, Int64 finalPos, bool relativeMovement);

void wFileClose (wFile* file);

///XMLReader///
wXmlReader* wXmlReaderCreate (char* cptrFile);

wXmlReader* wXMLReaderCreateUTF8 (char* cptrFile);

//Returns attribute count of the current XML node
UInt32 wXmlGetAttributesCount (wXmlReader* xml);

//Returns the value of an attribute
const(wchar_t)* wXmlGetAttributeNameByIdx (wXmlReader* xml, Int32 idx);

//Returns the value of an attribute
const(wchar_t)* wXmlGetAttributeValueByIdx (wXmlReader* xml, Int32 idx);

//Returns the value of an attribute
const(wchar_t)* wXmlGetAttributeValueByName (
    wXmlReader* xml,
    const(wchar_t)* name);

//Returns the value of an attribute as float
Float32 wXmlGetAttributeValueFloatByIdx (wXmlReader* xml, Int32 idx);

//Returns the value of an attribute as float
Float32 wXmlGetAttributeValueFloatByName (
    wXmlReader* xml,
    const(wchar_t)* name);

//Returns the value of an attribute as integer
Int32 wXmlGetAttributeValueIntByIdx (wXmlReader* xml, Int32 idx);

//Returns the value of an attribute as integer
Int32 wXmlGetAttributeValueIntByName (wXmlReader* xml, const(wchar_t)* name);

//Returns the value of an attribute in a safe way
const(wchar_t)* wXmlGetAttributeValueSafeByName (
    wXmlReader* xml,
    const(wchar_t)* name);

//Returns the name of the current node
const(wchar_t)* wXmlGetNodeName (wXmlReader* xml);

//Returns data of the current node
const(wchar_t)* wXmlGetNodeData (wXmlReader* xml);

//Returns format of the source xml file
wTextFormat wXmlGetSourceFormat (wXmlReader* xml);

//Returns format of the strings returned by the parser
wTextFormat wXmlGetParserFormat (wXmlReader* xml);

//Returns the type of the current XML node
wXmlNodeType wXmlGetNodeType (wXmlReader* xml);

//Returns if an element is an empty element, like <foo />
bool wXmlIsEmptyElement (wXmlReader* xml);

//Reads forward to the next xml node
bool wXmlRead (wXmlReader* xml);

void wXmlReaderDestroy (wXmlReader* xml);

///XmlWriter///
wXmlWriter* wXmlWriterCreate (char* cptrFile);
//Writes the closing tag for an element. Like "</foo>"
void wXmlWriteClosingTag (wXmlWriter* xml, const(wchar_t)* name);

//Writes a comment into the xml file
void wXmlWriteComment (wXmlWriter* xml, const(wchar_t)* comment);

//Writes a line break
void wXmlWriteLineBreak (wXmlWriter* xml);

//Writes a text into the file
void wXmlWriteText (wXmlWriter* xml, const(wchar_t)* file);

//Writes an xml 1.0 heade
void wXmlWriteHeader (wXmlWriter* xml);

void wXmlWriteElement (
    wXmlWriter* xml,
    const(wchar_t)* name,
    bool empty,
    const(wchar_t)* attr1Name,
    const(wchar_t)* attr1Value,
    const(wchar_t)* attr2Name,
    const(wchar_t)* attr2Value,
    const(wchar_t)* attr3Name,
    const(wchar_t)* attr3Value,
    const(wchar_t)* attr4Name,
    const(wchar_t)* attr4Value,
    const(wchar_t)* attr5Name,
    const(wchar_t)* attr5Value);

void wXmlWriterDestroy (wXmlWriter* xml);

///wInput///
///keyboard///
///Get character without waiting for Return to be pressed.
bool wInputWaitKey ();

bool wInputIsKeyEventAvailable ();

wKeyEvent* wInputReadKeyEvent ();

bool wInputIsKeyUp (wKeyCode num);

bool wInputIsKeyHit (wKeyCode num);

bool wInputIsKeyPressed (wKeyCode num);

///mouse///
bool wInputIsMouseEventAvailable ();

wMouseEvent* wInputReadMouseEvent ();

void wInputSetCursorVisible (bool boShow);

bool wInputIsCursorVisible ();

void wInputSetMousePosition (wVector2i* position);

void wInputGetMousePosition (wVector2i* position);

void wInputSetMouseLogicalPosition (wVector2f* position);

void wInputGetMouseLogicalPosition (wVector2f* position);

Float32 wInputGetMouseWheel ();

void wInputGetMouseDelta (wVector2i* deltaPos);

bool wInputIsMouseUp (wMouseButtons num);

bool wInputIsMouseHit (wMouseButtons num);

bool wInputIsMousePressed (wMouseButtons num);

Int32 wInputGetMouseX ();

Int32 wInputGetMouseY ();

Int32 wInputGetMouseDeltaX ();

Int32 wInputGetMouseDeltaY ();

///joystick///
bool wInputActivateJoystick ();

UInt32 wInputGetJoysitcksCount ();

void wInputGetJoystickInfo (UInt32 joyIndex, wJoystickInfo* joyInfo);

bool wInputIsJoystickEventAvailable ();

wJoystickEvent* wInputReadJoystickEvent ();

///wLight///
wNode* wLightCreate (wVector3f position, wColor4f color, Float32 radius);

void wLightSetAmbientColor (wNode* light, wColor4f color);

wColor4f wLightGetAmbientColor (wNode* light);

void wLightSetSpecularColor (wNode* light, wColor4f color);

wColor4f wLightGetSpecularColor (wNode* light);

void wLightSetAttenuation (wNode* light, wVector3f attenuation); //.x-constant, .y- linear, .z- quadratic

wVector3f wLightGetAttenuation (wNode* light);

void wLightSetCastShadows (wNode* light, bool castShadows);

bool wLightIsCastShadows (wNode* light);

void wLightSetDiffuseColor (wNode* light, wColor4f color);

wColor4f wLightGetDiffuseColor (wNode* light);

void wLightSetFallOff (wNode* light, Float32 FallOff);

Float32 wLightGetFallOff (wNode* light);

void wLightSetInnerCone (wNode* light, Float32 InnerCone);

Float32 wLightGetInnerCone (wNode* light);

void wLightSetOuterCone (wNode* light, Float32 OuterCone);

Float32 wLightGetOuterCone (wNode* light);

void wLightSetRadius (wNode* light, Float32 Radius);

Float32 wLightGetRadius (wNode* light);

void wLightSetType (wNode* light, wLightType Type);

wLightType wLightGetType (wNode* light);

//Read-ONLY! Direction of the light.
//If Type is WLT_POINT, it is ignored.
//Changed via light scene node's rotation.
wVector3f wLightGetDirection (wNode* light);

///wBillBoardGroup///

wNode* wBillBoardGroupCreate (
    wVector3f position,
    wVector3f rotation,
    wVector3f scale);
// __cplusplus

void wBillBoardGroupSetShadows (
    wNode* node,
    wVector3f direction,
    Float32 intensity,
    Float32 ambient);

void wBillBoardGroupResetShadows (wNode* node);

UInt32 wBillBoardGroupGetSize (wNode* node);

wMeshBuffer* wBillBoardGroupGetMeshBuffer (wNode* node);

wBillboard* wBillBoardGroupGetFirst (wNode* node);

void wBillBoardGroupUpdateForce (wNode* node);

///wBillBoard///
wBillboard* wBillBoardAddToGroup (
    wNode* node,
    wVector3f position,
    wVector2f size,
    Float32 roll,
    wColor4s color);

wBillboard* wBillBoardAddToGroupByAxis (
    wNode* node,
    wVector3f position,
    wVector2f size,
    Float32 roll,
    wColor4s color,
    wVector3f axis);

void wBillBoardRemoveFromGroup (wNode* node, wBillboard* billboard);

wNode* wBillBoardCreate (wVector3f position, wVector2f size);

void wBillboardSetEnabledAxis (wNode* billboard, wBillboardAxisParam param);

wBillboardAxisParam wBillboardGetEnabledAxis (wNode* billboard);

void wBillBoardSetColor (wNode* node, wColor4s topColor, wColor4s bottomColor);

void wBillBoardSetSize (wNode* node, wVector2f size);

wNode* wBillboardCreateText (
    wVector3f position,
    wVector2f size,
    wFont* font,
    const(wchar_t)* text,
    wColor4s topColor,
    wColor4s bottomColor);

///wSkyBox///
wNode* wSkyBoxCreate (
    wTexture* texture_up,
    wTexture* texture_down,
    wTexture* texture_left,
    wTexture* texture_right,
    wTexture* texture_front,
    wTexture* texture_back);

///wSkyDome///

wNode* wSkyDomeCreate (
    wTexture* texture_file,
    UInt32 horiRes,
    UInt32 vertRes,
    Float64 texturePercentage,
    Float64 spherePercentage,
    Float64 domeRadius);
// __cplusplus

void wSkyDomeSetColor (
    wNode* dome,
    wColor4s horizonColor,
    wColor4s zenithColor);

void wSkyDomeSetColorBand (
    wNode* dome,
    wColor4s horizonColor,
    Int32 position,
    Float32 fade,
    bool additive);

void wSkyDomeSetColorPoint (
    wNode* dome,
    wColor4s horizonColor,
    wVector3f position,
    Float32 radius,
    Float32 fade,
    bool additive);

///wLodManager///
wNode* wLodManagerCreate (
    UInt32 fadeScale,
    bool useAlpha,
    void function (UInt32, wNode*) callback);

void wLodManagerAddMesh (wNode* node, wMesh* mesh, Float32 distance);

void wLodManagerSetMaterialMap (
    wNode* node,
    wMaterialTypes source,
    wMaterialTypes target);

///wZoneManager///
wNode* wZoneManagerCreate (
    Float32 initialNearDistance,
    Float32 initialFarDistance);

void wZoneManagerSetProperties (
    wNode* node,
    Float32 newNearDistance,
    Float32 newFarDistance,
    bool accumulateChildBoxes);

void wZoneManagerSetBoundingBox (
    wNode* node,
    wVector3f position,
    wVector3f size);

void wZoneManagerAddTerrain (
    wNode* node,
    wNode* terrainSource,
    char* structureMap,
    char* colorMap,
    char* detailMap,
    wVector2i pos,
    Int32 sliceSize);

///wNode///
///primitives///
wNode* wNodeCreateEmpty ();

wNode* wNodeCreateCube (Float32 size, wColor4s color);
// __cplusplus

wNode* wNodeCreateSphere (Float32 radius, Int32 polyCount, wColor4s color);
// __cplusplus

wNode* wNodeCreateCylinder (
    UInt32 tesselation,
    Float32 radius,
    Float32 length,
    wColor4s color);
// __cplusplus

wNode* wNodeCreateCone (
    UInt32 tesselation,
    Float32 radius,
    Float32 length,
    wColor4s clorTop,
    wColor4s clorBottom);
// __cplusplus

wNode* wNodeCreatePlane (Float32 size, UInt32 tileCount, wColor4s color);
// __cplusplus

wNode* wNodeCreateFromMesh (wMesh* mesh);

wNode* wNodeCreateFromStaticMesh (wMesh* mesh);

wNode* wNodeCreateFromMeshAsOctree (
    wMesh* vptrMesh,
    Int32 minimalPolysPerNode,
    bool alsoAddIfMeshPointerZero);
// __cplusplus

wNode* wNodeCreateFromBatchingMesh (wMesh* batchMesh);

wNode* wNodeCreateFromBatchingMeshAsOctree (
    wMesh* batchMesh,
    Int32 minimalPolysPerNode,
    bool alsoAddIfMeshPointerZero);
// __cplusplus

///wWater///

wNode* wWaterSurfaceCreate (
    wMesh* mesh,
    Float32 waveHeight,
    Float32 waveSpeed,
    Float32 waveLength,
    wVector3f position,
    wVector3f rotation,
    wVector3f scale);
// __cplusplus

///wRealWater///
wNode* wRealWaterSurfaceCreate (
    const(char)* fileSource,
    wVector2f size,
    wVector2u renderSize);

void wRealWaterSetWindForce (wNode* water, Float32 force);

void wRealWaterSetWindDirection (wNode* water, wVector2f direction);

void wRealWaterSetWaveHeight (wNode* water, Float32 height);

void wRealWaterSetColor (wNode* water, wColor4f color);

void wRealWaterSetColorBlendFactor (wNode* water, Float32 factor);

///wClouds///
wNode* wCloudsCreate (
    wTexture* texture,
    UInt32 lod,
    UInt32 depth,
    UInt32 density);

///wRealClouds///
wNode* wRealCloudsCreate (
    wTexture* txture,
    wVector3f height,
    wVector2f speed,
    Float32 textureScale);

void wRealCloudsSetTextureTranslation (wNode* cloud, wVector2f speed);

wVector2f wRealCloudsGetTextureTranslation (wNode* cloud);

void wRealCloudsSetTextureScale (wNode* cloud, Float32 scale);

Float32 wRealCloudsGetTextureScale (wNode* cloud);

void wRealCloudsSetCloudHeight (wNode* cloud, wVector3f height);

wVector3f wRealCloudsGetCloudHeight (wNode* cloud);

void wRealCloudsSetCloudRadius (wNode* cloud, wVector2f radius);

wVector2f wRealCloudsGetCloudRadius (wNode* cloud);

void wRealCloudsSetColors (
    wNode* cloud,
    wColor4s centerColor,
    wColor4s innerColor,
    wColor4s outerColor);

void wRealCloudsGetColors (
    wNode* cloud,
    wColor4s* centerColor,
    wColor4s* innerColor,
    wColor4s* outerColor);

///wLensFlare///
wNode* wLensFlareCreate (wTexture* texture);

void wLensFlareSetStrength (wNode* flare, Float32 strength);

Float32 wLensFlareGetStrength (wNode* flare);

///wGrass///
wNode* wGrassCreate (
    wNode* terrain,
    wVector2i position,
    UInt32 patchSize,
    Float32 fadeDistance,
    bool crossed,
    Float32 grassScale,
    UInt32 maxDensity,
    wVector2u dataPosition,
    wImage* heightMap,
    wImage* textureMap,
    wImage* grassMap,
    wTexture* grassTexture);

void wGrassSetDensity (wNode* grass, UInt32 density, Float32 distance);

void wGrassSetWind (wNode* grass, Float32 strength, Float32 res);

UInt32 wGrassGetDrawingCount (wNode* grass);

///wTreeGenerator/////
wNode* wTreeGeneratorCreate (const(char)* xmlFilePath);

void wTreeGeneratorDestroy (wNode* generator);

///wTree///
wNode* wTreeCreate (wNode* generator, Int32 seed, wTexture* billboardTexture);

void wTreeSetDistances (wNode* tree, Float32 midRange, Float32 farRange);

wNode* wTreeGetLeafNode (wNode* tree);

void wTreeSetLeafEnabled (wNode* tree, bool value);

bool wTreeIsLeafEnabled (wNode* tree);

wMeshBuffer* wTreeGetMeshBuffer (wNode* tree, UInt32 idx); //0-HIGH meshbuffer,  1- MID meshbuffer

void wTreeSetBillboardVertexColor (wNode* tree, wColor4s color);

wColor4s wTreeGetBillboardVertexColor (wNode* tree);

///wWindGenerator
wNode* wWindGeneratorCreate ();

void wWindGeneratorDestroy (wNode* windGenerator);

void wWindGeneratorSetStrength (wNode* windGenerator, Float32 strength);

Float32 wWindGeneratorGetStrength (wNode* windGenerator);

void wWindGeneratorSetRegularity (wNode* windGenerator, Float32 regularity);

Float32 wWindGeneratorGetRegularity (wNode* windGenerator);

wVector2f wWindGeneratorGetWind (wNode* windGenerator, wVector3f position, UInt32 timeMs);

///wBolt///
wNode* wBoltCreate ();

void wBoltSetProperties (
    wNode* bolt,
    wVector3f start,
    wVector3f end,
    UInt32 updateTime,
    UInt32 height,
    Float32 thickness,
    UInt32 parts,
    UInt32 bolts,
    bool steddyend,
    wColor4s color);
///wBeam///
wNode* wBeamCreate ();

void wBeamSetSize (wNode* beam, Float32 size);

void wBeamSetPosition (wNode* beam, wVector3f start, wVector3f end);

///wParticleSystem///

wNode* wParticleSystemCreate (
    bool defaultemitter,
    wVector3f position,
    wVector3f rotation,
    wVector3f scale);
// __cplusplus

wEmitter* wParticleSystemGetEmitter (wNode* ps);

void wParticleSystemSetEmitter (wNode* ps, wEmitter* em);

void wParticleSystemRemoveAllAffectors (wNode* ps);

void wParticleSystemSetGlobal (wNode* ps, bool value);

void wParticleSystemSetParticleSize (wNode* ps, wVector2f size);

void wParticleSystemClear (wNode* ps);

///wParticleBoxEmitter///
wEmitter* wParticleBoxEmitterCreate (wNode* ps);

void wParticleBoxEmitterSetBox (
    wEmitter* em,
    wVector3f boxMin,
    wVector3f boxMax);
// __cplusplus

void wParticleBoxEmitterGetBox (
    wEmitter* em,
    wVector3f* boxMin,
    wVector3f* boxMax);

///wParticleCylinderEmitter///
wEmitter* wParticleCylinderEmitterCreate (
    wNode* ps,
    wVector3f center,
    Float32 radius,
    wVector3f normal,
    Float32 lenght);

void wParticleCylinderEmitterSetParameters (
    wEmitter* em,
    wParticleCylinderEmitter params);

void wParticleCylinderEmitterGetParameters (
    wEmitter* em,
    wParticleCylinderEmitter* params);

///wParticleMeshEmitter///
wEmitter* wParticleMeshEmitterCreate (wNode* ps, wNode* node);

void wParticleMeshEmitterSetParameters (
    wEmitter* em,
    wParticleMeshEmitter params);

void wParticleMeshEmitterGetParameters (
    wEmitter* em,
    wParticleMeshEmitter* params);

///wParticlePointEmitter///
wEmitter* wParticlePointEmitterCreate (wNode* ps);

///wParticleRingEmitter///
wEmitter* wParticleRingEmitterCreate (
    wNode* ps,
    wVector3f center,
    Float32 radius,
    Float32 ringThickness);

void wParticleRingEmitterSetParameters (
    wEmitter* em,
    wParticleRingEmitter params);

void wParticleRingEmitterGetParameters (
    wEmitter* em,
    wParticleRingEmitter* params);

///wParticleSphereEmitter///
wEmitter* wParticleSphereEmitterCreate (
    wNode* ps,
    wVector3f center,
    Float32 radius);

void wParticleSphereEmitterSetParameters (
    wEmitter* em,
    wParticleSphereEmitter params);

void wParticleSphereEmitterGetParameters (
    wEmitter* em,
    wParticleSphereEmitter* params);

///wParticleEmitter- FOR ALL///
void wParticleEmitterSetParameters (wEmitter* em, wParticleEmitter params);

void wParticleEmitterGetParameters (wEmitter* em, wParticleEmitter* params);

///wParticleAffector -FOR ALL///
void wParticleAffectorSetEnable (wAffector* foa, bool enable);

bool wParticleAffectorIsEnable (wAffector* foa);

///wParticleFadeOutAffector///
wAffector* wParticleFadeOutAffectorCreate (wNode* ps);

void wParticleFadeOutAffectorSetTime (wAffector* paf, UInt32 fadeOutTime);
// __cplusplus

UInt32 wParticleFadeOutAffectorGetTime (wAffector* paf);

void wParticleFadeOutAffectorSetColor (wAffector* paf, wColor4s targetColor);
// __cplusplus

wColor4s wParticleFadeOutAffectorGetColor (wAffector* paf);

///wParticleGravityAffector///
wAffector* wParticleGravityAffectorCreate (wNode* ps);

void wParticleGravityAffectorSetGravity (wAffector* paf, wVector3f gravity);
// __cplusplus

wVector3f wParticleGravityAffectorGetGravity (wAffector* paf);

void wParticleGravityAffectorSetTimeLost (wAffector* paf, UInt32 timeForceLost);
// __cplusplus

UInt32 wParticleGravityAffectorGetTimeLost (wAffector* paf);

///wParticleAttractionAffector///

wAffector* wParticleAttractionAffectorCreate (
    wNode* ps,
    wVector3f point,
    Float32 speed);
// __cplusplus

void wParticleAttractionAffectorSetParameters (
    wAffector* paf,
    wParticleAttractionAffector params);

void wParticleAttractionAffectorGetParameters (
    wAffector* paf,
    wParticleAttractionAffector* params);

///wParticleRotationAffector///
wAffector* wParticleRotationAffectorCreate (wNode* ps);

void wParticleRotationAffectorSetSpeed (wAffector* paf, wVector3f speed);

wVector3f wParticleRotationAffectorGetSpeed (wAffector* paf);

void wParticleRotationAffectorSetPivot (wAffector* paf, wVector3f pivotPoint);

wVector3f wParticleRotationAffectorGetPivot (wAffector* paf);

///wParticleStopAffector///

wAffector* wParticleStopAffectorCreate (wNode* ps, wEmitter* em, UInt32 time);
// __cplusplus

void wParticleStopAffectorSetTime (wAffector* paf, UInt32 time);

UInt32 wParticleStopAffectorGetTime (wAffector* paf);

///wParticleColorMorphAffector///
wAffector* wParticleColorMorphAffectorCreate (wNode* ps);

void wParticleColorAffectorSetParameters (
    wAffector* paf,
    wParticleColorMorphAffector params);

void wParticleColorAffectorGetParameters (
    wAffector* paf,
    wParticleColorMorphAffector* params);

///wParticlePushAffector///
wAffector* wParticlePushAffectorCreate (wNode* ps);

void wParticlePushAffectorSetParameters (
    wAffector* paf,
    wParticlePushAffector params);

void wParticlePushAffectorGetParameters (
    wAffector* paf,
    wParticlePushAffector* params);

///wParticleSplineAffector///
wAffector* wParticleSplineAffectorCreate (wNode* ps);

void wParticleSplineAffectorSetParameters (
    wAffector* paf,
    wParticleSplineAffector params);

void wParticleSplineAffectorGetParameters (
    wAffector* paf,
    wParticleSplineAffector* params);

///wParticleScaleAffector///
wAffector* wParticleScaleAffectorCreate (wNode* ps, wVector2f scaleTo);

///wNode///
void wNodeSetParent (wNode* node, wNode* parent);

wNode* wNodeGetParent (wNode* node);

void wNodeSetReadOnlyMaterials (wNode* node, bool readonly);

bool wNodeIsReadOnlyMaterials (wNode* node);

wNode* wNodeGetFirstChild (wNode* node, UInt32* iterator);

wNode* wNodeGetNextChild (wNode* node, UInt32* iterator);

bool wNodeIsLastChild (wNode* node, UInt32* iterator);

void wNodeSetId (wNode* node, Int32 id);

Int32 wNodeGetId (wNode* node);

void wNodeSetName (wNode* node, const(char)* name);

const(char)* wNodeGetName (wNode* node);

void wNodeSetUserData (wNode* node, void* newData);

void* wNodeGetUserData (wNode* node);

void wNodeSetDebugMode (wNode* node, wDebugMode visible);
// __cplusplus

void wNodeSetDebugDataVisible (wNode* node, bool value);

UInt32 wNodeGetMaterialsCount (wNode* node);

wMaterial* wNodeGetMaterial (wNode* node, UInt32 matIndex);

void wNodeSetPosition (wNode* node, wVector3f position);

wVector3f wNodeGetPosition (wNode* node);

wVector3f wNodeGetAbsolutePosition (wNode* node);

void wNodeSetRotation (wNode* node, wVector3f rotation);

void wNodeSetAbsoluteRotation (wNode* node, wVector3f rotation);

wVector3f wNodeGetRotation (wNode* node);

wVector3f wNodeGetAbsoluteRotation (wNode* node);

void wNodeTurn (wNode* Entity, wVector3f turn);

void wNodeMove (wNode* Entity, wVector3f direction);

void wNodeRotateToNode (wNode* Entity1, wNode* Entity2);

Float32 wNodesGetBetweenDistance (wNode* nodeA, wNode* nodeB);

bool wNodesAreIntersecting (wNode* nodeA, wNode* nodeB);

bool wNodeIsPointInside (wNode* node, wVector3f pos);

void wNodeDrawBoundingBox (wNode* node, wColor4s color);

void wNodeGetBoundingBox (wNode* Node, wVector3f* min, wVector3f* max);

void wNodeGetTransformedBoundingBox (
    wNode* Node,
    wVector3f* min,
    wVector3f* max);

void wNodeSetScale (wNode* node, wVector3f scale);

wVector3f wNodeGetScale (wNode* node);

wNode* wNodeDublicate (wNode* entity);

wNode* wNodeGetJoint (wNode* node, char* node_name);

wNode* wNodeGetJointById (wNode* node, UInt32 Id);

Int32 wNodeGetJointsCount (wNode* node);

void wNodeSetJointSkinningSpace (wNode* bone, wBoneSkinningSpace space);

wBoneSkinningSpace wNodeGetJointSkinningSpace (wNode* bone);

void wNodeAddShadowVolume (
    wNode* node,
    wMesh* mesh,
    bool zfailMethod,
    Float32 infinity,
    bool oldStyle);
// __cplusplus

wNode* wNodeAddShadowVolumeFromMeshBuffer (
    wNode* nodeParent,
    wMeshBuffer* meshbuffer,
    bool zfailMethod,
    Float32 infinity,
    bool oldStyle);
// __cplusplus

void wNodeUpdateShadow (wNode* shadow);

void wNodeSetVisibility (wNode* node, bool visible);

bool wNodeIsVisible (wNode* node);

bool wNodeIsInView (wNode* node);

void wNodeDestroy (wNode* node);

void wNodeSetMesh (wNode* node, wMesh* mesh);

wMesh* wNodeGetMesh (wNode* node);

void wNodeSetRotationPositionChange (
    wNode* node,
    wVector3f angles,
    wVector3f offset,
    wVector3f* forwardStore,
    wVector3f* upStore,
    UInt32 numOffsets,
    wVector3f* offsetStore);

void wNodeSetCullingState (wNode* node, wCullingState state);

wSceneNodeType wNodeGetType (wNode* node);

void wNodeSetDecalsEnabled (wNode* node);

void wNodeSetAnimationRange (wNode* node, wVector2i range);

void wNodePlayMD2Animation (wNode* node, wMd2AnimationType iAnimation);

void wNodeSetAnimationSpeed (wNode* node, Float32 fSpeed);

void wNodeSetAnimationFrame (wNode* node, Float32 fFrame);

Float32 wNodeGetAnimationFrame (wNode* node);

void wNodeSetTransitionTime (wNode* node, Float32 fTime);

void wNodeAnimateJoints (wNode* node);

void wNodeSetJointMode (wNode* node, wJointMode mode);

void wNodeSetAnimationLoopMode (wNode* node, bool value);

void wNodeDestroyAllAnimators (wNode* node);

UInt32 wNodeGetAnimatorsCount (wNode* node);

wAnimator* wNodeGetFirstAnimator (wNode* node);

wAnimator* wNodeGetLastAnimator (wNode* node);

wAnimator* wNodeGetAnimatorByIndex (wNode* node, UInt32 index);

void wNodeOnAnimate (wNode* node, UInt32 timeMs);

void wNodeRender (wNode* node);

void wNodeUpdateAbsolutePosition (wNode* node);

///wMaterial///
void wMaterialSetTexture (
    wMaterial* material,
    UInt32 texIdx,
    wTexture* texture);

wTexture* wMaterialGetTexture (wMaterial* material, UInt32 texIdx);

void wMaterialScaleTexture (
    wMaterial* material,
    UInt32 texIdx,
    wVector2f scale);

void wMaterialScaleTextureFromCenter (
    wMaterial* material,
    UInt32 texIdx,
    wVector2f scale);

void wMaterialTranslateTexture (
    wMaterial* material,
    UInt32 texIdx,
    wVector2f translate);

void wMaterialTranslateTextureTransposed (
    wMaterial* material,
    UInt32 texIdx,
    wVector2f translate);

void wMaterialRotateTexture (wMaterial* material, UInt32 texIdx, Float32 angle);

void wMaterialSetTextureWrapUMode (
    wMaterial* material,
    UInt32 texIdx,
    wTextureClamp value);

wTextureClamp wMaterialGetTextureWrapUMode (wMaterial* material, UInt32 texIdx);

void wMaterialSetTextureWrapVMode (
    wMaterial* material,
    UInt32 texIdx,
    wTextureClamp value);

wTextureClamp wMaterialGetTextureWrapVMode (wMaterial* material, UInt32 texIdx);

void wMaterialSetTextureLodBias (
    wMaterial* material,
    UInt32 texIdx,
    UInt32 lodBias);

UInt32 wMaterialGetTextureLodBias (wMaterial* material, UInt32 texIdx);

void wMaterialSetFlag (wMaterial* material, wMaterialFlags Flag, bool boValue);

bool wMaterialGetFlag (wMaterial* material, wMaterialFlags matFlag);

void wMaterialSetType (wMaterial* material, wMaterialTypes type);

void wMaterialSetShininess (wMaterial* material, Float32 shininess);

Float32 wMaterialGetShininess (wMaterial* material);

void wMaterialSetVertexColoringMode (
    wMaterial* material,
    wColorMaterial colorMaterial);

wColorMaterial wMaterialGetVertexColoringMode (wMaterial* material);

void wMaterialSetSpecularColor (wMaterial* material, wColor4s color);

wColor4s wMaterialGetSpecularColor (wMaterial* material);

void wMaterialSetDiffuseColor (wMaterial* material, wColor4s color);

wColor4s wMaterialGetDiffuseColor (wMaterial* material);

void wMaterialSetAmbientColor (wMaterial* material, wColor4s color);

wColor4s wMaterialGetAmbientColor (wMaterial* material);

void wMaterialSetEmissiveColor (wMaterial* material, wColor4s color);

wColor4s wMaterialGetEmissiveColor (wMaterial* material);

void wMaterialSetTypeParameter (wMaterial* material, Float32 param1);

Float32 wMaterialGetTypeParameter (wMaterial* material);

void wMaterialSetTypeParameter2 (wMaterial* material, Float32 param2);

Float32 wMaterialGetTypeParameter2 (wMaterial* material);

void wMaterialSetBlendingMode (
    wMaterial* material,
    const wBlendFactor blendSrc,
    const wBlendFactor blendDest);

//wMaterialGetBlendingMode = wMaterialGetTypeParameter

void wMaterialSetLineThickness (wMaterial* material, Float32 lineThickness);

Float32 wMaterialGetLineThickness (wMaterial* material);

void wMaterialSetColorMask (wMaterial* material, wColorPlane value);

wColorPlane wMaterialGetColorMask (wMaterial* material);

void wMaterialSetAntiAliasingMode (wMaterial* material, wAntiAliasingMode mode);

wAntiAliasingMode wMaterialGetAntiAliasingMode (wMaterial* material);

///wShader///
bool wShaderCreateNamedVertexConstant (
    wShader* shader,
    const(char)* name,
    Int32 preset,
    const(float)* floats,
    Int32 count);

bool wShaderCreateNamedPixelConstant (
    wShader* shader,
    const(char)* name,
    int preset,
    const(float)* floats,
    int count);

bool wShaderCreateAddressedVertexConstant (
    wShader* shader,
    Int32 address,
    int preset,
    const(float)* floats,
    int count);

bool wShaderCreateAddressedPixelConstant (
    wShader* shader,
    int address,
    int preset,
    const(float)* floats,
    int count);

wShader* wShaderAddHighLevelMaterial (
    const(char)* vertexShaderProgram,
    const(char)* vertexShaderEntryPointName,
    wVertexShaderVersion wVersion,
    const(char)* pixelShaderProgram,
    const(char)* pixelShaderEntryPointName,
    wPixelShaderVersion pVersion,
    wMaterialTypes materialType,
    Int32 userData);
// __cplusplus

wShader* wShaderAddHighLevelMaterialFromFiles (
    const(char)* vertexShaderProgramFileName,
    const(char)* vertexShaderEntryPointName,
    wVertexShaderVersion wVersion,
    const(char)* pixelShaderProgramFileName,
    const(char)* pixelShaderEntryPointName,
    wPixelShaderVersion pVersion,
    wMaterialTypes materialType,
    Int32 userData);
// __cplusplus

wShader* wShaderAddMaterial (
    const(char)* vertexShaderProgram,
    const(char)* pixelShaderProgram,
    wMaterialTypes materialType,
    Int32 userData);
// __cplusplus

wShader* wShaderAddMaterialFromFiles (
    const(char)* vertexShaderProgramFileName,
    const(char)* pixelShaderProgramFileName,
    wMaterialTypes materialType,
    Int32 userData);
// __cplusplus

///with geometry shader

wShader* wShaderAddHighLevelMaterialEx (
    const(char)* vertexShaderProgram,
    const(char)* vertexShaderEntryPointName,
    wVertexShaderVersion wVersion,
    const(char)* pixelShaderProgram,
    const(char)* pixelShaderEntryPointName,
    wPixelShaderVersion pVersion,
    const(char)* geometryShaderProgram,
    const(char)* geometryShaderEntryPointName,
    wGeometryShaderVersion gVersion,
    wPrimitiveType inType,
    wPrimitiveType outType,
    UInt32 verticesOut,
    wMaterialTypes materialType,
    Int32 userData);
// __cplusplus

///with geometry shader

wShader* wShaderAddHighLevelMaterialFromFilesEx (
    const(char)* vertexShaderProgramFileName,
    const(char)* vertexShaderEntryPointName,
    wVertexShaderVersion wVersion,
    const(char)* pixelShaderProgramFileName,
    const(char)* pixelShaderEntryPointName,
    wPixelShaderVersion pVersion,
    const(char)* geometryShaderProgram,
    const(char)* geometryShaderEntryPointName,
    wGeometryShaderVersion gVersion,
    wPrimitiveType inType,
    wPrimitiveType outType,
    UInt32 verticesOut,
    wMaterialTypes materialType,
    Int32 userData);
// __cplusplus

///wMesh///

wMesh* wMeshLoad (char* cptrFile, bool ToTangents);
// __cplusplus

wMesh* wMeshCreate (char* cptrMeshName);

void wMeshAddMeshBuffer (wMesh* mesh, wMeshBuffer* meshbuffer);

wMesh* wMeshCreateSphere (const(char)* name, Float32 radius, Int32 polyCount);

wMesh* wMeshCreateCube ();

Int32 wMeshSave (wMesh* mesh, wMeshFileFormat type, const(char)* filename); //return 0/1/2/3  3- успешно

void wMeshDestroy (wMesh* mesh);

bool wMeshSetName (wMesh* mesh, const(char)* name);

const(char)* wMeshGetName (wMesh* mesh);

wAnimatedMeshType wMeshGetType (wMesh* mesh);

void wMeshFlipSurface (wMesh* mesh);

void wMeshMakePlanarTextureMapping (wMesh* mesh, Float32 resolution);
// __cplusplus

void wMeshMakePlanarTextureMappingAdvanced (
    wMesh* mesh,
    Float32 resolutionH,
    Float32 resolutionV,
    UInt8 axis,
    wVector3f offset);

wMesh* wMeshCreateStaticWithTangents (wMesh* aMesh);

void wMeshRecalculateNormals (wMesh* mesh, bool smooth, bool angleWeighted);
// __cplusplus

void wMeshRecalculateTangents (
    wMesh* mesh,
    bool recalculateNormals,
    bool smooth,
    bool angleWeighted);
// __cplusplus

wMesh* wMeshCreateHillPlane (
    const(char)* meshname,
    wVector2f tilesSize,
    wVector2i tilesCount,
    wMaterial* material,
    Float32 hillHeight,
    wVector2f countHills,
    wVector2f texRepeatCount);
// __cplusplus

wMesh* wMeshCreateArrow (
    const(char)* name,
    wColor4s cylinderColor,
    wColor4s coneColor,
    UInt32 tesselationCylinder,
    UInt32 tesselationCone,
    Float32 height,
    Float32 heightCylinder,
    Float32 widthCylinder,
    Float32 widthCone);

wMesh* wMeshCreateBatching ();

void wMeshAddToBatching (
    wMesh* meshBatch,
    wMesh* mesh,
    wVector3f position,
    wVector3f rotation,
    wVector3f scale);
// __cplusplus

void wMeshUpdateBatching (wMesh* meshBatch);

void wMeshFinalizeBatching (wMesh* meshBatch);

void wMeshClearBatching (wMesh* meshBatch);

void wMeshDestroyBatching (wMesh* meshBatch);

void wMeshEnableHardwareAcceleration (wMesh* mesh, UInt32 iFrame);
// __cplusplus

UInt32 wMeshGetFramesCount (wMesh* mesh);

UInt32 wMeshGetIndicesCount (wMesh* mesh, UInt32 iFrame, UInt32 iMeshBuffer);
// __cplusplus

UInt16* wMeshGetIndices (wMesh* mesh, UInt32 iFrame, UInt32 iMeshBuffer);
// __cplusplus

void wMeshSetIndices (
    wMesh* mesh,
    UInt32 iFrame,
    UInt16* indicies,
    UInt32 iMeshBuffer);
// __cplusplus

UInt32 wMeshGetVerticesCount (wMesh* mesh, UInt32 iFrame, UInt32 iMeshBuffer);
// __cplusplus

void wMeshGetVertices (
    wMesh* mesh,
    UInt32 iFrame,
    wVert* verts,
    UInt32 iMeshBuffer);
// __cplusplus

UInt32* wMeshGetVerticesMemory (wMesh* mesh, UInt32 iFrame, UInt32 iMeshBuffer);
// __cplusplus

void wMeshSetVertices (
    wMesh* mesh,
    UInt32 iFrame,
    wVert* verts,
    UInt32 iMeshBuffer);
// __cplusplus

void wMeshSetScale (
    wMesh* mesh,
    Float32 scale,
    UInt32 iFrame,
    UInt32 iMeshBuffer,
    wMesh* sourceMesh);
// __cplusplus

void wMeshSetRotation (wMesh* mesh, wVector3f rot);

void wMeshSetVerticesColors (
    wMesh* mesh,
    UInt32 iFrame,
    wColor4s* verticesColor,
    UInt32 groupCount,
    UInt32* startPos,
    UInt32* endPos,
    UInt32 iMeshBuffer);
// __cplusplus

void wMeshSetVerticesAlpha (wMesh* mesh, UInt32 iFrame, UInt8 value);

void wMeshSetVerticesCoords (
    wMesh* mesh,
    UInt32 iFrame,
    wVector3f* vertexCoord,
    UInt32 groupCount,
    UInt32* startPos,
    UInt32* endPos,
    UInt32 iMeshBuffer);
// __cplusplus

void wMeshSetVerticesSingleColor (
    wMesh* mesh,
    UInt32 iFrame,
    wColor4s verticesColor,
    UInt32 groupCount,
    UInt32* startPos,
    UInt32* endPos,
    UInt32 iMeshBuffer);
// __cplusplus

void wMeshGetBoundingBox (wMesh* mesh, wVector3f* min, wVector3f* max);

wMesh* wMeshDuplicate (wMesh* src);

void wMeshFit (wMesh* src, wVector3f pivot, wVector3f* delta);

bool wMeshIsEmpty (wMesh* mesh);

UInt32 wMeshGetBuffersCount (wMesh* mesh, UInt32 iFrame);
// __cplusplus

wMeshBuffer* wMeshGetBuffer (wMesh* mesh, UInt32 iFrame, UInt32 index);

///wMeshBuffer///
wMeshBuffer* wMeshBufferCreate (
    UInt32 iVertexCount,
    wVert* vVertices,
    UInt32 iIndicesCount,
    UInt16* usIndices);

/*wMeshBuffer* wMeshBufferCreateFromMeshJoint(wMesh* mesh,
                                            Int32 jointIndex);*/

void wMeshBufferDestroy (wMeshBuffer* buf);

void wMeshBufferAddToBatching (
    wMesh* meshBatch,
    wMeshBuffer* buffer,
    wVector3f position,
    wVector3f rotation,
    wVector3f scale);
// __cplusplus

wMaterial* wMeshBufferGetMaterial (wMeshBuffer* buf);

///wBsp///
///Get BSP Entity List///
UInt32* wBspGetEntityList (wMesh* mesh);

///Get BSP Entity List size///
Int32 wBspGetEntityListSize (UInt32* entityList);

///Get First (vec.x) and Last (vec.y) BSP Entity Index///
wVector2i wBspGetEntityIndexByName (void* entityList, char* EntityName);
///Name BSP Entity From Index
const(char)* wBspGetEntityNameByIndex (UInt32* entityList, UInt32 number);
///Mesh from BSP Brush///
wMesh* wBspGetEntityMeshFromBrush (
    wMesh* bspMesh,
    UInt32* entityList,
    Int32 index);
///BSP VarGroup///
UInt32* wBspGetVarGroupByIndex (UInt32* entityList, Int32 index);

UInt32 wBspGetVarGroupSize (UInt32* entityList, Int32 index);

wVector3f wBspGetVarGroupValueAsVec (
    UInt32* group,
    char* strName,
    UInt32 parsePos);

Float32 wBspGetVarGroupValueAsFloat (
    UInt32* group,
    char* strName,
    UInt32 parsePos);

const(char)* wBspGetVarGroupValueAsString (UInt32* group, char* strName);
/*
UInt32 wBspGetVarGroupVariableSize(UInt32* group);


UInt32* wBspGetVariableFromVarGroup(UInt32* group,
                                          Int32 index);

const char* wBspGetVariableName(UInt32* variable);

const char* wBspGetVariableContent(UInt32* variable);

wVector3f wBspGetVariableValueAsVec(UInt32* variable,
                                    UInt32 parsePos);

Float32 wBspGetVariableValueAsFloat(UInt32* variable,
                                  UInt32 parsePos);
*/

wNode* wBspCreateFromMesh (
    wMesh* mesh,
    bool isTangent,
    bool isOctree,
    char* fileEntity,
    bool isLoadShaders,
    UInt32 PolysPerNode);
// __cplusplus

///Occlusion Query
void wOcclusionQueryAddNode (wNode* node);

void wOcclusionQueryAddMesh (wNode* node, wMesh* mesh);

void wOcclusionQueryUpdate (wNode* node, bool block);

void wOcclusionQueryRun (wNode* node, bool visible);

void wOcclusionQueryUpdateAll (bool block);

void wOcclusionQueryRunAll (bool visible);

void wOcclusionQueryRemoveNode (wNode* node);

void wOcclusionQueryRemoveAll ();

UInt32 wOcclusionQueryGetResult (wNode* node);

///wSphericalTerrain///
wNode* wSphericalTerrainCreate (
    char* cptrFile0,
    char* cptrFile1,
    char* cptrFile2,
    char* cptrFile3,
    char* cptrFile4,
    char* cptrFile5,
    wVector3f position,
    wVector3f rotation,
    wVector3f scale,
    wColor4s color,
    Int32 smootFactor,
    bool spherical,
    Int32 maxLOD,
    wTerrainPatchSize patchSize);

void wSphericalTerrainSetTextures (
    wNode* terrain,
    wTexture* textureTop,
    wTexture* textureFront,
    wTexture* textureBack,
    wTexture* textureLeft,
    wTexture* textureRight,
    wTexture* textureBottom,
    UInt32 materialIndex);

void wSphericalTerrainLoadVertexColor (
    wNode* terrain,
    wImage* imageTop,
    wImage* imageFront,
    wImage* imageBack,
    wImage* imageLeft,
    wImage* imageRight,
    wImage* imageBottom);

wVector3f wSphericalTerrainGetSurfacePosition (
    wNode* terrain,
    Int32 face,
    wVector2f logicalPos);

wVector3f wSphericalTerrainGetSurfaceAngle (
    wNode* terrain,
    Int32 face,
    wVector2f logicalPos);

wVector2f wSphericalTerrainGetSurfaceLogicalPosition (
    wNode* terrain,
    wVector3f position,
    int* face);

///wTerrain///

wNode* wTerrainCreate (
    char* cptrFile,
    wVector3f position,
    wVector3f rotation,
    wVector3f scale,
    wColor4s color,
    Int32 smoothing,
    Int32 maxLOD,
    wTerrainPatchSize patchSize);

void wTerrainScaleDetailTexture (wNode* terrain, wVector2f scale);

Float32 wTerrainGetHeight (wNode* terrain, wVector2f positionXZ);

///wTiledTerrain///

wNode* wTiledTerrainCreate (
    wImage* image,
    Int32 tileSize,
    wVector2i dataSize,
    wVector3f position,
    wVector3f rotation,
    wVector3f scale,
    wColor4s color,
    Int32 smoothing,
    Int32 maxLOD,
    wTerrainPatchSize patchSize);
// __cplusplus

void wTiledTerrainAddTile (
    wNode* terrain,
    wNode* neighbour,
    wTiledTerrainEdge edge);

void wTiledTerrainSetTileStructure (
    wNode* terrain,
    wImage* image,
    wVector2i data);

void wTiledTerrainSetTileColor (wNode* terrain, wImage* image, wVector2i data);

///wSound///
wSound* wSoundLoad (const(char)* filePath, bool stream);

wSound* wSoundLoadFromMemory (
    const(char)* name,
    const(char)* data,
    Int32 length,
    const(char)* extension);

wSound* wSoundLoadFromRaw (
    const(char)* name,
    const(char)* data,
    Int32 length,
    UInt32 frequency,
    wAudioFormats format);

bool wSoundIsPlaying (wSound* sound);

bool wSoundIsPaused (wSound* sound);

bool wSoundIsStopped (wSound* sound);

void wSoundSetVelocity (wSound* sound, wVector3f velocity);

wVector3f wSoundGetVelocity (wSound* sound);

void wSoundSetDirection (wSound* sound, wVector3f direction);

wVector3f wSoundGetDirection (wSound* sound);

void wSoundSetVolume (wSound* sound, Float32 value);

Float32 wSoundGetVolume (wSound* sound);

void wSoundSetMaxVolume (wSound* sound, Float32 value);

Float32 wSoundGetMaxVolume (wSound* sound);

void wSoundSetMinVolume (wSound* sound, Float32 value);

Float32 wSoundGetMinVolume (wSound* sound);

void wSoundSetPitch (wSound* sound, Float32 value);

Float32 wSoundGetPitch (wSound* sound);

void wSoundSetRollOffFactor (wSound* sound, Float32 value);

Float32 wSoundGetRollOffFactor (wSound* sound);

void wSoundSetStrength (wSound* sound, Float32 value);

Float32 wSoundGetStrength (wSound* sound);

void wSoundSetMinDistance (wSound* sound, Float32 value);

Float32 wSoundGetMinDistance (wSound* sound);

void wSoundSetMaxDistance (wSound* sound, Float32 Value);

Float32 wSoundGetMaxDistance (wSound* sound);

void wSoundSetInnerConeAngle (wSound* sound, Float32 Value);

Float32 wSoundGetInnerConeAngle (wSound* sound);

void wSoundSetOuterConeAngle (wSound* sound, Float32 Value);

Float32 wSoundGetOuterConeAngle (wSound* sound);

void wSoundSetOuterConeVolume (wSound* sound, Float32 Value);

Float32 wSoundGetOuterConeVolume (wSound* sound);

void wSoundSetDopplerStrength (wSound* sound, Float32 Value);

Float32 wSoundGetDopplerStrength (wSound* sound);

void wSoundSetDopplerVelocity (wSound* sound, wVector3f velocity);

wVector3f wSoundGetDopplerVelocity (wSound* sound);

Float32 wSoundCalculateGain (wSound* sound);

void wSoundSetRelative (wSound* sound, bool value);

bool wSoundIsRelative (wSound* sound);

bool wSoundPlay (wSound* sound, bool loop);

void wSoundStop (wSound* sound);

void wSoundPause (wSound* sound);

void wSoundSetLoopMode (wSound* sound, bool value);

bool wSoundIsLooping (wSound* sound);

bool wSoundIsValid (wSound* sound);

bool wSoundSeek (wSound* sound, Float32 seconds, bool relative);
// __cplusplus

void wSoundUpdate (wSound* sound);

Float32 wSoundGetTotalAudioTime (wSound* sound);

Int32 wSoundGetTotalAudioSize (wSound* sound);

Int32 wSoundGetCompressedAudioSize (wSound* sound);

Float32 wSoundGetCurrentAudioTime (wSound* sound);

Int32 wSoundGetCurrentAudioPosition (wSound* sound);

Int32 wSoundGetCurrentCompressedAudioPosition (wSound* sound);

UInt32 wSoundGetNumEffectSlotsAvailable (wSound* sound);

///sound effects///
bool wSoundAddEffect (wSound* sound, UInt32 slot, wSoundEffect* effect);

void wSoundRemoveEffect (wSound* sound, UInt32 slot);

wSoundEffect* wSoundCreateEffect ();

bool wSoundIsEffectValid (wSoundEffect* effect);

bool wSoundIsEffectSupported (wSoundEffectType type);

UInt32 wSoundGetMaxEffectsSupported ();

void wSoundSetEffectType (wSoundEffect* effect, wSoundEffectType type);

wSoundEffectType wSoundGetEffectType (wSoundEffect* effect);

void wSoundSetEffectAutowahParameters (
    wSoundEffect* effect,
    wAutowahParameters param);

void wSoundSetEffectChorusParameters (
    wSoundEffect* effect,
    wChorusParameters param);

void wSoundSetEffectCompressorParameters (
    wSoundEffect* effect,
    wCompressorParameters param);

void wSoundSetEffectDistortionParameters (
    wSoundEffect* effect,
    wDistortionParameters param);

void wSoundSetEffectEaxReverbParameters (
    wSoundEffect* effect,
    wEaxReverbParameters param);

void wSoundSetEffectEchoParameters (
    wSoundEffect* effect,
    wEchoParameters param);

void wSoundSetEffectEqualizerParameters (
    wSoundEffect* effect,
    wEqualizerParameters param);

void wSoundSetEffectFlangerParameters (
    wSoundEffect* effect,
    wFlangerParameters param);

void wSoundSetEffectFrequencyShiftParameters (
    wSoundEffect* effect,
    wFrequencyShiftParameters param);

void wSoundSetEffectPitchShifterParameters (
    wSoundEffect* effect,
    wPitchShifterParameters param);

void wSoundSetEffectReverbParameters (
    wSoundEffect* effect,
    wReverbParameters param);

void wSoundSetEffectRingModulatorParameters (
    wSoundEffect* effect,
    wRingModulatorParameters param);

void wSoundSetEffectVocalMorpherParameters (
    wSoundEffect* effect,
    wVocalMorpherParameters param);

///Sound filters///
wSoundFilter* wSoundCreateFilter ();

bool wSoundIsFilterValid (wSoundFilter* filter);

bool wSoundAddFilter (wSound* sound, wSoundFilter* filter);

void wSoundRemoveFilter (wSound* sound);

bool wSoundIsFilterSupported (wSoundFilterType type);

void wSoundSetFilterType (wSoundFilter* filter, wSoundFilterType type);

wSoundFilterType wSoundGetFilterType (wSoundFilter* filter);

void wSoundSetFilterVolume (wSoundFilter* filter, Float32 volume);

Float32 wSoundGetFilterVolume (wSoundFilter* filter);

void wSoundSetFilterHighFrequencyVolume (
    wSoundFilter* filter,
    Float32 volumeHF);

Float32 wSoundGetFilterHighFrequencyVolume (wSoundFilter* filter);

void wSoundSetFilterLowFrequencyVolume (wSoundFilter* filter, Float32 volumeLF);

Float32 wSoundGetFilterLowFrequencyVolume (wSoundFilter* filter);

///wVideo///
wVideo* wVideoLoad (char* fileName);

void wVideoPlay (wVideo* player);

bool wVideoIsPlaying (wVideo* player);

void wVideoRewind (wVideo* player);

void wVideoSetLoopMode (wVideo* player, bool looping);

bool wVideoIsLooping (wVideo* player);

wGuiObject* wVideoCreateTargetImage (wVideo* player, wVector2i position);

wTexture* wVideoGetTargetTexture (wVideo* player);

wSound* wVideoGetSoundNode (wVideo* player);

void wVideoUpdate (wVideo* player, UInt32 timeMs);

void wVideoPause (wVideo* player);

bool wVideoIsPaused (wVideo* player);

bool wVideoIsAtEnd (wVideo* player);

bool wVideoIsEmpty (wVideo* player);

Int64 wVideoGetFramePosition (wVideo* player);

UInt32 wVideoGetTimePosition (wVideo* player);

wVector2i wVideoGetFrameSize (wVideo* player);

Int32 wVideoGetQuality (wVideo* player);

void wVideoDestroy (wVideo* player);

///wDecal///
wNode* wDecalCreate (
    wTexture* texture,
    wVector3f startRay,
    wVector3f endRay,
    Float32 dimension,
    Float32 textureRotation,
    Float32 lifeTime,
    Float32 visibleDistance);

Float32 wDecalGetLifeTime (wNode* node);

void wDecalSetLifeTime (wNode* node, Float32 lifeTime);

Float32 wDecalGetMaxVisibleDistance (wNode* node);

void wDecalSetMaxVisibleDistance (wNode* node, Float32 distance);

void wDecalSetFadeOutParams (wNode* node, const bool isfadeOut, Float32 time);

wMaterial* wDecalGetMaterial (wNode* decal);

void wDecalsClear (); ///destroy all + disable new

void wDecalsDestroyAll ();

void wDecalsCombineAll ();

Int32 wDecalsGetCount ();

///wNetPacket///

wPacket* wNetPacketCreate (
    UInt64 id,
    bool inOrder,
    bool reliable,
    UInt64 priority);
// __cplusplus

void wNetPacketWriteUInt (wPacket* msg, UInt32 value);

void wNetPacketWriteInt (wPacket* msg, Int32 value);

void wNetPacketWriteFloat (wPacket* msg, Float32 value);

void wNetPacketWriteString (wPacket* msg, const(char)* newString);

UInt32 wNetPacketReadUint (Int32 numPacket);

Int32 wNetPacketReadInt (Int32 numPacket);

Float32 wNetPacketReadFloat (Int32 numPacket);

const(char)* wNetPacketReadString (Int32 numPacket);

const(char)* wNetPacketReadMessage (Int32 numPacket);

UInt64 wNetPacketGetId (Int32 numPacket);

const(char)* wNetPacketGetClientIp (Int32 numPacket);

void* wNetPacketGetClientPtr (Int32 numPacket);

UInt16 wNetPacketGetClientPort (Int32 numPacket);

///wNetManager///
void wNetManagerSetVerbose (bool value);

void wNetManagerSetMessageId (UInt64 newId);

UInt64 wNetManagerGetMessageId ();

void wNetManagerDestroyAllPackets ();

Int32 wNetManagerGetPacketsCount ();

///wNetServer///

bool wNetServerCreate (UInt16 port, Int32 mode, Int32 maxClientsCount);
// __cplusplus

void wNetServerUpdate (
    Int32 sleepMs,
    Int32 countIteration,
    Int32 maxMSecsToWait);
// __cplusplus

void wNetServerClear ();

void wNetServerSendPacket (void* destPtr, wPacket* msg);

void wNetServerBroadcastMessage (const(char)* text);

void wNetServerAcceptNewConnections (bool value);

void wNetServerStop (Int32 msTime);
// __cplusplus

Int32 wNetServerGetClientsCount ();

void wNetServerKickClient (void* clientPtr);

void wNetServerUnKickClient (void* clientPtr);

void wNetServerClearBannedList ();

///wNetClient///

bool wNetClientCreate (
    const(char)* address,
    UInt16 port,
    Int32 mode,
    Int32 maxMSecsToWait);
// __cplusplus

void wNetClientUpdate (
    Int32 maxMessagesToProcess,
    Int32 countIteration,
    Int32 maxMSecsToWait);
// __cplusplus

void wNetClientDisconnect (Int32 maxMSecsToWait);
// __cplusplus

void wNetClientStop (Int32 maxMSecsToWait);
// __cplusplus

bool wNetClientIsConnected ();

void wNetClientSendMessage (const(char)* text);

void wNetClientSendPacket (wPacket* msg);

///wPhys///
bool wPhysStart ();

void wPhysUpdate (Float32 timeStep);

void wPhysStop ();

void wPhysSetGravity (wVector3f gravity);

void wPhysSetWorldSize (wVector3f size);

void wPhysSetSolverModel (wPhysSolverModel model);

void wPhysSetFrictionModel (wPhysFrictionModel model);

void wPhysDestroyAllBodies ();

void wPhysDestroyAllJoints ();

Int32 wPhysGetBodiesCount ();

Int32 wPhysGetJointsCount ();

wNode* wPhysGetBodyPicked (wVector2i position, bool mouseLeftKey);

wNode* wPhysGetBodyFromRay (wVector3f start, wVector3f end);

wNode* wPhysGetBodyFromScreenCoords (wVector2i position);

wNode* wPhysGetBodyByName (const(char)* name);

wNode* wPhysGetBodyById (Int32 Id);

wNode* wPhysGetBodyByIndex (Int32 idx);

wNode* wPhysGetJointByName (const(char)* name);

wNode* wPhysGetJointById (Int32 Id);

wNode* wPhysGetJointByIndex (Int32 idx);

///wPhysBody///
wNode* wPhysBodyCreateNull ();

wNode* wPhysBodyCreateCube (wVector3f size, Float32 Mass);

wNode* wPhysBodyCreateSphere (wVector3f radius, Float32 Mass);

wNode* wPhysBodyCreateCone (
    Float32 radius,
    Float32 height,
    Float32 mass,
    bool Offset);

wNode* wPhysBodyCreateCylinder (
    Float32 radius,
    Float32 height,
    Float32 mass,
    bool Offset);

wNode* wPhysBodyCreateCapsule (
    Float32 radius,
    Float32 height,
    Float32 mass,
    bool Offset);
// __cplusplus

wNode* wPhysBodyCreateHull (wNode* mesh, Float32 mass);

wNode* wPhysBodyCreateTree (wNode* mesh);

wNode* wPhysBodyCreateTreeBsp (wMesh* mesh, wNode* node);

wNode* wPhysBodyCreateTerrain (wNode* mesh, Int32 LOD);

wNode* wPhysBodyCreateHeightField (wNode* mesh);

wNode* wPhysBodyCreateWaterSurface (
    wVector3f size,
    Float32 FluidDensity,
    Float32 LinearViscosity,
    Float32 AngulaViscosity);

wNode* wPhysBodyCreateCompound (wNode** nodes, Int32 CountNodes, Float32 mass);

///=> такой функции НЕ ТРЕБУЕТСЯ, так как
///так как здесь работает wNodeDestroy()
///void wPhysBodyDestroy(void* body);

void wPhysBodySetName (wNode* body_, const(char)* name);

const(char)* wPhysBodyGetName (wNode* body_);

void wPhysBodySetFreeze (wNode* body_, bool freeze);

bool wPhysBodyIsFreeze (wNode* body_);

void wPhysBodySetMaterial (wNode* body_, Int32 MatId);

Int32 wPhysBodyGetMaterial (wNode* body_);

void wPhysBodySetGravity (wNode* body_, wVector3f gravity);

wVector3f wPhysBodyGetGravity (wNode* body_);

void wPhysBodySetMass (wNode* body_, Float32 NewMass);

Float32 wPhysBodyGetMass (wNode* body_);

void wPhysBodySetCenterOfMass (wNode* body_, wVector3f center);

wVector3f wPhysBodyGetCenterOfMass (wNode* body_);

void wPhysBodySetMomentOfInertia (wNode* body_, wVector3f value);

wVector3f wPhysBodyGetMomentOfInertia (wNode* body_);

void wPhysBodySetAutoSleep (wNode* body_, bool value);

bool wPhysBodyIsAutoSleep (wNode* body_);

void wPhysBodySetLinearVelocity (wNode* body_, wVector3f velocity);

wVector3f wPhysBodyGetLinearVelocity (wNode* body_);

void wPhysBodySetAngularVelocity (wNode* body_, wVector3f velocity);

wVector3f wPhysBodyGetAngularVelocity (wNode* body_);

void wPhysBodySetLinearDamping (wNode* body_, Float32 linearDamp);

Float32 wPhysBodyGetLinearDamping (wNode* body_);

void wPhysBodySetAngularDamping (wNode* body_, wVector3f damping);

wVector3f wPhysBodyGetAngularDamping (wNode* body_);

void wPhysBodyAddImpulse (wNode* body_, wVector3f velosity, wVector3f position);

void wPhysBodyAddForce (wNode* body_, wVector3f force);

void wPhysBodyAddTorque (wNode* body_, wVector3f torque);

bool wPhysBodiesIsCollide (wNode* body1, wNode* body2);

wVector3f wPhysBodiesGetCollisionPoint (wNode* body1, wNode* body2);

wVector3f wPhysBodiesGetCollisionNormal (wNode* body1, wNode* body2);

void wPhysBodyDraw (wNode* body_);

///wPhysJoint///
wNode* wPhysJointCreateBall (
    wVector3f position,
    wVector3f pinDir,
    wNode* body1,
    wNode* body2);

wNode* wPhysJointCreateHinge (
    wVector3f position,
    wVector3f pinDir,
    wNode* body1,
    wNode* body2);

wNode* wPhysJointCreateSlider (
    wVector3f position,
    wVector3f pinDir,
    wNode* body1,
    wNode* body2);

wNode* wPhysJointCreateCorkScrew (
    wVector3f position,
    wVector3f pinDir,
    wNode* body1,
    wNode* body2);

wNode* wPhysJointCreateUpVector (wVector3f position, wNode* body_);

void wPhysJointSetName (wNode* joint, const(char)* name);

const(char)* wPhysJointGetName (wNode* joint);

void wPhysJointSetCollisionState (wNode* joint, bool isCollision);

bool wPhysJointIsCollision (wNode* Joint);

void wPhysJointSetBallLimits (
    wNode* joint,
    Float32 MaxConeAngle,
    wVector2f twistAngles);

void wPhysJointSetHingeLimits (wNode* joint, wVector2f anglesLimits);

void wPhysJointSetSliderLimits (wNode* joint, wVector2f anglesLimits);

void wPhysJointSetCorkScrewLinearLimits (wNode* joint, wVector2f distLimits);

void wPhysJointSetCorkScrewAngularLimits (wNode* joint, wVector2f distLimits);

///wPhysPlayerController///
wNode* wPhysPlayerControllerCreate (
    wVector3f position,
    wNode* body_,
    Float32 maxStairStepFactor,
    Float32 cushion);

void wPhysPlayerControllerSetVelocity (
    wNode* joint,
    Float32 forwardSpeed,
    Float32 sideSpeed,
    Float32 heading);

///wPhysVehicle
wNode* wPhysVehicleCreate (
    Int32 tiresCount,
    wPhysVehicleType rayCastType,
    wNode* CarBody);

Int32 wPhysVehicleAddTire (
    wNode* Car,
    wNode* UserData,
    wPhysVehicleTireType tireType,
    wVector3f position,
    Float32 Mass,
    Float32 Radius,
    Float32 Width,
    Float32 SLenght,
    Float32 SConst,
    Float32 SDamper);

Float32 wPhysVehicleGetSpeed (wNode* Car);

Int32 wPhysVehicleGetTiresCount (wNode* Car);

void wPhysVehicleSetBrake (wNode* Car, bool value);

bool wPhysVehicleIsBrake (wNode* Car);

bool wPhysVehicleIsAllTiresCollided (wNode* Car);

void wPhysVehicleSetSteering (wNode* Car, Float32 angle); ///-1.......+1

Float32 wPhysVehicleGetSteering (wNode* Car); /// return value: -1......+1

void wPhysVehicleSetTireMaxSteerAngle (
    wNode* Car,
    Int32 tireIndex,
    Float32 angleDeg);

Float32 wPhysVehicleGetTireMaxSteerAngle (wNode* Car, Int32 tireIndex);

wNode* wPhysVehicleGetBody (wNode* Car);

void wPhysVehicleSetMotorValue (wNode* Car, Float32 value);

Float32 wPhysVehicleGetMotorValue (wNode* Car);

///Vehicle Tires
wVector3f wPhysVehicleGetTireLocalPosition (wNode* Car, Int32 tireIndex);

Float32 wPhysVehicleGetTireUpDownPosition (wNode* Car, Int32 tireIndex);

/*bool wPhysVehicleIsTireOnAir(wNode* Car,
                             Int32 tireIndex);*/

Float32 wPhysVehicleGetTireAngularVelocity (wNode* Car, Int32 tireIndex);

Float32 wPhysVehicleGetTireSpeed (wNode* Car, Int32 tireIndex);

wVector3f wPhysVehicleGetTireContactPoint (wNode* Car, Int32 tireIndex);

wVector3f wPhysVehicleGetTireContactNormal (wNode* Car, Int32 tireIndex);

bool wPhysVehicleIsTireBrake (wNode* Car, Int32 tireIndex);

wPhysVehicleTireType wPhysVehicleGetTireType (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireType (
    wNode* Car,
    Int32 tireIndex,
    wPhysVehicleTireType tireType);

void wPhysVehicleSetTireBrakeForce (wNode* Car, Int32 tireIndex, Float32 value);

Float32 wPhysVehicleGetTireBrakeForce (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireBrakeLateralFriction (
    wNode* Car,
    Int32 tireIndex,
    Float32 value);

Float32 wPhysVehicleGetTireBrakeLateralFriction (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireBrakeLongitudinalFriction (
    wNode* Car,
    Int32 tireIndex,
    Float32 value);

Float32 wPhysVehicleGetTireBrakeLongitudinalFriction (
    wNode* Car,
    Int32 tireIndex);

void wPhysVehicleSetTireLateralFriction (
    wNode* Car,
    Int32 tireIndex,
    Float32 value);

Float32 wPhysVehicleGetTireLateralFriction (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireLongitudinalFriction (
    wNode* Car,
    Int32 tireIndex,
    Float32 value);

Float32 wPhysVehicleGetTireLongitudinalFriction (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireMass (wNode* Car, Int32 tireIndex, Float32 mass);

Float32 wPhysVehicleGetTireMass (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireRadius (wNode* Car, Int32 tireIndex, Float32 radius);

Float32 wPhysVehicleGetTireRadius (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireWidth (wNode* Car, Int32 tireIndex, Float32 width);

Float32 wPhysVehicleGetTireWidth (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireSpringConst (
    wNode* Car,
    Int32 tireIndex,
    Float32 value);

Float32 wPhysVehicleGetTireSpringConst (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireSpringDamper (
    wNode* Car,
    Int32 tireIndex,
    Float32 value);

Float32 wPhysVehicleGetTireSpringDamper (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireSuspensionLenght (
    wNode* Car,
    Int32 tireIndex,
    Float32 value);

Float32 wPhysVehicleGetTireSuspensionLenght (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireUserData (wNode* Car, Int32 tireIndex, wNode* userData);

wNode* wPhysVehicleGetTireUserData (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireMotorForce (wNode* Car, Int32 tireIndex, Float32 value);

Float32 wPhysVehicleGetTireMotorForce (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireTurnForceHelper (
    wNode* Car,
    Int32 tireIndex,
    Float32 value);

Float32 wPhysVehicleGetTireTurnForceHelper (wNode* Car, Int32 tireIndex);
void wPhysVehicleSetTireSpinTorqueFactor (
    wNode* Car,
    Int32 tireIndex,
    Float32 value);

Float32 wPhysVehicleGetTireSpinTorqueFactor (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireTorquePosition (
    wNode* Car,
    Int32 tireIndex,
    wVector3f position);

wVector3f wPhysVehicleGetTireTorquePosition (wNode* Car, Int32 tireIndex);

Float32 wPhysVehicleGetTireLoad (wNode* Car, Int32 tireIndex);

void wPhysVehicleSetTireSpinForce (wNode* Car, Int32 tireIndex, Float32 value);

Float32 wPhysVehicleGetTireSpinForce (wNode* Car, Int32 tireIndex);

/*///wPhysRagDoll
UInt32* wPhysRagDollCreate(wNode* node,
                           Int32 bonesCount,
                           wPhysRagDollBoneParameters* params);

Int32 wPhysRagDollGetBonesCount(wNode* ragdoll);

void wPhysRagDollSetBoneCollisionState(wNode* ragdoll,
                                       Int32 boneIndex,
                                       Int32 state);

void wPhysRagDollSetBoneConeLimits(wNode* ragdoll,
                                   Int32 boneIndex,
                                   Float32 angle);

void wPhysRagDollSetBoneTwistLimits(wNode* ragdoll,
                                    Int32 boneIndex,
                                    Float32 minAngle,
                                    Float32 maxAngle);

void wPhysRagDollDraw(wNode* ragdoll);*/

///wPhysMaterial///
int wPhysMaterialCreate ();

void wPhysMaterialSetElasticity (
    Int32 matId1,
    Int32 matId2,
    Float32 Elasticity);

void wPhysMaterialSetFriction (
    Int32 matId1,
    Int32 matId2,
    Float32 StaticFriction,
    Float32 KineticFriction);

void wPhysMaterialSetContactSound (
    Int32 matId1,
    Int32 matId2,
    wSound* soundNode);

void wPhysMaterialSetSoftness (Int32 matId1, Int32 matId2, Float32 Softness);

void wPhysMaterialSetCollidable (Int32 matId1, Int32 matId2, bool isCollidable);

///wGui///
void wGuiDrawAll ();

void wGuiDestroyAll ();

bool wGuiIsEventAvailable ();

wGuiEvent* wGuiReadEvent ();

bool wGuiLoad (char* fileName, wGuiObject* start);
// __cplusplus

bool wGuiSave (char* fileName, wGuiObject* start);
// __cplusplus

wGuiObject* wGuiGetSkin ();

void wGuiSetSkin (wGuiObject* skin);

const(wchar_t)* wGuiGetLastSelectedFile ();

///Returns the element which holds the focus.
wGuiObject* wGuiGetObjectFocused ();

///Returns the element which was last under the mouse cursor.
wGuiObject* wGuiGetObjectHovered ();

wGuiObject* wGuiGetRootNode ();

wGuiObject* wGuiGetObjectById (Int32 id, bool searchchildren);

wGuiObject* wGuiGetObjectByName (const(char)* name, bool searchchildren);

///wGuiObject///
void wGuiObjectDestroy (wGuiObject* element);

void wGuiObjectSetParent (wGuiObject* element, wGuiObject* parent);

wGuiObject* wGuiObjectGetParent (wGuiObject* element);

void wGuiObjectSetRelativePosition (wGuiObject* element, wVector2i position);

void wGuiObjectSetRelativeSize (wGuiObject* element, wVector2i size);

wVector2i wGuiObjectGetRelativePosition (wGuiObject* element);

wVector2i wGuiObjectGetRelativeSize (wGuiObject* element);

wVector2i wGuiObjectGetAbsolutePosition (wGuiObject* element);

wVector2i wGuiObjectGetAbsoluteClippedPosition (wGuiObject* element);

wVector2i wGuiObjectGetAbsoluteClippedSize (wGuiObject* element);

///Sets whether the element will ignore its parent's clipping rectangle.
void wGuiObjectSetClippingMode (wGuiObject* element, bool value);

bool wGuiObjectIsClipped (wGuiObject* element);

void wGuiObjectSetMaxSize (wGuiObject* element, wVector2i size);

void wGuiObjectSetMinSize (wGuiObject* element, wVector2i size);

void wGuiObjectSetAlignment (
    wGuiObject* element,
    wGuiAlignment left,
    wGuiAlignment right,
    wGuiAlignment top,
    wGuiAlignment bottom);

void wGuiObjectUpdateAbsolutePosition (wGuiObject* element);

///Возвращает гуи-объект -потомок element-а, который находится на пересечении
///с точкой экрана position
///Если нужен любой объект, то в качестве element-а нужно
///поставить root=wGuiGetRootNode()
///Примечание: Элемент root имеет размер ВСЕГО экрана
wGuiObject* wGuiObjectGetFromScreenPos (
    wGuiObject* element,
    wVector2i position);

///Персекается ли объект с точкой экрана position
bool wGuiObjectIsPointInside (wGuiObject* element, wVector2i position);

void wGuiObjectDestroyChild (wGuiObject* element, wGuiObject* child);

///Можно вызывать вместо wGuiDrawAll() для конкретного элемента
void wGuiObjectDraw (wGuiObject* element);

void wGuiObjectMoveTo (wGuiObject* element, wVector2i position);

void wGuiObjectSetVisible (wGuiObject* element, bool value);

bool wGuiObjectIsVisible (wGuiObject* element);

/// Устанавливает, был ли этот элемент управления создан
/// как часть родительского элемента.
/// Например, если полоса прокрутки является частью списка.
/// Подразделы не сохраняются на диск при вызове wGuiSave()
void wGuiObjectSetSubObject (wGuiObject* element, bool value);

/// Вовзращает, был ли этот элемент управления создан
/// как часть родительского элемента.
bool wGuiObjectIsSubObject (wGuiObject* element);

void wGuiObjectSetTabStop (wGuiObject* element, bool value);

///Returns true if this element can be focused by navigating with the tab key.
bool wGuiObjectIsTabStop (wGuiObject* element);

///Sets the priority of focus when using the tab key to navigate between a group of elements.
void wGuiObjectSetTabOrder (wGuiObject* element, Int32 index);

Int32 wGuiObjectGetTabOrder (wGuiObject* element);

///If set to true, the focus will visit this element when using the tab key to cycle through elements.
void wGuiObjectSetTabGroup (wGuiObject* element, bool value);

bool wGuiObjectIsTabGroup (wGuiObject* element);

void wGuiObjectSetEnable (wGuiObject* element, bool value);

bool wGuiObjectIsEnabled (wGuiObject* element);

void wGuiObjectSetText (wGuiObject* element, const(wchar_t)* text);

const(wchar_t)* wGuiObjectGetText (wGuiObject* element);

///Sets the new caption of this element.
void wGuiObjectSetToolTipText (wGuiObject* element, const(wchar_t)* text);

const(wchar_t)* wGuiObjectGetToolTipText (wGuiObject* element);

void wGuiObjectSetId (wGuiObject* element, Int32 id);

Int32 wGuiObjectGetId (wGuiObject* element);

void wGuiObjectSetName (wGuiObject* element, const(char)* name);

bool wGuiObjectIsHovered (wGuiObject* el);

const(char)* wGuiObjectGetName (wGuiObject* element);

///Ищет  среди "детей" объекта искомого по его Id
///Если требуется найти ЛЮБОЙ ГУИ-объект сцены,
///нужно в качестве элемента указать root=wGuiGetRootNode()
wGuiObject* wGuiObjectGetChildById (
    wGuiObject* element,
    Int32 id,
    bool searchchildren);

wGuiObject* wGuiObjectGetChildByName (
    wGuiObject* element,
    const(char)* name,
    bool searchchildren);

bool wGuiObjectIsChildOf (wGuiObject* element, wGuiObject* child);

bool wGuiObjectBringToFront (wGuiObject* element, wGuiObject* subElement);

wGuiElementType wGuiObjectGetType (wGuiObject* element);

const(char)* wGuiObjectGetTypeName (wGuiObject* element);

bool wGuiObjectHasType (wGuiObject* element, wGuiElementType type);

bool wGuiObjectSetFocus (wGuiObject* element);

bool wGuiObjectRemoveFocus (wGuiObject* element);

bool wGuiObjectIsFocused (wGuiObject* element);

void wGuiObjectReadFromXml (wGuiObject* node, wXmlReader* reader);

void wGuiObjectWriteToXml (wGuiObject* node, wXmlWriter* writer);

///wGuiSkin///
wGuiObject* wGuiSkinCreate (wGuiSkinSpace type);

wColor4s wGuiSkinGetColor (wGuiObject* skin, wGuiDefaultColor elementType);

void wGuiSkinSetColor (
    wGuiObject* skin,
    wGuiDefaultColor elementType,
    wColor4s color);

void wGuiSkinSetSize (
    wGuiObject* skin,
    wGuiDefaultSize sizeType,
    Int32 newSize);

Int32 wGuiSkinGetSize (wGuiObject* skin, wGuiDefaultSize sizeType);

const(wchar_t)* wGuiSkinGetDefaultText (wGuiObject* skin, wGuiDefaultText txt);

void wGuiSkinSetDefaultText (
    wGuiObject* skin,
    wGuiDefaultText txt,
    const(wchar_t)* newText);

void wGuiSkinSetFont (wGuiObject* skin, wFont* font, wGuiDefaultFont fntType);
// __cplusplus

wFont* wGuiSkinGetFont (wGuiObject* skin, wGuiDefaultFont fntType);
// __cplusplus

void wGuiSkinSetSpriteBank (wGuiObject* skin, wGuiObject* bank);

wGuiObject* wGuiSkinGetSpriteBank (wGuiObject* skin);

void wGuiSkinSetIcon (wGuiObject* skin, wGuiDefaultIcon icn, UInt32 index);

UInt32 wGuiSkinGetIcon (wGuiObject* skin, wGuiDefaultIcon icn);

wGuiSkinSpace wGuiSkinGetType (wGuiObject* skin);

///wGuiWindow///
wGuiObject* wGuiWindowCreate (
    const(wchar_t)* wcptrTitle,
    wVector2i minPos,
    wVector2i maxPos,
    bool modal);

wGuiObject* wGuiWindowGetButtonClose (wGuiObject* win);

wGuiObject* wGuiWindowGetButtonMinimize (wGuiObject* win);

wGuiObject* wGuiWindowGetButtonMaximize (wGuiObject* win);

void wGuiWindowSetDraggable (wGuiObject* win, bool value);

bool wGuiWindowIsDraggable (wGuiObject* win);

void wGuiWindowSetDrawBackground (wGuiObject* win, bool value);

bool wGuiWindowIsDrawBackground (wGuiObject* win);

void wGuiWindowSetDrawTitleBar (wGuiObject* win, bool value);

bool wGuiWindowIsDrawTitleBar (wGuiObject* win);

///wGuiLabel

wGuiObject* wGuiLabelCreate (
    const(wchar_t)* wcptrText,
    wVector2i minPos,
    wVector2i maxPos,
    bool boBorder,
    bool boWordWrap);
// __cplusplus

wVector2i wGuiLabelGetTextSize (wGuiObject* txt);

void wGuiLabelSetOverrideFont (wGuiObject* obj, wFont* font);

wFont* wGuiLabelGetOverrideFont (wGuiObject* obj);

wFont* wGuiLabelGetActiveFont (wGuiObject* obj);

void wGuiLabelEnableOverrideColor (wGuiObject* obj, bool value);

bool wGuiLabelIsOverrideColor (wGuiObject* obj);

void wGuiLabelSetOverrideColor (wGuiObject* obj, wColor4s color);

wColor4s wGuiLabelGetOverrideColor (wGuiObject* obj);

void wGuiLabelSetDrawBackground (wGuiObject* obj, bool value);

bool wGuiLabelIsDrawBackGround (wGuiObject* obj);

void wGuiLabelSetDrawBorder (wGuiObject* obj, bool value);

bool wGuiLabelIsDrawBorder (wGuiObject* obj);

void wGuiLabelSetTextAlignment (
    wGuiObject* obj,
    wGuiAlignment Horizontalvalue,
    wGuiAlignment Verticalvalue);

void wGuiLabelSetWordWrap (wGuiObject* obj, bool value);

bool wGuiLabelIsWordWrap (wGuiObject* obj);

void wGuiLabelSetBackgroundColor (wGuiObject* obj, wColor4s color);

wColor4s wGuiLabelGetBackgroundColor (wGuiObject* obj);

///wGuiButton
wGuiObject* wGuiButtonCreate (
    wVector2i minPos,
    wVector2i maxPos,
    const(wchar_t)* wcptrLabel,
    const(wchar_t)* wcptrTip);

void wGuiButtonSetImage (wGuiObject* btn, wTexture* img);

void wGuiButtonSetImageFromRect (
    wGuiObject* btn,
    wTexture* img,
    wVector2i* minRect,
    wVector2i* maxRect);

void wGuiButtonSetPressedImage (wGuiObject* btn, wTexture* img);

void wGuiButtonSetPressedImageFromRect (
    wGuiObject* btn,
    wTexture* img,
    wVector2i* minRect,
    wVector2i* maxRect);

void wGuiButtonSetSpriteBank (wGuiObject* btn, wGuiObject* bank);

void wGuiButtonSetSprite (
    wGuiObject* btn,
    wGuiButtonState state,
    Int32 index,
    wColor4s color,
    bool loop);

void wGuiButtonSetPush (wGuiObject* btn, bool value);

bool wGuiButtonIsPushed (wGuiObject* btn);

void wGuiButtonSetPressed (wGuiObject* btn, bool value);

bool wGuiButtonIsPressed (wGuiObject* btn);

void wGuiButtonUseAlphaChannel (wGuiObject* btn, bool value);

bool wGuiButtonIsUsedAlphaChannel (wGuiObject* btn);

void wGuiButtonEnableScaleImage (wGuiObject* btn, bool value);

bool wGuiButtonIsScaledImage (wGuiObject* btn);

void wGuiButtonSetOverrideFont (wGuiObject* obj, wFont* font);

wFont* wGuiButtonGetOverrideFont (wGuiObject* obj);

wFont* wGuiButtonGetActiveFont (wGuiObject* obj);

void wGuiButtonSetDrawBorder (wGuiObject* obj, bool value);

bool wGuiButtonIsDrawBorder (wGuiObject* obj);

///wGuiButtonGroup///
wGuiObject* wGuiButtonGroupCreate (wVector2i minPos, wVector2i maxPos);

Int32 wGuiButtonGroupAddButton (wGuiObject* group, wGuiObject* button);

Int32 wGuiButtonGroupInsertButton (
    wGuiObject* group,
    wGuiObject* button,
    UInt32 index);

wGuiObject* wGuiButtonGroupGetButton (wGuiObject* group, UInt32 index);

bool wGuiButtonGroupRemoveButton (wGuiObject* group, UInt32 index);

void wGuiButtonGroupRemoveAll (wGuiObject* group);

UInt32 wGuiButtonGroupGetSize (wGuiObject* group);

Int32 wGuiButtonGroupGetSelectedIndex (wGuiObject* group);

void wGuiButtonGroupSetSelectedIndex (wGuiObject* group, Int32 index);

void wGuiButtonGroupClearSelection (wGuiObject* group);

void wGuiButtonGroupSetBackgroundColor (wGuiObject* group, wColor4s color);

///wGuiListBox///
wGuiObject* wGuiListBoxCreate (
    wVector2i minPos,
    wVector2i maxPos,
    bool background);

UInt32 wGuiListBoxGetItemsCount (wGuiObject* lbox);

const(wchar_t)* wGuiListBoxGetItemByIndex (wGuiObject* lbox, UInt32 id);

UInt32 wGuiListBoxAddItem (wGuiObject* lbox, const(wchar_t)* text);

UInt32 wGuiListBoxAddItemWithIcon (
    wGuiObject* lbox,
    const(wchar_t)* text,
    Int32 icon);

void wGuiListBoxRemoveItem (wGuiObject* lbox, UInt32 index);

void wGuiListBoxRemoveAll (wGuiObject* lbox);

void wGuiListBoxSetItem (
    wGuiObject* lbox,
    UInt32 index,
    const(wchar_t)* text,
    Int32 icon);

void wGuiListBoxInsertItem (
    wGuiObject* lbox,
    UInt32 index,
    const(wchar_t)* text,
    Int32 icon);

Int32 wGuiListBoxGetItemIcon (wGuiObject* lbox, UInt32 index);

UInt32 wGuiListBoxGetSelectedIndex (wGuiObject* lbox);

void wGuiListBoxSelectItemByIndex (wGuiObject* lbox, UInt32 index);

void wGuiListBoxSelectItemByText (wGuiObject* lbox, const(wchar_t)* item);

void wGuiListBoxSwapItems (wGuiObject* lbox, UInt32 index1, UInt32 index2);

void wGuiListBoxSetItemsHeight (wGuiObject* lbox, Int32 height);

void wGuiListBoxSetAutoScrolling (wGuiObject* lbox, bool scroll);

bool wGuiListBoxIsAutoScrolling (wGuiObject* lbox);

void wGuiListBoxSetItemColor (wGuiObject* lbox, UInt32 index, wColor4s color);

void wGuiListBoxSetElementColor (
    wGuiObject* lbox,
    UInt32 index,
    wGuiListboxColor colorType,
    wColor4s color);

void wGuiListBoxClearItemColor (wGuiObject* lbox, UInt32 index);

void wListBoxClearElementColor (
    wGuiObject* lbox,
    UInt32 index,
    wGuiListboxColor colorType);

wColor4s wGuiListBoxGetElementColor (
    wGuiObject* lbox,
    UInt32 index,
    wGuiListboxColor colorType);

bool wGuiListBoxHasElementColor (
    wGuiObject* lbox,
    UInt32 index,
    wGuiListboxColor colorType);

wColor4s wGuiListBoxGetDefaultColor (
    wGuiObject* lbox,
    wGuiListboxColor colorType);

void wGuiListBoxSetDrawBackground (wGuiObject* obj, bool value);

///wGuiScrollBar
wGuiObject* wGuiScrollBarCreate (
    bool Horizontal,
    wVector2i minPos,
    wVector2i maxPos);

void wGuiScrollBarSetMaxValue (wGuiObject* scroll, Int32 max);

Int32 wGuiScrollBarGetMaxValue (wGuiObject* scroll);

void wGuiScrollBarSetMinValue (wGuiObject* scroll, Int32 min);

Int32 wGuiScrollBarGetMinValue (wGuiObject* scroll);

void wGuiScrollBarSetValue (wGuiObject* scroll, Int32 value);

Int32 wGuiScrollBarGetValue (wGuiObject* scroll);

void wGuiScrollBarSetSmallStep (wGuiObject* scroll, Int32 step);

Int32 wGuiScrollBarGetSmallStep (wGuiObject* scroll);

void wGuiScrollBarSetLargeStep (wGuiObject* scroll, Int32 step);

Int32 wGuiScrollBarGetLargeStep (wGuiObject* scroll);

///wGuiEditBox
wGuiObject* wGuiEditBoxCreate (
    const(wchar_t)* wcptrText,
    wVector2i minPos,
    wVector2i maxPos);

void wGuiEditBoxSetMultiLine (wGuiObject* box, bool value);

bool wGuiEditBoxIsMultiLine (wGuiObject* box);

void wGuiEditBoxSetAutoScrolling (wGuiObject* box, bool value);

bool wGuiEditBoxIsAutoScrolling (wGuiObject* box);

void wGuiEditBoxSetPasswordMode (wGuiObject* box, bool value);

bool wGuiEditBoxIsPasswordMode (wGuiObject* box);

wVector2i wGuiEditBoxGetTextSize (wGuiObject* box);

///Sets the maximum amount of characters which may be entered in the box.
void wGuiEditBoxSetCharactersLimit (wGuiObject* box, UInt32 max);

UInt32 wGuiEditGetCharactersLimit (wGuiObject* box);

void wGuiEditBoxSetOverrideFont (wGuiObject* obj, wFont* font);

wFont* wGuiEditBoxGetOverrideFont (wGuiObject* obj);

wFont* wGuiEditBoxGetActiveFont (wGuiObject* obj);

void wGuiEditBoxEnableOverrideColor (wGuiObject* obj, bool value);

bool wGuiEditBoxIsOverrideColor (wGuiObject* obj);

void wGuiEditBoxSetOverrideColor (wGuiObject* obj, wColor4s color);

wColor4s wGuiEditBoxGetOverrideColor (wGuiObject* obj);

void wGuiEditBoxSetDrawBackground (wGuiObject* obj, bool value);

void wGuiEditBoxSetDrawBorder (wGuiObject* obj, bool value);

bool wGuiEditBoxIsDrawBorder (wGuiObject* obj);

void wGuiEditBoxSetTextAlignment (
    wGuiObject* obj,
    wGuiAlignment Horizontalvalue,
    wGuiAlignment Verticalvalue);

void wGuiEditBoxSetWordWrap (wGuiObject* obj, bool value);

bool wGuiEditBoxIsWordWrap (wGuiObject* obj);

///wGuiImage///
wGuiObject* wGuiImageCreate (wTexture* texture, wVector2i size, bool useAlpha);

void wGuiImageSet (wGuiObject* img, wTexture* tex);

wTexture* wGuiImageGet (wGuiObject* img);

void wGuiImageSetColor (wGuiObject* img, wColor4s color);

wColor4s wGuiImageGetColor (wGuiObject* img);

void wGuiImageSetScaling (wGuiObject* img, bool scale);

bool wGuiImageIsScaled (wGuiObject* img);

void wGuiImageUseAlphaChannel (wGuiObject* img, bool use);

bool wGuiImageIsUsedAlphaChannel (wGuiObject* img);

///wGuiFader///
wGuiObject* wGuiFaderCreate (wVector2i minPos, wVector2i maxPos);

void wGuiFaderSetColor (wGuiObject* fader, wColor4s color);

wColor4s wGuiFaderGetColor (wGuiObject* fader);

void wGuiFaderSetColorExt (
    wGuiObject* fader,
    wColor4s colorSrc,
    wColor4s colorDest);

void wGuiFaderFadeIn (wGuiObject* fader, UInt32 timeMs);

void wGuiFaderFadeOut (wGuiObject* fader, UInt32 timeMs);

bool wGuiFaderIsReady (wGuiObject* fader);

///wGuiCheckBox///
wGuiObject* wGuiCheckBoxCreate (
    const(wchar_t)* wcptrText,
    wVector2i minPos,
    wVector2i maxPos,
    bool checked);

void wGuiCheckBoxCheck (wGuiObject* box, bool checked);

bool wGuiCheckBoxIsChecked (wGuiObject* box);

/// Sets whether to draw the background
void wGuiCheckBoxSetDrawBackground (wGuiObject* box, bool value);

/// Checks if background drawing is enabled
///return true if background drawing is enabled, false otherwise
bool wGuiCheckBoxIsDrawBackground (wGuiObject* box);

/// Sets whether to draw the border
void wGuiCheckBoxSetDrawBorder (wGuiObject* box, bool value);

/// Checks if border drawing is enabled
///return true if border drawing is enabled, false otherwise
bool wGuiCheckBoxIsDrawBorder (wGuiObject* box);

void wGuiCheckBoxSetFilled (wGuiObject* box, bool value);

bool wGuiCheckBoxIsFilled (wGuiObject* box);

///wGuiFileOpenDialog
/*Warning:
    When the user selects a folder this does change the current working directory

This element can create the following events of type EGUI_EVENT_TYPE:

        EGET_DIRECTORY_SELECTED
        EGET_FILE_SELECTED
        EGET_FILE_CHOOSE_DIALOG_CANCELLED
*/
wGuiObject* wGuiFileOpenDialogCreate (const(wchar_t)* wcptrLabel, bool modal);

///Returns the filename of the selected file. Returns NULL, if no file was selected.
const(wchar_t)* wGuiFileOpenDialogGetFile (wGuiObject* dialog);

///Returns the directory of the selected file. Returns NULL, if no directory was selected.
const(char)* wGuiFileOpenDialogGetDirectory (wGuiObject* dialog);

///wGuiComboBox///
wGuiObject* wGuiComboBoxCreate (wVector2i minPos, wVector2i maxPos);

UInt32 wGuiComboBoxGetItemsCount (wGuiObject* combo);

const(wchar_t)* wGuiComboBoxGetItemByIndex (wGuiObject* combo, UInt32 idx);

UInt32 wGuiComboBoxGetItemDataByIndex (wGuiObject* combo, UInt32 idx);

Int32 wGuiComboBoxGetIndexByItemData (wGuiObject* combo, UInt32 data);

UInt32 wGuiComboBoxAddItem (
    wGuiObject* combo,
    const(wchar_t)* text,
    UInt32 data);

void wGuiComboBoxRemoveItem (wGuiObject* combo, UInt32 idx);

void wGuiComboBoxRemoveAll (wGuiObject* combo);

///Returns id of selected item. returns -1 if no item is selected.
Int32 wGuiComboBoxGetSelected (wGuiObject* combo);

void wGuiComboBoxSetSelected (wGuiObject* combo, UInt32 idx);

void wGuiComboBoxSetMaxSelectionRows (wGuiObject* combo, UInt32 max);

UInt32 wGuiComboBoxGetMaxSelectionRows (wGuiObject* combo);

void wGuiComboBoxSetTextAlignment (
    wGuiObject* obj,
    wGuiAlignment Horizontalvalue,
    wGuiAlignment Verticalvalue);

///wGuiContextMenu///
wGuiObject* wGuiContextMenuCreate (wVector2i minPos, wVector2i maxPos);

void wGuiContextMenuSetCloseHandling (
    wGuiObject* cmenu,
    wContextMenuClose onClose);

wContextMenuClose wGuiContextMenuGetCloseHandling (wGuiObject* cmenu);

UInt32 wGuiContextMenuGetItemsCount (wGuiObject* cmenu);

UInt32 wGuiContextMenuAddItem (
    wGuiObject* cmenu,
    const(wchar_t)* text,
    Int32 commandId,
    bool enabled,
    bool hasSubMenu,
    bool checked,
    bool autoChecking);
// __cplusplus

UInt32 wGuiContextMenuInsertItem (
    wGuiObject* cmenu,
    UInt32 idx,
    const(wchar_t)* text,
    Int32 commandId,
    bool enabled,
    bool hasSubMenu,
    bool checked,
    bool autoChecking);
// __cplusplus

void wGuiContextMenuAddSeparator (wGuiObject* cmenu);

const(wchar_t)* wGuiContextMenuGetItemText (wGuiObject* cmenu, UInt32 idx);

void wGuiContextMenuSetItemText (
    wGuiObject* cmenu,
    UInt32 idx,
    const(wchar_t)* text);

void wGuiContextMenuSetItemEnabled (wGuiObject* cmenu, UInt32 idx, bool value);

bool wGuiContextMenuIsItemEnabled (wGuiObject* cmenu, UInt32 idx);

void wGuiContextMenuSetItemChecked (wGuiObject* cmenu, UInt32 idx, bool value);

bool wGuiContextMenuIsItemChecked (wGuiObject* cmenu, UInt32 idx);

void wGuiContextMenuRemoveItem (wGuiObject* cmenu, UInt32 idx);

void wGuiContextMenuRemoveAll (wGuiObject* cmenu);

Int32 wGuiContextMenuGetSelectedItem (wGuiObject* cmenu);

Int32 wGuiContextMenuGetItemCommandId (wGuiObject* cmenu, UInt32 idx);

Int32 wGuiContextMenuFindItem (wGuiObject* cmenu, Int32 id, UInt32 idx);
// __cplusplus

void wGuiContextMenuSetItemCommandId (wGuiObject* cmenu, UInt32 idx, Int32 id);

wGuiObject* wGuiContextMenuGetSubMenu (wGuiObject* cmenu, UInt32 idx);

void wGuiContextMenuSetAutoChecking (
    wGuiObject* cmenu,
    UInt32 idx,
    bool autoChecking);

bool wGuiContextMenuIsAutoChecked (wGuiObject* cmenu, UInt32 idx);

///When an eventparent is set it receives events instead of the usual parent element.
void wGuiContextMenuSetEventParent (wGuiObject* cmenu, wGuiObject* parent);

///wGuiMenu///
///Adds a menu to the environment.This is like the menu you can find on top of most windows in modern graphical user interfaces.
///Для работы с меню подходят все команды
///из раздела wGuiContextMenu///
wGuiObject* wGuiMenuCreate ();

///wGuiModalScreen///
///Adds a modal screen.
///This control stops its parent's members from being able to receive input until its last child is removed,
/// it then deletes itself.
wGuiObject* wGuiModalScreenCreate ();

///wGuiSpinBox///

wGuiObject* wGuiSpinBoxCreate (
    const(wchar_t)* wcptrText,
    wVector2i minPos,
    wVector2i maxPos,
    bool border);
// __cplusplus

wGuiObject* wGuiSpinBoxGetEditBox (wGuiObject* box);

void wGuiSpinBoxSetValue (wGuiObject* spin, Float32 value);

Float32 wGuiSpinBoxGetValue (wGuiObject* spin);

void wGuiSpinBoxSetRange (wGuiObject* spin, wVector2f range);

Float32 wGuiSpinBoxGetMin (wGuiObject* spin);

Float32 wGuiSpinBoxGetMax (wGuiObject* spin);

void wGuiSpinBoxSetStepSize (wGuiObject* spin, Float32 step);

Float32 wGuiSpinBoxGetStepSize (wGuiObject* spin);

void wGuiSpinBoxSetDecimalPlaces (wGuiObject* spin, Int32 places);

///wGuiTab///
wGuiObject* wGuiTabCreate (wVector2i minPos, wVector2i maxPos);

Int32 wGuiTabGetNumber (wGuiObject* tab);

void wGuiTabSetTextColor (wGuiObject* tab, wColor4s color);

wColor4s wGuiTabGetTextColor (wGuiObject* tab);

void wGuiTabSetDrawBackground (wGuiObject* tab, bool value);

void wGuiTabSetBackgroundColor (wGuiObject* tab, wColor4s color);

wColor4s wGuiTabGetBackgroundColor (wGuiObject* tab);

///wGuiTabControl///

wGuiObject* wGuiTabControlCreate (
    wVector2i minPos,
    wVector2i maxPos,
    bool background,
    bool border);
// __cplusplus

Int32 wGuiTabControlGetTabsCount (wGuiObject* control);

wGuiObject* wGuiTabControlAddTab (
    wGuiObject* control,
    const(wchar_t)* caption,
    Int32 id);
// __cplusplus

wGuiObject* wGuiTabControlInsertTab (
    wGuiObject* control,
    UInt32 idx,
    const(wchar_t)* caption,
    Int32 id);
// __cplusplus

wGuiObject* wGuiTabControlGetTab (wGuiObject* control, Int32 idx);

bool wGuiTabControlSetActiveTabByIndex (wGuiObject* control, Int32 idx);

bool wGuiTabControlSetActiveTab (wGuiObject* control, wGuiObject* tab);

Int32 wGuiTabControlGetActiveTab (wGuiObject* control);

Int32 wGuiTabControlGetTabFromPos (wGuiObject* control, wVector2i position);

void wGuiTabControlRemoveTab (wGuiObject* control, Int32 idx);

void wGuiTabControlRemoveAll (wGuiObject* control);

void wGuiTabControlSetTabHeight (wGuiObject* control, Int32 height);

Int32 wGuiTabControlGetTabHeight (wGuiObject* control);

void wGuiTabControlSetTabMaxWidth (wGuiObject* control, Int32 width);

Int32 wGuiTabControlGetTabMaxWidth (wGuiObject* control);

void wGuiTabControlSetVerticalAlignment (wGuiObject* control, wGuiAlignment al);

wGuiAlignment wGuiTabControlGetVerticalAlignment (wGuiObject* control);

void wGuiTabControlSetTabExtraWidth (wGuiObject* control, Int32 extraWidth);

Int32 wGuiTabControlGetTabExtraWidth (wGuiObject* control);

///wGuiTable///

wGuiObject* wGuiTableCreate (
    wVector2i minPos,
    wVector2i maxPos,
    bool background);
// __cplusplus

void wGuiTableAddColumn (
    wGuiObject* table,
    wchar_t* caption,
    Int32 columnIndex);
// __cplusplus

void wGuiTableRemoveColumn (wGuiObject* table, UInt32 columnIndex);

Int32 wGuiTableGetColumnsCount (wGuiObject* table);

bool wGuiTableSetActiveColumn (wGuiObject* table, Int32 idx, bool doOrder);
// __cplusplus

Int32 wGuiTableGetActiveColumn (wGuiObject* table);

wGuiColumnOrdering wGuiTableGetActiveColumnOrdering (wGuiObject* table);

void wGuiTableSetColumnWidth (
    wGuiObject* table,
    UInt32 columnIndex,
    UInt32 width);

void wGuiTableSetColumnsResizable (wGuiObject* table, bool resizible);

bool wGuiTableIsColumnsResizable (wGuiObject* table);

Int32 wGuiTableGetSelected (wGuiObject* table);

void wGuiTableSetSelectedByIndex (wGuiObject* table, Int32 index);

Int32 wGuiTableGetRowsCount (wGuiObject* table);

UInt32 wGuiTableAddRow (wGuiObject* table, UInt32 rowIndex);

void wGuiTableRemoveRow (wGuiObject* table, UInt32 rowIndex);

void wGuiTableClearRows (wGuiObject* table);

void wGuiTableSwapRows (wGuiObject* table, UInt32 rowIndexA, UInt32 rowIndexB);

void wGuiTableSetOrderRows (
    wGuiObject* table,
    Int32 columnIndex,
    wGuiOrderingMode mode);

void wGuiTableSetCellText (
    wGuiObject* table,
    UInt32 rowIndex,
    UInt32 columnIndex,
    const(wchar_t)* text,
    wColor4s color);

void wGuiTableSetCellData (
    wGuiObject* table,
    UInt32 rowIndex,
    UInt32 columnIndex,
    UInt32* data);

void wGuiTableSetCellColor (
    wGuiObject* table,
    UInt32 rowIndex,
    UInt32 columnIndex,
    wColor4s color);

const(wchar_t)* wGuiTableGetCellText (
    wGuiObject* table,
    UInt32 rowIndex,
    UInt32 columnIndex);

UInt32* wGuiTableGetCellData (
    wGuiObject* table,
    UInt32 rowIndex,
    UInt32 columnIndex);

void wGuiTableSetDrawFlags (wGuiObject* table, wGuiTableDrawFlags flags);

wGuiTableDrawFlags wGuiTableGetDrawFlags (wGuiObject* table);

///wGuiToolBar///
wGuiObject* wGuiToolBarCreate ();

wGuiObject* wGuiToolBarAddButton (
    wGuiObject* bar,
    const(wchar_t)* text,
    const(wchar_t)* tooltiptext,
    wTexture* img,
    wTexture* pressedImg,
    bool isPushButton,
    bool useAlphaChannel);
// __cplusplus

///wGuiMessageBox

wGuiObject* wGuiMessageBoxCreate (
    const(wchar_t)* wcptrTitle,
    const(wchar_t)* wcptrTCaption,
    bool modal,
    wGuiMessageBoxFlags flags,
    wTexture* image);
// __cplusplus

///wGuiTree///
///Create a tree view element.
wGuiObject* wGuiTreeCreate (
    wVector2i minPos,
    wVector2i maxPos,
    bool background,
    bool barvertical,
    bool barhorizontal);

///returns the root node (not visible) from the tree.
wGuiObject* wGuiTreeGetRoot (wGuiObject* tree);

///returns the selected node of the tree or 0 if none is selected
wGuiObject* wGuiTreeGetSelected (wGuiObject* tree);

///sets if the tree lines are visible
void wGuiTreeSetLinesVisible (wGuiObject* tree, bool visible);

///returns true if the tree lines are visible
bool wGuiTreeIsLinesVisible (wGuiObject* tree);

///Sets the font which should be used as icon font.
void wGuiTreeSetIconFont (wGuiObject* tree, wFont* font);

///Sets the image list which should be used for the image and selected image of every node.
void wGuiTreSetImageList (wGuiObject* tree, wGuiObject* list);

///Returns the image list which is used for the nodes.
wGuiObject* wGuiTreeGetImageList (wGuiObject* tree);

///Sets if the image is left of the icon. Default is true.
void wGuiTreeSetImageLeftOfIcon (wGuiObject* tree, bool bLeftOf);

///Returns if the Image is left of the icon. Default is true.
bool wGuiTreeIsImageLeftOfIcon (wGuiObject* tree);

///Returns the node which is associated to the last event.
wGuiObject* wGuiTreeGetLastEventNode (wGuiObject* tree);

///wGuiTreeNode///
///returns the owner (Gui tree) of this node
wGuiObject* wGuiTreeNodeGetOwner (wGuiObject* node);

///returns the text of the node
const(wchar_t)* wGuiTreeNodeGetText (wGuiObject* node);

///sets the text of the node
void wGuiTreeNodeSetText (wGuiObject* node, const(wchar_t)* text);

///sets the icon text of the node
void wGuiTreeNodeSetIcon (wGuiObject* node, const(wchar_t)* icon);

///returns the icon text of the node
const(wchar_t)* wGuiTreeNodeGetIcon (wGuiObject* node);

///sets the image index of the node
void wGuiTreeNodeSetImageIndex (wGuiObject* node, UInt32 imageIndex);

///returns the image index of the node
UInt32 wGuiTreeNodeGetImageIndex (wGuiObject* node);

///sets the image index of the node
void wGuiTreeNodeSetSelectedImageIndex (wGuiObject* node, UInt32 imageIndex);

///returns the image index of the node
UInt32 wGuiTreeNodeGetSelectedImageIndex (wGuiObject* node);

///sets the user data (UInt32*) of this node
void wGuiTreeNodeSetData (wGuiObject* node, UInt32* data);

///returns the user data (UInt32*) of this node
UInt32* wGuiTreeNodeGetData (wGuiObject* node);

///sets the user data2 of this node
void wGuiTreeNodeSetData2 (wGuiObject* node, UInt32* data2);

///returns the user data2 of this node
UInt32* wGuiTreeNodeGetData2 (wGuiObject* node);

///returns the child item count
UInt32 wGuiTreeNodeGetChildsCount (wGuiObject* node);

///Remove a child node.
void wGuiTreeNodeRemoveChild (wGuiObject* node, wGuiObject* child);

///removes all children (recursive) from this node
void wGuiTreeNodeRemoveChildren (wGuiObject* node);

///returns true if this node has child nodes
bool wGuiTreeNodeHasChildren (wGuiObject* node);

///Adds a new node behind the last child node.

wGuiObject* wGuiTreeNodeAddChildBack (
    wGuiObject* node,
    const(wchar_t)* text,
    const(wchar_t)* icon,
    Int32 imageIndex,
    Int32 selectedImageIndex,
    void* data,
    UInt32* data2);
// __cplusplus

///Adds a new node before the first child node.

wGuiObject* wGuiTreeNodeAddChildFront (
    wGuiObject* node,
    const(wchar_t)* text,
    const(wchar_t)* icon,
    Int32 imageIndex,
    Int32 selectedImageIndex,
    void* data,
    UInt32* data2);
// __cplusplus

///Adds a new node behind the other node.

wGuiObject* wGuiTreeNodeInsertChildAfter (
    wGuiObject* node,
    wGuiObject* other,
    const(wchar_t)* text,
    const(wchar_t)* icon,
    Int32 imageIndex,
    Int32 selectedImageIndex,
    void* data,
    UInt32* data2);
// __cplusplus

///Adds a new node before the other node.

wGuiObject* wGuiTreeNodeInsertChildBefore (
    wGuiObject* node,
    wGuiObject* other,
    const(wchar_t)* text,
    const(wchar_t)* icon,
    Int32 imageIndex,
    Int32 selectedImageIndex,
    void* data,
    UInt32* data2);
// __cplusplus

///Return the first child node from this node.
wGuiObject* wGuiTreeNodeGetFirstChild (wGuiObject* node);

///Return the last child node from this node.
wGuiObject* wGuiTreeNodeGetLastChild (wGuiObject* node);

///Returns the previous sibling node from this node.
wGuiObject* wGuiTreeNodeGetPrevSibling (wGuiObject* node);

///Returns the next sibling node from this node.
wGuiObject* wGuiTreeNodeGetNextSibling (wGuiObject* node);

///Returns the next visible (expanded, may be out of scrolling) node from this node.
wGuiObject* wGuiTreeNodeGetNextVisible (wGuiObject* node);

///Moves a child node one position up.
bool wGuiTreeNodeMoveChildUp (wGuiObject* node, wGuiObject* child);

///Moves a child node one position down.
bool wGuiTreeNodeMoveChildDown (wGuiObject* node, wGuiObject* child);

///Sets if the node is expanded.
void wGuiTreeNodeSetExpanded (wGuiObject* node, bool expanded);

///Returns true if the node is expanded (children are visible).
bool wGuiTreeNodeIsExpanded (wGuiObject* node);

///Sets this node as selected
void wGuiTreeNodeSetSelected (wGuiObject* node, bool selected);

///Returns true if the node is currently selected.
bool wGuiTreeNodeIsSelected (wGuiObject* node);

///Returns true if this node is the root node.
bool wGuiTreeNodeIsRoot (wGuiObject* node);

///Returns the level of this node.
///The root node has level 0.
///Direct children of the root has level 1 ...
Int32 wGuiTreeNodeGetLevel (wGuiObject* node);

///wGuiImageList///
wGuiObject* wGuiImageListCreate (
    wTexture* texture,
    wVector2i size,
    bool useAlphaChannel);

void wGuiImageListDraw (
    wGuiObject* list,
    Int32 index,
    wVector2i pos,
    wVector2i clipPos,
    wVector2i clipSize);

Int32 wGuiImageListGetCount (wGuiObject* list);

wVector2i wGuiImageListGetSize (wGuiObject* list);

///wGuiColorSelectDialog///
wGuiObject* wGuiColorSelectDialogCreate (const(wchar_t)* title, bool modal);

///wGuiMeshViewer///
wGuiObject* wGuiMeshViewerCreate (
    wVector2i minPos,
    wVector2i maxPos,
    const(wchar_t)* text);

void wGuiMeshViewerSetMesh (wGuiObject* viewer, wMesh* mesh);

wMesh* wGuiMeshViewerGetMesh (wGuiObject* viewer);

void wGuiMeshViewerSetMaterial (wGuiObject* viewer, wMaterial* material);

wMaterial* wMeshViewerGetMaterial (wGuiObject* viewer);

///wGuiSpriteBank///
///Returns pointer to the sprite bank with the specified file name.
///Loads the bank if it was not loaded before.
wGuiObject* wGuiSpriteBankLoad (char* file);

wGuiObject* wGuiSpriteBankCreate (char* name);

///Adds a texture to the sprite bank.
void wGuiSpriteBankAddTexture (wGuiObject* bank, wTexture* texture);

///Changes one of the textures in the sprite bank
void wGuiSpriteBankSetTexture (
    wGuiObject* bank,
    UInt32 index,
    wTexture* texture);

///Add the texture and use it for a single non-animated sprite.
///The texture and the corresponding rectangle and sprite will all be added
/// to the end of each array. returns the index of the sprite or -1 on failure
Int32 wGuiSpriteBankAddSprite (wGuiObject* bank, wTexture* texture);

wTexture* wGuiSpriteBankGetTexture (wGuiObject* bank, UInt32 index);

UInt32 wGuiSpriteBankGetTexturesCount (wGuiObject* bank);

void wGuiSpriteBankRemoveAll (wGuiObject* bank);

void wGuiSpriteBankDrawSprite (
    wGuiObject* bank,
    UInt32 index,
    wVector2i position,
    wVector2i* clipPosition,
    wVector2i* clipSize,
    wColor4s color,
    UInt32 starttime,
    UInt32 currenttime,
    bool loop,
    bool center);
// __cplusplus

void wGuiSpriteBankDrawSpriteBatch (
    wGuiObject* bank,
    UInt32* indexArray,
    UInt32 idxArrayCount,
    wVector2i* positionArray,
    UInt32 posArrayCount,
    wVector2i* clipPosition,
    wVector2i* clipSize,
    wColor4s color,
    UInt32 startTime,
    UInt32 currentTime,
    bool loop,
    bool center);
// __cplusplus

///wGuiCheckGroup
wGuiObject* wGuiCheckBoxGroupCreate (wVector2i minPos, wVector2i maxPos);

Int32 wGuiCheckBoxGroupAddCheckBox (wGuiObject* group, wGuiObject* check);

Int32 wGuiCheckBoxGroupInsertCheckBox (
    wGuiObject* group,
    wGuiObject* check,
    UInt32 index);

wGuiObject* wGuiCheckBoxGroupGetCheckBox (wGuiObject* group, UInt32 index);

Int32 wGuiCheckBoxGroupGetIndex (wGuiObject* group, wGuiObject* check);

Int32 wGuiCheckBoxGroupGetSelectedIndex (wGuiObject* group);

bool wGuiCheckBoxGroupRemoveCheckBox (wGuiObject* group, UInt32 index);

void wGuiCheckBoxGroupRemoveAll (wGuiObject* group);

UInt32 wGuiCheckBoxGroupGetSize (wGuiObject* group);

void wGuiCheckBoxGroupSelectCheckBox (wGuiObject* group, Int32 index);

void wGuiCheckBoxGroupClearSelection (wGuiObject* group);

void wGuiCheckBoxGroupSetBackgroundColor (wGuiObject* obj, wColor4s color);

///wGuiProgressBar///

wGuiObject* wGuiProgressBarCreate (
    wVector2i minPos,
    wVector2i maxPos,
    bool isHorizontal);
// __cplusplus

void wGuiProgressBarSetPercentage (wGuiObject* bar, UInt32 percent);

UInt32 wGuiProgressBarGetPercentage (wGuiObject* bar);

void wGuiProgressBarSetDirection (wGuiObject* bar, bool isHorizontal);

bool wGuiProgressBarIsHorizontal (wGuiObject* bar);

void wGuiProgressBarSetBorderSize (wGuiObject* bar, UInt32 size);

void wGuiProgressBarSetSize (wGuiObject* bar, wVector2u size);

void wProgressBarSetFillColor (wGuiObject* bar, wColor4s color);

wColor4s wProgressBarGetFillColor (wGuiObject* bar);

void wGuiProgressBarSetTextColor (wGuiObject* bar, wColor4s color);

void wGuiProgressBarShowText (wGuiObject* bar, bool value);

bool wGuiProgressBarIsShowText (wGuiObject* bar);

void wGuiProgressBarSetFillTexture (wGuiObject* bar, wTexture* tex);

void wGuiProgressBarSetBackTexture (wGuiObject* bar, wTexture* tex);

void wGuiProgressBarSetFont (wGuiObject* bar, wFont* font);

void wGuiProgressBarSetBackgroundColor (wGuiObject* bar, wColor4s color);

void wGuiProgressBarSetBorderColor (wGuiObject* obj, wColor4s color);

///wGuiTextArea///

wGuiObject* wGuiTextAreaCreate (
    wVector2i minPos,
    wVector2i maxPos,
    Int32 maxLines);
// __cplusplus

void wGuiTextAreaSetBorderSize (wGuiObject* tarea, UInt32 size);

void wGuiTextAreaSetAutoScroll (wGuiObject* tarea, bool value);

void wGuiTextAreaSetPadding (wGuiObject* tarea, UInt32 padding);

void wGuiTextAreaSetBackTexture (wGuiObject* tarea, wTexture* tex);

void wGuiTextAreaSetWrapping (wGuiObject* tarea, bool value);

void wGuiTextAreaSetFont (wGuiObject* tarea, wFont* font);

//iconMode=0/1

void wGuiTextAreaAddLine (
    wGuiObject* tarea,
    const(wchar_t)* text,
    UInt32 lifeTime,
    wColor4s color,
    wTexture* icon,
    Int32 iconMode);
// __cplusplus

void wGuiTextAreaRemoveAll (wGuiObject* tarea);

void wGuiTextAreaSetBackgroundColor (wGuiObject* tarea, wColor4s color);

void wGuiTextAreaSetBorderColor (wGuiObject* tarea, wColor4s color);

///wGuiCEditor///
wGuiObject* wGuiCEditorCreate (
    const(wchar_t)* wcptrText,
    wVector2i minPos,
    wVector2i maxPos,
    bool border);

void wGuiCEditorSetHScrollVisible (wGuiObject* box, bool value);

void wGuiCEditorSetText (wGuiObject* box, const(wchar_t)* text);

void wGuiCEditorSetColors (
    wGuiObject* box,
    wColor4s backColor,
    wColor4s lineColor,
    wColor4s textColor);

void wGuiCEditorSetLinesCountVisible (wGuiObject* box, bool value);

bool wGuiCEditorIsLinesCountVisible (wGuiObject* box);

void wGuiCEditorSetElementText (
    wGuiObject* box,
    UInt32 index,
    const(wchar_t)* text);

void wGuiCEditorSetSelectionColors (
    wGuiObject* box,
    wColor4s backColor,
    wColor4s textColor,
    wColor4s back2Color);

void wGuiCEditorRemoveText (wGuiObject* box);

void wGuiCEditorAddKeyword (
    wGuiObject* box,
    const(char)* word,
    wColor4s color,
    bool matchCase);

void wGuiCEditorAddLineKeyword (
    wGuiObject* box,
    const(char)* word,
    wColor4s color,
    bool matchCase);

void wGuiCEditorAddGroupKeyword (
    wGuiObject* box,
    const(char)* word,
    const(char)* endKeyword,
    wColor4s color,
    bool matchCase);

void wGuiCEditorBoxAddKeywordInfo (wGuiObject* box, Int32 size, Int32 type);

void wGuiCEditorBoxRemoveAllKeywords (wGuiObject* box);

void wGuiCEditorBoxAddCppKeywords (
    wGuiObject* box,
    wColor4s key,
    wColor4s string,
    wColor4s comment);

void wGuiCEditorAddLuaKeywords (
    wGuiObject* box,
    wColor4s key,
    wColor4s string,
    wColor4s comment);

void wGuiCEditorAddFbKeywords (
    wGuiObject* box,
    wColor4s key,
    wColor4s string,
    wColor4s comment);

void wGuiCEditorReplaceText (
    wGuiObject* box,
    Int32 start,
    Int32 end,
    const(wchar_t)* text);

void wGuiCEditorPressReturn (wGuiObject* box);

void wGuiCEditorAddText (wGuiObject* box, const(wchar_t)* addText);

const(wchar_t)* wGuiCEditorGetText (wGuiObject* box);

void wGuiCEditorSetLineToggleVisible (wGuiObject* box, bool value);

void wGuiCEditorSetContextMenuText (
    wGuiObject* box,
    const(wchar_t)* cut_text,
    const(wchar_t)* copy_text,
    const(wchar_t)* paste_text,
    const(wchar_t)* del_text,
    const(wchar_t)* redo_text,
    const(wchar_t)* undo_text,
    const(wchar_t)* btn_text);

void wGuiCEditorBoxCopy (wGuiObject* box);

void wGuiCEditorCut (wGuiObject* box);

void wGuiCEditorPaste (wGuiObject* box);

void wGuiCEditorUndo (wGuiObject* box);

void wGuiCEditorRedo (wGuiObject* box);

wFont* wGuiCEditorGetOverrideFont (wGuiObject* obj);

wFont* wGuiCEditorGetActiveFont (wGuiObject* obj);

void wGuiCEditorEnableOverrideColor (wGuiObject* obj, bool value);

bool wGuiCEditorIsOverrideColor (wGuiObject* obj);

void wGuiCEditorSetOverrideColor (wGuiObject* obj, wColor4s color);

wColor4s wGuiCEditorGetOverrideColor (wGuiObject* obj);

void wGuiCEditorSetDrawBackground (wGuiObject* obj, bool value);

bool wGuiCEditorIsDrawBackGround (wGuiObject* obj);

void wGuiCEditorSetDrawBorder (wGuiObject* obj, bool value);

bool wGuiCEditorIsDrawBorder (wGuiObject* obj);

void wGuiCEditorSetTextAlignment (
    wGuiObject* obj,
    wGuiAlignment Horizontalvalue,
    wGuiAlignment Verticalvalue);

void wGuiCEditorSetWordWrap (wGuiObject* obj, bool value);

bool wGuiCEditorIsWordWrap (wGuiObject* obj);

void wGuiCEditorSetBackgroundColor (wGuiObject* obj, wColor4s color);

// __cplusplus

// WORLDSIM3D_H_INCLUDED
