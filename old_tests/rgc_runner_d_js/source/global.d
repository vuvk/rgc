module global;

import std.stdio;
import std.algorithm;
import core.stdc.string;
import core.sync.mutex;

import WorldSim3D;
import SampleFunctions;

import physfs_util;
import constants;
import map;

__gshared int g_TexturesCount = 0;
__gshared int g_SpritesCount  = 0;
__gshared int g_DoorsCount    = 0;
__gshared int g_KeysCount     = 0;
__gshared int g_WeaponsCount  = 0;
__gshared int g_BulletsCount  = 0;
__gshared int g_AmmoCount     = 0;
__gshared int g_MapsCount     = 0;
__gshared uint g_ScriptsCount;      /** количество загруженных скриптов */

__gshared bool g_PreviewMode = false;

__gshared wVector2u g_Resolution = wDEFAULT_SCREENSIZE;
__gshared wDriverTypes g_Renderer = wDriverTypes.wDRT_OPENGL;   /** рендерер, с которым будет запущен раннер */
__gshared bool   g_FullScreenMode = false;  /** режим полного экрана */
__gshared bool   g_LimitFps = true;         /** установить лимит кадров в MAX_FPS */
__gshared bool   g_VSync    = false;        /** вертикальная синхронизация */
__gshared bool   g_ShowFog  = false;        /** отображать ли туман */
__gshared double g_DeltaTime = 0;           /** время между кадрами реальное */
__gshared double g_DeltaTimeScript = 0;     /** время между кадрами для скриптов */
__gshared double g_TimeForRunScript = 0;    /** задержка перед запуском скриптов */

__gshared wTexture* g_InvisibleTexture;
__gshared wMesh*    g_InvisibleMesh;
//extern wNode*    invisibleNode;

__gshared wSelector* g_WorldCollider;  /** коллайдер статичного мира - ВСЕ твёрдые объекты (включая стены) */
__gshared wNode* g_SolidObjects;       /** пустая нода, к которой припарентены все твердые внутриигровые объекты (только те, которыми можно управлять) */

/* флаги над теми номерами текстур, которые нужно грузить */
__gshared bool[] g_TexturesNeedForLoad;
__gshared bool[] g_SpritesNeedForLoad;
__gshared bool[] g_DoorsNeedForLoad;
__gshared bool[] g_KeysNeedForLoad;
__gshared bool[] g_WeaponItemsNeedForLoad;

/* мьютекс для обновления идентификатора объекта, обрабатываемого в данный момент */
__gshared Mutex g_MutexNodes;

/* участок для компиляции JS-кода */
__gshared uint[] g_ByteCode;

// инициализация запускатора - выставление глобальных настроек
void InitRunner();
// настройки материала для олдскульности
void MaterialSetOldSchool(wMaterial* material);

// пробегается по карте и заполняет g_TexturesNeedForLoad
void PrepareTexturesNeedForLoad(int numOfMap);
void PrepareSpritesNeedForLoad(int numOfMap);
void PrepareDoorsNeedForLoad(int numOfMap);
void PrepareKeysNeedForLoad(int numOfMap);
void PrepareWeaponsNeedForLoad(int numOfMap);
wMesh* CreateInvisibleBlock(const string name, float width, float height);


// IMPLEMENTATION


void InitRunner()
{
    if (g_LimitFps)
        wEngineSetFPS(MAX_FPS);

    //wLogSetLevel(wLL_DEBUG);
    ///Show logo WS3D
    //wEngineShowLogo(true);

    wSystemSetTextureCreationFlag(wTextureCreationFlag.wTCF_CREATE_MIP_MAPS,       true);
    wSystemSetTextureCreationFlag(wTextureCreationFlag.wTCF_ALLOW_NON_POWER_2,     false);
    wSystemSetTextureCreationFlag(wTextureCreationFlag.wTCF_OPTIMIZED_FOR_QUALITY, false);
    //wSystemSetTextureCreationFlag(wTextureCreationFlag.wTCF_OPTIMIZED_FOR_SPEED, true);
//    wSystemSetTextureCreationFlag(wTextureCreationFlag.wTCF_NO_ALPHA_CHANNEL,      true);

    if (g_Renderer == wDriverTypes.wDRT_SOFTWARE)
    {
        wSystemSetTextureCreationFlag(wTextureCreationFlag.wTCF_ALWAYS_16_BIT, true);
        wSystemSetTextureCreationFlag(wTextureCreationFlag.wTCF_NO_ALPHA_CHANNEL, true);
    }

    // запускаем интерпретатор
    //Duk_InitVM();

    // создаем мьютекс
    g_MutexNodes = new Mutex();

    /* создаем хранилища */
    g_TexturesNeedForLoad = new bool[TEXTURES_MAX_COUNT];
    g_SpritesNeedForLoad  = new bool[SPRITES_MAX_COUNT ];
    g_DoorsNeedForLoad    = new bool[DOORS_MAX_COUNT   ];
    g_KeysNeedForLoad     = new bool[KEYS_MAX_COUNT    ];
    g_WeaponItemsNeedForLoad = new bool[WEAPONS_MAX_COUNT];

    g_ByteCode = new uint[BYTECODE_MAX_LENGTH];

    /* инициализируем PhysFS */
    InitPhysFS();
}

void MaterialSetOldSchool(wMaterial* material)
{
    wMaterialSetAntiAliasingMode(material, wAntiAliasingMode.wAAM_OFF);
    wMaterialSetFlag(material, wMaterialFlags.wMF_LIGHTING,           false);
    wMaterialSetFlag(material, wMaterialFlags.wMF_ANISOTROPIC_FILTER, false);
    wMaterialSetFlag(material, wMaterialFlags.wMF_ANTI_ALIASING,      false);
    wMaterialSetFlag(material, wMaterialFlags.wMF_BILINEAR_FILTER,    false);
    wMaterialSetFlag(material, wMaterialFlags.wMF_TRILINEAR_FILTER,   false);
    wMaterialSetFlag(material, wMaterialFlags.wMF_TEXTURE_WRAP,       false);
    //wMaterialSetFlag(material, wMF_BACK_FACE_CULLING,                 false);
    wMaterialSetFlag(material, wMaterialFlags.wMF_FOG_ENABLE,         g_ShowFog);
}

void PrepareTexturesNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    //g_TexturesNeedForLoad = new bool[TEXTURES_MAX_COUNT];
    memset(g_TexturesNeedForLoad.ptr, 0, g_TexturesNeedForLoad.length * bool.sizeof);

    g_ShowFog = map.showFog;

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map.ceil[i][j];
            if ((element >  0) &&
                (element <= TEXTURES_MAX_COUNT))
                g_TexturesNeedForLoad[--element] = true;

            element = map.floor[i][j];
            if ((element >  0) &&
                (element <= TEXTURES_MAX_COUNT))
                g_TexturesNeedForLoad[--element] = true;

            element = map.level[i][j];
            if ((element >  0) &&
                (element <= TEXTURES_MAX_COUNT))
                g_TexturesNeedForLoad[--element] = true;
        }
    }
}

void PrepareSpritesNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    //g_SpritesNeedForLoad = new bool[SPRITES_MAX_COUNT];
    memset(g_SpritesNeedForLoad.ptr, 0, g_SpritesNeedForLoad.length * bool.sizeof);

    g_ShowFog = map.showFog;

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map.level[i][j] - TEXTURES_MAX_COUNT;
            if ((element >  0) &&
                (element <= SPRITES_MAX_COUNT))
                g_SpritesNeedForLoad[--element] = true;
        }
    }
}

void PrepareDoorsNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    //g_DoorsNeedForLoad = new bool[DOORS_MAX_COUNT];
    memset(g_DoorsNeedForLoad.ptr, 0, g_DoorsNeedForLoad.length * bool.sizeof);

    g_ShowFog = map.showFog;

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map.level[i][j] - TEXTURES_MAX_COUNT - SPRITES_MAX_COUNT;
            if ((element >  0) &&
                (element <= DOORS_MAX_COUNT))
                g_DoorsNeedForLoad[--element] = true;
        }
    }
}

void PrepareKeysNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    //g_KeysNeedForLoad = new bool[KEYS_MAX_COUNT];
    memset(g_KeysNeedForLoad.ptr, 0, g_KeysNeedForLoad.length * bool.sizeof);

    g_ShowFog = map.showFog;

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map.level[i][j] - TEXTURES_MAX_COUNT - SPRITES_MAX_COUNT - DOORS_MAX_COUNT;
            if ((element > 0) &&
                (element <= KEYS_MAX_COUNT))
                g_KeysNeedForLoad[--element] = true;
        }
    }
}

void PrepareWeaponsNeedForLoad(int numOfMap)
{
    if ((numOfMap < 0) || (numOfMap >= g_MapsCount))
        return;

    TMap* map = &(mapPack.maps[numOfMap]);
    memset(g_WeaponItemsNeedForLoad.ptr, 0, g_WeaponItemsNeedForLoad.length * bool.sizeof);

    int i, j;
    int element;
    for (i = 0; i < MAP_WIDTH; ++i)
    {
        for (j = 0; j < MAP_HEIGHT; ++j)
        {
            element = map.level[i][j] - TEXTURES_MAX_COUNT - SPRITES_MAX_COUNT - DOORS_MAX_COUNT - KEYS_MAX_COUNT;
            if ((element > 0) &&
                (element <= WEAPONS_MAX_COUNT))
                g_WeaponItemsNeedForLoad[--element] = true;
        }
    }
}


wMesh* CreateInvisibleBlock(const string name, float width, float height)
{
    // создаем невидимую текстуру, если она не создана ранее
    if (g_InvisibleTexture == null)
        g_InvisibleTexture = CreateColorTexture(wColor4s(0, 0, 0, 255), "Invisible Color");

    /*static wMesh* mesh;*/

    // создаем невидимый блок, если он не создан ранее
    if (g_InvisibleMesh == null)
    {
        float lt =  0.0    * BLOCK_SIZE;
        float rt =  width  * BLOCK_SIZE;
        float dn =  0.0    * BLOCK_SIZE;
        float up =  height * BLOCK_SIZE;
        float bk =  0.0    * BLOCK_SIZE;
        float fw = -width  * BLOCK_SIZE;

        wVert[] verts =
        [
            // forward
            { wVector3f( rt, dn, fw ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) },
            { wVector3f( lt, dn, fw ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) },
            { wVector3f( lt, up, fw ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) },
            { wVector3f( rt, up, fw ), wVECTOR3f_FORWARD,  wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) },

            // backward
            { wVector3f( lt, dn, bk ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) },
            { wVector3f( rt, dn, bk ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) },
            { wVector3f( rt, up, bk ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) },
            { wVector3f( lt, up, bk ), wVECTOR3f_BACKWARD, wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) },

            // right
            { wVector3f( rt, dn, bk ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) },
            { wVector3f( rt, dn, fw ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) },
            { wVector3f( rt, up, fw ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) },
            { wVector3f( rt, up, bk ), wVECTOR3f_RIGHT,    wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) },

            // left
            { wVector3f( lt, dn, fw ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 1.0 ) },
            { wVector3f( lt, dn, bk ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 1.0 ) },
            { wVector3f( lt, up, bk ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 0.0, 0.0 ) },
            { wVector3f( lt, up, fw ), wVECTOR3f_LEFT,     wCOLOR4s_WHITE, wVector2f( 1.0, 0.0 ) }
        ];
        ushort[] indices = [ 0,  1,  2,  0,  2,  3,
                             4,  5,  6,  4,  6,  7,
                             8,  9, 10,  8, 10, 11,
                            12, 13, 14, 12, 14, 15 ];

        g_InvisibleMesh = wMeshCreate(cast(char*)name.ptr);
        wMeshBuffer* meshBuffer = wMeshBufferCreate(verts.length, verts.ptr, indices.length, indices.ptr);

        wMaterial* material = wMeshBufferGetMaterial(meshBuffer);
        wMaterialSetTexture(material, 0, g_InvisibleTexture);

        MaterialSetOldSchool(material);
        wMaterialSetFlag(material, wMaterialFlags.wMF_FOG_ENABLE, false);
        wMaterialSetType(material, wMaterialTypes.wMT_TRANSPARENT_ALPHA_CHANNEL);

        wMeshAddMeshBuffer(g_InvisibleMesh, meshBuffer);
        wMeshEnableHardwareAcceleration(g_InvisibleMesh, 0);
    }

/*
        if (g_InvisibleMesh == null)
        {
            g_InvisibleMesh = wMeshCreateCube();

            wMeshBuffer* meshBuffer = wMeshGetBuffer(g_InvisibleMesh, 0, 0);
            if (meshBuffer != null)
            {
                wMaterial* material = wMeshBufferGetMaterial(meshBuffer);
                if (material != null)
                {
                    MaterialSetOldSchool(material);
                    wMaterialSetTexture(material, 0, g_InvisibleTexture);
                    wMaterialSetFlag(material, wMaterialFlags.wMF_FOG_ENABLE, false);
                    wMaterialSetType(material, wMaterialTypes.wMT_TRANSPARENT_ALPHA_CHANNEL);
                }
            }
        }
*/
    return g_InvisibleMesh;
}
