module weapons_ammo;

import std.stdio;
import std.conv;
import core.stdc.string;

import constants;
import global;
import res_manager;
import util;
import map;
import js_util;
import physfs_util;
import base_object;
import object_2d;
import object_3d;
import sprites;
import player;

import WorldSim3D;
import SampleFunctions;

extern (C):

struct TLaser
{
    wVector3f start, end;
    double timeForUnvis = 0.0;

    void show()
    {
        /* самоуничтожиться */
        if (timeForUnvis > 3.0)
        {
            if (lasers.length > 0)
            {
                bool founded = false;
                for (int i = 0; !founded && (i < lasers.length); ++i)
                    if (lasers[i] == this)
                    {
                        founded = true;
                        lasers[i] = lasers[$ - 1];
                    }
            }

            --lasers.length;
            return;
        }

        timeForUnvis += g_DeltaTimeScript;
        w3dDrawLine(start, end, wCOLOR4s_RED);
    }
}
TLaser[] lasers;

/** оружие подбирабельное - эталон, загруженное из пака */
struct TWeaponItemInMem
{
    string name;           // имя
    int    position;       // позиция в руках
    ubyte  animSpeed;      // кол-во кадров в секунду
    wTexture*[] frames;

    TScriptJS script;      // скрипт по умолчанию

    ~this()
    {
        clearFrames();
    }

    void clearFrames()
    {
        wTexture* txr;
        for (int i = 0; i < frames.length; ++i)
        {
            txr = frames[i];
            if (txr != null)
            {
                wTextureDestroy(txr);
            }
        }
        frames.length = 0;
    }
}

/** нода подбирабельного оружия, создаваемого на карте */
class TWeaponItemNode : TObject3D
{
    TWeaponItemInMem* index;    // индекс оружия (позиция в массиве g_WeaponItems)
    wNode*  billboard;          // нода биллборда

    // создание ноды
    this (TWeaponItemInMem* index, wNode* billboard)
    {
        wMaterial* material;

        addVar("name", index.name);
        addVar("type", "weaponItem");            // имя типа объекта
        addVar("weaponNumber", index.position + 1); // нумерация внутри скриптов с 1
        addVar("animationSpeed", index.animSpeed);

        this.index     = index;
        this.billboard = billboard;
        this.node      = wNodeCreateEmpty();

        this.script          = new TScriptJS();
        this.script.fileName = index.script.fileName.dup;
        this.script.text     = index.script.text.dup;
        this.script.setId();

        wVector3f pos = wNodeGetPosition(billboard);
        wNodeSetPosition(this.node, pos);

        // фиксация по вертикали
        wBillboardSetEnabledAxis(billboard, wBillboardAxisParam(false, true, false));
    }

    ~this()
    {
        //writeln("I am " ~ this.index.name);
        if (this.billboard != null)
        {
            wNodeDestroy(this.billboard);
        }
    }

    override void setFrame(int frame)
    {
        if (index == null || index.frames.length == 0)
            return;

        if (frame < 0 || frame >= index.frames.length)
            return;

        curFrame = frame;
    }

    override int getFrame()
    {
        return curFrame;
    }

    override int getFramesCount()
    {
        if (index == null)
            return 0;

        return index.frames.length;
    }

    override void setVisibility(bool isVisible)
    {
        if (this.billboard)
            wNodeSetVisibility(this.billboard, isVisible);

        super.setVisibility(isVisible);
    }

    override void update()
    {
        if (!this.isVisible)
            return;

        if (this.billboard == null)
            return;

        super.update();
    }
}
__gshared TWeaponItemInMem[] g_WeaponItems;
__gshared TWeaponItemNode[]  g_WeaponItemNodes;


/** пуля-эталон, загруженная из пака */
struct TBulletInMem
{
    string name;
    ubyte  animSpeed;      // кол-во кадров в секунду
    double speed;          // скорость движения
    double distance;       // макс. дистанция выстрела
    double lifeTime;       // время жизни снаряда
    float  solidX, solidY; // размеры ограничивающей коробки
    wTexture*[] frames;

    /* содержит текст скрипта */
    TScriptJS script;

    ~this()
    {
        clearFrames();
    }

    void clearFrames()
    {
        wTexture* txr;
        for (int i = 0; i < frames.length; ++i)
        {
            txr = frames[i];
            if (txr != null)
            {
                wTextureDestroy(txr);
            }
        }
        frames.length = 0;
    }
}

/** нода пули, создаваемая на карте */
class TBulletNode : TObject3D
{
    TBulletInMem* index;    // индекс спрайта (позиция в массиве bullets)
    wNode*  solidBox;       // ограничивающая коробка
    wNode*  billboard;      // нода биллборда

    // создание ноды
    this (TBulletInMem* index, wNode* solidBox, wNode* billboard)
    {
        wBillboardAxisParam axisParams;
        wMaterial* material;

        name = index.name;
        addVar("name",        index.name);          // имя
        addVar("type",        "bullet"  );          // имя типа
        addVar("speed",       index.speed);         // скорость движения пули
        addVar("distance",    index.distance);      // макс. дистанция выстрела
        addVar("lifeTime",    index.lifeTime);      // время жизни снаряда
        addVar("animationSpeed", index.animSpeed);

        this.index       = index;
        this.solidBox    = solidBox;
        this.billboard   = billboard;

        this.script          = new TScriptJS();
        this.script.fileName = index.script.fileName.dup;
        this.script.text     = index.script.text.dup;
        this.script.setId();

        if (this.solidBox)
        {
            // изменяем размеры коробки
            double solidX = index.solidX / 100.0;
            double solidY = index.solidY / 100.0;

            //writefln("solid of bullet is [%.3f, %.3f]", solidX, solidY);

            // меняем размер коробки с учетом масштаба
            wNodeSetScale(this.solidBox, wVector3f(solidX, solidY, solidX));

            // смешаем коробку в зависимости от размера коробки с учетом масштаба
            double dX = (1.0 - solidX) / 2.0;
            double dY = (1.0 - solidY) / 2.0;
            wNodeMove(this.solidBox, wVector3f(-dX * BLOCK_SIZE, -dY * BLOCK_SIZE, dX * BLOCK_SIZE));

            wMesh* mesh = wNodeGetMesh(this.solidBox);
            this.physBody = wCollisionCreateFromMesh(mesh, this.solidBox, 0);

            // помещаем биллборд в центр
            if (this.billboard)
            {
                wNodeSetParent(billboard, solidBox);
                wNodeMove(billboard, wVector3f(dX, dY, -dX));
            }

            this.node = solidBox;
        }
    }

    // уничтожить ноду
    ~this()
    {
        if (this.solidBox != null)
        {
            wMesh* mesh = wNodeGetMesh(this.solidBox);
            if (mesh != null)
                wMeshDestroy(mesh);
            wNodeDestroy(this.solidBox);
            this.solidBox = null;
        }

        if (this.billboard != null)
        {
            wNodeDestroy(this.billboard);
            this.billboard = null;
        }

        this.node = null;
    }

    override void setFrame(int frame)
    {
        if (index == null || index.frames.length == 0)
            return;

        if (frame < 0 || frame >= index.frames.length)
            return;

        curFrame = frame;

        if (billboard == null)
            return;

        wTexture* txr = this.index.frames[this.curFrame];
        if (txr == null)
            return;

        wMaterial* mat = wNodeGetMaterial(this.billboard, 0);
        wMaterialSetTexture(mat, 0, txr);
    }

    override int getFrame()
    {
        return curFrame;
    }

    override int getFramesCount()
    {
        if (index == null)
            return 0;

        return index.frames.length;
    }

    override void update()
    {
        if (this.billboard == null)
            return;

        super.update();
    }
}

__gshared TBulletInMem[] g_Bullets;
__gshared TBulletNode[]  g_BulletNodes;


/** информация об оружии в принципе */
class TWeapon : TObject2D
{
    bool   isAvailable;    // есть ли это оружие у игрока
    bool   isLoaded;       // загружен ли пакет текстур для оружия в руках

    override void update()
    {
        if (!this.isVisible)
            return;

        if (this.index == null)
            return;

        draw();
        super.update();
    }
}
__gshared TWeapon[] g_Weapons;  /** информация о всех оружиях у игрока */
__gshared int g_ActiveWeapon;   /** активное в текущий момент оружие   */





void InitWeapons();
void InitWeaponItems();
void LoadWeaponItemFrames(TWeaponItemInMem* weaponItem, int num);
bool LoadWeaponItemsPack();
bool LoadWeaponsPack();

void InitBullets();
void LoadBulletFrames(TBulletInMem* bullet, int num);
bool LoadBulletsPack();

void BulletNodesUpdate();
void WeaponItemNodesUpdate();
void WeaponUpdate();

void RegisterWeaponsAmmoJSFunctions();



// IMPLEMENTATION


void InitBullets()
{
    g_Bullets = new TBulletInMem[BULLETS_MAX_COUNT];
    //if (g_Bullets.length == 0)
    //    g_Bullets.length = BULLETS_MAX_COUNT;
    memset(g_Bullets.ptr, 0, g_Bullets.length * TBulletInMem.sizeof);
}

void InitWeapons()
{
    g_Weapons = new TWeapon[WEAPONS_MAX_COUNT];
    //if (g_Weapons.length == 0)
    //    g_Weapons.length = WEAPONS_MAX_COUNT;
    for (int i = 0; i < g_Weapons.length; ++i)
    {
        //if (g_Weapons[i])
        //    g_Weapons[i].destroy();
        g_Weapons[i] = new TWeapon();
    }

    g_ActiveWeapon = -1;
}


void InitWeaponItems()
{
    g_WeaponItems = new TWeaponItemInMem[WEAPONS_MAX_COUNT];
    //if (g_WeaponItems.length == 0)
    //    g_WeaponItems.length = BULLETS_MAX_COUNT;
    memset(g_WeaponItems.ptr, 0, g_WeaponItems.length * TWeaponItemInMem.sizeof);
}


void LoadBulletFrames(TBulletInMem* bullet, int num)
{
    if (bullet == null || num < 0 || num >= g_BulletsCount)
        return;

    ubyte[] buffer;
    int     bufferSize;
    string  fileName;
    string  bulletName;
    uint    framesCount;

    bullet.clearFrames();

    // читаем инфу по текстуре
    fileName = "bullet_" ~ to!string(num) ~ ".cfg\0";
    bufferSize = cast(int)FileSize(fileName);
    if (bufferSize <= 0)
    {
        PrintWithColor("can not get size of bullet_" ~ to!string(num) ~ ".cfg", wConsoleFontColor.wCFC_RED, false);
        return;
    }

    FileRead(fileName, buffer);
    Res_OpenIniFromBuffer(&(buffer[0]), bufferSize);
    framesCount = Res_IniReadInteger("Options", "frames_count", 0);
    Res_CloseIni();

    if (framesCount == 0)
        return;

    bullet.frames = new wTexture*[framesCount];

    // читаем кадры, если есть
    for (int i = 0; i < framesCount; ++i)
    {
        fileName   = "bullet_" ~ to!string(num) ~ "_frame_" ~ to!string(i) ~ ".dat\0";
        bulletName = "Bullet_" ~ to!string(num) ~ "_Frame_" ~ to!string(i);
        bullet.frames[i] = LoadTexture(fileName, bulletName);
    }
}

bool LoadBulletsPack()
{
    bool    isLoaded;
    TBulletInMem* bullet;

    ubyte[] buffer;
    int     bufferSize;
    string  fileName;
    string  bulletName;

    // читаем количество текстур
    FileRead("bullets.cfg", buffer);
    bufferSize = cast(int)FileSize("bullets.cfg");
    if (!Res_OpenIniFromBuffer(&(buffer[0]), bufferSize))
    {
        PrintWithColor("can not open bullets.cfg!!!\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    g_BulletsCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_BulletsCount == 0)
        goto end;

    InitBullets();

    for (int i = 0; i < g_BulletsCount; ++i)
    {
        // читаем инфу по текстуре
        fileName = "bullet_" ~ to!string(i) ~ ".cfg\0";
        bufferSize = cast(int)FileSize(fileName);
        if (bufferSize <= 0)
        {
            PrintWithColor("can not get size of bullet_" ~ to!string(i) ~ ".cfg", wConsoleFontColor.wCFC_RED, false);
            continue;
        }

        bullet = &(g_Bullets[i]);

        FileRead(fileName, buffer);
        Res_OpenIniFromBuffer(&(buffer[0]), bufferSize);
        with (bullet)
        {
            name        = to!string(Res_IniReadString("Options", "name", ("Bullet #" ~ to!string(i) ~ "\0").ptr));
            animSpeed   = cast(ubyte)Res_IniReadInteger("Options", "animation_speed", 0);
            //framesCount = Res_IniReadInteger("Options", "frames_count",    0);
            speed       = Res_IniReadDouble("Options", "speed",  1);
            distance    = Res_IniReadDouble("Options", "distance",  0);
            lifeTime    = Res_IniReadDouble("Options", "life_time", 0);
            solidX      = Res_IniReadDouble("Options", "solid_x",   100.0);
            solidY      = Res_IniReadDouble("Options", "solid_y",   100.0);

            script.loadFromFile("bullet.js");
        }
        Res_CloseIni();

        /*
        if (framesCount == 0)
            continue;

        bullet.frames = new wTexture*[framesCount];

        // читаем кадры, если есть
        for (j = 0; j < framesCount; ++j)
        {
            fileName   = "bullet_" ~ to!string(i) ~ "_frame_" ~ to!string(j) ~ ".dat\0";
            bulletName = "Bullet_" ~ to!string(i) ~ "_Frame_" ~ to!string(j);
            bullet.frames[j] = LoadTexture(fileName, bulletName);
        }
        */
    }
    isLoaded = true;

end:
    buffer.length = 0;

    return isLoaded;
}

void LoadWeaponItemFrames(TWeaponItemInMem* weaponItem, int num)
{
    if (weaponItem == null || num < 0 || num >= g_WeaponsCount)
        return;

    ubyte[] buffer;
    int     bufferSize;
    string  fileName;
    string  weaponName;
    uint    framesCount;

    weaponItem.clearFrames();

    // читаем инфу по текстуре
    fileName = "weapon_" ~ to!string(num) ~ ".cfg\0";
    bufferSize = cast(int)FileSize(fileName);
    if (bufferSize <= 0)
    {
        PrintWithColor("can not get size of weapon_" ~ to!string(num) ~ ".cfg", wConsoleFontColor.wCFC_RED, false);
        return;
    }

    FileRead(fileName, buffer);
    Res_OpenIniFromBuffer(&(buffer[0]), bufferSize);
    framesCount = Res_IniReadInteger("Options", "frames_count", 0);
    Res_CloseIni();

    if (framesCount == 0)
        return;

    // читаем кадры, если есть
    weaponItem.frames = new wTexture*[framesCount];
    for (int i = 0; i < framesCount; ++i)
    {
        fileName   = "weapon_" ~ to!string(num) ~ "_frame_" ~ to!string(i) ~ ".dat\0";
        weaponName = "WeaponItem_" ~ to!string(num) ~ "_Frame_" ~ to!string(i);
        weaponItem.frames[i] = LoadTexture(fileName, weaponName);
    }
}

bool LoadWeaponItemsPack()
{
    bool    isLoaded;
    TWeaponItemInMem* weaponItem;

    ubyte[] buffer;
    int     bufferSize;
    string  fileName;

    string  weaponName;
    int     i/*, j*/;

    string  name;
    ubyte   animSpeed;
    uint    framesCount;
    int     textureSize;

    // читаем количество текстур
    FileRead("weapons.cfg", buffer);
    bufferSize = cast(int)FileSize("weapons.cfg");
    if (!Res_OpenIniFromBuffer(&(buffer[0]), bufferSize))
    {
        PrintWithColor("can not open weapons.cfg!!!\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    g_WeaponsCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_WeaponsCount == 0)
        goto end;

    InitWeapons();
    InitWeaponItems();

    for (i = 0; i < g_WeaponsCount; ++i)
    {
        // читаем инфу по текстуре
        fileName = "weapon_" ~ to!string(i) ~ ".cfg\0";
        bufferSize = cast(int)FileSize(fileName);
        if (bufferSize <= 0)
        {
            PrintWithColor("can not get size of weapon_" ~ to!string(i) ~ ".cfg", wConsoleFontColor.wCFC_RED, false);
            continue;
        }

        weaponItem = &(g_WeaponItems[i]);

        FileRead(fileName, buffer);
        Res_OpenIniFromBuffer(&(buffer[0]), bufferSize);

        double damage, accuracy, distance;
        uint   maxAmmo;
        bool   infiniteAmmo;
        int    bulletId, fireType;

        name        = to!string(Res_IniReadString("Options", "name", ("Weapon #" ~ to!string(i) ~ "\0").ptr));
        damage      = Res_IniReadDouble ("Options", "damage",   0);
        maxAmmo     = Res_IniReadInteger("Options", "max_ammo", 0);
        infiniteAmmo = Res_IniReadBool  ("Options", "infinite_ammo", false);
        distance    = Res_IniReadDouble ("Options", "distance", 100);
        accuracy    = Res_IniReadDouble ("Options", "accuracy", 100);
        fireType    = Res_IniReadInteger("Options", "fire_type", 0);
        bulletId    = Res_IniReadInteger("Options", "bullet_id", 0);

        animSpeed   = cast(ubyte)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = Res_IniReadInteger("Options", "frames_count", 0);

        Res_CloseIni();

        // запоминаем настройки оружия
        TWeapon weapon = g_Weapons[i];
        if (weapon !is null)
        {
            weapon.name = name;
            weapon.addVar("name",     name    );
            weapon.addVar("type",     "weaponInHand");
            weapon.addVar("damage",   damage  );
            weapon.addVar("distance", distance);
            weapon.addVar("accuracy", accuracy);
            weapon.addVar("maxAmmo",  maxAmmo );
            weapon.addVar("infiniteAmmo", infiniteAmmo);
            weapon.addVar("fireType", fireType);
            // если тип - видимая пуля, то запомнить имя
            if (fireType == 1 && bulletId >= 0 && bulletId < g_BulletsCount)
                weapon.addVar("bulletName", g_Bullets[bulletId].name);
        }

        weaponItem.name      = name;
        weaponItem.position  = i;
        weaponItem.animSpeed = animSpeed;
        weaponItem.script.loadFromFile("weapon_item.js");

        // если не нужна для загрузки
        if (framesCount == 0 || !g_WeaponItemsNeedForLoad[i])
            continue;

        /*
        // читаем кадры, если есть
        weaponItem.frames = new wTexture*[framesCount];
        for (j = 0; j < framesCount; ++j)
        {
            fileName   = "weapon_" ~ to!string(i) ~ "_frame_" ~ to!string(j) ~ ".dat\0";
            weaponName = "WeaponItem_" ~ to!string(i) ~ "_Frame_" ~ to!string(j);
            weaponItem.frames[j] = LoadTexture(fileName, weaponName);
        }
        */
        LoadWeaponItemFrames(weaponItem, i);
    }
    isLoaded = true;

end:
    buffer.length = 0;

    return isLoaded;
}

bool LoadWeaponsPack()
{
    bool    isLoaded;
    TWeapon weapon;
    wTexture* txr;

    //ushort  w, h;
    ubyte[] buffer;
    int     bufferSize;
    string  fileName;

    string  weaponName;
    int     i, j;

    string  name;
    ubyte   animSpeed;
    uint    framesCount;
    int     textureSize;
    ubyte   weaponAlign;

    for (i = 0; i < g_WeaponsCount; ++i)
    {
        // читаем инфу по текстуре
        fileName = "weapon_inhand_" ~ to!string(i) ~ ".cfg\0";
        //bufferSize = Res_GetUncompressedFileSize(fileName.ptr);
        bufferSize = cast(int)FileSize(fileName);
        if (bufferSize <= 0)
        {
            //PrintWithColor("can not get size of weapon_inhand_" ~ to!string(i) ~ ".cfg", wConsoleFontColor.wCFC_RED, false);
            continue;
        }

        weapon = g_Weapons[i];
        if (weapon is null)
            continue;

        weapon.init();

        /*
        buffer.length = bufferSize;
        Res_ReadFileFromZipToBuffer(fileName.ptr, &(buffer[0]), bufferSize);
        */
        FileRead(fileName, buffer);
        Res_OpenIniFromBuffer(&(buffer[0]), bufferSize);

        int fireFrame;
        //TFireType fireType;

        fireFrame   = Res_IniReadInteger ("Options", "fire_frame",                0);
        weaponAlign = cast(ubyte)Res_IniReadInteger("Options", "align",           0);
        //fireType    = cast(TFireType)Res_IniReadInteger("Options", "fire_type",   0);
        animSpeed   = cast(ubyte)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = cast(ubyte)Res_IniReadInteger("Options", "frames_count",    0);
        Res_CloseIni();

        //weapon.fireType  = fireType;
        weapon.addVar("align", weaponAlign);
        weapon.addVar("fireFrame", fireFrame);
        weapon.addVar("animationSpeed", animSpeed);
        if (framesCount == 0)
            continue;

        // читаем кадры, если есть
        ++g_Images2D.length;
        TImage2D* image = &(g_Images2D[$ - 1]);
        image.frames = new wTexture*[framesCount];
        weapon.index = image;
        for (j = 0; j < framesCount; ++j)
        {
            fileName   = "weapon_inhand_" ~ to!string(i) ~ "_frame_" ~ to!string(j) ~ ".dat\0";
            weaponName = "WeaponInHand_" ~ to!string(i) ~ "_Frame_" ~ to!string(j);
            txr = LoadTexture(fileName, weaponName);
            if (txr != null)
            {
                //weapon.frames[j] = txr;
                image.frames[j] = txr;
                weapon.isLoaded = true;     // есть текстуры
                if (weapon.curTexture == null)
                    weapon.curTexture = txr;
            }
        }
    }
    isLoaded = true;

end:
    buffer.length = 0;

    return isLoaded;
}

void BulletNodesUpdate()
{
    if (g_BulletNodes.length == 0)
        return;

    TBulletNode bulletNode;
    for (int i = 0; i < g_BulletNodes.length; ++i)
    {
        bulletNode = g_BulletNodes[i];

        if (bulletNode !is null)
        {
            bulletNode.update();
        }
    }
}

void WeaponItemNodesUpdate()
{
    if (g_WeaponItemNodes.length == 0)
        return;

    TWeaponItemNode weaponItem;
    for (int i = 0; i < g_WeaponItemNodes.length; ++i)
    {
        weaponItem = g_WeaponItemNodes[i];

        if (weaponItem !is null)
        {
            weaponItem.update();
        }
    }
}

void WeaponUpdate()
{
    if (g_ActiveWeapon < 0 || g_ActiveWeapon >= WEAPONS_MAX_COUNT)
        return;

    TWeapon weapon = g_Weapons[g_ActiveWeapon];
    if (!weapon.isAvailable || !weapon.isLoaded || weapon.index == null/* || weapon.frames.length == 0*/)
        return;

    weapon.update();
}


/**
 *  JerryScript
 **/


/** работа с объектами */
static jerry_value_t js_weaponSetAvailable
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 2)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];
    jerry_value_t arg1 = args_p[1];

    if (!jerry_value_is_number(arg0) || !jerry_value_is_boolean(arg1))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos > WEAPONS_MAX_COUNT)
        return jerry_create_undefined();

    bool isAvailable = (jerry_get_boolean_value(arg1) != 0);
    g_Weapons[pos - 1].isAvailable = isAvailable;  // -1, потому что снаружи нумерация 1..10

    return jerry_create_undefined();
}
static jerry_value_t js_weaponIsAvailable
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_boolean(0);

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_boolean(0);

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos > WEAPONS_MAX_COUNT)
        return jerry_create_boolean(0);

    return jerry_create_boolean(g_Weapons[pos - 1].isAvailable);        // -1, потому что снаружи нумерация 1..10
}
static jerry_value_t js_weaponSetActive
                 (const jerry_value_t  func_obj_val, /**< function object */
                  const jerry_value_t  this_p,       /**< this arg */
                  const jerry_value_t* args_p,       /**< function arguments */
                  const jerry_length_t args_cnt)     /**< number of function arguments */
{
    if (args_cnt < 1)
        return jerry_create_undefined();

    jerry_value_t arg0 = args_p[0];

    if (!jerry_value_is_number(arg0))
        return jerry_create_undefined();

    uint pos = to!uint(jerry_get_number_value(arg0));
    if (pos > WEAPONS_MAX_COUNT)
        return jerry_create_undefined();

    --pos;       // -1, потому что снаружи нумерация 1..10
    if (g_Weapons[pos].isAvailable)
        g_ActiveWeapon = pos;

    //writefln("g_ActiveWeapon = %d", g_ActiveWeapon);

    return jerry_create_undefined();
}
/*
static jerry_value_t js_weaponRayHit
                 (const jerry_value_t  func_obj_val,
                  const jerry_value_t  this_p,
                  const jerry_value_t* args_p,
                  const jerry_length_t args_cnt)
{
    if (g_ActiveWeapon < 0 || g_ActiveWeapon >= WEAPONS_MAX_COUNT)
        return jerry_create_number(0);

    if (g_Weapons[g_ActiveWeapon].isAvailable)
    {
        int id = g_Weapons[g_ActiveWeapon].rayHit();
        return jerry_create_number(id);
    }

    return jerry_create_number(0);
}
*/

void RegisterWeaponsAmmoJSFunctions()
{
    JS_RegisterCFunction(&js_weaponSetAvailable, "weaponSetAvailable");
    JS_RegisterCFunction(&js_weaponIsAvailable,  "weaponIsAvailable" );
    JS_RegisterCFunction(&js_weaponSetActive,    "weaponSetActive"   );
//    JS_RegisterCFunction(&js_weaponRayHit,       "weaponRayHit"      );
}
