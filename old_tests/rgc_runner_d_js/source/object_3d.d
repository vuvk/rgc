module object_3d;

import constants;
import global;
import base_object;

import WorldSim3D;

extern (C):

class TObject3D : TObject
{
    wVector3f  pivotOffset;       /* смещение опорной точки */
    wNode*     node;
    wSelector* physBody;
    protected {
        bool isCollisionAdded;      /* добавлена ли коллизия к коллайдеру мира */
    }

    ~this()
    {
        if (this.physBody && isCollisionAdded)
        {
            if (g_WorldCollider != null)
                wCollisionGroupRemoveCollision(g_WorldCollider, this.physBody);
        }

        if (this.node)
        {
            //wMesh* mesh = wNodeGetMesh(this.node);
            //if (mesh != null)
            //    wMeshDestroy(mesh);
            wNodeDestroy(this.node);
        }
    }

    /* установить видимость объекта */
    override void setVisibility(bool isVisible)
    {
        super.setVisibility(isVisible);
        if (this.node)
            wNodeSetVisibility(this.node, isVisible);
    }

    /* управление коллизионной коробкой объекта */
    override void enableCollision()
    {
        if (g_WorldCollider == null || physBody == null)
            return;

        if (isCollisionAdded)
            return;

        wCollisionGroupAddCollision(g_WorldCollider, physBody);
        isCollisionAdded = true;
    }

    override void disableCollision()
    {
        if (g_WorldCollider == null || physBody == null)
            return;

        if (!isCollisionAdded)
            return;

        wCollisionGroupRemoveCollision(g_WorldCollider, physBody);
        isCollisionAdded = false;
    }

    override bool isEnabledCollision()
    {
        return isCollisionAdded;
    }
}
