module sprites;

import std.stdio;
import std.conv;
import core.stdc.string;

import constants;
import global;
import res_manager;
import util;
import js_util;
import physfs_util;
import base_object;

import WorldSim3D;
import SampleFunctions;


extern (C):

/** спрайт-эталон, загруженный из пака */
struct TSpriteInMem
{
    string name;
    ubyte  axisParams;     // поворот по осям: 0 = смотрит на игрока, 1 = фиксация по горизонтали, 2 = фиксация по вертикали
    bool   collision;      // можно столкнуться
    bool   destroyable;    // разрушаемый?
    ubyte  endurance;      // живучесть
    ubyte  animSpeed;      // кол-во кадров в секунду
    float  scaleX, scaleY; // масштабирование
    float  solidX, solidY; // размеры ограничивающей коробки
    wTexture*[] frames;

    TScriptJS script;

    ~this()
    {
        wTexture* txr;
        for (int i = 0; i < frames.length; ++i)
        {
            txr = frames[i];
            if (txr != null)
            {
                wTextureDestroy(txr);
            }
        }
        frames.length = 0;
    }
}

/** нода спрайта, создаваемая на карте */
class TSpriteNode : TObject3D
{
    TSpriteInMem* index;    // индекс спрайта (позиция в массиве sprites)
    wNode*  solidBox;       // ограничивающая коробка, если спрайт твердый
    wNode*  billboard;      // нода биллборда

    // создание ноды
    this (TSpriteInMem* index, wNode* solidBox, wNode* billboard)
    {
        wBillboardAxisParam axisParams;
        wMaterial* material;

        name = index.name;
        addVar("name",        index.name);          // имя
        addVar("type",        "sprite"  );          // имя типа
        addVar("destroyable", index.destroyable);   // можно ли разрушить
        addVar("health",      index.endurance);     // текущее значение жизней
        addVar("animationSpeed", index.animSpeed);

        this.index       = index;
        //this.animSpeed   = index.animSpeed;
        this.solidBox    = solidBox;
        this.billboard   = billboard;

        this.script          = new TScriptJS();
        this.script.fileName = index.script.fileName.dup;
        this.script.text     = index.script.text.dup;
        this.script.setId();

        double scaleX = index.scaleX / 100.0;
        double scaleY = index.scaleY / 100.0;

        if (this.billboard != null)
            wBillBoardSetSize(this.billboard, wVector2f(scaleX * BLOCK_SIZE, scaleY * BLOCK_SIZE));
        double dX;
        double dY = (1.0 - scaleY) / 2.0;
        //double dY = (index.scaleY / 100.0) / 2.0;
        if (this.billboard != null)
            wNodeMove(this.billboard, wVector3f(0.0, -dY * BLOCK_SIZE, 0.0));

        if (this.solidBox)
        {
            // изменяем размеры коробки
            double solidX = index.solidX / 100.0;
            double solidY = index.solidY / 100.0;

            // смешаем коробку в зависимости от размера коробки с учетом масштаба
            dX = (1.0 - scaleX * solidX) / 2;
            wNodeMove(this.solidBox, wVector3f(dX * BLOCK_SIZE, 0.0, -dX * BLOCK_SIZE));

            // меняем размер коробки с учетом масштаба
            dX = scaleX * solidX;
            dY = scaleY * solidY;
            wNodeSetScale(this.solidBox, wVector3f(dX, dY, dX));

            wMesh* mesh = wNodeGetMesh(this.solidBox);
            this.physBody = wCollisionCreateFromMesh(mesh, this.solidBox, 0);

            this.isCollisionAdded = true;
        }

        // устанавливаем параметры поворота спрайта
        switch (index.axisParams)
        {
            // смотреть на игрока
            default:
            case 0 :
                axisParams.isEnablePitch = false;
                axisParams.isEnableYaw   = true;
                axisParams.isEnableRoll  = false;
                break;

            // фиксация по горизонтали (XoY)
            case 1 :
                axisParams.isEnablePitch = true;
                axisParams.isEnableYaw   = true;
                axisParams.isEnableRoll  = false;
                if (billboard != null)
                {
                    material = wNodeGetMaterial(billboard, 0);
                    wMaterialSetFlag(material, wMaterialFlags.wMF_BACK_FACE_CULLING, false);
                }
                break;

            // фиксация по вертикали (ZoY)
            case 2 :
                axisParams.isEnablePitch = false;
                axisParams.isEnableYaw   = true;
                axisParams.isEnableRoll  = true;
                if (billboard != null)
                {
                    material = wNodeGetMaterial(billboard, 0);
                    wMaterialSetFlag(material, wMaterialFlags.wMF_BACK_FACE_CULLING, false);
                }
                break;
        }
        if (billboard != null)
            wBillboardSetEnabledAxis(billboard, axisParams);
    }

    // уничтожить ноду
    ~this()
    {
        if (this.solidBox != null)
        {
            wMesh* mesh = wNodeGetMesh(this.solidBox);
            if (mesh != null)
                wMeshDestroy(mesh);
            wNodeDestroy(this.solidBox);
        }

        if (this.billboard != null)
        {
            wNodeDestroy(this.billboard);
        }
    }

    override void setFrame(int frame)
    {
        if (index == null || index.frames.length == 0)
            return;

        if (frame < 0 || frame >= index.frames.length)
            return;

        curFrame = frame;

        if (billboard == null)
            return;

        wTexture* txr = this.index.frames[this.curFrame];
        if (txr == null)
            return;

        wMaterial* mat = wNodeGetMaterial(this.billboard, 0);
        wMaterialSetTexture(mat, 0, txr);
    }

    override int getFrame()
    {
        return curFrame;
    }

    override int getFramesCount()
    {
        if (index == null)
            return 0;

        return index.frames.length;
    }

    override void update()
    {
        if (this.billboard == null)
            return;

        super.update();
    }
}

__gshared TSpriteInMem[] g_Sprites;
__gshared TSpriteNode[]  g_SpriteNodes;

void InitSprites();
bool LoadSpritesPack();
void SpriteNodesUpdate();


// IMPLEMENTATION


void InitSprites()
{
    //g_Sprites = new TSpriteInMem[SPRITES_MAX_COUNT];
    if (g_Sprites.length == 0)
        g_Sprites.length = SPRITES_MAX_COUNT;
    memset(g_Sprites.ptr, 0, g_Sprites.length * TSpriteInMem.sizeof);
}

bool LoadSpritesPack()
{
    bool    isLoaded;
    TSpriteInMem* sprite;

    //ushort  w, h;
    ubyte[] buffer;
    int     bufferSize;
    string  fileName;

    string  name;
    string  spriteName;
    int     i, j;
    ubyte   animSpeed;
    uint    framesCount;
    ubyte   axisParams;
    bool    collision;
    bool    destroyable;
    ubyte   endurance;
    float   scaleX, scaleY;
    float   solidX, solidY;
    //int     textureSize;

    // читаем количество текстур
    /*
    bufferSize = Res_GetUncompressedFileSize("sprites.cfg\0");
    if (bufferSize <= 0)
    {
        PrintWithColor("can not get size of sprites.cfg\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    buffer.length = bufferSize;
    if (!Res_ReadFileFromZipToBuffer("sprites.cfg", &(buffer[0]), bufferSize))
    {
        PrintWithColor("can not read sprites.cfg\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    */
    FileRead("sprites.cfg", buffer);
    bufferSize = cast(int)FileSize("sprites.cfg");
    if (!Res_OpenIniFromBuffer(&(buffer[0]), bufferSize))
    {
        PrintWithColor("can not open sprites.cfg!!!\n", wConsoleFontColor.wCFC_RED, false);
        goto end;
    }
    g_SpritesCount = Res_IniReadInteger("Options", "count", 0);
    Res_CloseIni();
    if (g_SpritesCount == 0)
        goto end;

    InitSprites();

    for (i = 0; i < g_SpritesCount; ++i)
    {
        // если не нужна для загрузки
        if (!g_SpritesNeedForLoad[i])
            continue;

        // читаем инфу по текстуре
        fileName = "sprite_" ~ to!string(i) ~ ".cfg\0";
        //bufferSize = Res_GetUncompressedFileSize(fileName.ptr);
        bufferSize = cast(int)FileSize(fileName);
        if (bufferSize <= 0)
        {
            PrintWithColor("can not get size of sprite_" ~ to!string(i) ~ ".cfg", wConsoleFontColor.wCFC_RED, false);
            continue;
        }

        sprite = &(g_Sprites[i]);

        /*
        buffer.length = bufferSize;
        Res_ReadFileFromZipToBuffer(fileName.ptr, &(buffer[0]), bufferSize);
        */
        FileRead(fileName, buffer);
        Res_OpenIniFromBuffer(&(buffer[0]), bufferSize);
        name        = to!string(Res_IniReadString("Options", "name", ("Sprite #" ~ to!string(i) ~ "\0").ptr));
        animSpeed   = cast(ubyte)Res_IniReadInteger("Options", "animation_speed", 0);
        framesCount = cast(ubyte)Res_IniReadInteger("Options", "frames_count",    0);
        axisParams  = cast(ubyte)Res_IniReadInteger("Options", "axis",            0);
        collision   = Res_IniReadBool   ("Options", "collision",       false);
        destroyable = Res_IniReadBool   ("Options", "destroyable",     false);
        endurance   = cast(ubyte)Res_IniReadInteger("Options", "endurance",       0);
        scaleX      = Res_IniReadDouble("Options", "scale_x", 100.0);
        scaleY      = Res_IniReadDouble("Options", "scale_y", 100.0);
        solidX      = Res_IniReadDouble("Options", "solid_x", 100.0);
        solidY      = Res_IniReadDouble("Options", "solid_y", 100.0);
        Res_CloseIni();

        sprite.name        = name;
        sprite.animSpeed   = animSpeed;
        sprite.axisParams  = axisParams;
        sprite.collision   = collision;
        sprite.destroyable = destroyable;
        sprite.endurance   = endurance;
        sprite.scaleX      = scaleX;
        sprite.scaleY      = scaleY;
        sprite.solidX      = solidX;
        sprite.solidY      = solidY;
        //sprite.script.loadFromFile("assets/scripts/sprites/sprite.js");
        sprite.script.loadFromFile("sprite.js");
        if (framesCount == 0)
            continue;

        sprite.frames = new wTexture*[framesCount];

        // читаем кадры, если есть
        for (j = 0; j < framesCount; ++j)
        {
            fileName   = "sprite_" ~ to!string(i) ~ "_frame_" ~ to!string(j) ~ ".dat\0";
            spriteName = "Sprite_" ~ to!string(i) ~ "_Frame_" ~ to!string(j);
            sprite.frames[j] = LoadTexture(fileName, spriteName);
        }
    }
    isLoaded = true;

end:
    buffer.length = 0;

    return isLoaded;
}

void SpriteNodesUpdate()
{
    if (g_SpriteNodes.length == 0)
        return;

    foreach (sprite; g_SpriteNodes)
    {
        if (sprite !is null)
        {
            sprite.update();
        }
    }
}
