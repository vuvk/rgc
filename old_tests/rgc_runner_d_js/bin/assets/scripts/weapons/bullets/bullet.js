
var bulletId, bulletName;
var pos;

var animSpeed, animDelay;
var frame, maxFrame;

var maxLifeTime, lifeTime;
var maxDistance, distance;

var direction;
var moveSpeed;


/**
 *  Initialization of object
 */
this.onInit = function() {
	lifeTime = 0.0;
	distance = 0.0;
	animDelay = 0.0;	
	frame = 0;
	
	bulletId = objectGetId();
	bulletName = objectGetVar(bulletId, "name");
	pos = new Vector3f(objectGetPosition(bulletId));
	direction = new Vector3f(objectGetVar(bulletId, "direction"));
	moveSpeed = objectGetVar(bulletId, "speed");
	maxLifeTime = objectGetVar(bulletId, "lifeTime");
	maxDistance = objectGetVar(bulletId, "distance");
	
//	print('maxlife = ' + maxLifeTime);
//	print('maxDistance = ' + maxDistance);
	
	animSpeed = objectGetVar(bulletId, "animationSpeed");
	maxFrame = objectGetFramesCount(bulletId) - 1;
	objectSetFrame(bulletId, frame);
}


/**
 *  Update event
 */
this.onUpdate = function() {	
	if (g_PauseState || Level.deltaTime == 0.0)
		return;
	
	var speed = Level.deltaTime * moveSpeed;
	distance += speed;
	lifeTime += Level.deltaTime;
	
	var moveDir = direction.mul(speed);
	objectMove(bulletId, moveDir.x, moveDir.y, moveDir.z);
	
	if (distance >= maxDistance || (lifeTime >= maxLifeTime && maxLifeTime > 0)) {
		objectDestroy(bulletId);
	}
}


/**
 *  Destroy event
 */
this.onDestroy = function() {
}