
var weaponId, weaponName, weaponNumber;
var pos;

var animSpeed, animDelay;
var frame, maxFrame;


/**
 *  Initialization of object
 */
this.onInit = function() {
	weaponId = objectGetId();
	weaponName = objectGetVar(weaponId, "name");
	weaponNumber = objectGetVar(weaponId, "weaponNumber");
	pos = new Vector3f(objectGetPosition(weaponId));
	
	animSpeed = objectGetVar(weaponId, "animationSpeed");
	animDelay = 0.0;
	
	frame = 0;
	maxFrame = objectGetFramesCount(weaponId) - 1;
	objectSetFrame(weaponId, frame);	
}


/**
 *  Update event
 */
this.onUpdate = function() {
	if (g_PauseState || Level.deltaTime == 0.0)
		return;
	
	if (distanceBetweenPoints(pos.x, pos.z, Player.pos.x, Player.pos.z) <= 0.2) {		
		print("Now I have a '" + weaponName + "' - " + weaponNumber + " weapon!");
		
		if (!weaponIsAvailable(weaponNumber)) {
			weaponSetAvailable(weaponNumber, true);
			weaponSetActive(weaponNumber);
		}
		
		objectDestroy(weaponId);
	}
		
	/* update animation */
	if (animSpeed == 0.0)
		return;
	
	if (animDelay < 1.0)
		animDelay += Level.deltaTime * animSpeed;
	else {
		animDelay -= 1.0;
		++frame;

		if (frame > maxFrame) 
			frame = 0;
		
		objectSetFrame(weaponId, frame);
	}
}


/**
 *  Destroy
 */
this.onDestroy = function() {
}