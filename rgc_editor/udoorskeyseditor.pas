unit uDoorsKeysEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  FileUtil,
  Forms,
  Controls,
  Graphics,
  GraphType,
  Dialogs,
  ComCtrls,
  StdCtrls,
  Spin,
  ExtCtrls,
  Grids,
  Buttons,
  Types,
  Imaging,
  ImagingTypes, uPleaseWait,
  Math,
  LCLType;

type

  { TfrmDoorsKeysEditor }

  TfrmDoorsKeysEditor = class(TForm)
    AddDoorFrameBtn: TSpeedButton;
    AddKeyFrameBtn: TSpeedButton;
    AddDoorTextureBtn: TSpeedButton;
    AddKeyTextureBtn: TSpeedButton;
    Animator: TTimer;
    AutosizeBtn: TSpeedButton;
    DoorClearMoveSoundBtn: TSpeedButton;
    DoorClearStartSoundBtn: TSpeedButton;
    KeyClearPickSoundBtn: TSpeedButton;
    DoorClearStopSoundBtn: TSpeedButton;
    DoorMoveSoundsCmbBox: TComboBox;
    DoorStartSoundsCmbBox: TComboBox;
    KeyPickSoundsCmbBox: TComboBox;
    DoorStopSoundsCmbBox: TComboBox;
    DeleteDoorColorBtn: TSpeedButton;
    DeleteDoorColorShp: TShape;
    DeleteKeyColorBtn: TSpeedButton;
    DeleteKeyColorShp: TShape;
    DoorAnimSpeedSpin: TSpinEdit;
    KeyAnimSpeedSpin: TSpinEdit;
    ClearKeyFrameBtn: TSpeedButton;
    ClearKeyTextureBtn: TSpeedButton;
    DeleteKeyFrameBtn: TSpeedButton;
    DeleteRigBtn: TSpeedButton;
    DeleteKeyTextureBtn: TSpeedButton;
    KeyNameEdit: TEdit;
    KeysGrid: TDrawGrid;
    KeysGroupBox: TGroupBox;
    KeyFramesGrid: TDrawGrid;
    KeyFramesGroupBox: TGroupBox;
    KeyFramePlayBtn: TBitBtn;
    GroupBox1: TGroupBox;
    GroupBox3: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DoorNameEdit: TEdit;
    Label5: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    LoadKeyFrameBtn: TSpeedButton;
    LoadKeyTextureBtn: TSpeedButton;
    KeyParamsGroupBox: TGroupBox;
    PreviewKeyImage: TImage;
    PreviewRibImage: TImage;
    Label2: TLabel;
    LoadRibBtn: TSpeedButton;
    Panel1: TPanel;
    KeysEditGroupBox: TGroupBox;
    ClearKeyIdBtn: TSpeedButton;
    KeySolidXSpin: TFloatSpinEdit;
    KeySolidYSpin: TFloatSpinEdit;
    StretchKeyImageCheck: TCheckBox;
    WidthSpin: TFloatSpinEdit;
    StayOpenedChk: TCheckBox;
    NeedKeyChk: TCheckBox;
    ClearDoorFrameBtn: TSpeedButton;
    ClearDoorTextureBtn: TSpeedButton;
    DeleteDoorFrameBtn: TSpeedButton;
    DeleteDoorTextureBtn: TSpeedButton;
    NeedKeyMsgEdit: TEdit;
    DoorFramesGrid: TDrawGrid;
    DoorFramesGroupBox: TGroupBox;
    DoorFramePlayBtn: TBitBtn;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label13: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    LoadDoorFrameBtn: TSpeedButton;
    LoadDoorTextureBtn: TSpeedButton;
    PageControl1: TPageControl;
    DoorParamsGroupBox: TGroupBox;
    PreviewDoorImage: TImage;
    KeyIdCmbBox: TComboBox;
    DoorsEditGroupBox: TGroupBox;
    DoorsGrid: TDrawGrid;
    DoorsGroupBox: TGroupBox;
    StretchDoorImageCheck: TCheckBox;
    DoorsTab: TTabSheet;
    KeysTab: TTabSheet;
    OpenSpeedSpin: TFloatSpinEdit;
    procedure AddDoorFrameBtnClick(Sender: TObject);
    procedure AddDoorTextureBtnClick(Sender: TObject);
    procedure AddKeyFrameBtnClick(Sender: TObject);
    procedure AddKeyTextureBtnClick(Sender: TObject);
    procedure AnimatorTimer(Sender: TObject);
    procedure AutosizeBtnClick(Sender: TObject);
    procedure ClearKeyFrameBtnClick(Sender: TObject);
    procedure ClearKeyIdBtnClick(Sender: TObject);
    procedure ClearKeyTextureBtnClick(Sender: TObject);
    procedure DeleteDoorColorBtnClick(Sender: TObject);
    procedure DeleteKeyColorBtnClick(Sender: TObject);
    procedure DeleteKeyFrameBtnClick(Sender: TObject);
    procedure DeleteKeyTextureBtnClick(Sender: TObject);
    procedure DoorAnimSpeedSpinChange(Sender: TObject);
    procedure ClearDoorFrameBtnClick(Sender: TObject);
    procedure ClearDoorTextureBtnClick(Sender: TObject);
    procedure DeleteDoorFrameBtnClick(Sender: TObject);
    procedure DeleteRigBtnClick(Sender: TObject);
    procedure DeleteDoorTextureBtnClick(Sender: TObject);
    procedure DoorClearMoveSoundBtnClick(Sender: TObject);
    procedure DoorClearStartSoundBtnClick(Sender: TObject);
    procedure DoorClearStopSoundBtnClick(Sender: TObject);
    procedure DoorMoveSoundsCmbBoxChange(Sender: TObject);
    procedure DoorNameEditChange(Sender: TObject);
    procedure DoorsGridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure DoorsGridSelection(Sender: TObject; aCol, aRow: Integer);
    procedure DoorsTabContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure DoorStartSoundsCmbBoxChange(Sender: TObject);
    procedure DoorStopSoundsCmbBoxChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure DoorFramesGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure DoorFramesGridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure DoorFramePlayBtnClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure KeyAnimSpeedSpinChange(Sender: TObject);
    procedure KeyClearPickSoundBtnClick(Sender: TObject);
    procedure KeyFramePlayBtnClick(Sender: TObject);
    procedure KeyFramesGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure KeyFramesGridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure KeyIdCmbBoxChange(Sender: TObject);
    procedure KeyNameEditChange(Sender: TObject);
    procedure KeyPickSoundsCmbBoxChange(Sender: TObject);
    procedure KeysGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure KeysGridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure KeysGridSelection(Sender: TObject; aCol, aRow: Integer);
    procedure KeysGroupBoxKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure KeySolidXSpinChange(Sender: TObject);
    procedure KeySolidYSpinChange(Sender: TObject);
    procedure LoadDoorFrameBtnClick(Sender: TObject);
    procedure LoadKeyFrameBtnClick(Sender: TObject);
    procedure LoadKeyTextureBtnClick(Sender: TObject);
    procedure LoadRibBtnClick(Sender: TObject);
    procedure LoadDoorTextureBtnClick(Sender: TObject);
    procedure NeedKeyChkChange(Sender: TObject);
    procedure NeedKeyMsgEditChange(Sender: TObject);
    procedure OpenSpeedSpinChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure PreviewDoorImageClick(Sender: TObject);
    procedure PreviewDoorImageMouseEnter(Sender: TObject);
    procedure PreviewDoorImageMouseLeave(Sender: TObject);
    procedure PreviewDoorImageMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PreviewKeyImageClick(Sender: TObject);
    procedure PreviewKeyImageMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PreviewKeyImageMouseEnter(Sender: TObject);
    procedure PreviewKeyImageMouseLeave(Sender: TObject);
    procedure PreviewKeyImageMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PreviewKeyImageMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PreviewKeyImagePaint(Sender: TObject);
    procedure RotationRadioGroupClick(Sender: TObject);
    procedure DoorsGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure StayOpenedChkChange(Sender: TObject);
    procedure StretchDoorImageCheckChange(Sender: TObject);
    procedure StretchDoorImageCheckSizeConstraintsChange(Sender: TObject);
    procedure StretchKeyImageCheckChange(Sender: TObject);
    procedure TabSheet3ContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure WidthSpinChange(Sender: TObject);
  private       
    texNum : Integer;
    frmTexNum : Integer;
    rawImage : TRawImage;
    deleteColorPos : TPoint;    // позиция на реальном изображении для взятия цвета под удаление
    deleteColorMode : Boolean;  // режим удаления цвета?
    canInput : Boolean;         // можно ли вводить параметры
    canChooseSolid : Boolean;   // можно ли указать ограничивающую область мышкой

    procedure FormPrepare;
    procedure AnimatorDisable;
    procedure AnimatorEnable;  
    procedure DeleteColorModeEnable;
    procedure DeleteColorModeDisable;
    procedure DoorsGridUpdate;
    procedure DoorFramesGridUpdate;
    procedure NewRibImage;
    procedure KeysGridUpdate;
    procedure KeyFramesGridUpdate;
    procedure KeyListUpdate;


  public

  end;

implementation

{$R *.lfm}

uses
  uDM,
  uContainer,
  uUtil;

{ TfrmDoorsKeysEditor }

procedure TfrmDoorsKeysEditor.RotationRadioGroupClick(Sender: TObject);
begin

end;

procedure TfrmDoorsKeysEditor.FormCreate(Sender: TObject);
begin             
  PageControl1.ActivePageIndex := 0;

  DoubleBuffered := True;
  PreviewDoorImage.Parent.DoubleBuffered := True;   
  PreviewKeyImage.Parent.DoubleBuffered  := True;
                
  FormPrepare;
  KeysGridUpdate;
  DoorsGridUpdate;
end;

procedure TfrmDoorsKeysEditor.DoorFramesGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin    
  if (g_DoorsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (doorsInMem[texNum]) do
    begin
      if (framesCount = 0) then exit;
      if (pos >= framesCount) then exit;
      bmp := frames[pos];
    end;

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmDoorsKeysEditor.DoorFramesGridSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  AnimatorDisable;      
  DeleteColorModeDisable;

  PreviewDoorImage.Picture.Clear;

  LoadDoorFrameBtn.Enabled   := False;
  ClearDoorFrameBtn.Enabled  := False;
  DeleteDoorFrameBtn.Enabled := False;

  frmTexNum := 0;
  if ((texNum < 0) or
      (texNum >= Length(doorsInMem)) or
      (doorsInMem[texNum].framesCount = 0)) then
    exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (doorsInMem[texNum]) do
    begin
      if (pos >= framesCount) then exit;

      frmTexNum := pos;
      if (pos > 0) then
        begin
          LoadDoorFrameBtn.Enabled   := True;
          ClearDoorFrameBtn.Enabled  := True;
          DeleteDoorFrameBtn.Enabled := True;
        end;
      bmp := frames[pos];
      if (bmp = nil) then exit;

      LoadPreviewImageFromBitmap(PreviewDoorImage, bmp);
    end;
end;

procedure TfrmDoorsKeysEditor.DoorFramePlayBtnClick(Sender: TObject);
begin
  if (g_DoorsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (doorsInMem[texNum].framesCount = 0) then exit;

  if (Animator.Enabled) then
    begin
      AnimatorDisable;  
      DeleteColorModeDisable;
      if (texNum < g_DoorsCount) then
        with (doorsInMem[texNum]) do
          if (frmTexNum < framesCount) then
            LoadPreviewImageFromBitmap(PreviewDoorImage, frames[frmTexNum]);
    end
  else
    AnimatorEnable;
end;

procedure TfrmDoorsKeysEditor.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then Close;
end;

procedure TfrmDoorsKeysEditor.FormShow(Sender: TObject);
begin
  DoorsGrid.SetFocus;
end;

procedure TfrmDoorsKeysEditor.KeyAnimSpeedSpinChange(Sender: TObject);
begin
  if (g_KeysCount = 0) then exit;

  keysInMem[texNum].animSpeed := KeyAnimSpeedSpin.Value;
  if (KeyAnimSpeedSpin.Value <> 0) then
    Animator.Interval := 1000 div KeyAnimSpeedSpin.Value
  else
    Animator.Interval := 0;
end;

procedure TfrmDoorsKeysEditor.KeyClearPickSoundBtnClick(Sender: TObject);
begin
  if (g_KeysCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_KeysCount)) then exit;
  if (not canInput) then exit;

  KeyPickSoundsCmbBox.ItemIndex := -1;
  keysInMem[texNum].pickUpSoundId := -1;
end;

procedure TfrmDoorsKeysEditor.KeyFramePlayBtnClick(Sender: TObject);
begin     
  if (g_KeysCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_KeysCount)) then exit;
  if (keysInMem[texNum].framesCount = 0) then exit;

  if (Animator.Enabled) then
    begin
      AnimatorDisable;  
      DeleteColorModeDisable;
      if (texNum < g_KeysCount) then
        with (keysInMem[texNum]) do
          if (frmTexNum < framesCount) then
            LoadPreviewImageFromBitmap(PreviewKeyImage, frames[frmTexNum]);
    end
  else
    AnimatorEnable;
end;

procedure TfrmDoorsKeysEditor.KeyFramesGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin             
  if (g_KeysCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_KeysCount)) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (keysInMem[texNum]) do
    begin
      if (framesCount = 0) then exit;
      if (pos >= framesCount) then exit;
      bmp := frames[pos];
    end;

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmDoorsKeysEditor.KeyFramesGridSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  AnimatorDisable; 
  DeleteColorModeDisable;

  PreviewKeyImage.Picture.Clear;

  LoadKeyFrameBtn.Enabled   := False;
  ClearKeyFrameBtn.Enabled  := False;
  DeleteKeyFrameBtn.Enabled := False;

  frmTexNum := 0;   
  if ((texNum < 0) or
      (texNum >= Length(keysInMem)) or
      (keysInMem[texNum].framesCount = 0)) then
    exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (keysInMem[texNum]) do
    begin
      if (pos >= framesCount) then exit;

      frmTexNum := pos;
      if (pos > 0) then
        begin
          LoadKeyFrameBtn.Enabled   := True;
          ClearKeyFrameBtn.Enabled  := True;
          DeleteKeyFrameBtn.Enabled := True;
        end;
      bmp := frames[pos];
      if (bmp = nil) then exit;

      LoadPreviewImageFromBitmap(PreviewKeyImage, bmp);
    end;
end;

procedure TfrmDoorsKeysEditor.KeyIdCmbBoxChange(Sender: TObject);
begin
  if (g_DoorsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  doorsInMem[texNum].keyId := KeyIdCmbBox.ItemIndex;
end;

procedure TfrmDoorsKeysEditor.KeyNameEditChange(Sender: TObject);
begin   
  with (KeyNameEdit) do
    if (Text = '') then
      Color := clRed
    else
      Color := clWhite;

  if (g_KeysCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_KeysCount)) then exit;
  if (not canInput) then exit;

  keysInMem[texNum].name := KeyNameEdit.Text;

  // меняем имя в списке ключей
  KeyIdCmbBox.Items[texNum] := KeyNameEdit.Text;
end;

procedure TfrmDoorsKeysEditor.KeyPickSoundsCmbBoxChange(Sender: TObject);
begin
  if (g_KeysCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_KeysCount)) then exit;
  if (not canInput) then exit;

  keysInMem[texNum].pickUpSoundId := KeyPickSoundsCmbBox.ItemIndex;
end;

procedure TfrmDoorsKeysEditor.KeysGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_KeysCount = 0) then exit;
  //if ((texNum < 0) or (texNum >= g_KeysCount)) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (keysInMem[pos].framesCount = 0) then exit;
  bmp := keysInMem[pos].frames[0];

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmDoorsKeysEditor.KeysGridSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  FormPrepare;

  if (g_KeysCount = 0) then exit;

  LoadKeyTextureBtn.Enabled   := True;
  ClearKeyTextureBtn.Enabled  := True;
  DeleteKeyTextureBtn.Enabled := True;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  texNum := pos;
  frmTexNum := 0;
  if (keysInMem[pos].framesCount = 0) then exit;
  bmp := keysInMem[pos].frames[0];
  if (bmp = nil) then exit;

  LoadPreviewImageFromBitmap(PreviewKeyImage, bmp);
  KeyParamsGroupBox.Enabled := True;
  KeyFramesGroupBox.Enabled := True;

  with (keysInMem[pos]) do
    begin
      KeyNameEdit.Text       := name;
      KeyPickSoundsCmbBox.ItemIndex := pickUpSoundId;
      KeyAnimSpeedSpin.Value := animSpeed;
      KeySolidXSpin.Value    := solidX;   
      KeySolidYSpin.Value    := solidY;
    end;

  canInput := True;
  KeyFramesGridUpdate; 
  KeysGridSelection(Sender, aCol, aRow);
end;

procedure TfrmDoorsKeysEditor.KeysGridSelection(Sender: TObject; aCol,
  aRow: Integer);
begin
  DoorsGrid.Options := [goAlwaysShowEditor, goSmoothScroll];
  KeysGrid.Options  := [goAlwaysShowEditor, goSmoothScroll, goDrawFocusSelected];
end;

procedure TfrmDoorsKeysEditor.KeysGroupBoxKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

end;

procedure TfrmDoorsKeysEditor.KeySolidXSpinChange(Sender: TObject);
begin      
  if (g_KeysCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_KeysCount)) then exit;
  if (not canInput) then exit;

  keysInMem[texNum].solidX := KeySolidXSpin.Value;
  PreviewKeyImage.Invalidate;
end;

procedure TfrmDoorsKeysEditor.KeySolidYSpinChange(Sender: TObject);
begin
  if (g_KeysCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_KeysCount)) then exit;
  if (not canInput) then exit;

  keysInMem[texNum].solidY := KeySolidYSpin.Value;
  PreviewKeyImage.Invalidate;
end;

procedure TfrmDoorsKeysEditor.LoadDoorFrameBtnClick(Sender: TObject);
var
  image : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (DM.OpenTextureDialog.Execute) then
    begin
      PrepareLoadTexture(image, TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

      // записываем изображение в поток
      stream := TMemoryStream.Create;
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image);  
      FreeImage(image);

      stream.Position := 0;
      with (doorsInMem[texNum]) do
        begin
          if (frames[frmTexNum] = nil) then
            frames[frmTexNum] := TBitmap.Create;
          frames[frmTexNum].LoadFromStream(stream);
        end;
      stream.Destroy;

      DoorFramesGridUpdate;
      DoorFramesGridSelectCell(DoorFramesGrid, 0, frmTexNum, canSelect);
      GridSellectRow(DoorFramesGrid, frmTexNum);
    end;
end;

procedure TfrmDoorsKeysEditor.LoadKeyFrameBtnClick(Sender: TObject);
var
  image : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (DM.OpenTextureDialog.Execute) then
    begin
      PrepareLoadTexture(image, TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

      // записываем изображение в поток
      stream := TMemoryStream.Create;
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image);  
      FreeImage(image);

      stream.Position := 0;
      with (keysInMem[texNum]) do
        begin
          if (frames[frmTexNum] = nil) then
            frames[frmTexNum] := TBitmap.Create;
          frames[frmTexNum].LoadFromStream(stream);
        end;
      stream.Destroy;

      KeyFramesGridUpdate;
      KeyFramesGridSelectCell(KeyFramesGrid, 0, frmTexNum, canSelect);
      GridSellectRow(KeyFramesGrid, frmTexNum);
    end;
end;

procedure TfrmDoorsKeysEditor.LoadKeyTextureBtnClick(Sender: TObject);
var
  i : Integer;
  image  : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;
  filesCount : Integer; 
  frmPleaseWait : TfrmPleaseWait;
begin
  AnimatorDisable;  
  DeleteColorModeDisable;

  if ((keysInMem[texNum].framesCount > 0) and
      (MessageDlg('Warning!',
                  'Do you really want to load new texture for key? It''s event clear all old frames!',
                  mtWarning,
                  [mbYes, mbNo],
                  0) = mrNo)) then
     exit;
                                                 
  frmPleaseWait := TfrmPleaseWait.Create(Self);
  DM.OpenTextureDialog.Options := g_OpenOptionsMulti;

  if (DM.OpenTextureDialog.Execute) then
    begin          
      frmPleaseWait.Show;
      frmPleaseWait.Repaint;

      InitKey(texNum);
                        
      filesCount := DM.OpenTextureDialog.Files.Count;

      keysInMem[texNum].framesCount := filesCount;
      keysInMem[texNum].name := 'Key #' + IntToStr(texNum);
      SetLength(keysInMem[texNum].frames, filesCount);
                                     
      stream := TMemoryStream.Create;
      for i := 0 to filesCount - 1 do
        begin
          keysInMem[texNum].frames[i] := TBitmap.Create;

          PrepareLoadTexture(image, DM.OpenTextureDialog.Files[i], TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

          // записываем изображение в поток
          stream.Position := 0;
          SaveImageToStream('bmp', stream, image);
          FreeImage(image);

          stream.Position := 0;
          keysInMem[texNum].frames[i].LoadFromStream(stream);  
          stream.Clear;
        end;       
      stream.Destroy;

      KeysGridUpdate;
      KeysGridSelectCell(KeysGrid, 0, texNum, canSelect);
      GridSellectRow(KeysGrid, texNum);
    end;

  DM.OpenTextureDialog.Options := g_OpenOptionsOnce;
  frmPleaseWait.Destroy;
end;

procedure TfrmDoorsKeysEditor.LoadRibBtnClick(Sender: TObject);
var
  image : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;
begin
  AnimatorDisable;      
  DeleteColorModeDisable;

  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  if (DM.OpenTextureDialog.Execute) then
    begin
      PrepareLoadTexture(image, TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

      // записываем изображение в поток
      stream := TMemoryStream.Create;
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image);

      stream.Position := 0;
      with (doorsInMem[texNum]) do
        begin
          NewRibImage;
          rib.LoadFromStream(stream);

          // загружаем превью
          LoadPreviewImageFromBitmap(PreviewRibImage, rib);
        end;

      FreeImage(image);
      stream.Destroy;
    end;
end;

procedure TfrmDoorsKeysEditor.LoadDoorTextureBtnClick(Sender: TObject);
var              
  i : Integer;
  image  : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;   
  filesCount : Integer;
  frmPleaseWait : TfrmPleaseWait;
begin
  AnimatorDisable;      
  DeleteColorModeDisable;

  if ((doorsInMem[texNum].framesCount > 0) and
      (MessageDlg('Warning!',
                  'Do you really want to load new texture for door? It''s event clear all old frames!',
                  mtWarning,
                  [mbYes, mbNo],
                  0) = mrNo)) then
     exit;

  DM.OpenTextureDialog.Options := g_OpenOptionsMulti;
  frmPleaseWait := TfrmPleaseWait.Create(Self);

  if (DM.OpenTextureDialog.Execute) then
    begin              
      frmPleaseWait.Show;
      frmPleaseWait.Repaint;

      InitDoor(texNum);
                          
      filesCount := DM.OpenTextureDialog.Files.Count;

      doorsInMem[texNum].name := 'Door #' + IntToStr(texNum);
      doorsInMem[texNum].framesCount := filesCount;
      SetLength(doorsInMem[texNum].frames, filesCount);
                                
      stream := TMemoryStream.Create;
      for i := 0 to filesCount - 1 do
        begin
          doorsInMem[texNum].frames[i] := TBitmap.Create;
                                                
          PrepareLoadTexture(image, DM.OpenTextureDialog.Files[i], TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

          // записываем изображение в поток
          stream.Position := 0;
          SaveImageToStream('bmp', stream, image);  
          FreeImage(image);

          stream.Position := 0;
          doorsInMem[texNum].frames[i].LoadFromStream(stream);  
          stream.Clear;
        end;
      stream.Destroy;

      // создаем пустую текстуру для ребра
      NewRibImage;

      DoorsGridUpdate;   
      DoorsGridSelectCell(DoorsGrid, 0, texNum, canSelect);
      GridSellectRow(DoorsGrid, texNum);
    end;

  frmPleaseWait.Destroy;
  DM.OpenTextureDialog.Options := g_OpenOptionsOnce;
end;

procedure TfrmDoorsKeysEditor.NeedKeyChkChange(Sender: TObject);
begin
  if (g_DoorsCount = 0) then exit;     
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  doorsInMem[texNum].needKey := NeedKeyChk.Checked;
end;

procedure TfrmDoorsKeysEditor.NeedKeyMsgEditChange(Sender: TObject);
begin
  if (g_DoorsCount = 0) then exit;     
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  doorsInMem[texNum].needKeyMsg := NeedKeyMsgEdit.Text;
end;

procedure TfrmDoorsKeysEditor.OpenSpeedSpinChange(Sender: TObject);
begin
  if (g_DoorsCount = 0) then exit;  
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  doorsInMem[texNum].openSpeed := OpenSpeedSpin.Value;
end;

procedure TfrmDoorsKeysEditor.PageControl1Change(Sender: TObject);
begin
  FormPrepare; 
  //DoorsGridUpdate;
  //KeysGridUpdate;
end;

procedure TfrmDoorsKeysEditor.PreviewDoorImageClick(Sender: TObject);
var
  i : Integer;
  image : TImageData;
  bmp : TBitmap = nil;
  stream : TMemoryStream = nil;
  clr : TColor32Rec;             // цвет для удаления
  canSelect : Boolean;
begin
  if (g_DoorsCount = 0) then exit;
  if (not deleteColorMode) then exit;

  with (doorsInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      stream := TMemoryStream.Create;
      frames[frmTexNum].SaveToStream(stream);

      stream.Position := 0;
      FreeImage(image);
      LoadImageFromStream(stream, image);
      stream.Clear;

      // берем цвет для удаления и удаляем его в памяти
      clr := (PColor32Rec(image.Bits) + image.Width * deleteColorPos.y + deleteColorPos.x)^;
      for i := 0 to (image.Size div SizeOf(clr)) - 1 do
        begin
          if ((PColor32Rec(image.Bits) + i)^.Color = clr.Color) then
            (PColor32Rec(image.Bits) + i)^.A := 0;
        end;

      // сохраняем картинку из памяти в превью
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image);
      FreeImage(image);

      stream.Position := 0;
      bmp := TBitmap.Create;
      bmp.LoadFromStream(stream, stream.Size);
      stream.Destroy;

      LoadPreviewImageFromBitmap(PreviewDoorImage, bmp);
      bmp.Destroy;

      // Если принять изменения, то сохранить фрейм из превью
      if (MessageDlg('Confirm', 'Do you want to save the current changes?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
        begin
          stream := TMemoryStream.Create;
          PreviewDoorImage.Picture.SaveToStream(stream);

          stream.Position := 0;
          frames[frmTexNum].Clear;
          frames[frmTexNum].LoadFromStream(stream);

          stream.Destroy;
        end;
    end;

  DoorFramesGridUpdate;
  DoorFramesGridSelectCell(DoorFramesGrid, 0, frmTexNum, canSelect);
end;

procedure TfrmDoorsKeysEditor.PreviewDoorImageMouseEnter(Sender: TObject);
begin
  if (deleteColorMode) then
    begin
      DeleteDoorColorShp.Visible := True;
      Label8.Visible := True;
    end;
end;

procedure TfrmDoorsKeysEditor.PreviewDoorImageMouseLeave(Sender: TObject);
begin
  DeleteDoorColorShp.Visible := False;
  Label8.Visible := False;
end;

procedure TfrmDoorsKeysEditor.PreviewDoorImageMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  _x, _y : Integer;
  coeffX, coeffY : Single;    // поправочные коэффициенты для взятия пикселя (в превью может быть растянуто)
  clr : TColor;
begin
  if (deleteColorMode) then
    begin
      DeleteDoorColorShp.Brush.Color := clDefault;

      // высчитываем координаты пикселя, цвет которого будем удалять
      coeffX := PreviewDoorImage.Picture.Width  / PreviewDoorImage.Width;
      coeffY := PreviewDoorImage.Picture.Height / PreviewDoorImage.Height;
      _x := round(X * coeffX);
      _y := round(Y * coeffY);
      if (_x > PreviewDoorImage.Picture.Width ) then _x := PreviewDoorImage.Picture.Width;
      if (_y > PreviewDoorImage.Picture.Height) then _y := PreviewDoorImage.Picture.Height;

      clr := (PInt32(PreviewDoorImage.Picture.Bitmap.RawImage.Data) + PreviewDoorImage.Picture.Width * deleteColorPos.y + deleteColorPos.x)^;
      if ((clr shr 24) <> 0) then
        DeleteDoorColorShp.Brush.Color := PreviewDoorImage.Picture.Bitmap.Canvas.Pixels[_x, _y];;

      deleteColorPos.x := _x;
      deleteColorPos.y := _y;
    end;
end;

procedure TfrmDoorsKeysEditor.PreviewKeyImageClick(Sender: TObject);
var
  i : Integer;
  image : TImageData;
  bmp : TBitmap = nil;
  stream : TMemoryStream = nil;
  clr : TColor32Rec;             // цвет для удаления
  canSelect : Boolean;
begin
  if (g_KeysCount = 0) then exit;
  if (not deleteColorMode) then exit;

  with (keysInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      stream := TMemoryStream.Create;
      frames[frmTexNum].SaveToStream(stream);

      stream.Position := 0;
      FreeImage(image);
      LoadImageFromStream(stream, image);
      stream.Clear;

      // берем цвет для удаления и удаляем его в памяти
      clr := (PColor32Rec(image.Bits) + image.Width * deleteColorPos.y + deleteColorPos.x)^;
      for i := 0 to (image.Size div SizeOf(clr)) - 1 do
        begin
          if ((PColor32Rec(image.Bits) + i)^.Color = clr.Color) then
            (PColor32Rec(image.Bits) + i)^.A := 0;
        end;

      // сохраняем картинку из памяти в превью
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image);
      FreeImage(image);

      stream.Position := 0;
      bmp := TBitmap.Create;
      bmp.LoadFromStream(stream, stream.Size);
      stream.Destroy;

      LoadPreviewImageFromBitmap(PreviewKeyImage, bmp);
      bmp.Destroy;

      // Если принять изменения, то сохранить фрейм из превью
      if (MessageDlg('Confirm', 'Do you want to save the current changes?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
        begin
          stream := TMemoryStream.Create;
          PreviewKeyImage.Picture.SaveToStream(stream);

          stream.Position := 0;
          frames[frmTexNum].Clear;
          frames[frmTexNum].LoadFromStream(stream);

          stream.Destroy;
        end;
    end;

  KeyFramesGridUpdate;
  KeyFramesGridSelectCell(KeyFramesGrid, 0, frmTexNum, canSelect);
end;

procedure TfrmDoorsKeysEditor.PreviewKeyImageMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (not deleteColorMode) then
    canChooseSolid := True;
end;

procedure TfrmDoorsKeysEditor.PreviewKeyImageMouseEnter(Sender: TObject);
begin
  if (deleteColorMode) then
    begin                  
      DeleteKeyColorShp.Visible := True;
      Label3.Visible := True;
    end;
end;

procedure TfrmDoorsKeysEditor.PreviewKeyImageMouseLeave(Sender: TObject);
begin
  DeleteKeyColorShp.Visible := False;
  Label3.Visible := False;     
  canChooseSolid := False;
  PreviewKeyImage.Invalidate;
end;

procedure TfrmDoorsKeysEditor.PreviewKeyImageMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  _x, _y : Integer;
  coeffX, coeffY : Single;    // поправочные коэффициенты для взятия пикселя (в превью может быть растянуто)
  clr : TColor;
begin     
  // высчитываем координаты пикселя, цвет которого будем удалять
  if (not StretchKeyImageCheck.Checked) then
    begin
      _x := X;
      _y := Y;
    end
  else
    begin
      coeffX := PreviewKeyImage.Picture.Width  / PreviewKeyImage.Width;
      coeffY := PreviewKeyImage.Picture.Height / PreviewKeyImage.Height;
      _x := round(X * coeffX);
      _y := round(Y * coeffY);
    end;
  if (_x > PreviewKeyImage.Picture.Width ) then _x := PreviewKeyImage.Picture.Width;
  if (_y > PreviewKeyImage.Picture.Height) then _y := PreviewKeyImage.Picture.Height;

  if (deleteColorMode) then
    begin
      DeleteKeyColorShp.Brush.Color := clDefault;

      clr := (PInt32(PreviewKeyImage.Picture.Bitmap.RawImage.Data) + PreviewKeyImage.Picture.Width * deleteColorPos.y + deleteColorPos.x)^;
      if ((clr shr 24) <> 0) then
        DeleteKeyColorShp.Brush.Color := PreviewKeyImage.Picture.Bitmap.Canvas.Pixels[_x, _y];;

      deleteColorPos.x := _x;
      deleteColorPos.y := _y;
    end
  else
    if (canChooseSolid) then
      begin
        if (_x > (PreviewKeyImage.Picture.Width shr 1)) then
          _x := PreviewKeyImage.Picture.Width - _x;

        KeySolidXSpin.Value := ((PreviewKeyImage.Picture.Width  - _x shl 1) / PreviewKeyImage.Picture.Width ) * 100.0;
        KeySolidYSpin.Value := ((PreviewKeyImage.Picture.Height - _y      ) / PreviewKeyImage.Picture.Height) * 100.0;

        PreviewKeyImage.Invalidate;
      end;
end;

procedure TfrmDoorsKeysEditor.PreviewKeyImageMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (not deleteColorMode) then
    canChooseSolid := False;
  PreviewKeyImage.Invalidate;
end;

procedure TfrmDoorsKeysEditor.PreviewKeyImagePaint(Sender: TObject);
var
  x0, x1, y0, y1 : Integer;
  xCoeff, yCoeff : Single;    // соотношение реального размера к растянутому в превью
  w, h : Integer;
  sW, sH : Integer;
begin
  if (g_KeysCount = 0) then exit;
  with (keysInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[0] = nil)) then exit;

      w := PreviewKeyImage.Picture.Width;
      h := PreviewKeyImage.Picture.Height;

      sW := ceil(w * (solidX / 100.0));
      sH := ceil(h * (solidY / 100.0));

      x0 := (w - sW) shr 1;
      x1 := w - x0;
      y0 := h - sH;
      y1 := h;

      if (StretchKeyImageCheck.Checked) then
        begin
          xCoeff := PreviewKeyImage.Width  / w;
          yCoeff := PreviewKeyImage.Height / h;

          x0 := ceil(x0 * xCoeff);
          x1 := ceil(x1 * xCoeff);
          y0 := ceil(y0 * yCoeff);
          y1 := ceil(y1 * yCoeff);
        end
      else
        begin
          xCoeff := 1.0;
          yCoeff := 1.0;
        end;

      if (x0 < 0) then x0 := 0;
      if (y0 < 0) then y0 := 0;
      if (x1 >= PreviewKeyImage.Width ) then x1 := PreviewKeyImage.Width  - 1;
      if (y1 >= PreviewKeyImage.Height) then y1 := PreviewKeyImage.Height - 1;

      with (PreviewKeyImage.Canvas) do
        begin
          Pen.Color := clRed;
          Line(x0, y0, x1, y0);
          Line(x1, y0, x1, y1);
          Line(x0, y1, x1, y1);
          Line(x0, y0, x0, y1);
          // перекрестие
          if (canChooseSolid) then
            begin
              Line(x0, y0, x1, y1);
              Line(x1, y0, x0, y1);
            end;

          Pen.Color   := clLime;
          Brush.Color := clLime;
          Rectangle(x0 - 3, y0 - 3, x0 + 3, y0 + 3);
          Rectangle(x0 - 3, y1 - 3, x0 + 3, y1 + 3);
          Rectangle(x1 - 3, y0 - 3, x1 + 3, y0 + 3);
          Rectangle(x1 - 3, y1 - 3, x1 + 3, y1 + 3);

          // рамка рисунка
          if (not StretchKeyImageCheck.Checked) then
            begin
              Pen.Color := clGray;
              Line(0, 0, w, 0);
              Line(w, 0, w, h);
              Line(0, h, w, h);
              Line(0, 0, 0, h);
            end;
        end;
    end;
end;

procedure TfrmDoorsKeysEditor.DoorsGridSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  FormPrepare;

  if (g_DoorsCount = 0) then exit;

  LoadDoorTextureBtn.Enabled   := True;
  ClearDoorTextureBtn.Enabled  := True;
  DeleteDoorTextureBtn.Enabled := True;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  texNum := pos;
  frmTexNum := 0;
  if (doorsInMem[pos].framesCount = 0) then exit;
  bmp := doorsInMem[pos].frames[0];
  if (bmp = nil) then exit;

  LoadPreviewImageFromBitmap(PreviewDoorImage, bmp);
  DoorParamsGroupBox.Enabled := True;
  DoorFramesGroupBox.Enabled := True;

  with (doorsInMem[pos]) do
    begin
      DoorNameEdit.Text      := name;
      OpenSpeedSpin.Value    := openSpeed;
      StayOpenedChk.Checked  := stayOpened;
      NeedKeyChk.Checked     := needKey;
      NeedKeyMsgEdit.Text    := needKeyMsg;
      if ((KeyIdCmbBox.Items.Count = 0) and (g_KeysCount > 0)) then
        KeyListUpdate;
      KeyIdCmbBox.ItemIndex  := keyId;
      DoorStartSoundsCmbBox.ItemIndex:= startSoundId;
      DoorMoveSoundsCmbBox.ItemIndex := moveSoundId;
      DoorStopSoundsCmbBox.ItemIndex := stopSoundId;
      WidthSpin.Value        := width;
      if (rib = nil) then
        NewRibImage;
      LoadPreviewImageFromBitmap(PreviewRibImage, rib);
      DoorAnimSpeedSpin.Value    := animSpeed;
    end;
            
  canInput := True;
  DoorFramesGridUpdate;
  DoorsGridSelection(Sender, aCol, aRow);
end;

procedure TfrmDoorsKeysEditor.DoorsGridSelection(Sender: TObject; aCol,
  aRow: Integer);
begin
  DoorsGrid.Options := [goAlwaysShowEditor, goSmoothScroll, goDrawFocusSelected];
  KeysGrid.Options  := [goAlwaysShowEditor, goSmoothScroll];
end;

procedure TfrmDoorsKeysEditor.DoorsTabContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
end;

procedure TfrmDoorsKeysEditor.DoorStartSoundsCmbBoxChange(Sender: TObject);
begin
  if (g_DoorsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  doorsInMem[texNum].startSoundId := DoorStartSoundsCmbBox.ItemIndex;
end;

procedure TfrmDoorsKeysEditor.DoorStopSoundsCmbBoxChange(Sender: TObject);
begin
  if (g_DoorsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  doorsInMem[texNum].stopSoundId := DoorStopSoundsCmbBox.ItemIndex;
end;

procedure TfrmDoorsKeysEditor.FormActivate(Sender: TObject);
var
  canSelect : Boolean;
begin
  if (PageControl1.ActivePageIndex = 0) then
    begin
      if (DoorsGrid.ColCount > 0) and
         (DoorsGrid.RowCount > 0) then
        DoorsGridSelectCell(DoorsGrid, 0, 0, canSelect);
      DoorsGrid.SetFocus;
    end
  else
    begin
      if (KeysGrid.ColCount > 0) and
         (KeysGrid.RowCount > 0) then
        KeysGridSelectCell(KeysGrid, 0, 0, canSelect); 
      KeysGrid.SetFocus;
    end;
end;

procedure TfrmDoorsKeysEditor.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  SaveDoorKeyPack;
end;

procedure TfrmDoorsKeysEditor.DoorNameEditChange(Sender: TObject);
begin    
  with (DoorNameEdit) do
    if (Text = '') then
      Color := clRed
    else
      Color := clWhite;

  if (g_DoorsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  doorsInMem[texNum].name := DoorNameEdit.Text;
end;

procedure TfrmDoorsKeysEditor.DeleteDoorTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
  i : Integer;
  x, y, m : Integer;
  cell : Integer;
  fromPos, toPos : Integer;
begin
  AnimatorDisable;      
  DeleteColorModeDisable;

  if (g_DoorsCount = 0) then exit;

  if (MessageDlg('Warning!',
                 'Do you really want to delete texture of door? It''s event clear all frames!',
                 mtWarning,
                 [mbYes, mbNo],
                 0) = mrNo) then
     exit;

  // удаляем файлы
  DeleteFile(g_DkPath + '/door_' + IntToStr(texNum) + '.cfg');
  DeleteFile(g_DkPath + '/door_' + IntToStr(texNum) + '_rib.dat');
  for i := 0 to High(doorsInMem[texNum].frames) do
    DeleteFile(g_DkPath + '/door_' + IntToStr(texNum) + '_frame_' + IntToStr(i) + '.dat');

  InitDoor(texNum);

  // удаляем на картах элементы, которые использовали удаленную текстуру
  //CheckMaps;
  if (texNum < g_DoorsCount) then
    begin
      // сдвигаем назад все элементы на карте
      if (texNum >= 0) then
        begin
          fromPos := TEXTURES_MAX_COUNT + SPRITES_MAX_COUNT;
          toPos   := fromPos + DOORS_MAX_COUNT;

          for m := 0 to g_MapsCount - 1 do
            with (mapPack.maps[m]) do
              for x := 0 to MAP_WIDTH  - 1 do
                for y := 0 to MAP_HEIGHT - 1 do
                  begin
                    cell := level[x, y] - 1;
                    if ((cell >= fromPos) and (cell < toPos)) then
                      begin
                        if (cell > texNum + fromPos) then
                          level[x, y] := cell
                        else
                          level[x, y] := 0;
                      end;
                  end;
        end;

      // теперь отодвигаем назад все текстуры
      for i := texNum to DOORS_MAX_COUNT - 2 do
        doorsInMem[i] := doorsInMem[i + 1];
      InitDoor(g_DoorsCount - 1);
    end;

  Dec(g_DoorsCount);
  DoorsGridUpdate;
  DoorsGridSelectCell(DoorsGrid, 0, g_DoorsCount - 1, canSelect);
  GridSellectRow(DoorsGrid, g_DoorsCount - 1);

  if (g_DoorsCount = 0) then
    begin
      LoadDoorTextureBtn.Enabled   := False;
      ClearDoorTextureBtn.Enabled  := False;
      DeleteDoorTextureBtn.Enabled := False;
      DoorParamsGroupBox.Enabled  := False;
      DoorFramesGroupBox.Enabled  := False;
      PreviewDoorImage.Picture.Clear;
    end;
end;

procedure TfrmDoorsKeysEditor.DoorClearMoveSoundBtnClick(Sender: TObject);
begin
  if (g_DoorsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit; 
  if (not canInput) then exit;

  DoorMoveSoundsCmbBox.ItemIndex := -1;
  doorsInMem[texNum].moveSoundId := -1;
end;

procedure TfrmDoorsKeysEditor.DoorClearStartSoundBtnClick(Sender: TObject);
begin
  if (g_DoorsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  DoorStartSoundsCmbBox.ItemIndex := -1;
  doorsInMem[texNum].startSoundId := -1;
end;

procedure TfrmDoorsKeysEditor.DoorClearStopSoundBtnClick(Sender: TObject);
begin
  if (g_DoorsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  DoorStopSoundsCmbBox.ItemIndex := -1;
  doorsInMem[texNum].stopSoundId := -1;
end;

procedure TfrmDoorsKeysEditor.DoorMoveSoundsCmbBoxChange(Sender: TObject);
begin
  if (g_DoorsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  doorsInMem[texNum].moveSoundId := DoorMoveSoundsCmbBox.ItemIndex;
end;

procedure TfrmDoorsKeysEditor.AddDoorTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable;       
  DeleteColorModeDisable;

  if (g_DoorsCount < DOORS_MAX_COUNT) then
    begin
      texNum := g_DoorsCount;

      Inc(g_DoorsCount);
      DoorsGridUpdate;
      DoorsGrid.Col := 1;
      DoorsGrid.Row := g_DoorsCount;

      LoadDoorTextureBtnClick(Self);  

      doorsInMem[texNum].name := 'Door #' + IntToStr(texNum);
    end
  else
    MessageDlg('Error', 'Достигнут предел объектов дверей. Текущий максимум = ' + IntToStr(DOORS_MAX_COUNT), mtError, [mbOK], 0);

  DoorsGridSelectCell(DoorsGrid, 0, texNum, canSelect);
  GridSellectRow(DoorsGrid, texNum);
end;

procedure TfrmDoorsKeysEditor.AddKeyFrameBtnClick(Sender: TObject);
begin
  AnimatorDisable;        
  DeleteColorModeDisable;

  with (keysInMem[texNum]) do
    begin
      frmTexNum := framesCount;
      Inc(framesCount);
      SetLength(frames, framesCount);
      frames[frmTexNum] := nil;

      KeyFramesGridUpdate;
      KeyFramesGrid.Col := 1;
      KeyFramesGrid.Row := framesCount;
    end;

  LoadKeyFrameBtnClick(Self);

  KeyFramesGrid.Invalidate;
end;

procedure TfrmDoorsKeysEditor.AddKeyTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (g_KeysCount < KEYS_MAX_COUNT) then
    begin
      texNum := g_KeysCount;

      Inc(g_KeysCount);  
      KeysGridUpdate;
      KeysGrid.Col := 1;
      KeysGrid.Row := g_KeysCount;

      LoadKeyTextureBtnClick(Self);

      keysInMem[texNum].name := 'Key #' + IntToStr(texNum);
    end
  else
    MessageDlg('Error', 'Достигнут предел объектов ключей. Текущий максимум = ' + IntToStr(KEYS_MAX_COUNT), mtError, [mbOK], 0);

  KeysGridSelectCell(KeysGrid, 0, texNum, canSelect);
  GridSellectRow(KeysGrid, texNum);
end;

procedure TfrmDoorsKeysEditor.AnimatorTimer(Sender: TObject);
const
  animFrame : Integer = -1;
var
  framesCount : UInt32;
begin
  case (PageControl1.ActivePageIndex) of
    // двери
    0 : begin
          framesCount := doorsInMem[texNum].framesCount;

          Inc(animFrame);
          if (animFrame >= framesCount) then
            animFrame := 0;

          while (doorsInMem[texNum].frames[animFrame] = nil) do
            begin
              Inc(animFrame);
              if (animFrame >= framesCount) then
                animFrame := 0;
            end;

          LoadPreviewImageFromBitmap(PreviewDoorImage, doorsInMem[texNum].frames[animFrame]);
        end;
    // ключи
    1 : begin
          framesCount := keysInMem[texNum].framesCount;

          Inc(animFrame);
          if (animFrame >= framesCount) then
            animFrame := 0;

          while (keysInMem[texNum].frames[animFrame] = nil) do
            begin
              Inc(animFrame);
              if (animFrame >= framesCount) then
                animFrame := 0;
            end;

          LoadPreviewImageFromBitmap(PreviewKeyImage, keysInMem[texNum].frames[animFrame]);
        end;
  end;
end;

procedure TfrmDoorsKeysEditor.AutosizeBtnClick(Sender: TObject);
var
  image  : TImageData;
  stream : TMemoryStream;

  x, y : Integer;
  clr : TColor32Rec;
  halfW : Integer;
  minW, maxW : Integer;
  minH : Integer;
  w0, w1 : Integer;
begin
  AnimatorDisable;

  if (g_KeysCount = 0) then exit;

  with (keysInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      stream := TMemoryStream.Create;
      frames[frmTexNum].SaveToStream(stream);

      stream.Position := 0;
      FreeImage(image);
      LoadImageFromStream(stream, image);
      stream.Clear;

      //
      // высчитываем solid box
      //
      halfW := image.Width  shr 1;
      minW := halfW;
      maxW := halfW;
      minH := image.Height;
      for y := 0 to image.Height - 1 do
        for x := 0 to image.Width - 1 do
          begin
            clr := (PColor32Rec(image.Bits) + y * image.Width + x)^;
            if (clr.A <> 0) then
              begin
                if ((x <= halfW) and (x < minW)) then minW := x;
                if ((x >  halfW) and (x > maxW)) then maxW := x;
                if (y < minH) then minH := y;
              end;
          end;
      // если minW = halfW и maxW = halfW, значит текстура вся непрозрачная
      if ((minW = halfW) and (maxW = halfW)) then
        solidX := 100
      // проверяем что дальше от центра
      else
        begin
          w0 := halfW - minW;
          w1 := maxW - halfW;
          // то, что дальше, то и будет взято за основу
          if (w1 > w0) then
            w0 := w1;
          w0 := w0 shl 1;
          solidX := (w0 / image.Width) * 100;
        end;
      // устанавливаем высоту
      if (minH <> image.Height) then
        minH := image.Height - minH;
      solidY := (minH / image.Height) * 100;

      KeySolidXSpin.Value := solidX;
      KeySolidYSpin.Value := solidY;

      FreeImage(image);
      stream.Destroy;
    end;

  PreviewKeyImage.Invalidate;
end;

procedure TfrmDoorsKeysEditor.ClearKeyFrameBtnClick(Sender: TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable;      
  DeleteColorModeDisable;   

  PreviewKeyImage.Picture.Clear;

  with (keysInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      frames[frmTexNum].Clear;
      frames[frmTexNum].Destroy;
      frames[frmTexNum] := nil;

      DeleteFile(g_DkPath + '/key_' + IntToStr(texNum) + '_frame_' + IntToStr(frmTexNum) + '.dat');
    end;

  KeyFramesGridUpdate;
  KeyFramesGridSelectCell(KeyFramesGrid, 0, frmTexNum, canSelect);
  GridSellectRow(KeyFramesGrid, frmTexNum);
end;

procedure TfrmDoorsKeysEditor.ClearKeyIdBtnClick(Sender: TObject);
begin
  if (g_DoorsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  KeyIdCmbBox.ItemIndex := -1;
  doorsInMem[texNum].keyId := -1;
end;

procedure TfrmDoorsKeysEditor.ClearKeyTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
  i : Integer;
begin
  AnimatorDisable; 
  DeleteColorModeDisable;

  if (MessageDlg('Warning!',
                 'Do you really want to clear texture of key? It''s event clear all frames!',
                 mtWarning,
                 [mbYes, mbNo],
                 0) = mrNo) then
     exit;

  // удаляем файлы
  DeleteFile(g_DkPath + '/key_' + IntToStr(texNum) + '.cfg');
  for i := 0 to High(keysInMem[texNum].frames) do
    DeleteFile(g_DkPath + '/key_' + IntToStr(texNum) + '_frame_' + IntToStr(i) + '.dat');

  InitKey(texNum); 
  keysInMem[texNum].name := 'Key #' + IntToStr(texNum);

  KeysGridUpdate;
  KeysGridSelectCell(KeysGrid, 0, texNum, canSelect);
  GridSellectRow(KeysGrid, texNum);
end;

procedure TfrmDoorsKeysEditor.DeleteDoorColorBtnClick(Sender: TObject);
begin                                     
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;
  if (doorsInMem[texNum].framesCount = 0) then exit;
  if (doorsInMem[texNum].frames[frmTexNum] = nil) then exit;

  deleteColorMode := not deleteColorMode;

  if (not deleteColorMode) then
    DeleteColorModeDisable
  else
    DeleteColorModeEnable;
end;

procedure TfrmDoorsKeysEditor.DeleteKeyColorBtnClick(Sender: TObject);
begin
  if ((texNum < 0) or (texNum >= Length(keysInMem))) then exit;
  if (keysInMem[texNum].framesCount = 0) then exit;
  if (keysInMem[texNum].frames[frmTexNum] = nil) then exit;

  deleteColorMode := not deleteColorMode;

  if (not deleteColorMode) then
    DeleteColorModeDisable
  else
    DeleteColorModeEnable;
end;

procedure TfrmDoorsKeysEditor.DeleteKeyFrameBtnClick(Sender: TObject);
var
  i : Integer;
  canSelect : Boolean;
begin
  ClearKeyFrameBtnClick(Self);

  with (keysInMem[texNum]) do
    begin
      // сначала переносим все текстуры вниз
      for i := frmTexNum to framesCount - 2 do
        frames[i] := frames[i + 1];

      Dec(framesCount);
      Dec(frmTexNum);  

      SetLength(frames, framesCount);
    end;

  KeyFramesGridUpdate;
  KeyFramesGridSelectCell(KeyFramesGrid, 0, frmTexNum, canSelect); 
  GridSellectRow(KeyFramesGrid, frmTexNum);
end;

procedure TfrmDoorsKeysEditor.DeleteKeyTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
  i : Integer;  
  x, y, m : Integer;
  cell : Integer;
  fromPos, toPos : Integer;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (g_KeysCount = 0) then exit;

  if (MessageDlg('Warning!',
                 'Do you really want to delete texture of key? It''s event clear all frames!',
                 mtWarning,
                 [mbYes, mbNo],
                 0) = mrNo) then
     exit;

  // удаляем файлы
  DeleteFile(g_DkPath + '/key_' + IntToStr(texNum) + '.cfg');
  for i := 0 to High(keysInMem[texNum].frames) do
    DeleteFile(g_DkPath + '/key_' + IntToStr(texNum) + '_frame_' + IntToStr(i) + '.dat');

  InitKey(texNum);

  // удаляем на картах элементы, которые использовали удаленную текстуру
  //CheckMaps;
  if (texNum < g_KeysCount) then
    begin         
      // сдвигаем назад все элементы на карте
      if (texNum >= 0) then
        begin
          fromPos := TEXTURES_MAX_COUNT +
                     SPRITES_MAX_COUNT  +
                     DOORS_MAX_COUNT;
          toPos   := fromPos + KEYS_MAX_COUNT;

          for m := 0 to g_MapsCount - 1 do
            with (mapPack.maps[m]) do
              for x := 0 to MAP_WIDTH  - 1 do
                for y := 0 to MAP_HEIGHT - 1 do
                  begin
                    cell := level[x, y] - 1;
                    if ((cell >= fromPos) and (cell < toPos)) then
                      begin
                        if (cell > texNum + fromPos) then
                          level[x, y] := cell
                        else
                          level[x, y] := 0;
                      end;
                  end;
        end;

      // теперь отодвигаем назад все текстуры
      for i := texNum to KEYS_MAX_COUNT - 2 do
        keysInMem[i] := keysInMem[i + 1];
      InitKey(g_KeysCount - 1);

      // теперь правим ссылки на ключ у дверей
      for i := 0 to g_DoorsCount - 1 do
        begin
          if (doorsInMem[i].keyId = texNum) then
            doorsInMem[i].keyId := -1;
          if (doorsInMem[i].keyId > texNum) then
            Dec(doorsInMem[i].keyId);
        end;
    end;

  Dec(g_KeysCount);
  KeysGridUpdate;
  KeysGridSelectCell(KeysGrid, 0, g_KeysCount - 1, canSelect);
  GridSellectRow(KeysGrid, g_KeysCount - 1);

  if (g_KeysCount = 0) then
    begin
      LoadKeyTextureBtn.Enabled   := False;
      ClearKeyTextureBtn.Enabled  := False;
      DeleteKeyTextureBtn.Enabled := False;
      KeyParamsGroupBox.Enabled  := False;
      KeyFramesGroupBox.Enabled  := False;
      PreviewKeyImage.Picture.Clear;
    end;
end;

procedure TfrmDoorsKeysEditor.AddDoorFrameBtnClick(Sender: TObject);
begin
  AnimatorDisable;           
  DeleteColorModeDisable;

  with (doorsInMem[texNum]) do
    begin
      frmTexNum := framesCount;
      Inc(framesCount);
      SetLength(frames, framesCount);
      frames[frmTexNum] := nil;

      DoorFramesGridUpdate;
      DoorFramesGrid.Col := 1;
      DoorFramesGrid.Row := framesCount;
    end;

  LoadDoorFrameBtnClick(Self);

  DoorFramesGrid.Invalidate;
end;

procedure TfrmDoorsKeysEditor.DoorAnimSpeedSpinChange(Sender: TObject);
begin
  if (g_DoorsCount = 0) then exit;

  doorsInMem[texNum].animSpeed := DoorAnimSpeedSpin.Value;
  if (DoorAnimSpeedSpin.Value <> 0) then
    Animator.Interval := 1000 div DoorAnimSpeedSpin.Value
  else
    Animator.Interval := 0;
end;

procedure TfrmDoorsKeysEditor.ClearDoorFrameBtnClick(Sender: TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable;          
  DeleteColorModeDisable; 

  PreviewDoorImage.Picture.Clear;

  with (doorsInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      frames[frmTexNum].Clear;
      frames[frmTexNum].Destroy;
      frames[frmTexNum] := nil;

      DeleteFile(g_DkPath + '/door_' + IntToStr(texNum) + '_frame_' + IntToStr(frmTexNum) + '.dat');
    end;

  DoorFramesGridUpdate;
  DoorFramesGridSelectCell(DoorFramesGrid, 0, frmTexNum, canSelect);
  GridSellectRow(DoorFramesGrid, frmTexNum);
end;

procedure TfrmDoorsKeysEditor.ClearDoorTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
  i : Integer;
begin
  AnimatorDisable;          
  DeleteColorModeDisable;

  if (MessageDlg('Warning!',
                 'Do you really want to clear texture of door? It''s event clear all frames!',
                 mtWarning,
                 [mbYes, mbNo],
                 0) = mrNo) then
     exit;

  // удаляем файлы
  DeleteFile(g_DkPath + '/door_' + IntToStr(texNum) + '.cfg');
  DeleteFile(g_DkPath + '/door_' + IntToStr(texNum) + '_rib.dat');
  for i := 0 to High(doorsInMem[texNum].frames) do
    DeleteFile(g_DkPath + '/door_' + IntToStr(texNum) + '_frame_' + IntToStr(i) + '.dat');

  InitDoor(texNum);
  doorsInMem[texNum].name := 'Door #' + IntToStr(texNum);

  DoorsGridUpdate;
  DoorsGridSelectCell(DoorsGrid, 0, texNum, canSelect);
  GridSellectRow(DoorsGrid, texNum);
end;

procedure TfrmDoorsKeysEditor.DeleteDoorFrameBtnClick(Sender: TObject);
var
  i : Integer;
  canSelect : Boolean;
begin
  ClearDoorFrameBtnClick(Self);

  with (doorsInMem[texNum]) do
    begin
      // сначала переносим все текстуры вниз
      for i := frmTexNum to framesCount - 2 do
        frames[i] := frames[i + 1];

      Dec(framesCount);
      Dec(frmTexNum); 

      SetLength(frames, framesCount);
    end;

  DoorFramesGridUpdate;
  DoorFramesGridSelectCell(DoorFramesGrid, 0, frmTexNum, canSelect);
  GridSellectRow(DoorFramesGrid, frmTexNum);
end;

procedure TfrmDoorsKeysEditor.DeleteRigBtnClick(Sender: TObject);
begin         
  if (g_DoorsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  with (doorsInMem[texNum]) do
    begin
      if (rib <> nil) then
        rib.Destroy;
      rib := nil;
      NewRibImage;
    end;
end;

procedure TfrmDoorsKeysEditor.DoorsGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_DoorsCount = 0) then exit;
  //if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (doorsInMem[pos].framesCount = 0) then exit;
  bmp := doorsInMem[pos].frames[0];

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmDoorsKeysEditor.StayOpenedChkChange(Sender: TObject);
begin
  if (g_DoorsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  doorsInMem[texNum].stayOpened := StayOpenedChk.Checked;
end;

procedure TfrmDoorsKeysEditor.StretchDoorImageCheckChange(Sender: TObject);
begin
  PreviewDoorImage.Stretch := StretchDoorImageCheck.Checked;
end;

procedure TfrmDoorsKeysEditor.StretchDoorImageCheckSizeConstraintsChange(
  Sender: TObject);
begin
  PreviewDoorImage.Stretch := StretchDoorImageCheck.Checked;
end;

procedure TfrmDoorsKeysEditor.StretchKeyImageCheckChange(Sender: TObject);
begin
  PreviewKeyImage.Stretch := StretchKeyImageCheck.Checked;
end;

procedure TfrmDoorsKeysEditor.TabSheet3ContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin

end;

procedure TfrmDoorsKeysEditor.WidthSpinChange(Sender: TObject);
begin 
  if (g_DoorsCount = 0) then exit;  
  if ((texNum < 0) or (texNum >= g_DoorsCount)) then exit;
  if (not canInput) then exit;

  doorsInMem[texNum].width := WidthSpin.Value;
end;

procedure TfrmDoorsKeysEditor.FormPrepare;
var
  i : Integer;
begin
  AnimatorDisable;
  //DeleteColorModeDisable;
  texNum := -1;
  frmTexNum := -1; 
  canInput := False;

  // двери
  PreviewDoorImage.Picture.Clear;
  PreviewRibImage.Picture.Clear;
  PreviewDoorImage.Invalidate;
  PreviewRibImage.Invalidate;

  DoorParamsGroupBox.Enabled := False;
  DoorFramesGroupBox.Enabled := False;
  DoorFramesGrid.ColCount := 0;
  DoorFramesGrid.RowCount := 0;

  LoadDoorFrameBtn.Enabled   := False;
  ClearDoorFrameBtn.Enabled  := False;
  DeleteDoorFrameBtn.Enabled := False;

  LoadDoorTextureBtn.Enabled   := False;
  ClearDoorTextureBtn.Enabled  := False;
  DeleteDoorTextureBtn.Enabled := False;

  DoorNameEdit.Text := '<unknown>';
  KeyIdCmbBox.ItemIndex := -1;   
  DoorMoveSoundsCmbBox.ItemIndex := -1;
  DoorStopSoundsCmbBox.ItemIndex := -1;

  //DoorsGridUpdate;

  // ключи
  PreviewKeyImage.Picture.Clear;
  KeyParamsGroupBox.Enabled := False;
  KeyFramesGroupBox.Enabled := False;
  KeyFramesGrid.ColCount := 0;
  KeyFramesGrid.RowCount := 0;

  LoadKeyFrameBtn.Enabled   := False;
  ClearKeyFrameBtn.Enabled  := False;
  DeleteKeyFrameBtn.Enabled := False;

  LoadKeyTextureBtn.Enabled   := False;
  ClearKeyTextureBtn.Enabled  := False;
  DeleteKeyTextureBtn.Enabled := False;

  KeyNameEdit.Text := '<unknown>';

  //KeysGridUpdate;
  //KeyListUpdate;

  DoorsGrid.Options := [goAlwaysShowEditor, goSmoothScroll];
  KeysGrid.Options  := [goAlwaysShowEditor, goSmoothScroll];

  // заполняем списки 
  with (DoorStartSoundsCmbBox) do
    begin
      Items.Clear;
      for i := 0 to g_SoundsCount - 1 do
        Items.Add(soundsInMem[i].name);
      ItemIndex := -1;
    end;

  with (DoorMoveSoundsCmbBox) do
    begin
      Items.Clear;
      for i := 0 to g_SoundsCount - 1 do
        Items.Add(soundsInMem[i].name);
      ItemIndex := -1;
    end;

  with (DoorStopSoundsCmbBox) do
    begin
      Items.Clear;
      for i := 0 to g_SoundsCount - 1 do
        Items.Add(soundsInMem[i].name);
      ItemIndex := -1;
    end;

  with (KeyPickSoundsCmbBox) do
    begin
      Items.Clear;
      for i := 0 to g_SoundsCount - 1 do
        Items.Add(soundsInMem[i].name);
      ItemIndex := -1;
    end;
end;

procedure TfrmDoorsKeysEditor.AnimatorDisable;
begin
  DM.ImageList.GetRawImage(0, rawImage);
  DoorFramePlayBtn.Glyph.Clear;   
  KeyFramePlayBtn.Glyph.Clear;
  DoorFramePlayBtn.Glyph.LoadFromRawImage(rawImage, False);
  KeyFramePlayBtn.Glyph.LoadFromRawImage(rawImage, False);

  Animator.Enabled := False;
  DoorFramePlayBtn.Caption := 'Play';    
  KeyFramePlayBtn.Caption  := 'Play';
end;

procedure TfrmDoorsKeysEditor.AnimatorEnable;
begin
  DM.ImageList.GetRawImage(1, rawImage);
  DoorFramePlayBtn.Glyph.Clear;
  KeyFramePlayBtn.Glyph.Clear;
  DoorFramePlayBtn.Glyph.LoadFromRawImage(rawImage, false);
  KeyFramePlayBtn.Glyph.LoadFromRawImage(rawImage, false);
                         
  Animator.Interval := 0;
  case (PageControl1.ActivePageIndex) of
    // двери
    0 : if (DoorAnimSpeedSpin.Value <> 0) then
          Animator.Interval := 1000 div DoorAnimSpeedSpin.Value;
    // ключи
    1 : if (KeyAnimSpeedSpin.Value <> 0) then
          Animator.Interval := 1000 div KeyAnimSpeedSpin.Value;
  end;

  Animator.Enabled  := True;
  DoorFramePlayBtn.Caption := 'Stop';   
  KeyFramePlayBtn.Caption  := 'Stop';
end;

procedure TfrmDoorsKeysEditor.DeleteColorModeEnable;
begin
  AnimatorDisable;

  case (PageControl1.ActivePageIndex) of
    // двери
    0 : begin   
          if ((texNum < 0) or (texNum >= Length(doorsInMem))) then exit;
          if (doorsInMem[texNum].framesCount = 0) then exit;
          if (doorsInMem[texNum].frames[frmTexNum] = nil) then exit;
        end;
    // ключи
    1 : begin  
          if ((texNum < 0) or (texNum >= Length(keysInMem))) then exit;
          if (keysInMem[texNum].framesCount = 0) then exit;
          if (keysInMem[texNum].frames[frmTexNum] = nil) then exit;
        end;
  end;

  DM.ImageList.GetRawImage(5, rawImage);
  DeleteDoorColorBtn.Glyph.Clear;
  DeleteDoorColorBtn.Glyph.LoadFromRawImage(rawImage, False);
  DeleteKeyColorBtn.Glyph.Clear;
  DeleteKeyColorBtn.Glyph.LoadFromRawImage(rawImage, False);
  PreviewDoorImage.Cursor := crCross;     
  PreviewKeyImage.Cursor  := crCross;

  DoorsGroupBox.Enabled      := False;
  DoorsEditGroupBox.Enabled  := False;
  DoorFramesGroupBox.Enabled := False;
  DoorParamsGroupBox.Enabled := False;

  KeysGroupBox.Enabled      := False;
  KeysEditGroupBox.Enabled  := False;
  KeyFramesGroupBox.Enabled := False;
  KeyParamsGroupBox.Enabled := False;

  deleteColorMode := True;     
  canChooseSolid  := False;
end;

procedure TfrmDoorsKeysEditor.DeleteColorModeDisable;
begin
  DM.ImageList.GetRawImage(4, rawImage);
  DeleteDoorColorBtn.Glyph.Clear;
  DeleteDoorColorBtn.Glyph.LoadFromRawImage(rawImage, False);
  DeleteKeyColorBtn.Glyph.Clear;
  DeleteKeyColorBtn.Glyph.LoadFromRawImage(rawImage, False); 
  PreviewDoorImage.Cursor := crDefault;
  PreviewKeyImage.Cursor  := crDefault;   
                        
  DoorsGroupBox.Enabled := True;
  if (g_DoorsCount > 0) then
    begin
      DoorsEditGroupBox.Enabled  := True;
      DoorFramesGroupBox.Enabled := True;
      DoorParamsGroupBox.Enabled := True;
    end;
                        
  KeysGroupBox.Enabled := True;
  if (g_KeysCount > 0) then
    begin
      KeysEditGroupBox.Enabled  := True;
      KeyFramesGroupBox.Enabled := True;
      KeyParamsGroupBox.Enabled := True;
    end;

  deleteColorMode := False; 
  canChooseSolid  := False;
end;

procedure TfrmDoorsKeysEditor.DoorsGridUpdate;
var
  i : Integer;
begin
  if (g_DoorsCount = 0) then
    begin
      DoorsGrid.ColCount := 0;
      DoorsGrid.RowCount := 0;
      exit;
    end;

  // формируем грид со спрайтами
  DoorsGrid.ColCount := 1;
  DoorsGrid.RowCount := g_DoorsCount;

  for i := 0 to DoorsGrid.ColCount - 1 do
    DoorsGrid.ColWidths [i] := 96;
  for i := 0 to DoorsGrid.RowCount - 1 do
    DoorsGrid.RowHeights[i] := 96;
  DoorsGrid.Invalidate;
end;

procedure TfrmDoorsKeysEditor.DoorFramesGridUpdate;
var
  i : Integer;
begin
  if (doorsInMem[texNum].framesCount = 0) then
    begin
      DoorFramesGrid.ColCount := 0;
      DoorFramesGrid.RowCount := 0;
      exit;
    end;

  // формируем грид с текстурами
  DoorFramesGrid.ColCount := 1;
  DoorFramesGrid.RowCount := doorsInMem[texNum].framesCount;

  for i := 0 to DoorFramesGrid.ColCount - 1 do
    DoorFramesGrid.ColWidths [i] := 48;
  for i := 0 to DoorFramesGrid.RowCount - 1 do
    DoorFramesGrid.RowHeights[i] := 48;
  DoorFramesGrid.Invalidate;
end;

procedure TfrmDoorsKeysEditor.NewRibImage;
var
  image : TImageData;
  stream : TMemoryStream = nil;
begin
  stream := TMemoryStream.Create;
  NewImage(1, 1, ifA8R8G8B8, image);

  stream.Position := 0;
  SaveImageToStream('bmp', stream, image);
  stream.Position := 0;

  with (doorsInMem[texNum]) do
    begin
      if (rib <> nil) then
        rib.Destroy;
      rib := TBitmap.Create;
      rib.LoadFromStream(stream);
      LoadPreviewImageFromBitmap(PreviewRibImage, rib);
    end;

  FreeImage(image);
  stream.Destroy;
end;

procedure TfrmDoorsKeysEditor.KeysGridUpdate;
var
  i : Integer;
begin
  if (g_KeysCount = 0) then
    begin
      KeysGrid.ColCount := 0;
      KeysGrid.RowCount := 0;
      exit;
    end;

  // формируем грид со спрайтами
  KeysGrid.ColCount := 1;
  KeysGrid.RowCount := g_KeysCount;

  for i := 0 to KeysGrid.ColCount - 1 do
    KeysGrid.ColWidths [i] := 96;
  for i := 0 to KeysGrid.RowCount - 1 do
    KeysGrid.RowHeights[i] := 96;
  KeysGrid.Invalidate;
end;

procedure TfrmDoorsKeysEditor.KeyFramesGridUpdate;
var
  i : Integer;
begin
  if (keysInMem[texNum].framesCount = 0) then
    begin
      KeyFramesGrid.ColCount := 0;
      KeyFramesGrid.RowCount := 0;
      exit;
    end;

  // формируем грид с текстурами
  KeyFramesGrid.ColCount := 1;
  KeyFramesGrid.RowCount := keysInMem[texNum].framesCount;

  for i := 0 to KeyFramesGrid.ColCount - 1 do
    KeyFramesGrid.ColWidths [i] := 48;
  for i := 0 to KeyFramesGrid.RowCount - 1 do
    KeyFramesGrid.RowHeights[i] := 48;
  KeyFramesGrid.Invalidate;
end;

procedure TfrmDoorsKeysEditor.KeyListUpdate;
var
  i : Integer;
begin
  KeyIdCmbBox.Clear;

  for i := 0 to g_KeysCount - 1 do
    KeyIdCmbBox.AddItem(keysInMem[i].name, nil);
end;

end.

