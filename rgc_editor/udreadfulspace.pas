unit uDreadfulSpace;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  FileUtil,
  Forms,
  Controls,
  Graphics,
  Dialogs,
  ExtCtrls,
  LCLType;

type

  { TfrmDreadfulSpace }

  TfrmDreadfulSpace = class(TForm)
    PaintBox: TImage;
    Timer1: TTimer;
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Timer1Timer(Sender: TObject);
  private   
    procedure StartGame();
    procedure StopGame;
  public

  end;

  { TStar }

  TStar = class
    x, y : Integer;
    speed : Integer;
    size : Integer;  
    color : TColor;

    constructor Create;
    procedure Init;
    procedure Update;
  end;

  { TBullet }

  TBullet = class
    x, y : Integer;
    speed : Integer;
    size : Integer;   
    color : TColor;
                    
    constructor Create(_x, _y, direction : Integer);
    procedure Update;
  end;

  { TParticle }

  TParticle = class
    x, y : Double;
    dirX, dirY : Double;
    speed : Integer;
    size : Integer;
    delayDelete : Integer;
    color : TColor;

    constructor Create(_x, _y : Integer);
    procedure Update;
  end;

  { TEnemy }

  TEnemy = class
    x, y : Integer;
    delayShoot : Integer;
    speed : Integer;
    color : TColor;

    constructor Create(_x, _y : Integer);
    procedure Update;
  end;

  { TPlayer }

  TPlayer = class
    x, y : Integer;
    moveL, moveR : Boolean;
    canShoot : Boolean;
    delayShoot : Integer;
    speed : Integer;
    life : Integer;
    score : Integer;  
    color : TColor;

    constructor Create;
    procedure Update;
  end;

const
  FPS = 120;
  PIXEL_SMALL  = 1;
  PIXEL_MIDDLE = 2;
  PIXEL_SIZE   = 4;
  IMAGE_WIDTH  = 8;
  IMAGE_HEIGHT = 8;
  IMAGE_SIZE   = IMAGE_WIDTH * PIXEL_SIZE;
  IMAGE_HALF   = IMAGE_SIZE shr 1;

  WINDOW_WIDTH  = 480;
  WINDOW_HEIGHT = 640;
  WINDOW_HALF_WIDTH  = WINDOW_WIDTH  shr 1;
  WINDOW_HALF_HEIGHT = WINDOW_HEIGHT shr 1;

  DREADFUL_COMING_TEXT = '...DREADFUL SPACE IS COMING...';

var
  frmDreadfulSpace: TfrmDreadfulSpace;
var
  dreadfulSpacePos : Integer;
  stars : array of TStar;
  bullets : array of TBullet;   
  particles : array of TParticle;
  enemies : array of TEnemy;
  enemyImg : array [0..IMAGE_WIDTH - 1, 0..IMAGE_HEIGHT - 1] of Byte = (   
                                            (1,1,1,1,1,1,1,1),
                                            (1,1,1,1,1,1,1,1),
                                            (1,0,0,1,1,0,0,1),
                                            (0,0,0,1,1,0,0,0),
                                            (0,0,0,1,1,0,0,0),
                                            (0,0,0,1,1,0,0,0),
                                            (0,0,0,0,0,0,0,0),
                                            (0,0,0,0,0,0,0,0));
  enemyDelay : Integer;

  player : TPlayer;
  playerImg : array [0..IMAGE_WIDTH - 1, 0..IMAGE_HEIGHT - 1] of Byte = (
                                            (0,0,0,1,1,0,0,0),
                                            (0,0,0,1,1,0,0,0),
                                            (0,0,0,1,1,0,0,0),
                                            (0,0,0,1,1,0,0,0),
                                            (1,0,1,1,1,1,0,1),
                                            (1,0,1,1,1,1,0,1),
                                            (1,1,1,1,1,1,1,1),
                                            (1,1,1,1,1,1,1,1));

{ objects are collided? }
function IsCollided(x1, y1, x2, y2, x3, y3, x4, y4 : Integer) : Boolean;

implementation

function IsCollided(x1, y1, x2, y2, x3, y3, x4, y4: Integer): Boolean;
begin
  Result := ((x2 >= x3) and (x1 <= x4)) and
            ((y2 >= y3) and (y1 <= y4));
end;

procedure TfrmDreadfulSpace.StartGame();
var
  i : Integer;
begin
  player := TPlayer.Create;

  SetLength(stars, 100);
  for i := Low(stars) to High(stars) do
    stars[i] := TStar.Create;

  SetLength(bullets,   100);
  SetLength(particles, 100);
  SetLength(enemies,   100);
  enemyDelay := Random(FPS * 4);

  dreadfulSpacePos := WINDOW_HEIGHT;

  Timer1.Enabled  := True;
end;

procedure TfrmDreadfulSpace.StopGame;
var
  i : Integer;
begin           
  Timer1.Enabled  := False;

  for i := Low(stars) to High(stars) do
    stars[i].Free;
  SetLength(stars, 0);

  for i := Low(bullets) to High(bullets) do
    if (bullets[i] <> nil) then
      bullets[i].Free;
  SetLength(bullets, 0);

  for i := Low(particles) to High(particles) do
    if (particles[i] <> nil) then
      particles[i].Free;
  SetLength(particles, 0);

  for i := Low(enemies) to High(enemies) do
    if (enemies[i] <> nil) then
      enemies[i].Free;
  SetLength(enemies, 0);

  player.Free;
end;

{$R *.lfm}

{ TParticle }

constructor TParticle.Create(_x, _y : Integer);  
var
  brightness : Byte;
begin  
  x := _x;
  y := _y;

  dirX := Cos(Random(360));
  dirY := Sin(Random(360));

  size  := 1 + Random(PIXEL_SIZE);
  speed := 1 + Random(PIXEL_SIZE);

  delayDelete := FPS shr 1 + Random(FPS shr 1);

  brightness := 128 + Random(127);
  color :=  $FF +
           (brightness shl 16) +
           (brightness shl 8 ) +
            brightness;
end;

procedure TParticle.Update;
begin
  x += dirX * speed;
  y += dirY * speed;

  if (delayDelete > 0) then
    Dec(delayDelete);
end;

{ TEnemy }

constructor TEnemy.Create(_x, _y: Integer);
var
  brightness : Byte;
begin
  x := _x;
  y := _y;

  speed := 1 + Random(PIXEL_MIDDLE);
  delayShoot := 0;

  brightness := 128 + Random(127);
  color :=  $FF +
           (brightness shl 16) +
           (brightness shl 8 ) +
            brightness;
end;

procedure TEnemy.Update;
var
  i : Integer;
begin
  y += speed;

  if (delayShoot > 0) then
    Dec(delayShoot)
  else
    begin
      delayShoot := FPS shr 1 + Random(FPS shr 1);

      for i := Low(bullets) to High(bullets) do
        if (bullets[i] = nil) then
          begin
            bullets[i] := TBullet.Create(x, y + IMAGE_SIZE + PIXEL_SIZE, 1);
            break;
          end;
    end;
end;

{ TBullet }

constructor TBullet.Create(_x, _y, direction: Integer);
begin
  x := _x;
  y := _y;

  size := PIXEL_SIZE shl 1;
  speed := 2 * PIXEL_SIZE * direction;
  color := clWhite;
end;

procedure TBullet.Update;
begin
  y += speed;
end;

{ TPlayer }

constructor TPlayer.Create;
begin
  x := WINDOW_WIDTH shr 1;
  y := WINDOW_HEIGHT - PIXEL_SIZE;

  speed := PIXEL_SIZE;

  moveL := False;
  moveR := False;

  delayShoot := 0;

  life := 5;
  score := 0;

  color := clWhite;
end;

procedure TPlayer.Update;
var
  i : Integer;
begin
  if (delayShoot > 0) then Dec(delayShoot);

  if (moveL and (x > IMAGE_HALF))                then x -= speed;
  if (moveR and (x < WINDOW_WIDTH - IMAGE_HALF)) then x += speed;

  if (player.canShoot and (player.delayShoot = 0)) then
    begin
      for i := Low(bullets) to High(bullets) do
        if (bullets[i] = nil) then
          begin
            bullets[i] := TBullet.Create(player.x, player.y - IMAGE_SIZE, -1);
            break;
          end;
      player.delayShoot := FPS shr 3;
    end;
end;

{ TStar }

constructor TStar.Create;
begin
  Init;
end;

procedure TStar.Init;
var
  brightness : Byte;
begin  
  x := Random(WINDOW_WIDTH);
  y := Random(WINDOW_HEIGHT);

  size := Random(4) + 2;  // PIXEL_*
  speed := Random(PIXEL_SIZE) + 1;

  brightness := 128 + Random(127);
  color :=  $FF +
           (brightness shl 16) +
           (brightness shl 8 ) +
            brightness;
end;

procedure TStar.Update;
begin
  y += speed;

  if (y > WINDOW_HEIGHT) then
    begin
      Init;
      y := 0;
    end;
end;

{ TfrmDreadfulSpace }

procedure TfrmDreadfulSpace.FormCreate(Sender: TObject);
begin
  DoubleBuffered  := True;
  PaintBox.AntialiasingMode := amOn;

  Timer1.Interval := 1000 div FPS;

  Width  := WINDOW_WIDTH;
  Height := WINDOW_HEIGHT;

  StartGame();
end;

procedure TfrmDreadfulSpace.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_LEFT) then
    begin
      player.moveR := False;
      player.moveL := True;
    end;

  if (Key = VK_RIGHT) then
    begin
      player.moveR := True;
      player.moveL := False;
    end;

  if (Key = VK_ESCAPE) then
    Close;

  if (Key = VK_SPACE) then
    player.canShoot := True;
end;

procedure TfrmDreadfulSpace.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_LEFT) then
    player.moveL := False;
                   
  if (Key = VK_RIGHT) then
    player.moveR := False;

  if (Key = VK_SPACE) then
    player.canShoot := False;
end;

procedure TfrmDreadfulSpace.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  StopGame;
end;

procedure TfrmDreadfulSpace.Timer1Timer(Sender: TObject);
var
  i, j, e, p : Integer;
  x1, y1, x2, y2 : Integer;
  star : TStar;
  bullet : TBullet;
  particle : TParticle;
  enemy : TEnemy;
  score : Integer;
begin
  with (PaintBox.Canvas) do
    begin
      Lock;
      Brush.Color := clBlack;
      Pen.Color   := clBlack;

      Rectangle(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
    end;

  { CAPTION }
  Dec(dreadfulSpacePos);
  if (dreadfulSpacePos > 0) then
    with (PaintBox.Canvas) do
      begin
        Font.Color  := clGrayText;
        Font.Style  := [fsBold];
        Font.Size   := 10;
        Brush.Color := clBlack;
        TextOut (WINDOW_HALF_WIDTH - (Font.Size * Length(DREADFUL_COMING_TEXT)) shr 1, dreadfulSpacePos, DREADFUL_COMING_TEXT);
      end;

  { STARS }
  for i := Low(stars) to High(stars) do
    begin
      star := stars[i];
      star.Update;    
      with (PaintBox.Canvas) do
        begin
          Lock;
          Brush.Color := star.color;
          Pen.Color   := star.color;

          Rectangle(star.x - (star.size shr 1),
                    star.y - (star.size shr 1),
                    star.x + (star.size shr 1),
                    star.y + (star.size shr 1));
        end;
    end;

  { BULLETS }
  for i := Low(bullets) to High(bullets) do
    begin
      bullet := bullets[i];
      if (bullet = nil) then
        continue;

      bullet.Update;   
      with (PaintBox.Canvas) do
        begin
          Lock;
          Brush.Color := bullet.color;
          Pen.Color   := bullet.color;

          Rectangle(bullet.x - (bullet.size shr 1),
                    bullet.y - (bullet.size shr 1),
                    bullet.x + (bullet.size shr 1),
                    bullet.y + (bullet.size shr 1));
        end;

      { DAMAGE TO ENEMY }
      for e := Low(enemies) to High(enemies) do
        begin
          enemy := enemies[e];
          if (enemy = nil) then
            Continue;

          if (IsCollided(bullet.x - bullet.size div 2,
                         bullet.y - bullet.size div 2,
                         bullet.x + bullet.size div 2,
                         bullet.y + bullet.size div 2,
                         enemy.x  - IMAGE_HALF,
                         enemy.y,
                         enemy.x  + IMAGE_HALF,
                         enemy.y  + IMAGE_SIZE)) then
            begin
              { create particles }
              p := 0;
              for j := Low(particles) to High(particles) do
                if (particles[j] = nil) then
                  begin
                    particles[j] := TParticle.Create(enemy.x, enemy.y + IMAGE_HALF);
                    Inc(p);
                    if (p >= 20) then
                      break;
                  end;
                            
              enemy.Destroy;
              enemies[e] := nil;
              bullet.y := -666; // delete bullet

              Inc(player.score);
            end;
        end;

      { DAMAGE TO PLAYER }    
      if (IsCollided(bullet.x - bullet.size div 2,
                     bullet.y - bullet.size div 2,
                     bullet.x + bullet.size div 2,
                     bullet.y + bullet.size div 2,
                     player.x - IMAGE_HALF,
                     player.y - IMAGE_SIZE,
                     player.x + IMAGE_HALF,
                     player.y)) then
        begin
          Dec(player.life);
          with (PaintBox.Canvas) do
            begin
              Brush.Color := clRed;
              Rectangle(bullet.x - (bullet.size shr 1),
                        bullet.y - (bullet.size shr 1),
                        bullet.x + (bullet.size shr 1),
                        bullet.y + (bullet.size shr 1));
            end;

          bullet.y := -666;
        end;

      if ((bullet.y < 0) or (bullet.y > WINDOW_HEIGHT)) then
        begin
          bullet.Destroy;
          bullets[i] := nil;
        end;
    end;

  { PARTICLES }
  for i := Low(particles) to High(particles) do
    begin
      particle := particles[i];
      if (particle = nil) then
        continue;

      particle.Update;
      x1 := Round(particle.x) - (particle.size shr 1);
      y1 := Round(particle.y) - (particle.size shr 1);
      x2 := Round(particle.x) + (particle.size shr 1);
      y2 := Round(particle.y) + (particle.size shr 1);

      with (PaintBox.Canvas) do
        begin
          Lock;
          Brush.Color := particle.color;
          Pen.Color   := particle.color;

          Rectangle(x1, y1, x2, y2);
        end;

      if ((particle.x < 0) or
          (particle.x > WINDOW_WIDTH) or
          (particle.y < 0) or
          (particle.y > WINDOW_HEIGHT) or
          (particle.delayDelete = 0)) then
        begin
          particle.Destroy;
          particles[i] := nil;
        end;
    end;

  { ENEMY }
  if (enemyDelay > 0) then
    Dec(enemyDelay)
  else
    begin
      enemyDelay := Random(FPS * 4);
                  
      for i := Low(enemies) to High(enemies) do
        if (enemies[i] = nil) then
          begin
            enemies[i] := TEnemy.Create(Random(WINDOW_WIDTH - IMAGE_SIZE) + IMAGE_SIZE, -IMAGE_SIZE);
            break;
          end;
    end;

  for e := Low(enemies) to High(enemies) do
    begin
      enemy := enemies[e];
      if (enemy = nil) then
        continue;

      enemy.Update;

      for j := 0 to 7 do
        for i := 0 to 7 do
          if (enemyImg[j, i] = 1) then
            begin
              x1 := enemy.x - IMAGE_HALF + PIXEL_SIZE * i;
              y1 := enemy.y + PIXEL_SIZE * j;
              x2 := x1 + PIXEL_SIZE;
              y2 := y1 + PIXEL_SIZE;

              with (PaintBox.Canvas) do
                begin
                  Pen.Color   := enemy.color;
                  Brush.Color := enemy.color;
                  Rectangle(x1, y1, x2, y2);
                end;
            end;

      if (enemy.y > WINDOW_HEIGHT) then
        begin
          enemy.Destroy;
          enemies[e] := nil;
        end;
    end;

  { PLAYER }
  player.Update;
  for j := 0 to 7 do
    for i := 0 to 7 do
      if (playerImg[j, i] = 1) then
        begin
          x1 := player.x - IMAGE_HALF + PIXEL_SIZE * i;
          y1 := player.y - IMAGE_SIZE + PIXEL_SIZE * j;
          x2 := x1 + PIXEL_SIZE;
          y2 := y1 + PIXEL_SIZE;
                                                
          with (PaintBox.Canvas) do
            begin
              Lock;
              Brush.Color := player.color;
              Pen.Color   := player.color;

              Rectangle(x1, y1, x2, y2);
            end;
        end;

  PaintBox.Canvas.Unlock;
  PaintBox.Invalidate;

  Caption := 'DREADFUL SPACE    LIFE: ' + IntToStr(player.life) + '    SCORE: ' + IntToStr(player.score);
  if (player.life <= 0) then
    begin
      score := player.score;
      StopGame;
      ShowMessage('GAME OVER! YOUR SCORE: ' + IntToStr(score));
      StartGame;
    end;
end;

end.

