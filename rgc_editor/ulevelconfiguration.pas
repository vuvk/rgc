unit uLevelConfiguration;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  FileUtil,
  Forms,
  Controls,
  Graphics,
  Dialogs,
  StdCtrls,
  LCLType,
  Spin,
  Buttons,
  uSkyboxEditor;

type

  { TfrmLevelConfiguration }

  TfrmLevelConfiguration = class(TForm)
    ClearKeyIdBtn: TSpeedButton;
    EditSkyboxesBtn: TBitBtn;
    CancelBtn: TBitBtn;
    ApplyBtn: TBitBtn;
    Label7: TLabel;
    Label8: TLabel;
    NameEdit: TEdit;
    SkyboxCmbBox: TComboBox;
    ShowFloorChk: TCheckBox;
    ShowCeilChk: TCheckBox;
    ShowFogChk: TCheckBox;
    ShowSkyChk: TCheckBox;
    FloorColorBtn: TColorButton;
    CeilColorBtn: TColorButton;
    FogColorBtn: TColorButton;
    FogIntensitySpin: TFloatSpinEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    LevelCountInfoLbl: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    LevelList: TListBox;
    LevelCountSpin: TSpinEdit;
    procedure CancelBtnClick(Sender: TObject);
    procedure ApplyBtnClick(Sender: TObject);
    procedure CeilColorBtnColorChanged(Sender: TObject);
    procedure ClearKeyIdBtnClick(Sender: TObject);
    procedure EditSkyboxesBtnClick(Sender: TObject);
    procedure FloorColorBtnColorChanged(Sender: TObject);
    procedure FogColorBtnColorChanged(Sender: TObject);
    procedure FogIntensitySpinChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure LevelListClick(Sender: TObject);
    procedure LevelListSelectionChange(Sender: TObject; User: boolean);
    procedure NameEditChange(Sender: TObject);
    procedure ShowCeilChkChange(Sender: TObject);
    procedure ShowFloorChkChange(Sender: TObject);
    procedure ShowFogChkChange(Sender: TObject);
    procedure ShowSkyChkChange(Sender: TObject);
    procedure ShowSkyChkSizeConstraintsChange(Sender: TObject);
    procedure SkyboxCmbBoxChange(Sender: TObject);
  private

    procedure InitForm;

  public

  end;

implementation

uses
  uContainer;

var
  curMapNum  : Integer = 0;   // номер выбранной карты
  prevMapNum : Integer = 0;   // для выбора того же уровня при переходе в редактор скайбоксов
  oldMapsCount : Integer = 0;
  isFormLoaded : Boolean = False;  // загружена ли уже форма? Для предотвращения срабатывания OnChange при программном заполнении комбобокса

  skyBoxNamesList : TStringList = nil;  
  canInputName : Boolean = False; // можно ли вводить имя

{$R *.lfm}

{ TfrmLevelConfiguration }

procedure TfrmLevelConfiguration.CeilColorBtnColorChanged(Sender: TObject);
begin
  mapPack.maps[curMapNum].ceilColor := CeilColorBtn.ButtonColor;
end;

procedure TfrmLevelConfiguration.ClearKeyIdBtnClick(Sender: TObject);
begin  
  if (g_MapsCount = 0) then exit;
  if ((curMapNum < 0) or (curMapNum >= g_MapsCount)) then exit;

  SkyboxCmbBox.ItemIndex := -1;
  mapPack.maps[curMapNum].skyNumber := 0;
end;

procedure TfrmLevelConfiguration.EditSkyboxesBtnClick(Sender: TObject);
var
  frmSkyboxEditor : TfrmSkyboxEditor;
begin                                
  frmSkyboxEditor := TfrmSkyboxEditor.Create(Self);
  with (frmSkyboxEditor) do
    try
      ShowModal;
    finally
      Destroy;
    end;

  prevMapNum := LevelList.ItemIndex;
  skyBoxNamesList.Destroy;
  skyBoxNamesList := LoadSkyboxNames;
  InitForm;
end;

procedure TfrmLevelConfiguration.CancelBtnClick(Sender: TObject);
begin
  LevelCountSpin.Value := oldMapsCount;
end;

procedure TfrmLevelConfiguration.ApplyBtnClick(Sender: TObject);
var
  i : Integer;
begin              
  if (oldMapsCount = LevelCountSpin.Value) then exit;

  // пользователь решил добавить карт
  if (oldMapsCount < LevelCountSpin.Value) then
    begin
      g_MapsCount := LevelCountSpin.Value;

      SetLength(mapPack.maps, g_MapsCount);
      for i := oldMapsCount to High(mapPack.maps) do
        if (mapPack.maps[i] = nil) then
          begin
            mapPack.maps[i] := TMap.Create;
            mapPack.maps[i].name := 'Level #' + IntToStr(i);
          end;
    end
  else
    // пользователь решил урезать количество карт
    begin
      if (MessageDlg('WARNING!',
                     'Are you sure want to delete old levels?',
                     mtWarning,
                     [mbNo, mbYes],
                     0) = mrNo) then
        begin
          LevelCountSpin.Value := oldMapsCount;
          exit;
        end;

      if (prevMapNum > g_MapsCount - 1) then
        prevMapNum := g_MapsCount - 1;

      for i := LevelCountSpin.Value to g_MapsCount - 1 do
        DeleteMap(i);
      g_MapsCount := LevelCountSpin.Value;
      SetLength(mapPack.maps, g_MapsCount);
    end;

  InitForm;
end;

procedure TfrmLevelConfiguration.FloorColorBtnColorChanged(Sender: TObject);
begin
  mapPack.maps[curMapNum].floorColor := FloorColorBtn.ButtonColor;
end;

procedure TfrmLevelConfiguration.FogColorBtnColorChanged(Sender: TObject);
begin
  mapPack.maps[curMapNum].fogColor := FogColorBtn.ButtonColor;
end;

procedure TfrmLevelConfiguration.FogIntensitySpinChange(Sender: TObject);
begin
  if ((curMapNum < 0) or (curMapNum > High(mapPack.maps))) then exit;
  if (mapPack.maps[curMapNum] = nil) then exit;
  mapPack.maps[curMapNum].fogIntensity := FogIntensitySpin.Value;
end;

procedure TfrmLevelConfiguration.FormActivate(Sender: TObject);
begin
  SetFocus;
end;

procedure TfrmLevelConfiguration.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  skyBoxNamesList.Destroy;
  skyBoxNamesList := nil;
end;

procedure TfrmLevelConfiguration.FormCreate(Sender: TObject);
begin
  isFormLoaded := False;
  curMapNum := 0;
  prevMapNum := 0;
  skyBoxNamesList := LoadSkyboxNames;

  InitForm;

  isFormLoaded := True;
end;

procedure TfrmLevelConfiguration.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then Close;
end;

procedure TfrmLevelConfiguration.LevelListClick(Sender: TObject);
begin
  canInputName := False;  
  NameEdit.Text := '<unknown>';

  curMapNum := LevelList.ItemIndex;
  if (curMapNum < 0) then exit;

  with (mapPack.maps[curMapNum]) do
    begin                   
      NameEdit.Text             := name;
      FloorColorBtn.ButtonColor := floorColor;
      CeilColorBtn.ButtonColor  := ceilColor;
      FogColorBtn.ButtonColor   := fogColor;
      FogIntensitySpin.Value    := fogIntensity;
      ShowFloorChk.Checked      := showFloor;
      ShowCeilChk.Checked       := showCeil;
      ShowFogChk.Checked        := showFog;
      ShowSkyChk.Checked        := showSky;
      SkyboxCmbBox.ItemIndex    := skyNumber;
    end;                                     
  canInputName := True;
end;

procedure TfrmLevelConfiguration.LevelListSelectionChange(Sender: TObject;
  User: boolean);
begin
  //LevelListClick(Sender);
end;

procedure TfrmLevelConfiguration.NameEditChange(Sender: TObject);
begin             
  with (NameEdit) do
    if (Text = '') then
      Color := clRed
    else
      Color := clWhite;

  if (g_MapsCount = 0) then exit;
  if (curMapNum < 0) then exit;
  if (not canInputName) then exit;

  mapPack.maps[curMapNum].name := NameEdit.Text;
  LevelList.Items[curMapNum]   := NameEdit.Text;
end;

procedure TfrmLevelConfiguration.ShowCeilChkChange(Sender: TObject);
begin
  if ((curMapNum < 0) or (curMapNum > High(mapPack.maps))) then exit;
  if (mapPack.maps[curMapNum] = nil) then exit;
  mapPack.maps[curMapNum].showCeil := ShowCeilChk.Checked;
end;


procedure TfrmLevelConfiguration.ShowFloorChkChange(Sender: TObject);
begin
  if ((curMapNum < 0) or (curMapNum > High(mapPack.maps))) then exit;
  if (mapPack.maps[curMapNum] = nil) then exit;
  mapPack.maps[curMapNum].showFloor := ShowFloorChk.Checked;
end;

procedure TfrmLevelConfiguration.ShowFogChkChange(Sender: TObject);
begin
  if ((curMapNum < 0) or (curMapNum > High(mapPack.maps))) then exit;
  if (mapPack.maps[curMapNum] = nil) then exit;
  mapPack.maps[curMapNum].showFog := ShowFogChk.Checked;
end;

procedure TfrmLevelConfiguration.ShowSkyChkChange(Sender: TObject);
begin
  if ((curMapNum < 0) or (curMapNum > High(mapPack.maps))) then exit;
  if (mapPack.maps[curMapNum] = nil) then exit;
  mapPack.maps[curMapNum].showSky := ShowSkyChk.Checked;
end;

procedure TfrmLevelConfiguration.ShowSkyChkSizeConstraintsChange(Sender: TObject);
begin
  if ((curMapNum < 0) or (curMapNum > High(mapPack.maps))) then exit;
  if (mapPack.maps[curMapNum] = nil) then exit;
  mapPack.maps[curMapNum].showSky := ShowSkyChk.Checked;
end;

procedure TfrmLevelConfiguration.SkyboxCmbBoxChange(Sender: TObject);
begin             
  if ((curMapNum < 0) or (curMapNum > High(mapPack.maps))) then exit;
  if (mapPack.maps[curMapNum] = nil) then exit;
  if (g_SkyboxesCount = 0) then exit;

  if (isFormLoaded) then
    mapPack.maps[curMapNum].skyNumber := SkyboxCmbBox.ItemIndex;
end;

procedure TfrmLevelConfiguration.InitForm;
var
  i : Integer;
begin
  oldMapsCount := g_MapsCount;

  LevelCountInfoLbl.Caption := '(1..' + IntToStr(MAPS_MAX_COUNT) + ')';
  LevelCountSpin.MinValue := 1;
  LevelCountSpin.MaxValue := MAPS_MAX_COUNT;
  LevelCountSpin.Value    := g_MapsCount;
  LevelList.Clear;
  for i := 0 to g_MapsCount - 1 do
    LevelList.AddItem(mapPack.maps[i].name, nil);
  LevelList.ItemIndex := prevMapNum;

  // заполняем список скайбоксов
  LoadSkyboxCount;
  SkyboxCmbBox.Items.Clear;
  SkyboxCmbBox.ItemIndex := -1;
  for i := 0 to skyBoxNamesList.Count - 1 do
    SkyboxCmbBox.Items.Add(skyBoxNamesList.Strings[i]);

  LevelListClick(Self);
end;

end.

