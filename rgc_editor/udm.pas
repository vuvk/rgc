unit uDM;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  FileUtil,
  Graphics,
  Dialogs,
  Controls;

{var
  tmpStream : TMemoryStream; }

type

  { TDM }

  TDM = class(TDataModule)
    ColorDialog: TColorDialog;
    ImageList : TImageList;
    OpenTextureDialog : TOpenDialog;
    OpenSoundDialog: TOpenDialog;
  private
  public
  end;

var
  DM : TDM;

const
  g_OpenOptionsMulti : TOpenOptions = [ofReadOnly,ofAllowMultiSelect,ofPathMustExist,ofFileMustExist,ofEnableSizing,ofViewDetail,ofAutoPreview];
  g_OpenOptionsOnce  : TOpenOptions = [ofReadOnly,ofPathMustExist,ofFileMustExist,ofEnableSizing,ofViewDetail,ofAutoPreview];

implementation

{$R *.lfm}

{ TDM }

end.

