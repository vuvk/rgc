unit uSkyboxEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  FileUtil,
  Forms,
  Controls,
  Graphics,
  Dialogs,
  StdCtrls,
  Spin,
  Buttons,
  LCLType,
  ExtCtrls,
  uPleaseWait,
  uSkyboxPreviewer;

type

  { TfrmSkyboxEditor }

  TfrmSkyboxEditor = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Label3: TLabel;
    NameEdit: TEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    ParamsGroupBox: TGroupBox;
    GroupBox4: TGroupBox;
    GroupBox5: TGroupBox;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    GroupBox8: TGroupBox;
    GroupBox9: TGroupBox;
    FrontImg: TImage;
    TopImg: TImage;
    BackImg: TImage;
    LeftImg: TImage;
    RightImg: TImage;
    BottomImg: TImage;
    Label1: TLabel;
    Label2: TLabel;
    SkyboxCountInfoLbl: TLabel;
    SkyboxCountSpin: TSpinEdit;
    SkyboxList: TListBox;
    procedure BackImgMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BottomImgMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FrontImgMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LeftImgMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure NameEditChange(Sender: TObject);
    procedure RightImgMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SkyboxListClick(Sender: TObject);
    procedure SkyboxListSelectionChange(Sender: TObject; User: boolean);
    procedure TopImgMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private           
    namesList : TStringList;
    oldSkyboxNum : Integer;
    curSkyboxNum : Integer;
    oldSkyboxesCount : Integer;
    canInputName : Boolean;    // можно ли вводить имя

    procedure InitForm;

  public

  end;

implementation

uses
  uContainer,
  Imaging,
  ImagingTypes,
  uDM,
  uUtil;

{$R *.lfm}

{ TfrmSkyboxEditor }

procedure TfrmSkyboxEditor.FormCreate(Sender: TObject);
begin
  LoadSkyboxCount;
  namesList := LoadSkyboxNames;

  InitForm;
end;

procedure TfrmSkyboxEditor.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then Close;
end;

procedure TfrmSkyboxEditor.FrontImgMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  image : TImageData;
  stream : TMemoryStream = nil;

  bitmap : TBitmap = nil;
  previewImg : TImage = nil;

  side : Byte = SKYBOX_FRONT;
begin
  previewImg := TImage(Sender);

  if (Button = mbRight) then
    begin
      previewImg.Picture.Clear;
      DeleteSkybox(curSkyboxNum, side);
    end;

  if (Button = mbLeft) then
    if (DM.OpenTextureDialog.Execute) then
      begin
        FreeImage(image);
        PrepareLoadTexture(image, SKYBOX_MAX_WIDTH, SKYBOX_MAX_HEIGHT);

        // записываем изображение в поток
        stream := TMemoryStream.Create;
        stream.Position := 0;
        SaveImageToStream('bmp', stream, image);
        FreeImage(image);

        stream.Position := 0;
        bitmap := TBitmap.Create;
        bitmap.LoadFromStream(stream);
        stream.Destroy;

        // загружаем превью
        LoadPreviewImageFromBitmap(previewImg, bitmap);

        SaveSkybox(curSkyboxNum, bitmap, side);
        bitmap.Destroy;
      end;
end;

procedure TfrmSkyboxEditor.LeftImgMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  image : TImageData;
  stream : TMemoryStream = nil;

  bitmap : TBitmap = nil;
  previewImg : TImage = nil;

  side : Byte = SKYBOX_LEFT;
begin
  previewImg := TImage(Sender);

  if (Button = mbRight) then
    begin
      previewImg.Picture.Clear;
      DeleteSkybox(curSkyboxNum, side);
    end;

  if (Button = mbLeft) then
    if (DM.OpenTextureDialog.Execute) then
      begin
        FreeImage(image);
        PrepareLoadTexture(image, SKYBOX_MAX_WIDTH, SKYBOX_MAX_HEIGHT);

        // записываем изображение в поток
        stream := TMemoryStream.Create;
        stream.Position := 0;
        SaveImageToStream('bmp', stream, image);
        FreeImage(image);

        stream.Position := 0;
        bitmap := TBitmap.Create;
        bitmap.LoadFromStream(stream);
        stream.Destroy;

        // загружаем превью
        LoadPreviewImageFromBitmap(previewImg, bitmap);

        SaveSkybox(curSkyboxNum, bitmap, side);
        bitmap.Destroy;
      end;
end;

procedure TfrmSkyboxEditor.NameEditChange(Sender: TObject);
begin 
  with (NameEdit) do
    if (Text = '') then
      Color := clRed
    else
      Color := clWhite;

  if (g_SkyboxesCount = 0) then exit;
  if (curSkyboxNum < 0) then exit;
  if (not canInputName) then exit;

  namesList.Strings[curSkyboxNum] := NameEdit.Text;
  SkyBoxList.Items[curSkyboxNum]  := NameEdit.Text;
end;

procedure TfrmSkyboxEditor.RightImgMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  image : TImageData;
  stream : TMemoryStream = nil;

  bitmap : TBitmap = nil;
  previewImg : TImage = nil;

  side : Byte = SKYBOX_RIGHT;
begin
  previewImg := TImage(Sender);

  if (Button = mbRight) then
    begin
      previewImg.Picture.Clear;
      DeleteSkybox(curSkyboxNum, side);
    end;

  if (Button = mbLeft) then
    if (DM.OpenTextureDialog.Execute) then
      begin
        FreeImage(image);
        PrepareLoadTexture(image, SKYBOX_MAX_WIDTH, SKYBOX_MAX_HEIGHT);

        // записываем изображение в поток
        stream := TMemoryStream.Create;
        stream.Position := 0;
        SaveImageToStream('bmp', stream, image);
        FreeImage(image);

        stream.Position := 0;
        bitmap := TBitmap.Create;
        bitmap.LoadFromStream(stream);
        stream.Destroy;

        // загружаем превью
        LoadPreviewImageFromBitmap(previewImg, bitmap);

        SaveSkybox(curSkyboxNum, bitmap, side);
        bitmap.Destroy;
      end;
end;

procedure TfrmSkyboxEditor.SkyboxListClick(Sender: TObject);
var
  bitmap : TBitmap = nil;
  frmPleaseWait : TfrmPleaseWait;
begin                          
  ParamsGroupBox.Enabled := False;     
  canInputName := False;
  NameEdit.Text := '<unknown>';

  if (g_SkyboxesCount = 0) then exit;

  ParamsGroupBox.Enabled := True;
  curSkyboxNum := SkyboxList.ItemIndex;
  NameEdit.Text := namesList.Strings[curSkyboxNum];
  if (curSkyboxNum <> oldSkyboxNum) then
    begin
      frmPleaseWait := TfrmPleaseWait.Create(Self);
      frmPleaseWait.Show;
      frmPleaseWait.Repaint;

      bitmap := LoadSkybox(curSkyboxNum, SKYBOX_FRONT);
      LoadPreviewImageFromBitmap(FrontImg, bitmap);
      if (bitmap <> nil) then bitmap.Destroy;

      bitmap := LoadSkybox(curSkyboxNum, SKYBOX_BACK);
      LoadPreviewImageFromBitmap(BackImg, bitmap);
      if (bitmap <> nil) then bitmap.Destroy;

      bitmap := LoadSkybox(curSkyboxNum, SKYBOX_LEFT);
      LoadPreviewImageFromBitmap(LeftImg, bitmap);
      if (bitmap <> nil) then bitmap.Destroy;

      bitmap := LoadSkybox(curSkyboxNum, SKYBOX_RIGHT);
      LoadPreviewImageFromBitmap(RightImg, bitmap);
      if (bitmap <> nil) then bitmap.Destroy;

      bitmap := LoadSkybox(curSkyboxNum, SKYBOX_TOP);
      LoadPreviewImageFromBitmap(TopImg, bitmap);
      if (bitmap <> nil) then bitmap.Destroy;

      bitmap := LoadSkybox(curSkyboxNum, SKYBOX_BOTTOM);
      LoadPreviewImageFromBitmap(BottomImg, bitmap);
      if (bitmap <> nil) then bitmap.Destroy;

      frmPleaseWait.Destroy;
    end;          
  oldSkyboxNum := curSkyboxNum;
  canInputName := True;
end;

procedure TfrmSkyboxEditor.BackImgMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  image : TImageData;
  stream : TMemoryStream = nil;

  bitmap : TBitmap = nil;
  previewImg : TImage = nil;

  side : Byte = SKYBOX_BACK;
begin
  previewImg := TImage(Sender);

  if (Button = mbRight) then
    begin
      previewImg.Picture.Clear;
      DeleteSkybox(curSkyboxNum, side);
    end;

  if (Button = mbLeft) then
    if (DM.OpenTextureDialog.Execute) then
      begin
        FreeImage(image);
        PrepareLoadTexture(image, SKYBOX_MAX_WIDTH, SKYBOX_MAX_HEIGHT);

        // записываем изображение в поток
        stream := TMemoryStream.Create;
        stream.Position := 0;
        SaveImageToStream('bmp', stream, image);
        FreeImage(image);

        stream.Position := 0;
        bitmap := TBitmap.Create;
        bitmap.LoadFromStream(stream);
        stream.Destroy;

        // загружаем превью
        LoadPreviewImageFromBitmap(previewImg, bitmap);

        SaveSkybox(curSkyboxNum, bitmap, side);
        bitmap.Destroy;
      end;
end;

procedure TfrmSkyboxEditor.BitBtn1Click(Sender: TObject);
var
  frmSkyboxPreviewer : TfrmSkyboxPreviewer = nil;
begin            
  frmSkyboxPreviewer := TfrmSkyboxPreviewer.Create(Self);
  with (frmSkyboxPreviewer) do
    try
      frmSkyboxPreviewer.skyboxNum := curSkyboxNum;
      ShowModal;
    finally
      Destroy;
    end;
end;

procedure TfrmSkyboxEditor.BitBtn2Click(Sender: TObject);
begin                
  SkyboxCountSpin.Value := oldSkyboxesCount;
end;

procedure TfrmSkyboxEditor.BitBtn3Click(Sender: TObject);
var
  i : Integer;
begin
  if (oldSkyboxesCount = SkyboxCountSpin.Value) then exit;

  // пользователь решил добавить карт
  if (oldSkyboxesCount < SkyboxCountSpin.Value) then
    begin
      g_SkyboxesCount := SkyboxCountSpin.Value;
      SaveSkyboxCount;
      namesList.Add('Skybox #' + IntToStr(namesList.Count));
      SaveSkyboxNames(namesList);
    end
  else
    // пользователь решил урезать количество карт
    begin
      if (MessageDlg('WARNING!',
                     'Are you sure want to delete old skyboxes?',
                     mtWarning,
                     [mbNo, mbYes],
                     0) = mrNo) then
        begin
          SkyboxCountSpin.Value := oldSkyboxesCount;
          exit;
        end;

      for i := SkyboxCountSpin.Value to SKYBOXES_MAX_COUNT - 1 do
        begin
          DeleteSkybox(i, SKYBOX_ALL_SIDES);
          if (namesList.Count > 0) then
            namesList.Delete(namesList.Count - 1);
        end;
      g_SkyboxesCount := SkyboxCountSpin.Value;

      SaveSkyboxCount;    
      SaveSkyboxNames(namesList);
    end;

  InitForm;
end;

procedure TfrmSkyboxEditor.BottomImgMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  image : TImageData;
  stream : TMemoryStream = nil;

  bitmap : TBitmap = nil;
  previewImg : TImage = nil;

  side : Byte = SKYBOX_BOTTOM;
begin
  previewImg := TImage(Sender);

  if (Button = mbRight) then
    begin
      previewImg.Picture.Clear;
      DeleteSkybox(curSkyboxNum, side);
    end;

  if (Button = mbLeft) then
    if (DM.OpenTextureDialog.Execute) then
      begin
        FreeImage(image);
        PrepareLoadTexture(image, SKYBOX_MAX_WIDTH, SKYBOX_MAX_HEIGHT);

        // записываем изображение в поток
        stream := TMemoryStream.Create;
        stream.Position := 0;
        SaveImageToStream('bmp', stream, image);
        FreeImage(image);

        stream.Position := 0;
        bitmap := TBitmap.Create;
        bitmap.LoadFromStream(stream);
        stream.Destroy;

        // загружаем превью
        LoadPreviewImageFromBitmap(previewImg, bitmap);

        SaveSkybox(curSkyboxNum, bitmap, side);
        bitmap.Destroy;
      end;
end;

procedure TfrmSkyboxEditor.FormActivate(Sender: TObject);
begin
  SetFocus;
end;

procedure TfrmSkyboxEditor.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  SaveSkyboxCount;
  SaveSkyboxNames(namesList);
  namesList.Destroy;
  namesList := nil;
end;

procedure TfrmSkyboxEditor.SkyboxListSelectionChange(Sender: TObject; User: boolean);
begin
  SkyboxListClick(Self);
end;

procedure TfrmSkyboxEditor.TopImgMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  image : TImageData;
  stream : TMemoryStream = nil;

  bitmap : TBitmap = nil;
  previewImg : TImage = nil;

  side : Byte = SKYBOX_TOP;
begin
  previewImg := TImage(Sender);

  if (Button = mbRight) then
    begin
      previewImg.Picture.Clear;
      DeleteSkybox(curSkyboxNum, side);
    end;

  if (Button = mbLeft) then
    if (DM.OpenTextureDialog.Execute) then
      begin
        FreeImage(image);
        PrepareLoadTexture(image, SKYBOX_MAX_WIDTH, SKYBOX_MAX_HEIGHT);

        // записываем изображение в поток
        stream := TMemoryStream.Create;
        stream.Position := 0;
        SaveImageToStream('bmp', stream, image);
        FreeImage(image);

        stream.Position := 0;
        bitmap := TBitmap.Create;
        bitmap.LoadFromStream(stream);
        stream.Destroy;

        // загружаем превью
        LoadPreviewImageFromBitmap(previewImg, bitmap);

        SaveSkybox(curSkyboxNum, bitmap, side);
        bitmap.Destroy;
      end;
end;

procedure TfrmSkyboxEditor.InitForm;
var
  i : Integer;
begin                     
  oldSkyboxNum := -1;
  curSkyboxNum := -1;

  oldSkyboxesCount := g_SkyboxesCount;
  canInputName := False;

  FrontImg.Picture.Clear;
  BackImg.Picture.Clear;
  LeftImg.Picture.Clear;
  RightImg.Picture.Clear;
  TopImg.Picture.Clear;
  BottomImg.Picture.Clear;

  SkyboxCountSpin.MinValue := 0;
  SkyboxCountSpin.MaxValue := SKYBOXES_MAX_COUNT;
  SkyboxCountSpin.Value    := g_SkyboxesCount;
  SkyboxCountInfoLbl.Caption := '(0..' + IntToStr(SKYBOXES_MAX_COUNT) + ')';
  SkyboxList.Clear;
  ParamsGroupBox.Enabled := False;

  if ((g_SkyboxesCount = 0) or
      (namesList = nil) or
      (namesList.Count = 0)) then
    exit;

  for i := 0 to namesList.Count - 1 do
    SkyboxList.AddItem(namesList.Strings[i], nil);
  SkyboxList.ItemIndex := 0;
  SkyboxListClick(Self);
end;

end.

