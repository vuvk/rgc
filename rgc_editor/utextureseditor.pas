unit uTexturesEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  FileUtil,
  Forms,
  Controls,
  Graphics,
  GraphType,
  Dialogs,
  StdCtrls,
  Buttons,
  LCLType,
  ExtCtrls,
  Spin,
  Grids,
  Imaging,
  ImagingTypes, uPleaseWait,
  Types;

type

  { TfrmTexturesEditor }

  TfrmTexturesEditor = class(TForm)
    AddFrameBtn: TSpeedButton;
    Animator : TTimer;
    AnimSpeedSpin: TSpinEdit;
    ClearFrameBtn: TSpeedButton;
    ClearImageBtn : TSpeedButton;
    DeleteImageBtn : TSpeedButton;
    DeleteFrameBtn: TSpeedButton;
    frm_play_btn: TBitBtn;
    GroupBox1 : TGroupBox;
    GroupBox3 : TGroupBox;
    ParamsGroupBox: TGroupBox;
    Label13: TLabel;
    LoadFrameBtn: TSpeedButton;
    LoadImageBtn : TSpeedButton;
    AddImageBtn : TSpeedButton;
    CheckBox1: TCheckBox;
    GroupBox2: TGroupBox;
    PreviewImage : TImage;
    TexturesGrid : TDrawGrid;
    FramesGrid: TDrawGrid;
    procedure AddFrameBtnClick(Sender: TObject);
    procedure AnimatorTimer(Sender : TObject);
    procedure AnimSpeedSpinChange(Sender : TObject);
    procedure ClearFrameBtnClick(Sender : TObject);
    procedure DeleteFrameBtnClick(Sender: TObject);
    procedure DeleteImageBtnClick(Sender : TObject);
    procedure FormActivate(Sender: TObject);
    procedure FramesGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure FramesGridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure frm_play_btn_oldClick(Sender : TObject);
    procedure LoadFrameBtnClick(Sender : TObject);
    procedure AddImageBtnClick(Sender : TObject);
    procedure LoadImageBtnClick(Sender: TObject);
    procedure ClearImageBtnClick(Sender: TObject);
    procedure CheckBox1Change(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender : TObject; var Key : Word; Shift : TShiftState
      );
    procedure TexturesGridDrawCell(Sender : TObject; aCol, aRow : Integer;
      aRect : TRect; aState : TGridDrawState);
    procedure TexturesGridSelectCell(Sender : TObject; aCol, aRow : Integer;
      var CanSelect : Boolean);
  private     
    texNum : Integer;
    frmTexNum : Integer;
    rawImage : TRawImage;

    procedure FormPrepare;
    procedure AnimatorDisable;
    procedure AnimatorEnable;
    procedure TexturesGridUpdate;    
    procedure FramesGridUpdate;
  public

  end;

implementation

uses
  uUtil,
  uContainer,
  uDM, uMain;

{$R *.lfm}

{ TfrmTexturesEditor }

procedure TfrmTexturesEditor.CheckBox1Change(Sender: TObject);
begin
  PreviewImage.Stretch := CheckBox1.Checked;
end;

procedure TfrmTexturesEditor.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  SaveTexturePack;
  //frmMain.Enabled := True;
end;

procedure TfrmTexturesEditor.FormCreate(Sender: TObject);
begin
  FormPrepare;

  TexturesGridUpdate; 

  DoubleBuffered := True;
  PreviewImage.Parent.DoubleBuffered := True;
end;

procedure TfrmTexturesEditor.FormKeyDown(Sender : TObject; var Key : Word;
  Shift : TShiftState);
begin
  if (Key = VK_ESCAPE) then Close;
end;

procedure TfrmTexturesEditor.TexturesGridDrawCell(Sender : TObject; aCol,
  aRow : Integer; aRect : TRect; aState : TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_TexturesCount = 0) then exit; 
  if ((texNum < 0) or (texNum >= g_TexturesCount)) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;                 
  if (texturesInMem[pos].framesCount = 0) then exit;
  bmp := texturesInMem[pos].frames[0];

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmTexturesEditor.TexturesGridSelectCell(Sender : TObject; aCol,
  aRow : Integer; var CanSelect : Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  FormPrepare;

  if (g_TexturesCount = 0) then exit;

  LoadImageBtn.Enabled   := True;
  ClearImageBtn.Enabled  := True;
  DeleteImageBtn.Enabled := True;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;    
  texNum := pos;
  frmTexNum := 0;
  if (texturesInMem[pos].framesCount = 0) then exit;
  bmp := texturesInMem[pos].frames[0];
  if (bmp = nil) then exit;

  LoadPreviewImageFromBitmap(PreviewImage, bmp);
  ParamsGroupBox.Enabled := True;
  AnimSpeedSpin.Value    := texturesInMem[pos].animSpeed;

  FramesGridUpdate;
end;

procedure TfrmTexturesEditor.FormPrepare;
begin
  AnimatorDisable;

  PreviewImage.Picture.Clear;
  ParamsGroupBox.Enabled := False;
  FramesGrid.ColCount := 0;
  FramesGrid.RowCount := 0;

  texNum := -1;
  frmTexNum := -1;

  LoadFrameBtn.Enabled   := False;
  ClearFrameBtn.Enabled  := False;
  DeleteFrameBtn.Enabled := False;

  LoadImageBtn.Enabled   := False;
  ClearImageBtn.Enabled  := False;
  DeleteImageBtn.Enabled := False;
end;

procedure TfrmTexturesEditor.AnimatorDisable;
begin
  DM.ImageList.GetRawImage(0, rawImage);
  frm_play_btn.Glyph.Clear;
  frm_play_btn.Glyph.LoadFromRawImage(rawImage, false);

  Animator.Enabled := False;
  frm_play_btn.Caption := 'Play';

  //if (texNum < g_TexturesCount) then
  //  with (texturesInMem[texNum]) do
  //    if (frmTexNum < framesCount) then
  //      LoadPreviewImageFromBitmap(PreviewImage, frames[frmTexNum]);
end;

procedure TfrmTexturesEditor.AnimatorEnable;
begin
  DM.ImageList.GetRawImage(1, rawImage);
  frm_play_btn.Glyph.Clear;
  frm_play_btn.Glyph.LoadFromRawImage(rawImage, false);

  if (AnimSpeedSpin.Value <> 0) then
    Animator.Interval := 1000 div AnimSpeedSpin.Value
  else
    Animator.Interval := 0;
  Animator.Enabled  := True;
  frm_play_btn.Caption := 'Stop';
end;

procedure TfrmTexturesEditor.TexturesGridUpdate;
var
  i : Integer;
begin
  if (g_TexturesCount = 0) then
    begin
      TexturesGrid.ColCount := 0;
      TexturesGrid.RowCount := 0;
      exit;
    end;

  // формируем грид с текстурами
  TexturesGrid.ColCount := 1;
  TexturesGrid.RowCount := g_TexturesCount;

  for i := 0 to TexturesGrid.ColCount - 1 do
    TexturesGrid.ColWidths [i] := 96;
  for i := 0 to TexturesGrid.RowCount - 1 do
    TexturesGrid.RowHeights[i] := 96;
  TexturesGrid.Invalidate;
end;

procedure TfrmTexturesEditor.FramesGridUpdate;
var
  i : Integer;
begin
  if (texturesInMem[texNum].framesCount = 0) then
    begin
      FramesGrid.ColCount := 0;
      FramesGrid.RowCount := 0;
      exit;
    end;

  // формируем грид с текстурами
  FramesGrid.ColCount := 1;
  FramesGrid.RowCount := texturesInMem[texNum].framesCount;

  for i := 0 to FramesGrid.ColCount - 1 do
    FramesGrid.ColWidths [i] := 48;
  for i := 0 to FramesGrid.RowCount - 1 do
    FramesGrid.RowHeights[i] := 48;
  FramesGrid.Invalidate;
end;

procedure TfrmTexturesEditor.LoadImageBtnClick(Sender: TObject);
var
  i : Integer;
  image  : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean; 
  filesCount : Integer;     
  frmPleaseWait : TfrmPleaseWait;
begin 
  AnimatorDisable;

  if ((texturesInMem[texNum].framesCount > 0) and
      (MessageDlg('Warning!',
                  'Do you really want to load new texture? It''s event clear all old frames!',
                  mtWarning,
                  [mbYes, mbNo],
                  0) = mrNo)) then
     exit;
             
  frmPleaseWait := TfrmPleaseWait.Create(Self);
  DM.OpenTextureDialog.Options := g_OpenOptionsMulti;

  if (DM.OpenTextureDialog.Execute) then
    begin            
      frmPleaseWait.Show;
      frmPleaseWait.Repaint;

      InitTexture(texNum);

      filesCount := DM.OpenTextureDialog.Files.Count;
      texturesInMem[texNum].framesCount := filesCount;
      SetLength(texturesInMem[texNum].frames, filesCount);
      //texturesInMem[texNum].name := 'Texture #' + IntToStr(texNum);

      stream := TMemoryStream.Create;
      for i := 0 to filesCount - 1 do
        begin
          texturesInMem[texNum].frames[i] := TBitmap.Create;

          PrepareLoadTexture(image, DM.OpenTextureDialog.Files[i], TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

          // записываем изображение в поток
          stream.Position := 0;
          SaveImageToStream('bmp', stream, image);  
          FreeImage(image);

          stream.Position := 0;
          texturesInMem[texNum].frames[i].LoadFromStream(stream);
          stream.Clear;
        end;
      stream.Destroy;

      TexturesGridUpdate;
      TexturesGridSelectCell(TexturesGrid, 0, texNum, canSelect);
      GridSellectRow(TexturesGrid, texNum);
    end;

  frmPleaseWait.Destroy;
  DM.OpenTextureDialog.Options := g_OpenOptionsOnce;
end;

procedure TfrmTexturesEditor.LoadFrameBtnClick(Sender : TObject);
var
  image : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;
begin
  AnimatorDisable;

  if (DM.OpenTextureDialog.Execute) then
    begin
      PrepareLoadTexture(image, TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

      // записываем изображение в поток
      stream := TMemoryStream.Create;
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image);  
      FreeImage(image);

      stream.Position := 0;
      with (texturesInMem[texNum]) do
        begin
          if (frames[frmTexNum] = nil) then
            frames[frmTexNum] := TBitmap.Create;
          frames[frmTexNum].LoadFromStream(stream);
        end;
      stream.Destroy;    

      FramesGridUpdate;
      FramesGridSelectCell(FramesGrid, 0, frmTexNum, canSelect); 
      GridSellectRow(FramesGrid, frmTexNum);
    end;
end;

procedure TfrmTexturesEditor.AddImageBtnClick(Sender : TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable;

  if (g_TexturesCount < TEXTURES_MAX_COUNT) then
    begin
      texNum := g_TexturesCount;

      Inc(g_TexturesCount);  
      TexturesGridUpdate;
      TexturesGrid.Col := 1;
      TexturesGrid.Row := g_TexturesCount;

      LoadImageBtnClick(Self);

      //texturesInMem[texNum].name := 'Texture #' + IntToStr(texNum);
    end
  else
    MessageDlg('Error', 'Достигнут предел текстур. Текущий максимум = ' + IntToStr(TEXTURES_MAX_COUNT), mtError, [mbOK], 0); 

  TexturesGridSelectCell(TexturesGrid, 0, texNum, canSelect);
  GridSellectRow(TexturesGrid, texNum);
end;

procedure TfrmTexturesEditor.frm_play_btn_oldClick(Sender : TObject);
begin           
  if (g_TexturesCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_TexturesCount)) then exit;
  if (texturesInMem[texNum].framesCount = 0) then exit;

  if (Animator.Enabled) then
    begin
      AnimatorDisable;
      if (texNum < g_TexturesCount) then
        with (texturesInMem[texNum]) do
          if (frmTexNum < framesCount) then
            LoadPreviewImageFromBitmap(PreviewImage, frames[frmTexNum]);
    end
  else
    AnimatorEnable;
end;

procedure TfrmTexturesEditor.ClearFrameBtnClick(Sender : TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable; 

  PreviewImage.Picture.Clear;

  with (texturesInMem[texNum]) do
    begin
      if (frames[frmTexNum] = nil) then exit;

      frames[frmTexNum].Clear;
      frames[frmTexNum].Destroy;
      frames[frmTexNum] := nil;
    end;

  DeleteFile(g_TexPath + '/texture_' + IntToStr(texNum) + '_frame_' + IntToStr(frmTexNum) + '.dat');

  FramesGridUpdate;
  FramesGridSelectCell(FramesGrid, 0, frmTexNum, canSelect);
  GridSellectRow(FramesGrid, frmTexNum);
end;

procedure TfrmTexturesEditor.DeleteFrameBtnClick(Sender: TObject);
var
  i : Integer;
  canSelect : Boolean;
begin
  ClearFrameBtnClick(Self);

  with (texturesInMem[texNum]) do
    begin
      // сначала переносим все текстуры вниз
      for i := frmTexNum to framesCount - 2 do
        frames[i] := frames[i + 1];

      Dec(framesCount);
      Dec(frmTexNum);  

      SetLength(frames, framesCount);
    end;           

  FramesGridUpdate;  
  FramesGridSelectCell(FramesGrid, 0, frmTexNum, canSelect);
  GridSellectRow(FramesGrid, frmTexNum);
end;

procedure TfrmTexturesEditor.DeleteImageBtnClick(Sender : TObject);
var
  canSelect : Boolean;
  i : Integer;
  x, y, m : Integer;
  cell : Integer;
begin
  AnimatorDisable;

  if (g_TexturesCount = 0) then
    exit;

  if (MessageDlg('Warning!',
                 'Do you really want to delete texture? It''s event clear all frames!',
                 mtWarning,
                 [mbYes, mbNo],
                 0) = mrNo) then
     exit;
                
  DeleteFile(g_TexPath + '/texture_' + IntToStr(texNum) + '.cfg');
  for i := 0 to High(texturesInMem[texNum].frames) do
    DeleteFile(g_TexPath + '/texture_' + IntToStr(texNum) + '_frame_' + IntToStr(i) + '.dat');

  InitTexture(texNum);

  // удаляем на картах элементы, которые использовали удаленную текстуру
  //CheckMaps;
  if (texNum < g_TexturesCount) then
    begin
      // сдвигаем назад все элементы на карте
      if (texNum >= 0) then
        for m := 0 to g_MapsCount - 1 do
          with (mapPack.maps[m]) do
            begin
              for x := 0 to MAP_WIDTH  - 1 do
                for y := 0 to MAP_HEIGHT - 1 do
                  begin
                    cell := floor[x, y] - 1;
                    if ((cell >= 0) and (cell < TEXTURES_MAX_COUNT) and (cell > texNum)) then
                      floor[x, y] := cell
                    else
                      floor[x, y] := 0;

                    cell := level[x, y] - 1;
                    if ((cell >= 0) and (cell < TEXTURES_MAX_COUNT) and (cell > texNum)) then
                      level[x, y] := cell
                    else
                      level[x, y] := 0;

                    cell := ceil[x, y] - 1;
                    if ((cell >= 0) and (cell < TEXTURES_MAX_COUNT) and (cell > texNum)) then
                      ceil[x, y] := cell
                    else
                      ceil[x, y] := 0;
                  end;
            end;

      // теперь отодвигаем назад все текстуры
      for i := texNum to TEXTURES_MAX_COUNT - 2 do
        texturesInMem[i] := texturesInMem[i + 1];
      InitTexture(g_TexturesCount - 1);
    end;
            
  Dec(g_TexturesCount);
  TexturesGridUpdate;
  TexturesGridSelectCell(TexturesGrid, 0, g_TexturesCount - 1, canSelect);
  GridSellectRow(TexturesGrid, texNum);

  if (g_TexturesCount = 0) then
    begin
      LoadImageBtn.Enabled   := False;
      ClearImageBtn.Enabled  := False;
      DeleteImageBtn.Enabled := False;
      ParamsGroupBox.Enabled := False;
      PreviewImage.Picture.Clear;
    end;
end;

procedure TfrmTexturesEditor.FormActivate(Sender: TObject);
begin
  frmMain.Enabled := False;
  //SetFocus;
end;

procedure TfrmTexturesEditor.FramesGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin     
  if (g_TexturesCount = 0) then exit;
  //if ((texNum < 0) or (texNum >= g_TexturesCount)) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (texturesInMem[texNum]) do
    begin
      if (framesCount = 0) then exit;  
      if (pos >= framesCount) then exit;
      bmp := frames[pos];
    end;

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmTexturesEditor.FramesGridSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  AnimatorDisable;

  PreviewImage.Picture.Clear;

  LoadFrameBtn.Enabled   := False;
  ClearFrameBtn.Enabled  := False;
  DeleteFrameBtn.Enabled := False;

  frmTexNum := 0;
  if ((texNum < 0) or
      (texNum >= Length(texturesInMem)) or
      (texturesInMem[texNum].framesCount = 0)) then
    exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (texturesInMem[texNum]) do
    begin
      if (pos >= framesCount) then exit;  

      frmTexNum := pos;
      if (pos > 0) then
        begin             
          LoadFrameBtn.Enabled   := True;
          ClearFrameBtn.Enabled  := True;
          DeleteFrameBtn.Enabled := True;
        end;
      bmp := frames[pos];
      if (bmp = nil) then exit;

      LoadPreviewImageFromBitmap(PreviewImage, bmp);
    end;
end;

procedure TfrmTexturesEditor.AnimSpeedSpinChange(Sender : TObject);
begin
  if (g_TexturesCount = 0) then exit;

  texturesInMem[texNum].animSpeed := AnimSpeedSpin.Value;
  if (AnimSpeedSpin.Value <> 0) then
    Animator.Interval := 1000 div AnimSpeedSpin.Value
  else
    Animator.Interval := 0;
end;

procedure TfrmTexturesEditor.AnimatorTimer(Sender : TObject);
const
  animFrame : Integer = -1;
var
  framesCount : UInt32;
begin
  framesCount := texturesInMem[texNum].framesCount;

  Inc(animFrame);
  if (animFrame >= framesCount) then
    animFrame := 0;

  while (texturesInMem[texNum].frames[animFrame] = nil) do
    begin
      Inc(animFrame);
      if (animFrame >= framesCount) then
        animFrame := 0;
    end;

  LoadPreviewImageFromBitmap(PreviewImage, texturesInMem[texNum].frames[animFrame]);
end;

procedure TfrmTexturesEditor.AddFrameBtnClick(Sender: TObject);
begin
  AnimatorDisable;

  with (texturesInMem[texNum]) do
    begin     
      frmTexNum := framesCount;
      Inc(framesCount);
      SetLength(frames, framesCount);
      frames[frmTexNum] := nil;  

      FramesGridUpdate;
      FramesGrid.Col := 1;
      FramesGrid.Row := framesCount;
    end;

  LoadFrameBtnClick(Self);

  FramesGrid.Invalidate;
end;

procedure TfrmTexturesEditor.ClearImageBtnClick(Sender: TObject);
var
  canSelect : Boolean;
  i : Integer;
begin
  AnimatorDisable;

  if (MessageDlg('Warning!',
                 'Do you really want to clear texture? It''s event clear all frames!',
                 mtWarning,
                 [mbYes, mbNo],
                 0) = mrNo) then
     exit;
               
  DeleteFile(g_TexPath + '/texture_' + IntToStr(texNum) + '.cfg');
  for i := 0 to High(texturesInMem[texNum].frames) do
    DeleteFile(g_TexPath + '/texture_' + IntToStr(texNum) + '_frame_' + IntToStr(i) + '.dat');

  InitTexture(texNum);
  //texturesInMem[texNum].name := 'Texture #' + IntToStr(texNum);

  TexturesGridUpdate;
  TexturesGridSelectCell(TexturesGrid, 0, texNum, canSelect);   
  GridSellectRow(TexturesGrid, texNum);
end;

end.

