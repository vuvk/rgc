unit uSpritesEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  FileUtil,
  SpinEx,
  Forms,
  Controls,
  Graphics,
  GraphType,
  Dialogs,
  ExtCtrls,
  Buttons,
  StdCtrls,
  Spin,
  Imaging,
  ImagingTypes, uPleaseWait,
  LCLType,
  Grids,
  ValEdit,
  CheckLst,
  DBGrids,
  Types,
  Math;

type

  { TfrmSpritesEditor }

  TfrmSpritesEditor = class(TForm)
    AddFrameBtn: TSpeedButton;
    AddSpriteBtn: TSpeedButton;
    AnimSpeedSpin: TSpinEdit;
    ClearDeathSoundBtn: TSpeedButton;
    ClearFrameBtn: TSpeedButton;
    ClearSpriteBtn: TSpeedButton;
    DeleteFrameBtn: TSpeedButton;
    DeleteSpriteBtn: TSpeedButton;
    DeathSoundsCmbBox: TComboBox;
    Label6: TLabel;
    PlayOnceChk: TCheckBox;
    DeleteOnLastFrameChk: TCheckBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    NameEdit: TEdit;
    ScaleXSpin: TFloatSpinEdit;
    EnduranceSpin: TFloatSpinEdit;
    SolidXSpin: TFloatSpinEdit;
    ScaleYSpin: TFloatSpinEdit;
    FramesGrid: TDrawGrid;
    frm_play_btn: TBitBtn;
    SolidYSpin: TFloatSpinEdit;
    AutosizeBtn: TSpeedButton;
    SpritesGroupBox: TGroupBox;
    GroupBox2: TGroupBox;
    SpritesEditGroupBox: TGroupBox;
    Label1: TLabel;
    Label13: TLabel;
    LoadFrameBtn: TSpeedButton;
    LoadSpriteBtn: TSpeedButton;
    FramesGroupBox: TGroupBox;
    PreviewImage: TImage;
    RotationRadioGroup : TRadioGroup;
    DeleteColorBtn: TSpeedButton;
    DeleteColorShp: TShape;
    StretchImageCheck : TCheckBox;
    DetectCollisionChk : TCheckBox;
    DestroyableChk : TCheckBox;
    ParamsGroupBox : TGroupBox;
    Label5 : TLabel;
    Animator : TTimer;
    SpritesGrid: TDrawGrid;
    ValueListEditor1: TValueListEditor;
    procedure AddFrameBtnClick(Sender: TObject);
    procedure AddSpriteBtnClick(Sender: TObject);
    procedure AnimatorTimer(Sender : TObject);
    procedure AnimSpeedSpinChange(Sender : TObject);
    procedure ClearDeathSoundBtnClick(Sender: TObject);
    procedure ClearFrameBtnClick(Sender : TObject);
    procedure ClearSpriteBtnClick(Sender : TObject);
    procedure DeathSoundsCmbBoxChange(Sender: TObject);
    procedure DeleteFrameBtnClick(Sender: TObject);
    procedure DeleteOnLastFrameChkChange(Sender: TObject);
    procedure DeleteSpriteBtnClick(Sender: TObject);
    procedure DestroyableChkChange(Sender : TObject);
    procedure DetectCollisionChkChange(Sender : TObject);
    procedure EnduranceSpinChange(Sender : TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender : TObject; var CloseAction : TCloseAction);
    procedure FormCreate(Sender : TObject);
    procedure FormKeyDown(Sender : TObject; var Key : Word; Shift : TShiftState
      );
    procedure FramesGridClick(Sender: TObject);
    procedure FramesGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure FramesGridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure frm_play_btnClick(Sender : TObject);
    procedure LoadFrameBtnClick(Sender : TObject);
    procedure LoadSpriteBtnClick(Sender : TObject);
    procedure NameEditChange(Sender: TObject);
    procedure ParamsGroupBoxClick(Sender: TObject);
    procedure PlayOnceChkChange(Sender: TObject);
    procedure PreviewImageClick(Sender: TObject);
    procedure PreviewImageMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PreviewImageMouseEnter(Sender: TObject);
    procedure PreviewImageMouseLeave(Sender: TObject);
    procedure PreviewImageMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PreviewImageMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PreviewImagePaint(Sender: TObject);
    procedure RotationRadioGroupSizeConstraintsChange(Sender : TObject);
    procedure DeleteColorBtnClick(Sender: TObject);
    procedure ScaleXSpinChange(Sender: TObject);
    procedure ScaleYSpinChange(Sender: TObject);
    procedure SolidXSpinChange(Sender: TObject);
    procedure SolidYSpinChange(Sender: TObject);
    procedure SolidYSpinChangeBounds(Sender: TObject);
    procedure AutosizeBtnClick(Sender: TObject);
    procedure SpritesGridSelection(Sender: TObject; aCol, aRow: Integer);
    procedure StretchImageCheckChange(Sender : TObject);
    procedure SpritesGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure SpritesGridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
  private            
    sprTexNum : Integer;
    frmTexNum : Integer;

    rawImage : TRawImage;

    deleteColorPos : TPoint;    // позиция на реальном изображении для взятия цвета под удаление
    deleteColorMode : Boolean;  // режим удаления цвета?
    canInput : Boolean;         // можно ли вводить значения
    canChooseSolid : Boolean;   // можно ли указать ограничивающую область мышкой

    procedure FormPrepare;
    procedure AnimatorDisable;
    procedure AnimatorEnable;  
    procedure SpritesGridUpdate;
    procedure FramesGridUpdate;
    procedure DeleteColorModeEnable;
    procedure DeleteColorModeDisable;
  public

  end;

implementation

uses
  uContainer,
  uDM,
  uUtil;

{$R *.lfm}

{ TfrmSpritesEditor }


procedure TfrmSpritesEditor.StretchImageCheckChange(Sender : TObject);
begin
  PreviewImage.Stretch := StretchImageCheck.Checked;
end;

procedure TfrmSpritesEditor.SpritesGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin       
  if (g_SpritesCount = 0) then exit;
  //if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (spritesInMem[pos].framesCount = 0) then exit;
  bmp := spritesInMem[pos].frames[0];

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmSpritesEditor.SpritesGridSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  FormPrepare();

  if (g_SpritesCount = 0) then exit;

  LoadSpriteBtn.Enabled   := True;
  ClearSpriteBtn.Enabled  := True;
  DeleteSpriteBtn.Enabled := True;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  sprTexNum := pos;
  frmTexNum := 0;
  if (spritesInMem[pos].framesCount = 0) then exit;
  bmp := spritesInMem[pos].frames[0];
  if (bmp = nil) then exit;

  LoadPreviewImageFromBitmap(PreviewImage, bmp);
  ParamsGroupBox.Enabled := True;
  FramesGroupBox.Enabled := True; 
  DeleteColorBtn.Enabled := True; 

  with (spritesInMem[pos]) do
    begin
      NameEdit.Text              := name;
      DetectCollisionChk.Checked := collision;
      DestroyableChk.Checked     := destroyable;
      DeathSoundsCmbBox.ItemIndex:= deathSoundId;
      EnduranceSpin.Value        := endurance;
      PlayOnceChk.Checked        := playOnce;
      DeleteOnLastFrameChk.Checked := deleteOnLastFrame;
      RotationRadioGroup.ItemIndex := axisParams;
      AnimSpeedSpin.Value        := animSpeed;
      ScaleXSpin.Value           := scaleX;   
      ScaleYSpin.Value           := scaleY;       
      SolidXSpin.Value           := solidX;
      SolidYSpin.Value           := solidY;
    end;
               
  canInput := True;
  FramesGridUpdate;
end;

procedure TfrmSpritesEditor.FormPrepare;
var
  i : Integer;
begin           
  canInput := False;
  AnimatorDisable;
  DeleteColorModeDisable;
                             
  with (DeathSoundsCmbBox) do
    begin
      Items.Clear;
      //Items.Add('');
      for i := 0 to g_SoundsCount - 1 do
        Items.Add(soundsInMem[i].name);
      ItemIndex := -1;
    end;

  PreviewImage.Picture.Clear;
  ParamsGroupBox.Enabled := False;
  FramesGroupBox.Enabled := False;

  FramesGrid.ColCount := 0;
  FramesGrid.RowCount := 0;

  sprTexNum := -1;
  frmTexNum := -1;

  LoadFrameBtn.Enabled   := False;
  ClearFrameBtn.Enabled  := False;
  DeleteFrameBtn.Enabled := False;
  DeleteColorBtn.Enabled := False;

  LoadSpriteBtn.Enabled   := False;
  ClearSpriteBtn.Enabled  := False;
  DeleteSpriteBtn.Enabled := False;

  NameEdit.Text := '<unknown>';
end;

procedure TfrmSpritesEditor.AnimatorDisable;
begin
  DM.ImageList.GetRawImage(0, rawImage);
  frm_play_btn.Glyph.Clear;
  frm_play_btn.Glyph.LoadFromRawImage(rawImage, False);

  Animator.Enabled := False;
  frm_play_btn.Caption := 'Play';    

  //if (sprTexNum < g_SpritesCount) then
  //  with (spritesInMem[sprTexNum]) do
  //    if (frmTexNum < framesCount) then
  //      LoadPreviewImageFromBitmap(PreviewImage, frames[frmTexNum]);
end;

procedure TfrmSpritesEditor.AnimatorEnable;
begin
  DM.ImageList.GetRawImage(1, rawImage);
  frm_play_btn.Glyph.Clear;
  frm_play_btn.Glyph.LoadFromRawImage(rawImage, false);

  if (AnimSpeedSpin.Value <> 0) then
    Animator.Interval := 1000 div AnimSpeedSpin.Value
  else
    Animator.Interval := 0;
  Animator.Enabled  := True;   
  frm_play_btn.Caption := 'Stop';
end;

procedure TfrmSpritesEditor.SpritesGridUpdate;
var
  i : Integer;
begin
  if (g_SpritesCount = 0) then
    begin
      SpritesGrid.ColCount := 0;
      SpritesGrid.RowCount := 0;
      exit;
    end;

  // формируем грид со спрайтами
  SpritesGrid.ColCount := 1;
  SpritesGrid.RowCount := g_SpritesCount;

  for i := 0 to SpritesGrid.ColCount - 1 do
    SpritesGrid.ColWidths [i] := 96;
  for i := 0 to SpritesGrid.RowCount - 1 do
    SpritesGrid.RowHeights[i] := 96;
  SpritesGrid.Invalidate;
end;

procedure TfrmSpritesEditor.FramesGridUpdate;
var
  i : Integer;
begin
  if (spritesInMem[sprTexNum].framesCount = 0) then
    begin
      FramesGrid.ColCount := 0;
      FramesGrid.RowCount := 0;
      exit;
    end;

  // формируем грид с текстурами
  FramesGrid.ColCount := 1;
  FramesGrid.RowCount := spritesInMem[sprTexNum].framesCount;

  for i := 0 to FramesGrid.ColCount - 1 do
    FramesGrid.ColWidths [i] := 48;
  for i := 0 to FramesGrid.RowCount - 1 do
    FramesGrid.RowHeights[i] := 48;
  FramesGrid.Invalidate;
end;

procedure TfrmSpritesEditor.DeleteColorModeEnable;
begin
  AnimatorDisable;

  if ((sprTexNum < 0) or (sprTexNum >= Length(spritesInMem))) then exit;
  if (spritesInMem[sprTexNum].framesCount = 0) then exit;
  if (spritesInMem[sprTexNum].frames[frmTexNum] = nil) then exit;

  DM.ImageList.GetRawImage(5, rawImage);
  DeleteColorBtn.Glyph.Clear;
  DeleteColorBtn.Glyph.LoadFromRawImage(rawImage, False);
  PreviewImage.Cursor := crCross;  
  //frm_play_btn.Enabled := False;
  SpritesGroupBox.Enabled := False;
  SpritesEditGroupBox.Enabled := False;
  FramesGroupBox.Enabled := False;
  ParamsGroupBox.Enabled := False;
  deleteColorMode := True;
  canChooseSolid  := False;
end;

procedure TfrmSpritesEditor.DeleteColorModeDisable;
begin
  AnimatorDisable;
  DM.ImageList.GetRawImage(4, rawImage);
  DeleteColorBtn.Glyph.Clear;
  DeleteColorBtn.Glyph.LoadFromRawImage(rawImage, False);
  PreviewImage.Cursor := crDefault;
  //frm_play_btn.Enabled := True;

  if (g_SpritesCount > 0) then
    begin
      SpritesGroupBox.Enabled     := True;
      SpritesEditGroupBox.Enabled := True;
      FramesGroupBox.Enabled      := True;
      ParamsGroupBox.Enabled      := True;
    end;

  deleteColorMode := False;
  canChooseSolid  := False;
end;


procedure TfrmSpritesEditor.FormCreate(Sender : TObject);
begin
  canInput := False;
  FormPrepare;

  SpritesGridUpdate;

  DoubleBuffered := True;
  PreviewImage.Parent.DoubleBuffered := True;
  {
  with (ValueListEditor1) do
    begin
      RowCount := 4;

      Keys[1] := 'Name';
                        
      Keys[2] := 'Detect Collision';
      ItemProps[1].EditStyle := esPickList;
      ItemProps[1].PickList.Add('0');
      ItemProps[1].PickList.Add('1');
      ItemProps[1].ReadOnly := True;

      Keys[3] := 'Damage for Destroy';
      ItemProps[2].EditMask := '!99/99/00; 1;_';
      ItemProps[2].KeyDesc  := 'damage description';

      AutoSizeColumns;
    end;
  }
end;

procedure TfrmSpritesEditor.FormKeyDown(Sender : TObject; var Key : Word;
  Shift : TShiftState);
begin
  if (Key = VK_ESCAPE) then Close;
end;

procedure TfrmSpritesEditor.FramesGridClick(Sender: TObject);
begin

end;

procedure TfrmSpritesEditor.FramesGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin      
  if (g_SpritesCount = 0) then exit;
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (spritesInMem[sprTexNum]) do
    begin
      if (framesCount = 0) then exit;
      if (pos >= framesCount) then exit;
      bmp := frames[pos];
    end;

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmSpritesEditor.FramesGridSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  AnimatorDisable;

  PreviewImage.Picture.Clear;

  LoadFrameBtn.Enabled   := False;
  ClearFrameBtn.Enabled  := False;
  DeleteFrameBtn.Enabled := False;

  frmTexNum := 0;
  if ((sprTexNum < 0) or
      (sprTexNum >= Length(spritesInMem)) or
      (spritesInMem[sprTexNum].framesCount = 0)) then
    exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (spritesInMem[sprTexNum]) do
    begin
      if (pos >= framesCount) then exit;

      frmTexNum := pos;
      if (pos > 0) then
        begin
          LoadFrameBtn.Enabled   := True;
          ClearFrameBtn.Enabled  := True;
          DeleteFrameBtn.Enabled := True;
        end;
      bmp := frames[pos];
      if (bmp = nil) then exit;

      LoadPreviewImageFromBitmap(PreviewImage, bmp);
    end;

  DeleteColorModeDisable;
end;


procedure TfrmSpritesEditor.frm_play_btnClick(Sender : TObject);
begin        
  if (g_SpritesCount = 0) then exit;
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (spritesInMem[sprTexNum].framesCount = 0) then exit;

  if (Animator.Enabled) then
    begin
      AnimatorDisable;
      if (sprTexNum < g_SpritesCount) then
        with (spritesInMem[sprTexNum]) do
          if (frmTexNum < framesCount) then
            LoadPreviewImageFromBitmap(PreviewImage, frames[frmTexNum]);
    end
  else
    AnimatorEnable;
end;

procedure TfrmSpritesEditor.LoadFrameBtnClick(Sender : TObject);
var
  image : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (DM.OpenTextureDialog.Execute) then
    begin
      PrepareLoadTexture(image, TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

      // записываем изображение в поток
      stream := TMemoryStream.Create;
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image); 
      FreeImage(image);

      stream.Position := 0;
      with (spritesInMem[sprTexNum]) do
        begin
          if (frames[frmTexNum] = nil) then
            frames[frmTexNum] := TBitmap.Create;
          frames[frmTexNum].LoadFromStream(stream);
        end;
      stream.Destroy;

      FramesGridUpdate;
      FramesGridSelectCell(FramesGrid, 0, frmTexNum, canSelect);
      GridSellectRow(FramesGrid, frmTexNum);
    end;
end;

procedure TfrmSpritesEditor.LoadSpriteBtnClick(Sender : TObject);
var
  image  : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;
  i : Integer;
  filesCount : Integer;  
  frmPleaseWait : TfrmPleaseWait;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if ((spritesInMem[sprTexNum].framesCount > 0) and
      (MessageDlg('Warning!',
                  'Do you really want to load new sprite? It''s event clear all old frames!',
                  mtWarning,
                  [mbYes, mbNo],
                  0) = mrNo)) then
     exit;
              
  frmPleaseWait := TfrmPleaseWait.Create(Self);
  DM.OpenTextureDialog.Options := g_OpenOptionsMulti;

  if (DM.OpenTextureDialog.Execute) then
    begin  
      frmPleaseWait.Show;
      frmPleaseWait.Repaint;

      InitSprite(sprTexNum);
                             
      spritesInMem[sprTexNum].name := 'Sprite #' + IntToStr(sprTexNum);

      filesCount := DM.OpenTextureDialog.Files.Count; 
      spritesInMem[sprTexNum].framesCount := filesCount;
      SetLength(spritesInMem[sprTexNum].frames, filesCount);

      stream := TMemoryStream.Create;
      for i := 0 to filesCount - 1 do
        begin
          spritesInMem[sprTexNum].frames[i] := TBitmap.Create;

          PrepareLoadTexture(image, DM.OpenTextureDialog.Files[i], TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

          // записываем изображение в поток
          stream.Position := 0;
          SaveImageToStream('bmp', stream, image);  
          FreeImage(image);

          stream.Position := 0;
          spritesInMem[sprTexNum].frames[i].LoadFromStream(stream);      
          stream.Clear;
        end;
      stream.Destroy;

      SpritesGridUpdate;
      SpritesGridSelectCell(SpritesGrid, 0, sprTexNum, canSelect);
      GridSellectRow(SpritesGrid, sprTexNum);
    end;

  frmPleaseWait.Destroy;
  DM.OpenTextureDialog.Options := g_OpenOptionsOnce;
end;

procedure TfrmSpritesEditor.NameEditChange(Sender: TObject);
begin
  with (NameEdit) do
    if (Text = '') then
      Color := clRed
    else
      Color := clWhite;

  if (g_SpritesCount = 0) then exit;
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (not canInput) then exit;

  spritesInMem[sprTexNum].name := NameEdit.Text;
end;

procedure TfrmSpritesEditor.ParamsGroupBoxClick(Sender: TObject);
begin
end;

procedure TfrmSpritesEditor.PlayOnceChkChange(Sender: TObject);
begin
  if (g_SpritesCount = 0) then exit;
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (not canInput) then exit;

  spritesInMem[sprTexNum].playOnce := PlayOnceChk.Checked;
end;

procedure TfrmSpritesEditor.PreviewImageClick(Sender: TObject);
var
  i : Integer;
  image : TImageData;
  bmp : TBitmap = nil;
  stream : TMemoryStream = nil;
  clr : TColor32Rec;             // цвет для удаления
  canSelect : Boolean;
begin
  if (g_SpritesCount = 0) then exit; 
  if (not deleteColorMode) then exit;

  with (spritesInMem[sprTexNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      stream := TMemoryStream.Create;
      frames[frmTexNum].SaveToStream(stream);

      stream.Position := 0;
      FreeImage(image);
      LoadImageFromStream(stream, image);
      stream.Clear;

      // берем цвет для удаления и удаляем его в памяти
      clr := (PColor32Rec(image.Bits) + image.Width * deleteColorPos.y + deleteColorPos.x)^;
      for i := 0 to (image.Size div SizeOf(clr)) - 1 do
        begin
          if ((PColor32Rec(image.Bits) + i)^.Color = clr.Color) then
            (PColor32Rec(image.Bits) + i)^.A := 0;
        end;

      // сохраняем картинку из памяти в превью
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image);
      FreeImage(image);
                           
      stream.Position := 0;
      bmp := TBitmap.Create;
      bmp.LoadFromStream(stream, stream.Size);
      stream.Destroy;

      LoadPreviewImageFromBitmap(PreviewImage, bmp);
      bmp.Destroy;

      // Если принять изменения, то сохранить фрейм из превью
      if (MessageDlg('Confirm', 'Do you want to save the current changes?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
        begin
          stream := TMemoryStream.Create;
          PreviewImage.Picture.SaveToStream(stream);

          stream.Position := 0;
          frames[frmTexNum].Clear;
          frames[frmTexNum].LoadFromStream(stream);

          stream.Destroy;
        end;
    end;    

  FramesGridUpdate;
  FramesGridSelectCell(FramesGrid, 0, frmTexNum, canSelect);
end;

procedure TfrmSpritesEditor.PreviewImageMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (not deleteColorMode) then
    canChooseSolid := True;
end;

procedure TfrmSpritesEditor.PreviewImageMouseEnter(Sender: TObject);
begin
  if (deleteColorMode) then
    begin
      DeleteColorShp.Visible := True;
      Label1.Visible := True;
    end;
end;

procedure TfrmSpritesEditor.PreviewImageMouseLeave(Sender: TObject);
begin
  DeleteColorShp.Visible := False;
  Label1.Visible := False;
  canChooseSolid := False;  
  PreviewImage.Invalidate;
end;

procedure TfrmSpritesEditor.PreviewImageMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  _x, _y : Integer;
  coeffX, coeffY : Single;    // поправочные коэффициенты для взятия пикселя (в превью может быть растянуто)
  clr : TColor;
begin          
  // высчитываем координаты пикселя
  if (not StretchImageCheck.Checked) then
    begin
      _x := X;
      _y := Y;
    end
  else
    begin
      coeffX := PreviewImage.Picture.Width  / PreviewImage.Width;
      coeffY := PreviewImage.Picture.Height / PreviewImage.Height;
      _x := round(X * coeffX);
      _y := round(Y * coeffY);
    end;
  if (_x > PreviewImage.Picture.Width ) then _x := PreviewImage.Picture.Width;
  if (_y > PreviewImage.Picture.Height) then _y := PreviewImage.Picture.Height;

  if (deleteColorMode) then
    begin
      DeleteColorShp.Brush.Color := clDefault;

      clr := (PInt32(PreviewImage.Picture.Bitmap.RawImage.Data) + PreviewImage.Picture.Width * deleteColorPos.y + deleteColorPos.x)^;
      if ((clr shr 24) <> 0) then
        DeleteColorShp.Brush.Color := PreviewImage.Picture.Bitmap.Canvas.Pixels[_x, _y];

      deleteColorPos.x := _x;
      deleteColorPos.y := _y;
    end
  else
    if (canChooseSolid) then
      begin
        if (_x > (PreviewImage.Picture.Width shr 1)) then
          _x := PreviewImage.Picture.Width - _x;

        SolidXSpin.Value := ((PreviewImage.Picture.Width  - _x shl 1) / PreviewImage.Picture.Width ) * 100.0;
        SolidYSpin.Value := ((PreviewImage.Picture.Height - _y      ) / PreviewImage.Picture.Height) * 100.0;

        PreviewImage.Invalidate;
      end;
end;

procedure TfrmSpritesEditor.PreviewImageMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (not deleteColorMode) then
    canChooseSolid := False;
  PreviewImage.Invalidate;
end;

procedure TfrmSpritesEditor.PreviewImagePaint(Sender: TObject);
var
  x0, x1, y0, y1 : Integer;
  xCoeff, yCoeff : Single;    // соотношение реального размера к растянутому в превью
  w, h : Integer;
  sW, sH : Integer;
begin
  if (g_SpritesCount = 0) then exit;
  with (spritesInMem[sprTexNum]) do
    begin
      if ((framesCount = 0) or (frames[0] = nil)) then exit;

      w := PreviewImage.Picture.Width;
      h := PreviewImage.Picture.Height;

      sW := ceil(w * (solidX / 100.0));
      sH := ceil(h * (solidY / 100.0));

      x0 := (w - sW) shr 1;
      x1 := w - x0;
      y0 := h - sH;
      y1 := h;

      if (StretchImageCheck.Checked) then
        begin        
          xCoeff := PreviewImage.Width  / w;
          yCoeff := PreviewImage.Height / h;

          x0 := ceil(x0 * xCoeff);
          x1 := ceil(x1 * xCoeff);
          y0 := ceil(y0 * yCoeff);
          y1 := ceil(y1 * yCoeff);
        end
      else
        begin
          xCoeff := 1.0;
          yCoeff := 1.0;
        end;

      if (x0 < 0) then x0 := 0;
      if (y0 < 0) then y0 := 0;
      if (x1 >= PreviewImage.Width ) then x1 := PreviewImage.Width  - 1;
      if (y1 >= PreviewImage.Height) then y1 := PreviewImage.Height - 1;

      with (PreviewImage.Canvas) do
        begin                  
          Pen.Color := clRed;
          Line(x0, y0, x1, y0);
          Line(x1, y0, x1, y1);
          Line(x0, y1, x1, y1);
          Line(x0, y0, x0, y1);
          // перекрестие
          if (canChooseSolid) then
            begin
              Line(x0, y0, x1, y1);
              Line(x1, y0, x0, y1);
            end;

          Pen.Color   := clLime;
          Brush.Color := clLime;  
          Rectangle(x0 - 3, y0 - 3, x0 + 3, y0 + 3);
          Rectangle(x0 - 3, y1 - 3, x0 + 3, y1 + 3);
          Rectangle(x1 - 3, y0 - 3, x1 + 3, y0 + 3);
          Rectangle(x1 - 3, y1 - 3, x1 + 3, y1 + 3);

          // рамка рисунка
          if (not StretchImageCheck.Checked) then
            begin
              Pen.Color := clGray;
              Line(0, 0, w, 0);
              Line(w, 0, w, h);
              Line(0, h, w, h);
              Line(0, 0, 0, h);
            end;
        end;
    end;
end;

procedure TfrmSpritesEditor.RotationRadioGroupSizeConstraintsChange(
  Sender : TObject);
begin                               
  if (g_SpritesCount = 0) then exit;   
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (not canInput) then exit;

  spritesInMem[sprTexNum].axisParams := RotationRadioGroup.ItemIndex;
end;

procedure TfrmSpritesEditor.DeleteColorBtnClick(Sender: TObject);
begin
  deleteColorMode := not deleteColorMode;

  if (not deleteColorMode) then
    DeleteColorModeDisable
  else
    DeleteColorModeEnable;
end;

procedure TfrmSpritesEditor.ScaleXSpinChange(Sender: TObject);
begin
  if (g_SpritesCount = 0) then exit;
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (not canInput) then exit;

  spritesInMem[sprTexNum].scaleX := ScaleXSpin.Value;
end;

procedure TfrmSpritesEditor.ScaleYSpinChange(Sender: TObject);
begin
  if (g_SpritesCount = 0) then exit; 
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (not canInput) then exit;

  spritesInMem[sprTexNum].scaleY := ScaleYSpin.Value;
end;

procedure TfrmSpritesEditor.SolidXSpinChange(Sender: TObject);
begin
  if (g_SpritesCount = 0) then exit;    
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (not canInput) then exit;

  spritesInMem[sprTexNum].solidX := SolidXSpin.Value;
  PreviewImage.Invalidate;
end;

procedure TfrmSpritesEditor.SolidYSpinChange(Sender: TObject);
begin
  if (g_SpritesCount = 0) then exit;
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (not canInput) then exit;

  spritesInMem[sprTexNum].solidY := SolidYSpin.Value;  
  PreviewImage.Invalidate;
end;

procedure TfrmSpritesEditor.SolidYSpinChangeBounds(Sender: TObject);
begin

end;

procedure TfrmSpritesEditor.AutosizeBtnClick(Sender: TObject);
var
  image  : TImageData;
  stream : TMemoryStream;

  x, y : Integer;
  clr : TColor32Rec;
  halfW : Integer;
  minW, maxW : Integer;
  minH : Integer;
  w0, w1 : Integer;
begin      
  AnimatorDisable;

  if (g_SpritesCount = 0) then exit;

  with (spritesInMem[sprTexNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      stream := TMemoryStream.Create;
      frames[frmTexNum].SaveToStream(stream);

      stream.Position := 0;
      FreeImage(image);
      LoadImageFromStream(stream, image);
      stream.Clear;

      //
      // высчитываем solid box
      //
      halfW := image.Width  shr 1;
      minW := halfW;
      maxW := halfW;
      minH := image.Height;
      for y := 0 to image.Height - 1 do
        for x := 0 to image.Width - 1 do
          begin
            clr := (PColor32Rec(image.Bits) + y * image.Width + x)^;
            if (clr.A <> 0) then
              begin
                if ((x <= halfW) and (x < minW)) then minW := x;
                if ((x >  halfW) and (x > maxW)) then maxW := x;
                if (y < minH) then minH := y;
              end;
          end;
      // если minW = halfW и maxW = halfW, значит текстура вся непрозрачная
      if ((minW = halfW) and (maxW = halfW)) then
        solidX := 100
      // проверяем что дальше от центра
      else
        begin
          w0 := halfW - minW;
          w1 := maxW - halfW;
          // то, что дальше, то и будет взято за основу
          if (w1 > w0) then
            w0 := w1;
          w0 := w0 shl 1;
          solidX := (w0 / image.Width) * 100;
        end;
      // устанавливаем высоту
      if (minH <> image.Height) then
        minH := image.Height - minH;
      solidY := (minH / image.Height) * 100;  
                                
      SolidXSpin.Value := solidX;
      SolidYSpin.Value := solidY;

      FreeImage(image);
      stream.Destroy;
    end;

  PreviewImage.Invalidate;
end;

procedure TfrmSpritesEditor.SpritesGridSelection(Sender: TObject; aCol,
  aRow: Integer);
begin
  SpritesGrid.SetFocus;
end;

procedure TfrmSpritesEditor.ClearSpriteBtnClick(Sender : TObject);
var
  i : Integer;
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (MessageDlg('Warning!',
                 'Do you really want to clear sprite? It''s event clear all frames!',
                 mtWarning,
                 [mbYes, mbNo],
                 0) = mrNo) then
     exit;

  DeleteFile(g_SprPath + '/sprite_' + IntToStr(sprTexNum) + '.cfg');
  for i := 0 to High(spritesInMem[sprTexNum].frames) do
    DeleteFile(g_SprPath + '/sprite_' + IntToStr(sprTexNum) + '_frame_' + IntToStr(i) + '.dat');

  InitSprite(sprTexNum); 
  spritesInMem[sprTexNum].name := 'Sprite #' + IntToStr(sprTexNum);

  SpritesGridUpdate;
  SpritesGridSelectCell(SpritesGrid, 0, sprTexNum, canSelect);
  GridSellectRow(SpritesGrid, sprTexNum);
end;

procedure TfrmSpritesEditor.DeathSoundsCmbBoxChange(Sender: TObject);
begin
  if (g_SpritesCount = 0) then exit;
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (not canInput) then exit;

  spritesInMem[sprTexNum].deathSoundId := DeathSoundsCmbBox.ItemIndex;
end;

procedure TfrmSpritesEditor.DeleteFrameBtnClick(Sender: TObject);
var
  i : Integer;
  canSelect : Boolean;
begin
  ClearFrameBtnClick(Self);

  with (spritesInMem[sprTexNum]) do
    begin
      // сначала переносим все текстуры вниз
      for i := frmTexNum to framesCount - 2 do
        frames[i] := frames[i + 1];

      Dec(framesCount);
      Dec(frmTexNum);     

      SetLength(frames, framesCount);
    end;

  FramesGridUpdate;
  FramesGridSelectCell(FramesGrid, 0, frmTexNum, canSelect);
  GridSellectRow(FramesGrid, frmTexNum);
end;

procedure TfrmSpritesEditor.DeleteOnLastFrameChkChange(Sender: TObject);
begin
  if (g_SpritesCount = 0) then exit;
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (not canInput) then exit;

  spritesInMem[sprTexNum].deleteOnLastFrame := DeleteOnLastFrameChk.Checked;
end;

procedure TfrmSpritesEditor.DeleteSpriteBtnClick(Sender: TObject);
var
  canSelect : Boolean;
  i : Integer;
  x, y, m : Integer;
  cell : Integer;
  fromPos, toPos : Integer;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (g_SpritesCount = 0) then exit;

  if (MessageDlg('Warning!',
                 'Do you really want to delete sprite? It''s event clear all frames!',
                 mtWarning,
                 [mbYes, mbNo],
                 0) = mrNo) then
     exit;

  DeleteFile(g_SprPath + '/sprite_' + IntToStr(sprTexNum) + '.cfg');
  for i := 0 to High(spritesInMem[sprTexNum].frames) do
    DeleteFile(g_SprPath + '/sprite_' + IntToStr(sprTexNum) + '_frame_' + IntToStr(i) + '.dat');

  InitSprite(sprTexNum);

  // удаляем на картах элементы, которые использовали удаленную текстуру
  //CheckMaps;
  if (sprTexNum < g_SpritesCount) then
    begin
      // сдвигаем назад все элементы на карте
      if (sprTexNum >= 0) then
        begin
          fromPos := TEXTURES_MAX_COUNT;
          toPos   := fromPos + SPRITES_MAX_COUNT;

          for m := 0 to g_MapsCount - 1 do
            with (mapPack.maps[m]) do
              for x := 0 to MAP_WIDTH  - 1 do
                for y := 0 to MAP_HEIGHT - 1 do
                  begin
                    cell := level[x, y] - 1;
                    if ((cell >= fromPos) and (cell < toPos)) then
                      begin
                        if (cell > sprTexNum + fromPos) then
                          level[x, y] := cell
                        else
                          level[x, y] := 0;
                      end;
                  end;
        end;

      // теперь отодвигаем назад все текстуры
      for i := sprTexNum to SPRITES_MAX_COUNT - 2 do
        spritesInMem[i] := spritesInMem[i + 1];
      InitSprite(g_SpritesCount - 1);
    end;

  Dec(g_SpritesCount);
  SpritesGridUpdate;
  SpritesGridSelectCell(SpritesGrid, 0, g_SpritesCount - 1, canSelect);
  GridSellectRow(SpritesGrid, g_SpritesCount - 1);

  if (g_SpritesCount = 0) then
    begin
      LoadSpriteBtn.Enabled   := False;
      ClearSpriteBtn.Enabled  := False;
      DeleteSpriteBtn.Enabled := False;
      ParamsGroupBox.Enabled  := False;
      FramesGroupBox.Enabled  := False;
      PreviewImage.Picture.Clear;
    end;
end;

procedure TfrmSpritesEditor.ClearFrameBtnClick(Sender : TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable; 

  PreviewImage.Picture.Clear;

  with (spritesInMem[sprTexNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;
                        
      frames[frmTexNum].Clear;
      frames[frmTexNum].Destroy;
      frames[frmTexNum] := nil;

      DeleteFile(g_SprPath + '/sprite_' + IntToStr(sprTexNum) + '_frame_' + IntToStr(frmTexNum) + '.dat');
    end;        

  FramesGridUpdate;
  FramesGridSelectCell(FramesGrid, 0, frmTexNum, canSelect);
  GridSellectRow(FramesGrid, frmTexNum);
end;

procedure TfrmSpritesEditor.AnimSpeedSpinChange(Sender : TObject);
begin
  if (g_SpritesCount = 0) then exit;     
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (not canInput) then exit;

  spritesInMem[sprTexNum].animSpeed := AnimSpeedSpin.Value;
  if (AnimSpeedSpin.Value <> 0) then
    Animator.Interval := 1000 div AnimSpeedSpin.Value
  else
    Animator.Interval := 0;
end;

procedure TfrmSpritesEditor.ClearDeathSoundBtnClick(Sender: TObject);
begin      
  if (g_SpritesCount = 0) then exit;
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (not canInput) then exit;
                                           
  DeathSoundsCmbBox.Text := '';
  spritesInMem[sprTexNum].deathSoundId := -1;
end;

procedure TfrmSpritesEditor.AnimatorTimer(Sender : TObject);
const
  animFrame : Integer = -1;
var
  framesCount : UInt32;
begin
  framesCount := spritesInMem[sprTexNum].framesCount;

  Inc(animFrame);
  if (animFrame >= framesCount) then
    animFrame := 0;

  while (spritesInMem[sprTexNum].frames[animFrame] = nil) do
    begin
      Inc(animFrame);
      if (animFrame >= framesCount) then
        animFrame := 0;
    end;

  LoadPreviewImageFromBitmap(PreviewImage, spritesInMem[sprTexNum].frames[animFrame]);
end;

procedure TfrmSpritesEditor.AddSpriteBtnClick(Sender: TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (g_SpritesCount < SPRITES_MAX_COUNT) then
    begin
      sprTexNum := g_SpritesCount;

      Inc(g_SpritesCount); 
      SpritesGridUpdate;
      SpritesGrid.Col := 1;
      SpritesGrid.Row := g_SpritesCount;

      LoadSpriteBtnClick(Self);    

      spritesInMem[sprTexNum].name := 'Sprite #' + IntToStr(sprTexNum);
    end
  else
    MessageDlg('Error', 'Достигнут предел текстур. Текущий максимум = ' + IntToStr(SPRITES_MAX_COUNT), mtError, [mbOK], 0);

  SpritesGridSelectCell(SpritesGrid, 0, sprTexNum, canSelect);
  GridSellectRow(SpritesGrid, sprTexNum);
end;

procedure TfrmSpritesEditor.AddFrameBtnClick(Sender: TObject);
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  with (spritesInMem[sprTexNum]) do
    begin
      frmTexNum := framesCount;
      Inc(framesCount);
      SetLength(frames, framesCount);
      frames[frmTexNum] := nil;

      FramesGridUpdate;
      FramesGrid.Col := 1;
      FramesGrid.Row := framesCount;
    end;

  LoadFrameBtnClick(Self);

  FramesGrid.Invalidate;
end;

procedure TfrmSpritesEditor.DestroyableChkChange(Sender : TObject);
begin           
  if (g_SpritesCount = 0) then exit;
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (not canInput) then exit;

  spritesInMem[sprTexNum].destroyable := DestroyableChk.Checked;
end;

procedure TfrmSpritesEditor.DetectCollisionChkChange(Sender : TObject);
begin                              
  if (g_SpritesCount = 0) then exit;
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (not canInput) then exit;

  spritesInMem[sprTexNum].collision := DetectCollisionChk.Checked;
end;

procedure TfrmSpritesEditor.EnduranceSpinChange(Sender : TObject);
begin
  if (g_SpritesCount = 0) then exit;
  if ((sprTexNum < 0) or (sprTexNum >= g_SpritesCount)) then exit;
  if (not canInput) then exit;

  spritesInMem[sprTexNum].endurance := EnduranceSpin.Value;
end;

procedure TfrmSpritesEditor.FormActivate(Sender: TObject);
begin
  SetFocus;
end;

procedure TfrmSpritesEditor.FormClose(Sender : TObject;
  var CloseAction : TCloseAction);
begin
  SaveSpritePack;
end;

end.

