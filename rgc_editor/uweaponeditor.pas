unit uWeaponEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  FileUtil,
  Forms,
  Controls,
  Graphics,
  GraphType,
  Dialogs,
  StdCtrls,
  Grids,
  Buttons,
  ExtCtrls,
  Spin,
  Types,
  Imaging,
  ImagingTypes,
  uContainer,
  uUtil,
  uDM, uPleaseWait;

type

  { TfrmWeaponEditor }

  TfrmWeaponEditor = class(TForm)
    AddWeaponFrameBtn: TSpeedButton;
    Animator: TTimer;
    ClearFireSoundBtn: TSpeedButton;
    ClearWeaponFrameBtn: TSpeedButton;
    DeleteWeaponColorBtn: TSpeedButton;
    DeleteWeaponColorShp: TShape;
    DeleteWeaponFrameBtn: TSpeedButton;
    FireFrameSpin: TSpinEdit;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label13: TLabel;
    Label2: TLabel;
    CurrentFrameLabel: TLabel;
    Label3: TLabel;
    LoadWeaponFrameBtn: TSpeedButton;
    PreviewWeaponImage: TImage;
    AlignRadioGroup: TRadioGroup;
    FireSoundsCmbBox: TComboBox;
    StretchWeaponImageCheck: TCheckBox;
    WeaponAnimSpeedSpin: TSpinEdit;
    WeaponFramePlayBtn: TBitBtn;
    WeaponFramesGroupBox: TGroupBox;
    WeaponParamsGroupBox: TGroupBox;
    WeaponFramesGrid: TDrawGrid;
    WeaponGroupBox: TGroupBox;
    procedure AddWeaponFrameBtnClick(Sender: TObject);
    procedure AlignRadioGroupClick(Sender: TObject);
    procedure AnimatorTimer(Sender: TObject);
    procedure ClearFireSoundBtnClick(Sender: TObject);
    procedure ClearWeaponFrameBtnClick(Sender: TObject);
    procedure DeleteWeaponColorBtnClick(Sender: TObject);
    procedure DeleteWeaponFrameBtnClick(Sender: TObject);
    procedure FireSoundsCmbBoxChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FireFrameSpinChange(Sender: TObject);
    procedure LoadWeaponFrameBtnClick(Sender: TObject);
    procedure PreviewWeaponImageClick(Sender: TObject);
    procedure PreviewWeaponImageMouseEnter(Sender: TObject);
    procedure PreviewWeaponImageMouseLeave(Sender: TObject);
    procedure PreviewWeaponImageMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure StretchWeaponImageCheckChange(Sender: TObject);
    procedure WeaponAnimSpeedSpinChange(Sender: TObject);
    procedure WeaponFramePlayBtnClick(Sender: TObject);
    procedure WeaponFramesGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure WeaponFramesGridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure WeaponFramesGridSelection(Sender: TObject; aCol, aRow: Integer);
  private
    isFormLoaded : Boolean;
    frmTexNum : Integer;
    rawImage : TRawImage;
    deleteColorPos : TPoint;    // позиция на реальном изображении для взятия цвета под удаление
    deleteColorMode : Boolean;  // режим удаления цвета?

    procedure FormPrepare;
    procedure AnimatorDisable;
    procedure AnimatorEnable;
    procedure DeleteColorModeEnable;
    procedure DeleteColorModeDisable;
    procedure WeaponFramesGridUpdate;
  public

  end;

//var
//  frmWeaponEditor: TfrmWeaponEditor;

implementation

{$R *.lfm}

{ TfrmWeaponEditor }

procedure TfrmWeaponEditor.StretchWeaponImageCheckChange(Sender: TObject);
begin
  PreviewWeaponImage.Stretch := StretchWeaponImageCheck.Checked;
end;

procedure TfrmWeaponEditor.AddWeaponFrameBtnClick(Sender: TObject);
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  with (g_WeaponInMem) do
    begin
      frmTexNum := framesCount;
      Inc(framesCount);
      SetLength(frames, framesCount);
      frames[frmTexNum] := nil;

      WeaponFramesGridUpdate;
      WeaponFramesGrid.Col := 0;
      WeaponFramesGrid.Row := framesCount - 1;
    end;

  LoadWeaponFrameBtnClick(Self); 
  FireFrameSpin.MaxValue := g_WeaponInMem.framesCount - 1;

  WeaponFramesGrid.Invalidate;
end;

procedure TfrmWeaponEditor.AlignRadioGroupClick(Sender: TObject);
begin
  if (isFormLoaded) then
    g_WeaponInMem.align := AlignRadioGroup.ItemIndex;
end;

procedure TfrmWeaponEditor.AnimatorTimer(Sender: TObject);
const
  animFrame : Integer = -1;
var
  framesCount : UInt32;
begin
  framesCount := g_WeaponInMem.framesCount;

  Inc(animFrame);
  if (animFrame >= framesCount) then
    animFrame := 0;

  while (g_WeaponInMem.frames[animFrame] = nil) do
    begin
      Inc(animFrame);
      if (animFrame >= framesCount) then
        animFrame := 0;
    end;

  LoadPreviewImageFromBitmap(PreviewWeaponImage, g_WeaponInMem.frames[animFrame]);
end;

procedure TfrmWeaponEditor.ClearFireSoundBtnClick(Sender: TObject);
begin
  if (isFormLoaded) then
    begin
      FireSoundsCmbBox.Text := '';
      g_WeaponInMem.fireSoundId := -1;
    end;
end;

procedure TfrmWeaponEditor.ClearWeaponFrameBtnClick(Sender: TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  PreviewWeaponImage.Picture.Clear;

  with (g_WeaponInMem) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      frames[frmTexNum].Clear;
      frames[frmTexNum].Destroy;
      frames[frmTexNum] := nil;

      DeleteFile(g_WeapPath + '/weapon_inhand_' + IntToStr(g_WeaponInMem.weaponId) + '_frame_' + IntToStr(frmTexNum) + '.dat');
    end;

  WeaponFramesGridUpdate;
  WeaponFramesGridSelectCell(WeaponFramesGrid, 0, frmTexNum, canSelect);
end;

procedure TfrmWeaponEditor.DeleteWeaponColorBtnClick(Sender: TObject);
begin
  if ((frmTexNum < 0) or (frmTexNum >= g_WeaponInMem.framesCount)) then exit;
  if (g_WeaponInMem.framesCount = 0) then exit;
  if (g_WeaponInMem.frames[frmTexNum] = nil) then exit;

  deleteColorMode := not deleteColorMode;

  if (not deleteColorMode) then
    DeleteColorModeDisable
  else
    DeleteColorModeEnable;
end;

procedure TfrmWeaponEditor.DeleteWeaponFrameBtnClick(Sender: TObject);
var
  i : Integer;
  canSelect : Boolean;
begin
  ClearWeaponFrameBtnClick(Self);

  with (g_WeaponInMem) do
    begin      
      if (framesCount = 0) then exit;

      for i := frmTexNum to framesCount - 2 do
        frames[i] := frames[i + 1];

      Dec(framesCount);
      Dec(frmTexNum);

      SetLength(frames, framesCount);
    end;
                             
  FireFrameSpin.MaxValue := g_WeaponInMem.framesCount - 1;
  WeaponFramesGridUpdate;
  WeaponFramesGridSelectCell(WeaponFramesGrid, 0, frmTexNum, canSelect);
end;

procedure TfrmWeaponEditor.FireSoundsCmbBoxChange(Sender: TObject);
begin
  if (isFormLoaded) then
    g_WeaponInMem.fireSoundId := FireSoundsCmbBox.ItemIndex;
end;

procedure TfrmWeaponEditor.FormActivate(Sender: TObject);
begin
  SetFocus;
end;

procedure TfrmWeaponEditor.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  SaveWeapon(g_WeaponInMem.weaponId);
  InitWeapon;
end;

procedure TfrmWeaponEditor.FormCreate(Sender: TObject);
begin
  DoubleBuffered := True;
  PreviewWeaponImage.Parent.DoubleBuffered := True;
  WeaponFramesGrid.DoubleBuffered := True;

  FormPrepare;
  WeaponFramesGridUpdate;
  //AmmoGridUpdate;
end;

procedure TfrmWeaponEditor.FireFrameSpinChange(Sender: TObject);
begin             
  if (isFormLoaded) then
    g_WeaponInMem.fireFrame := FireFrameSpin.value - 1;
end;

procedure TfrmWeaponEditor.LoadWeaponFrameBtnClick(Sender: TObject);
var
  image : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;
  frmPleaseWait : TfrmPleaseWait;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  frmPleaseWait := TfrmPleaseWait.Create(Self);

  if (DM.OpenTextureDialog.Execute) then
    begin
      frmPleaseWait.Show;
      frmPleaseWait.Repaint;

      PrepareLoadTexture(image, TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

      // записываем изображение в поток 
      stream := TMemoryStream.Create;
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image);
      FreeImage(image);

      stream.Position := 0;
      with (g_WeaponInMem) do
        begin
          if (frames[frmTexNum] = nil) then
            frames[frmTexNum] := TBitmap.Create;
          frames[frmTexNum].LoadFromStream(stream);
        end;

      stream.Destroy;

      WeaponFramesGridUpdate;
      WeaponFramesGridSelectCell(WeaponFramesGrid, 0, frmTexNum, canSelect);
      GridSellectRow(WeaponFramesGrid, frmTexNum);
    end;

  frmPleaseWait.Destroy;
end;

procedure TfrmWeaponEditor.PreviewWeaponImageClick(Sender: TObject);
var
  i : Integer;
  image : TImageData;
  bmp : TBitmap = nil;
  stream : TMemoryStream = nil;
  clr : TColor32Rec;             // цвет для удаления
  canSelect : Boolean;
begin
  if (not deleteColorMode) then exit;

  with (g_WeaponInMem) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      stream := TMemoryStream.Create;
      frames[frmTexNum].SaveToStream(stream);

      stream.Position := 0;
      FreeImage(image);
      LoadImageFromStream(stream, image);
      stream.Clear;

      // берем цвет для удаления и удаляем его в памяти
      clr := (PColor32Rec(image.Bits) + image.Width * deleteColorPos.y + deleteColorPos.x)^;
      for i := 0 to (image.Size div SizeOf(clr)) - 1 do
        begin
          if ((PColor32Rec(image.Bits) + i)^.Color = clr.Color) then
            (PColor32Rec(image.Bits) + i)^.A := 0;
        end;

      // сохраняем картинку из памяти в превью
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image);
      FreeImage(image);

      stream.Position := 0;
      bmp := TBitmap.Create;
      bmp.LoadFromStream(stream, stream.Size);
      stream.Destroy;

      LoadPreviewImageFromBitmap(PreviewWeaponImage, bmp);
      bmp.Destroy;

      // Если принять изменения, то сохранить фрейм из превью
      if (MessageDlg('Confirm', 'Do you want to save the current changes?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
        begin
          stream := TMemoryStream.Create;
          PreviewWeaponImage.Picture.SaveToStream(stream);

          stream.Position := 0;
          frames[frmTexNum].Clear;
          frames[frmTexNum].LoadFromStream(stream);

          stream.Destroy;
        end;
    end;

  WeaponFramesGridUpdate;
  WeaponFramesGridSelectCell(WeaponFramesGrid, 0, frmTexNum, canSelect);
end;

procedure TfrmWeaponEditor.PreviewWeaponImageMouseEnter(Sender: TObject);
begin
  if (deleteColorMode) then
    begin
      DeleteWeaponColorShp.Visible := True;
      Label1.Visible := True;
    end;
end;

procedure TfrmWeaponEditor.PreviewWeaponImageMouseLeave(Sender: TObject);
begin
  DeleteWeaponColorShp.Visible := False;
  Label1.Visible := False;
end;

procedure TfrmWeaponEditor.PreviewWeaponImageMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  _x, _y : Integer;
  coeffX, coeffY : Single;    // поправочные коэффициенты для взятия пикселя (в превью может быть растянуто)
  clr : TColor;
begin
  if (deleteColorMode) then
    begin
      DeleteWeaponColorShp.Brush.Color := clDefault;

      // высчитываем координаты пикселя, цвет которого будем удалять
      coeffX := PreviewWeaponImage.Picture.Width  / PreviewWeaponImage.Width;
      coeffY := PreviewWeaponImage.Picture.Height / PreviewWeaponImage.Height;
      _x := round(X * coeffX);
      _y := round(Y * coeffY);
      if (_x > PreviewWeaponImage.Picture.Width ) then _x := PreviewWeaponImage.Picture.Width;
      if (_y > PreviewWeaponImage.Picture.Height) then _y := PreviewWeaponImage.Picture.Height;

      clr := (PInt32(PreviewWeaponImage.Picture.Bitmap.RawImage.Data) + PreviewWeaponImage.Picture.Width * deleteColorPos.y + deleteColorPos.x)^;
      if ((clr shr 24) <> 0) then
        DeleteWeaponColorShp.Brush.Color := PreviewWeaponImage.Picture.Bitmap.Canvas.Pixels[_x, _y];;

      deleteColorPos.x := _x;
      deleteColorPos.y := _y;
    end;
end;

procedure TfrmWeaponEditor.WeaponAnimSpeedSpinChange(Sender: TObject);
begin
  if (g_WeaponInMem.framesCount = 0) then
    begin
      WeaponAnimSpeedSpin.Value := 0;
      exit;
    end;

  g_WeaponInMem.animSpeed := WeaponAnimSpeedSpin.Value;
  if (WeaponAnimSpeedSpin.Value <> 0) then
    Animator.Interval := 1000 div WeaponAnimSpeedSpin.Value
  else
    Animator.Interval := 0;
end;

procedure TfrmWeaponEditor.WeaponFramePlayBtnClick(Sender: TObject);
begin                        
  if (g_WeaponInMem.framesCount = 0) then exit;
  if ((frmTexNum < 0) or (frmTexNum >= g_WeaponInMem.framesCount)) then exit;

  if (Animator.Enabled) then
    begin
      AnimatorDisable;
      DeleteColorModeDisable;       
      with (g_WeaponInMem) do
        if (frmTexNum < g_WeaponInMem.framesCount) then
          LoadPreviewImageFromBitmap(PreviewWeaponImage, frames[frmTexNum]);
    end
  else
    AnimatorEnable;
end;

procedure TfrmWeaponEditor.WeaponFramesGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_WeaponInMem.framesCount = 0) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (g_WeaponInMem) do
    begin
      if (framesCount = 0) then exit;
      if (pos >= framesCount) then exit;
      bmp := frames[pos];
    end;

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmWeaponEditor.WeaponFramesGridSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  AnimatorDisable;
  DeleteColorModeDisable;
                                     
  LoadWeaponFrameBtn.Enabled   := False;
  ClearWeaponFrameBtn.Enabled  := False;
  DeleteWeaponFrameBtn.Enabled := False;

  WeaponParamsGroupBox.Enabled := False; 
  CurrentFrameLabel.Caption := 'Current Frame: -1';

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (g_WeaponInMem) do
    begin
      if (framesCount > 0) then
        begin
          LoadWeaponFrameBtn.Enabled   := True;
          ClearWeaponFrameBtn.Enabled  := True;
          DeleteWeaponFrameBtn.Enabled := True;
        end;

      if (pos >= framesCount) then exit;
                       
      WeaponParamsGroupBox.Enabled := True;
      frmTexNum := pos;   
      CurrentFrameLabel.Caption := 'Current Frame: ' + IntToStr(frmTexNum + 1);

      bmp := frames[pos];
      if (bmp = nil) then exit;

      LoadPreviewImageFromBitmap(PreviewWeaponImage, bmp);
    end;
end;

procedure TfrmWeaponEditor.WeaponFramesGridSelection(Sender: TObject; aCol,
  aRow: Integer);
begin
  WeaponFramesGrid.Options := [goAlwaysShowEditor, goSmoothScroll, goDrawFocusSelected];
end;

procedure TfrmWeaponEditor.FormPrepare;
var
  i : Integer;
begin             
  isFormLoaded := False;
  AnimatorDisable;
  frmTexNum := -1;

  with (FireSoundsCmbBox) do
    begin
      Items.Clear;
      //Items.Add('');
      for i := 0 to g_SoundsCount - 1 do
        Items.Add(soundsInMem[i].name);
      ItemIndex := -1;
    end;

  WeaponAnimSpeedSpin.Value  := g_WeaponInMem.animSpeed;
  AlignRadioGroup.ItemIndex  := g_WeaponInMem.align;
  FireFrameSpin.Value        := g_WeaponInMem.fireFrame + 1;
  FireSoundsCmbBox.ItemIndex := g_WeaponInMem.fireSoundId;
  FireFrameSpin.MinValue     := 1;
  FireFrameSpin.MaxValue     := g_WeaponInMem.framesCount;

  LoadWeaponFrameBtn.Enabled   := False;
  ClearWeaponFrameBtn.Enabled  := False;
  DeleteWeaponFrameBtn.Enabled := False;

  PreviewWeaponImage.Picture.Clear;
  PreviewWeaponImage.Invalidate;

  WeaponParamsGroupBox.Enabled := False;
  WeaponFramesGrid.ColCount := 0;
  WeaponFramesGrid.RowCount := 0;

  WeaponFramesGrid.Options := [goAlwaysShowEditor, goSmoothScroll];

  isFormLoaded := True;
end;

procedure TfrmWeaponEditor.AnimatorDisable;
begin
  DM.ImageList.GetRawImage(0, rawImage);
  WeaponFramePlayBtn.Glyph.Clear;
  WeaponFramePlayBtn.Glyph.LoadFromRawImage(rawImage, False);

  Animator.Enabled := False;
  WeaponFramePlayBtn.Caption := 'Play';
end;

procedure TfrmWeaponEditor.AnimatorEnable;
begin
  DM.ImageList.GetRawImage(1, rawImage);
  WeaponFramePlayBtn.Glyph.Clear;
  WeaponFramePlayBtn.Glyph.LoadFromRawImage(rawImage, false);

  Animator.Interval := 0;
  if (WeaponAnimSpeedSpin.Value <> 0) then
    Animator.Interval := 1000 div WeaponAnimSpeedSpin.Value;

  Animator.Enabled  := True;
  WeaponFramePlayBtn.Caption := 'Stop';
end;

procedure TfrmWeaponEditor.DeleteColorModeEnable;
begin
  AnimatorDisable;

  if ((frmTexNum < 0) or (frmTexNum >= Length(g_WeaponInMem.frames))) then exit;
  if (g_WeaponInMem.framesCount = 0) then exit;
  if (g_WeaponInMem.frames[frmTexNum] = nil) then exit;

  DM.ImageList.GetRawImage(5, rawImage);
  DeleteWeaponColorBtn.Glyph.Clear;
  DeleteWeaponColorBtn.Glyph.LoadFromRawImage(rawImage, False);
  PreviewWeaponImage.Cursor := crCross;

  WeaponGroupBox.Enabled       := False;
  WeaponFramesGroupBox.Enabled := False;
  WeaponParamsGroupBox.Enabled := False;

  deleteColorMode := True;
end;

procedure TfrmWeaponEditor.DeleteColorModeDisable;
begin
  DM.ImageList.GetRawImage(4, rawImage);
  DeleteWeaponColorBtn.Glyph.Clear;
  DeleteWeaponColorBtn.Glyph.LoadFromRawImage(rawImage, False);
  PreviewWeaponImage.Cursor := crDefault;
                  
  WeaponGroupBox.Enabled       := True;
  WeaponFramesGroupBox.Enabled := True;
  if (Length(g_WeaponInMem.frames) > 0) then
    WeaponParamsGroupBox.Enabled := True;

  deleteColorMode := False;
end;

procedure TfrmWeaponEditor.WeaponFramesGridUpdate;
var
  i : Integer;
begin
  if (g_WeaponInMem.framesCount = 0) then
    begin
      WeaponFramesGrid.ColCount := 0;
      WeaponFramesGrid.RowCount := 0;
      exit;
    end;

  // формируем грид с текстурами
  WeaponFramesGrid.ColCount := 1;
  WeaponFramesGrid.RowCount := g_WeaponInMem.framesCount;

  for i := 0 to WeaponFramesGrid.ColCount - 1 do
    WeaponFramesGrid.ColWidths [i] := 96;
  for i := 0 to WeaponFramesGrid.RowCount - 1 do
    WeaponFramesGrid.RowHeights[i] := 96;
  WeaponFramesGrid.Invalidate;
end;

end.

