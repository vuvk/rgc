program map_editor;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms,
  lazcontrols,
  lazopenglcontext, runtimetypeinfocontrols,
  uMain,    
  uContainer,
  {uTexturesEditor,
  uUtil,
  uMapChoose,
  uSpritesEditor,
  uSoundsEditor,
  udreadfulspace,}
  uDM
  { you can add units after this };

{$R *.res}

begin
  Application.Title := 'Nuke3D Editor';
  RequireDerivedFormResource := True;
  Application.Initialize;

  SetLength(texturesInMem,  TEXTURES_MAX_COUNT);
  SetLength(spritesInMem,   SPRITES_MAX_COUNT );
  SetLength(doorsInMem,     DOORS_MAX_COUNT   );
  SetLength(keysInMem,      KEYS_MAX_COUNT    );
  SetLength(weapItemsInMem, WEAPONS_MAX_COUNT );   
  SetLength(bulletsInMem,   BULLETS_MAX_COUNT );
  SetLength(ammoInMem,      AMMO_MAX_COUNT    );
  SetLength(soundsInMem,    SOUNDS_MAX_COUNT  );
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TDM, DM);
  Application.Run;
end.

