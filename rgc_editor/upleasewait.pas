unit uPleaseWait;

{$mode objfpc}{$H+}

interface

uses
  Forms,
  StdCtrls,
  ExtCtrls, Classes;

type

  { TfrmPleaseWait }

  TfrmPleaseWait = class(TForm)
    Image1: TImage;
    Label1: TLabel;
    procedure FormActivate(Sender: TObject);
  private

  public

  end;

implementation

{$R *.lfm}

{ TfrmPleaseWait }

procedure TfrmPleaseWait.FormActivate(Sender: TObject);
begin
  SetFocus;
end;

end.

