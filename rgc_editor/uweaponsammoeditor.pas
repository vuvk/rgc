unit uWeaponsAmmoEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Math,
  FileUtil,
  Forms,
  Controls,
  Graphics,
  GraphType,
  Dialogs,
  ComCtrls,
  StdCtrls,
  Grids,
  Buttons,
  Spin,
  ExtCtrls,
  Types,       
  Imaging,
  ImagingTypes,
  uContainer,
  uUtil,
  uDM,
  uWeaponEditor,
  uPleaseWait;

type

  { TfrmWeaponsAmmoEditor }

  TfrmWeaponsAmmoEditor = class(TForm)
    AmmoAddFrameBtn: TSpeedButton;
    AmmoAddTextureBtn: TSpeedButton;
    Label28: TLabel;
    CurAmmoSpin: TSpinEdit;
    WeaponPickTypeCmbBox: TComboBox;
    AmmoTypeSaveBtn: TSpeedButton;
    AmmoTypeNameEdit: TEdit;
    Label25: TLabel;
    AmmoTypesList: TListBox;
    Label26: TLabel;
    WeaponAddFrameBtn: TSpeedButton;
    BulletAddFrameBtn: TSpeedButton;
    WeaponAddTextureBtn: TSpeedButton;
    BulletAddTextureBtn: TSpeedButton;
    Animator: TTimer;
    BulletAutosizeBtn: TSpeedButton;
    AmmoAutosizeBtn: TSpeedButton;
    AmmoTypeAddBtn: TSpeedButton;
    AmmoTypeDelBtn: TSpeedButton;
    WeaponAutosizeBtn: TSpeedButton;
    AmmoAnimSpeedSpin: TSpinEdit;
    AmmoFramePlayBtn: TBitBtn;
    AmmoFramesGrid: TDrawGrid;
    AmmoFramesGroupBox: TGroupBox;
    AmmoNameEdit: TEdit;
    AmmoParamsGroupBox: TGroupBox;
    AmmoEditGroupBox: TGroupBox;
    AmmoGrid: TDrawGrid;
    AmmoGroupBox: TGroupBox;
    AmmoSolidXSpin: TFloatSpinEdit;
    AmmoSolidYSpin: TFloatSpinEdit;
    AmmoVolumeSpin: TSpinEdit;
    AmmoClearFrameBtn: TSpeedButton;
    AmmoClearTextureBtn: TSpeedButton;
    AmmoDeleteColorBtn: TSpeedButton;
    AmmoDeleteColorShp: TShape;
    AmmoDeleteFrameBtn: TSpeedButton;
    AmmoDeleteTextureBtn: TSpeedButton;
    GroupBox4: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label27: TLabel;
    AmmoLoadFrameBtn: TSpeedButton;
    AmmoLoadTextureBtn: TSpeedButton;
    AmmoPreviewImage: TImage;
    AmmoStretchImageCheck: TCheckBox;
    WeaponClearPickSoundBtn: TSpeedButton;
    AmmoClearPickSoundBtn: TSpeedButton;
    WeaponPickSoundsCmbBox: TComboBox;
    Label18: TLabel;
    AmmoPickSoundsCmbBox: TComboBox;
    AmmoPickTypeCmbBox: TComboBox;
    WeaponSolidXSpin: TFloatSpinEdit;
    WeaponSolidYSpin: TFloatSpinEdit;
    BulletSpeedSpin: TFloatSpinEdit;
    BulletLifeTimeSpin: TFloatSpinEdit;
    Edit1: TEdit;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label8: TLabel;
    RaycastingCheck: TCheckBox;
    BulletCheck: TCheckBox;
    AccuracySpin: TFloatSpinEdit;
    BulletClearFrameBtn: TSpeedButton;
    BulletClearTextureBtn: TSpeedButton;
    BulletDeleteColorBtn: TSpeedButton;
    BulletDeleteColorShp: TShape;
    BulletDeleteFrameBtn: TSpeedButton;
    BulletDeleteTextureBtn: TSpeedButton;
    GroupBox3: TGroupBox;
    InfiniteChk: TCheckBox;
    DistanceSpin: TFloatSpinEdit;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    BulletLoadFrameBtn: TSpeedButton;
    BulletLoadTextureBtn: TSpeedButton;
    MaxAmmoSpin: TSpinEdit;
    WeaponEditBtn: TBitBtn;
    Label2: TLabel;
    Label3: TLabel;
    MoveUpBtn: TSpeedButton;
    MoveDownBtn: TSpeedButton;
    BulletPreviewImage: TImage;
    BulletCmbBox: TComboBox;
    BulletSolidXSpin: TFloatSpinEdit;
    BulletSolidYSpin: TFloatSpinEdit;
    BulletLifeFrameLastSpin: TSpinEdit;
    BulletStretchImageCheck: TCheckBox;
    AmmoTypesTab: TTabSheet;
    TabSheet2: TTabSheet;
    WeaponAnimSpeedSpin: TSpinEdit;
    WeaponStretchImageCheck: TCheckBox;
    WeaponClearFrameBtn: TSpeedButton;
    WeaponClearTextureBtn: TSpeedButton;
    WeaponDeleteColorBtn: TSpeedButton;
    WeaponDeleteColorShp: TShape;
    WeaponDeleteFrameBtn: TSpeedButton;
    WeaponDeleteTextureBtn: TSpeedButton;
    BulletAnimSpeedSpin: TSpinEdit;
    BulletFramePlayBtn: TBitBtn;
    WeaponFramesGrid: TDrawGrid;
    WeaponFramePlayBtn: TBitBtn;
    BulletFramesGrid: TDrawGrid;
    BulletFramesGroupBox: TGroupBox;
    BulletNameEdit: TEdit;
    BulletParamsGroupBox: TGroupBox;
    BulletsEditGroupBox: TGroupBox;
    BulletsGrid: TDrawGrid;
    WeaponsGroupBox: TGroupBox;
    GroupBox2: TGroupBox;
    WeaponsEditGroupBox: TGroupBox;
    Label1: TLabel;
    Label13: TLabel;
    Label4: TLabel;
    WeaponLoadFrameBtn: TSpeedButton;
    WeaponLoadTextureBtn: TSpeedButton;
    WeaponFramesGroupBox: TGroupBox;
    WeaponNameEdit: TEdit;
    PageControl1: TPageControl;
    WeaponParamsGroupBox: TGroupBox;
    WeaponPreviewImage: TImage;
    WeaponsGrid: TDrawGrid;
    BulletsGroupBox: TGroupBox;
    WeaponsTab: TTabSheet;     
    BulletsTab: TTabSheet;
    AmmoTab: TTabSheet;
    DamageSpin: TFloatSpinEdit;
    procedure AccuracySpinChange(Sender: TObject);
    procedure AmmoAddFrameBtnClick(Sender: TObject);
    procedure AmmoAddTextureBtnClick(Sender: TObject);
    procedure AmmoAnimSpeedSpinChange(Sender: TObject);
    procedure AmmoAutosizeBtnClick(Sender: TObject);
    procedure AmmoClearFrameBtnClick(Sender: TObject);
    procedure AmmoClearPickSoundBtnClick(Sender: TObject);
    procedure AmmoClearTextureBtnClick(Sender: TObject);
    procedure AmmoDeleteColorBtnClick(Sender: TObject);
    procedure AmmoDeleteFrameBtnClick(Sender: TObject);
    procedure AmmoDeleteTextureBtnClick(Sender: TObject);
    procedure AmmoFramePlayBtnClick(Sender: TObject);
    procedure AmmoFramesGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure AmmoFramesGridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure AmmoGridClick(Sender: TObject);
    procedure AmmoGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure AmmoGridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure AmmoGridSelection(Sender: TObject; aCol, aRow: Integer);
    procedure AmmoLoadFrameBtnClick(Sender: TObject);
    procedure AmmoLoadTextureBtnClick(Sender: TObject);
    procedure AmmoNameEditChange(Sender: TObject);
    procedure AmmoPickSoundsCmbBoxChange(Sender: TObject);
    procedure AmmoPickTypeCmbBoxChange(Sender: TObject);
    procedure AmmoPreviewImageClick(Sender: TObject);
    procedure AmmoPreviewImageMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AmmoPreviewImageMouseEnter(Sender: TObject);
    procedure AmmoPreviewImageMouseLeave(Sender: TObject);
    procedure AmmoPreviewImageMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure AmmoPreviewImageMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AmmoPreviewImagePaint(Sender: TObject);
    procedure AmmoSolidXSpinChange(Sender: TObject);
    procedure AmmoSolidYSpinChange(Sender: TObject);
    procedure AmmoStretchImageCheckChange(Sender: TObject);
    procedure AmmoTypeAddBtnClick(Sender: TObject);
    procedure AmmoTypeDelBtnClick(Sender: TObject);
    procedure AmmoTypeNameEditChange(Sender: TObject);
    procedure AmmoTypeSaveBtnClick(Sender: TObject);
    procedure AmmoTypesListSelectionChange(Sender: TObject; User: boolean);
    procedure AmmoVolumeSpinChange(Sender: TObject);
    procedure BulletAddFrameBtnClick(Sender: TObject);
    procedure BulletAddTextureBtnClick(Sender: TObject);
    procedure CurAmmoSpinChange(Sender: TObject);
    procedure WeaponAddFrameBtnClick(Sender: TObject);
    procedure WeaponAddTextureBtnClick(Sender: TObject);
    procedure AnimatorTimer(Sender: TObject);
    procedure BulletAutosizeBtnClick(Sender: TObject);
    procedure WeaponAutosizeBtnClick(Sender: TObject);
    procedure WeaponEditBtnClick(Sender: TObject);
    procedure BulletAnimSpeedSpinChange(Sender: TObject);
    procedure BulletAnimSpeedSpinChangeBounds(Sender: TObject);
    procedure BulletFramePlayBtnClick(Sender: TObject);
    procedure BulletFramesGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure BulletFramesGridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure BulletLifeFrameLastSpinChange(Sender: TObject);
    procedure BulletNameEditChange(Sender: TObject);
    procedure BulletsGridSelection(Sender: TObject; aCol, aRow: Integer);
    procedure BulletLifeTimeSpinChange(Sender: TObject);
    procedure BulletSpeedSpinChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure WeaponClearPickSoundBtnClick(Sender: TObject);
    procedure BulletPreviewImageMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure BulletPreviewImageMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure WeaponPickTypeCmbBoxChange(Sender: TObject);
    procedure WeaponPreviewImageMouseDown(Sender: TObject;
      Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure WeaponPreviewImageMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure WeaponPreviewImagePaint(Sender: TObject);
    procedure RaycastingCheckChange(Sender: TObject);
    procedure BulletClearFrameBtnClick(Sender: TObject);
    procedure BulletClearTextureBtnClick(Sender: TObject);
    procedure WeaponClearFrameBtnClick(Sender: TObject);
    procedure WeaponClearTextureBtnClick(Sender: TObject);
    procedure BulletDeleteColorBtnClick(Sender: TObject);
    procedure BulletDeleteFrameBtnClick(Sender: TObject);
    procedure BulletDeleteTextureBtnClick(Sender: TObject);
    procedure DistanceSpinChange(Sender: TObject);
    procedure DamageSpinChange(Sender: TObject);
    procedure WeaponDeleteColorBtnClick(Sender: TObject);
    procedure WeaponDeleteFrameBtnClick(Sender: TObject);
    procedure WeaponDeleteTextureBtnClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure InfiniteChkChange(Sender: TObject);
    procedure BulletLoadFrameBtnClick(Sender: TObject);
    procedure BulletLoadTextureBtnClick(Sender: TObject);
    procedure WeaponLoadFrameBtnClick(Sender: TObject);
    procedure WeaponLoadTextureBtnClick(Sender: TObject);
    procedure MaxAmmoSpinChange(Sender: TObject);
    procedure MoveDownBtnClick(Sender: TObject);
    procedure MoveUpBtnClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure BulletPreviewImageClick(Sender: TObject);
    procedure BulletPreviewImageMouseEnter(Sender: TObject);
    procedure BulletPreviewImageMouseLeave(Sender: TObject);
    procedure BulletPreviewImageMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure BulletPreviewImagePaint(Sender: TObject);
    procedure WeaponPreviewImageClick(Sender: TObject);
    procedure WeaponPreviewImageMouseEnter(Sender: TObject);
    procedure WeaponPreviewImageMouseLeave(Sender: TObject);
    procedure WeaponPreviewImageMouseMove(Sender: TObject; Shift: TShiftState;
      X, Y: Integer);
    procedure BulletCheckChange(Sender: TObject);
    procedure BulletCmbBoxChange(Sender: TObject);
    procedure BulletSolidXSpinChange(Sender: TObject);
    procedure BulletSolidYSpinChange(Sender: TObject);
    procedure BulletStretchImageCheckChange(Sender: TObject);
    procedure WeaponStretchImageCheckChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WeaponAnimSpeedSpinChange(Sender: TObject);
    procedure WeaponFramePlayBtnClick(Sender: TObject);
    procedure WeaponFramesGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure WeaponFramesGridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure WeaponNameEditChange(Sender: TObject);
    procedure BulletsGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure BulletsGridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure WeaponPickSoundsCmbBoxChange(Sender: TObject);
    procedure WeaponsGridDblClick(Sender: TObject);
    procedure WeaponsGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure WeaponsGridSelectCell(Sender: TObject; aCol, aRow: Integer;
      var CanSelect: Boolean);
    procedure WeaponsGridSelection(Sender: TObject; aCol, aRow: Integer);
    procedure WeaponSolidXSpinChange(Sender: TObject);
    procedure WeaponSolidYSpinChange(Sender: TObject);
  private        
    texNum : Integer;
    frmTexNum : Integer;
    rawImage : TRawImage;
    deleteColorPos : TPoint;    // позиция на реальном изображении для взятия цвета под удаление
    deleteColorMode : Boolean;  // режим удаления цвета?
    canInput : Boolean;         // можно ли вводить имя? выбрать позицию/кнопку, ввести дистанцию?  
    canChooseSolid : Boolean;   // можно ли указать ограничивающую область мышкой

    procedure FormPrepare;           
    procedure AnimatorDisable;
    procedure AnimatorEnable;
    procedure DeleteColorModeEnable;
    procedure DeleteColorModeDisable;
    procedure WeaponsGridUpdate;
    procedure WeaponFramesGridUpdate;
    procedure BulletsGridUpdate;
    procedure BulletFramesGridUpdate;
    procedure BulletCmbBoxFill;
    procedure AmmoGridUpdate;
    procedure AmmoFramesGridUpdate;
  public

  end;
implementation

{$R *.lfm}

{ TfrmWeaponsAmmoEditor }

procedure TfrmWeaponsAmmoEditor.FormCreate(Sender: TObject);
begin
  DoubleBuffered := True;
  WeaponPreviewImage.Parent.DoubleBuffered := True;
  BulletPreviewImage.Parent.DoubleBuffered := True;
  AmmoPreviewImage.Parent.DoubleBuffered   := True;

  LoadBulletPack;
  LoadAmmoTypes;

  FormPrepare;
  WeaponsGridUpdate;  
  BulletsGridUpdate;
  AmmoGridUpdate;
                  
  PageControl1.ActivePageIndex := 0;
  WeaponParamsGroupBox.Enabled := False;
  WeaponFramesGroupBox.Enabled := False;
end;

procedure TfrmWeaponsAmmoEditor.WeaponAnimSpeedSpinChange(Sender: TObject);
begin
  if (g_WeaponsCount = 0) then exit;

  weapItemsInMem[texNum].animSpeed := WeaponAnimSpeedSpin.Value;
  if (WeaponAnimSpeedSpin.Value <> 0) then
    Animator.Interval := 1000 div WeaponAnimSpeedSpin.Value
  else
    Animator.Interval := 0;
end;

procedure TfrmWeaponsAmmoEditor.WeaponFramePlayBtnClick(Sender: TObject);
begin
  if (g_WeaponsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
  if (weapItemsInMem[texNum].framesCount = 0) then exit;

  if (Animator.Enabled) then
    begin
      AnimatorDisable;
      DeleteColorModeDisable;
      if (texNum < g_WeaponsCount) then
        with (weapItemsInMem[texNum]) do
          if (frmTexNum < framesCount) then
            LoadPreviewImageFromBitmap(WeaponPreviewImage, frames[frmTexNum]);
    end
  else
    AnimatorEnable;
end;

procedure TfrmWeaponsAmmoEditor.WeaponFramesGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_WeaponsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (weapItemsInMem[texNum]) do
    begin
      if (framesCount = 0) then exit;
      if (pos >= framesCount) then exit;
      bmp := frames[pos];
    end;

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmWeaponsAmmoEditor.WeaponFramesGridSelectCell(Sender: TObject;
  aCol, aRow: Integer; var CanSelect: Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  WeaponPreviewImage.Picture.Clear;

  WeaponLoadFrameBtn.Enabled   := False;
  WeaponClearFrameBtn.Enabled  := False;
  WeaponDeleteFrameBtn.Enabled := False;

  frmTexNum := 0;
  if ((texNum < 0) or
      (texNum >= Length(weapItemsInMem)) or
      (weapItemsInMem[texNum].framesCount = 0)) then
    exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (weapItemsInMem[texNum]) do
    begin
      if (pos >= framesCount) then exit;

      frmTexNum := pos;
      if (pos > 0) then
        begin
          WeaponLoadFrameBtn.Enabled   := True;
          WeaponClearFrameBtn.Enabled  := True;
          WeaponDeleteFrameBtn.Enabled := True;
        end;
      bmp := frames[pos];
      if (bmp = nil) then exit;

      LoadPreviewImageFromBitmap(WeaponPreviewImage, bmp);
    end;
end;

procedure TfrmWeaponsAmmoEditor.WeaponNameEditChange(Sender: TObject);
begin           
  with (WeaponNameEdit) do
    if (Text = '') then
      Color := clRed
    else
      Color := clWhite;

  if (g_WeaponsCount = 0) then exit;
  if (texNum < 0) then exit;
  if (not canInput) then exit;

  weapItemsInMem[texNum].name := WeaponNameEdit.Text;
end;

procedure TfrmWeaponsAmmoEditor.BulletsGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_BulletsCount = 0) then exit;
  //if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (bulletsInMem[pos].framesCount = 0) then exit;
  bmp := bulletsInMem[pos].frames[0];

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmWeaponsAmmoEditor.BulletsGridSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  FormPrepare;

  if (g_BulletsCount = 0) then exit;

  BulletLoadTextureBtn.Enabled   := True;
  BulletClearTextureBtn.Enabled  := True;
  BulletDeleteTextureBtn.Enabled := True;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  texNum := pos;
  frmTexNum := 0;
  if (bulletsInMem[pos].framesCount = 0) then exit;
  bmp := bulletsInMem[pos].frames[0];
  if (bmp = nil) then exit;

  LoadPreviewImageFromBitmap(BulletPreviewImage, bmp);
  BulletParamsGroupBox.Enabled := True;
  BulletFramesGroupBox.Enabled := True;
  BulletDeleteColorBtn.Enabled := True;

  with (bulletsInMem[pos]) do
    begin
      BulletNameEdit.Text       := name;
      BulletSpeedSpin.Value     := speed;
      BulletLifeTimeSpin.Value  := lifeTime;
      BulletLifeFrameLastSpin.Value    := lifeFrameLast;
      BulletLifeFrameLastSpin.MaxValue := framesCount;
      BulletAnimSpeedSpin.Value := animSpeed;
      BulletSolidXSpin.Value    := solidX;
      BulletSolidYSpin.Value    := solidY;
    end;

  canInput := True;
  BulletFramesGridUpdate; 
  //BulletsGridSelection(Sender, aCol, aRow);
end;

procedure TfrmWeaponsAmmoEditor.WeaponPickSoundsCmbBoxChange(Sender: TObject);
begin
  if (g_WeaponsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
  if (not canInput) then exit;

  weapItemsInMem[texNum].pickUpSoundId := WeaponPickSoundsCmbBox.ItemIndex;
end;

procedure TfrmWeaponsAmmoEditor.WeaponsGridDblClick(Sender: TObject);
begin
  if (g_WeaponsCount <= 0) then
    exit;

  WeaponEditBtnClick(Self);
end;

procedure TfrmWeaponsAmmoEditor.WeaponStretchImageCheckChange(Sender: TObject);
begin
  WeaponPreviewImage.Stretch := WeaponStretchImageCheck.Checked;
end;

procedure TfrmWeaponsAmmoEditor.AnimatorTimer(Sender: TObject);
const
  animFrame : Integer = -1;
var
  framesCount : UInt32;
begin
  case (PageControl1.ActivePageIndex) of
    // оружие
    0 : begin
          framesCount := weapItemsInMem[texNum].framesCount;

          Inc(animFrame);
          if (animFrame >= framesCount) then
            animFrame := 0;

          while (weapItemsInMem[texNum].frames[animFrame] = nil) do
            begin
              Inc(animFrame);
              if (animFrame >= framesCount) then
                animFrame := 0;
            end;

          LoadPreviewImageFromBitmap(WeaponPreviewImage, weapItemsInMem[texNum].frames[animFrame]);
        end;
    // пули
    1 : begin
          framesCount := bulletsInMem[texNum].framesCount;

          Inc(animFrame);
          if (animFrame >= framesCount) then
            animFrame := 0;

          while (bulletsInMem[texNum].frames[animFrame] = nil) do
            begin
              Inc(animFrame);
              if (animFrame >= framesCount) then
                animFrame := 0;
            end;

          LoadPreviewImageFromBitmap(BulletPreviewImage, bulletsInMem[texNum].frames[animFrame]);
        end;      
    // аммо
    2 : begin
          framesCount := ammoInMem[texNum].framesCount;

          Inc(animFrame);
          if (animFrame >= framesCount) then
            animFrame := 0;

          while (ammoInMem[texNum].frames[animFrame] = nil) do
            begin
              Inc(animFrame);
              if (animFrame >= framesCount) then
                animFrame := 0;
            end;

          LoadPreviewImageFromBitmap(AmmoPreviewImage, ammoInMem[texNum].frames[animFrame]);
        end;
  end;
end;

procedure TfrmWeaponsAmmoEditor.BulletAutosizeBtnClick(Sender: TObject);
var
  image  : TImageData;
  stream : TMemoryStream;

  x, y : Integer;
  clr : TColor32Rec;
  halfW, halfH : Integer;
  minW, maxW : Integer;
  minH, maxH : Integer;
  w0, w1 : Integer;
begin
  AnimatorDisable;

  if (g_BulletsCount = 0) then exit;

  with (bulletsInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      stream := TMemoryStream.Create;
      frames[frmTexNum].SaveToStream(stream);

      stream.Position := 0;
      FreeImage(image);
      LoadImageFromStream(stream, image);
      stream.Clear;

      //
      // высчитываем solid box
      //
      halfW := image.Width  shr 1;
      minW := halfW;
      maxW := halfW;

      halfH := image.Height shr 1;
      minH := halfH;
      maxH := halfH;

      for y := 0 to image.Height - 1 do
        for x := 0 to image.Width - 1 do
          begin
            clr := (PColor32Rec(image.Bits) + y * image.Width + x)^;
            if (clr.A <> 0) then
              begin
                if ((x <= halfW) and (x < minW)) then minW := x;
                if ((x >  halfW) and (x > maxW)) then maxW := x;
                //if (y < minH) then minH := y; 
                if ((y <= halfH) and (y < minH)) then minH := y;
                if ((y >  halfH) and (y > maxH)) then maxH := y;
              end;
          end;
      // устанавливаем ширину
      // если minW = halfW и maxW = halfW, значит текстура вся непрозрачная по ширине
      if ((minW = halfW) and (maxW = halfW)) then
        solidX := 100
      // проверяем что дальше от центра
      else
        begin
          // сначала установим ширину
          w0 := halfW - minW;
          w1 := maxW - halfW;
          // то, что дальше, то и будет взято за основу
          if (w1 > w0) then
            w0 := w1;
          w0 := w0 shl 1;
          solidX := (w0 / image.Width) * 100;
        end;

      // устанавливаем высоту
      // если minH = halfH и maxH = halfH, значит текстура вся непрозрачная по высоте
      if ((minH = halfH) and (maxH = halfH)) then
        solidY := 100
      // проверяем что дальше от центра
      else
        begin
          // сначала установим ширину
          w0 := halfH - minH;
          w1 := maxH - halfH;
          // то, что дальше, то и будет взято за основу
          if (w1 > w0) then
            w0 := w1;
          w0 := w0 shl 1;
          solidY := (w0 / image.Height) * 100;
        end;

      BulletSolidXSpin.Value := solidX;
      BulletSolidYSpin.Value := solidY;

      FreeImage(image);
      stream.Destroy;
    end;

  BulletPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.WeaponAutosizeBtnClick(Sender: TObject);
var
  image  : TImageData;
  stream : TMemoryStream;

  x, y : Integer;
  clr : TColor32Rec;
  halfW : Integer;
  minW, maxW : Integer;
  minH : Integer;
  w0, w1 : Integer;
begin
  AnimatorDisable;

  if (g_WeaponsCount = 0) then exit;

  with (weapItemsInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      stream := TMemoryStream.Create;
      frames[frmTexNum].SaveToStream(stream);

      stream.Position := 0;
      FreeImage(image);
      LoadImageFromStream(stream, image);
      stream.Clear;

      //
      // высчитываем solid box
      //
      halfW := image.Width  shr 1;
      minW := halfW;
      maxW := halfW;
      minH := image.Height;
      for y := 0 to image.Height - 1 do
        for x := 0 to image.Width - 1 do
          begin
            clr := (PColor32Rec(image.Bits) + y * image.Width + x)^;
            if (clr.A <> 0) then
              begin
                if ((x <= halfW) and (x < minW)) then minW := x;
                if ((x >  halfW) and (x > maxW)) then maxW := x;
                if (y < minH) then minH := y;
              end;
          end;
      // если minW = halfW и maxW = halfW, значит текстура вся непрозрачная
      if ((minW = halfW) and (maxW = halfW)) then
        solidX := 100
      // проверяем что дальше от центра
      else
        begin
          w0 := halfW - minW;
          w1 := maxW - halfW;
          // то, что дальше, то и будет взято за основу
          if (w1 > w0) then
            w0 := w1;
          w0 := w0 shl 1;
          solidX := (w0 / image.Width) * 100;
        end;
      // устанавливаем высоту
      if (minH <> image.Height) then
        minH := image.Height - minH;
      solidY := (minH / image.Height) * 100;

      WeaponSolidXSpin.Value := solidX;
      WeaponSolidYSpin.Value := solidY;

      FreeImage(image);
      stream.Destroy;
    end;

  WeaponPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.WeaponEditBtnClick(Sender: TObject);
var
  frmWeaponEditor : TfrmWeaponEditor;
begin
  if (texNum < 0) or (texNum >= g_WeaponsCount) then exit;

  LoadWeapon(texNum);

  frmWeaponEditor := TfrmWeaponEditor.Create(Self);
  with (frmWeaponEditor) do
    try
      ShowModal;
    finally
      Destroy;
      frmWeaponEditor := nil;
    end;

  FormPrepare;
end;

procedure TfrmWeaponsAmmoEditor.BulletAnimSpeedSpinChange(Sender: TObject);
begin
  if (g_BulletsCount = 0) then exit;

  bulletsInMem[texNum].animSpeed := BulletAnimSpeedSpin.Value;
  if (BulletAnimSpeedSpin.Value <> 0) then
    Animator.Interval := 1000 div BulletAnimSpeedSpin.Value
  else
    Animator.Interval := 0;
end;

procedure TfrmWeaponsAmmoEditor.BulletAnimSpeedSpinChangeBounds(Sender: TObject
  );
begin

end;

procedure TfrmWeaponsAmmoEditor.BulletFramePlayBtnClick(Sender: TObject);
begin
  if (g_BulletsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_BulletsCount)) then exit;
  if (bulletsInMem[texNum].framesCount = 0) then exit;

  if (Animator.Enabled) then
    begin
      AnimatorDisable;
      DeleteColorModeDisable;
      if (texNum < g_BulletsCount) then
        with (bulletsInMem[texNum]) do
          if (frmTexNum < framesCount) then
            LoadPreviewImageFromBitmap(BulletPreviewImage, frames[frmTexNum]);
    end
  else
    AnimatorEnable;
end;

procedure TfrmWeaponsAmmoEditor.BulletFramesGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_BulletsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_BulletsCount)) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (bulletsInMem[texNum]) do
    begin
      if (framesCount = 0) then exit;
      if (pos >= framesCount) then exit;
      bmp := frames[pos];
    end;

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmWeaponsAmmoEditor.BulletFramesGridSelectCell(Sender: TObject;
  aCol, aRow: Integer; var CanSelect: Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  BulletPreviewImage.Picture.Clear;

  BulletLoadFrameBtn.Enabled   := False;
  BulletClearFrameBtn.Enabled  := False;
  BulletDeleteFrameBtn.Enabled := False;

  frmTexNum := 0;
  if ((texNum < 0) or
      (texNum >= Length(bulletsInMem)) or
      (bulletsInMem[texNum].framesCount = 0)) then
    exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (bulletsInMem[texNum]) do
    begin
      if (pos >= framesCount) then exit;

      frmTexNum := pos;
      if (pos > 0) then
        begin
          BulletLoadFrameBtn.Enabled   := True;
          BulletClearFrameBtn.Enabled  := True;
          BulletDeleteFrameBtn.Enabled := True;
        end;
      bmp := frames[pos];
      if (bmp = nil) then exit;

      LoadPreviewImageFromBitmap(BulletPreviewImage, bmp);
    end;
end;

procedure TfrmWeaponsAmmoEditor.BulletLifeFrameLastSpinChange(Sender: TObject);
begin
  if (g_BulletsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_BulletsCount)) then exit;
  if (not canInput) then exit;

  bulletsInMem[texNum].lifeFrameLast := BulletLifeFrameLastSpin.Value;
end;

procedure TfrmWeaponsAmmoEditor.BulletNameEditChange(Sender: TObject);
begin     
  with (BulletNameEdit) do
    if (Text = '') then
      Color := clRed
    else
      Color := clWhite;

  if (g_BulletsCount = 0) then exit;
  if (texNum < 0) then exit;
  if (not canInput) then exit;

  bulletsInMem[texNum].name := BulletNameEdit.Text;
end;

procedure TfrmWeaponsAmmoEditor.BulletsGridSelection(Sender: TObject; aCol,
  aRow: Integer);
begin
  WeaponsGrid.Options := [goAlwaysShowEditor, goSmoothScroll];
  BulletsGrid.Options := [goAlwaysShowEditor, goSmoothScroll, goDrawFocusSelected];
  AmmoGrid.Options    := [goAlwaysShowEditor, goSmoothScroll];

  BulletsGrid.SetFocus;
end;

procedure TfrmWeaponsAmmoEditor.BulletLifeTimeSpinChange(Sender: TObject);
begin
  if (g_BulletsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_BulletsCount)) then exit;
  if (not canInput) then exit;

  bulletsInMem[texNum].lifeTime := BulletLifeTimeSpin.Value;
end;

procedure TfrmWeaponsAmmoEditor.BulletSpeedSpinChange(Sender: TObject);
begin
  if (g_BulletsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_BulletsCount)) then exit;
  if (not canInput) then exit;

  bulletsInMem[texNum].speed := BulletSpeedSpin.Value;
end;

procedure TfrmWeaponsAmmoEditor.FormActivate(Sender: TObject);
var
  canSelect : Boolean;
begin
  case (PageControl1.ActivePageIndex) of
    0 : begin
          if (WeaponsGrid.ColCount > 0) and
             (WeaponsGrid.RowCount > 0) then
            WeaponsGridSelectCell(WeaponsGrid, 0, 0, canSelect);
          WeaponsGrid.SetFocus;
        end;

    1 : begin
          if (BulletsGrid.ColCount > 0) and
             (BulletsGrid.RowCount > 0) then
            BulletsGridSelectCell(BulletsGrid, 0, 0, canSelect); 
          BulletsGrid.SetFocus;
        end;

    2 : begin
          if (AmmoGrid.ColCount > 0) and
             (AmmoGrid.RowCount > 0) then
            AmmoGridSelectCell(AmmoGrid, 0, 0, canSelect);
          AmmoGrid.SetFocus;
        end;
  end;
end;

procedure TfrmWeaponsAmmoEditor.WeaponClearPickSoundBtnClick(Sender: TObject);
begin
  if (g_WeaponsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
  if (not canInput) then exit;

  WeaponPickSoundsCmbBox.ItemIndex := -1;
  weapItemsInMem[texNum].pickUpSoundId := -1;
end;

procedure TfrmWeaponsAmmoEditor.BulletPreviewImageMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (not deleteColorMode) then
    canChooseSolid := True;
end;

procedure TfrmWeaponsAmmoEditor.BulletPreviewImageMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (not deleteColorMode) then
    canChooseSolid := False;
  BulletPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.WeaponPickTypeCmbBoxChange(Sender: TObject);
begin
  if (g_WeaponsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
  if (not canInput) then exit;

  weapItemsInMem[texNum].ammoTypeId := WeaponPickTypeCmbBox.ItemIndex;
  if (WeaponPickTypeCmbBox.ItemIndex >= 0) then
    InfiniteChk.Checked := False;
end;

procedure TfrmWeaponsAmmoEditor.WeaponPreviewImageMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (not deleteColorMode) then
    canChooseSolid := True;
end;

procedure TfrmWeaponsAmmoEditor.WeaponPreviewImageMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (not deleteColorMode) then
    canChooseSolid := False;
  WeaponPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.WeaponPreviewImagePaint(Sender: TObject);
var
  x0, x1, y0, y1 : Integer;
  xCoeff, yCoeff : Single;    // соотношение реального размера к растянутому в превью
  w, h : Integer;
  sW, sH : Integer;
begin
  if (g_WeaponsCount = 0) then exit;
  with (weapItemsInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[0] = nil)) then exit;

      w := WeaponPreviewImage.Picture.Width;
      h := WeaponPreviewImage.Picture.Height;

      sW := ceil(w * (solidX / 100.0));
      sH := ceil(h * (solidY / 100.0));

      x0 := (w - sW) shr 1;
      x1 := w - x0;
      y0 := h - sH;
      y1 := h;

      if (WeaponStretchImageCheck.Checked) then
        begin
          xCoeff := WeaponPreviewImage.Width  / w;
          yCoeff := WeaponPreviewImage.Height / h;

          x0 := ceil(x0 * xCoeff);
          x1 := ceil(x1 * xCoeff);
          y0 := ceil(y0 * yCoeff);
          y1 := ceil(y1 * yCoeff);
        end
      else
        begin
          xCoeff := 1.0;
          yCoeff := 1.0;
        end;

      if (x0 < 0) then x0 := 0;
      if (y0 < 0) then y0 := 0;
      if (x1 >= WeaponPreviewImage.Width ) then x1 := WeaponPreviewImage.Width  - 1;
      if (y1 >= WeaponPreviewImage.Height) then y1 := WeaponPreviewImage.Height - 1;

      with (WeaponPreviewImage.Canvas) do
        begin
          Pen.Color := clRed;
          Line(x0, y0, x1, y0);
          Line(x1, y0, x1, y1);
          Line(x0, y1, x1, y1);
          Line(x0, y0, x0, y1);
          // перекрестие
          if (canChooseSolid) then
            begin
              Line(x0, y0, x1, y1);
              Line(x1, y0, x0, y1);
            end;

          Pen.Color   := clLime;
          Brush.Color := clLime;
          Rectangle(x0 - 3, y0 - 3, x0 + 3, y0 + 3);
          Rectangle(x0 - 3, y1 - 3, x0 + 3, y1 + 3);
          Rectangle(x1 - 3, y0 - 3, x1 + 3, y0 + 3);
          Rectangle(x1 - 3, y1 - 3, x1 + 3, y1 + 3);

          // рамка рисунка
          if (not WeaponStretchImageCheck.Checked) then
            begin
              Pen.Color := clGray;
              Line(0, 0, w, 0);
              Line(w, 0, w, h);
              Line(0, h, w, h);
              Line(0, 0, 0, h);
            end;
        end;
    end;
end;

procedure TfrmWeaponsAmmoEditor.RaycastingCheckChange(Sender: TObject);
begin
  if (RaycastingCheck.Checked) then
    begin       
      if (g_WeaponsCount = 0) then exit;
      if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
      if (not canInput) then exit;

      BulletCheck.Checked    := False;
      BulletCmbBox.Enabled   := False;
      BulletCmbBox.ItemIndex := -1;  
      weapItemsInMem[texNum].fireType := 0;
      weapItemsInMem[texNum].bulletId := -1;
    end
  else
    BulletCheck.Checked := True;
end;

procedure TfrmWeaponsAmmoEditor.BulletClearFrameBtnClick(Sender: TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  BulletPreviewImage.Picture.Clear;

  with (bulletsInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      frames[frmTexNum].Clear;
      frames[frmTexNum].Destroy;
      frames[frmTexNum] := nil;

      DeleteFile(g_BulPath + '/bullet_' + IntToStr(texNum) + '_frame_' + IntToStr(frmTexNum) + '.dat');
    end;

  BulletFramesGridUpdate;
  BulletFramesGridSelectCell(BulletFramesGrid, 0, frmTexNum, canSelect); 
  GridSellectRow(BulletFramesGrid, frmTexNum);
end;

procedure TfrmWeaponsAmmoEditor.BulletClearTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
  i : Integer;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (MessageDlg('Warning!',
                 'Do you really want to clear bullet? It''s event clear all frames!',
                 mtWarning,
                 [mbYes, mbNo],
                 0) = mrNo) then
     exit;

  DeleteFile(g_BulPath + '/bullet_' + IntToStr(texNum) + '.cfg');
  for i := 0 to High(bulletsInMem[texNum].frames) do
    DeleteFile(g_BulPath + '/bullet_' + IntToStr(texNum) + '_frame_' + IntToStr(i) + '.dat');

  InitBullet(texNum);
  bulletsInMem[texNum].name := 'Bullet #' + IntToStr(texNum);

  BulletsGridUpdate;
  BulletsGridSelectCell(BulletsGrid, 0, texNum, canSelect);
  GridSellectRow(BulletsGrid, texNum);
end;

procedure TfrmWeaponsAmmoEditor.WeaponClearFrameBtnClick(Sender: TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable; 

  WeaponPreviewImage.Picture.Clear;

  with (weapItemsInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      frames[frmTexNum].Clear;
      frames[frmTexNum].Destroy;
      frames[frmTexNum] := nil;

      DeleteFile(g_WaPath + '/weapon_' + IntToStr(texNum) + '_frame_' + IntToStr(frmTexNum) + '.dat');
    end;

  WeaponFramesGridUpdate;
  WeaponFramesGridSelectCell(WeaponFramesGrid, 0, frmTexNum, canSelect);
  GridSellectRow(WeaponFramesGrid, frmTexNum);
end;

procedure TfrmWeaponsAmmoEditor.WeaponClearTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
  i : Integer;       
  weaponInMem : TWeaponInMem;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (MessageDlg('Warning!',
                 'Do you really want to clear texture of weapon? It''s event clear all frames!',
                 mtWarning,
                 [mbYes, mbNo],
                 0) = mrNo) then
     exit;

  // удаляем файлы
  // подбираемое
  DeleteFile(g_WaPath + '/weapon_' + IntToStr(texNum) + '.cfg');
  for i := 0 to High(weapItemsInMem[texNum].frames) do
    DeleteFile(g_WaPath + '/weapon_' + IntToStr(texNum) + '_frame_' + IntToStr(i) + '.dat');
  // в руках
  LoadWeapon(texNum, @weaponInMem);
  DeleteFile(g_WeapPath + '/weapon_inhand_' + IntToStr(texNum) + '.cfg');
  for i := 0 to High(weaponInMem.frames) do
    DeleteFile(g_WeapPath + '/weapon_inhand_' + IntToStr(texNum) + '_frame_' + IntToStr(i) + '.dat');
  InitWeapon(@weaponInMem);

  InitWeaponItem(texNum);
  weapItemsInMem[texNum].name := 'Weapon #' + IntToStr(texNum);

  WeaponsGridUpdate;                             
  GridSellectRow(WeaponsGrid, texNum);
  WeaponsGridSelectCell(WeaponsGrid, 0, texNum, canSelect);
end;

procedure TfrmWeaponsAmmoEditor.BulletDeleteColorBtnClick(Sender: TObject);
begin
  if ((texNum < 0) or (texNum >= Length(bulletsInMem))) then exit;
  if (bulletsInMem[texNum].framesCount = 0) then exit;
  if (bulletsInMem[texNum].frames[frmTexNum] = nil) then exit;

  deleteColorMode := not deleteColorMode;

  if (not deleteColorMode) then
    DeleteColorModeDisable
  else
    DeleteColorModeEnable;
end;

procedure TfrmWeaponsAmmoEditor.BulletDeleteFrameBtnClick(Sender: TObject);
var
  i : Integer;
  canSelect : Boolean;
begin
  BulletClearFrameBtnClick(Self);

  with (bulletsInMem[texNum]) do
    begin
      // сначала переносим все текстуры вниз
      for i := frmTexNum to framesCount - 2 do
        frames[i] := frames[i + 1];

      Dec(framesCount);
      Dec(frmTexNum);

      SetLength(frames, framesCount);

      BulletLifeFrameLastSpin.MaxValue := framesCount;
    end;

  BulletFramesGridUpdate;
  BulletFramesGridSelectCell(BulletFramesGrid, 0, frmTexNum, canSelect);  
  GridSellectRow(BulletFramesGrid, frmTexNum);
end;

procedure TfrmWeaponsAmmoEditor.BulletDeleteTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
  i : Integer;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (g_BulletsCount = 0) then exit;

  if (MessageDlg('Warning!',
                 'Do you really want to delete bullet? It''s event clear all frames!',
                 mtWarning,
                 [mbYes, mbNo],
                 0) = mrNo) then
     exit;

  DeleteFile(g_BulPath + '/bullet_' + IntToStr(texNum) + '.cfg');
  for i := 0 to High(bulletsInMem[texNum].frames) do
    DeleteFile(g_BulPath + '/bullet_' + IntToStr(texNum) + '_frame_' + IntToStr(i) + '.dat');

  InitBullet(texNum);

  // удаляем в оружиях ссылку на эту пулю
  if (g_WeaponsCount > 0) then
    begin
      for i := 0 to g_WeaponsCount - 1 do
        with (weapItemsInMem[i]) do
          if ((g_BulletsCount > 1) and (bulletId = texNum)) or
             (g_BulletsCount = 1) then   // если g_BulletsCount = 1, то при удалении станет 0,
                                         // а значит выставить всем оружиям рейкаст
            begin
              fireType := 0;
              bulletId := -1;
            end;
    end;
                                
  // теперь отодвигаем назад все текстуры
  if (texNum < g_BulletsCount) then
    begin
      for i := texNum to BULLETS_MAX_COUNT - 2 do
        bulletsInMem[i] := bulletsInMem[i + 1];
      InitBullet(g_BulletsCount - 1);
    end;

  Dec(g_BulletsCount);
  BulletsGridUpdate;
  BulletsGridSelectCell(BulletsGrid, 0, g_BulletsCount - 1, canSelect); 
  GridSellectRow(BulletsGrid, g_BulletsCount - 1);

  if (g_BulletsCount = 0) then
    begin
      BulletLoadTextureBtn.Enabled   := False;
      BulletClearTextureBtn.Enabled  := False;
      BulletDeleteTextureBtn.Enabled := False;
      BulletParamsGroupBox.Enabled  := False;
      BulletFramesGroupBox.Enabled  := False;
      BulletPreviewImage.Picture.Clear;
    end;
end;

procedure TfrmWeaponsAmmoEditor.DistanceSpinChange(Sender: TObject);
begin
  if (g_WeaponsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
  if (not canInput) then exit;

  weapItemsInMem[texNum].distance := DistanceSpin.Value;
end;

procedure TfrmWeaponsAmmoEditor.DamageSpinChange(Sender: TObject);
begin
  if (g_WeaponsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
  if (not canInput) then exit;

  weapItemsInMem[texNum].damage := DamageSpin.Value;
end;

procedure TfrmWeaponsAmmoEditor.WeaponAddTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (g_WeaponsCount < WEAPONS_MAX_COUNT) then
    begin
      texNum := g_WeaponsCount;

      Inc(g_WeaponsCount);
      WeaponsGridUpdate;
      WeaponsGrid.Col := 1;
      WeaponsGrid.Row := g_WeaponsCount;

      WeaponLoadTextureBtnClick(Self);

      weapItemsInMem[texNum].name := 'Weapon #' + IntToStr(texNum);
    end
  else
    MessageDlg('Error', 'Достигнут предел объектов оружия. Текущий максимум = ' + IntToStr(WEAPONS_MAX_COUNT), mtError, [mbOK], 0);
                                       
  GridSellectRow(WeaponsGrid, texNum);
  WeaponsGridSelectCell(WeaponsGrid, 0, texNum, canSelect);
end;

procedure TfrmWeaponsAmmoEditor.WeaponAddFrameBtnClick(Sender: TObject);
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  with (weapItemsInMem[texNum]) do
    begin
      frmTexNum := framesCount;
      Inc(framesCount);
      SetLength(frames, framesCount);
      frames[frmTexNum] := nil;

      WeaponFramesGridUpdate;
      WeaponFramesGrid.Col := 1;
      WeaponFramesGrid.Row := framesCount;
    end;

  WeaponLoadFrameBtnClick(Self);

  WeaponFramesGrid.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.AccuracySpinChange(Sender: TObject);
begin
  if (g_WeaponsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
  if (not canInput) then exit;

  weapItemsInMem[texNum].accuracy := AccuracySpin.Value;
end;

procedure TfrmWeaponsAmmoEditor.AmmoAddFrameBtnClick(Sender: TObject);
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  with (ammoInMem[texNum]) do
    begin
      frmTexNum := framesCount;
      Inc(framesCount);
      SetLength(frames, framesCount);
      frames[frmTexNum] := nil;

      AmmoFramesGridUpdate;
      AmmoFramesGrid.Col := 1;
      AmmoFramesGrid.Row := framesCount;
    end;

  AmmoLoadFrameBtnClick(Self);

  AmmoFramesGrid.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.AmmoAddTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (g_AmmoCount < AMMO_MAX_COUNT) then
    begin
      texNum := g_AmmoCount;

      Inc(g_AmmoCount);
      AmmoGridUpdate;
      AmmoGrid.Col := 1;
      AmmoGrid.Row := g_AmmoCount;

      AmmoLoadTextureBtnClick(Self);

      ammoInMem[texNum].name := 'Ammo #' + IntToStr(texNum);
    end
  else
    MessageDlg('Error', 'Достигнут предел объектов патронов. Текущий максимум = ' + IntToStr(AMMO_MAX_COUNT), mtError, [mbOK], 0);

  AmmoGridSelectCell(AmmoGrid, 0, texNum, canSelect);
end;

procedure TfrmWeaponsAmmoEditor.AmmoAnimSpeedSpinChange(Sender: TObject);
begin
  if (g_AmmoCount = 0) then exit;

  ammoInMem[texNum].animSpeed := AmmoAnimSpeedSpin.Value;
  if (AmmoAnimSpeedSpin.Value <> 0) then
    Animator.Interval := 1000 div AmmoAnimSpeedSpin.Value
  else
    Animator.Interval := 0;
end;

procedure TfrmWeaponsAmmoEditor.AmmoAutosizeBtnClick(Sender: TObject);
var
  image  : TImageData;
  stream : TMemoryStream;

  x, y : Integer;
  clr : TColor32Rec;
  halfW : Integer;
  minW, maxW : Integer;
  minH : Integer;
  w0, w1 : Integer;
begin
  AnimatorDisable;

  if (g_AmmoCount = 0) then exit;

  with (ammoInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      stream := TMemoryStream.Create;
      frames[frmTexNum].SaveToStream(stream);

      stream.Position := 0;
      FreeImage(image);
      LoadImageFromStream(stream, image);
      stream.Clear;

      //
      // высчитываем solid box
      //
      halfW := image.Width  shr 1;
      minW := halfW;
      maxW := halfW;
      minH := image.Height;
      for y := 0 to image.Height - 1 do
        for x := 0 to image.Width - 1 do
          begin
            clr := (PColor32Rec(image.Bits) + y * image.Width + x)^;
            if (clr.A <> 0) then
              begin
                if ((x <= halfW) and (x < minW)) then minW := x;
                if ((x >  halfW) and (x > maxW)) then maxW := x;
                if (y < minH) then minH := y;
              end;
          end;
      // если minW = halfW и maxW = halfW, значит текстура вся непрозрачная
      if ((minW = halfW) and (maxW = halfW)) then
        solidX := 100
      // проверяем что дальше от центра
      else
        begin
          w0 := halfW - minW;
          w1 := maxW - halfW;
          // то, что дальше, то и будет взято за основу
          if (w1 > w0) then
            w0 := w1;
          w0 := w0 shl 1;
          solidX := (w0 / image.Width) * 100;
        end;
      // устанавливаем высоту
      if (minH <> image.Height) then
        minH := image.Height - minH;
      solidY := (minH / image.Height) * 100;

      AmmoSolidXSpin.Value := solidX;
      AmmoSolidYSpin.Value := solidY;

      FreeImage(image);
      stream.Destroy;
    end;

  AmmoPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.AmmoClearFrameBtnClick(Sender: TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  AmmoPreviewImage.Picture.Clear;

  with (ammoInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      frames[frmTexNum].Clear;
      frames[frmTexNum].Destroy;
      frames[frmTexNum] := nil;

      DeleteFile(g_WaPath + '/ammo_' + IntToStr(texNum) + '_frame_' + IntToStr(frmTexNum) + '.dat');
    end;

  AmmoFramesGridUpdate;
  AmmoFramesGridSelectCell(AmmoFramesGrid, 0, frmTexNum, canSelect);
  GridSellectRow(AmmoFramesGrid, frmTexNum);
end;

procedure TfrmWeaponsAmmoEditor.AmmoClearPickSoundBtnClick(Sender: TObject);
begin
  if (g_AmmoCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_AmmoCount)) then exit;
  if (not canInput) then exit;

  AmmoPickSoundsCmbBox.ItemIndex := -1;
  ammoInMem[texNum].pickUpSoundId := -1;
end;

procedure TfrmWeaponsAmmoEditor.AmmoClearTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
  i : Integer;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (MessageDlg('Warning!',
                 'Do you really want to clear ammo? It''s event clear all frames!',
                 mtWarning,
                 [mbYes, mbNo],
                 0) = mrNo) then
     exit;

  DeleteFile(g_WaPath + '/ammo_' + IntToStr(texNum) + '.cfg');
  for i := 0 to High(ammoInMem[texNum].frames) do
    DeleteFile(g_WaPath + '/ammo_' + IntToStr(texNum) + '_frame_' + IntToStr(i) + '.dat');

  InitAmmo(texNum);
  ammoInMem[texNum].name := 'Ammo #' + IntToStr(texNum);
  if (ammoTypes.Count > 0) then
    ammoInMem[texNum].ammoTypeId := 0;

  AmmoGridUpdate;
  AmmoGridSelectCell(AmmoGrid, 0, texNum, canSelect);
  GridSellectRow(AmmoGrid, texNum);
end;

procedure TfrmWeaponsAmmoEditor.AmmoDeleteColorBtnClick(Sender: TObject);
begin
  if ((texNum < 0) or (texNum >= Length(ammoInMem))) then exit;
  if (ammoInMem[texNum].framesCount = 0) then exit;
  if (ammoInMem[texNum].frames[frmTexNum] = nil) then exit;

  deleteColorMode := not deleteColorMode;

  if (not deleteColorMode) then
    DeleteColorModeDisable
  else
    DeleteColorModeEnable;
end;

procedure TfrmWeaponsAmmoEditor.AmmoDeleteFrameBtnClick(Sender: TObject);
var
  i : Integer;
  canSelect : Boolean;
begin
  AmmoClearFrameBtnClick(Self);

  with (ammoInMem[texNum]) do
    begin
      // сначала переносим все текстуры вниз
      for i := frmTexNum to framesCount - 2 do
        frames[i] := frames[i + 1];

      Dec(framesCount);
      Dec(frmTexNum);

      SetLength(frames, framesCount);
    end;

  AmmoFramesGridUpdate;
  AmmoFramesGridSelectCell(AmmoFramesGrid, 0, frmTexNum, canSelect);
  GridSellectRow(AmmoFramesGrid, frmTexNum);
end;

procedure TfrmWeaponsAmmoEditor.AmmoDeleteTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
  i : Integer;    
  x, y, m : Integer;
  cell : Integer;
  fromPos, toPos : Integer;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (g_AmmoCount = 0) then exit;

  if (MessageDlg('Warning!',
                 'Do you really want to delete ammo? It''s event clear all frames!',
                 mtWarning,
                 [mbYes, mbNo],
                 0) = mrNo) then
     exit;

  DeleteFile(g_WaPath + '/ammo_' + IntToStr(texNum) + '.cfg');
  for i := 0 to High(ammoInMem[texNum].frames) do
    DeleteFile(g_WaPath + '/ammo_' + IntToStr(texNum) + '_frame_' + IntToStr(i) + '.dat');

  InitAmmo(texNum);
                         
  // удаляем на картах элементы, которые использовали удаленную текстуру
  //CheckMaps;
  if (texNum < g_AmmoCount) then
    begin
      // сдвигаем назад все элементы на карте
      if (texNum >= 0) then
        begin
          fromPos := TEXTURES_MAX_COUNT +
                     SPRITES_MAX_COUNT  +
                     DOORS_MAX_COUNT    +
                     KEYS_MAX_COUNT     +
                     WEAPONS_MAX_COUNT;
          toPos   := fromPos + AMMO_MAX_COUNT;

          for m := 0 to g_MapsCount - 1 do
            with (mapPack.maps[m]) do
              for x := 0 to MAP_WIDTH  - 1 do
                for y := 0 to MAP_HEIGHT - 1 do
                  begin
                    cell := level[x, y] - 1;
                    if ((cell >= fromPos) and (cell < toPos)) then
                      begin
                        if (cell > texNum + fromPos) then
                          level[x, y] := cell
                        else
                          level[x, y] := 0;
                      end;
                  end;
        end;

      // теперь отодвигаем назад все текстуры
      if (texNum < g_AmmoCount) then
        begin
          for i := texNum to AMMO_MAX_COUNT - 2 do
            ammoInMem[i] := ammoInMem[i + 1];
          InitAmmo(g_AmmoCount - 1);
        end;
    end;

  Dec(g_AmmoCount);
  AmmoGridUpdate;
  AmmoGridSelectCell(AmmoGrid, 0, g_AmmoCount - 1, canSelect);
  GridSellectRow(AmmoGrid, g_AmmoCount - 1);

  if (g_AmmoCount = 0) then
    begin
      AmmoLoadTextureBtn.Enabled   := False;
      AmmoClearTextureBtn.Enabled  := False;
      AmmoDeleteTextureBtn.Enabled := False;
      AmmoParamsGroupBox.Enabled  := False;
      AmmoFramesGroupBox.Enabled  := False;
      AmmoPreviewImage.Picture.Clear;
    end;
end;

procedure TfrmWeaponsAmmoEditor.AmmoFramePlayBtnClick(Sender: TObject);
begin
  if (g_AmmoCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_AmmoCount)) then exit;
  if (ammoInMem[texNum].framesCount = 0) then exit;

  if (Animator.Enabled) then
    begin
      AnimatorDisable;
      DeleteColorModeDisable;
      if (texNum < g_AmmoCount) then
        with (ammoInMem[texNum]) do
          if (frmTexNum < framesCount) then
            LoadPreviewImageFromBitmap(AmmoPreviewImage, frames[frmTexNum]);
    end
  else
    AnimatorEnable;
end;

procedure TfrmWeaponsAmmoEditor.AmmoFramesGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_AmmoCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_AmmoCount)) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (ammoInMem[texNum]) do
    begin
      if (framesCount = 0) then exit;
      if (pos >= framesCount) then exit;
      bmp := frames[pos];
    end;

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmWeaponsAmmoEditor.AmmoFramesGridSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  AmmoPreviewImage.Picture.Clear;

  AmmoLoadFrameBtn.Enabled   := False;
  AmmoClearFrameBtn.Enabled  := False;
  AmmoDeleteFrameBtn.Enabled := False;

  frmTexNum := 0;
  if ((texNum < 0) or
      (texNum >= Length(ammoInMem)) or
      (ammoInMem[texNum].framesCount = 0)) then
    exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  with (ammoInMem[texNum]) do
    begin
      if (pos >= framesCount) then exit;

      frmTexNum := pos;
      if (pos > 0) then
        begin
          AmmoLoadFrameBtn.Enabled   := True;
          AmmoClearFrameBtn.Enabled  := True;
          AmmoDeleteFrameBtn.Enabled := True;
        end;
      bmp := frames[pos];
      if (bmp = nil) then exit;

      LoadPreviewImageFromBitmap(AmmoPreviewImage, bmp);
    end;
end;

procedure TfrmWeaponsAmmoEditor.AmmoGridClick(Sender: TObject);
begin

end;

procedure TfrmWeaponsAmmoEditor.AmmoGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_AmmoCount = 0) then exit;
  //if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (ammoInMem[pos].framesCount = 0) then exit;
  bmp := ammoInMem[pos].frames[0];

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmWeaponsAmmoEditor.AmmoGridSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  FormPrepare;

  if (g_AmmoCount = 0) then exit;

  AmmoLoadTextureBtn.Enabled   := True;
  AmmoClearTextureBtn.Enabled  := True;
  AmmoDeleteTextureBtn.Enabled := True;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  texNum := pos;
  frmTexNum := 0;
  if (ammoInMem[pos].framesCount = 0) then exit;
  bmp := ammoInMem[pos].frames[0];
  if (bmp = nil) then exit;

  LoadPreviewImageFromBitmap(AmmoPreviewImage, bmp);
  AmmoParamsGroupBox.Enabled := True;
  AmmoFramesGroupBox.Enabled := True;
  AmmoDeleteColorBtn.Enabled := True;

  with (ammoInMem[pos]) do
    begin
      AmmoNameEdit.Text       := name;
      AmmoVolumeSpin.Value    := volume;
      AmmoPickSoundsCmbBox.ItemIndex := pickUpSoundId;
      AmmoPickTypeCmbBox.ItemIndex := ammoTypeId;
      AmmoAnimSpeedSpin.Value := animSpeed;
      AmmoSolidXSpin.Value    := solidX;
      AmmoSolidYSpin.Value    := solidY;
    end;

  canInput := True;
  AmmoFramesGridUpdate;
  //AmmoGridSelection(Sender, aCol, aRow);
end;

procedure TfrmWeaponsAmmoEditor.AmmoGridSelection(Sender: TObject; aCol,
  aRow: Integer);
begin
  WeaponsGrid.Options := [goAlwaysShowEditor, goSmoothScroll];
  BulletsGrid.Options := [goAlwaysShowEditor, goSmoothScroll];
  AmmoGrid.Options    := [goAlwaysShowEditor, goSmoothScroll, goDrawFocusSelected];

  AmmoGrid.SetFocus;
end;

procedure TfrmWeaponsAmmoEditor.AmmoLoadFrameBtnClick(Sender: TObject);
var
  image : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (DM.OpenTextureDialog.Execute) then
    begin
      PrepareLoadTexture(image, TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

      // записываем изображение в поток
      stream := TMemoryStream.Create;
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image);
      FreeImage(image);

      stream.Position := 0;
      with (ammoInMem[texNum]) do
        begin
          if (frames[frmTexNum] = nil) then
            frames[frmTexNum] := TBitmap.Create;
          frames[frmTexNum].LoadFromStream(stream);
        end;
      stream.Destroy;

      AmmoFramesGridUpdate;
      AmmoFramesGridSelectCell(AmmoFramesGrid, 0, frmTexNum, canSelect);
      GridSellectRow(AmmoFramesGrid, frmTexNum);
    end;
end;

procedure TfrmWeaponsAmmoEditor.AmmoLoadTextureBtnClick(Sender: TObject);
var
  image  : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;
  i : Integer;
  filesCount : Integer;
  frmPleaseWait : TfrmPleaseWait;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if ((ammoInMem[texNum].framesCount > 0) and
      (MessageDlg('Warning!',
                  'Do you really want to load new texture for ammo? It''s event clear all old frames!',
                  mtWarning,
                  [mbYes, mbNo],
                  0) = mrNo)) then
     exit;

  frmPleaseWait := TfrmPleaseWait.Create(Self);
  DM.OpenTextureDialog.Options := g_OpenOptionsMulti;

  if (DM.OpenTextureDialog.Execute) then
    begin
      frmPleaseWait.Show;
      frmPleaseWait.Repaint;

      InitAmmo(texNum);

      filesCount := DM.OpenTextureDialog.Files.Count;

      ammoInMem[texNum].name := 'Ammo #' + IntToStr(texNum);
      ammoInMem[texNum].framesCount := filesCount;
      SetLength(ammoInMem[texNum].frames, filesCount);

      // выбрать хоть какой-то тип
      if (ammoTypes.Count > 0) then
        ammoInMem[texNum].ammoTypeId := 0
      else     
        ammoInMem[texNum].ammoTypeId := -1;

      stream := TMemoryStream.Create;
      for i := 0 to filesCount - 1 do
        begin
          ammoInMem[texNum].frames[i] := TBitmap.Create;

          PrepareLoadTexture(image, DM.OpenTextureDialog.Files[i], TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

          // записываем изображение в поток
          stream.Position := 0;
          SaveImageToStream('bmp', stream, image);
          FreeImage(image);

          stream.Position := 0;
          ammoInMem[texNum].frames[i].LoadFromStream(stream);
          stream.Clear;
        end;
      stream.Destroy;

      AmmoGridUpdate;
      AmmoGridSelectCell(AmmoGrid, 0, texNum + 1, canSelect);
      GridSellectRow(AmmoGrid, texNum + 1);
    end;

  frmPleaseWait.Destroy;
  DM.OpenTextureDialog.Options := g_OpenOptionsOnce;
end;

procedure TfrmWeaponsAmmoEditor.AmmoNameEditChange(Sender: TObject);
begin
  with (AmmoNameEdit) do
    if (Text = '') then
      Color := clRed
    else
      Color := clWhite;

  if (g_AmmoCount = 0) then exit;
  if (texNum < 0) then exit;
  if (not canInput) then exit;

  ammoInMem[texNum].name := AmmoNameEdit.Text;
end;

procedure TfrmWeaponsAmmoEditor.AmmoPickSoundsCmbBoxChange(Sender: TObject);
begin
  if (g_AmmoCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_AmmoCount)) then exit;
  if (not canInput) then exit;

  ammoInMem[texNum].pickUpSoundId := AmmoPickSoundsCmbBox.ItemIndex;
end;

procedure TfrmWeaponsAmmoEditor.AmmoPickTypeCmbBoxChange(Sender: TObject);
begin
  if (g_AmmoCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_AmmoCount)) then exit;
  if (not canInput) then exit;

  ammoInMem[texNum].ammoTypeId := AmmoPickTypeCmbBox.ItemIndex;
end;

procedure TfrmWeaponsAmmoEditor.AmmoPreviewImageClick(Sender: TObject);
var
  i : Integer;
  image : TImageData;
  bmp : TBitmap = nil;
  stream : TMemoryStream = nil;
  clr : TColor32Rec;             // цвет для удаления
  canSelect : Boolean;
begin
  if (g_AmmoCount = 0) then exit;
  if (not deleteColorMode) then exit;

  with (ammoInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      stream := TMemoryStream.Create;
      frames[frmTexNum].SaveToStream(stream);

      stream.Position := 0;
      FreeImage(image);
      LoadImageFromStream(stream, image);
      stream.Clear;

      // берем цвет для удаления и удаляем его в памяти
      clr := (PColor32Rec(image.Bits) + image.Width * deleteColorPos.y + deleteColorPos.x)^;
      for i := 0 to (image.Size div SizeOf(clr)) - 1 do
        begin
          if ((PColor32Rec(image.Bits) + i)^.Color = clr.Color) then
            (PColor32Rec(image.Bits) + i)^.A := 0;
        end;

      // сохраняем картинку из памяти в превью
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image);
      FreeImage(image);

      stream.Position := 0;
      bmp := TBitmap.Create;
      bmp.LoadFromStream(stream, stream.Size);
      stream.Destroy;

      LoadPreviewImageFromBitmap(AmmoPreviewImage, bmp);
      bmp.Destroy;

      // Если принять изменения, то сохранить фрейм из превью
      if (MessageDlg('Confirm', 'Do you want to save the current changes?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
        begin
          stream := TMemoryStream.Create;
          AmmoPreviewImage.Picture.SaveToStream(stream);

          stream.Position := 0;
          frames[frmTexNum].Clear;
          frames[frmTexNum].LoadFromStream(stream);

          stream.Destroy;
        end;
    end;

  AmmoFramesGridUpdate;
  AmmoFramesGridSelectCell(AmmoFramesGrid, 0, frmTexNum, canSelect);
end;

procedure TfrmWeaponsAmmoEditor.AmmoPreviewImageMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (not deleteColorMode) then
    canChooseSolid := True;
end;

procedure TfrmWeaponsAmmoEditor.AmmoPreviewImageMouseEnter(Sender: TObject);
begin
  if (deleteColorMode) then
    begin
      AmmoDeleteColorShp.Visible := True;
      Label27.Visible := True;
    end;
end;

procedure TfrmWeaponsAmmoEditor.AmmoPreviewImageMouseLeave(Sender: TObject);
begin
  AmmoDeleteColorShp.Visible := False;
  Label27.Visible := False;
  canChooseSolid  := False;
  AmmoPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.AmmoPreviewImageMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  _x, _y : Integer;
  coeffX, coeffY : Single;    // поправочные коэффициенты для взятия пикселя (в превью может быть растянуто)
  clr : TColor;
begin
  // высчитываем координаты пикселя
  if (not AmmoStretchImageCheck.Checked) then
    begin
      _x := X;
      _y := Y;
    end
  else
    begin
      coeffX := AmmoPreviewImage.Picture.Width  / AmmoPreviewImage.Width;
      coeffY := AmmoPreviewImage.Picture.Height / AmmoPreviewImage.Height;
      _x := round(X * coeffX);
      _y := round(Y * coeffY);
    end;
  if (_x > AmmoPreviewImage.Picture.Width ) then _x := AmmoPreviewImage.Picture.Width;
  if (_y > AmmoPreviewImage.Picture.Height) then _y := AmmoPreviewImage.Picture.Height;

  if (deleteColorMode) then
    begin
      AmmoDeleteColorShp.Brush.Color := clDefault;

      clr := (PInt32(AmmoPreviewImage.Picture.Bitmap.RawImage.Data) + AmmoPreviewImage.Picture.Width * deleteColorPos.y + deleteColorPos.x)^;
      if ((clr shr 24) <> 0) then
        AmmoDeleteColorShp.Brush.Color := AmmoPreviewImage.Picture.Bitmap.Canvas.Pixels[_x, _y];

      deleteColorPos.x := _x;
      deleteColorPos.y := _y;
    end
  else
    if (canChooseSolid) then
      begin
        if (_x > (AmmoPreviewImage.Picture.Width shr 1)) then
          _x := AmmoPreviewImage.Picture.Width - _x;

        AmmoSolidXSpin.Value := ((AmmoPreviewImage.Picture.Width  - _x shl 1) / AmmoPreviewImage.Picture.Width ) * 100.0;
        AmmoSolidYSpin.Value := ((AmmoPreviewImage.Picture.Height - _y      ) / AmmoPreviewImage.Picture.Height) * 100.0;

        AmmoPreviewImage.Invalidate;
      end;
end;

procedure TfrmWeaponsAmmoEditor.AmmoPreviewImageMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (not deleteColorMode) then
    canChooseSolid := False;
  AmmoPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.AmmoPreviewImagePaint(Sender: TObject);
var
  x0, x1, y0, y1 : Integer;
  xCoeff, yCoeff : Single;    // соотношение реального размера к растянутому в превью
  w, h : Integer;
  sW, sH : Integer;
begin
  if (g_AmmoCount = 0) then exit;
  with (ammoInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[0] = nil)) then exit;

      w := AmmoPreviewImage.Picture.Width;
      h := AmmoPreviewImage.Picture.Height;

      sW := ceil(w * (solidX / 100.0));
      sH := ceil(h * (solidY / 100.0));

      x0 := (w - sW) shr 1;
      x1 := w - x0;
      y0 := h - sH;
      y1 := h;

      if (AmmoStretchImageCheck.Checked) then
        begin
          xCoeff := AmmoPreviewImage.Width  / w;
          yCoeff := AmmoPreviewImage.Height / h;

          x0 := ceil(x0 * xCoeff);
          x1 := ceil(x1 * xCoeff);
          y0 := ceil(y0 * yCoeff);
          y1 := ceil(y1 * yCoeff);
        end
      else
        begin
          xCoeff := 1.0;
          yCoeff := 1.0;
        end;

      if (x0 < 0) then x0 := 0;
      if (y0 < 0) then y0 := 0;
      if (x1 >= AmmoPreviewImage.Width ) then x1 := AmmoPreviewImage.Width  - 1;
      if (y1 >= AmmoPreviewImage.Height) then y1 := AmmoPreviewImage.Height - 1;

      with (AmmoPreviewImage.Canvas) do
        begin
          Pen.Color := clRed;
          Line(x0, y0, x1, y0);
          Line(x1, y0, x1, y1);
          Line(x0, y1, x1, y1);
          Line(x0, y0, x0, y1);
          // перекрестие
          if (canChooseSolid) then
            begin
              Line(x0, y0, x1, y1);
              Line(x1, y0, x0, y1);
            end;

          Pen.Color   := clLime;
          Brush.Color := clLime;
          Rectangle(x0 - 3, y0 - 3, x0 + 3, y0 + 3);
          Rectangle(x0 - 3, y1 - 3, x0 + 3, y1 + 3);
          Rectangle(x1 - 3, y0 - 3, x1 + 3, y0 + 3);
          Rectangle(x1 - 3, y1 - 3, x1 + 3, y1 + 3);

          // рамка рисунка
          if (not AmmoStretchImageCheck.Checked) then
            begin
              Pen.Color := clGray;
              Line(0, 0, w, 0);
              Line(w, 0, w, h);
              Line(0, h, w, h);
              Line(0, 0, 0, h);
            end;
        end;
    end;
end;

procedure TfrmWeaponsAmmoEditor.AmmoSolidXSpinChange(Sender: TObject);
begin
  if (g_AmmoCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_AmmoCount)) then exit;
  if (not canInput) then exit;

  ammoInMem[texNum].solidX := AmmoSolidXSpin.Value;
  AmmoPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.AmmoSolidYSpinChange(Sender: TObject);
begin
  if (g_AmmoCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_AmmoCount)) then exit;
  if (not canInput) then exit;

  ammoInMem[texNum].solidY := AmmoSolidYSpin.Value;
  AmmoPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.AmmoStretchImageCheckChange(Sender: TObject);
begin
  AmmoPreviewImage.Stretch := AmmoStretchImageCheck.Checked;
end;

procedure TfrmWeaponsAmmoEditor.AmmoTypeAddBtnClick(Sender: TObject);
begin
  if (Trim(AmmoTypeNameEdit.Text) = '') then exit;

  ammoTypes.Add(AmmoTypeNameEdit.Text);
  AmmoTypesList.Items.Add(AmmoTypeNameEdit.Text);

  AmmoTypesList.ClearSelection;  
  AmmoTypesList.Selected[AmmoTypesList.Count - 1] := True;
end;

procedure TfrmWeaponsAmmoEditor.AmmoTypeDelBtnClick(Sender: TObject);
var
  i : Integer;
begin  
  if ((AmmoTypesList.Count = 0) or
      (texNum < 0) or
      (texNum >= AmmoTypesList.Count)) then
    begin
      AmmoTypeSaveBtn.Enabled := False;
      AmmoTypeDelBtn.Enabled  := False;
      exit;
    end;

  ammoTypes.Delete(texNum);
  AmmoTypesList.Items.Delete(texNum);

  // удаляем в оружиях ссылку на этот тип
  for i := 0 to g_WeaponsCount - 1 do
    with (weapItemsInMem[i]) do
      begin
        if (ammoTypeId > texNum) then
          Dec(ammoTypeId)
        else
          if ((ammoTypes.Count > 1) and (ammoTypeId = texNum)) or
             (ammoTypes.Count = 1) then   // если = 1, то при удалении станет 0
                                          // а значит выставить всем -1
            begin
              ammoTypeId := -1;
            end;
      end;

  // удаляем в патронах ссылку на этот тип
  for i := 0 to g_AmmoCount - 1 do
    with (ammoInMem[i]) do
      begin
        if (ammoTypeId > texNum) then
          Dec(ammoTypeId)
        else
          if ((ammoTypes.Count > 1) and (ammoTypeId = texNum)) or
             (ammoTypes.Count = 1) then   // если = 1, то при удалении станет 0
                                          // а значит выставить всем -1
            begin
              ammoTypeId := -1;
            end;
      end;

  texNum := -1;
  AmmoTypeNameEdit.Clear;
end;

procedure TfrmWeaponsAmmoEditor.AmmoTypeNameEditChange(Sender: TObject);
begin
  with (AmmoTypeNameEdit) do
    if (Text = '') then
      Color := clRed
    else
      Color := clWhite;
end;

procedure TfrmWeaponsAmmoEditor.AmmoTypeSaveBtnClick(Sender: TObject);
begin  
  if ((AmmoTypesList.Count = 0) or
      (texNum < 0) or
      (texNum >= AmmoTypesList.Count)) then
    begin
      AmmoTypeSaveBtn.Enabled := False;
      AmmoTypeDelBtn.Enabled  := False;
      exit;
    end;

  if (Trim(AmmoTypeNameEdit.Text) = '') then exit;

  ammoTypes[texNum]           := AmmoTypeNameEdit.Text;
  AmmoTypesList.Items[texNum] := AmmoTypeNameEdit.Text;
end;

procedure TfrmWeaponsAmmoEditor.AmmoTypesListSelectionChange(Sender: TObject;
  User: boolean);
begin
  if (AmmoTypesList.Count = 0) then
    begin
      AmmoTypeSaveBtn.Enabled := False;
      AmmoTypeDelBtn.Enabled  := False;
      exit;
    end;

  texNum := AmmoTypesList.ItemIndex;
  if (texNum < 0) then exit;

  AmmoTypeNameEdit.Text := AmmoTypesList.Items[texNum];
  AmmoTypeSaveBtn.Enabled := True;
  AmmoTypeDelBtn.Enabled  := True;
end;

procedure TfrmWeaponsAmmoEditor.AmmoVolumeSpinChange(Sender: TObject);
begin
  if (g_AmmoCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_AmmoCount)) then exit;
  if (not canInput) then exit;

  ammoInMem[texNum].volume := AmmoVolumeSpin.Value;
end;

procedure TfrmWeaponsAmmoEditor.BulletAddFrameBtnClick(Sender: TObject);
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  with (bulletsInMem[texNum]) do
    begin
      frmTexNum := framesCount;
      Inc(framesCount);
      SetLength(frames, framesCount);
      frames[frmTexNum] := nil;

      BulletFramesGridUpdate;
      BulletFramesGrid.Col := 1;
      BulletFramesGrid.Row := framesCount;

      BulletLifeFrameLastSpin.MaxValue := framesCount;
    end;

  BulletLoadFrameBtnClick(Self);

  BulletFramesGrid.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.BulletAddTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (g_BulletsCount < BULLETS_MAX_COUNT) then
    begin
      texNum := g_BulletsCount;

      Inc(g_BulletsCount);
      BulletsGridUpdate;
      BulletsGrid.Col := 1;
      BulletsGrid.Row := g_BulletsCount;

      BulletLoadTextureBtnClick(Self);

      bulletsInMem[texNum].name := 'Bullet #' + IntToStr(texNum);
    end
  else
    MessageDlg('Error', 'Достигнут предел объектов пуль. Текущий максимум = ' + IntToStr(BULLETS_MAX_COUNT), mtError, [mbOK], 0);

  BulletsGridSelectCell(BulletsGrid, 0, texNum, canSelect);
end;

procedure TfrmWeaponsAmmoEditor.CurAmmoSpinChange(Sender: TObject);
begin
  if (g_WeaponsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
  if (not canInput) then exit;

  weapItemsInMem[texNum].curAmmo := CurAmmoSpin.Value;
end;

procedure TfrmWeaponsAmmoEditor.WeaponDeleteColorBtnClick(Sender: TObject);
begin
  if ((texNum < 0) or (texNum >= Length(weapItemsInMem))) then exit;
  if (weapItemsInMem[texNum].framesCount = 0) then exit;
  if (weapItemsInMem[texNum].frames[frmTexNum] = nil) then exit;

  deleteColorMode := not deleteColorMode;

  if (not deleteColorMode) then
    DeleteColorModeDisable
  else
    DeleteColorModeEnable;
end;

procedure TfrmWeaponsAmmoEditor.WeaponDeleteFrameBtnClick(Sender: TObject);
var
  i : Integer;
  canSelect : Boolean;
begin
  WeaponClearFrameBtnClick(Self);

  with (weapItemsInMem[texNum]) do
    begin
      // сначала переносим все текстуры вниз
      for i := frmTexNum to framesCount - 2 do
        frames[i] := frames[i + 1];

      Dec(framesCount);
      Dec(frmTexNum);  

      SetLength(frames, framesCount);
    end;

  WeaponFramesGridUpdate;
  WeaponFramesGridSelectCell(WeaponFramesGrid, 0, frmTexNum, canSelect);
  GridSellectRow(WeaponFramesGrid, frmTexNum);
end;

procedure TfrmWeaponsAmmoEditor.WeaponDeleteTextureBtnClick(Sender: TObject);
var
  canSelect : Boolean;
  i : Integer;
  x, y, m : Integer;
  cell : Integer;
  fromPos, toPos : Integer;  
  weaponInMem : TWeaponInMem;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (g_WeaponsCount = 0) then exit;

  if (MessageDlg('Warning!',
                 'Do you really want to delete texture of weapon? It''s event clear all frames!',
                 mtWarning,
                 [mbYes, mbNo],
                 0) = mrNo) then
     exit;

  // удаляем файлы
  // подбираемое
  DeleteFile(g_WaPath + '/weapon_' + IntToStr(texNum) + '.cfg');
  for i := 0 to High(weapItemsInMem[texNum].frames) do
    DeleteFile(g_WaPath + '/weapon_' + IntToStr(texNum) + '_frame_' + IntToStr(i) + '.dat');
  // в руках
  LoadWeapon(texNum, @weaponInMem);
  DeleteFile(g_WeapPath + '/weapon_inhand_' + IntToStr(texNum) + '.cfg');
  for i := 0 to High(weaponInMem.frames) do
    DeleteFile(g_WeapPath + '/weapon_inhand_' + IntToStr(texNum) + '_frame_' + IntToStr(i) + '.dat');
  InitWeapon(@weaponInMem);

  InitWeaponItem(texNum);

  // удаляем на картах элементы, которые использовали удаленную текстуру
  //CheckMaps;
  if (texNum < g_WeaponsCount) then
    begin
      // сдвигаем назад все элементы на карте
      if (texNum >= 0) then
        begin
          fromPos := TEXTURES_MAX_COUNT +
                     SPRITES_MAX_COUNT  +
                     DOORS_MAX_COUNT    +
                     KEYS_MAX_COUNT;
          toPos   := fromPos + WEAPONS_MAX_COUNT;

          for m := 0 to g_MapsCount - 1 do
            with (mapPack.maps[m]) do
              for x := 0 to MAP_WIDTH  - 1 do
                for y := 0 to MAP_HEIGHT - 1 do
                  begin
                    cell := level[x, y] - 1;
                    if ((cell >= fromPos) and (cell < toPos)) then
                      begin
                        if (cell > texNum + fromPos) then
                          level[x, y] := cell
                        else
                          level[x, y] := 0;
                      end;
                  end;
        end;

      // теперь отодвигаем назад все текстуры
      for i := texNum to WEAPONS_MAX_COUNT - 2 do
        weapItemsInMem[i] := weapItemsInMem[i + 1];
      InitWeaponItem(g_WeaponsCount - 1);       
      InitWeapon;
    end;

  Dec(g_WeaponsCount);
  WeaponsGridUpdate;
  WeaponsGridSelectCell(WeaponsGrid, 0, g_WeaponsCount - 1, canSelect);
  GridSellectRow(WeaponsGrid, g_WeaponsCount - 1);

  if (g_WeaponsCount = 0) then
    begin
      WeaponLoadTextureBtn.Enabled   := False;
      WeaponClearTextureBtn.Enabled  := False;
      WeaponDeleteTextureBtn.Enabled := False;
      WeaponParamsGroupBox.Enabled   := False;
      WeaponFramesGroupBox.Enabled   := False;
      WeaponPreviewImage.Picture.Clear;
    end;
end;

procedure TfrmWeaponsAmmoEditor.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  SaveWeaponAmmoPack;
  SaveBulletPack;
  SaveAmmoTypes;
  InitBullets;

  if (ammoTypes <> nil) then
    begin
      ammoTypes.Destroy;
      ammoTypes := nil;
    end;
end;

procedure TfrmWeaponsAmmoEditor.InfiniteChkChange(Sender: TObject);
begin
  if (g_WeaponsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
  if (not canInput) then exit;

  weapItemsInMem[texNum].infiniteAmmo := InfiniteChk.Checked;
  if (InfiniteChk.Checked) then
    begin
      WeaponPickTypeCmbBox.ItemIndex := -1;
      weapItemsInMem[texNum].ammoTypeId := -1;
    end;
  CurAmmoSpin.Enabled := not InfiniteChk.Checked;
  MaxAmmoSpin.Enabled := not InfiniteChk.Checked;     
  WeaponPickTypeCmbBox.Enabled := not InfiniteChk.Checked;
end;

procedure TfrmWeaponsAmmoEditor.BulletLoadFrameBtnClick(Sender: TObject);
var
  image : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if (DM.OpenTextureDialog.Execute) then
    begin
      PrepareLoadTexture(image, TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

      // записываем изображение в поток
      stream := TMemoryStream.Create;
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image); 
      FreeImage(image);

      stream.Position := 0;
      with (bulletsInMem[texNum]) do
        begin
          if (frames[frmTexNum] = nil) then
            frames[frmTexNum] := TBitmap.Create;
          frames[frmTexNum].LoadFromStream(stream);
        end;
      stream.Destroy;

      BulletFramesGridUpdate;
      BulletFramesGridSelectCell(BulletFramesGrid, 0, frmTexNum, canSelect);
      GridSellectRow(BulletFramesGrid, frmTexNum);
    end;
end;

procedure TfrmWeaponsAmmoEditor.BulletLoadTextureBtnClick(Sender: TObject);
var
  image  : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;   
  i : Integer;
  filesCount : Integer; 
  frmPleaseWait : TfrmPleaseWait;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if ((bulletsInMem[texNum].framesCount > 0) and
      (MessageDlg('Warning!',
                  'Do you really want to load new texture for bullet? It''s event clear all old frames!',
                  mtWarning,
                  [mbYes, mbNo],
                  0) = mrNo)) then
     exit;
              
  frmPleaseWait := TfrmPleaseWait.Create(Self);
  DM.OpenTextureDialog.Options := g_OpenOptionsMulti;

  if (DM.OpenTextureDialog.Execute) then
    begin            
      frmPleaseWait.Show;
      frmPleaseWait.Repaint;

      InitBullet(texNum);

      filesCount := DM.OpenTextureDialog.Files.Count;

      bulletsInMem[texNum].name := 'Bullet #' + IntToStr(texNum);
      bulletsInMem[texNum].framesCount := filesCount;
      SetLength(bulletsInMem[texNum].frames, filesCount);
                                
      stream := TMemoryStream.Create;
      for i := 0 to filesCount - 1 do
        begin
          bulletsInMem[texNum].frames[i] := TBitmap.Create;

          PrepareLoadTexture(image, DM.OpenTextureDialog.Files[i], TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

          // записываем изображение в поток
          stream.Position := 0;
          SaveImageToStream('bmp', stream, image); 
          FreeImage(image);

          stream.Position := 0;
          bulletsInMem[texNum].frames[i].LoadFromStream(stream);
          stream.Clear;
        end;      
      stream.Destroy;

      BulletsGridUpdate;       
      BulletsGridSelectCell(BulletsGrid, 0, texNum + 1, canSelect);
      GridSellectRow(BulletsGrid, texNum + 1);
    end;

  frmPleaseWait.Destroy;
  DM.OpenTextureDialog.Options := g_OpenOptionsOnce;
end;

procedure TfrmWeaponsAmmoEditor.WeaponLoadFrameBtnClick(Sender: TObject);
var
  image : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;
begin
  AnimatorDisable;    
  DeleteColorModeDisable;

  if (DM.OpenTextureDialog.Execute) then
    begin
      PrepareLoadTexture(image, TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

      // записываем изображение в поток
      stream := TMemoryStream.Create;
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image); 
      FreeImage(image);

      stream.Position := 0;
      with (weapItemsInMem[texNum]) do
        begin
          if (frames[frmTexNum] = nil) then
            frames[frmTexNum] := TBitmap.Create;
          frames[frmTexNum].LoadFromStream(stream);
        end;
      stream.Destroy;

      WeaponFramesGridUpdate;
      WeaponFramesGridSelectCell(WeaponFramesGrid, 0, frmTexNum, canSelect);
      GridSellectRow(WeaponFramesGrid, frmTexNum);
    end;
end;

procedure TfrmWeaponsAmmoEditor.WeaponLoadTextureBtnClick(Sender: TObject);
var
  image  : TImageData;
  stream : TMemoryStream;
  canSelect : Boolean;  
  i : Integer;
  filesCount : Integer;  
  frmPleaseWait : TfrmPleaseWait;
begin
  AnimatorDisable;
  DeleteColorModeDisable;

  if ((weapItemsInMem[texNum].framesCount > 0) and
      (MessageDlg('Warning!',
                  'Do you really want to load new texture for weapon? It''s event clear all old frames!',
                  mtWarning,
                  [mbYes, mbNo],
                  0) = mrNo)) then
     exit;
                            
  frmPleaseWait := TfrmPleaseWait.Create(Self);
  DM.OpenTextureDialog.Options := g_OpenOptionsMulti;

  if (DM.OpenTextureDialog.Execute) then
    begin      
      frmPleaseWait.Show;
      frmPleaseWait.Repaint;

      InitWeaponItem(texNum);
      InitWeapon;

      // выбрать хоть какой-то тип
      if (ammoTypes.Count > 0) then
        weapItemsInMem[texNum].ammoTypeId := 0
      else
        weapItemsInMem[texNum].ammoTypeId := -1;

      filesCount := DM.OpenTextureDialog.Files.Count;

      weapItemsInMem[texNum].name := 'Weapon #' + IntToStr(texNum);
      weapItemsInMem[texNum].framesCount := filesCount;
      SetLength(weapItemsInMem[texNum].frames, filesCount);
                           
      stream := TMemoryStream.Create;
      for i := 0 to filesCount - 1 do
        begin
          weapItemsInMem[texNum].frames[i] := TBitmap.Create;

          PrepareLoadTexture(image, DM.OpenTextureDialog.Files[i], TEXTURE_MAX_WIDTH, TEXTURE_MAX_HEIGHT);

          // записываем изображение в поток
          stream.Position := 0;
          SaveImageToStream('bmp', stream, image);
          FreeImage(image);

          stream.Position := 0;
          weapItemsInMem[texNum].frames[i].LoadFromStream(stream);
          stream.Clear;
        end;     
      stream.Destroy;

      WeaponsGridUpdate;                
      GridSellectRow(WeaponsGrid, texNum);
      WeaponsGridSelectCell(WeaponsGrid, 0, texNum, canSelect);
    end;
                                         
  DM.OpenTextureDialog.Options := g_OpenOptionsOnce;
  frmPleaseWait.Destroy;
end;

procedure TfrmWeaponsAmmoEditor.MaxAmmoSpinChange(Sender: TObject);
begin
  if (g_WeaponsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
  if (not canInput) then exit;

  weapItemsInMem[texNum].maxAmmo := MaxAmmoSpin.Value;
end;

procedure TfrmWeaponsAmmoEditor.MoveDownBtnClick(Sender: TObject);
var
  weaponItemInMem : TWeaponItemInMem;
  weaponInMem1, weaponInMem2 : TWeaponInMem;
  canSelect : Boolean; 

  weaponId1, weaponId2 : TMapElement;
  m, x, y : Integer;
  element : TMapElement;
begin
  if (g_WeaponsCount <= 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount - 1)) then exit;

  // обмен подбирабельным оружием
  weaponItemInMem := weapItemsInMem[texNum + 1];
  weapItemsInMem[texNum + 1] := weapItemsInMem[texNum];
  weapItemsInMem[texNum] := weaponItemInMem;

  // обмен оружия в руках
  LoadWeapon(texNum + 1, @weaponInMem1);
  LoadWeapon(texNum,     @weaponInMem2);

  SaveWeapon(texNum,     @weaponInMem1);
  SaveWeapon(texNum + 1, @weaponInMem2);
                     
  InitWeapon(@weaponInMem1);
  InitWeapon(@weaponInMem2);  

  // теперь ищем во всех картах оружие и меняем номера обменяных  
  // отсчет начинаем с 1, т.к. 0 на карте = ничего нет
  weaponId1 := 1                  +
               TEXTURES_MAX_COUNT +
               SPRITES_MAX_COUNT  +
               DOORS_MAX_COUNT    +
               KEYS_MAX_COUNT     +
               texNum;
  weaponId2 := 1                  +
               TEXTURES_MAX_COUNT +
               SPRITES_MAX_COUNT  +
               DOORS_MAX_COUNT    +
               KEYS_MAX_COUNT     +
               texNum + 1;
  for m := 0 to g_MapsCount - 1 do
    for y := 0 to MAP_HEIGHT - 1 do
      for x := 0 to MAP_WIDTH - 1 do
        with (mapPack.maps[m]) do
          begin
            element := level[x, y];
            if (element = weaponId1) then
              level[x, y] := weaponId2
            else
              if (element = weaponId2) then
                level[x, y] := weaponId1;
          end;

  Inc(texNum);

  WeaponsGrid.ClearSelections;
  WeaponsGrid.Col := 0;
  WeaponsGrid.Row := texNum;
  WeaponsGridSelectCell(WeaponsGrid, 0, texNum, canSelect);
  WeaponsGrid.SetFocus;
end;

procedure TfrmWeaponsAmmoEditor.MoveUpBtnClick(Sender: TObject);
var
  weaponItemInMem : TWeaponItemInMem;
  weaponInMem1, weaponInMem2 : TWeaponInMem;
  canSelect : Boolean;

  weaponId1, weaponId2 : TMapElement;
  m, x, y : Integer;
  element : TMapElement;
begin
  if (g_WeaponsCount <= 0) then exit;
  if ((texNum < 1) or (texNum >= g_WeaponsCount)) then exit;

  // обмен подбирабельным оружием
  weaponItemInMem := weapItemsInMem[texNum - 1];
  weapItemsInMem[texNum - 1] := weapItemsInMem[texNum]; 
  weapItemsInMem[texNum] := weaponItemInMem;

  // обмен оружия в руках
  LoadWeapon(texNum - 1, @weaponInMem1);
  LoadWeapon(texNum,     @weaponInMem2);

  SaveWeapon(texNum,     @weaponInMem1); 
  SaveWeapon(texNum - 1, @weaponInMem2);

  InitWeapon(@weaponInMem1);
  InitWeapon(@weaponInMem2);
               
  // теперь ищем во всех картах оружие и меняем номера обменяных
  // отсчет начинаем с 1, т.к. 0 на карте = ничего нет
  weaponId1 := 1                  +
               TEXTURES_MAX_COUNT +
               SPRITES_MAX_COUNT  +
               DOORS_MAX_COUNT    +
               KEYS_MAX_COUNT     +
               texNum - 1;
  weaponId2 := 1                  +
               TEXTURES_MAX_COUNT +
               SPRITES_MAX_COUNT  +
               DOORS_MAX_COUNT    +
               KEYS_MAX_COUNT     +
               texNum;
  for m := 0 to g_MapsCount - 1 do
    for y := 0 to MAP_HEIGHT - 1 do
      for x := 0 to MAP_WIDTH - 1 do
        with (mapPack.maps[m]) do
          begin
            element := level[x, y];
            if (element = weaponId1) then
              level[x, y] := weaponId2
            else
              if (element = weaponId2) then
                level[x, y] := weaponId1;
          end;

  Dec(texNum);

  WeaponsGrid.ClearSelections;
  WeaponsGrid.Col := 0;
  WeaponsGrid.Row := texNum;
  WeaponsGridSelectCell(WeaponsGrid, 0, texNum, canSelect);
  WeaponsGrid.SetFocus;
end;

procedure TfrmWeaponsAmmoEditor.PageControl1Change(Sender: TObject);
begin
  FormPrepare;
end;

procedure TfrmWeaponsAmmoEditor.BulletPreviewImageClick(Sender: TObject);
var
  i : Integer;
  image : TImageData;
  bmp : TBitmap = nil;
  stream : TMemoryStream = nil;
  clr : TColor32Rec;             // цвет для удаления
  canSelect : Boolean;
begin
  if (g_BulletsCount = 0) then exit;
  if (not deleteColorMode) then exit;

  with (bulletsInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      stream := TMemoryStream.Create;
      frames[frmTexNum].SaveToStream(stream);

      stream.Position := 0;
      FreeImage(image);
      LoadImageFromStream(stream, image);
      stream.Clear;

      // берем цвет для удаления и удаляем его в памяти
      clr := (PColor32Rec(image.Bits) + image.Width * deleteColorPos.y + deleteColorPos.x)^;
      for i := 0 to (image.Size div SizeOf(clr)) - 1 do
        begin
          if ((PColor32Rec(image.Bits) + i)^.Color = clr.Color) then
            (PColor32Rec(image.Bits) + i)^.A := 0;
        end;

      // сохраняем картинку из памяти в превью
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image);
      FreeImage(image);

      stream.Position := 0;
      bmp := TBitmap.Create;
      bmp.LoadFromStream(stream, stream.Size);
      stream.Destroy;

      LoadPreviewImageFromBitmap(BulletPreviewImage, bmp);
      bmp.Destroy;

      // Если принять изменения, то сохранить фрейм из превью
      if (MessageDlg('Confirm', 'Do you want to save the current changes?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
        begin
          stream := TMemoryStream.Create;
          BulletPreviewImage.Picture.SaveToStream(stream);

          stream.Position := 0;
          frames[frmTexNum].Clear;
          frames[frmTexNum].LoadFromStream(stream);

          stream.Destroy;
        end;
    end;

  BulletFramesGridUpdate;
  BulletFramesGridSelectCell(BulletFramesGrid, 0, frmTexNum, canSelect);
end;

procedure TfrmWeaponsAmmoEditor.BulletPreviewImageMouseEnter(Sender: TObject);
begin
  if (deleteColorMode) then
    begin
      BulletDeleteColorShp.Visible := True;
      Label12.Visible := True;
    end;
end;

procedure TfrmWeaponsAmmoEditor.BulletPreviewImageMouseLeave(Sender: TObject);
begin
  BulletDeleteColorShp.Visible := False;
  Label12.Visible := False; 
  canChooseSolid  := False;
  BulletPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.BulletPreviewImageMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  _x, _y : Integer;
  coeffX, coeffY : Single;    // поправочные коэффициенты для взятия пикселя (в превью может быть растянуто)
  clr : TColor;
begin
  // высчитываем координаты пикселя
  if (not BulletStretchImageCheck.Checked) then
    begin
      _x := X;
      _y := Y;
    end
  else
    begin
      coeffX := BulletPreviewImage.Picture.Width  / BulletPreviewImage.Width;
      coeffY := BulletPreviewImage.Picture.Height / BulletPreviewImage.Height;
      _x := round(X * coeffX);
      _y := round(Y * coeffY);
    end;
  if (_x > BulletPreviewImage.Picture.Width ) then _x := BulletPreviewImage.Picture.Width;
  if (_y > BulletPreviewImage.Picture.Height) then _y := BulletPreviewImage.Picture.Height;

  if (deleteColorMode) then
    begin
      BulletDeleteColorShp.Brush.Color := clDefault;

      clr := (PInt32(BulletPreviewImage.Picture.Bitmap.RawImage.Data) + BulletPreviewImage.Picture.Width * deleteColorPos.y + deleteColorPos.x)^;
      if ((clr shr 24) <> 0) then
        BulletDeleteColorShp.Brush.Color := BulletPreviewImage.Picture.Bitmap.Canvas.Pixels[_x, _y];;

      deleteColorPos.x := _x;
      deleteColorPos.y := _y;
    end
  else
    if (canChooseSolid) then
      begin
        if (_x > (BulletPreviewImage.Picture.Width shr 1)) then
          _x := BulletPreviewImage.Picture.Width - _x;
        if (_y > (BulletPreviewImage.Picture.Height shr 1)) then
          _y := BulletPreviewImage.Picture.Height - _y;

        BulletSolidXSpin.Value := ((BulletPreviewImage.Picture.Width  - _x shl 1) / BulletPreviewImage.Picture.Width ) * 100.0;
        BulletSolidYSpin.Value := ((BulletPreviewImage.Picture.Height - _y shl 1) / BulletPreviewImage.Picture.Height) * 100.0;

        BulletPreviewImage.Invalidate;
      end;
end;

procedure TfrmWeaponsAmmoEditor.BulletPreviewImagePaint(Sender: TObject);
var
  x0, x1, y0, y1 : Integer;
  xCoeff, yCoeff : Single;    // соотношение реального размера к растянутому в превью
  w, h : Integer;
  sW, sH : Integer;
begin
  if (g_BulletsCount = 0) then exit;
  with (bulletsInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[0] = nil)) then exit;

      w := BulletPreviewImage.Picture.Width;
      h := BulletPreviewImage.Picture.Height;

      sW := ceil(w * (solidX / 100.0));
      sH := ceil(h * (solidY / 100.0));

      x0 := (w - sW) shr 1;
      x1 := w - x0;
      //y0 := h - sH;
      //y1 := h;     
      y0 := (h - sH) shr 1;
      y1 := h - y0;

      if (BulletStretchImageCheck.Checked) then
        begin
          xCoeff := BulletPreviewImage.Width  / w;
          yCoeff := BulletPreviewImage.Height / h;

          x0 := ceil(x0 * xCoeff);
          x1 := ceil(x1 * xCoeff);
          y0 := ceil(y0 * yCoeff);
          y1 := ceil(y1 * yCoeff);
        end;

      if (x0 < 0) then x0 := 0;
      if (y0 < 0) then y0 := 0;
      if (x1 >= BulletPreviewImage.Width ) then x1 := BulletPreviewImage.Width  - 1;
      if (y1 >= BulletPreviewImage.Height) then y1 := BulletPreviewImage.Height - 1;

      with (BulletPreviewImage.Canvas) do
        begin
          Pen.Color := clRed;
          Line(x0, y0, x1, y0);
          Line(x1, y0, x1, y1);
          Line(x0, y1, x1, y1);
          Line(x0, y0, x0, y1); 
          // перекрестие
          if (canChooseSolid) then
            begin
              Line(x0, y0, x1, y1);
              Line(x1, y0, x0, y1);
            end;

          Pen.Color   := clLime;
          Brush.Color := clLime;
          Rectangle(x0 - 3, y0 - 3, x0 + 3, y0 + 3);
          Rectangle(x0 - 3, y1 - 3, x0 + 3, y1 + 3);
          Rectangle(x1 - 3, y0 - 3, x1 + 3, y0 + 3);
          Rectangle(x1 - 3, y1 - 3, x1 + 3, y1 + 3); 

          // рамка рисунка
          if (not BulletStretchImageCheck.Checked) then
            begin
              Pen.Color := clGray;
              Line(0, 0, w, 0);
              Line(w, 0, w, h);
              Line(0, h, w, h);
              Line(0, 0, 0, h);
            end;
        end;
    end;
end;

procedure TfrmWeaponsAmmoEditor.WeaponPreviewImageClick(Sender: TObject);
var
  i : Integer;
  image : TImageData;
  bmp : TBitmap = nil;
  stream : TMemoryStream = nil;
  clr : TColor32Rec;             // цвет для удаления
  canSelect : Boolean;
begin
  if (g_WeaponsCount = 0) then exit;
  if (not deleteColorMode) then exit;

  with (weapItemsInMem[texNum]) do
    begin
      if ((framesCount = 0) or (frames[frmTexNum] = nil)) then exit;

      stream := TMemoryStream.Create;
      frames[frmTexNum].SaveToStream(stream);

      stream.Position := 0;
      FreeImage(image);
      LoadImageFromStream(stream, image);
      stream.Clear;

      // берем цвет для удаления и удаляем его в памяти
      clr := (PColor32Rec(image.Bits) + image.Width * deleteColorPos.y + deleteColorPos.x)^;
      for i := 0 to (image.Size div SizeOf(clr)) - 1 do
        begin
          if ((PColor32Rec(image.Bits) + i)^.Color = clr.Color) then
            (PColor32Rec(image.Bits) + i)^.A := 0;
        end;

      // сохраняем картинку из памяти в превью
      stream.Position := 0;
      SaveImageToStream('bmp', stream, image);
      FreeImage(image);

      stream.Position := 0;
      bmp := TBitmap.Create;
      bmp.LoadFromStream(stream, stream.Size);
      stream.Destroy;

      LoadPreviewImageFromBitmap(WeaponPreviewImage, bmp);
      bmp.Destroy;

      // Если принять изменения, то сохранить фрейм из превью
      if (MessageDlg('Confirm', 'Do you want to save the current changes?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
        begin
          stream := TMemoryStream.Create;
          WeaponPreviewImage.Picture.SaveToStream(stream);

          stream.Position := 0;
          frames[frmTexNum].Clear;
          frames[frmTexNum].LoadFromStream(stream);

          stream.Destroy;
        end;
    end;

  WeaponFramesGridUpdate;
  WeaponFramesGridSelectCell(WeaponFramesGrid, 0, frmTexNum, canSelect);
end;

procedure TfrmWeaponsAmmoEditor.WeaponPreviewImageMouseEnter(Sender: TObject);
begin
  if (deleteColorMode) then
    begin
      WeaponDeleteColorShp.Visible := True;
      Label1.Visible := True;
    end;
end;

procedure TfrmWeaponsAmmoEditor.WeaponPreviewImageMouseLeave(Sender: TObject);
begin
  WeaponDeleteColorShp.Visible := False;
  Label1.Visible := False; 
  canChooseSolid  := False;
  WeaponPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.WeaponPreviewImageMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  _x, _y : Integer;
  coeffX, coeffY : Single;    // поправочные коэффициенты для взятия пикселя (в превью может быть растянуто)
  clr : TColor;
begin
  // высчитываем координаты пикселя
  if (not WeaponStretchImageCheck.Checked) then
    begin
      _x := X;
      _y := Y;
    end
  else
    begin
      coeffX := WeaponPreviewImage.Picture.Width  / WeaponPreviewImage.Width;
      coeffY := WeaponPreviewImage.Picture.Height / WeaponPreviewImage.Height;
      _x := round(X * coeffX);
      _y := round(Y * coeffY);
    end;
  if (_x > WeaponPreviewImage.Picture.Width ) then _x := WeaponPreviewImage.Picture.Width;
  if (_y > WeaponPreviewImage.Picture.Height) then _y := WeaponPreviewImage.Picture.Height;

  if (deleteColorMode) then
    begin
      WeaponDeleteColorShp.Brush.Color := clDefault;

      clr := (PInt32(WeaponPreviewImage.Picture.Bitmap.RawImage.Data) + WeaponPreviewImage.Picture.Width * deleteColorPos.y + deleteColorPos.x)^;
      if ((clr shr 24) <> 0) then
        WeaponDeleteColorShp.Brush.Color := WeaponPreviewImage.Picture.Bitmap.Canvas.Pixels[_x, _y];

      deleteColorPos.x := _x;
      deleteColorPos.y := _y;
    end
  else
    if (canChooseSolid) then
      begin
        if (_x > (WeaponPreviewImage.Picture.Width shr 1)) then
          _x := WeaponPreviewImage.Picture.Width - _x;

        WeaponSolidXSpin.Value := ((WeaponPreviewImage.Picture.Width  - _x shl 1) / WeaponPreviewImage.Picture.Width ) * 100.0;
        WeaponSolidYSpin.Value := ((WeaponPreviewImage.Picture.Height - _y      ) / WeaponPreviewImage.Picture.Height) * 100.0;

        WeaponPreviewImage.Invalidate;
      end;
end;

procedure TfrmWeaponsAmmoEditor.BulletCheckChange(Sender: TObject);
begin
  if ((BulletCheck.Checked) and (g_BulletsCount > 0)) then
    begin
      if (g_WeaponsCount = 0) then exit;
      if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
      if (not canInput) then exit;

      RaycastingCheck.Checked := False;
      BulletCmbBox.Enabled    := True;
      BulletCmbBox.ItemIndex  := 0;
      weapItemsInMem[texNum].fireType := 1;
      weapItemsInMem[texNum].bulletId := 0;
    end
  else
    begin
      BulletCheck.Checked     := False;
      RaycastingCheck.Checked := True;
    end;
end;

procedure TfrmWeaponsAmmoEditor.BulletCmbBoxChange(Sender: TObject);
begin
  if (g_WeaponsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
  if (not canInput) then exit;

  weapItemsInMem[texNum].bulletId := BulletCmbBox.ItemIndex;
end;

procedure TfrmWeaponsAmmoEditor.BulletSolidXSpinChange(Sender: TObject);
begin
  if (g_BulletsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_BulletsCount)) then exit;
  if (not canInput) then exit;

  bulletsInMem[texNum].solidX := BulletSolidXSpin.Value;
  BulletPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.BulletSolidYSpinChange(Sender: TObject);
begin
  if (g_BulletsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_BulletsCount)) then exit;
  if (not canInput) then exit;

  bulletsInMem[texNum].solidY := BulletSolidYSpin.Value;
  BulletPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.BulletStretchImageCheckChange(Sender: TObject);
begin
  BulletPreviewImage.Stretch := BulletStretchImageCheck.Checked;
end;

procedure TfrmWeaponsAmmoEditor.WeaponsGridDrawCell(Sender: TObject; aCol,
  aRow: Integer; aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_WeaponsCount = 0) then exit;
  //if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (weapItemsInMem[pos].framesCount = 0) then exit;
  bmp := weapItemsInMem[pos].frames[0];

  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmWeaponsAmmoEditor.WeaponsGridSelectCell(Sender: TObject; aCol,
  aRow: Integer; var CanSelect: Boolean);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  FormPrepare;

  if (g_WeaponsCount = 0) then exit;

  WeaponLoadTextureBtn.Enabled   := True;
  WeaponClearTextureBtn.Enabled  := True;
  WeaponDeleteTextureBtn.Enabled := True;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  texNum := pos;
  frmTexNum := 0;
  if (weapItemsInMem[pos].framesCount = 0) then exit;
  bmp := weapItemsInMem[pos].frames[0];
  if (bmp = nil) then exit;

  LoadPreviewImageFromBitmap(WeaponPreviewImage, bmp);
  WeaponParamsGroupBox.Enabled := True;
  WeaponFramesGroupBox.Enabled := True;  
  WeaponDeleteColorBtn.Enabled := True;

  with (weapItemsInMem[pos]) do
    begin
      WeaponNameEdit.Text       := name;
      DamageSpin.Value          := damage;
      DistanceSpin.Value        := distance;
      AccuracySpin.Value        := accuracy;   
      CurAmmoSpin.Value         := curAmmo;         
      CurAmmoSpin.Enabled       := not infiniteAmmo;
      MaxAmmoSpin.Value         := maxAmmo;
      MaxAmmoSpin.Enabled       := not infiniteAmmo;
      WeaponPickTypeCmbBox.ItemIndex := ammoTypeId;   
      WeaponPickTypeCmbBox.Enabled   := not infiniteAmmo;
      InfiniteChk.Checked       := infiniteAmmo;
      RaycastingCheck.Checked   := (fireType = 0);
      BulletCheck.Checked       := (fireType = 1);
      BulletCmbBox.Enabled      := (fireType = 1);
      BulletCmbBox.ItemIndex    := bulletId;
      WeaponPickSoundsCmbBox.ItemIndex := pickUpSoundId;
      WeaponSolidXSpin.Value    := solidX;  
      WeaponSolidYSpin.Value    := solidY;
      WeaponAnimSpeedSpin.Value := animSpeed;
    end;

  canInput := True;
  WeaponFramesGridUpdate;
  //WeaponsGridSelection(Sender, aCol, aRow);
end;

procedure TfrmWeaponsAmmoEditor.WeaponsGridSelection(Sender: TObject; aCol,
  aRow: Integer);
begin
  WeaponsGrid.Options := [goAlwaysShowEditor, goSmoothScroll, goDrawFocusSelected];
  BulletsGrid.Options := [goAlwaysShowEditor, goSmoothScroll];
  AmmoGrid.Options    := [goAlwaysShowEditor, goSmoothScroll];

  WeaponsGrid.SetFocus;
end;

procedure TfrmWeaponsAmmoEditor.WeaponSolidXSpinChange(Sender: TObject);
begin
  if (g_WeaponsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
  if (not canInput) then exit;

  weapItemsInMem[texNum].solidX := WeaponSolidXSpin.Value;
  WeaponPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.WeaponSolidYSpinChange(Sender: TObject);
begin
  if (g_WeaponsCount = 0) then exit;
  if ((texNum < 0) or (texNum >= g_WeaponsCount)) then exit;
  if (not canInput) then exit;

  weapItemsInMem[texNum].solidY := WeaponSolidYSpin.Value;
  WeaponPreviewImage.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.FormPrepare;
var
  i : Integer;
begin     
  canInput := False;
  AnimatorDisable;
  //DeleteColorModeDisable;

  // оружие
  WeaponPreviewImage.Picture.Clear;
  WeaponPreviewImage.Invalidate;

  WeaponParamsGroupBox.Enabled := False;
  WeaponFramesGroupBox.Enabled := False;
  WeaponFramesGrid.ColCount := 0;
  WeaponFramesGrid.RowCount := 0;

  WeaponLoadFrameBtn.Enabled   := False;
  WeaponClearFrameBtn.Enabled  := False;
  WeaponDeleteFrameBtn.Enabled := False;

  WeaponLoadTextureBtn.Enabled   := False;
  WeaponClearTextureBtn.Enabled  := False;
  WeaponDeleteTextureBtn.Enabled := False;

  // пули
  BulletPreviewImage.Picture.Clear;
  BulletPreviewImage.Invalidate;

  BulletParamsGroupBox.Enabled := False;
  BulletFramesGroupBox.Enabled := False;
  BulletFramesGrid.ColCount := 0;
  BulletFramesGrid.RowCount := 0;

  BulletLoadFrameBtn.Enabled   := False;
  BulletClearFrameBtn.Enabled  := False;
  BulletDeleteFrameBtn.Enabled := False;

  BulletLoadTextureBtn.Enabled   := False;
  BulletClearTextureBtn.Enabled  := False;
  BulletDeleteTextureBtn.Enabled := False;

  RaycastingCheck.Checked := False;
  BulletCheck.Checked     := False;
  BulletCmbBox.Enabled    := False;
  BulletCmbBox.ItemIndex  := -1;

  // патроны    
  AmmoPreviewImage.Picture.Clear;
  AmmoPreviewImage.Invalidate;

  AmmoParamsGroupBox.Enabled := False;
  AmmoFramesGroupBox.Enabled := False;
  AmmoFramesGrid.ColCount := 0;
  AmmoFramesGrid.RowCount := 0;

  AmmoLoadFrameBtn.Enabled   := False;
  AmmoClearFrameBtn.Enabled  := False;
  AmmoDeleteFrameBtn.Enabled := False;

  AmmoLoadTextureBtn.Enabled   := False;
  AmmoClearTextureBtn.Enabled  := False;
  AmmoDeleteTextureBtn.Enabled := False;

  AmmoPickSoundsCmbBox.ItemIndex := -1;
  AmmoPickTypeCmbBox.ItemIndex   := -1;

  texNum := -1;
  frmTexNum := -1;

  WeaponNameEdit.Text   := '<unknown>';
  BulletNameEdit.Text   := '<unknown>';
  AmmoNameEdit.Text     := '<unknown>';
  AmmoTypeNameEdit.Text := '<unknown>';

  WeaponsGrid.Options := [goAlwaysShowEditor, goSmoothScroll];
  BulletsGrid.Options := [goAlwaysShowEditor, goSmoothScroll]; 
  AmmoGrid.Options    := [goAlwaysShowEditor, goSmoothScroll];

  BulletCmbBoxFill;

  with (WeaponPickSoundsCmbBox) do
    begin
      Items.Clear;
      for i := 0 to g_SoundsCount - 1 do
        Items.Add(soundsInMem[i].name);
      ItemIndex := -1;
    end;  
  with (WeaponPickTypeCmbBox) do
    begin
      Items.Clear;
      for i := 0 to ammoTypes.Count - 1 do
        Items.Add(ammoTypes[i]);
      ItemIndex := -1;
    end;
        
  with (AmmoPickSoundsCmbBox) do
    begin
      Items.Clear;
      for i := 0 to g_SoundsCount - 1 do
        Items.Add(soundsInMem[i].name);
      ItemIndex := -1;
    end;
  with (AmmoPickTypeCmbBox) do
    begin
      Items.Clear;
      for i := 0 to ammoTypes.Count - 1 do
        Items.Add(ammoTypes[i]);
      ItemIndex := -1;
    end;

  with (AmmoTypesList) do
    begin
      Clear;     
      for i := 0 to ammoTypes.Count - 1 do
        Items.Add(ammoTypes[i]);
    end;
end;

procedure TfrmWeaponsAmmoEditor.AnimatorDisable;
begin
  DM.ImageList.GetRawImage(0, rawImage);
  WeaponFramePlayBtn.Glyph.Clear;    
  BulletFramePlayBtn.Glyph.Clear;
  AmmoFramePlayBtn.Glyph.Clear;
  WeaponFramePlayBtn.Glyph.LoadFromRawImage(rawImage, False);  
  BulletFramePlayBtn.Glyph.LoadFromRawImage(rawImage, False);
  AmmoFramePlayBtn.Glyph.LoadFromRawImage(rawImage, False);

  Animator.Enabled := False;
  WeaponFramePlayBtn.Caption := 'Play';  
  BulletFramePlayBtn.Caption := 'Play';
  AmmoFramePlayBtn.Caption   := 'Play';
end;

procedure TfrmWeaponsAmmoEditor.AnimatorEnable;
begin
  DM.ImageList.GetRawImage(1, rawImage);
  WeaponFramePlayBtn.Glyph.Clear;          
  BulletFramePlayBtn.Glyph.Clear;
  AmmoFramePlayBtn.Glyph.Clear;
  WeaponFramePlayBtn.Glyph.LoadFromRawImage(rawImage, false);   
  BulletFramePlayBtn.Glyph.LoadFromRawImage(rawImage, false);
  AmmoFramePlayBtn.Glyph.LoadFromRawImage(rawImage, false);

  Animator.Interval := 0;
  case (PageControl1.ActivePageIndex) of
    // оружие
    0 : if (WeaponAnimSpeedSpin.Value <> 0) then
          Animator.Interval := 1000 div WeaponAnimSpeedSpin.Value;
    // пули
    1 : if (BulletAnimSpeedSpin.Value <> 0) then
          Animator.Interval := 1000 div BulletAnimSpeedSpin.Value;
    // припасы
    2 : if (AmmoAnimSpeedSpin.Value <> 0) then
          Animator.Interval := 1000 div AmmoAnimSpeedSpin.Value;
  end;

  Animator.Enabled  := True;
  WeaponFramePlayBtn.Caption := 'Stop';   
  BulletFramePlayBtn.Caption := 'Stop';
  AmmoFramePlayBtn.Caption   := 'Stop';
end;

procedure TfrmWeaponsAmmoEditor.DeleteColorModeEnable;
begin
  AnimatorDisable;

  case (PageControl1.ActivePageIndex) of
    // оружие
    0 : begin
          if ((texNum < 0) or (texNum >= Length(weapItemsInMem))) then exit;
          if (weapItemsInMem[texNum].framesCount = 0) then exit;
          if (weapItemsInMem[texNum].frames[frmTexNum] = nil) then exit;
        end;     
    // пули
    1 : begin
          if ((texNum < 0) or (texNum >= Length(bulletsInMem))) then exit;
          if (bulletsInMem[texNum].framesCount = 0) then exit;
          if (bulletsInMem[texNum].frames[frmTexNum] = nil) then exit;
        end;
    // аммо
    2 : begin
          if ((texNum < 0) or (texNum >= Length(ammoInMem))) then exit;
          if (ammoInMem[texNum].framesCount = 0) then exit;
          if (ammoInMem[texNum].frames[frmTexNum] = nil) then exit;
        end;
  end;

  DM.ImageList.GetRawImage(5, rawImage);
  WeaponDeleteColorBtn.Glyph.Clear;
  WeaponDeleteColorBtn.Glyph.LoadFromRawImage(rawImage, False);
  BulletDeleteColorBtn.Glyph.Clear;
  BulletDeleteColorBtn.Glyph.LoadFromRawImage(rawImage, False);
  AmmoDeleteColorBtn.Glyph.Clear;
  AmmoDeleteColorBtn.Glyph.LoadFromRawImage(rawImage, False);

  WeaponPreviewImage.Cursor := crCross;
  BulletPreviewImage.Cursor := crCross;
  AmmoPreviewImage.Cursor   := crCross;

  WeaponsGroupBox.Enabled      := False;
  WeaponsEditGroupBox.Enabled  := False;
  WeaponFramesGroupBox.Enabled := False;
  WeaponParamsGroupBox.Enabled := False;  

  BulletsGroupBox.Enabled      := False;
  BulletsEditGroupBox.Enabled  := False;
  BulletFramesGroupBox.Enabled := False;
  BulletParamsGroupBox.Enabled := False;

  AmmoGroupBox.Enabled       := False;
  AmmoEditGroupBox.Enabled   := False;
  AmmoFramesGroupBox.Enabled := False;
  AmmoParamsGroupBox.Enabled := False;

  deleteColorMode := True;
  canChooseSolid  := False;
end;

procedure TfrmWeaponsAmmoEditor.DeleteColorModeDisable;
begin
  DM.ImageList.GetRawImage(4, rawImage);
  WeaponDeleteColorBtn.Glyph.Clear;
  WeaponDeleteColorBtn.Glyph.LoadFromRawImage(rawImage, False);
  BulletDeleteColorBtn.Glyph.Clear;
  BulletDeleteColorBtn.Glyph.LoadFromRawImage(rawImage, False);
  AmmoDeleteColorBtn.Glyph.Clear;
  AmmoDeleteColorBtn.Glyph.LoadFromRawImage(rawImage, False);
  WeaponPreviewImage.Cursor := crDefault;
  BulletPreviewImage.Cursor := crDefault;
  AmmoPreviewImage.Cursor   := crDefault;
                                    
  WeaponsGroupBox.Enabled := True;
  if (g_WeaponsCount > 0) then
    begin
      WeaponsEditGroupBox.Enabled  := True;
      WeaponFramesGroupBox.Enabled := True;
      WeaponParamsGroupBox.Enabled := True;
    end;
                                 
  BulletsGroupBox.Enabled := True;
  if (g_BulletsCount > 0) then
    begin
      BulletsEditGroupBox.Enabled  := True;
      BulletFramesGroupBox.Enabled := True;
      BulletParamsGroupBox.Enabled := True;
    end;

  AmmoGroupBox.Enabled := True;
  if (g_AmmoCount > 0) then
    begin
      AmmoEditGroupBox.Enabled   := True;
      AmmoFramesGroupBox.Enabled := True;
      AmmoParamsGroupBox.Enabled := True;
    end;

  deleteColorMode := False;   
  canChooseSolid  := False;
end;

procedure TfrmWeaponsAmmoEditor.WeaponsGridUpdate;
var
  i : Integer;
begin
  if (g_WeaponsCount = 0) then
    begin
      WeaponsGrid.ColCount := 0;
      WeaponsGrid.RowCount := 0;
      exit;
    end;

  // формируем грид со спрайтами
  WeaponsGrid.ColCount := 1;
  WeaponsGrid.RowCount := g_WeaponsCount;

  for i := 0 to WeaponsGrid.ColCount - 1 do
    WeaponsGrid.ColWidths [i] := 96;
  for i := 0 to WeaponsGrid.RowCount - 1 do
    WeaponsGrid.RowHeights[i] := 96;
  WeaponsGrid.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.WeaponFramesGridUpdate;
var
  i : Integer;
begin
  if (weapItemsInMem[texNum].framesCount = 0) then
    begin
      WeaponFramesGrid.ColCount := 0;
      WeaponFramesGrid.RowCount := 0;
      exit;
    end;

  // формируем грид с текстурами
  WeaponFramesGrid.ColCount := 1;
  WeaponFramesGrid.RowCount := weapItemsInMem[texNum].framesCount;

  for i := 0 to WeaponFramesGrid.ColCount - 1 do
    WeaponFramesGrid.ColWidths [i] := 48;
  for i := 0 to WeaponFramesGrid.RowCount - 1 do
    WeaponFramesGrid.RowHeights[i] := 48;
  WeaponFramesGrid.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.BulletsGridUpdate;
var
  i : Integer;
begin
  if (g_BulletsCount = 0) then
    begin
      BulletsGrid.ColCount := 0;
      BulletsGrid.RowCount := 0;
      exit;
    end;

  // формируем грид со спрайтами
  BulletsGrid.ColCount := 1;
  BulletsGrid.RowCount := g_BulletsCount;

  for i := 0 to BulletsGrid.ColCount - 1 do
    BulletsGrid.ColWidths [i] := 96;
  for i := 0 to BulletsGrid.RowCount - 1 do
    BulletsGrid.RowHeights[i] := 96;
  BulletsGrid.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.BulletFramesGridUpdate;
var
  i : Integer;
begin
  if (bulletsInMem[texNum].framesCount = 0) then
    begin
      BulletFramesGrid.ColCount := 0;
      BulletFramesGrid.RowCount := 0;
      exit;
    end;

  // формируем грид с текстурами
  BulletFramesGrid.ColCount := 1;
  BulletFramesGrid.RowCount := bulletsInMem[texNum].framesCount;

  for i := 0 to BulletFramesGrid.ColCount - 1 do
    BulletFramesGrid.ColWidths [i] := 48;
  for i := 0 to BulletFramesGrid.RowCount - 1 do
    BulletFramesGrid.RowHeights[i] := 48;
  BulletFramesGrid.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.BulletCmbBoxFill;
var
  i : Integer;
begin
  BulletCmbBox.Clear;
  for i := 0 to g_BulletsCount - 1 do
    BulletCmbBox.Items.Add(bulletsInMem[i].name);
end;

procedure TfrmWeaponsAmmoEditor.AmmoGridUpdate;
var
  i : Integer;
begin
  if (g_AmmoCount = 0) then
    begin
      AmmoGrid.ColCount := 0;
      AmmoGrid.RowCount := 0;
      exit;
    end;

  // формируем грид со спрайтами
  AmmoGrid.ColCount := 1;
  AmmoGrid.RowCount := g_AmmoCount;

  for i := 0 to AmmoGrid.ColCount - 1 do
    AmmoGrid.ColWidths [i] := 96;
  for i := 0 to AmmoGrid.RowCount - 1 do
    AmmoGrid.RowHeights[i] := 96;
  AmmoGrid.Invalidate;
end;

procedure TfrmWeaponsAmmoEditor.AmmoFramesGridUpdate;
var
  i : Integer;
begin
  if (ammoInMem[texNum].framesCount = 0) then
    begin
      AmmoFramesGrid.ColCount := 0;
      AmmoFramesGrid.RowCount := 0;
      exit;
    end;

  // формируем грид с текстурами
  AmmoFramesGrid.ColCount := 1;
  AmmoFramesGrid.RowCount := ammoInMem[texNum].framesCount;

  for i := 0 to AmmoFramesGrid.ColCount - 1 do
    AmmoFramesGrid.ColWidths [i] := 48;
  for i := 0 to AmmoFramesGrid.RowCount - 1 do
    AmmoFramesGrid.RowHeights[i] := 48;
  AmmoFramesGrid.Invalidate;
end;

end.

