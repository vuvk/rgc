unit uResManager;

interface

const
  RES_MANAGER_LIB = 'res_manager.dll';

  RES_TRUE  = 1;
  RES_FALSE = 0;
  RES_ERROR = -1;
  RES_NOT_FOUND  = -2;
  RES_NOT_OPENED = -3;
  RES_NOT_READABLE = -4;
  RES_NOT_WRITABLE = -5;


function Res_OpenZipForRead(const zipName : PChar) : Int32; cdecl; external RES_MANAGER_LIB;
function Res_GetCompressedFileSize(const fileName : PChar) : Int32; cdecl; external RES_MANAGER_LIB;
function Res_GetUncompressedFileSize(const fileName : PChar) : Int32; cdecl; external RES_MANAGER_LIB;
//function Res_ReadFileFromZipToBuffer(const fileName : PChar; var buffer : Pointer; var bufferSize : Int32) : Boolean; cdecl; external RES_MANAGER_LIB;   
function Res_ReadFileFromZipToBuffer(const fileName : PChar; buffer : Pointer; bufferSize : Int32) : Int32; cdecl; external RES_MANAGER_LIB;
function Res_CloseZipAfterRead() : Boolean; cdecl; external RES_MANAGER_LIB;
function Res_OpenZipForWrite(const zipName : PChar; isAppend : Boolean = False) : Boolean; cdecl; external RES_MANAGER_LIB;
function Res_WriteFileFromBufferToZip(fileName : PChar; const buffer : Pointer; bufferSize : UInt32; compressionLevel : Int32) : Int32; cdecl; external RES_MANAGER_LIB;
function Res_CloseZipAfterWrite() : Boolean; cdecl; external RES_MANAGER_LIB;
function Res_PackAllFilesFromDirToZip(path : PChar;{ zipName : PChar;} compressionLevel : Int32) : Boolean; cdecl; external RES_MANAGER_LIB;
function Res_UnpackAllFilesFromZipToDir(zipName : PChar; path : PChar; rewrite : Boolean) : Boolean; cdecl; external RES_MANAGER_LIB;    
function Res_ReadFileToBuffer(var buffer : Pointer; var bufferSize : Integer; fileName : PChar) : Boolean; cdecl; external RES_MANAGER_LIB;
function Res_WriteBufferToFile(buffer : Pointer; bufferSize : Integer; fileName : PChar; rewrite : Boolean) : Boolean; cdecl; external RES_MANAGER_LIB;
procedure Res_DeleteAllFilesInDir(dirName : PChar; deleteDir: Boolean); cdecl; external RES_MANAGER_LIB;
                 
function Res_OpenIniFromFile(const fileName : PChar) : Boolean; cdecl; external RES_MANAGER_LIB;
function Res_OpenIniFromBuffer(buffer : Pointer; bufferSize : UInt32) : Boolean; cdecl; external RES_MANAGER_LIB;
function Res_IniReadString(const section, key, defaultValue : PChar) : PChar; cdecl; external RES_MANAGER_LIB;
function Res_IniReadInteger(const section, key : PChar; defaultValue : Int32) : Int32; cdecl; external RES_MANAGER_LIB;
function Res_IniReadInteger64(const section, key : PChar; defaultValue : Int64) : Int64; cdecl; external RES_MANAGER_LIB;
function Res_IniReadBool(const section, key : PChar; defaultValue : Boolean) : Boolean; cdecl; external RES_MANAGER_LIB;
function Res_IniReadDouble(const section, key : PChar; defaultValue : Double) : Double; cdecl; external RES_MANAGER_LIB;
procedure Res_IniWriteString(const section, key, value : PChar); cdecl; external RES_MANAGER_LIB;
procedure Res_IniWriteInteger(const section, key : PChar; value : Int32); cdecl; external RES_MANAGER_LIB;
procedure Res_IniWriteInteger64(const section, key : PChar; value : Int64); cdecl; external RES_MANAGER_LIB;
procedure Res_IniWriteBool(const section, key : PChar; value : Boolean); cdecl; external RES_MANAGER_LIB;
procedure Res_IniWriteDouble(const section, key : PChar; value : Double); cdecl; external RES_MANAGER_LIB;
function Res_SaveIniToBuffer(var buffer : Pointer; var bufferSize : Int32) : Boolean; cdecl; external RES_MANAGER_LIB;
function Res_SaveIniToFile(const fileName : PChar) : Boolean; cdecl; external RES_MANAGER_LIB;
procedure Res_CloseIni(); cdecl; external RES_MANAGER_LIB;

implementation

end.

