unit uSoundsEditor;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  FileUtil,
  Forms,
  Controls,
  Graphics,
  GraphType,
  Dialogs,
  StdCtrls,
  Grids,
  ExtCtrls,
  Buttons,
  LCLType,
  strutils,
  sdl2,
  sdl2_mixer,
  uPleaseWait;

type

  { TfrmSoundsEditor }

  TfrmSoundsEditor = class(TForm)
    AddSoundBtn: TSpeedButton;
    PrecachedChk: TCheckBox;
    DeleteSoundBtn: TSpeedButton;
    frm_play_btn: TBitBtn;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    NameEdit: TEdit;
    SoundsList: TListBox;
    ParametersGroupBox: TGroupBox;
    SoundsEditGroupBox: TGroupBox;
    Timer1: TTimer;
    procedure AddSoundBtnClick(Sender: TObject);
    procedure DeleteSoundBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure frm_play_btnClick(Sender: TObject);
    procedure NameEditChange(Sender: TObject);
    procedure NameEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState
      );
    procedure PrecachedChkChange(Sender: TObject);
    procedure SoundsListClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    sndNum   : Integer;
    canInput : Boolean;         // можно ли вводить значения
    sdlInitialized : Boolean;   // запущен ли sdl2 ?    
    sound : PMix_Chunk;
    music : PMix_Music;
    rawImage : TRawImage;
    isSoundPlaying : Boolean;

    procedure InitForm;
    procedure PlaySound;
    procedure StopSound;
    procedure FillSoundsList;
    procedure CheckDeleteSounds(sndName : String);   // проверить использование звука по имени и удалить зависимость
  public

  end;

//var
//  frmSoundsEditor: TfrmSoundsEditor;

var
  audioExtensions : array[0..0] of String = ('.wav');
  musicExtensions : array[0..8] of String = ('.ogg', '.flac', '.mp3', '.mid', '.midi', '.xm', '.s3m', '.mptm', '.it');

implementation

uses
  uContainer,
  uDM;

{$R *.lfm}

{ TfrmSoundsEditor }

procedure TfrmSoundsEditor.AddSoundBtnClick(Sender: TObject);
var
  destFileName : String;
  i : Integer;
  frmPleaseWait : TfrmPleaseWait;
begin
  StopSound;

  frmPleaseWait := TfrmPleaseWait.Create(Self);

  DM.OpenSoundDialog.Options := g_OpenOptionsMulti;   
  canInput := True;
                           
  with (DM.OpenSoundDialog) do
    { Выбираем файл }
    if (Execute and (Files.Count > 0)) then
      begin 
        frmPleaseWait.Show;
        frmPleaseWait.Repaint;

        for i := 0 to Files.Count - 1 do
          if (g_SoundsCount < SOUNDS_MAX_COUNT) then
            begin
              sndNum := g_SoundsCount;
              Inc(g_SoundsCount);

              SoundsList.AddItem(soundsInMem[sndNum].name, nil);
              SoundsList.ItemIndex := sndNum;
              NameEdit.Text := 'Sound #' + IntToStr(g_SoundsCount);

              destFileName := g_SndPath + '/sound_' + IntToStr(sndNum) + '.dat';
              if (FileExists(destFileName)) then
                DeleteFile(destFileName);

              soundsInMem[sndNum].extension := ExtractFileExt(FileName);

              CopyFile(FileName, destFileName);
            end
          else
            { Нельзя добавлять файлы - выйти }
            begin
              MessageDlg('Error', 'Достигнут предел звуков. Текущий максимум = ' + IntToStr(SOUNDS_MAX_COUNT), mtError, [mbOK], 0);
              break;
            end;
      end;

  SoundsListClick(Self);                           
  DM.OpenSoundDialog.Options := g_OpenOptionsOnce;
  frmPleaseWait.Destroy;
end;

procedure TfrmSoundsEditor.DeleteSoundBtnClick(Sender: TObject);
var
  i : Integer;
  oldFileName,
  newFileName : String;
begin
  StopSound;

  if (g_SoundsCount = 0) then exit;
  if (sndNum < 0) then exit;

  CheckDeleteSounds(soundsInMem[sndNum].name);

  DeleteSound(sndNum);

  for i := sndNum + 1 to g_SoundsCount - 1 do
    begin
      { Переносим файлы звука назад }
      oldFileName := g_SndPath + '/sound_' + IntToStr(i)     + '.dat';
      newFileName := g_SndPath + '/sound_' + IntToStr(i - 1) + '.dat';

      if (FileExists(oldFileName)) then
        RenameFile(oldFileName, newFileName);

      { Переносим настройки звуков назад }
      soundsInMem[i - 1] := soundsInMem[i];
    end;

  Dec(g_SoundsCount); 
  DeleteSound(g_SoundsCount);  { удалить последний звук, т.к. он сместился на один вниз }
  FillSoundsList;

  SoundsListClick(Self);
end;

procedure TfrmSoundsEditor.FormActivate(Sender: TObject);
begin
  SetFocus;
end;

procedure TfrmSoundsEditor.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  SaveSoundPack;

  StopSound;

  Mix_CloseAudio;
  SDL_Quit; 
  writeln('SDL2_mixer stopped!');
end;

procedure TfrmSoundsEditor.FormCreate(Sender: TObject);
begin
  InitForm;
  sdlInitialized := False;

  sound := nil;
  music := nil;

  if (SDL_Init(SDL_INIT_AUDIO) < 0) then exit;
  if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,
                    MIX_DEFAULT_CHANNELS,  1024) < 0) then exit;

  sdlInitialized := True;
  writeln('SDL2_mixer initialized!');
end;

procedure TfrmSoundsEditor.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then Close;
end;

procedure TfrmSoundsEditor.frm_play_btnClick(Sender: TObject);
var
  fileName : String;
begin
  // останов ранее запущенной музыки
  if (isSoundPlaying) then
    begin
      StopSound;
      exit;
    end;

  if (g_SoundsCount = 0) then exit;
  if (sndNum < 0) then exit;

  fileName := g_SndPath + '/sound_' + IntToStr(sndNum) + '.dat';

  if (not sdlInitialized) then exit;

  StopSound;

  if (AnsiIndexStr(soundsInMem[sndNum].extension, audioExtensions) <> -1) then
    begin
      if (not FileExists(fileName)) then exit;
      sound := Mix_LoadWAV(PChar(fileName));
    end;

  if (AnsiIndexStr(soundsInMem[sndNum].extension, musicExtensions) <> -1) then
    begin
      if (not FileExists(fileName)) then exit;
      music := Mix_LoadMUS(PChar(fileName));
    end;

  PlaySound;
end;

procedure TfrmSoundsEditor.NameEditChange(Sender: TObject);
begin      
  with (NameEdit) do
    if (Text = '') then
      Color := clRed
    else
      Color := clWhite;

  if (g_SoundsCount = 0) then exit;
  if (sndNum < 0) then exit;
  if (not canInput) then exit;

  soundsInMem[sndNum].name := NameEdit.Text;
  SoundsList.Items[sndNum] := NameEdit.Text;
end;

procedure TfrmSoundsEditor.NameEditKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
end;

procedure TfrmSoundsEditor.PrecachedChkChange(Sender: TObject);
begin
  if (g_SoundsCount = 0) then exit;
  if (sndNum < 0) then exit;
  if (not canInput) then exit;

  soundsInMem[sndNum].precached := PrecachedChk.Checked;
end;

procedure TfrmSoundsEditor.SoundsListClick(Sender: TObject);
begin
  ParametersGroupBox.Enabled := False;
  canInput := False;
  NameEdit.Text := '<unknown>';
  PrecachedChk.Checked := False;
  DeleteSoundBtn.Enabled := False;

  StopSound;

  sndNum := SoundsList.ItemIndex;
  if (sndNum < 0) then exit;

  with (soundsInMem[sndNum]) do
    begin
      NameEdit.Text        := name;
      PrecachedChk.Checked := precached;

      // MIDI умеет воспроизводиться аудио-системой только в потоковом режиме
      if ((soundsInMem[sndNum].extension = '.mid') or
          (soundsInMem[sndNum].extension = '.midi')) then
        begin
          PrecachedChk.Checked := False;
          PrecachedChk.Enabled := False;
        end
      else 
        PrecachedChk.Enabled := True;
    end;
  canInput := True;   
  ParametersGroupBox.Enabled := True;
  DeleteSoundBtn.Enabled := True;
end;

procedure TfrmSoundsEditor.Timer1Timer(Sender: TObject);
begin
  if (isSoundPlaying) then
    begin
      if (((sound <> nil) and (Mix_Playing(-1) = 0)) or
          ((music <> nil) and (Mix_PlayingMusic = 0))) then
        StopSound;
    end;
end;

procedure TfrmSoundsEditor.InitForm;
begin
  FillSoundsList;
  ParametersGroupBox.Enabled := False;
  DeleteSoundBtn.Enabled := False;
  sndNum := -1;
  canInput := False;
  StopSound;
end;

procedure TfrmSoundsEditor.PlaySound;
begin
  if (sound <> nil) then    // загружен звук?
    begin
      Mix_VolumeChunk(sound, MIX_MAX_VOLUME);
      Mix_PlayChannel(-1, sound, -1);
    end
  else
    if (music <> nil) then  // загружена музыка?
      begin
        Mix_VolumeMusic(MIX_MAX_VOLUME);
        //Mix_RewindMusic;
        Mix_PlayMusic(music, -1);
      end
    else                    // ничего не загружено - выход
      exit;

  DM.ImageList.GetRawImage(1, rawImage);
  frm_play_btn.Glyph.Clear;
  frm_play_btn.Glyph.LoadFromRawImage(rawImage, False);
  frm_play_btn.Caption := 'Stop';

  isSoundPlaying := True;
end;

procedure TfrmSoundsEditor.StopSound;
begin
  if (sound <> nil) then
    begin
      Mix_Pause(-1);
      Mix_FreeChunk(sound);
      sound := nil;
      Mix_HaltChannel(-1);
    end;

  if (music <> nil) then
    begin
      //Mix_PauseMusic;
      Mix_FreeMusic(music);
      music := nil;
      Mix_HaltMusic;
    end;

  DM.ImageList.GetRawImage(0, rawImage);
  frm_play_btn.Glyph.Clear;
  frm_play_btn.Glyph.LoadFromRawImage(rawImage, False);
  frm_play_btn.Caption := 'Play';
  isSoundPlaying := False;
end;

procedure TfrmSoundsEditor.FillSoundsList;
var
  i : Integer;
begin
  with (SoundsList) do
    begin
      Clear;
      for i := 0 to g_SoundsCount - 1 do
        AddItem(soundsInMem[i].name, nil);
      ItemIndex := -1;
    end;
end;

procedure TfrmSoundsEditor.CheckDeleteSounds(sndName: String);
var
  i : Integer;
  _weaponInMem : TWeaponInMem;
begin
  // проверить зависимости от звука (по имени) и удалить зависимость

  // разрушение спрайта
  for i := 0 to g_SpritesCount - 1 do
    with (spritesInMem[i]) do
      if (deathSoundId = sndNum) then     // если совпадает, то удалить индекс
        deathSoundId := -1
      else
        if (deathSoundId > sndNum) then   // если индекс был больше, то сдвинуть его
          Dec(deathSoundId);

  // звук дверей        
  for i := 0 to g_DoorsCount - 1 do
    with (doorsInMem[i]) do
      begin
        if (moveSoundId = sndNum) then     // если совпадает, то удалить индекс
          moveSoundId := -1
        else
          if (moveSoundId > sndNum) then   // если индекс был больше, то сдвинуть его
            Dec(moveSoundId);
                               
        if (stopSoundId = sndNum) then     // если совпадает, то удалить индекс
          stopSoundId := -1
        else
          if (stopSoundId > sndNum) then   // если индекс был больше, то сдвинуть его
            Dec(stopSoundId);
      end;

  // звук стрельбы из оружия
  for i := 0 to g_WeaponsCount - 1 do
    begin
      InitWeapon(@_weaponInMem);
      LoadWeaponInfo(i, @_weaponInMem);
      with (_weaponInMem) do
        begin
          if (fireSoundId = sndNum) then
            fireSoundId := -1
          else
            if (fireSoundId > sndNum) then
              Dec(fireSoundId);

          if (fireSoundId >= sndNum) then
            SaveWeaponInfo(i, @_weaponInMem);
        end;
    end;

  InitWeapon(@_weaponInMem);
end;

end.

