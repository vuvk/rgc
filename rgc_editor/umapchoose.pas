unit uMapChoose;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, Buttons,
  Spin, StdCtrls, LCLType;

type

  { TfrmMapChoose }

  TfrmMapChoose = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    SpinEdit1: TSpinEdit;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender : TObject; var Key : Word; Shift : TShiftState
      );
  private

  public

  end;

implementation

uses
  uMain,
  uContainer;

{$R *.lfm}

{ TfrmMapChoose }

procedure TfrmMapChoose.BitBtn2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmMapChoose.FormActivate(Sender: TObject);
begin
  SetFocus;
end;

procedure TfrmMapChoose.FormCreate(Sender: TObject);
begin
  SpinEdit1.MinValue := 1;
  SpinEdit1.MaxValue := g_MapsCount;
  SpinEdit1.Value := curMapNum + 1;

  Label1.Caption := 'Level number (Min = 1, Max = ' + IntToStr(g_MapsCount) + ')';
end;

procedure TfrmMapChoose.FormKeyDown(Sender : TObject; var Key : Word;
  Shift : TShiftState);
begin
  if (Key = VK_ESCAPE) then Close;
end;

procedure TfrmMapChoose.BitBtn1Click(Sender: TObject);
begin
  uMain.curMapNum := SpinEdit1.Value - 1;
  Close;
end;

end.

