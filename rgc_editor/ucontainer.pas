{
  Модуль, в котором описаны все использующиеся массивы, паки, константы и проч.
}

unit uContainer;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Dialogs,
  Imaging,
  ImagingTypes,
  Graphics,
  ExtCtrls;
    
const
  DEFAULT_COMPRESSION_LEVEL = 9;

  MAP_WIDTH   = 64;
  MAP_HEIGHT  = 64;
  MAPS_MAX_COUNT = 256;
var
  g_MapsCount : Byte = 1;

const
  TEXTURE_MAX_WIDTH  = 256;
  TEXTURE_MAX_HEIGHT = 256;
  TEXTURE_MAX_SIZE   = TEXTURE_MAX_WIDTH*TEXTURE_MAX_HEIGHT*SizeOf(Int32);
  TEXTURES_MAX_COUNT = 1024;
var
  g_TexturesCount : Word = 0;

const
  SPRITES_MAX_COUNT   = 1024;
var
  g_SpritesCount : Word = 0;

const
  SKYBOXES_MAX_COUNT = MAPS_MAX_COUNT;
  SKYBOX_MAX_WIDTH   = 640;
  SKYBOX_MAX_HEIGHT  = 640;
  SKYBOX_MAX_SIZE    = SKYBOX_MAX_WIDTH*SKYBOX_MAX_HEIGHT*SizeOf(Int32);
var
  g_SkyboxesCount : Word = 0;

const
  DOORS_MAX_COUNT = 1024;
  KEYS_MAX_COUNT  = 1024;
var
  g_DoorsCount : Word = 0;  
  g_KeysCount  : Word = 0;

const
  WEAPONS_MAX_COUNT = 10;
  AMMO_MAX_COUNT    = 1024;   
  BULLETS_MAX_COUNT = 1024;
var
  g_WeaponsCount : Word = 0;
  g_AmmoCount    : Word = 0;
  g_BulletsCount : Word = 0;

const
  SOUNDS_MAX_COUNT   = 1024;
var
  g_SoundsCount : Word = 0;

var
  g_CellWidth  : Byte = 32;
  g_CellHeight : Byte = 32;
const
  //CELL_TEXTURE_SIZE = 4150;
  CELL_TEXTURES_COUNT = TEXTURES_MAX_COUNT +
                        SPRITES_MAX_COUNT  +
                        DOORS_MAX_COUNT    +
                        KEYS_MAX_COUNT     +
                        WEAPONS_MAX_COUNT  +
                        AMMO_MAX_COUNT;

const
  PREVIEW_MAX_WIDTH  = 256;
  PREVIEW_MAX_HEIGHT = 256;

type
  // заголовок TGA
  TTgaHeader = packed record
    idlength : byte;
    colourmaptype : byte;
    datatypecode : byte;
    colourmaporigin : Int16;
    colourmaplength : Int16;
    colourmapdepth : byte;
    x_origin : Int16;
    y_origin : Int16;
    width : Word;
    height : Word;
    bitsperpixel : byte;
    imagedescriptor : byte;
  end;

  // один файл текстуры = TEXTURE_MAX_SIZE байт в формате ARGB
  TTexture = array[0..TEXTURE_MAX_SIZE - 1] of Byte;
  // информация о текстуре и кадрах в памяти редактора
  TTextureInMem = record
    animSpeed : Byte;           // скорость анимации
    framesCount : UInt32;       // количество кадров
    frames    : array of TBitmap;
  end;

  // карта
  { TMap }
  TMapElement = Word;
  PMapElement = ^TMapElement;

  TMap = class(TObject)
    public
      name : String;              // имя карты
      floorColor : Int32;         // цвет пола
      ceilColor  : Int32;         // цвет потолка
      fogColor   : Int32;         // цвет тумана
      fogIntensity : Single;      // интенсивность тумана
      showFloor  : Boolean;       // показывать текстурный пол?
      showCeil   : Boolean;       // показывать текстурный потолок?
      showFog    : Boolean;       // показывать туман?
      showSky    : Boolean;       // показывать небо?
      skyNumber  : SmallInt;      // номер скайбокса
      floor : array[0..MAP_WIDTH - 1, 0..MAP_HEIGHT - 1] of TMapElement;    // массив с индексами текстур пола
      level : array[0..MAP_WIDTH - 1, 0..MAP_HEIGHT - 1] of TMapElement;    // массив с содержимым уровня (стены, спрайты, двери и т.д.)
      ceil  : array[0..MAP_WIDTH - 1, 0..MAP_HEIGHT - 1] of TMapElement;    // массив с индексами текстур потолка

      constructor Create;
      destructor Destroy; override;
      procedure Clear;
  end;
  PMap = ^TMap;

  // пак карт
  TMapPack = record
    //id   : Int32;     // MAP_PACK_ID
    //ver  : Int16;     // MAP_PACK_VER
    //maps : PMap;
    //maps : array[0..g_MapsCount - 1] of TMap;
    maps : array of TMap;
  end;

  // спрайт по сути текстура
  TSprite = TTexture;
  // информация о спрайте в памяти редактора
  TSpriteInMem = record                        
    name        : String;         // имя спрайта
    axisParams  : Byte;           // поворот по осям: 0 = смотрит на игрока, 1 = фиксация по горизонтали, 2 = фиксация по вертикали
    collision   : Boolean;        // можно столкнуться
    destroyable : Boolean;        // разрушаемый?
    deathSoundId : SmallInt;         // звук разрушения
    endurance   : Single;         // живучесть
    playOnce    : Boolean;        // играть анимацию один раз
    deleteOnLastFrame : Boolean;  // удалить объект на последнем кадре?
    animSpeed   : Byte;           // скорость анимации
    scaleX      : Single;         // скалирование по X
    scaleY      : Single;         // скалирование по Y
    solidX      : Single;         // размер ограничивающей коробки по ширине
    solidY      : Single;         // размер ограничивающей коробки по высоте
    framesCount : UInt32;         // количество кадров
    frames      : array of TBitmap;
  end;

  // скайбокс - набор из шести текстур куба
  // стороны скайбокса
const
  SKYBOX_FRONT  = 1;
  SKYBOX_BACK   = 2;
  SKYBOX_LEFT   = 4;
  SKYBOX_RIGHT  = 8;
  SKYBOX_TOP    = 16;
  SKYBOX_BOTTOM = 32;
  SKYBOX_ALL_SIDES = SKYBOX_FRONT or SKYBOX_BACK   or
                     SKYBOX_LEFT  or SKYBOX_RIGHT  or
                     SKYBOX_TOP   or SKYBOX_BOTTOM;

type
  // дверь по сути текстура
  TDoor = TTexture;
  // информация о спрайте в памяти редактора
  TDoorInMem = record
    name        : String;          // имя двери
    openSpeed   : Single;          // скорость открывания (скорость - блок/сек)
    stayOpened  : Boolean;         // не закрывается после открытия
    needKey     : Boolean;         // необходимость ключа
    needKeyMsg  : ShortString;     // сообщение о необходимости ключа
    startSoundId: SmallInt;        // звук старта движения
    moveSoundId : SmallInt;        // звук движения двери
    stopSoundId : SmallInt;        // звук останова двери
    keyId       : SmallInt;        // номер ключа, который её может открыть
    width       : Single;          // толщина двери
    rib         : TBitmap;         // текстура ребра двери

    animSpeed   : Byte;            // скорость анимации
    framesCount : UInt32;          // количество кадров
    frames      : array of TBitmap;// кадры
  end;     
  // дверь по сути текстура
  TKey = TTexture;
  // информация о спрайте в памяти редактора
  TKeyInMem = record
    name        : String;          // имя ключа
    animSpeed   : Byte;            // скорость анимации
    pickUpSoundId : SmallInt;      // звук подбора ключа
    solidX      : Single;          // размер ограничивающей коробки по ширине
    solidY      : Single;          // размер ограничивающей коробки по высоте
    framesCount : UInt32;          // количество кадров
    frames      : array of TBitmap;// кадры
  end;

  // оружие (как подбирабельное)
  TWeaponItem = TTexture;
  // информация в памяти редактора
  TWeaponItemInMem = record
    name        : String;          // имя
    damage      : Single;          // наносимый урон
    distance    : Single;          // расстояние наносимого урона
    accuracy    : Single;          // кучность стрельбы в процентах (1 - 100)
    curAmmo     : UInt32;          // количество пуль при подборе
    maxAmmo     : UInt32;          // максимально возможное для переноски кол-во боеприпасов
    ammoTypeId  : SmallInt;        // тип пуль
    infiniteAmmo : Boolean;        // бесконечные патроны?
    fireType    : Byte;            // тип огня (0 - рейкаст, 1 - видимая пуля)
    bulletId    : Int32;           // идентификатор пули, если fireType = 1
    pickUpSoundId : SmallInt;      // звук подбора
    solidX      : Single;          // размер ограничивающей коробки по ширине
    solidY      : Single;          // размер ограничивающей коробки по высоте
    animSpeed   : Byte;            // скорость анимации
    framesCount : UInt32;          // количество кадров
    frames      : array of TBitmap;// кадры
  end;
const
  WEAPON_INHAND_MAX_WIDTH  = 512;
  WEAPON_INHAND_MAX_HEIGHT = 512;
type
  // оружие в руках  
  TWeaponInMem = record
    weaponId    : Int32;           // номер оружия
    fireFrame   : UInt32;          // номер кадра выстрела   
    fireSoundId : SmallInt;        // номер звука
    align       : Byte;            // выравнивание на экране
    animSpeed   : Byte;            // скорость анимации
    framesCount : UInt32;          // количество кадров
    frames      : array of TBitmap;// кадры
  end;
  PWeaponInMem = ^TWeaponInMem;

  // пуля по сути текстура
  TBullet = TTexture;
  // информация о пуле в памяти редактора
  TBulletInMem = record
    name        : String;         // имя
    solidX      : Single;         // размер ограничивающей коробки по ширине
    solidY      : Single;         // размер ограничивающей коробки по высоте
    speed       : Single;         // скорость движения
    lifeTime    : Single;         // максимальная продолжительность "жизни" снаряда
    lifeFrameLast : UInt32;       // максимальный кадр зацикленной анимации жизни
    animSpeed   : Byte;           // скорость анимации
    framesCount : UInt32;         // количество кадров
    frames      : array of TBitmap;
  end;
  
  // патрон по сути текстура
  TAmmo = TTexture;
  // информация в памяти редактора
  TAmmoInMem = record
    name        : String;         // имя
    solidX      : Single;         // размер ограничивающей коробки по ширине
    solidY      : Single;         // размер ограничивающей коробки по высоте
    volume      : Word;           // объем патронника
    pickUpSoundId : SmallInt;     // звук подбора
    ammoTypeId  : SmallInt;       // тип патронов
    animSpeed   : Byte;           // скорость анимации
    framesCount : UInt32;         // количество кадров
    frames      : array of TBitmap;
  end;

  // калибры по умолчанию
const
  DEF_AMMO_TYPES : array [0..8] of String = (
    'Projectile',
    'Pistol ammo',
    'Rifle ammo',
    'Shotgun ammo',
    'Machine-gun ammo',
    'Grenade',
    'Rocket',
    'Fire',
    'Plasma'
  );
  AMMO_TYPES_FILE = 'ammo_types.cfg';

type
  // звуки
  TSoundInMem = record
    name      : String;         // имя
    precached : Boolean;        // загрузить предварительно в память?
    extension : String;         // расширение оригинального файла
  end;

// пути до ресурсов
var
  g_WorkDir : AnsiString = './';
const
  RES_PATH = 'resources';
var
  g_TexPath  : AnsiString;   // текстуры
  g_SprPath  : AnsiString;   // спрайты
  g_MapPath  : AnsiString;   // карты
  g_SkyPath  : AnsiString;   // небеса
  g_DkPath   : AnsiString;   // двери/ключи
  g_WeapPath : AnsiString;   // оружие
  g_WaPath   : AnsiString;   // оружие(подбирабельное)/патроны
  g_BulPath  : AnsiString;   // видимые пули
  g_SndPath  : AnsiString;   // звуки

var
  // текстуры в памяти редактора
  texturesInMem : array of TTextureInMem;
  spritesInMem  : array of TSpriteInMem;
  doorsInMem    : array of TDoorInMem;
  keysInMem     : array of TKeyInMem;
  weapItemsInMem: array of TWeaponItemInMem;
  g_WeaponInMem : TWeaponInMem;
  bulletsInMem  : array of TBulletInMem;
  ammoInMem     : array of TAmmoInMem;
  soundsInMem   : array of TSoundInMem;   

  // Калибры, типы пуль
  ammoTypes : TStringList;

  cellBmp : array[0..CELL_TEXTURES_COUNT - 1] of TBitmap;           // текстуры ячеек для рисования (в обычном режиме)
  cellBmpShadowed : array[0..CELL_TEXTURES_COUNT - 1] of TBitmap;   // текстуры ячеек стен для рисования (в режиме рисования пола/потолка)

  // пак карт в памяти редактора
  mapPack : TMapPack;


// подготовить пути проекта
procedure UpdatePaths(workDir : AnsiString);

// проверяет карту на несуществующие блоки (не загруженные, удаленные...)
procedure CheckMap(num : Integer);
procedure CheckMaps;

// пак текстур      
procedure InitTexture(num : Integer);
procedure InitTextures;
procedure LoadTexturePack;
procedure SaveTexturePack;
// работа с паком карт  
procedure DeleteMap(num : Integer);
procedure InitMapPack;
procedure LoadMapPack;
procedure SaveMapPack;
// текстуры для ячеек в редакторе карт
procedure InitCellTextures;
procedure LoadCellTextures; 
// текстуры спрайтов     
procedure InitSprite(num : Integer);
procedure InitSprites;
procedure LoadSpritePack;
procedure SaveSpritePack;
// пак скайбоксов
procedure InitSkyboxes;
procedure LoadSkyboxCount;
function  LoadSkyboxNames : TStringList;
function  LoadSkybox(num : Cardinal; side : Byte) : TBitmap;
procedure SaveSkyboxCount;
procedure SaveSkyboxNames(list : TStringList);
procedure SaveSkybox(num : Cardinal; bitmap : TBitmap; side : Byte);
procedure DeleteSkybox(num : Cardinal; side : Byte);
// пак дверей и ключей
procedure InitDoor(num : Integer);   
procedure InitKey(num : Integer);
procedure InitDoorsKeys;
procedure LoadDoorKeyPack;
procedure SaveDoorKeyPack;
// пак оружия и боеприпасов      
procedure InitWeaponItem(num : Integer);
procedure InitAmmo(num : Integer);
procedure InitWeaponsAmmo;
procedure LoadWeaponAmmoPack;
procedure SaveWeaponAmmoPack;  
procedure InitBullet(num : Integer);
procedure InitBullets;
procedure LoadBulletPack;
procedure SaveBulletPack;
{
  инициализация оружия.
    _weaponInMem - что обнулять. По умолчанию g_WeaponInMem
}
procedure InitWeapon(_weaponInMem : PWeaponInMem = nil);
procedure LoadWeaponInfo(num : Integer; _weaponInMem : PWeaponInMem = nil); 
procedure LoadWeaponFrames(num : Integer; _weaponInMem : PWeaponInMem = nil);
{
  загрузка в память оружия.
    num - номер оружия
    _weaponInMem - куда грузить. По умолчанию грузится в g_WeaponInMem
}
procedure LoadWeapon(num : Integer; _weaponInMem : PWeaponInMem = nil); 
{
  сохранение из памяти оружия.
    num - номер оружия
    _weaponInMem - откуда грузить. По умолчанию грузится из g_WeaponInMem
}
procedure SaveWeaponInfo(num : Integer; _weaponInMem : PWeaponInMem = nil);
procedure SaveWeaponFrames(num : Integer; _weaponInMem : PWeaponInMem = nil);
procedure SaveWeapon(num : Integer; _weaponInMem : PWeaponInMem = nil);
// Типы патронов
procedure LoadAmmoTypes;
procedure SaveAmmoTypes;
// звуки      
procedure InitSound(num : Integer);
procedure InitSounds;
procedure LoadSoundPack;
procedure SaveSoundPack;
procedure SaveSound(num : Integer);
procedure DeleteSound(num : Integer);




implementation

uses
  uUtil,
  uDM,
  uResManager;

procedure UpdatePaths(workDir: AnsiString);
var
  i : Cardinal;
begin                
  if ((workDir[Length(workDir)] <> '\') and
      (workDir[Length(workDir)] <> '/')) then
    workDir += '/';
  for i := 1 to Length(workDir) do
    if (workDir[i] = '\') then
      workDir[i] := '/';

  g_WorkDir := workDir;

  g_TexPath  := g_WorkDir + RES_PATH + '/textures';
  g_SprPath  := g_WorkDir + RES_PATH + '/sprites';
  g_MapPath  := g_WorkDir + RES_PATH + '/maps';
  g_SkyPath  := g_WorkDir + RES_PATH + '/skyboxes';
  g_DkPath   := g_WorkDir + RES_PATH + '/doors_keys';  
  g_WaPath   := g_WorkDir + RES_PATH + '/weapons_ammo';
  g_WeapPath := g_WaPath  + '/in_hand';
  g_BulPath  := g_WaPath  + '/bullets';
  g_SndPath  := g_WorkDir + RES_PATH + '/sounds';
end;

procedure CheckMap(num : Integer);
var
  i, j : Integer;
  cell : TMapElement;
  fromPos, toPos : Integer;
begin
  if ((num < 0) or (num > High(mapPack.maps))) then exit;

  for i := 0 to MAP_WIDTH - 1 do
    for j := 0 to MAP_HEIGHT - 1 do
      with (mapPack.maps[num]) do
        begin
          // в итоге получается, что -1 это ничего на карте


          // если на карте что-то есть, а в массиве ячеек нет,
          // то обнулить информацию на карте, что там ничего нет
          //if ((cell >= 0) and (cellBmp[cell] = nil)) then
          //  mapPack.maps[num].level[i, j] := 0;

          // на уровне
          cell := level[i, j];
          if (cell > 0) then
            begin
              // проверка стен
              fromPos := 0;
              toPos := fromPos + TEXTURES_MAX_COUNT;
              if (cell <= toPos) then
                if ((texturesInMem[cell - 1].framesCount = 0) or
                    (Length(texturesInMem[cell - 1].frames) = 0) or
                    (texturesInMem[cell - 1].frames[0] = nil)) then
                  begin
                    level[i, j] := 0;
                    continue;
                  end;

              // спрайтов
              fromPos := TEXTURES_MAX_COUNT;
              toPos   := fromPos + SPRITES_MAX_COUNT;
              if ((cell > fromPos) and (cell <= toPos)) then  
                if ((spritesInMem[cell - fromPos - 1].framesCount = 0) or
                    (Length(spritesInMem[cell - fromPos - 1].frames) = 0) or
                    (spritesInMem[cell - fromPos - 1].frames[0] = nil)) then
                  begin
                    level[i, j] := 0;
                    continue;
                  end;

              // дверей
              fromPos := TEXTURES_MAX_COUNT +
                         SPRITES_MAX_COUNT;
              toPos   := fromPos + DOORS_MAX_COUNT;
              if ((cell > fromPos) and (cell <= toPos)) then
                if ((doorsInMem[cell - fromPos - 1].framesCount = 0) or
                    (Length(doorsInMem[cell - fromPos - 1].frames) = 0) or
                    (doorsInMem[cell - fromPos - 1].frames[0] = nil)) then
                  begin
                    level[i, j] := 0;
                    continue;
                  end;

              // ключей
              fromPos := TEXTURES_MAX_COUNT +
                         SPRITES_MAX_COUNT  +
                         DOORS_MAX_COUNT;
              toPos   := fromPos + KEYS_MAX_COUNT;
              if ((cell > fromPos) and (cell <= toPos)) then
                if ((keysInMem[cell - fromPos - 1].framesCount = 0) or
                    (Length(keysInMem[cell - fromPos - 1].frames) = 0) or
                    (keysInMem[cell - fromPos - 1].frames[0] = nil)) then
                  begin
                    level[i, j] := 0;
                    continue;
                  end;

              // оружия
              fromPos := TEXTURES_MAX_COUNT +
                         SPRITES_MAX_COUNT  +
                         DOORS_MAX_COUNT    +
                         KEYS_MAX_COUNT;
              toPos   := fromPos + WEAPONS_MAX_COUNT;
              if ((cell > fromPos) and (cell <= toPos)) then
                if ((weapItemsInMem[cell - fromPos - 1].framesCount = 0) or
                    (Length(weapItemsInMem[cell - fromPos - 1].frames) = 0) or
                    (weapItemsInMem[cell - fromPos - 1].frames[0] = nil)) then
                  begin
                    level[i, j] := 0;
                    continue;
                  end;

              // патронов  
              fromPos := TEXTURES_MAX_COUNT +
                         SPRITES_MAX_COUNT  +
                         DOORS_MAX_COUNT    +
                         KEYS_MAX_COUNT     +
                         WEAPONS_MAX_COUNT;
              toPos   := fromPos + AMMO_MAX_COUNT;
              if ((cell > fromPos) and (cell <= toPos)) then
                if ((ammoInMem[cell - fromPos - 1].framesCount = 0) or
                    (Length(ammoInMem[cell - fromPos - 1].frames) = 0) or
                    (ammoInMem[cell - fromPos - 1].frames[0] = nil)) then
                  begin
                    level[i, j] := 0;
                    continue;
                  end;
            end;        

          // на полу
          cell := floor[i, j];
          if (cell > 0) then
            begin
              if ((cell <= TEXTURES_MAX_COUNT) and
                  ((texturesInMem[cell - 1].framesCount = 0) or
                   (Length(texturesInMem[cell - 1].frames) = 0) or
                   (texturesInMem[cell - 1].frames[0] = nil))) then
                floor[i, j] := 0;
            end;

          // на потолке
          cell := ceil[i, j];
          if (cell > 0) then
            begin
              if ((cell <= TEXTURES_MAX_COUNT) and
                  ((texturesInMem[cell - 1].framesCount = 0) or
                   (Length(texturesInMem[cell - 1].frames) = 0) or
                   (texturesInMem[cell - 1].frames[0] = nil))) then
                ceil[i, j] := 0;
            end;

          // обновляем инфу по скайбоксам
          if (skyNumber > g_SkyboxesCount) then skyNumber := -1;
      end;
end;

procedure CheckMaps;
var
  i : Integer;
begin
  for i := 0 to g_MapsCount - 1 do
    CheckMap(i);
end;

procedure InitTexture(num : Integer);
var
  i : Integer;
begin
  if ((num < 0) or (num > High(texturesInMem))) then exit;

  with (texturesInMem[num]) do
    begin
      animSpeed := 0;
      framesCount := 0;

      // обнуляем кадры
      for i := 0 to High(frames) do
        begin
          if (frames[i] <> nil) then
            begin
              frames[i].Clear;
              frames[i].Destroy;
            end;
          frames[i] := nil;
        end;
      SetLength(frames, 0);
    end;
end;

procedure InitTextures;
var
  i : Integer;
begin
  // выставляем параметры по умолчанию
  for i := 0 to High(texturesInMem) do
    InitTexture(i);

  g_TexturesCount := 0;
end;

procedure LoadTexturePack;
var
  i, j : Integer;
begin
  InitTextures;

  if (not DirectoryExists(RES_PATH)) then exit;
  if (not DirectoryExists(g_TexPath)) then exit;

  // читаем количество текстур
  if (not Res_OpenIniFromFile(PChar(g_TexPath + '/textures.cfg'))) then
    begin
      WriteLn('can not open textures.cfg!!!');
      exit;
    end;
  g_TexturesCount := Res_IniReadInteger('Options', 'count', 0);
  Res_CloseIni();
  if (g_TexturesCount = 0) then exit;

  for i := 0 to g_TexturesCount - 1 do
    with (texturesInMem[i]) do
      begin
        // читаем инфу по текстуре
        if (not Res_OpenIniFromFile(PChar(g_TexPath + '/texture_' + IntToStr(i) + '.cfg'))) then continue;

        animSpeed   := Res_IniReadInteger('Options', 'animation_speed', 0);
        framesCount := Res_IniReadInteger('Options', 'frames_count',    0);
        Res_CloseIni();

        SetLength(frames, framesCount);

        // читаем кадры, если есть
        for j := 0 to framesCount - 1 do
          frames[j] := LoadBitmapFromDat(g_TexPath + '/texture_' + IntToStr(i) + '_frame_' + IntToStr(j) + '.dat');
      end;
end;

procedure SaveTexturePack;
var
  i, j : Integer;
begin
  if (not DirectoryExists(RES_PATH)) then CreateDir(RES_PATH);
  if (not DirectoryExists(g_TexPath)) then CreateDir(g_TexPath);
  Res_DeleteAllFilesInDir(PChar(g_TexPath), False);

  // записываем конфигурацию текстурного пака
  Res_OpenIniFromBuffer(nil, 0);
  Res_IniWriteInteger('Options', 'count', g_TexturesCount);
  Res_SaveIniToFile(PChar(g_TexPath + '/textures.cfg'));
  Res_CloseIni();

  // формируем пак
  for i := 0 to g_TexturesCount - 1 do
    with (texturesInMem[i]) do
      if (framesCount <> 0) then
        begin
          // записываем параметры текстуры
          Res_OpenIniFromBuffer(nil, 0);
          Res_IniWriteInteger('Options', 'animation_speed', animSpeed);
          Res_IniWriteInteger('Options', 'frames_count',    framesCount);
          Res_SaveIniToFile(PChar(g_TexPath + '/texture_' + IntToStr(i) + '.cfg'));
          Res_CloseIni();

          // записываем текстуры
          for j := 0 to framesCount - 1 do
            SaveBitmapToDat(frames[j], g_TexPath + '/texture_' + IntToStr(i) + '_frame_' + IntToStr(j) + '.dat');
        end;
end;

procedure DeleteMap(num : Integer);
begin
  if ((num < 0) or (num >= g_MapsCount)) then exit;

  with (mapPack) do
    begin
      if (Length(maps) = 0) then
        exit;

      if (maps[num] <> nil) then
        begin
          maps[num].Destroy;
          maps[num] := nil;
        end;
    end;
end;

procedure InitMapPack;
var
  i : Integer;
begin
  with (mapPack) do
    begin
      for i := 0 to High(maps) do
        DeleteMap(i);

      SetLength(maps, g_MapsCount);
      for i := 0 to High(maps) do
        maps[i] := TMap.Create;
    end;
end;

procedure LoadMapPack; 
type
  TCellInfo = packed record
    t : TMapElement;     // тип (номер)
    x, y : Byte;         // позиция
  end;
  //PCellInfo = ^TCellInfo;

var                    
  fileHandle : THandle;
  cellInfo : TCellInfo;
  count : UInt16;
  m, c : Integer;
begin
  if (not DirectoryExists(RES_PATH)) then exit;
  if (not DirectoryExists(g_MapPath)) then exit;

  // читаем количество карт
  if (not Res_OpenIniFromFile(PChar(g_MapPath + '/maps.cfg'))) then
    begin
      WriteLn('can not open maps.cfg!!!');
      exit;
    end;
  g_MapsCount := Res_IniReadInteger('Options', 'count', 0);
  Res_CloseIni();
  if (g_MapsCount = 0) then
    begin
      g_MapsCount := 1; //всегда есть как минимум одна карта 
      InitMapPack;
      exit;
    end;

  with (mapPack) do
    begin
      for m := 0 to High(maps) do
        DeleteMap(m);
      InitMapPack;
    end;

  for m := 0 to g_MapsCount - 1 do
    begin
      with (mapPack.maps[m]) do
        begin
          // читаем параметры карты
          if (not Res_OpenIniFromFile(PChar(g_MapPath + '/map_' + IntToStr(m) + '.cfg'))) then continue;

          name       := Res_IniReadString ('Options', 'name',        PChar('Level #' + IntToStr(m)));
          floorColor := Res_IniReadInteger('Options', 'floor_color', 0);
          ceilColor  := Res_IniReadInteger('Options', 'ceil_color',  0);
          fogColor   := Res_IniReadInteger('Options', 'fog_color',   0);
          fogIntensity := Res_IniReadDouble('Options', 'fog_intensity', 0);
          showFloor  := Res_IniReadBool   ('Options', 'show_floor',  True);
          showCeil   := Res_IniReadBool   ('Options', 'show_ceil',   True);
          showFog    := Res_IniReadBool   ('Options', 'show_fog',    False);
          showSky    := Res_IniReadBool   ('Options', 'show_sky',    False);
          skyNumber  := Res_IniReadInteger('Options', 'sky_number',  -1);
          // конвертируем цвета ABGR -> ARGB
          floorColor := ConvertColor(floorColor);
          ceilColor  := ConvertColor(ceilColor);
          fogColor   := ConvertColor(fogColor);
          Res_CloseIni();

          // читаем карту
          fileHandle := FileOpen(g_MapPath + '/map_' + IntToStr(m) + '.dat', fmOpenRead);
          if (fileHandle = 0) then continue;

          /////////////////////////////////////////////////////
          // читаем зарисованные ячейки пола (если есть)
          /////////////////////////////////////////////////////
          FileRead(fileHandle, count, SizeOf(count));
          if (count > 0) then
            for c := 0 to count - 1 do
              begin
                FileRead(fileHandle, cellInfo, SizeOf(cellInfo));
                floor[cellInfo.x, cellInfo.y] := cellInfo.t;
              end;

          /////////////////////////////////////////////////////
          // читаем зарисованные ячейки уровня (если есть)
          /////////////////////////////////////////////////////
          FileRead(fileHandle, count, SizeOf(count));
          if (count > 0) then
            for c := 0 to count - 1 do
              begin
                FileRead(fileHandle, cellInfo, SizeOf(cellInfo));
                level[cellInfo.x, cellInfo.y] := cellInfo.t;
              end;

          /////////////////////////////////////////////////////
          // читаем зарисованные ячейки потолка (если есть)
          /////////////////////////////////////////////////////
          FileRead(fileHandle, count, SizeOf(count));
          if (count > 0) then
            for c := 0 to count - 1 do
              begin
                FileRead(fileHandle, cellInfo, SizeOf(cellInfo));
                ceil[cellInfo.x, cellInfo.y] := cellInfo.t;
              end;

          FileClose(fileHandle);
          fileHandle := 0;
        end;
    end;
end;

procedure SaveMapPack;
type
  TCellInfo = packed record
    t : TMapElement;     // тип (номер)
    x, y : Byte;         // позиция
  end;

var          
  fileHandle : THandle;
  cells : array [0..MAP_WIDTH*MAP_HEIGHT - 1] of TCellInfo;
  count : UInt16;
  x, y, m : Byte;
begin
  if (not DirectoryExists(RES_PATH)) then CreateDir(RES_PATH);
  if (not DirectoryExists(g_MapPath)) then CreateDir(g_MapPath);
  Res_DeleteAllFilesInDir(PChar(g_MapPath), False);

  // записываем конфигурацию пака
  Res_OpenIniFromBuffer(nil, 0);
  Res_IniWriteInteger('Options', 'count', g_MapsCount);
  Res_SaveIniToFile(PChar(g_MapPath + '/maps.cfg'));
  Res_CloseIni();

  // новый формат карт (сжатый)
  for m := 0 to g_MapsCount - 1 do
    with (mapPack.maps[m]) do
      begin           
        // записываем параметры карты
        Res_OpenIniFromBuffer(nil, 0);
        Res_IniWriteString ('Options', 'name', PChar(name));
        // конвертируем цвета ABGR -> ARGB
        Res_IniWriteInteger('Options', 'floor_color', ConvertColor(floorColor));
        Res_IniWriteInteger('Options', 'ceil_color',  ConvertColor(ceilColor));
        Res_IniWriteInteger('Options', 'fog_color',   ConvertColor(fogColor));
        Res_IniWriteDouble ('Options', 'fog_intensity', fogIntensity);
        Res_IniWriteBool   ('Options', 'show_floor',  showFloor);
        Res_IniWriteBool   ('Options', 'show_ceil',   showCeil);
        Res_IniWriteBool   ('Options', 'show_fog',    showFog);
        Res_IniWriteBool   ('Options', 'show_sky',    showSky);
        Res_IniWriteInteger('Options', 'sky_number',  skyNumber);
        Res_SaveIniToFile(PChar(g_MapPath + '/map_' + IntToStr(m) + '.cfg'));
        Res_CloseIni();

        // записываем саму карту
        fileHandle := FileCreate(g_MapPath + '/map_' + IntToStr(m) + '.dat', fmOpenReadWrite);
        if (fileHandle = 0) then continue;
        FileSeek(fileHandle, 0, fsFromBeginning);

        /////////////////////////////////////////////////////
        // теперь ищем количество зарисованных ячеек пола
        /////////////////////////////////////////////////////
        count := 0;
        for x := 0 to MAP_WIDTH - 1 do
          for y := 0 to MAP_HEIGHT - 1 do
            if (floor[x, y] <> 0) then
              begin
                cells[count].t := floor[x, y];
                cells[count].x := x;
                cells[count].y := y;
                Inc(count);
              end;
        // если что-то было, то записать информацию об ячейках
        FileWrite(fileHandle, count, SizeOf(count));
        if (count > 0) then
          FileWrite(fileHandle, cells, SizeOf(TCellInfo)*count);

        /////////////////////////////////////////////////////
        // теперь ищем количество зарисованных ячеек уровня
        /////////////////////////////////////////////////////
        count := 0;
        for x := 0 to MAP_WIDTH - 1 do
          for y := 0 to MAP_HEIGHT - 1 do
            if (level[x, y] <> 0) then
              begin
                cells[count].t := level[x, y];
                cells[count].x := x;
                cells[count].y := y;
                Inc(count);
              end;
        // если что-то было, то записать информацию об ячейках
        FileWrite(fileHandle, count, SizeOf(count));
        if (count > 0) then
          FileWrite(fileHandle, cells, SizeOf(TCellInfo)*count);

        /////////////////////////////////////////////////////
        // теперь ищем количество зарисованных ячеек потолка
        /////////////////////////////////////////////////////
        count := 0;
        for x := 0 to MAP_WIDTH - 1 do
          for y := 0 to MAP_HEIGHT - 1 do
            if (ceil[x, y] <> 0) then
              begin
                cells[count].t := ceil[x, y];
                cells[count].x := x;
                cells[count].y := y;
                Inc(count);
              end;
        // если что-то было, то записать информацию об ячейках
        FileWrite(fileHandle, count, SizeOf(count));
        if (count > 0) then
          FileWrite(fileHandle, cells, SizeOf(TCellInfo)*count);

        FileClose(fileHandle);   
        fileHandle := 0;
    end;
end;

procedure InitCellTextures;
var
  i : Integer;
begin
  for i := 0 to CELL_TEXTURES_COUNT - 1 do
    begin
      if (cellBmp[i] <> nil) then
        begin            
          cellBmp[i].Clear;
          cellBmp[i].Destroy;
          cellBmp[i] := nil;
        end;

      if (cellBmpShadowed[i] <> nil) then
        begin                       
          cellBmpShadowed[i].Clear;
          cellBmpShadowed[i].Destroy;
          cellBmpShadowed[i] := nil;
        end;
    end;
end;

procedure LoadCellTextures;
var
  i : Integer;
  offset : Integer;
begin
  InitCellTextures;

  // сначала подгружаем текстуры стен
  offset := 0;
  for i := 0 to g_TexturesCount - 1 do
    with (texturesInMem[i]) do
      begin
        if ((framesCount = 0) or
            (frames[0] = nil)) then
          continue;

        cellBmp[i + offset] := ResizeBitmap(frames[0], g_CellWidth, g_CellHeight);
        cellBmpShadowed[i] := ShadowBitmap(cellBmp[i]);
      end;

  // спрайты
  offset := TEXTURES_MAX_COUNT;
  for i := 0 to g_SpritesCount - 1 do
    with (spritesInMem[i]) do
      begin
        if ((framesCount = 0) or
            (frames[0] = nil)) then
          continue;

        cellBmp[i + offset] := ResizeBitmap(frames[0], g_CellWidth, g_CellHeight);
      end;

  // двери
  offset := TEXTURES_MAX_COUNT + SPRITES_MAX_COUNT;
  for i := 0 to g_DoorsCount - 1 do
    with (doorsInMem[i]) do
      begin
        if ((framesCount = 0) or
            (frames[0] = nil)) then
          continue;

        cellBmp[i + offset] := ResizeBitmap(frames[0], g_CellWidth, g_CellHeight);
      end;
              
  // ключи
  offset := TEXTURES_MAX_COUNT + SPRITES_MAX_COUNT + DOORS_MAX_COUNT;
  for i := 0 to g_KeysCount - 1 do
    with (keysInMem[i]) do
      begin
        if ((framesCount = 0) or
            (frames[0] = nil)) then
          continue;

        cellBmp[i + offset] := ResizeBitmap(frames[0], g_CellWidth, g_CellHeight);
      end;

  // оружие
  offset := TEXTURES_MAX_COUNT +
            SPRITES_MAX_COUNT  +
            DOORS_MAX_COUNT    +
            KEYS_MAX_COUNT;
  for i := 0 to g_WeaponsCount - 1 do
    with (weapItemsInMem[i]) do
      begin
        if ((framesCount = 0) or
            (frames[0] = nil)) then
          continue;

        cellBmp[i + offset] := ResizeBitmap(frames[0], g_CellWidth, g_CellHeight);
      end;       

  // патроны
  offset := TEXTURES_MAX_COUNT +
            SPRITES_MAX_COUNT  +
            DOORS_MAX_COUNT    +
            KEYS_MAX_COUNT     +
            WEAPONS_MAX_COUNT;
  for i := 0 to g_AmmoCount - 1 do
    with (ammoInMem[i]) do
      begin
        if ((framesCount = 0) or
            (frames[0] = nil)) then
          continue;

        cellBmp[i + offset] := ResizeBitmap(frames[0], g_CellWidth, g_CellHeight);
      end;
end;

procedure InitSprite(num : Integer); 
var
  i : Integer;
begin
  if ((num < 0) or (num > High(spritesInMem))) then exit;

  with (spritesInMem[num]) do
    begin
      name        := '';
      axisParams  := 0;
      collision   := True;
      destroyable := False;
      deathSoundId:= -1;
      endurance   := 0.0;
      playOnce    := False;
      deleteOnLastFrame := False;
      animSpeed   := 0;  
      framesCount := 0;
      scaleX      := 100.0;
      scaleY      := 100.0;  
      solidX      := 100.0;
      solidY      := 100.0;

      // обнуляем кадры
      for i := 0 to High(frames) do
        begin
          if (frames[i] <> nil) then
            begin
              frames[i].Clear;
              frames[i].Destroy;
            end;
          frames[i] := nil;
        end;
      SetLength(frames, 0);
    end;
end;

procedure InitSprites;
var
  i : Integer;
begin
  // выставляем параметры по умолчанию
  for i := 0 to High(spritesInMem) do
    InitSprite(i);

  g_SpritesCount := 0;
end;

procedure LoadSpritePack;
var
  i, j : Integer;
begin
  InitSprites;

  if (not DirectoryExists(RES_PATH)) then exit;
  if (not DirectoryExists(g_SprPath)) then exit;

  // читаем количество текстур 
  if (not Res_OpenIniFromFile(PChar(g_SprPath + '/sprites.cfg'))) then
    begin
      WriteLn('can not open sprites.cfg!!!');
      exit;
    end;
  g_SpritesCount := Res_IniReadInteger(PChar('Options'), PChar('count'), 0);
  Res_CloseIni();
  if (g_SpritesCount = 0) then exit;

  for i := 0 to g_SpritesCount - 1 do
    with (spritesInMem[i]) do
      begin
        // читаем инфу по текстуре
        if (not Res_OpenIniFromFile(PChar(g_SprPath + '/sprite_' + IntToStr(i) + '.cfg'))) then continue;

        name        := Res_IniReadString ('Options', 'name', PChar('Sprite #' + IntToStr(i)));
        animSpeed   := Res_IniReadInteger('Options', 'animation_speed', 0);
        framesCount := Res_IniReadInteger('Options', 'frames_count',    0);
        axisParams  := Res_IniReadInteger('Options', 'axis',            0);
        collision   := Res_IniReadBool   ('Options', 'collision',       False);
        destroyable := Res_IniReadBool   ('Options', 'destroyable',     False);
        deathSoundId:= Res_IniReadInteger('Options', 'death_sound_id',  -1);
        endurance   := Res_IniReadDouble ('Options', 'endurance',       0.0);
        playOnce    := Res_IniReadBool   ('Options', 'play_once',       False);
        deleteOnLastFrame := Res_IniReadBool('Options', 'delete_on_last_frame', False);
        scaleX      := Res_IniReadDouble ('Options', 'scale_x',         100.0);
        scaleY      := Res_IniReadDouble ('Options', 'scale_y',         100.0);  
        solidX      := Res_IniReadDouble ('Options', 'solid_x',         100.0);
        solidY      := Res_IniReadDouble ('Options', 'solid_y',         100.0);
        Res_CloseIni();

        SetLength(frames, framesCount);

        // читаем кадры, если есть
        for j := 0 to framesCount - 1 do
          frames[j] := LoadBitmapFromDat(g_SprPath + '/sprite_' + IntToStr(i) + '_frame_' + IntToStr(j) + '.dat');
      end;
end;

procedure SaveSpritePack;
var
  i, j : Integer;
begin
  if (not DirectoryExists(RES_PATH)) then CreateDir(RES_PATH);
  if (not DirectoryExists(g_SprPath)) then CreateDir(g_SprPath);
  Res_DeleteAllFilesInDir(PChar(g_SprPath), False);

  // записываем конфигурацию спрайтового пака
  Res_OpenIniFromBuffer(nil, 0);
  Res_IniWriteInteger('Options', 'count', g_SpritesCount);
  Res_SaveIniToFile(PChar(g_SprPath + '/sprites.cfg'));
  Res_CloseIni();

  // формируем пак
  for i := 0 to g_SpritesCount - 1 do
    with (spritesInMem[i]) do
      if (framesCount <> 0) then
        begin
          // записываем параметры текстуры
          Res_OpenIniFromBuffer(nil, 0);
          Res_IniWriteString ('Options', 'name',            PChar(name));
          Res_IniWriteInteger('Options', 'animation_speed', animSpeed);
          Res_IniWriteInteger('Options', 'frames_count',    framesCount);
          Res_IniWriteInteger('Options', 'axis',            axisParams);
          Res_IniWriteBool   ('Options', 'collision',       collision);
          Res_IniWriteBool   ('Options', 'destroyable',     destroyable);
          Res_IniWriteInteger('Options', 'death_sound_id',  deathSoundId);
          Res_IniWriteBool   ('Options', 'play_once',       playOnce);
          Res_IniWriteBool   ('Options', 'delete_on_last_frame', deleteOnLastFrame);
          Res_IniWriteDouble ('Options', 'endurance',       endurance);
          Res_IniWriteDouble ('Options', 'scale_x',         scaleX);   
          Res_IniWriteDouble ('Options', 'scale_y',         scaleY);      
          Res_IniWriteDouble ('Options', 'solid_x',         solidX);
          Res_IniWriteDouble ('Options', 'solid_y',         solidY);
          Res_SaveIniToFile(PChar(g_SprPath + '/sprite_' + IntToStr(i) + '.cfg'));
          Res_CloseIni();

          // записываем текстуры
          for j := 0 to framesCount - 1 do
            SaveBitmapToDat(frames[j], g_SprPath + '/sprite_' + IntToStr(i) + '_frame_' + IntToStr(j) + '.dat');
        end;
end;

procedure InitSkyboxes;
begin
  g_SkyboxesCount := 0;
  Res_DeleteAllFilesInDir(PChar(g_SkyPath), False);
end;

procedure LoadSkyboxCount;
begin
  g_SkyboxesCount := 0;

  if (not FileExists(g_SkyPath + '/skyboxes.cfg')) then exit;
  if (not Res_OpenIniFromFile(PChar(g_SkyPath + '/skyboxes.cfg'))) then exit;

  g_SkyboxesCount := Res_IniReadInteger('Options', 'count', 0);
  Res_CloseIni();
end;

function LoadSkyboxNames: TStringList;
var
  list : TStringList = nil;
  i : Integer;
  defName : String;
  name : String;
begin         
  list := TStringList.Create; 
  Result := list;

  if (g_SkyboxesCount = 0) then exit;

  for i := 0 to g_SkyboxesCount - 1 do
    begin
      defName := 'Skybox #' + IntToStr(i);

      if (not FileExists(g_SkyPath + '/skybox_' + IntToStr(i) + '.cfg')) then
        name := defName
      else
        if (not Res_OpenIniFromFile(PChar(g_SkyPath + '/skybox_' + IntToStr(i) + '.cfg'))) then
          name := defName
        else
          begin
            name := Res_IniReadString('Options', 'name', PChar(defName));
            Res_CloseIni();
          end;

      list.Add(name);
    end;
end;

function LoadSkybox(num: Cardinal; side: Byte): TBitmap;  
var
  fName : AnsiString = '';
begin
  Result := nil;

  if (num >= g_SkyboxesCount) then exit;
  if (side = 0) then exit;;

  if (not DirectoryExists(g_SkyPath)) then exit;

  if ((side and SKYBOX_FRONT ) > 0) then fName := g_SkyPath + '/skybox_front_'  + IntToStr(num) + '.dat';
  if ((side and SKYBOX_BACK  ) > 0) then fName := g_SkyPath + '/skybox_back_'   + IntToStr(num) + '.dat';
  if ((side and SKYBOX_LEFT  ) > 0) then fName := g_SkyPath + '/skybox_left_'   + IntToStr(num) + '.dat';
  if ((side and SKYBOX_RIGHT ) > 0) then fName := g_SkyPath + '/skybox_right_'  + IntToStr(num) + '.dat';
  if ((side and SKYBOX_TOP   ) > 0) then fName := g_SkyPath + '/skybox_top_'    + IntToStr(num) + '.dat';
  if ((side and SKYBOX_BOTTOM) > 0) then fName := g_SkyPath + '/skybox_bottom_' + IntToStr(num) + '.dat';

  if (FileExists(fName)) then
    Result := LoadBitmapFromDat(fName);
end;

procedure SaveSkyboxCount;
begin
  if (not DirectoryExists(RES_PATH)) then CreateDir(RES_PATH);
  if (not DirectoryExists(g_SkyPath)) then CreateDir(g_SkyPath);

  if (FileExists(g_SkyPath + '/skyboxes.cfg')) then
    DeleteFile(g_SkyPath + '/skyboxes.cfg');

  if (not Res_OpenIniFromBuffer(nil, 0)) then exit;

  Res_IniWriteInteger('Options', 'count', g_SkyboxesCount);
  Res_SaveIniToFile(PChar(g_SkyPath + '/skyboxes.cfg'));
  Res_CloseIni();
end;

procedure SaveSkyboxNames(list : TStringList);
var
  i : Integer;
begin
  for i := 0 to list.Count - 1 do
    begin
      if (not DirectoryExists(RES_PATH)) then CreateDir(RES_PATH);
      if (not DirectoryExists(g_SkyPath)) then CreateDir(g_SkyPath);

      if (FileExists(g_SkyPath + '/skybox_' + IntToStr(i) + '.cfg')) then
        DeleteFile(g_SkyPath + '/skybox_' + IntToStr(i) + '.cfg');

      if (not Res_OpenIniFromBuffer(nil, 0)) then
        continue;

      Res_IniWriteString('Options', 'name', PChar(list.Strings[i]));
      Res_SaveIniToFile(PChar(g_SkyPath + '/skybox_' + IntToStr(i) + '.cfg'));
      Res_CloseIni();
    end;
end;

procedure SaveSkybox(num: Cardinal; bitmap: TBitmap; side: Byte);
var
  fName : AnsiString = '';
begin
  if (not DirectoryExists(RES_PATH)) then CreateDir(RES_PATH);
  if (not DirectoryExists(g_SkyPath)) then CreateDir(g_SkyPath);
  if (side = 0) then exit;

  if ((side and SKYBOX_FRONT ) > 0) then fName := g_SkyPath + '/skybox_front_'  + IntToStr(num) + '.dat';
  if ((side and SKYBOX_BACK  ) > 0) then fName := g_SkyPath + '/skybox_back_'   + IntToStr(num) + '.dat';
  if ((side and SKYBOX_LEFT  ) > 0) then fName := g_SkyPath + '/skybox_left_'   + IntToStr(num) + '.dat';
  if ((side and SKYBOX_RIGHT ) > 0) then fName := g_SkyPath + '/skybox_right_'  + IntToStr(num) + '.dat';
  if ((side and SKYBOX_TOP   ) > 0) then fName := g_SkyPath + '/skybox_top_'    + IntToStr(num) + '.dat';
  if ((side and SKYBOX_BOTTOM) > 0) then fName := g_SkyPath + '/skybox_bottom_' + IntToStr(num) + '.dat';

  if (fName <> '') then
    SaveBitmapToDat(bitmap, fName);
end;

procedure DeleteSkybox(num: Cardinal; side: Byte);
begin
  if (num >= g_SkyboxesCount) then exit;

  if (not DirectoryExists(RES_PATH)) then exit;
  if (not DirectoryExists(g_SkyPath)) then exit;
  DeleteFile(g_SkyPath + '/skybox_'  + IntToStr(num) + '.cfg');

  if (side = 0) then exit;
  if ((side and SKYBOX_FRONT ) > 0) then DeleteFile(g_SkyPath + '/skybox_front_'  + IntToStr(num) + '.dat');
  if ((side and SKYBOX_BACK  ) > 0) then DeleteFile(g_SkyPath + '/skybox_back_'   + IntToStr(num) + '.dat');
  if ((side and SKYBOX_LEFT  ) > 0) then DeleteFile(g_SkyPath + '/skybox_left_'   + IntToStr(num) + '.dat');
  if ((side and SKYBOX_RIGHT ) > 0) then DeleteFile(g_SkyPath + '/skybox_right_'  + IntToStr(num) + '.dat');
  if ((side and SKYBOX_TOP   ) > 0) then DeleteFile(g_SkyPath + '/skybox_top_'    + IntToStr(num) + '.dat');
  if ((side and SKYBOX_BOTTOM) > 0) then DeleteFile(g_SkyPath + '/skybox_bottom_' + IntToStr(num) + '.dat');
end;

procedure InitDoor(num: Integer);
var
  i : Integer;
begin
  if ((num < 0) or (num > High(doorsInMem))) then exit;

  with (doorsInMem[num]) do
    begin
      name        := '';
      openSpeed   := 1.0;
      stayOpened  := False;
      needKey     := False;
      needKeyMsg  := 'I need a key.';
      startSoundId:= -1;
      moveSoundId := -1;
      stopSoundId := -1;
      keyId       := -1;
      width       := 1.0;
      if (rib <> nil) then
        begin
          rib.Clear;
          rib.Destroy;
          rib := nil;
        end;

      animSpeed   := 0;
      framesCount := 0;

      // обнуляем кадры
      for i := 0 to High(frames) do
        begin
          if (frames[i] <> nil) then
            begin
              frames[i].Clear;
              frames[i].Destroy;
            end;
          frames[i] := nil;
        end;
      SetLength(frames, 0);
    end;
end;

procedure InitKey(num: Integer);
var
  i : Integer;
begin
  if ((num < 0) or (num > High(keysInMem))) then exit;

  with (keysInMem[num]) do
    begin
      name        := '';
      pickUpSoundId := -1;
      animSpeed   := 0;
      framesCount := 0;   
      solidX      := 100.0;
      solidY      := 100.0;

      // обнуляем кадры
      for i := 0 to High(frames) do
        begin
          if (frames[i] <> nil) then
            begin
              frames[i].Clear;
              frames[i].Destroy;
            end;
          frames[i] := nil;
        end;
      SetLength(frames, 0);
    end;
end;

procedure InitDoorsKeys;
var
  i : Integer;
begin
  for i := Low(doorsInMem) to High(doorsInMem) do
    InitDoor(i);

  for i := Low(keysInMem) to High(keysInMem) do
    InitKey(i);

  g_DoorsCount := 0;    
  g_KeysCount  := 0;
end;

procedure LoadDoorKeyPack;
var
  i, j : Integer;
  label load_keys;
begin
  InitDoorsKeys;

  if (not DirectoryExists(RES_PATH)) then exit;
  if (not DirectoryExists(g_DkPath)) then exit;

  // ДВЕРИ
  // читаем количество текстур
  if (not Res_OpenIniFromFile(PChar(g_DkPath + '/doors.cfg'))) then
    begin
      WriteLn('can not open doors.cfg!!!');
      goto load_keys;
    end;
  g_DoorsCount := Res_IniReadInteger(PChar('Options'), PChar('count'), 0);
  Res_CloseIni();
  if (g_DoorsCount = 0) then
    goto load_keys;

  for i := 0 to g_DoorsCount - 1 do
    with (doorsInMem[i]) do
      begin
        // читаем инфу по текстуре
        if (not Res_OpenIniFromFile(PChar(g_DkPath + '/door_' + IntToStr(i) + '.cfg'))) then
          continue;

        name        := Res_IniReadString ('Options', 'name',        PChar('Door #' + IntToStr(i)));
        openSpeed   := Res_IniReadDouble ('Options', 'open_speed',  1.0);
        stayOpened  := Res_IniReadBool   ('Options', 'stay_opened', False);
        needKey     := Res_IniReadBool   ('Options', 'need_key',    False);
        needKeyMsg  := Res_IniReadString ('Options', 'message',     'I need a key.');  
        startSoundId:= Res_IniReadInteger('Options', 'start_sound_id', -1);
        moveSoundId := Res_IniReadInteger('Options', 'move_sound_id',  -1);
        stopSoundId := Res_IniReadInteger('Options', 'stop_sound_id',  -1);
        keyId       := Res_IniReadInteger('Options', 'key_id',         -1);
        width       := Res_IniReadDouble ('Options', 'width',       1.0);

        animSpeed   := Res_IniReadInteger('Options', 'animation_speed', 0);
        framesCount := Res_IniReadInteger('Options', 'frames_count',    0);
        Res_CloseIni();

        // грузим текстуру ребра, если есть
        rib := LoadBitmapFromDat(g_DkPath + '/door_' + IntToStr(i) + '_rib.dat');

        // читаем кадры, если есть
        SetLength(frames, framesCount);
        for j := 0 to framesCount - 1 do
          frames[j] := LoadBitmapFromDat(g_DkPath + '/door_' + IntToStr(i) + '_frame_' + IntToStr(j) + '.dat');
      end;


  // КЛЮЧИ    
load_keys:
  // читаем количество текстур
  if (not Res_OpenIniFromFile(PChar(g_DkPath + '/keys.cfg'))) then
    begin
      WriteLn('can not open keys.cfg!!!');
      exit;
    end;
  g_KeysCount := Res_IniReadInteger(PChar('Options'), PChar('count'), 0);
  Res_CloseIni();
  if (g_KeysCount = 0) then exit;

  for i := 0 to g_KeysCount - 1 do
    with (keysInMem[i]) do
      begin
        // читаем инфу по текстуре
        if (not Res_OpenIniFromFile(PChar(g_DkPath + '/key_' + IntToStr(i) + '.cfg'))) then
          continue;

        name        := Res_IniReadString ('Options', 'name', PChar('Key #' + IntToStr(i)));
        pickUpSoundId := Res_IniReadInteger('Options', 'pickup_sound_id', -1);
        animSpeed   := Res_IniReadInteger('Options', 'animation_speed', 0);
        solidX      := Res_IniReadDouble ('Options', 'solid_x', 100.0);     
        solidY      := Res_IniReadDouble ('Options', 'solid_y', 100.0);
        framesCount := Res_IniReadInteger('Options', 'frames_count',    0);
        Res_CloseIni();

        // читаем кадры, если есть
        SetLength(frames, framesCount);
        for j := 0 to framesCount - 1 do
          frames[j] := LoadBitmapFromDat(g_DkPath + '/key_' + IntToStr(i) + '_frame_' + IntToStr(j) + '.dat');
      end;
end;

procedure SaveDoorKeyPack;
var
  i, j : Integer;
begin
  if (not DirectoryExists(RES_PATH)) then CreateDir(RES_PATH);
  if (not DirectoryExists(g_DkPath)) then CreateDir(g_DkPath);
  Res_DeleteAllFilesInDir(PChar(g_DkPath), False);

  // записываем конфигурацию дверного пака
  // двери
  Res_OpenIniFromBuffer(nil, 0);
  Res_IniWriteInteger('Options', 'count', g_DoorsCount);
  Res_SaveIniToFile(PChar(g_DkPath + '/doors.cfg'));
  Res_CloseIni();
  // ключи
  Res_OpenIniFromBuffer(nil, 0);
  Res_IniWriteInteger('Options', 'count', g_KeysCount);
  Res_SaveIniToFile(PChar(g_DkPath + '/keys.cfg'));
  Res_CloseIni();

  // формируем пак
  // двери
  for i := 0 to g_DoorsCount - 1 do
    with (doorsInMem[i]) do
      if (framesCount <> 0) then
        begin
          // записываем параметры дверей
          Res_OpenIniFromBuffer(nil, 0);
          Res_IniWriteString ('Options', 'name',        PChar(name));
          Res_IniWriteDouble ('Options', 'open_speed',  openSpeed);
          Res_IniWriteBool   ('Options', 'stay_opened', stayOpened);
          Res_IniWriteBool   ('Options', 'need_key',    needKey);
          Res_IniWriteString ('Options', 'message',     @(needKeyMsg[1]));
          Res_IniWriteInteger('Options', 'start_sound_id', startSoundId);
          Res_IniWriteInteger('Options', 'move_sound_id',  moveSoundId);
          Res_IniWriteInteger('Options', 'stop_sound_id',  stopSoundId);
          Res_IniWriteInteger('Options', 'key_id',      keyId);       
          Res_IniWriteDouble ('Options', 'width',       width);

          Res_IniWriteInteger('Options', 'animation_speed', animSpeed);
          Res_IniWriteInteger('Options', 'frames_count',    framesCount);
          Res_SaveIniToFile(PChar(g_DkPath + '/door_' + IntToStr(i) + '.cfg'));
          Res_CloseIni();

          // сохраняем текстуру ребра, если есть                        
          SaveBitmapToDat(rib, g_DkPath + '/door_' + IntToStr(i) + '_rib.dat');

          // записываем текстуры дверей
          for j := 0 to framesCount - 1 do
            SaveBitmapToDat(frames[j], g_DkPath + '/door_' + IntToStr(i) + '_frame_' + IntToStr(j) + '.dat');
        end;

  // ключи
  for i := 0 to g_KeysCount - 1 do
    with (keysInMem[i]) do
      if (framesCount <> 0) then
        begin
          // записываем параметры дверей
          Res_OpenIniFromBuffer(nil, 0);
          Res_IniWriteString ('Options', 'name',            PChar(name));   
          Res_IniWriteInteger('Options', 'pickup_sound_id', pickUpSoundId);
          Res_IniWriteInteger('Options', 'animation_speed', animSpeed);
          Res_IniWriteDouble ('Options', 'solid_x',         solidX);   
          Res_IniWriteDouble ('Options', 'solid_y',         solidY);
          Res_IniWriteInteger('Options', 'frames_count',    framesCount);
          Res_SaveIniToFile(PChar(g_DkPath + '/key_' + IntToStr(i) + '.cfg'));
          Res_CloseIni();

          // записываем текстуры ключей
          for j := 0 to framesCount - 1 do
            SaveBitmapToDat(frames[j], g_DkPath + '/key_' + IntToStr(i) + '_frame_' + IntToStr(j) + '.dat');
        end;
end;

procedure InitWeaponItem(num: Integer);
var
  i : Integer;
begin
  if ((num < 0) or (num > High(weapItemsInMem))) then exit;

  with (weapItemsInMem[num]) do
    begin
      name         := '';
      damage       := 10;
      distance     := 100;
      accuracy     := 100;
      curAmmo      := 25;
      maxAmmo      := 100;
      ammoTypeId   := -1;
      infiniteAmmo := False;
      fireType     := 0;
      bulletId     := -1;
      pickUpSoundId:= -1;
      solidX       := 100.0;
      solidY       := 100.0;
      animSpeed    := 0;
      framesCount  := 0;

      // обнуляем кадры
      for i := 0 to High(frames) do
        begin
          if (frames[i] <> nil) then
            frames[i].Destroy;
          frames[i] := nil;
        end;
      SetLength(frames, 0);
    end;
end;

procedure InitBullet(num: Integer);
var
  i : Integer;
begin
  if ((num < 0) or (num > High(bulletsInMem))) then exit;

  with (bulletsInMem[num]) do
    begin
      name        := '';
      speed       := 1;
      lifeTime    := 0;
      lifeFrameLast := 0;
      solidX      := 100.0;
      solidY      := 100.0;
      animSpeed   := 0;
      framesCount := 0;

      // обнуляем кадры
      for i := 0 to High(frames) do
        begin
          if (frames[i] <> nil) then
            begin
              frames[i].Clear;
              frames[i].Destroy;
            end;
          frames[i] := nil;
        end;
      SetLength(frames, 0);
    end;
end;

procedure InitBullets;
var
  i : Integer;
begin
  // выставляем параметры по умолчанию
  for i := 0 to High(bulletsInMem) do
    InitBullet(i);

  g_BulletsCount := 0;
end;

procedure LoadBulletPack;
var
  i, j : Integer;
begin
  InitBullets;

  if (not DirectoryExists(g_BulPath)) then exit;

  // читаем количество текстур
  if (not Res_OpenIniFromFile(PChar(g_BulPath + '/bullets.cfg'))) then
    begin
      WriteLn('can not open bullets.cfg!!!');
      exit;
    end;
  g_BulletsCount := Res_IniReadInteger(PChar('Options'), PChar('count'), 0);
  Res_CloseIni();
  if (g_BulletsCount = 0) then exit;

  for i := 0 to g_BulletsCount - 1 do
    with (bulletsInMem[i]) do
      begin
        // читаем инфу по текстуре
        if (not Res_OpenIniFromFile(PChar(g_BulPath + '/bullet_' + IntToStr(i) + '.cfg'))) then
          continue;

        name          := Res_IniReadString ('Options', 'name',        PChar('Bullet #' + IntToStr(i)));
        solidX        := Res_IniReadDouble ('Options', 'solid_x',     100.0);
        solidY        := Res_IniReadDouble ('Options', 'solid_y',     100.0);
        speed         := Res_IniReadDouble ('Options', 'speed',       1.0);
        lifeTime      := Res_IniReadDouble ('Options', 'life_time',   0.0);
        lifeFrameLast := Res_IniReadInteger('Options', 'life_frame_last', 0);

        animSpeed   := Res_IniReadInteger('Options', 'animation_speed', 0);
        framesCount := Res_IniReadInteger('Options', 'frames_count',    0);
        Res_CloseIni();

        // читаем кадры, если есть
        SetLength(frames, framesCount);
        for j := 0 to framesCount - 1 do
          frames[j] := LoadBitmapFromDat(g_BulPath + '/bullet_' + IntToStr(i) + '_frame_' + IntToStr(j) + '.dat');
      end;
end;

procedure SaveBulletPack;
var
  i, j : Integer;
begin
  if (not DirectoryExists(RES_PATH)) then CreateDir(RES_PATH);
  if (not DirectoryExists(g_WaPath)) then CreateDir(g_WaPath);   
  if (not DirectoryExists(g_BulPath)) then CreateDir(g_BulPath);
  Res_DeleteAllFilesInDir(PChar(g_BulPath), False);

  // записываем конфигурацию пака
  Res_OpenIniFromBuffer(nil, 0);
  Res_IniWriteInteger('Options', 'count', g_BulletsCount);
  Res_SaveIniToFile(PChar(g_BulPath + '/bullets.cfg'));
  Res_CloseIni();

  // формируем пак
  for i := 0 to g_BulletsCount - 1 do
    with (bulletsInMem[i]) do
      if (framesCount <> 0) then
        begin
          // записываем параметры дверей
          Res_OpenIniFromBuffer(nil, 0);
          Res_IniWriteString ('Options', 'name',       PChar(name));
          Res_IniWriteDouble ('Options', 'solid_x',    solidX);
          Res_IniWriteDouble ('Options', 'solid_y',    solidY);    
          Res_IniWriteDouble ('Options', 'speed',      speed);
          Res_IniWriteDouble ('Options', 'life_time',  lifeTime);  
          Res_IniWriteInteger('Options', 'life_frame_last', lifeFrameLast);

          Res_IniWriteInteger('Options', 'animation_speed', animSpeed);
          Res_IniWriteInteger('Options', 'frames_count',    framesCount);
          Res_SaveIniToFile(PChar(g_BulPath + '/bullet_' + IntToStr(i) + '.cfg'));
          Res_CloseIni();

          // записываем текстуры дверей
          for j := 0 to framesCount - 1 do
            SaveBitmapToDat(frames[j], g_BulPath + '/bullet_' + IntToStr(i) + '_frame_' + IntToStr(j) + '.dat');
        end;
end;

procedure InitWeapon(_weaponInMem: PWeaponInMem);
var
  i : Integer;
begin
  if (_weaponInMem = nil) then
    _weaponInMem := @g_WeaponInMem;

  with (_weaponInMem^) do
    begin
      weaponId  := -1;
      fireFrame := 0;
      fireSoundId := -1;

      align       := 0;
      animSpeed   := 0;
      framesCount := 0;

      // обнуляем кадры
      for i := 0 to High(frames) do
        begin
          if (frames[i] <> nil) then
            frames[i].Destroy;
          frames[i] := nil;
        end;
      SetLength(frames, 0);
    end;
end;

procedure InitAmmo(num: Integer);
var
  i : Integer;
begin
  if ((num < 0) or (num > High(ammoInMem))) then exit;

  with (ammoInMem[num]) do
    begin
      name        := '';
      solidX      := 100.0;
      solidY      := 100.0;
      volume      := 25;
      pickUpSoundId:= -1;
      ammoTypeId  := -1;
      animSpeed   := 0;
      framesCount := 0;

      // обнуляем кадры
      for i := 0 to High(frames) do
        begin
          if (frames[i] <> nil) then
            begin
              frames[i].Clear;
              frames[i].Destroy;
            end;
          frames[i] := nil;
        end;
      SetLength(frames, 0);
    end;
end;

procedure InitWeaponsAmmo;
var
  i : Integer;
begin
  for i := Low(weapItemsInMem) to High(weapItemsInMem) do
    InitWeaponItem(i);
  InitWeapon;

  for i := Low(ammoInMem) to High(ammoInMem) do
    InitAmmo(i);

  g_WeaponsCount := 0;
  g_AmmoCount    := 0;
end;

procedure LoadWeaponAmmoPack;
var
  i, j : Integer;
  label load_ammo;
begin
  InitWeaponsAmmo;

  if (not DirectoryExists(RES_PATH)) then exit; 
  if (not DirectoryExists(g_WaPath)) then exit;
  //if (not DirectoryExists(g_WeapPath)) then exit;

  // ОРУЖИЕ
  // читаем количество текстур
  if (not Res_OpenIniFromFile(PChar(g_WaPath + '/weapons.cfg'))) then
    begin
      WriteLn('can not open weapons.cfg!!!');
      goto load_ammo;
    end;
  g_WeaponsCount := Res_IniReadInteger(PChar('Options'), PChar('count'), 0);
  Res_CloseIni();
  if (g_WeaponsCount = 0) then
    goto load_ammo;

  for i := 0 to g_WeaponsCount - 1 do
    with (weapItemsInMem[i]) do
      begin
        // читаем инфу по текстуре
        if (not Res_OpenIniFromFile(PChar(g_WaPath + '/weapon_' + IntToStr(i) + '.cfg'))) then
          continue;

        name         := Res_IniReadString ('Options', 'name',      PChar('Weapon #' + IntToStr(i)));
        damage       := Res_IniReadDouble ('Options', 'damage',    10 );
        distance     := Res_IniReadDouble ('Options', 'distance',  100);
        accuracy     := Res_IniReadDouble ('Options', 'accuracy',  100); 
        curAmmo      := Res_IniReadInteger('Options', 'cur_ammo',  25);
        maxAmmo      := Res_IniReadInteger('Options', 'max_ammo',  100);
        ammoTypeId   := Res_IniReadInteger('Options', 'ammo_type_id',  -1);
        infiniteAmmo := Res_IniReadBool  ('Options', 'infinite_ammo', False);  
        fireType     := Res_IniReadInteger('Options', 'fire_type', 0);
        bulletId     := Res_IniReadInteger('Options', 'bullet_id', -1);
        pickUpSoundId:= Res_IniReadInteger('Options', 'pickup_sound_id', -1);
        solidX       := Res_IniReadDouble ('Options', 'solid_x',   100.0);
        solidY       := Res_IniReadDouble ('Options', 'solid_y',   100.0);

        animSpeed   := Res_IniReadInteger('Options', 'animation_speed', 0);
        framesCount := Res_IniReadInteger('Options', 'frames_count',    0);
        Res_CloseIni();

        // читаем кадры, если есть
        SetLength(frames, framesCount);
        for j := 0 to framesCount - 1 do
          frames[j] := LoadBitmapFromDat(g_WaPath + '/weapon_' + IntToStr(i) + '_frame_' + IntToStr(j) + '.dat');
      end;


  // АММО
load_ammo:
  // читаем количество текстур
  if (not Res_OpenIniFromFile(PChar(g_WaPath + '/ammo.cfg'))) then
    begin
      WriteLn('can not open ammo.cfg!!!');
      exit;
    end;
  g_AmmoCount := Res_IniReadInteger(PChar('Options'), PChar('count'), 0);
  Res_CloseIni();
  if (g_AmmoCount = 0) then exit;

  for i := 0 to g_AmmoCount - 1 do
    with (ammoInMem[i]) do
      begin
        // читаем инфу по текстуре
        if (not Res_OpenIniFromFile(PChar(g_WaPath + '/ammo_' + IntToStr(i) + '.cfg'))) then
          continue;

        name         := Res_IniReadString ('Options', 'name', PChar('Key #' + IntToStr(i)));
        volume       := Res_IniReadInteger('Options', 'volume', 25);
        ammoTypeId   := Res_IniReadInteger('Options', 'ammo_type_id', -1);
        pickUpSoundId:= Res_IniReadInteger('Options', 'pickup_sound_id', -1);
        solidX       := Res_IniReadDouble ('Options', 'solid_x',   100.0);
        solidY       := Res_IniReadDouble ('Options', 'solid_y',   100.0);
        animSpeed    := Res_IniReadInteger('Options', 'animation_speed', 0);
        framesCount  := Res_IniReadInteger('Options', 'frames_count',    0);
        Res_CloseIni();

        // читаем кадры, если есть
        SetLength(frames, framesCount);
        for j := 0 to framesCount - 1 do
          frames[j] := LoadBitmapFromDat(g_WaPath + '/ammo_' + IntToStr(i) + '_frame_' + IntToStr(j) + '.dat');
      end;
end;

procedure SaveWeaponAmmoPack;
var
  i, j : Integer;
begin
  if (not DirectoryExists(RES_PATH))   then CreateDir(RES_PATH);
  if (not DirectoryExists(g_WaPath))   then CreateDir(g_WaPath);
  Res_DeleteAllFilesInDir(PChar(g_WaPath), False);

  // записываем конфигурацию пака
  // оружие
  Res_OpenIniFromBuffer(nil, 0);
  Res_IniWriteInteger('Options', 'count', g_WeaponsCount);
  Res_SaveIniToFile(PChar(g_WaPath + '/weapons.cfg'));
  Res_CloseIni();
  // аммо
  Res_OpenIniFromBuffer(nil, 0);
  Res_IniWriteInteger('Options', 'count', g_AmmoCount);
  Res_SaveIniToFile(PChar(g_WaPath + '/ammo.cfg'));
  Res_CloseIni();

  // формируем пак
  // оружие
  for i := 0 to g_WeaponsCount - 1 do
    with (weapItemsInMem[i]) do
      if (framesCount <> 0) then
        begin
          // записываем параметры
          Res_OpenIniFromBuffer(nil, 0);
          Res_IniWriteString ('Options', 'name',      PChar(name));
          Res_IniWriteDouble ('Options', 'damage',    damage     );
          Res_IniWriteDouble ('Options', 'distance',  distance   );
          Res_IniWriteDouble ('Options', 'accuracy',  accuracy   );  
          Res_IniWriteInteger('Options', 'cur_ammo',  curAmmo    );
          Res_IniWriteInteger('Options', 'max_ammo',  maxAmmo    );  
          Res_IniWriteInteger('Options', 'ammo_type_id', ammoTypeId);
          Res_IniWriteBool   ('Options', 'infinite_ammo', infiniteAmmo); 
          Res_IniWriteInteger('Options', 'fire_type', fireType   );
          Res_IniWriteInteger('Options', 'bullet_id', bulletId   );
          Res_IniWriteInteger('Options', 'pickup_sound_id', pickUpSoundId);
          Res_IniWriteDouble ('Options', 'solid_x',   solidX     );
          Res_IniWriteDouble ('Options', 'solid_y',   solidY     );

          Res_IniWriteInteger('Options', 'animation_speed', animSpeed);
          Res_IniWriteInteger('Options', 'frames_count',    framesCount);
          Res_SaveIniToFile(PChar(g_WaPath + '/weapon_' + IntToStr(i) + '.cfg'));
          Res_CloseIni();

          // записываем текстуры
          for j := 0 to framesCount - 1 do
            SaveBitmapToDat(frames[j], g_WaPath + '/weapon_' + IntToStr(i) + '_frame_' + IntToStr(j) + '.dat');
        end;

  // боеприпасы
  for i := 0 to g_AmmoCount - 1 do
    with (ammoInMem[i]) do
      if (framesCount <> 0) then
        begin
          // записываем параметры дверей
          Res_OpenIniFromBuffer(nil, 0);
          Res_IniWriteString ('Options', 'name',      PChar(name));
          Res_IniWriteInteger('Options', 'volume',    volume);
          Res_IniWriteInteger('Options', 'ammo_type_id', ammoTypeId);
          Res_IniWriteInteger('Options', 'pickup_sound_id', pickUpSoundId);
          Res_IniWriteDouble ('Options', 'solid_x',   solidX);
          Res_IniWriteDouble ('Options', 'solid_y',   solidY);
          Res_IniWriteInteger('Options', 'animation_speed', animSpeed);
          Res_IniWriteInteger('Options', 'frames_count',    framesCount);
          Res_SaveIniToFile(PChar(g_WaPath + '/ammo_' + IntToStr(i) + '.cfg'));
          Res_CloseIni();

          // записываем текстуры ключей
          for j := 0 to framesCount - 1 do
            SaveBitmapToDat(frames[j], g_WaPath + '/ammo_' + IntToStr(i) + '_frame_' + IntToStr(j) + '.dat');
        end;
end;

procedure LoadWeaponInfo(num : Integer; _weaponInMem : PWeaponInMem = nil);
begin                     
  if (not DirectoryExists(RES_PATH)) then exit;
  if (not DirectoryExists(g_WaPath)) then exit;
  if (not DirectoryExists(g_WeapPath)) then exit;
  if ((num < 0) or (num >= g_WeaponsCount)) then exit;

  if (_weaponInMem = nil) then
    _weaponInMem := @g_WeaponInMem;
                        
  _weaponInMem^.weaponId := num;

  with (_weaponInMem^) do
    begin
      // читаем инфу по текстуре
      if (not Res_OpenIniFromFile(PChar(g_WeapPath + '/weapon_inhand_' + IntToStr(num) + '.cfg'))) then
        exit;

      fireFrame   := Res_IniReadInteger('Options', 'fire_frame', 0);
      fireSoundId := Res_IniReadInteger('Options', 'fire_sound_id', -1);
      align       := Res_IniReadInteger('Options', 'align',      0);

      animSpeed   := Res_IniReadInteger('Options', 'animation_speed', 0);
      framesCount := Res_IniReadInteger('Options', 'frames_count',    0);
      Res_CloseIni();
    end;
end;

procedure LoadWeaponFrames(num : Integer; _weaponInMem : PWeaponInMem = nil); 
var
  j : Integer;
begin
  if (_weaponInMem = nil) then
    _weaponInMem := @g_WeaponInMem;

  with (_weaponInMem^) do
    if (framesCount > 0) then
      begin
        SetLength(frames, framesCount);
        for j := 0 to framesCount - 1 do
          frames[j] := LoadBitmapFromDat(g_WeapPath + '/weapon_inhand_' + IntToStr(num) + '_frame_' + IntToStr(j) + '.dat');
      end;
end;

procedure LoadWeapon(num: Integer; _weaponInMem : PWeaponInMem);
begin
  if (_weaponInMem = nil) then
    _weaponInMem := @g_WeaponInMem;

  InitWeapon(_weaponInMem);
  LoadWeaponInfo(num, _weaponInMem);
  LoadWeaponFrames(num, _weaponInMem);
end;

procedure SaveWeaponInfo(num: Integer; _weaponInMem: PWeaponInMem);
begin     
  if (not DirectoryExists(RES_PATH))   then CreateDir(RES_PATH);
  if (not DirectoryExists(g_WaPath))   then CreateDir(g_WaPath);
  if (not DirectoryExists(g_WeapPath)) then CreateDir(g_WeapPath);
  if ((num < 0) or (num >= g_WeaponsCount)) then exit;
           
  if (_weaponInMem = nil) then
    _weaponInMem := @g_WeaponInMem;

  if (_weaponInMem^.weaponId < 0) then exit;

  with (_weaponInMem^) do
    begin
      // записываем параметры
      Res_OpenIniFromBuffer(nil, 0);
      Res_IniWriteInteger('Options', 'fire_frame',    fireFrame);
      Res_IniWriteInteger('Options', 'fire_sound_id', fireSoundId);
      Res_IniWriteInteger('Options', 'align',         align);

      Res_IniWriteInteger('Options', 'animation_speed', animSpeed);
      Res_IniWriteInteger('Options', 'frames_count',    framesCount);
      Res_SaveIniToFile(PChar(g_WeapPath + '/weapon_inhand_' + IntToStr(num) + '.cfg'));
      Res_CloseIni();
    end;
end;

procedure SaveWeaponFrames(num: Integer; _weaponInMem: PWeaponInMem);
var
  i, j : Integer;
begin     
  if (not DirectoryExists(RES_PATH))   then CreateDir(RES_PATH);
  if (not DirectoryExists(g_WaPath))   then CreateDir(g_WaPath);
  if (not DirectoryExists(g_WeapPath)) then CreateDir(g_WeapPath);
  if ((num < 0) or (num >= g_WeaponsCount)) then exit;
               
  if (_weaponInMem = nil) then
    _weaponInMem := @g_WeaponInMem;

  if (_weaponInMem^.weaponId < 0) then exit;

  with (_weaponInMem^) do
    if (framesCount <> 0) then
      // записываем текстуры
      for j := 0 to framesCount - 1 do
        SaveBitmapToDat(frames[j], g_WeapPath + '/weapon_inhand_' + IntToStr(num) + '_frame_' + IntToStr(j) + '.dat');
end;

procedure SaveWeapon(num: Integer; _weaponInMem: PWeaponInMem);
var
  fName : String;
  SR : TSearchRec;
  FindRes : Integer;
begin
  if (_weaponInMem = nil) then
    _weaponInMem := @g_WeaponInMem;

  if (_weaponInMem^.weaponId < 0) then exit;

  // удаляем все файлы текстур данного оружия
  fName := g_WeapPath + '/weapon_inhand_' + IntToStr(num) + '*.*';
  FindRes := FindFirst(fName, faAnyFile, SR);
  while (FindRes = 0) do
    begin
      if (((SR.Attr and faDirectory) <> faDirectory) and (SR.Name <> '.') and (SR.Name <> '..')) then  
        begin
          DeleteFile(SR.Name); 
          DeleteFile(g_WeapPath + '/' + SR.Name);
        end;

      FindRes := FindNext(SR);
    end;
  FindClose(SR);

  SaveWeaponInfo(num, _weaponInMem);
  SaveWeaponFrames(num, _weaponInMem);
end;

procedure LoadAmmoTypes;
var
  i : Integer;
  key : String;
  count : Integer;
begin
  if (ammoTypes = nil) then
    ammoTypes := TStringList.Create;
  ammoTypes.Clear;

  // если внешнего файла нет или не получилось загрузить,
  // то загрузить из дефолтных значений
  if (not Res_OpenIniFromFile(PChar(g_WaPath + '/' + AMMO_TYPES_FILE))) then
    begin
      for i := 0 to High(DEF_AMMO_TYPES) do
        ammoTypes.Add(DEF_AMMO_TYPES[i]);
      exit;
    end
  else
    begin
      count := Res_IniReadInteger('Options', 'count', 0);
      for i := 0 to count - 1 do
        begin
          key := 'ammo_type_' + IntToStr(i);
          ammoTypes.Add(Res_IniReadString('Types', PChar(key), ''));
        end;
      Res_CloseIni();
    end;
end;

procedure SaveAmmoTypes;   
var
  i : Integer;
  key : String;
begin        
  if ((ammoTypes = nil) or
      (ammoTypes.Count = 0)) then
    LoadAmmoTypes;

  Res_OpenIniFromBuffer(nil, 0);    
  Res_IniWriteInteger('Options', 'count', ammoTypes.Count);
  for i := 0 to ammoTypes.Count - 1 do
    begin
      key := 'ammo_type_' + IntToStr(i);
      Res_IniWriteString('Types', PChar(key), PChar(ammoTypes.Strings[i]));
    end;

  Res_SaveIniToFile(PChar(g_WaPath + '/' + AMMO_TYPES_FILE));
  Res_CloseIni();
end;

procedure InitSound(num: Integer);
begin
  if ((num < 0) or (num > High(soundsInMem))) then exit;

  with (soundsInMem[num]) do
    begin
      name      := '';
      precached := False;
      extension := '';
    end;
end;

procedure InitSounds;
var
  i : Integer;
begin
  for i := Low(soundsInMem) to High(soundsInMem) do
    InitSound(i);

  g_SoundsCount := 0;
end;

procedure LoadSoundPack;
var
  i : Integer;
begin
  InitSounds;

  if (not DirectoryExists(RES_PATH))  then exit;
  if (not DirectoryExists(g_SndPath)) then exit;

  // ЗВУКИ
  // читаем количество
  if (not Res_OpenIniFromFile(PChar(g_SndPath + '/sounds.cfg'))) then
    begin
      WriteLn('can not open sounds.cfg!!!');
      exit;
    end;
  g_SoundsCount := Res_IniReadInteger(PChar('Options'), PChar('count'), 0);
  Res_CloseIni();
  if (g_SoundsCount = 0) then exit;

  for i := 0 to g_SoundsCount - 1 do
    with (soundsInMem[i]) do
      begin
        // читаем инфу по звуку
        if (not Res_OpenIniFromFile(PChar(g_SndPath + '/sound_' + IntToStr(i) + '.cfg'))) then
          continue;

        name      := Res_IniReadString('Options', 'name',      PChar('Sound #' + IntToStr(i)));
        precached := Res_IniReadBool  ('Options', 'precached', False);          
        extension := Res_IniReadString('Options', 'extension', '');
        Res_CloseIni();
      end;
end;         

procedure SaveSound(num: Integer);
begin
  if ((num < 0) or (num > High(soundsInMem))) then exit;

  if (not DirectoryExists(RES_PATH))  then CreateDir(RES_PATH);
  if (not DirectoryExists(g_SndPath)) then CreateDir(g_SndPath);

  // записываем параметры
  with (soundsInMem[num]) do
    begin
      Res_OpenIniFromBuffer(nil, 0);
      Res_IniWriteString('Options', 'name',      PChar(name));
      Res_IniWriteBool  ('Options', 'precached', precached);
      Res_IniWriteString('Options', 'extension', PChar(extension));
      Res_SaveIniToFile(PChar(g_SndPath + '/sound_' + IntToStr(num) + '.cfg'));
      Res_CloseIni();
    end;
end;

procedure SaveSoundPack;
var
  i : Integer;
begin
  if (not DirectoryExists(RES_PATH))  then CreateDir(RES_PATH);
  if (not DirectoryExists(g_SndPath)) then CreateDir(g_SndPath);
  //Res_DeleteAllFilesInDir(PChar(g_SndPath), False);

  // записываем конфигурацию пака
  Res_OpenIniFromBuffer(nil, 0);
  Res_IniWriteInteger('Options', 'count', g_SoundsCount);
  Res_SaveIniToFile(PChar(g_SndPath + '/sounds.cfg'));
  Res_CloseIni();

  // формируем пак
  // оружие
  for i := 0 to g_SoundsCount - 1 do
    SaveSound(i);
end;

procedure DeleteSound(num: Integer);
var
  i, j : Integer;
begin        
  if ((num < 0) or (num > High(soundsInMem))) then exit;

  if (not DirectoryExists(RES_PATH))  then exit;
  if (not DirectoryExists(g_SndPath)) then exit;

  DeleteFile(g_SndPath + '/sound_' + IntToStr(num) + '.cfg');
  DeleteFile(g_SndPath + '/sound_' + IntToStr(num) + '.dat');

  InitSound(num);
end;

{ TMap }

constructor TMap.Create;
begin       
  inherited Create;
  Clear;
end;

destructor TMap.Destroy;
begin
  Clear;    
  inherited Destroy;
end;

procedure TMap.Clear;
begin
  name       := 'Level #0';
  floorColor := 0;
  ceilColor  := 0;
  fogColor   := 0;
  fogIntensity := 0.0;
  showFloor  := True;
  showCeil   := True;
  showFog    := False;
  showSky    := False;
  skyNumber  := -1;

  FillChar(floor[0], SizeOf(floor), 0);  
  FillChar(level[0], SizeOf(level), 0);
  FillChar(ceil [0], SizeOf(ceil ), 0);
end;

end.

