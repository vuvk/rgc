unit uMain;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  FileUtil,
  Forms,
  Controls,
  Graphics,
  Dialogs,
  Menus,
  StdCtrls,
  Buttons,
  ExtCtrls,
  ComCtrls,
  Grids,
  Imaging,
  Types,
  uContainer,
  uResManager,
  uDoorsKeysEditor,
  uWeaponsAmmoEditor, uDreadfulSpace;

type

  { TfrmMain }

  TfrmMain = class(TForm)
    MenuItem13: TMenuItem;
    MenuItem15: TMenuItem;
    SoundsMenu: TMenuItem;
    WeaponsAmmoMenu: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    WeaponsGrid: TDrawGrid;
    KeysGrid: TDrawGrid;
    AmmoGrid: TDrawGrid;
    LevelOfDrawRadioGroup : TRadioGroup;
    LevelsMenu: TMenuItem;
    MenuItem1: TMenuItem;
    DoorsKeysMenu: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    ProjectExitMenu: TMenuItem;
    ProjectNewMenu: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem14: TMenuItem;
    MenuItem5: TMenuItem;
    SkyboxesMenu: TMenuItem;
    MenuItem17: TMenuItem;
    MenuItem18: TMenuItem;
    ProjectMenu: TMenuItem;
    ProjectOpenMenu: TMenuItem;
    ProjectSaveMenu: TMenuItem;
    ProjectExportMenu: TMenuItem;
    MenuItem23: TMenuItem;
    ProjectCloseMenu: TMenuItem;
    MenuItem25: TMenuItem;
    PageControl1 : TPageControl;
    Splitter2: TSplitter;
    Splitter3: TSplitter;
    SpritesGrid : TDrawGrid;
    LevelTab : TTabSheet;
    SpritesTab : TTabSheet;
    DoorsKeysTab: TTabSheet;
    WeaponsAmmoTab: TTabSheet;
    TabSheet3 : TTabSheet;
    TabSheet4 : TTabSheet;
    Label1 : TLabel;
    LevelNumLabel : TLabel;
    SpritesMenu: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem12: TMenuItem;
    MainMenu1: TMainMenu;
    DoorsGrid: TDrawGrid;
    TexturesMenu: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    PaintBox: TPaintBox;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    ScrollBox: TScrollBox;
    ScrollBox1: TScrollBox;
    ShowGrid : TCheckBox;
    Splitter1: TSplitter;
    TexturesGrid : TDrawGrid;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    DrawFloorBtn: TToolButton;
    ZoomBar : TTrackBar;
    procedure AmmoGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure AmmoGridSelection(Sender: TObject; aCol, aRow: Integer);
    procedure DoorsGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure DoorsGridSelection(Sender: TObject; aCol, aRow: Integer);
    procedure DoorsKeysTabResize(Sender: TObject);
    procedure DrawFloorBtnClick(Sender: TObject);
    procedure FormDestroy(Sender : TObject);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure KeysGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure KeysGridSelection(Sender: TObject; aCol, aRow: Integer);
    procedure LevelTabContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure MenuItem10Click(Sender: TObject);
    procedure MenuItem13Click(Sender: TObject);
    procedure MenuItem14Click(Sender: TObject);
    procedure MenuItem15Click(Sender: TObject);
    procedure MenuItem17Click(Sender: TObject);
    procedure MenuItem18Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure Panel1Click(Sender: TObject);
    procedure ProjectExitMenuClick(Sender: TObject);
    procedure ProjectOpenMenuClick(Sender: TObject);
    procedure ProjectSaveMenuClick(Sender: TObject);
    procedure ProjectExportMenuClick(Sender: TObject);
    procedure ProjectCloseMenuClick(Sender: TObject);
    procedure PageControl1Change(Sender : TObject);
    procedure SpritesGridDrawCell(Sender : TObject; aCol, aRow : Integer;
      aRect : TRect; aState : TGridDrawState);
    procedure LevelTabResize(Sender : TObject);
    procedure SpritesGridSelection(Sender: TObject; aCol, aRow: Integer);
    procedure SpritesTabContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);
    procedure SpritesTabResize(Sender : TObject);
    procedure TexturesGridDrawCell(Sender : TObject; aCol, aRow : Integer;
      aRect : TRect; aState : TGridDrawState);
    procedure FormClose(Sender : TObject; var CloseAction : TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure MenuItem11Click(Sender : TObject);
    procedure MenuItem12Click(Sender: TObject);
    procedure DoorsKeysMenuClick(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure MenuItem7Click(Sender: TObject);
    procedure MenuItem8Click(Sender: TObject);
    procedure MenuItem9Click(Sender: TObject);
    procedure PaintBoxMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PaintBoxMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure PaintBoxMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PaintBoxMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure PaintBoxMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure PaintBoxPaint(Sender: TObject);
    procedure LevelOfDrawRadioGroupClick(Sender : TObject);
    procedure ScrollBoxSizeConstraintsChange(Sender : TObject);
    procedure ShowGridChange(Sender : TObject);
    procedure TexturesGridSelection(Sender: TObject; aCol, aRow: Integer);
    procedure TimerDrawerTimer(Sender: TObject);
    procedure WeaponsAmmoTabResize(Sender: TObject);
    procedure WeaponsGridClick(Sender: TObject);
    procedure WeaponsGridDrawCell(Sender: TObject; aCol, aRow: Integer;
      aRect: TRect; aState: TGridDrawState);
    procedure WeaponsGridSelection(Sender: TObject; aCol, aRow: Integer);
    procedure ZoomBarChange(Sender: TObject);
  private
    procedure TexturesGridUpdate;  
    procedure SpritesGridUpdate;
    procedure DoorsGridUpdate;
    procedure KeysGridUpdate;  
    procedure WeaponsGridUpdate;
    procedure AmmoGridUpdate;

    procedure CurMapPrepare;

    procedure EditorClear;      // подготавливает редактор к работе (удаляет старую информацию, блокирует "ненужные" кнопки)

  public

  end;

type
  TDrawRect = record
    isDraw  : Boolean;
    isLeft  : Boolean;
    isRight : Boolean;
    x0, y0,
    x1, y1 : Integer;
  end;

var
  frmMain: TfrmMain;
  // информация по рисованию выделяющих прямоугольников
  drawRect : TDrawRect =
  (
    isDraw : False;
    isLeft : False;
    isRight: False;
    x0: 0; y0: 0;
    x1: 0; y1: 0
  );

  // выбранный в данный момент элемент
  mapElement : TMapElement = 0;

  // рисовать пол, уровень или потолок
  levelOfDraw : Byte = 1; // 0 - пол
                          // 1 - уровень
                          // 2 - потолок

  // номер редактируемой карты
  curMapNum : Integer = 0;

  //spd_btn_cur  : TSpeedButton = nil;
  //spd_btn_prev : TSpeedButton = nil;

  isProjectOpened : Boolean = False;

  // строка для сбора клавиш - если введено spiced, то вывести пасхалку
  spicedStr : String = '';


const
  //TPK = 'TPK_';
  //MPK = 'MPK_';
  //SPK = 'SPK_';
  //SPK_INT = $5F4B5053;

  TOOLS_CELL_SIZE = 64;       // размер ячейки в палитре инструментов

implementation

uses
  uUtil,
  uMapChoose,
  uTexturesEditor,
  uSpritesEditor,
  uPleaseWait,
  uLevelConfiguration,
  uSkyboxEditor, uSoundsEditor;

{$R *.lfm}

{ TfrmMain }

procedure TfrmMain.PaintBoxPaint(Sender: TObject);
var
  i, j : Integer;
  x, y : Integer;
  cell : SmallInt;
  x0, y0, x1, y1 : Integer;
begin
  with (mapPack) do
    begin
      if (Length(maps) = 0) then exit;
      if (curMapNum > g_MapsCount) then exit;
      if (maps[curMapNum] = nil) then exit;
    end;

  with (PaintBox) do
    begin
      Canvas.Lock;

      // заливка
      Canvas.Pen.Color := clBlack;
      Canvas.Brush.Color := clBlack;
      Canvas.Rectangle(0, 0, Width, Height);

      // рисуем элементы карты
      for i := 0 to MAP_WIDTH - 1 do
        for j := 0 to MAP_HEIGHT - 1 do
          begin
            //////////////////////////////////////////////////////////////////////
            // сначала обрабатываем пол и потолок, если выбран режим их рисования  
            //////////////////////////////////////////////////////////////////////
            if ((levelOfDraw <> 1) or                                  // если уровень рисования не стены
                ((levelOfDraw = 1) and (DrawFloorBtn.Down))) then      // или стены, но нажато рисование пола
              begin     
                if ((levelOfDraw = 0) or (DrawFloorBtn.Down)) then
                  cell := mapPack.maps[curMapNum].floor[i, j] - 1;
                if (levelOfDraw = 2) then
                  cell := mapPack.maps[curMapNum].ceil [i, j] - 1;

                // в итоге получается, что -1 это ничего на карте
                if (cell >= 0) then
                  begin
                    // вычисляем позицию с учетом скролла
                    x := i * g_CellWidth;
                    y := j * g_CellHeight;

                    // если позиция за пределами области рисования, то идем дальше
                    if ((x + g_CellWidth  < 0) or (x > PaintBox.Width ) or
                        (y + g_CellHeight < 0) or (y > PaintBox.Height)) then
                      continue;

                    if (levelOfDraw <> 1) then
                      Canvas.Draw(x, y, cellBmp[cell])
                    else  // режим рисования стен и пола одновременно, видимо
                      Canvas.Draw(x, y, cellBmpShadowed[cell]);
                  end;
              end;
                      
            //////////////////////////////////////////////////////////////////////
            // теперь рисуем уровень в любом случае
            //////////////////////////////////////////////////////////////////////
            cell := mapPack.maps[curMapNum].level[i, j] - 1;
            // в итоге получается, что -1 это ничего на карте
            if (cell >= 0) then
              begin
                // вычисляем позицию с учетом скролла
                x := i * g_CellWidth;
                y := j * g_CellHeight;

                // если позиция за пределами области рисования, то идем дальше
                if ((x + g_CellWidth  < 0) or (x > PaintBox.Width ) or
                    (y + g_CellHeight < 0) or (y > PaintBox.Height)) then
                  continue;  

                // если режим рисования пола или потолка
                if (levelOfDraw <> 1) then
                  begin
                    if (cellBmpShadowed[cell] <> nil) then
                      Canvas.Draw(x, y, cellBmpShadowed[cell]);
                  end
                // рисование уровня
                else        
                  begin
                    if (cellBmp[cell] <> nil) then
                      Canvas.Draw(x, y, cellBmp[cell]);
                  end;
              end;
          end;

      // сетка
      if (ShowGrid.Checked) then
        begin
          Canvas.Pen.Color := clLtGray;
          for i := 0 to MAP_WIDTH - 1 do
            begin
              x := i * g_CellWidth;
              Canvas.Line(x, 0, x, Height);
            end;
          for j := 0 to MAP_HEIGHT - 1 do
            begin
              y := j * g_CellHeight;
              Canvas.Line(0, y, Width, y);
            end;
        end;

      // выделение ячейки при наведении
      if (not drawRect.isDraw and (mapElement <> 0)) then
        begin
          Canvas.Pen.Color := clLime;

          x0 := (drawRect.x1 div g_CellWidth ) * g_CellWidth;
          y0 := (drawRect.y1 div g_CellHeight) * g_CellHeight;
          x1 := x0 + g_CellWidth;
          y1 := y0 + g_CellHeight;

          // гор
          Canvas.Line(x0, y0, x1, y0);
          Canvas.Line(x0, y1, x1, y1);

          // верт
          Canvas.Line(x0, y0, x0, y1);
          Canvas.Line(x1, y0, x1, y1);
        end;

      // прямоугольник рисования
      if (drawRect.isDraw) then
        begin
          if (drawRect.isLeft) then
            Canvas.Pen.Color := clYellow;
          if (drawRect.isRight) then
            Canvas.Pen.Color := clRed;

          x0 := drawRect.x0;
          y0 := drawRect.y0;
          x1 := drawRect.x1;
          y1 := drawRect.y1;

          // гор
          Canvas.Line(x0, y0, x1, y0);
          Canvas.Line(x0, y1, x1, y1);

          // верт
          Canvas.Line(x0, y0, x0, y1);
          Canvas.Line(x1, y0, x1, y1);
        end;

      Canvas.Unlock;
    end;
end;

procedure TfrmMain.LevelOfDrawRadioGroupClick(Sender : TObject);
begin
  levelOfDraw := LevelOfDrawRadioGroup.ItemIndex;      
  PaintBox.Invalidate;
end;

procedure TfrmMain.ScrollBoxSizeConstraintsChange(Sender : TObject);
begin
  PaintBox.Invalidate;
end;

procedure TfrmMain.ShowGridChange(Sender : TObject);
begin 
  PaintBox.Invalidate;
end;

procedure TfrmMain.TimerDrawerTimer(Sender: TObject);
begin
  PaintBox.Invalidate;
end;

procedure TfrmMain.WeaponsAmmoTabResize(Sender: TObject);
begin
  WeaponsGridUpdate;
  AmmoGridUpdate;
end;

procedure TfrmMain.WeaponsGridClick(Sender: TObject);
begin

end;

procedure TfrmMain.WeaponsGridDrawCell(Sender: TObject; aCol, aRow: Integer;
  aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_WeaponsCount = 0) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (pos > g_WeaponsCount - 1) then
    begin
      TDrawGrid(Sender).Canvas.Pen.Color   := clWhite;
      TDrawGrid(Sender).Canvas.Brush.Color := clWhite;
      TDrawGrid(Sender).Canvas.Rectangle(aRect);
      exit;
    end;

  if (weapItemsInMem[pos].framesCount = 0) then exit;
  bmp := weapItemsInMem[pos].frames[0];
  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmMain.WeaponsGridSelection(Sender: TObject; aCol, aRow: Integer);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  if (g_WeaponsCount < 1) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (pos < 0) then
    pos := 0;
  if (pos >= g_WeaponsCount) then
    begin
      pos := g_WeaponsCount - 1;
      x := pos mod w;
      y := pos div w;
      TDrawGrid(Sender).Col := x;
      TDrawGrid(Sender).Row := y;
      exit;
    end;
  if (weapItemsInMem[pos].framesCount = 0) then exit;
  bmp := weapItemsInMem[pos].frames[0];

  if (bmp <> nil) then
    mapElement := pos + 1 + TEXTURES_MAX_COUNT +
                            SPRITES_MAX_COUNT  +
                            DOORS_MAX_COUNT    +
                            KEYS_MAX_COUNT
  else
    mapElement := 0;

  TexturesGrid.Options := [goAlwaysShowEditor, goSmoothScroll];
  SpritesGrid.Options  := [goAlwaysShowEditor, goSmoothScroll];
  DoorsGrid.Options    := [goAlwaysShowEditor, goSmoothScroll];
  KeysGrid.Options     := [goAlwaysShowEditor, goSmoothScroll]; 
  WeaponsGrid.Options  := [goAlwaysShowEditor, goSmoothScroll, goDrawFocusSelected];
  AmmoGrid.Options     := [goAlwaysShowEditor, goSmoothScroll];

  // спрайты можно рисовать только на уровне стен
  LevelOfDrawRadioGroup.ItemIndex := 1;
  LevelOfDrawRadioGroup.Enabled := False;
end;

procedure TfrmMain.ZoomBarChange(Sender: TObject);
begin
  case (ZoomBar.Position) of
    0 : Label1.Caption := 'Scale Map ( 25%)';
    1 : Label1.Caption := 'Scale Map ( 50%)';
    2 : Label1.Caption := 'Scale Map (100%)';
    3 : Label1.Caption := 'Scale Map (200%)';
    4 : Label1.Caption := 'Scale Map (400%)';
  end;

  g_CellWidth  := 8 shl ZoomBar.Position;
  g_CellHeight := 8 shl ZoomBar.Position;

  LoadCellTextures;

  PaintBox.Width  := g_CellWidth  * MAP_WIDTH;
  PaintBox.Height := g_CellHeight * MAP_HEIGHT;  
  PaintBox.Invalidate;
end;

procedure TfrmMain.TexturesGridUpdate;
var
  cnt : Integer;
  i : Integer;
begin
  TexturesGrid.ColCount := 0;
  TexturesGrid.RowCount := 0;
  TexturesGrid.Invalidate;

  if (g_TexturesCount < 1) then exit;

  cnt := LevelTab.Width div TOOLS_CELL_SIZE;
  if (cnt = 0) then
    cnt := 1;
  if (cnt > g_TexturesCount) then
    cnt := g_TexturesCount;

  // формируем грид с текстурами
  TexturesGrid.ColCount := cnt;
  TexturesGrid.RowCount := g_TexturesCount div TexturesGrid.ColCount;

  if (g_TexturesCount mod TexturesGrid.ColCount > 0) then
    TexturesGrid.RowCount := TexturesGrid.RowCount + 1;

  for i := 0 to TexturesGrid.ColCount - 1 do
    TexturesGrid.ColWidths [i] := 64;
  for i := 0 to TexturesGrid.RowCount - 1 do
    TexturesGrid.RowHeights[i] := 64;
  TexturesGrid.Invalidate;
end;

procedure TfrmMain.SpritesGridUpdate;
var
  cnt : Integer;
  i : Integer;
begin
  {
  cnt := SpritesTab.Width div TOOLS_CELL_SIZE;
  if (cnt = 0) then cnt := 1;
  if (cnt > g_SpritesCount) then cnt := g_SpritesCount;

  // формируем грид с текстурами
  SpritesGrid.ColCount := cnt;
  SpritesGrid.RowCount := g_SpritesCount div SpritesGrid.ColCount;

  if (g_SpritesCount mod SpritesGrid.ColCount > 0) then
    SpritesGrid.RowCount := SpritesGrid.RowCount + 1;

  for i := 0 to SpritesGrid.ColCount - 1 do
    SpritesGrid.ColWidths [i] := 64;
  for i := 0 to SpritesGrid.RowCount - 1 do
    SpritesGrid.RowHeights[i] := 64;
  SpritesGrid.Invalidate;
  }

  SpritesGrid.ColCount := 0;
  SpritesGrid.RowCount := 0;
  SpritesGrid.Invalidate;

  if (g_SpritesCount < 1) then exit;

  cnt := SpritesTab.Width div TOOLS_CELL_SIZE;
  if (cnt = 0) then
    cnt := 1;
  if (cnt > g_SpritesCount) then
    cnt := g_SpritesCount;

  // формируем грид с текстурами
  SpritesGrid.ColCount := cnt;
  SpritesGrid.RowCount := g_SpritesCount div SpritesGrid.ColCount;

  if (g_SpritesCount mod SpritesGrid.ColCount > 0) then
    SpritesGrid.RowCount := SpritesGrid.RowCount + 1;

  for i := 0 to SpritesGrid.ColCount - 1 do
    SpritesGrid.ColWidths [i] := 64;
  for i := 0 to SpritesGrid.RowCount - 1 do
    SpritesGrid.RowHeights[i] := 64;
  SpritesGrid.Invalidate;
end;

procedure TfrmMain.DoorsGridUpdate;
var
  cnt : Integer;
  i : Integer;
begin        
  DoorsGrid.ColCount := 0;
  DoorsGrid.RowCount := 0;
  DoorsGrid.Invalidate;

  if (g_DoorsCount < 1) then exit;

  cnt := DoorsKeysTab.Width div TOOLS_CELL_SIZE;
  if (cnt = 0) then
    cnt := 1;
  if (cnt > g_DoorsCount) then
    cnt := g_DoorsCount;

  // формируем грид с текстурами
  DoorsGrid.ColCount := cnt;
  DoorsGrid.RowCount := g_DoorsCount div DoorsGrid.ColCount;

  if (g_DoorsCount mod DoorsGrid.ColCount > 0) then
    DoorsGrid.RowCount := DoorsGrid.RowCount + 1;

  for i := 0 to DoorsGrid.ColCount - 1 do
    DoorsGrid.ColWidths [i] := 64;
  for i := 0 to DoorsGrid.RowCount - 1 do
    DoorsGrid.RowHeights[i] := 64;
  DoorsGrid.Invalidate;
end;

procedure TfrmMain.KeysGridUpdate;
var
  cnt : Integer;
  i : Integer;
begin
  KeysGrid.ColCount := 0;
  KeysGrid.RowCount := 0;
  KeysGrid.Invalidate;

  if (g_KeysCount < 1) then exit;

  cnt := DoorsKeysTab.Width div TOOLS_CELL_SIZE;
  if (cnt = 0) then
    cnt := 1;
  if (cnt > g_KeysCount) then
    cnt := g_KeysCount;

  // формируем грид с текстурами
  KeysGrid.ColCount := cnt;
  KeysGrid.RowCount := g_KeysCount div KeysGrid.ColCount;

  if (g_KeysCount mod KeysGrid.ColCount > 0) then
    KeysGrid.RowCount := KeysGrid.RowCount + 1;

  for i := 0 to KeysGrid.ColCount - 1 do
    KeysGrid.ColWidths [i] := 64;
  for i := 0 to KeysGrid.RowCount - 1 do
    KeysGrid.RowHeights[i] := 64;
  KeysGrid.Invalidate;
end;

procedure TfrmMain.WeaponsGridUpdate;
var
  cnt : Integer;
  i : Integer;
begin
  WeaponsGrid.ColCount := 0;
  WeaponsGrid.RowCount := 0;
  WeaponsGrid.Invalidate;

  if (g_WeaponsCount < 1) then exit;

  cnt := WeaponsAmmoTab.Width div TOOLS_CELL_SIZE;
  if (cnt = 0) then
    cnt := 1;
  if (cnt > g_WeaponsCount) then
    cnt := g_WeaponsCount;

  // формируем грид с текстурами
  WeaponsGrid.ColCount := cnt;
  WeaponsGrid.RowCount := g_WeaponsCount div WeaponsGrid.ColCount;

  if (g_WeaponsCount mod WeaponsGrid.ColCount > 0) then
    WeaponsGrid.RowCount := WeaponsGrid.RowCount + 1;

  for i := 0 to WeaponsGrid.ColCount - 1 do
    WeaponsGrid.ColWidths [i] := 64;
  for i := 0 to WeaponsGrid.RowCount - 1 do
    WeaponsGrid.RowHeights[i] := 64;
  WeaponsGrid.Invalidate;
end;

procedure TfrmMain.AmmoGridUpdate;
var
  cnt : Integer;
  i : Integer;
begin
  AmmoGrid.ColCount := 0;
  AmmoGrid.RowCount := 0;
  AmmoGrid.Invalidate;

  if (g_AmmoCount < 1) then exit;

  cnt := WeaponsAmmoTab.Width div TOOLS_CELL_SIZE;
  if (cnt = 0) then
    cnt := 1;
  if (cnt > g_AmmoCount) then
    cnt := g_AmmoCount;

  // формируем грид с текстурами
  AmmoGrid.ColCount := cnt;
  AmmoGrid.RowCount := g_AmmoCount div AmmoGrid.ColCount;

  if (g_AmmoCount mod AmmoGrid.ColCount > 0) then
    AmmoGrid.RowCount := AmmoGrid.RowCount + 1;

  for i := 0 to AmmoGrid.ColCount - 1 do
    AmmoGrid.ColWidths [i] := 64;
  for i := 0 to AmmoGrid.RowCount - 1 do
    AmmoGrid.RowHeights[i] := 64;
  AmmoGrid.Invalidate;
end;

procedure TfrmMain.CurMapPrepare;
begin               
  if (curMapNum >= g_MapsCount) then
    curMapNum := g_MapsCount - 1;

  CheckMap(curMapNum);
  LevelNumLabel.Caption := 'Current Level : ' + IntToStr(curMapNum + 1);
end;

procedure TfrmMain.EditorClear;
var i : Integer;
begin
  isProjectOpened := False;

  // грязный хак от утечки памяти
  for i := 0 to g_MapsCount - 1 do
    DeleteMap(i);

  g_TexturesCount := 0;
  g_SpritesCount  := 0;
  g_MapsCount     := 1;
  g_SkyboxesCount := 0;
  g_DoorsCount    := 0;
  g_KeysCount     := 0;
  g_WeaponsCount  := 0;
  g_BulletsCount  := 0;
  g_AmmoCount     := 0;
  g_SoundsCount   := 0;

  InitMapPack;
  InitTextures;
  InitSprites;
  InitCellTextures;
  InitDoorsKeys;    
  InitWeaponsAmmo;   
  InitBullets;
  InitSounds;

  mapElement := 0;
  curMapNum := 0;

  // делаем недоступными подменю
  ProjectSaveMenu.Enabled   := False;
  ProjectExportMenu.Enabled := False;
  ProjectCloseMenu.Enabled  := False;

  TexturesMenu.Enabled  := False;
  SpritesMenu.Enabled   := False;
  LevelsMenu.Enabled    := False;
  SkyboxesMenu.Enabled  := False;
  DoorsKeysMenu.Enabled := False;  
  WeaponsAmmoMenu.Enabled := False;
  SoundsMenu.Enabled    := False;

  TexturesGridUpdate;
  SpritesGridUpdate;    
  DoorsGridUpdate;
  KeysGridUpdate;
  WeaponsGridUpdate;
  AmmoGridUpdate;

  PaintBoxPaint(Self);
end;

procedure TfrmMain.FormCreate(Sender: TObject);
{var
  str : array [0..4] of Byte = (0,0,0,0,0);
  int : Int32 = SPK_INT;

  i : Integer; }
begin
  DecimalSeparator := '.';

  PaintBox.Width  := MAP_WIDTH  * g_CellWidth;
  PaintBox.Height := MAP_HEIGHT * g_CellHeight;

  DoubleBuffered := True;
  PaintBox.Parent.DoubleBuffered := True;     
  TexturesGrid.DoubleBuffered := True;
  SpritesGrid.DoubleBuffered  := True;
  DoorsGrid.DoubleBuffered    := True;
  KeysGrid.DoubleBuffered     := True;   
  WeaponsGrid.DoubleBuffered  := True;
  AmmoGrid.DoubleBuffered     := True;

  PageControl1.ActivePageIndex := 0;
                
  TexturesGrid.Options := [goAlwaysShowEditor, goDrawFocusSelected, goSmoothScroll]; 
  SpritesGrid.Options  := [goAlwaysShowEditor, goSmoothScroll];  
  DoorsGrid.Options    := [goAlwaysShowEditor, goSmoothScroll];
  KeysGrid.Options     := [goAlwaysShowEditor, goSmoothScroll];   
  WeaponsGrid.Options  := [goAlwaysShowEditor, goSmoothScroll];
  AmmoGrid.Options     := [goAlwaysShowEditor, goSmoothScroll];

  EditorClear;
end;

procedure TfrmMain.MenuItem11Click(Sender : TObject);
begin          
  if (MessageDlg('Warning', 'Do you really want to clear all textures for sprites and frames?',
                 mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    begin
      InitSprites;

      SaveSpritePack;
      LoadCellTextures;   

      CheckMaps;
    end;
end;

procedure TfrmMain.MenuItem12Click(Sender: TObject);
var
  frmSpritesEditor : TfrmSpritesEditor;
begin
  frmSpritesEditor := TfrmSpritesEditor.Create(Self);
  with frmSpritesEditor do
    try
      ShowModal;
    finally
      Destroy;
    end;

  CheckMaps;   
  LoadCellTextures;
  SpritesGridUpdate;
end;

procedure TfrmMain.DoorsKeysMenuClick(Sender: TObject);
begin
end;

procedure TfrmMain.MenuItem3Click(Sender: TObject);
begin     
  if (MessageDlg('Warning', 'Do you really want to clear all textures for doors and keys?',
                 mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    begin
      InitDoorsKeys;

      SaveDoorKeyPack;
      LoadCellTextures;

      CheckMaps;
    end;
end;

procedure TfrmMain.MenuItem4Click(Sender: TObject);
var
  frmDoorsKeysEditor : TfrmDoorsKeysEditor = nil;
begin
  frmDoorsKeysEditor := TfrmDoorsKeysEditor.Create(Self);
  with frmDoorsKeysEditor do
    try
      ShowModal;
    finally
      Destroy;
    end;

  CheckMaps;
  LoadCellTextures;
  DoorsGridUpdate;
  KeysGridUpdate;
end;

procedure TfrmMain.TexturesGridDrawCell(Sender : TObject; aCol, aRow : Integer;
  aRect : TRect; aState : TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_TexturesCount = 0) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (pos > g_TexturesCount - 1) then
    begin
      TDrawGrid(Sender).Canvas.Pen.Color   := clWhite;
      TDrawGrid(Sender).Canvas.Brush.Color := clWhite;
      TDrawGrid(Sender).Canvas.Rectangle(aRect);
      exit;
    end;

  if (texturesInMem[pos].framesCount = 0) then exit;
  bmp := texturesInMem[pos].frames[0];
  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmMain.TexturesGridSelection(Sender: TObject; aCol, aRow: Integer);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  if (g_TexturesCount < 1) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (pos < 0) then
    pos := 0;
  if (pos >= g_TexturesCount) then
    begin
      pos := g_TexturesCount - 1;
      x := pos mod w;
      y := pos div w;
      TDrawGrid(Sender).Col := x;                       
      TDrawGrid(Sender).Row := y;
      exit;
    end;
  if (texturesInMem[pos].framesCount = 0) then exit;
  bmp := texturesInMem[pos].frames[0];

  if (bmp <> nil) then
    mapElement := pos + 1
  else
    mapElement := 0;

  TexturesGrid.Options := [goAlwaysShowEditor, goSmoothScroll, goDrawFocusSelected];
  SpritesGrid.Options  := [goAlwaysShowEditor, goSmoothScroll];        
  DoorsGrid.Options    := [goAlwaysShowEditor, goSmoothScroll];
  KeysGrid.Options     := [goAlwaysShowEditor, goSmoothScroll];

  // разрешить изменение уровня рисования
  LevelOfDrawRadioGroup.Enabled := True;

  TDrawGrid(Sender).Invalidate;
end;

procedure TfrmMain.SpritesGridDrawCell(Sender : TObject; aCol, aRow : Integer;
  aRect : TRect; aState : TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_SpritesCount = 0) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (pos > g_SpritesCount - 1) then
    begin
      TDrawGrid(Sender).Canvas.Pen.Color   := clWhite;
      TDrawGrid(Sender).Canvas.Brush.Color := clWhite;
      TDrawGrid(Sender).Canvas.Rectangle(aRect);
      exit;
    end;

  if (spritesInMem[pos].framesCount = 0) then exit;
  bmp := spritesInMem[pos].frames[0];
  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmMain.SpritesGridSelection(Sender: TObject; aCol, aRow: Integer);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  if (g_SpritesCount < 1) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (pos < 0) then
    pos := 0;
  if (pos >= g_SpritesCount) then
    begin
      pos := g_SpritesCount - 1;
      x := pos mod w;
      y := pos div w;
      TDrawGrid(Sender).Col := x;
      TDrawGrid(Sender).Row := y;
      exit;
    end;
  if (spritesInMem[pos].framesCount = 0) then exit;
  bmp := spritesInMem[pos].frames[0];

  if (bmp <> nil) then
    mapElement := pos + 1 + TEXTURES_MAX_COUNT
  else
    mapElement := 0;

  TexturesGrid.Options := [goAlwaysShowEditor, goSmoothScroll];
  SpritesGrid.Options  := [goAlwaysShowEditor, goSmoothScroll, goDrawFocusSelected];
  DoorsGrid.Options    := [goAlwaysShowEditor, goSmoothScroll];
  KeysGrid.Options     := [goAlwaysShowEditor, goSmoothScroll];

  // спрайты можно рисовать только на уровне стен
  LevelOfDrawRadioGroup.ItemIndex := 1;
  LevelOfDrawRadioGroup.Enabled := False;
end;

procedure TfrmMain.SpritesTabContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin

end;

procedure TfrmMain.FormDestroy(Sender : TObject);
var
  i : Integer;
begin
  with (mapPack) do
    for i := 0 to High(maps) do
      if (maps[i] <> nil) then
        maps[i].Destroy;
end;


procedure TfrmMain.FormKeyPress(Sender: TObject; var Key: char);
begin
  if ((Key = 's') or
      (Key = 'p') or
      (Key = 'i') or
      (Key = 'c') or
      (Key = 'e') or
      (Key = 'd')) then
    spicedStr += Key
  else
    spicedStr := '';

  if (Length(spicedStr) > 6) then
    spicedStr := '';

  if (spicedStr = 'spiced') then
    begin               
      with (TfrmDreadfulSpace.Create(Self)) do
        try
          ShowModal;
        finally
          Free;
        end; 
      spicedStr := '';
    end;
end;

procedure TfrmMain.FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState
  );
begin

end;

procedure TfrmMain.KeysGridDrawCell(Sender: TObject; aCol, aRow: Integer;
  aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_KeysCount = 0) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (pos > g_KeysCount - 1) then
    begin
      TDrawGrid(Sender).Canvas.Pen.Color   := clWhite;
      TDrawGrid(Sender).Canvas.Brush.Color := clWhite;
      TDrawGrid(Sender).Canvas.Rectangle(aRect);
      exit;
    end;

  if (keysInMem[pos].framesCount = 0) then exit;
  bmp := keysInMem[pos].frames[0];
  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmMain.KeysGridSelection(Sender: TObject; aCol, aRow: Integer);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  if (g_KeysCount < 1) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (pos < 0) then
    pos := 0;
  if (pos >= g_KeysCount) then
    begin
      pos := g_KeysCount - 1;
      x := pos mod w;
      y := pos div w;
      TDrawGrid(Sender).Col := x;
      TDrawGrid(Sender).Row := y;
      exit;
    end;
  if (keysInMem[pos].framesCount = 0) then exit;
  bmp := keysInMem[pos].frames[0];

  if (bmp <> nil) then
    mapElement := pos + 1 + TEXTURES_MAX_COUNT +
                            SPRITES_MAX_COUNT  +
                            DOORS_MAX_COUNT
  else
    mapElement := 0;

  TexturesGrid.Options := [goAlwaysShowEditor, goSmoothScroll];
  SpritesGrid.Options  := [goAlwaysShowEditor, goSmoothScroll];
  DoorsGrid.Options    := [goAlwaysShowEditor, goSmoothScroll];
  KeysGrid.Options     := [goAlwaysShowEditor, goSmoothScroll, goDrawFocusSelected];

  // спрайты можно рисовать только на уровне стен
  LevelOfDrawRadioGroup.ItemIndex := 1;
  LevelOfDrawRadioGroup.Enabled := False;
end;

procedure TfrmMain.LevelTabContextPopup(Sender: TObject; MousePos: TPoint;
  var Handled: Boolean);
begin

end;

procedure TfrmMain.DoorsKeysTabResize(Sender: TObject);
begin
  DoorsGridUpdate;
  KeysGridUpdate;
end;

procedure TfrmMain.DrawFloorBtnClick(Sender: TObject);
begin
  PaintBox.Invalidate;
end;

procedure TfrmMain.DoorsGridDrawCell(Sender: TObject; aCol, aRow: Integer;
  aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_DoorsCount = 0) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (pos > g_DoorsCount - 1) then
    begin
      TDrawGrid(Sender).Canvas.Pen.Color   := clWhite;
      TDrawGrid(Sender).Canvas.Brush.Color := clWhite;
      TDrawGrid(Sender).Canvas.Rectangle(aRect);
      exit;
    end;

  if (doorsInMem[pos].framesCount = 0) then exit;
  bmp := doorsInMem[pos].frames[0];
  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmMain.AmmoGridDrawCell(Sender: TObject; aCol, aRow: Integer;
  aRect: TRect; aState: TGridDrawState);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
  rect : TRect;
begin
  if (g_AmmoCount = 0) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (pos > g_AmmoCount - 1) then
    begin
      TDrawGrid(Sender).Canvas.Pen.Color   := clWhite;
      TDrawGrid(Sender).Canvas.Brush.Color := clWhite;
      TDrawGrid(Sender).Canvas.Rectangle(aRect);
      exit;
    end;

  if (ammoInMem[pos].framesCount = 0) then exit;
  bmp := ammoInMem[pos].frames[0];
  if (bmp <> nil) then
    begin
      rect.Left   := aRect.Left   + 3;
      rect.Top    := aRect.Top    + 3;
      rect.Width  := aRect.Width  - 6;
      rect.Height := aRect.Height - 6;
      TDrawGrid(Sender).Canvas.StretchDraw(rect, bmp);
    end;
end;

procedure TfrmMain.AmmoGridSelection(Sender: TObject; aCol, aRow: Integer);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  if (g_AmmoCount < 1) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (pos < 0) then
    pos := 0;
  if (pos >= g_AmmoCount) then
    begin
      pos := g_AmmoCount - 1;
      x := pos mod w;
      y := pos div w;
      TDrawGrid(Sender).Col := x;
      TDrawGrid(Sender).Row := y;
      exit;
    end;
  if (ammoInMem[pos].framesCount = 0) then exit;
  bmp := ammoInMem[pos].frames[0];

  if (bmp <> nil) then
    mapElement := pos + 1 + TEXTURES_MAX_COUNT +
                            SPRITES_MAX_COUNT  +
                            DOORS_MAX_COUNT    +
                            KEYS_MAX_COUNT     +
                            WEAPONS_MAX_COUNT
  else
    mapElement := 0;

  TexturesGrid.Options := [goAlwaysShowEditor, goSmoothScroll];
  SpritesGrid.Options  := [goAlwaysShowEditor, goSmoothScroll];
  DoorsGrid.Options    := [goAlwaysShowEditor, goSmoothScroll];
  KeysGrid.Options     := [goAlwaysShowEditor, goSmoothScroll];
  WeaponsGrid.Options  := [goAlwaysShowEditor, goSmoothScroll];
  AmmoGrid.Options     := [goAlwaysShowEditor, goSmoothScroll, goDrawFocusSelected];

  // спрайты можно рисовать только на уровне стен
  LevelOfDrawRadioGroup.ItemIndex := 1;
  LevelOfDrawRadioGroup.Enabled := False;
end;

procedure TfrmMain.DoorsGridSelection(Sender: TObject; aCol, aRow: Integer);
var
  x, y : Integer;
  w : Integer;
  pos : Integer;
  bmp : TBitmap;
begin
  if (g_DoorsCount < 1) then exit;

  x := aCol;
  y := aRow;
  w := TDrawGrid(Sender).ColCount;

  pos := y * w + x;
  if (pos < 0) then
    pos := 0;
  if (pos >= g_DoorsCount) then
    begin
      pos := g_DoorsCount - 1;
      x := pos mod w;
      y := pos div w;
      TDrawGrid(Sender).Col := x;
      TDrawGrid(Sender).Row := y;
      exit;
    end;
  if (doorsInMem[pos].framesCount = 0) then exit;
  bmp := doorsInMem[pos].frames[0];

  if (bmp <> nil) then
    mapElement := pos + 1 + TEXTURES_MAX_COUNT +
                            SPRITES_MAX_COUNT
  else
    mapElement := 0;

  TexturesGrid.Options := [goAlwaysShowEditor, goSmoothScroll];
  SpritesGrid.Options  := [goAlwaysShowEditor, goSmoothScroll];
  DoorsGrid.Options    := [goAlwaysShowEditor, goSmoothScroll, goDrawFocusSelected];
  KeysGrid.Options     := [goAlwaysShowEditor, goSmoothScroll];

  // спрайты можно рисовать только на уровне стен
  LevelOfDrawRadioGroup.ItemIndex := 1;
  LevelOfDrawRadioGroup.Enabled := False;
end;

procedure TfrmMain.MenuItem10Click(Sender: TObject);
begin
  if (MessageDlg('Confirm', 'Really clear current level?',
                 mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    begin
      mapPack.maps[curMapNum].Clear;
      PaintBox.Invalidate;
    end;
end;

procedure TfrmMain.MenuItem13Click(Sender: TObject);
begin
  if (MessageDlg('Warning', 'Do you really want to clear all sounds?',
                 mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    begin          
      Res_DeleteAllFilesInDir(PChar(g_SndPath), False);
      InitSounds;
      SaveSoundPack;
    end;
end;

procedure TfrmMain.MenuItem14Click(Sender: TObject);
var
  frmLevelConfiguration : TfrmLevelConfiguration;
begin
  frmLevelConfiguration := TfrmLevelConfiguration.Create(Self);
  with (frmLevelConfiguration) do
    try
      ShowModal;
    finally
      Destroy;
    end;

  CurMapPrepare;

  PaintBox.Invalidate;
end;

procedure TfrmMain.MenuItem15Click(Sender: TObject);
var
  frmSoundsEditor : TfrmSoundsEditor;
begin
  frmSoundsEditor := TfrmSoundsEditor.Create(Self);
  with frmSoundsEditor do
    try
      ShowModal;
    finally
      Destroy;
    end;
end;

procedure TfrmMain.MenuItem17Click(Sender: TObject);
begin
  if (MessageDlg('Warning', 'Do you really want to clear all skyboxes?',
                 mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    begin
      InitSkyboxes;
      CheckMaps;
    end;
end;

procedure TfrmMain.MenuItem18Click(Sender: TObject);
var
  frmSkyboxEditor : TfrmSkyboxEditor;
begin
  frmSkyboxEditor := TfrmSkyboxEditor.Create(Self);
  with frmSkyboxEditor do
    try
      ShowModal;
    finally
      Destroy;
    end;

  CheckMaps;
end;

procedure TfrmMain.MenuItem5Click(Sender: TObject);
var
  frmMapChoose : TfrmMapChoose;
begin
  frmMapChoose := TfrmMapChoose.Create(Self);
  with frmMapChoose do
    try
      ShowModal;
    finally
      Destroy;
    end;

  CurMapPrepare;

  PaintBox.Invalidate;
end;

procedure TfrmMain.Panel1Click(Sender: TObject);
begin

end;

procedure TfrmMain.ProjectExitMenuClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.ProjectOpenMenuClick(Sender: TObject);
var
  frmPleaseWait : TfrmPleaseWait;
begin
  if (isProjectOpened) then
    begin
      MessageDlg('Error', 'Project is already opened! Please close them before load new!',
                 mtError, [mbOK], 0);
      exit;
    end;

  frmPleaseWait := TfrmPleaseWait.Create(Self);
  frmPleaseWait.Show;
  frmPleaseWait.Repaint;

  EditorClear;

  // обновляем пути
  UpdatePaths('./');

  //InitMapPack;

  LoadTexturePack;
  LoadSpritePack;
  LoadDoorKeyPack;
  LoadWeaponAmmoPack;  
  LoadBulletPack;
  LoadCellTextures;
  LoadMapPack;
  LoadSkyboxCount;
  LoadSoundPack;

  CurMapPrepare;

  TexturesGridUpdate;
  SpritesGridUpdate;
  DoorsGridUpdate;
  KeysGridUpdate;
  WeaponsGridUpdate;
  AmmoGridUpdate;

  if (g_TexturesCount > 0) then
    mapElement := 1;

  // делаем доступными подменю
  ProjectSaveMenu.Enabled   := True;
  ProjectExportMenu.Enabled := True;
  ProjectCloseMenu.Enabled  := True;

  TexturesMenu.Enabled  := True;
  SpritesMenu.Enabled   := True;
  LevelsMenu.Enabled    := True;
  SkyboxesMenu.Enabled  := True;
  DoorsKeysMenu.Enabled := True;
  WeaponsAmmoMenu.Enabled := True;
  SoundsMenu.Enabled    := True;

  frmPleaseWait.Destroy;

  isProjectOpened := True;

  PaintBox.Invalidate;
end;

procedure TfrmMain.ProjectSaveMenuClick(Sender: TObject);
var
  frmPleaseWait : TfrmPleaseWait;
begin
  frmPleaseWait := TfrmPleaseWait.Create(Self);
  frmPleaseWait.Show;
  frmPleaseWait.Repaint;

  SaveTexturePack;
  SaveSpritePack;
  SaveDoorKeyPack;
  SaveMapPack;
  SaveWeaponAmmoPack;
  SaveSoundPack;

  frmPleaseWait.Destroy;
end;

procedure TfrmMain.ProjectExportMenuClick(Sender: TObject);
const
  PACK_NAME = 'resources.pk3';             
var
  frmPleaseWait : TfrmPleaseWait;
begin
  if (FileExists(PACK_NAME)) then
    DeleteFile(PACK_NAME);

  if (not Res_OpenZipForWrite(PACK_NAME)) then
    begin
      MessageDlg('Error!', 'Error when openning ZIP for write!', mtError, [mbOK], 0);
      exit;
    end;

  ProjectSaveMenuClick(Self);

  frmPleaseWait := TfrmPleaseWait.Create(Self);
  frmPleaseWait.Show;
  frmPleaseWait.Repaint;

  Res_PackAllFilesFromDirToZip(PChar(g_TexPath ), DEFAULT_COMPRESSION_LEVEL);
  Res_PackAllFilesFromDirToZip(PChar(g_SprPath ), DEFAULT_COMPRESSION_LEVEL);
  Res_PackAllFilesFromDirToZip(PChar(g_DkPath  ), DEFAULT_COMPRESSION_LEVEL);
  Res_PackAllFilesFromDirToZip(PChar(g_WaPath  ), DEFAULT_COMPRESSION_LEVEL);
  Res_PackAllFilesFromDirToZip(PChar(g_WeapPath), DEFAULT_COMPRESSION_LEVEL);    
  Res_PackAllFilesFromDirToZip(PChar(g_BulPath ), DEFAULT_COMPRESSION_LEVEL);
  Res_PackAllFilesFromDirToZip(PChar(g_MapPath ), DEFAULT_COMPRESSION_LEVEL);
  Res_PackAllFilesFromDirToZip(PChar(g_SkyPath ), DEFAULT_COMPRESSION_LEVEL);  
  Res_PackAllFilesFromDirToZip(PChar(g_SndPath ), DEFAULT_COMPRESSION_LEVEL);

  Res_CloseZipAfterWrite();

  frmPleaseWait.Destroy;

  MessageDlg('Success!', 'Resources have been exported!', mtInformation, [mbOK], 0);
end;

procedure TfrmMain.ProjectCloseMenuClick(Sender: TObject);
begin
  if (MessageDlg('Confirmation', 'Save all changes in project?',
                 mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    ProjectSaveMenuClick(Self);

  EditorClear;
end;

procedure TfrmMain.PageControl1Change(Sender : TObject);
begin

end;

procedure TfrmMain.LevelTabResize(Sender : TObject);
begin
  TexturesGridUpdate;
end;

procedure TfrmMain.SpritesTabResize(Sender : TObject);
begin
  SpritesGridUpdate;
end;

procedure TfrmMain.FormClose(Sender : TObject; var CloseAction : TCloseAction);
begin
  if (MessageDlg('Confirmation', 'Exit from program?',
                 mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
    CloseAction := caNone
  else
    begin
      if (isProjectOpened) then
        ProjectCloseMenuClick(Self);
      {
      InitTextures;
      InitSprites;
      InitMapPack;
      }           
      DeleteMap(0);
      SetLength(texturesInMem,  0);
      SetLength(spritesInMem,   0);
      SetLength(doorsInMem,     0);
      SetLength(keysInMem,      0);
      SetLength(weapItemsInMem, 0);
      SetLength(bulletsInMem,   0);
      SetLength(mapPack.maps,   0); 
      SetLength(soundsInMem,    0);
    end;
end;

procedure TfrmMain.MenuItem6Click(Sender: TObject);
begin
  if (MessageDlg('Warning', 'Do you really want to clear all textures of level?',
                 mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    begin
      InitTextures;
      g_TexturesCount := 0;

      SaveTexturePack;
      LoadCellTextures;

      CheckMaps;
      TexturesGridUpdate;
    end;
end;

procedure TfrmMain.MenuItem7Click(Sender: TObject);
var
  frmTexturesEditor : TfrmTexturesEditor;
begin  
  frmTexturesEditor := TfrmTexturesEditor.Create(Self);
  with frmTexturesEditor do
    try
      ShowModal;
    finally
      Destroy;
    end;
                  
  CheckMaps;
  LoadCellTextures;
  TexturesGridUpdate;
end;

procedure TfrmMain.MenuItem8Click(Sender: TObject);     
begin
  if (MessageDlg('Warning', 'Do you really want to clear all textures for weapons, bullets and ammo?',
                 mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    begin
      InitBullets;
      InitWeaponsAmmo;

      SaveBulletPack;
      SaveWeaponAmmoPack;
      LoadCellTextures;

      CheckMaps;
    end;
end;

procedure TfrmMain.MenuItem9Click(Sender: TObject);
var
  frmWeaponsAmmoEditor : TfrmWeaponsAmmoEditor = nil;
begin
  frmWeaponsAmmoEditor := TfrmWeaponsAmmoEditor.Create(Self);
  with frmWeaponsAmmoEditor do
    try
      ShowModal;
    finally
      Destroy;
    end;

  CheckMaps;
  LoadCellTextures;
  WeaponsGridUpdate;
  AmmoGridUpdate;
end;

procedure TfrmMain.PaintBoxMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  mX, mY : Integer;
  placeForElement : PMapElement;
begin
  if (mapElement = 0) then exit;

  mX := X div g_CellWidth;
  mY := Y div g_CellHeight;

  case (levelOfDraw) of
    0 : placeForElement := @(mapPack.maps[curMapNum].floor[mX, mY]);  
    1 : placeForElement := @(mapPack.maps[curMapNum].level[mX, mY]);
    2 : placeForElement := @(mapPack.maps[curMapNum].ceil [mX, mY]);
  end;
        
  if (Button = mbLeft ) then drawRect.isLeft  := True;
  if (Button = mbRight) then drawRect.isRight := True;

  if (ssCtrl in Shift) then
    begin
      drawRect.isDraw := True;
      drawRect.x0 := X;
      drawRect.y0 := Y;
      drawRect.x1 := X;
      drawRect.y1 := Y;
    end
  else
    begin
      if (Button = mbLeft ) then placeForElement^ := mapElement;
      if (Button = mbRight) then placeForElement^ := 0;
    end;
end;

procedure TfrmMain.PaintBoxMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  mX, mY : Integer;
  placeForElement : PMapElement;
begin
  if (mapElement = 0) then exit;

  drawRect.x1 := X;
  drawRect.y1 := Y;

  if (not drawRect.isDraw) then
    if ((drawRect.isLeft) or (drawRect.isRight)) then
      begin 
        mX := X div g_CellWidth;
        mY := Y div g_CellHeight;

        case (levelOfDraw) of
          0 : placeForElement := @(mapPack.maps[curMapNum].floor[mX, mY]);
          1 : placeForElement := @(mapPack.maps[curMapNum].level[mX, mY]);
          2 : placeForElement := @(mapPack.maps[curMapNum].ceil [mX, mY]);
        end;

        if (drawRect.isLeft ) then placeForElement^ := mapElement;
        if (drawRect.isRight) then placeForElement^ := 0;
      end;

  PaintBox.Invalidate;
end;

procedure TfrmMain.PaintBoxMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
type
  TMapDrawPosition = array [0..MAP_WIDTH - 1, 0..MAP_HEIGHT - 1] of TMapElement;
  PMapDrawPosition = ^TMapDrawPosition;

var
  i, j : Integer;
  x0, y0, x1, y1 : Integer;
  mapDrawPosition : PMapDrawPosition;
begin
  if (mapElement = 0) then exit;

  if (drawRect.isDraw) then
    begin
      x0 := drawRect.x0 div g_CellWidth;
      y0 := drawRect.y0 div g_CellHeight;

      x1 := drawRect.x1 div g_CellWidth;
      y1 := drawRect.y1 div g_CellHeight;

      if (x0 > x1) then swap(x0, x1);
      if (y0 > y1) then swap(y0, y1);

      case levelOfDraw of
        0 : mapDrawPosition := @(mapPack.maps[curMapNum].floor[0, 0]);
        1 : mapDrawPosition := @(mapPack.maps[curMapNum].level[0, 0]);
        2 : mapDrawPosition := @(mapPack.maps[curMapNum].ceil [0, 0]);
      end;

      for i := x0 to x1 do
        for j := y0 to y1 do
          begin
            if (drawRect.isLeft ) then mapDrawPosition^[i, j] := mapElement;
            if (drawRect.isRight) then mapDrawPosition^[i, j] := 0;
          end;
    end;

  drawRect.isDraw  := False; 
  drawRect.isLeft  := False;
  drawRect.isRight := False;
  //drawRect.isLeft  := False;
  //drawRect.isRight := False;
  //if (Button = mbLeft)  then drawRect.isLeft  := False;
  //if (Button = mbRight) then drawRect.isRight := False;

  PaintBox.Invalidate;
end;

procedure TfrmMain.PaintBoxMouseWheelDown(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
  if (ssCtrl in Shift) then
    ZoomBar.Position := ZoomBar.Position - 1;
end;

procedure TfrmMain.PaintBoxMouseWheelUp(Sender: TObject; Shift: TShiftState;
  MousePos: TPoint; var Handled: Boolean);
begin
  if (ssCtrl in Shift) then
    ZoomBar.Position := ZoomBar.Position + 1;
end;

end.

