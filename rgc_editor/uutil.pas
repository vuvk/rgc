unit uUtil;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  Graphics,
  Imaging,
  ImagingTypes,
  Dialogs,
  ExtCtrls,
  Grids,
  uDM;

type
  PBitmap = ^TBitmap;

procedure swap(var a, b : Integer);

// число является степенью двойки?
function IsPowerOfTwo(n : Integer) : Boolean;
// возврат ближайшего числа степени двойки к данному
function GetPowerOfTwo(n : Integer) : Integer;

// конвертирует ARGB -> ABGR и обратно
function ConvertColor(color : Cardinal) : Cardinal;
                                        
// подготовка текстуры при загрузке из OpenTextureDialog
procedure PrepareLoadTexture(var image : TImageData; maxWidth, maxHeight : Integer);
procedure PrepareLoadTexture(var image : TImageData; fileName : String; maxWidth, maxHeight : Integer);

// меняет размер TBitmap и возвращает новый с новыми размерами
function ResizeBitmap(const bitmap : TBitmap; newWidth, newHeight : Integer) : TBitmap;
// меняет размер TBitmap
procedure ResizeBitmap(newWidth, newHeight : Integer; var bitmap : TBitmap); 
// меняет размер TBitmap и записывает в поток полученную картинку
procedure ResizeBitmapToStream(const bitmap : TBitmap; newWidth, newHeight : Integer; var stream : TMemoryStream);
// затемняет TBitmap и возвращает новый
function ShadowBitmap(const bitmap : TBitmap) : TBitmap;
// безопасно загружает в окно предпросмотра битмап
procedure LoadPreviewImageFromBitmap(var image : TImage; bitmap : TBitmap);
// сохраняет TBitmap в zip
procedure SaveBitmapToZip(bitmap : TBitmap; fileName : AnsiString);    
// сохраняет TBitmap в dat
procedure SaveBitmapToDat(bitmap : TBitmap; fileName : AnsiString);
// грузит картинку из зипа и выдает на выходе TBitmap
function LoadBitmapFromZip(fileName : AnsiString) : TBitmap;      
// грузит картинку из dat и выдает на выходе TBitmap
function LoadBitmapFromDat(fileName : AnsiString) : TBitmap;

// выбрать строку в таблице
procedure GridSellectRow(grid: TDrawGrid; r: Integer);


implementation

uses
  uContainer,
  uResManager;

procedure swap(var a, b: Integer);
//var t : Integer;
begin
{  t := a;
  a := b;
  b := t;}
  a += b;
  b := a - b;
  a := a - b;
end;

function IsPowerOfTwo(n : Integer) : Boolean;
begin
  Result := ((n and (n - 1)) = 0);
end;

function GetPowerOfTwo(n : Integer) : Integer;
var
  res : Integer = 1;
begin
  while (res < n) do
    res := res shl 1;
  Result := res;
end;

function ConvertColor(color: Cardinal): Cardinal;
begin
  Result := ((color shr 24) shl 24  ) or         // Alpha
            ((color shr 16) and $FF) or          // Red  -> Blue
            (((color shr 8) and $FF) shl 8 ) or  // Green
            ((color         and $FF) shl 16);    // Blue -> Red
end;

procedure PrepareLoadTexture(var image: TImageData; maxWidth, maxHeight: Integer);
begin
  PrepareLoadTexture(image, DM.OpenTextureDialog.FileName, maxWidth, maxHeight);
end;

procedure PrepareLoadTexture(var image: TImageData; fileName: String; maxWidth, maxHeight: Integer);
var
  newWidth, newHeight : Integer;
  tmpImage : TImageData;
  w, h : Integer;
begin
  FreeImage(image);
  LoadImageFromFile(fileName, image);

  // если размер не квадратный, то подогнать под квадрат
  if (image.Width <> image.Height) then
    begin
      if (image.Width < image.Height) then
        begin
          newWidth  := image.Height;
          newHeight := image.Height;
        end
      else // (image.Width > image.Height)
        begin
          newWidth  := image.Width;
          newHeight := image.Width;
        end;

      NewImage(newWidth, newHeight, ifA8R8G8B8, tmpImage);

      // оригинальная картинка Уже по горизонтали
      if (newWidth > image.Width) then
        begin
          w := (newWidth - image.Width) shr 1;
          CopyRect(image, 0, 0, image.Width, image.Height, tmpImage, w, 0);
        end;

      // оригинальная картинка Уже по вертикали
      if (newHeight > image.Height) then
        begin
          h := newHeight - image.Height;
          CopyRect(image, 0, 0, image.Width, image.Height, tmpImage, 0, h);
        end;

      FreeImage(image);
      image := tmpImage;
    end;

  // проверяем и подгоняем текстуры к размерам по канону
  newWidth  := image.Width;
  newHeight := image.Height;

  if ((newWidth  > maxWidth) or (newHeight > maxHeight)) then
    begin
      if (newWidth  > maxWidth ) then newWidth  := maxWidth;
      if (newHeight > maxHeight) then newHeight := maxHeight;
    end
  else
    begin
      if (not IsPowerOfTwo(newWidth ) and (newWidth  < maxWidth )) then newWidth  := GetPowerOfTwo(newWidth );
      if (not IsPowerOfTwo(newHeight) and (newHeight < maxHeight)) then newHeight := GetPowerOfTwo(newHeight);
    end;

  if ((image.Width  <> newWidth) or
      (image.Height <> newHeight)) then
  begin
    {MessageDlg('Warning!',
               'Размер не соответствует необходимым и будет подогнан к ' +
               IntToStr(newWidth) + 'x' + IntToStr(newHeight) + '!',
               mtWarning, [mbOK], 0);    }
    ResizeImage(image, newWidth, newHeight, rfNearest);
  end;

  if (image.Format <> ifA8R8G8B8) then
    begin
      {MessageDlg('Warning!',
                 'Формат не соответствует необходимому (ARGB8888) и будет подогнан!',
                 mtWarning, [mbOK], 0);}
      ConvertImage(image, ifA8R8G8B8);
    end;
end;

function ResizeBitmap(const bitmap : TBitmap; newWidth, newHeight : Integer) : TBitmap;
var
  newBitmap : TBitmap;
  stream : TMemoryStream;
  image : TImageData;
begin
  if (bitmap = nil) then
    begin
      Result := nil;
      exit;
    end;
  stream := TMemoryStream.Create;

  bitmap.SaveToStream(stream);
  stream.Position := 0;
  InitImage(image);
  LoadImageFromStream(stream, image);
  ResizeImage(image, newWidth, newHeight, rfNearest);

  stream.Clear;
  stream.Position := 0;
  SaveImageToStream('bmp', stream, image);

  stream.Position := 0;
  newBitmap := TBitmap.Create;
  newBitmap.LoadFromStream(stream, stream.Size);

  stream.Destroy;
  FreeImage(image);

  Result := newBitmap;
end;

procedure ResizeBitmap(newWidth, newHeight : Integer; var bitmap : TBitmap);
var
  stream : TMemoryStream;
  image : TImageData;
begin
  if (bitmap = nil) then exit;

  stream := TMemoryStream.Create;

  bitmap.SaveToStream(stream);
  stream.Position := 0;
  InitImage(image);
  LoadImageFromStream(stream, image);
  ResizeImage(image, newWidth, newHeight, rfNearest);

  stream.Clear;
  stream.Position := 0;
  SaveImageToStream('bmp', stream, image);

  stream.Position := 0;
  bitmap.Clear;
  bitmap.LoadFromStream(stream, stream.Size);

  stream.Destroy;
  FreeImage(image);
end;

procedure ResizeBitmapToStream(const bitmap : TBitmap; newWidth, newHeight : Integer; var stream : TMemoryStream);
var
  tmpBmp : TBitmap = nil;
begin
  if (bitmap = nil) then exit;
  if (stream = nil) then stream := TMemoryStream.Create;

  stream.Clear;
  stream.Position := 0;
                         
  tmpBmp := ResizeBitmap(bitmap, newWidth, newHeight);
  tmpBmp.SaveToStream(stream);
  stream.Position := 0;
  tmpBmp.Clear;
  tmpBmp.Destroy;
end;

function ShadowBitmap(const bitmap : TBitmap) : TBitmap;
const
  COLOR_DEC = 75;
var
  tmpBmp : TBitmap = nil;
  stream : TMemoryStream;
  image : TImageData;
  x, y : Byte;
  color : TColor32Rec;
begin       
  if (bitmap = nil) then exit;  

  stream := TMemoryStream.Create;

  bitmap.SaveToStream(stream);
  stream.Position := 0;
  InitImage(image);
  LoadImageFromStream(stream, image);
  for x := 0 to image.Width - 1 do
    for y := 0 to image.Height - 1 do
      begin
        color := GetPixel32(image, x, y);
        //if (color.A >= COLOR_DEC) then color.A -= COLOR_DEC else color.A := 0;
        if (color.R >= COLOR_DEC) then color.R -= COLOR_DEC else color.R := 0;
        if (color.G >= COLOR_DEC) then color.G -= COLOR_DEC else color.G := 0;
        if (color.B >= COLOR_DEC) then color.B -= COLOR_DEC else color.B := 0;
        SetPixel32(image, x, y, color);
      end;

  stream.Clear;
  stream.Position := 0;
  SaveImageToStream('bmp', stream, image);

  stream.Position := 0;
  tmpBmp := TBitmap.Create();
  tmpBmp.LoadFromStream(stream, stream.Size);

  stream.Destroy;
  FreeImage(image);

  Result := tmpBmp;
end;

procedure LoadPreviewImageFromBitmap(var image : TImage; bitmap : TBitmap);
var
  stream : TMemoryStream = nil;
  newBitmap : TBitmap = nil;
begin
  if (image = nil) then exit;
                  
  image.Picture.Clear;
  if (bitmap <> nil) then
    begin
      stream := TMemoryStream.Create;
      stream.Position := 0;

      // если размеры картинки больше допустимого, то создать
      // временную уменьшенную копию
      if ((bitmap.Width > PREVIEW_MAX_WIDTH) or (bitmap.Height > PREVIEW_MAX_HEIGHT)) then
        begin
          newBitmap := ResizeBitmap(bitmap, PREVIEW_MAX_WIDTH, PREVIEW_MAX_HEIGHT);
          newBitmap.SaveToStream(stream);
          newBitmap.Destroy;
        end
      // а иначе загрузить в превью что есть
      else
        bitmap.SaveToStream(stream);

      stream.Position := 0;
      image.Picture.LoadFromStream(stream);

      stream.Destroy;
    end;
end;

procedure SaveBitmapToZip(bitmap: TBitmap; fileName: AnsiString);
var
  w, h : Word;
  buffer : Pointer = nil;
  bufferSize : Integer = 0;
  fName : PChar = nil;
begin
  if (bitmap = nil) then exit;

  fName := PChar(fileName);

  w := bitmap.Width;
  h := bitmap.Height;
  bufferSize := bitmap.RawImage.DataSize + SizeOf(w) + SizeOf(h);
  buffer := GetMem(bufferSize);

  // dat-формат это 4 байта на ширину, высоту + информация о цветах 4 байта на цвет
  PWord(buffer)^ := w;
  PWord(PByte(buffer) + SizeOf(w))^ := h;
  Move(PByte(bitmap.RawImage.Data)^, (PByte(buffer) + SizeOf(w) + SizeOf(h))^, bitmap.RawImage.DataSize);

  Res_WriteFileFromBufferToZip(fName, buffer, bufferSize, DEFAULT_COMPRESSION_LEVEL);

  FreeMemAndNil(buffer);
end;

procedure SaveBitmapToDat(bitmap: TBitmap; fileName: AnsiString);
var
  w, h : Word;
  buffer : Pointer = nil;
  bufferSize : Integer = 0;
  fHandle : THandle = 0;
  res : Integer;
begin
  if (bitmap = nil) then exit;

  w := bitmap.Width;
  h := bitmap.Height;
  bufferSize := bitmap.RawImage.DataSize + SizeOf(w) + SizeOf(h);
  buffer := GetMem(bufferSize);

  // dat-формат это 4 байта на ширину, высоту + информация о цветах 4 байта на цвет
  PWord(buffer)^ := w;
  PWord(PByte(buffer) + SizeOf(w))^ := h;
  Move(PByte(bitmap.RawImage.Data)^, (PByte(buffer) + SizeOf(w) + SizeOf(h))^, bitmap.RawImage.DataSize);

  if (FileExists(fileName)) then
    DeleteFile(fileName);

  fHandle := FileCreate(fileName);
  if (fHandle <> 0) then
    begin
      res := FileWrite(fHandle, PByte(buffer)^, bufferSize);
      if (res <> bufferSize) then
        MessageDlg('Error!', 'Error when writing "' + fileName + '"!', mtError, [mbOK], 0);
      FileClose(fHandle);
    end;

  FreeMemAndNil(buffer);
end;

function LoadBitmapFromZip(fileName: AnsiString): TBitmap;
var
  fName : PChar = nil;
  bufferSize : Integer;
  buffer : Pointer = nil;
  image : TImageData;
  w, h : Word;
  bitmap : TBitmap = nil;
  stream : TMemoryStream = nil;
  loadTexture : array of Byte;
  textureSize : Integer;        
  tgaHeader : TTgaHeader = (idlength : 0;
                            colourmaptype : 0;
                            datatypecode : 2;
                            colourmaporigin : 0;
                            colourmaplength : 0;
                            colourmapdepth  : 0;
                            x_origin : 0;
                            y_origin : 0;
                            width  : 128;
                            height : 128;
                            bitsperpixel : 32;
                            imagedescriptor : 40);
begin
  Result := nil;

  fName := PChar(fileName);

  bufferSize := Res_GetUncompressedFileSize(fName);
  if (bufferSize <= 0) then
    begin
      WriteLn('cannot get size of ', fName, '!');
      exit;
    end;

  buffer := GetMem(bufferSize);
  if (buffer = nil) then
    begin
      WriteLn('can not allocate memory for buffer where loading ', fName, '!');
      exit;
    end;

  if (Res_ReadFileFromZipToBuffer(fName, buffer, bufferSize) <= 0) then
    begin
      FreeMemAndNil(buffer);
      exit;
    end;

  w := PWord(buffer)^;
  h := PWord(PByte(buffer) + 2)^;
  textureSize := w * h * SizeOf(Int32) + SizeOf(tgaHeader);

  // подгружаем
  bitmap := TBitmap.Create;

  // формируем TGA заголовок
  tgaHeader.width  := w;
  tgaHeader.height := h;

  // а тут лютое колдунство. Берем заголовок файлов TGA, прикрепляем массив цветов
  // и вот это всё грузим через Vampyre  
  SetLength(loadTexture, textureSize);
  FillChar(loadTexture[0], SizeOf(loadTexture), 0);
  Move(tgaHeader, loadTexture[0], SizeOf(TTgaHeader));
  Move((PByte(buffer) + 4)^, loadTexture[SizeOf(TTgaHeader)], w * h * SizeOf(Int32)); 
  FreeMemAndNil(buffer);

  InitImage(image);
  LoadImageFromMemory(@(loadTexture[0]), textureSize, image);
  SetLength(loadTexture, 0);

  stream := TMemoryStream.Create;
  stream.Position := 0;
  SaveImageToStream('bmp', stream, image);
  FreeImage(image);

  stream.Position := 0;
  bitmap.LoadFromStream(stream);
  stream.Destroy;

  Result := bitmap;
end;

function LoadBitmapFromDat(fileName: AnsiString): TBitmap;
var
  image : TImageData;
  w, h : Word;
  stream : TMemoryStream = nil;
  bitmap : TBitmap = nil;
  loadTexture : array of Byte;
  textureSize : Integer;
  tgaHeader : TTgaHeader = (idlength : 0;
                            colourmaptype : 0;
                            datatypecode : 2;
                            colourmaporigin : 0;
                            colourmaplength : 0;
                            colourmapdepth  : 0;
                            x_origin : 0;
                            y_origin : 0;
                            width  : 128;
                            height : 128;
                            bitsperpixel : 32;
                            imagedescriptor : 40);
  fHandle : THandle = 0;
begin
  Result := nil;

  if (not FileExists(fileName)) then exit;

  fHandle := FileOpen(fileName, fmOpenRead);
  if (fHandle = 0) then exit;

  FileRead(fHandle, w, SizeOf(w));
  FileRead(fHandle, h, SizeOf(h));

  textureSize := w * h * SizeOf(Int32) + SizeOf(tgaHeader);

  // подгружаем
  bitmap := TBitmap.Create;

  // формируем TGA заголовок
  tgaHeader.width  := w;
  tgaHeader.height := h;

  // а тут лютое колдунство. Берем заголовок файлов TGA, прикрепляем массив цветов
  // и вот это всё грузим через Vampyre
  SetLength(loadTexture, textureSize);
  FillChar(loadTexture[0], SizeOf(loadTexture), 0);
  Move(tgaHeader, loadTexture[0], SizeOf(TTgaHeader));
  FileRead(fHandle, loadTexture[SizeOf(TTgaHeader)], w * h * SizeOf(Int32));
  FileClose(fHandle);

  InitImage(image);
  LoadImageFromMemory(@(loadTexture[0]), textureSize, image);
  SetLength(loadTexture, 0);

  stream := TMemoryStream.Create;
  stream.Position := 0;
  SaveImageToStream('bmp', stream, image);
  FreeImage(image);

  stream.Position := 0;
  bitmap.LoadFromStream(stream);
  stream.Destroy;

  Result := bitmap;
end;

procedure GridSellectRow(grid: TDrawGrid; r: Integer);
begin
  grid.Row := r;
  grid.Selection := TGridRect(Rect(grid.FixedCols, r, grid.ColCount, r));
end;

end.

