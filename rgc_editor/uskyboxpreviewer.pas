unit uSkyboxPreviewer;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  SysUtils,
  FileUtil,
  OpenGLContext,
  Forms,
  Controls,
  Graphics,
  Dialogs,
  LCLType,
  Imaging,
  ImagingFormats,
  ImagingOpenGL,
  Math,
  GL,
  Glu,
  StdCtrls,
  ExtCtrls;

type

  { TfrmSkyboxPreviewer }

  TfrmSkyboxPreviewer = class(TForm)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    Shape4: TShape;
    Shape5: TShape;
    Shape6: TShape;
    ShowSidesChk: TCheckBox;
    GroupBox1: TGroupBox;
    OpenGLControl: TOpenGLControl;
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OpenGLControlMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure OpenGLControlMouseLeave(Sender: TObject);
    procedure OpenGLControlMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure OpenGLControlMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure OpenGLControlPaint(Sender: TObject);
    procedure ShowSidesChkChange(Sender: TObject);
  private            
    frontTexture  : GLuint;
    backTexture   : GLuint;
    leftTexture   : GLuint;
    rightTexture  : GLuint;
    topTexture    : GLuint;
    bottomTexture : GLuint;

    angleX : Single;
    angleY : Single;

    mouseAim : Boolean;
    oldMouseX : Integer;
    oldMouseY : Integer;
  public
    skyboxNum  : Integer;
  end;

implementation

uses
  uContainer;

{$R *.lfm}

{ TfrmSkyboxPreviewer }

procedure TfrmSkyboxPreviewer.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  if (frontTexture  <> 0) then glDeleteTextures(1, @frontTexture );
  if (backTexture   <> 0) then glDeleteTextures(1, @backTexture  );
  if (leftTexture   <> 0) then glDeleteTextures(1, @leftTexture  );
  if (rightTexture  <> 0) then glDeleteTextures(1, @rightTexture );
  if (topTexture    <> 0) then glDeleteTextures(1, @topTexture   );
  if (bottomTexture <> 0) then glDeleteTextures(1, @bottomTexture);
end;

procedure TfrmSkyboxPreviewer.FormActivate(Sender: TObject);
begin
  SetFocus;
end;

procedure TfrmSkyboxPreviewer.FormCreate(Sender: TObject);
begin
  frontTexture  := 0;
  backTexture   := 0;
  leftTexture   := 0;
  rightTexture  := 0;
  topTexture    := 0;
  bottomTexture := 0;

  skyboxNum  := -1;

  angleX := 0.0;
  angleY := 0.0;

  mouseAim := False;
end;

procedure TfrmSkyboxPreviewer.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_ESCAPE) then Close;
end;

procedure TfrmSkyboxPreviewer.FormResize(Sender: TObject);
begin
  OpenGLControlPaint(Self);
end;

procedure TfrmSkyboxPreviewer.FormShow(Sender: TObject);
var
  bitmap : TBitmap = nil;
  stream : TMemoryStream = nil;
begin
  if (not OpenGLControl.MakeCurrent()) then
    begin
      MessageDlg('Error!', 'Can''t initialize OpenGL! Please, check your videocard or drivers!', mtError, [mbOK], 0);
      Close;
    end;
                              
  stream := TMemoryStream.Create;

  glClearColor(0.9, 0.7, 0.25, 1.0);
  glCullFace(GL_FRONT);
  glEnable(GL_CULL_FACE);

  if (skyboxNum <> -1) then
    begin
      // front
      bitmap := LoadSkybox(skyboxNum, SKYBOX_FRONT);
      if (bitmap <> nil) then
        begin
          bitmap.SaveToStream(stream);
          bitmap.Destroy;
          stream.Position := 0;
          frontTexture := LoadGLTextureFromStream(stream);
          stream.Clear;
        end;

      // back
      bitmap := LoadSkybox(skyboxNum, SKYBOX_BACK);
      if (bitmap <> nil) then
        begin
          bitmap.SaveToStream(stream);
          bitmap.Destroy;  
          stream.Position := 0;
          backTexture := LoadGLTextureFromStream(stream);
          stream.Clear;
        end;  

      // left
      bitmap := LoadSkybox(skyboxNum, SKYBOX_LEFT);
      if (bitmap <> nil) then
        begin
          bitmap.SaveToStream(stream);
          bitmap.Destroy;    
          stream.Position := 0;
          leftTexture := LoadGLTextureFromStream(stream);
          stream.Clear;
        end;    

      // right
      bitmap := LoadSkybox(skyboxNum, SKYBOX_RIGHT);
      if (bitmap <> nil) then
        begin
          bitmap.SaveToStream(stream);
          bitmap.Destroy;   
          stream.Position := 0;
          rightTexture := LoadGLTextureFromStream(stream);
          stream.Clear;
        end;  

      // top
      bitmap := LoadSkybox(skyboxNum, SKYBOX_TOP);
      if (bitmap <> nil) then
        begin
          bitmap.SaveToStream(stream);
          bitmap.Destroy;  
          stream.Position := 0;
          topTexture := LoadGLTextureFromStream(stream);
          stream.Clear;
        end;

      // bottom
      bitmap := LoadSkybox(skyboxNum, SKYBOX_BOTTOM);
      if (bitmap <> nil) then
        begin
          bitmap.SaveToStream(stream);
          bitmap.Destroy;  
          stream.Position := 0;
          bottomTexture := LoadGLTextureFromStream(stream);
          stream.Clear;
        end;
    end;

  stream.Destroy;
end;

procedure TfrmSkyboxPreviewer.OpenGLControlMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  mouseAim := True;
  oldMouseX := X;
  oldMouseY := Y;
end;

procedure TfrmSkyboxPreviewer.OpenGLControlMouseLeave(Sender: TObject);
begin
  mouseAim := False;
end;

procedure TfrmSkyboxPreviewer.OpenGLControlMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
var
  dX, dY : Single;
begin
  if (mouseAim) then
    begin
      dX := (oldMouseX - X) / 4.0;
      dY := (oldMouseY - Y) / 4.0;

      angleX += dX;
      angleY += dY;

      if (angleX <    0) then angleX += 360;
      if (angleX >= 360) then angleX -= 360;

      if (angleY < -89) then angleY := -89;
      if (angleY >  89) then angleY :=  89;

      oldMouseX := X;
      oldMouseY := Y;
    end;

  OpenGLControlPaint(Self);
end;

procedure TfrmSkyboxPreviewer.OpenGLControlMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  mouseAim := False;
end;

procedure TfrmSkyboxPreviewer.OpenGLControlPaint(Sender: TObject);
const
  DEG_TO_RAD_COEFF = 1.0 / 180.0 * 3.1415926535;
var
  clr : array [0..2] of Single;
begin
  glClear(GL_COLOR_BUFFER_BIT);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();                  
  glViewport(0, 0, OpenGLControl.Width, OpenGLControl.Height);

  glMatrixMode(GL_MODELVIEW);     
  glLoadIdentity();
  gluPerspective(80.0, OpenGLControl.Width / OpenGLControl.Height, 0.1, 100.0);
  gluLookAt(0.0, 0.0, 0.0,
            -sin(angleX * DEG_TO_RAD_COEFF), (tan(angleY * DEG_TO_RAD_COEFF)), -cos(angleX * DEG_TO_RAD_COEFF),
            0.0, 1.0, 0.0);


  // FRONT
  if (frontTexture <> 0) then
    begin
      glBindTexture(GL_TEXTURE_2D, frontTexture);
      glEnable(GL_TEXTURE_2D);
      if (ShowSidesChk.Checked) then
        begin
          clr[0] := 1.0;
          clr[1] := 0.0;
          clr[2] := 0.0;
        end
      else
        begin
          clr[0] := 1.0;
          clr[1] := 1.0;
          clr[2] := 1.0;
        end;
      glBegin(GL_QUADS);
        glColor3fv(@clr);
        glTexCoord2f(0.0, 0.0);
        glVertex3f(-1.0,  1.0, -1.0);

        glColor3fv(@clr);
        glTexCoord2f(1.0, 0.0);
        glVertex3f( 1.0,  1.0, -1.0);

        glColor3fv(@clr);
        glTexCoord2f(1.0, 1.0);
        glVertex3f( 1.0, -1.0, -1.0);

        glColor3fv(@clr);
        glTexCoord2f(0.0, 1.0);
        glVertex3f(-1.0, -1.0, -1.0);
      glEnd();
      glDisable(GL_TEXTURE_2D);
    end;

  // BACK
  if (backTexture <> 0) then
    begin
      glBindTexture(GL_TEXTURE_2D, backTexture);
      glEnable(GL_TEXTURE_2D);
      if (ShowSidesChk.Checked) then
        begin
          clr[0] := 0.0;
          clr[1] := 1.0;
          clr[2] := 0.0;
        end
      else
        begin
          clr[0] := 1.0;
          clr[1] := 1.0;
          clr[2] := 1.0;
        end;
      glBegin(GL_QUADS);
        glColor3fv(@clr);
        glTexCoord2f(0.0, 0.0);
        glVertex3f( 1.0,  1.0,  1.0);

        glColor3fv(@clr);
        glTexCoord2f(1.0, 0.0);
        glVertex3f(-1.0,  1.0,  1.0);

        glColor3fv(@clr);
        glTexCoord2f(1.0, 1.0);
        glVertex3f(-1.0, -1.0,  1.0);

        glColor3fv(@clr);
        glTexCoord2f(0.0, 1.0);
        glVertex3f( 1.0, -1.0,  1.0);
      glEnd();
      glDisable(GL_TEXTURE_2D);
    end;


  // LEFT           
  if (leftTexture <> 0) then
    begin
      glBindTexture(GL_TEXTURE_2D, leftTexture);
      glEnable(GL_TEXTURE_2D);
      if (ShowSidesChk.Checked) then
        begin
          clr[0] := 0.0;
          clr[1] := 0.0;
          clr[2] := 1.0;
        end
      else
        begin
          clr[0] := 1.0;
          clr[1] := 1.0;
          clr[2] := 1.0;
        end;
      glBegin(GL_QUADS);
        glColor3fv(@clr);
        glTexCoord2f(0.0, 0.0);
        glVertex3f(-1.0,  1.0,  1.0);

        glColor3fv(@clr);
        glTexCoord2f(1.0, 0.0);
        glVertex3f(-1.0,  1.0, -1.0);

        glColor3fv(@clr);
        glTexCoord2f(1.0, 1.0);
        glVertex3f(-1.0, -1.0, -1.0);

        glColor3fv(@clr);
        glTexCoord2f(0.0, 1.0);
        glVertex3f(-1.0, -1.0,  1.0);
      glEnd();
      glDisable(GL_TEXTURE_2D);
    end;

  // RIGHT        
  if (rightTexture <> 0) then
    begin
      glBindTexture(GL_TEXTURE_2D, rightTexture);
      glEnable(GL_TEXTURE_2D);
      if (ShowSidesChk.Checked) then
        begin
          clr[0] := 1.0;
          clr[1] := 0.0;
          clr[2] := 1.0;
        end
      else
        begin
          clr[0] := 1.0;
          clr[1] := 1.0;
          clr[2] := 1.0;
        end;
      glBegin(GL_QUADS);
        glColor3fv(@clr);
        glTexCoord2f(0.0, 0.0);
        glVertex3f( 1.0,  1.0, -1.0);

        glColor3fv(@clr);
        glTexCoord2f(1.0, 0.0);
        glVertex3f( 1.0,  1.0,  1.0);

        glColor3fv(@clr);
        glTexCoord2f(1.0, 1.0);
        glVertex3f( 1.0, -1.0,  1.0);

        glColor3fv(@clr);
        glTexCoord2f(0.0, 1.0);
        glVertex3f( 1.0, -1.0, -1.0);
      glEnd();
      glDisable(GL_TEXTURE_2D);
    end;

  // TOP    
  if (topTexture <> 0) then
    begin
      glBindTexture(GL_TEXTURE_2D, topTexture);
      glEnable(GL_TEXTURE_2D);
      if (ShowSidesChk.Checked) then
        begin
          clr[0] := 1.0;
          clr[1] := 1.0;
          clr[2] := 0.0;
        end
      else
        begin
          clr[0] := 1.0;
          clr[1] := 1.0;
          clr[2] := 1.0;
        end;
      glBegin(GL_QUADS);
        glColor3fv(@clr);
        glTexCoord2f(1.0, 1.0);
        glVertex3f(-1.0,  1.0, -1.0);

        glColor3fv(@clr);
        glTexCoord2f(0.0, 1.0);
        glVertex3f(-1.0,  1.0,  1.0);

        glColor3fv(@clr);
        glTexCoord2f(0.0, 0.0);
        glVertex3f( 1.0,  1.0,  1.0);

        glColor3fv(@clr);
        glTexCoord2f(1.0, 0.0);
        glVertex3f( 1.0,  1.0, -1.0);
      glEnd();
      glDisable(GL_TEXTURE_2D);
    end;

  // BOTTOM    
  if (bottomTexture <> 0) then
    begin
      glBindTexture(GL_TEXTURE_2D, bottomTexture);
      glEnable(GL_TEXTURE_2D);
      if (ShowSidesChk.Checked) then
        begin
          clr[0] := 0.0;
          clr[1] := 1.0;
          clr[2] := 1.0;
        end
      else
        begin
          clr[0] := 1.0;
          clr[1] := 1.0;
          clr[2] := 1.0;
        end;
      glBegin(GL_QUADS);
        glColor3fv(@clr);
        glTexCoord2f(0.0, 0.0);
        glVertex3f(-1.0, -1.0,  1.0);

        glColor3fv(@clr);
        glTexCoord2f(1.0, 0.0);
        glVertex3f(-1.0, -1.0, -1.0);

        glColor3fv(@clr);
        glTexCoord2f(1.0, 1.0);
        glVertex3f( 1.0, -1.0, -1.0);

        glColor3fv(@clr);
        glTexCoord2f(0.0, 1.0);
        glVertex3f( 1.0, -1.0,  1.0);
      glEnd();
      glDisable(GL_TEXTURE_2D);
    end;

  OpenGLControl.SwapBuffers;
end;

procedure TfrmSkyboxPreviewer.ShowSidesChkChange(Sender: TObject);
begin
  OpenGLControlPaint(Self);
end;

end.

